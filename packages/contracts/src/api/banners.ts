/* eslint-disable */
/* tslint:disable */
/*
 * ---------------------------------------------------------------
 * ## THIS FILE WAS GENERATED VIA SWAGGER-TYPESCRIPT-API        ##
 * ##                                                           ##
 * ## AUTHOR: acacode                                           ##
 * ## SOURCE: https://github.com/acacode/swagger-typescript-api ##
 * ---------------------------------------------------------------
 */

export type AdminResponseBanners = ({
  id?: number;
  desktop?: string;
  tablet?: string;
  mobile?: string;
} & Banner)[];

export type RequestBanner = {
  desktop?: string;
  tablet?: string;
  mobile?: string;
} & Banner;

export interface Banner {
  /**
   * тип баннера <br> * `1` - слайдер главная<br> * `2` - одиночный главная<br> * `3` - одиночный в списке товаров<br> * `3` - одиночный в каталоге под товарами
   *
   * @example 1
   */
  type?: 1 | 2 | 3 | 4;

  /**
   * ссылка, на которую ведет баннер
   * @example https://grosternew.vertical-web.ru/catalog
   */
  url?: string | null;

  /**
   * порядок вывода баннера. Чем меньше значение, тем раньше выводятся
   * @example 1
   */
  weight?: number;
}
