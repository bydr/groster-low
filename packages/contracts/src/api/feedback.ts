/* eslint-disable */
/* tslint:disable */
/*
 * ---------------------------------------------------------------
 * ## THIS FILE WAS GENERATED VIA SWAGGER-TYPESCRIPT-API        ##
 * ##                                                           ##
 * ## AUTHOR: acacode                                           ##
 * ## SOURCE: https://github.com/acacode/swagger-typescript-api ##
 * ---------------------------------------------------------------
 */

export interface Feedback {
  /**
   * Имя
   * @example Александр
   */
  name: string;

  /**
   * телефон
   * @example +79889632541
   */
  phone: string;

  /**
   * email
   * @example test@example.com
   */
  email: string;

  /**
   * сообщение
   * @example У меня важный вопрос
   */
  message: string;
}

export interface RequestNotFoundNeeded {
  /**
   * телефон
   * @example +79889632541
   */
  phone: string;

  /**
   * сообщение
   * @example У меня важный вопрос
   */
  message: string;
}

export interface RequestFoundCheaper {
  /**
   * телефон
   * @example +79889632541
   */
  phone: string;

  /**
   * ссылка на товар другого магазина
   * @example https://anothershop.ru
   */
  link: string;

  /**
   * сообщение
   * @example Нашел дешевле
   */
  message: string;
}

export type ResponseFaq = FaqBlock[];

export interface FaqBlock {
  /**
   * название блока
   * @example Общие вопросы
   */
  name?: string;

  /** список вопросов-ответов */
  questions?: Question[];
}

export interface Question {
  /**
   * вопрос
   * @example Как осуществляется доставка в регионы?
   */
  title?: string;

  /**
   * ответ
   * @example На основе одного экземпляра продукта можно создать два сайта в рамках одного веб-проекта.
   */
  answer?: string;
}

export type ResponseAdminFaq = (Question & { block?: number })[];

export type RequestQuestion = Question & { weight?: number; block?: number };

export type ResponseAdminBlock = ({ id?: number } & AdminBlock)[];

export interface AdminBlock {
  /**
   * название блока
   * @example Общие вопросы
   */
  name?: string;

  /**
   * порядок вывода блока
   * @example 1
   */
  weight?: number;
}

export interface V1RecallCreatePayload {
  /**
   * Имя
   * @example Александр
   */
  name: string;

  /**
   * телефон
   * @example +79889632541
   */
  phone: string;
}

export interface V1SubscribeCreatePayload {
  /**
   * Email
   * @example test@example.com
   */
  email: string;
}

export interface V1UnsubscribeCreatePayload {
  /**
   * Email
   * @example test@example.com
   */
  email: string;
}
