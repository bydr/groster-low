/* eslint-disable */
/* tslint:disable */
/*
 * ---------------------------------------------------------------
 * ## THIS FILE WAS GENERATED VIA SWAGGER-TYPESCRIPT-API        ##
 * ##                                                           ##
 * ## AUTHOR: acacode                                           ##
 * ## SOURCE: https://github.com/acacode/swagger-typescript-api ##
 * ---------------------------------------------------------------
 */

export interface CartResponse {
  products?: Product[];

  /**
   * Примененный промокод
   * @example newYear2021
   */
  promocode?: string;

  /**
   * размер скидки в копейках
   * @example 2000
   */
  discount?: number;

  /**
   * сумма к оплате в копейках
   * @example 10240
   */
  total_cost?: number;
}

export interface ChangeQtyRequest {
  /**
   * uid корзины, если есть
   * @example gcart4b340550242239.64159797
   */
  cart?: string;

  /**
   * uuid товара, который добавляется в корзину
   * @example 00000000-0000-0000-0000-000000000000
   */
  product: string;

  /**
   * Количество единиц товара.
   * @example 1
   */
  quantity: number;

  /**
   * если товар является составляющей комплекта, то здесь передается uuid комплекта
   * @example 00000000-0000-0000-0000-000000000001
   */
  parent?: string;
}

export interface ChangeQtyResponse {
  /**
   * uid корзины, в которую добавили товар
   * @example gcart4b340550242239.64159797
   */
  cart?: string;

  /**
   * Количество добавленных единиц
   * @example 12
   */
  addedQty?: number;

  /**
   * размер скидки в копейках
   * @example 2000
   */
  discount?: number;

  /**
   * сумма к оплате в копейках
   * @example 10240
   */
  total_cost?: number;
}

export interface DeleteProductRequest {
  /**
   * uuid корзины
   * @example gcart4b340550242239.64159797
   */
  cart: string;

  /**
   * uuid товара
   * @example 00000000-0000-0000-0000-000000000000
   */
  product: string;

  /**
   * если товар является составляющей комплекта, то здесь передается uuid комплекта
   * @example 00000000-0000-0000-0000-000000000001
   */
  parent?: string;
}

export interface DeleteProductResponse {
  /**
   * uid корзины, в которую добавили товар
   * @example gcart4b340550242239.64159797
   */
  cart?: string;

  /**
   * размер скидки в копейках
   * @example 2000
   */
  discount?: number;

  /**
   * сумма к оплате в копейках
   * @example 10240
   */
  total_cost?: number;
}

export interface Product {
  /**
   * uuid товара
   * @example 00000000-0000-0000-0000-000000000000
   */
  uuid?: string;

  /**
   * количество в заказе
   * @example 50
   */
  quantity?: number;

  /**
   * количество образцов в заказе (максимум 2)
   * @example 1
   */
  sample?: number;
}

export interface FastOrderRequest {
  /**
   * email или телефон, на который оформляется заказ
   * @example test@example.com
   */
  contact: string;
}

export interface V1FreeShippingListParams {
  /**
   * населенный пункт заказа
   * @example г Волгоград
   */
  region?: string;
}
