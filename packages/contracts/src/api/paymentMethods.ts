/* eslint-disable */
/* tslint:disable */
/*
 * ---------------------------------------------------------------
 * ## THIS FILE WAS GENERATED VIA SWAGGER-TYPESCRIPT-API        ##
 * ##                                                           ##
 * ## AUTHOR: acacode                                           ##
 * ## SOURCE: https://github.com/acacode/swagger-typescript-api ##
 * ---------------------------------------------------------------
 */

export interface PaymentMethod {
  /**
   * уникальный идентификатор метода.
   * @example gpay00000000
   */
  uid?: string;

  /**
   * заголовок
   * @example Оплата картой
   */
  name?: string;

  /**
   * очередность способа
   * @example 1
   */
  weight?: number;
}

export type AdminPaymentMethod = { is_active?: boolean } & PaymentMethod;
