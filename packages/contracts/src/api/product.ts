/* eslint-disable */
/* tslint:disable */
/*
 * ---------------------------------------------------------------
 * ## THIS FILE WAS GENERATED VIA SWAGGER-TYPESCRIPT-API        ##
 * ##                                                           ##
 * ## AUTHOR: acacode                                           ##
 * ## SOURCE: https://github.com/acacode/swagger-typescript-api ##
 * ---------------------------------------------------------------
 */

export type ProductListResponse = Product[];

export type ProductDetailResponse = Product & {
  description?: string;
  params?: string[];
  variations?: string[];
  meta_title?: string;
  meta_description?: string;
  kit_parents?: string[];
};

export interface Product {
  /** @example 00000000-0000-0000-0000-000000000000 */
  uuid?: string;

  /**
   * алиас товара
   * @example paket-bumajniy
   */
  alias?: string;

  /** @example Пакет бумажный 140*95*305 Крафт (650 шт.кор) */
  name?: string;
  images?: string[];

  /**
   * Цена товара в копейках
   * @example 12500
   */
  price?: number;

  /**
   * количество на складах
   * @example 50
   */
  total_qty?: number;

  /**
   * количество на основном складе
   * @example 5
   */
  fast_qty?: number;

  /** @example НФ-00000888 */
  code?: string;

  /**
   * базовая единицы измерения
   * @example шт
   */
  unit?: string;

  /** тара */
  containers?: {
    only_containers?: boolean;
    units?: { name?: string; value?: number }[];
  };
  categories?: string[];

  /**
   * список uuid товаров состава комплекта
   * @example ["00000000-0000-0000-0000-000000000000","00000000-0000-0000-0000-000000000001"]
   */
  kit?: string[];

  /**
   * является ли частью комплекта
   * @example ["00000000-0000-0000-0000-000000000000"]
   */
  kit_parents?: string[];

  /** характеристики товара */
  properties?: { name?: string; value?: string }[];

  /**
   * родитель для вариации
   * @example 00000000-0000-0000-0000-000000000002
   */
  parent?: string;

  /**
   * список товаров-аналогов
   * @example {"fast":[{"uuid":"00000000-0000-0000-0000-000000000000","weight":1}],"other":[]}
   */
  analogs?: {
    fast?: { uuid?: string; weight?: number }[];
    other?: { uuid?: string; weight?: number }[];
  };

  /**
   * список сопутствующих товаров
   * @example [{"uuid":"00000000-0000-0000-0000-000000000000","weight":1}]
   */
  companions?: { uuid?: string; weight?: number }[];

  /** список складов */
  stores?: {
    uuid?: string;
    name?: string;
    quantity?: number;
    is_main?: boolean;
  }[];

  /**
   * является ли товар хитом
   * @example true
   */
  is_bestseller?: boolean;

  /**
   * является ли товар новинкой
   * @example true
   */
  is_new?: boolean;

  /**
   * ключевые слова
   * @example крышка, супница
   */
  keywords?: string;

  /**
   * можно ли продавать образцы
   * @example true
   */
  allow_sample?: boolean;
}

/**
 * @example name, categories
 */
export type Fields = string[];

export interface V1ProductListListParams {
  /**
   * список uuid или алиасов товаров, разделенных запятой. P.S. названия полей из Product. SnakeCase меняется на CamelCase
   * @example paket-bumajniy,stakan
   */
  uuid: string;

  /** список полей, которые нужно вывести, разделенные запятой */
  fields?: Fields;
}

export interface V1ProductDetailParams {
  /** список полей, которые нужно вывести, разделенные запятой. P.S. названия полей из Product. SnakeCase меняется на CamelCase */
  fields?: Fields;

  /**
   * uuid или алиас товара
   * @example paket-bumajniy
   */
  uuid: string;
}

export interface V1ProductWaitingCreatePayload {
  /**
   * email, куда будет сообщено о поступлении товара
   * @example test@example.com
   */
  email: string;
}
