/* eslint-disable */
/* tslint:disable */
/*
 * ---------------------------------------------------------------
 * ## THIS FILE WAS GENERATED VIA SWAGGER-TYPESCRIPT-API        ##
 * ##                                                           ##
 * ## AUTHOR: acacode                                           ##
 * ## SOURCE: https://github.com/acacode/swagger-typescript-api ##
 * ---------------------------------------------------------------
 */

export interface CatalogResponse {
  products?: Product[];

  /**
   * количество доступных товаров по фильтрам
   * @example [{"00000000-0000-0000-0000-000000000000":10}]
   */
  params?: Record<string, number>[];

  /** диапазон доступных цен в копейках */
  priceRange?: { min?: number; max?: number };

  /**
   * общее количество товаров по фильтрам
   * @example 120
   */
  total?: number;
}

export interface CategoryResponse {
  bussinessAreas?: Category[];
  categories?: Category[];
}

export type CategoryByAreaResponse = CategoryByArea[];

/**
 * список параметров
 */
export type ParamsResponse = Param[];

/**
 * типы сортировки
 */
export type SortTypeResponse = { alias?: string; name?: string }[];

/**
 * список тегов
 */
export type TagResponse = { id?: number; name?: string; url?: string }[];

export type AdminCategory = Category & { name_1c?: string };

export interface Category {
  /** @example 2 */
  id?: number;

  /** @example 00000000-0000-0000-0000-000000000000 */
  uuid?: string;

  /** @example стаканы */
  name?: string;

  /** @example stakany */
  alias?: string;

  /**
   * id родительской категории
   * @example 1
   */
  parent?: number;

  /** @example https://groster-service.com/images/2a/cat2.jpg */
  image?: string;
  tags?: number[];

  /**
   * количество товаров в категории
   * @example 125
   */
  product_qty?: number;

  /**
   * мета заголовок
   * @example Купить Пакет бумажный
   */
  meta_title?: string;

  /**
   * мета описание
   * @example Купить Пакет бумажный в Волгограде
   */
  meta_description?: string;

  /**
   * ссылка на файл "Помощь в выборе"
   * @example https://groster-service.com/files/cat2.jpg
   */
  selection_file?: string;

  /**
   * 'вес' категории при сортировке
   * @example 1
   */
  weight?: number;
}

export interface CategoryByArea {
  /**
   * uuid категории
   * @example 00000000-0000-0000-0000-000000000000
   */
  uuid?: string;

  /**
   * количество товаров в категории
   * @example 12
   */
  product_qty?: number;
  children?: CategoryByArea[];

  /**
   * 'вес' категории при сортировке
   * @example 1
   */
  weight?: number;
}

export interface Param {
  /** @example 00000000-0000-0000-0000-000000000000 */
  uuid?: string;

  /** @example цвет */
  name?: string;

  /**
   * порядок вывода
   * @example 1
   */
  order?: number;
  values?: { uuid?: string; name?: string; product_qty?: number; image?: string }[];
}

export interface Product {
  /** @example 00000000-0000-0000-0000-000000000000 */
  uuid?: string;

  /**
   * алиас товара
   * @example paket-bumajniy
   */
  alias?: string;

  /** @example Пакет бумажный 140*95*305 Крафт (650 шт.кор) */
  name?: string;
  images?: string[];

  /**
   * Цена товара в копейках
   * @example 12500
   */
  price?: number;

  /**
   * количество на складах
   * @example 50
   */
  total_qty?: number;

  /**
   * количество товара на основном складе
   * @example 20
   */
  fast_qty?: number;

  /** @example НФ-00000888 */
  code?: string;
  categories?: string[];
  business_areas?: string[];

  /**
   * базовая единицы измерения
   * @example шт
   */
  unit?: string;

  /** тара */
  containers?: { only_containers?: boolean; units?: { name?: string; value?: number }[] };

  /** характеристики товара */
  properties?: { name?: string; value?: string }[];

  /** список складов */
  stores?: { uuid?: string; name?: string; quantity?: number; is_main?: boolean }[];

  /**
   * список uuid товаров состава комплекта
   * @example ["00000000-0000-0000-0000-000000000000","00000000-0000-0000-0000-000000000001"]
   */
  kit?: string[];

  /**
   * является ли товар хитом
   * @example true
   */
  is_bestseller?: boolean;

  /**
   * является ли товар новинкой
   * @example true
   */
  is_new?: boolean;

  /**
   * параметры товара
   * @example ["00000000-0000-0000-0000-000000000000","00000000-0000-0000-0000-000000000001"]
   */
  params?: string[];

  /**
   * список товаров-аналогов
   * @example {"fast":[{"uuid":"00000000-0000-0000-0000-000000000000","weight":1}],"other":[]}
   */
  analogs?: { fast?: { uuid?: string; weight?: number }[]; other?: { uuid?: string; weight?: number }[] };

  /**
   * ключевые слова
   * @example крышка, супница
   */
  keywords?: string;
}

/**
 * Сущность, которая хранит ссылки на некоторые страницы
 */
export interface Tag {
  /** @example Бумажные стаканы */
  name?: string;

  /** @example https://groster-service.com/catalog/bumazhnaya_upakovka_dlya_fastfuda_kafe_konditerskikh_izd/bumazhnye_stakany/ */
  url?: string;

  /** @example Бумажные стаканы */
  meta_title?: string;

  /** @example Качественные бумажные стаканы для воды */
  meta_description?: string;

  /** @example Бумажные стаканы */
  h1?: string;

  /** @example Бумажные стаканы */
  text?: string;
}

export interface V1CatalogListParams {
  /**
   * список выбранных категорий, разделенных запятой
   * @example 00000000-0000-0000-0000-000000000000,00000000-0000-0000-0000-000000000001
   */
  categories?: string;

  /**
   * список параметров фильтрации, разделенных запятой
   * @example 00000000-0000-0000-0000-000000000000,00000000-0000-0000-0000-000000000001
   */
  params?: string;

  /**
   * диапазон цен для фильтрации
   * @example 2000-8000
   */
  price?: string;

  /**
   * возвращать только доступные для заказы товары <br> * `0` - возвращать все товары (по умолчанию)<br> * `1` - возвращать только доступные для заказа
   *
   * @example 1
   */
  is_enabled?: 0 | 1;

  /**
   * возвращать только товары, доступные для быстрой доставки <br> * `0` - возвращать все товары (по умолчанию)<br> * `1` - возвращать только доступные для быстрой доставки
   *
   * @example 1
   */
  is_fast?: 0 | 1;

  /**
   * номер страницы списка товаров
   * @example 1
   */
  page?: number;

  /**
   * возвращать только хиты. Если false (по умолчанию), то возвращаются все товары
   * @example true
   */
  bestseller?: boolean;

  /**
   * возвращать только новинки. Если false (по умолчанию), то возвращаются все товары
   * @example true
   */
  new?: boolean;

  /**
   * uuid складов, разделенные запятой, на которых искать товары
   * @example c6559408-1695-11ea-2093-ac1f6b855a52,12477414-e0ba-11ea-608d-ac1f6b855a52
   */
  store?: string;

  /**
   * Порядок сортировки:<br> * `name` - по алфавиту<br> * `price_asc` - по возрастанию цены<br> * `price_desc` - по убыванию цены * `popular` - по популярности
   *
   */
  sortby?: "name" | "price_asc" | "price_desc" | "popular";

  /** количество товаров в ответе (макс 100) */
  per_page?: number;
}

export interface V1CatalogPricelistListParams {
  /**
   * список выбранных категорий, разделенных запятой
   * @example 00000000-0000-0000-0000-000000000000,00000000-0000-0000-0000-000000000001
   */
  categories?: string;

  /**
   * email, куда отправить прайслист после создания
   * @example email@example.com
   */
  email?: string;
}

export interface V1AdminCategoryUpdatePayload {
  /**
   * Название категории для сайта
   * @example Стаканчики
   */
  name: string;

  /**
   * Url категории для сайта
   * @example stakanchiki
   */
  alias: string;

  /** Теги категории */
  tags: number[];

  /** Страница помощь в выборе */
  help_page?: string;

  /**
   * мета заголовок
   * @example Купить Пакет бумажный
   */
  meta_title?: string;

  /**
   * мета описание
   * @example Купить Пакет бумажный в Волгограде
   */
  meta_description?: string;
}

export interface V1AdminTagAddCreatePayload {
  /** Сущность, которая хранит ссылки на некоторые страницы */
  type?: Tag;
}

export interface V1AdminTagsUpdatePayload {
  /**
   * Название категории для сайта
   * @example Стаканчики
   */
  name: string;
}
