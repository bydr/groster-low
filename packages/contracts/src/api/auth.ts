/* eslint-disable */
/* tslint:disable */
/*
 * ---------------------------------------------------------------
 * ## THIS FILE WAS GENERATED VIA SWAGGER-TYPESCRIPT-API        ##
 * ##                                                           ##
 * ## AUTHOR: acacode                                           ##
 * ## SOURCE: https://github.com/acacode/swagger-typescript-api ##
 * ---------------------------------------------------------------
 */

export interface RequestSendCode {
  /**
   * email, на который отправится письмо с проверкой, или номер телефона, на который отправится смс
   * @example +7(999) 999-99-99
   */
  contact: string;
}

export interface ResponseSendCode {
  /**
   * свободен ли контакт пользователя
   * если да - то регистрация дальше
   * если нет, то авторизация
   * @example true
   */
  is_free: boolean;

  /**
   * время жизни кода (в секундах).
   * @example 120
   */
  lifetime: number;
}

export interface RequestResetPassword {
  /**
   * email для сброса пароля
   * @example email@mail.ru
   */
  email: string;
}

export interface ResponseSendCodeResetPassword {
  /**
   * время жизни кода (в секундах).
   * @example 120
   */
  lifetime: number;
}

/**
 * ответ
 */
export interface ResponseCheckEmail {
  /**
   * Если email свободен - true
   * иначе false
   *
   * @example true
   */
  is_free?: boolean;
}

/**
 * информация о пользователе
 */
export interface UserInfo {
  /**
   * Фамилия, Имя и Отчество клиента
   * @example Иванов Иван Иванович
   */
  fio?: string;

  /**
   * email клиента
   * @example test@example.com
   */
  email?: string;

  /**
   * телефон клиента
   * @example 79896325410
   */
  phone?: string;
}

export interface RequestUpdatePassword {
  /**
   * email, на который отправлялся код для сброса пароля
   * @example email@mail.ru
   */
  email: string;

  /**
   * новый пароль
   * @example drowssap
   */
  password: string;

  /**
   * проверочный код
   * @example 1111
   */
  code: string;
}

export interface RegisterDataRequestEmail {
  /** email для регистрации */
  email: string;

  /** Пароль для регистрации */
  password: string;

  /** Код из письма */
  code: string;
}

export interface RegisterDataRequestPhone {
  /**
   * номер телефона
   * @example +7(999) 999-99-99
   */
  phone: string;

  /**
   * Код из смс
   * @example 1111
   */
  code: string;
}

export interface LoginEmailDataRequest {
  /**
   * имя пользователя (логин) используемый для аутентификации
   *
   * @example emai@mail.ru
   */
  email: string;

  /**
   * пароль пользователя
   * @example drowssap
   */
  password: string;
}

export interface LoginPhoneDataRequest {
  /**
   * телефон, используемый для аутентификации
   * @example +7(999) 999-99-99
   */
  phone: string;

  /**
   * код из СМС
   * @example 1111
   */
  code: string;
}

export interface TokenResponse {
  /** Сообщение с доп информацией */
  message?: string;

  /** токен выдаваемый пользователю */
  access_token?: string;

  /** токен выдаваемый пользователю */
  refresh_token?: string;
  info?: { cart?: string; fio?: string; is_admin?: boolean };
}

export interface V1AuthRefreshCreatePayload {
  /** Токен для обновления основного токена */
  refresh_token?: string;
}

export interface V1AuthEmailCheckListParams {
  /** Email который проверяется */
  email: string;
}
