/* eslint-disable */
/* tslint:disable */
/*
 * ---------------------------------------------------------------
 * ## THIS FILE WAS GENERATED VIA SWAGGER-TYPESCRIPT-API        ##
 * ##                                                           ##
 * ## AUTHOR: acacode                                           ##
 * ## SOURCE: https://github.com/acacode/swagger-typescript-api ##
 * ---------------------------------------------------------------
 */

export interface DeliveryMethod {
  /**
   * заголовок
   * @example Срочная доставка в пределах города
   */
  name?: string;

  /**
   * описание
   * @example Срочная доставка в пределах города
   */
  description?: string;

  /**
   * очередность способа
   * @example 1
   */
  weight?: number;

  /**
   * значение для поля стоимости
   * @example от 1500
   */
  cost?: string;

  /**
   * использовать ли поле для ввода адреса
   * @example true
   */
  use_address?: boolean;

  /**
   * указывать значек быстрой доставки
   * @example true
   */
  is_fast?: boolean;
}

export type FrontDeliveryMethod = { alias?: string; region?: string } & DeliveryMethod;

export interface AdminDeliveryMethodList {
  /**
   * уникальный идентификатор метода.
   * @example express
   */
  alias?: string;

  /**
   * заголовок
   * @example Срочная доставка в пределах города
   */
  name?: string;

  /**
   * порядок вывода способа
   * @example 1
   */
  weight?: number;

  /**
   * доступен ли способ для использования
   * @example true
   */
  is_active?: boolean;
}

export type ResponseLimit = { id?: number } & Limit;

export interface Limit {
  /**
   * мин. стоимость заказа для применения ограничения в копейках
   * @example 10000
   */
  order_min_cost?: number;

  /**
   * стоимость доставки в копейках
   * @example 1000
   */
  shipping_cost: number;

  /**
   * регион применения ограничения
   * @example г Волгоград
   */
  region?: string;
}

export interface V1CheckoutShippingMethodsListParams {
  /**
   * Список регионов, разделенных запятой
   * @example г Волгоград,Волгоградская обл
   */
  regions: string;
}

export type V1AdminShippingMethodAddCreatePayload = DeliveryMethod & { alias?: string };
