/* eslint-disable */
/* tslint:disable */
/*
 * ---------------------------------------------------------------
 * ## THIS FILE WAS GENERATED VIA SWAGGER-TYPESCRIPT-API        ##
 * ##                                                           ##
 * ## AUTHOR: acacode                                           ##
 * ## SOURCE: https://github.com/acacode/swagger-typescript-api ##
 * ---------------------------------------------------------------
 */

export interface Settings {
  /**
   * часы и минуты, при заказе до которых в этот день, доставка будет в этот же день (при заказе на основном складе) или на X дней позже (при заказе на остальных складах)
   * @example 11:00
   */
  delivery_fast_time?: string;

  /**
   * количество дней, на которые смещается доставка при заказе не с основного склада
   * @example 1
   */
  delivery_shift?: number;

  /** праздничные дни */
  holidays?: string[];

  /**
   * минимальное значение остатков для указания пометки "много"
   * @example 100
   */
  min_many_quantity?: number;

  /**
   * чат для вайбера
   * @example 79996665544
   */
  viber?: string;

  /**
   * чат для whatsapp
   * @example 79996665544
   */
  whatsApp?: string;

  /**
   * чат для telegram
   * @example groster
   */
  telegram?: string;
}
