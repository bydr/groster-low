/* eslint-disable */
/* tslint:disable */
/*
 * ---------------------------------------------------------------
 * ## THIS FILE WAS GENERATED VIA SWAGGER-TYPESCRIPT-API        ##
 * ##                                                           ##
 * ## AUTHOR: acacode                                           ##
 * ## SOURCE: https://github.com/acacode/swagger-typescript-api ##
 * ---------------------------------------------------------------
 */

export type ShopListResponse = ({ uuid?: string } & Shop)[];

export type ShopResponse = Shop & {
  email?: string;
  description?: string;
  image?: string;
};

export type ShopAdmin = Shop & {
  email?: string;
  description?: string;
  image?: string;
  is_active?: boolean;
  name?: string;
};

export interface Shop {
  /**
   * адрес
   * @example 400048, Волгоградская обл, Волгоград г, Авиаторов ш, дом 18
   */
  address?: string;

  name?: string;

  /**
   * график работы
   * @example Пн-Вс 09:00-18:00
   */
  schedule?: string;

  /**
   * контактный телефон
   * @example +79888888888
   */
  phone?: string;

  /** координаты */
  coordinates?: { latitude?: string; longitude?: string };
}
