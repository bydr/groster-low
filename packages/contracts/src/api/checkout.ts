/* eslint-disable */
/* tslint:disable */
/*
 * ---------------------------------------------------------------
 * ## THIS FILE WAS GENERATED VIA SWAGGER-TYPESCRIPT-API        ##
 * ##                                                           ##
 * ## AUTHOR: acacode                                           ##
 * ## SOURCE: https://github.com/acacode/swagger-typescript-api ##
 * ---------------------------------------------------------------
 */

export interface ResponseCustomerDataList {
  /** список сфер бизнеса */
  areas?: string[];

  /** список email, на которые делались заказы */
  emails?: string[];

  /** список телефонов, на которые делались заказы */
  phones?: string[];
}

export interface ReplacementMethod {
  /**
   * заголовок
   * @example Не звонить мне
   */
  title: string;

  /**
   * описание
   * @example Убрать отсутствующий товар
   */
  description?: string;

  /**
   * порядок вывода метода. Чем меньше цифра, тем выше в списке
   * @example 1
   */
  weight: number;
}

export interface RequestShippingCost {
  /**
   * идентификатор корзины
   * @example
   */
  cart: string;

  /**
   * идентификатор способа доставки
   * @example express
   */
  shipping_method: string;

  /**
   * населенный пункт
   * @example г Волгоград
   */
  region?: string;

  /**
   * дата доставки. Влияет на стоимость при условии наличия другого заказа
   * @example 2022-01-01T11:20:33+0300
   */
  shipping_date?: string;
}

export interface RequestOrderSave {
  /**
   * имя в заказе
   * @example Иванов И.И.
   */
  fio: string;

  /**
   * email заказа
   * @example test@gmail.com
   */
  email: string;

  /**
   * контактный телефон
   * @example 79899632541
   */
  phone: string;

  /**
   * сфера деятельности
   * @example Магазины
   */
  bussiness_area?: string;

  /**
   * идентификатор способа доставки
   * @example express
   */
  shipping_method: string;

  /**
   * желаемая дата доставки в формате ISO-8601
   * @example 2022-01-01T11:20:33+0300
   */
  shipping_date?: string;

  /**
   * идентификатор плательщика
   * @example payer4b340550242239.64159797
   */
  payer?: string;

  /**
   * идентификатор адреса доставки
   * @example address4b340550242239.64159797
   */
  shipping_address?: string;

  /**
   * идентификатор способа оплаты
   * @example gpay123456
   */
  payment_method?: string;

  /**
   * населенный пункт
   * @example г Волгоград
   */
  region: string;

  /**
   * способ замены товара
   * @example 1
   */
  replacement?: number;

  /**
   * комментарий к заказу
   * @example тестовый заказ
   */
  comment?: string;

  /**
   * подтверждаю заказ. Не звонить
   * @example true
   */
  not_call?: boolean;
}

export interface PaymentMethod {
  /**
   * уникальный идентификатор метода.
   * @example gpay00000000
   */
  uid?: string;

  /**
   * заголовок
   * @example Оплата картой
   */
  name?: string;

  /**
   * очередность способа
   * @example 1
   */
  weight?: number;
}
