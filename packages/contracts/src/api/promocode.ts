/* eslint-disable */
/* tslint:disable */
/*
 * ---------------------------------------------------------------
 * ## THIS FILE WAS GENERATED VIA SWAGGER-TYPESCRIPT-API        ##
 * ##                                                           ##
 * ## AUTHOR: acacode                                           ##
 * ## SOURCE: https://github.com/acacode/swagger-typescript-api ##
 * ---------------------------------------------------------------
 */

export interface CodeUseResponse {
  /**
   * итоговый размер скидки в копейках
   * @example 12000
   */
  discount?: number;

  /**
   * сумма к оплате в копейках
   * @example 10240
   */
  total_cost?: number;
}

export interface CodeRemoveResponse {
  /**
   * сумма к оплате в копейках
   * @example 10240
   */
  total_cost?: number;
}

export type CodeRequest = Code & { categories?: string };

export type CodeResponse = Code & { categories?: string[] };

export interface Code {
  /** @example 1 */
  id?: number;

  /** @example new year 2022 */
  name?: string;

  /** @example newYear2022 */
  code?: string;

  /**
   * тип скидки <br> * `1` - в процентах<br> * `2` - в рублях
   *
   * @example 1
   */
  type?: 1 | 2;

  /**
   * размер скидки (для типа 1 - в копейках, для типа 2 - в процентах)
   * @example 10
   */
  value?: number;

  /**
   * многоразовый
   * @example true
   */
  reusable?: boolean;

  /**
   * время начала действия промокода в формате ISO-8601
   * @example 2022-01-01T11:20:33+0300
   */
  start?: string;

  /**
   * время окончания действия промокода в формате ISO-8601
   * @example 2022-01-26T11:20:33+0300
   */
  finish?: string;
}

export interface V1PromocodeUpdatePayload {
  /**
   * uid корзины
   * @example gcart4b340550242239.64159797
   */
  cart?: string;

  /**
   * промокод
   * @example newyear2021
   */
  code?: string;
}

export interface V1PromocodeDeletePayload {
  /**
   * uid корзины
   * @example gcart4b340550242239.64159797
   */
  cart?: string;
}
