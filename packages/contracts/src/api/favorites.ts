/* eslint-disable */
/* tslint:disable */
/*
 * ---------------------------------------------------------------
 * ## THIS FILE WAS GENERATED VIA SWAGGER-TYPESCRIPT-API        ##
 * ##                                                           ##
 * ## AUTHOR: acacode                                           ##
 * ## SOURCE: https://github.com/acacode/swagger-typescript-api ##
 * ---------------------------------------------------------------
 */

export interface V1FavoritesAddCreatePayload {
  /**
   * uuid товаров, которые добавляются в избранное, разделенные запятой
   * @example 00000000-0000-0000-0000-000000000000,00000000-0000-0000-0000-000000000001
   */
  products: string;
}

export interface V1FavoritesDeleteDeletePayload {
  /**
   * uuid товара, который удаляется из избранного
   * @example 00000000-0000-0000-0000-000000000000
   */
  product: string;
}
