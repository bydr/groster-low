/* eslint-disable */
/* tslint:disable */
/*
 * ---------------------------------------------------------------
 * ## THIS FILE WAS GENERATED VIA SWAGGER-TYPESCRIPT-API        ##
 * ##                                                           ##
 * ## AUTHOR: acacode                                           ##
 * ## SOURCE: https://github.com/acacode/swagger-typescript-api ##
 * ---------------------------------------------------------------
 */

export interface RequestUpdatePassword {
  /**
   * новый пароль
   * @example drowssap1
   */
  old_password: string;

  /**
   * новый пароль
   * @example drowssap2
   */
  new_password: string;
}

/**
 * адрес доставки
 */
export interface Address {
  /**
   * Название адреса
   * @example Новый адрес
   */
  name?: string;

  /**
   * Адрес
   * @example г. Волгоград, ул. Ленина 16
   */
  address?: string;

  /**
   * является ли адресом по умолчанию
   * @example false
   */
  is_default?: boolean;

  uid?: string;

  comment?: string;
}

/**
 * плательщик
 */
export interface Payer {
  /**
   * Название организации
   * @example ИП Яровина
   */
  name?: string;

  /**
   * ОГРНИП
   * @example 012345678901234
   */
  ogrnip?: string;

  /**
   * ИНН
   * @example 012345678901
   */
  inn?: string;

  /**
   * номер счета
   * @example 0123456789
   */
  account?: string;

  /**
   * БИК
   * @example 0123456789
   */
  bik?: string;

  /**
   * КПП
   * @example 0123456789
   */
  kpp?: string;

  /**
   * контактное лицо
   * @example Иванов И.И.
   */
  contact?: string;

  /**
   * контактный телефон
   * @example +79899632587
   */
  contact_phone?: string;

  /**
   * идентификатор плательщика
   * @example payer4b340550242239.64159797
   */
  uid?: string;

  /**
   * условное название плательщика
   * @example ИП Яровина, ИНН 012345678901234
   */
  title?: string;
}

export interface ResponseOrderListOrders {
  /**
   * общее количество заказов для пагинации
   * @example 15
   */
  total?: number;

  /** список заказов для текущей страницы */
  orders?: OrderList[];
}

export interface OrderList {
  /**
   * идентификатор
   * @example gcart4b340550242239.64159797
   */
  uid?: string;

  /**
   * дата создания в формате ISO-8601
   * @example 2022-01-01T11:20:33+0300
   */
  create_at?: string;

  /**
   * сумма заказа в копейках
   * @example 250000
   */
  total_cost?: number;

  /**
   * статусы заказа <br> * `2` - новый<br> * `3` - завешен<br> * `4` - отменен<br> * `5` - в обработке
   * @example 3
   */
  state?: number;

  /**
   * номер заказа
   * @example 1
   */
  number?: string;

  /**
   * периодичность уведомлений
   * @example 1
   */
  subscribe_interval?: number;
}

export interface Order {
  /**
   * номер заказа
   * @example 1
   */
  number?: string;

  /**
   * дата создания в формате ISO-8601
   * @example 2022-01-01T11:20:33+0300
   */
  create_at?: string;

  /**
   * статусы заказа <br> * `2` - новый<br> * `3` - завешен<br> * `4` - отменен<br> * `5` - в обработке
   * @example 3
   */
  state?: number;

  /**
   * имя в заказе
   * @example Иванов И.И.
   */
  fio?: string;

  /**
   * email заказа
   * @example test@gmail.com
   */
  email?: string;

  /**
   * контактный телефон
   * @example 79899632541
   */
  phone?: string;

  /**
   * сфера деятельности
   * @example Магазины
   */
  bussiness_area?: string;

  /**
   * способ доставки
   * @example Самовывод
   */
  shipping_method?: string;

  /**
   * желаемая дата доставки в формате ISO-8601
   * @example 2022-01-01T11:20:33+0300
   */
  shipping_date?: string;

  /**
   * идентификатор плательщика
   * @example payer4b340550242239.64159797
   */
  payer?: string;

  /**
   * идентификатор адреса доставки
   * @example address4b340550242239.64159797
   */
  shipping_address?: string;

  /**
   * сумма скидки в копейках
   * @example 2500
   */
  discount?: number;

  /**
   * стоимость доставки в копейках
   * @example 10000
   */
  shipping_cost?: number;

  /**
   * стоимость заказа в копейках
   * @example 90000
   */
  total_cost?: number;

  /** список товаров в заказе */
  products?: {
    uuid?: string;
    quantity?: number;
    sample?: number;
    total_cost?: number;
  }[];

  /**
   * периодичность уведомлений
   * @example 1
   */
  subscribe_interval?: number;
}

export interface V1AccountOrderListOrdersListParams {
  /**
   * статусы заказа <br> * `2` - новый<br> * `3` - завешен<br> * `4` - отменен<br> * `5` - в обработке
   * @example 3
   */
  state?: number;

  /**
   * фильтрация по плательщику (список идентификаторов через запятую)
   * @example payer090000000000.64159797,payer4b340550242239.64159797
   */
  payer?: string;

  /**
   * фильтрация по году
   * @example 2022
   */
  year?: number;

  /**
   * страница пагинации
   * @example 1
   */
  page?: number;

  /** количество товаров в ответе (макс 100) */
  per_page?: number;
}

export interface V1AccountOrderListProductsListParams {
  /**
   * статусы заказа <br> * `3` - активен<br> * `4` - завешен<br> * `5` - отменен
   * @example 3
   */
  state?: number;

  /**
   * фильтрация по плательщику (список идентификаторов через запятую)
   * @example payer090000000000.64159797,payer4b340550242239.64159797
   */
  payer?: string;

  /**
   * фильтрация по году
   * @example 2022
   */
  year?: number;

  /**
   * страница пагинации
   * @example 1
   */
  page?: number;

  /** количество товаров в ответе (макс 100) */
  per_page?: number;

  /**
   * товары указанной категории будут в начале списка
   * @example 00000000-0000-0000-0000-000000000000
   */
  category?: string;
}

export interface V1AccountOrdersSubscribeUpdatePayload {
  /**
   * периодичность уведомлений <br> * `1` - каждый день<br> * `2` - каждые 2 недели<br> * `3` - 1-е число каждгого месяца
   *
   * @example 1
   */
  interval: 1 | 2 | 3;
}

export interface V1AccountWaitingProductsDeletePayload {
  /**
   * идентификатор товара
   * @example 00000000-0000-0000-0000-000000000000
   */
  product?: string;
}
