import {
  AdminDeliveryMethodList as AdminDeliveryMethodListInit,
  DeliveryMethod,
} from "./src/api/shippingMethods";

export type {
  UserInfo,
  TokenResponse,
  LoginEmailDataRequest,
  LoginPhoneDataRequest,
  RegisterDataRequestEmail,
  RegisterDataRequestPhone,
  RequestResetPassword,
  ResponseCheckEmail,
  ResponseSendCode,
  ResponseSendCodeResetPassword,
  V1AuthEmailCheckListParams,
  V1AuthRefreshCreatePayload,
  RequestSendCode,
  RequestUpdatePassword as AuthRequestUpdatePassword,
} from "./src/api/auth";
export type {
  SortTypeResponse,
  TagResponse,
  Tag,
  CategoryByArea,
  CategoryByAreaResponse,
  Param,
  ParamsResponse,
  CategoryResponse,
  CatalogResponse,
  AdminCategory,
  Category,
  V1AdminCategoryUpdatePayload,
} from "./src/api/catalog";
import { Product as ProductCatalog } from "./src/api/catalog";
export type {
  Product as ProductSpecificationType,
  ChangeQtyRequest,
  CartResponse,
  DeleteProductRequest,
  FastOrderRequest,
} from "./src/api/cart";
export type {
  ProductListResponse as ProductDetailListType,
  V1ProductListListParams,
  ProductDetailResponse,
  Product as ProductDetail,
} from "./src/api/product";

export type ProductCatalogType = ProductCatalog &
  Pick<ProductDetailResponse, "analogs" | "companions">;

export * from "./src/api/favorites";
export * from "./src/api/promocode";
export * from "./src/api/checkout";
export type {
  Order as OrderType,
  Address as AddressType,
  Payer,
  OrderList,
  V1AccountOrderListOrdersListParams,
  V1AccountOrderListProductsListParams,
  ResponseOrderListOrders,
  RequestUpdatePassword as AccountRequestUpdatePassword,
} from "./src/api/account";
export type { ShopAdmin, Shop, ShopListResponse } from "./src/api/shop";
import { ShopResponse as ShopResponseInit } from "./src/api/shop";
export type ShopResponse = ShopResponseInit & { name?: string };

export * from "./src/api/shippingMethods";
export type { Settings as SettingsFetchedType } from "./src/api/settings";
export type {
  Banner as BannerApiType,
  AdminResponseBanners,
  RequestBanner,
} from "./src/api/banners";
export type ApiError = {
  code: string;
  message: string;
  status?: number;
};
import type {
  AdminPaymentMethod,
  PaymentMethod,
} from "./src/api/paymentMethods";
export type PaymentMethodCreateRequest = Omit<PaymentMethod, "uid"> &
  Pick<AdminPaymentMethod, "is_active">;
export type ShippingMethodDetail = DeliveryMethod &
  Pick<AdminDeliveryMethodListInit, "is_active"> & { alias?: string };
export type ShippingMethodCreateRequest = ShippingMethodDetail & {
  alias: string;
};
export type ShippingMethodEditRequest = ShippingMethodDetail & {
  alias: string;
};
export type AdminDeliveryMethodList = AdminDeliveryMethodListInit &
  Pick<DeliveryMethod, "cost">;
export * from "./src/api/feedback";
import { Question } from "./src/api/feedback";
import { ProductDetailResponse } from "./src/api/product";
export type QuestionType = Question & { block?: number; id: number };
export type ResponseAdminFaq = QuestionType[];
