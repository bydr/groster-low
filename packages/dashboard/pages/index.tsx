import { NextPageWithRequiredAuth } from "./_app"
import { Empty } from "../layouts/Empty/Empty"
import { Auth } from "../components/Auth/Auth"

const Home: NextPageWithRequiredAuth = () => {
  return (
    <>
      <Auth />
    </>
  )
}

Home.requireAuth = true
Home.getLayout = (page) => {
  return <Empty>{page}</Empty>
}

export default Home
