import { catalogAPI } from "../../../api/catalogAPI"
import { NextPageWithRequiredAuth } from "../../_app"
import React from "react"
import { Default } from "../../../layouts/Default/Default"
import { Tag } from "../../../../contracts/contracts"
import { GetServerSidePropsContext } from "next"
import { TagForm } from "../../../components/Tag/Form"
import { ROUTES } from "../../../utils/constants"

type ServerSidePropsType = {
  tag: (Tag & { id: string }) | null
}

export const getServerSideProps = async ({
  params,
}: GetServerSidePropsContext): Promise<{
  props: ServerSidePropsType
}> => {
  const resultTag = await catalogAPI
    .getTagById({
      id: params?.id as string,
      server: true,
    })
    .catch((err) => {
      console.log("err ", err)
    })
  return {
    props: {
      tag: !!resultTag ? { ...resultTag, id: params?.id as string } : null,
    },
  }
}

const TagDetailPage: NextPageWithRequiredAuth = (props): JSX.Element => {
  const { tag } = props as ServerSidePropsType
  return (
    <>
      <TagForm tag={tag || undefined} />
    </>
  )
}

TagDetailPage.requireAuth = true
TagDetailPage.getLayout = (page) => {
  return (
    <Default
      pageTitle={page.props.tag.name || "Тег"}
      backRoute={`${ROUTES.tags}`}
    >
      {page}
    </Default>
  )
}

export default TagDetailPage
