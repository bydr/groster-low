import React from 'react'
import { TagForm } from '../../../components/Tag/Form'
import { Default } from '../../../layouts/Default/Default'
import { ROUTES } from '../../../utils/constants'
import { NextPageWithRequiredAuth } from '../../_app'

const TagCreatePage: NextPageWithRequiredAuth = (): JSX.Element => {
  return (
    <>
      <TagForm />
    </>
  )
}

TagCreatePage.requireAuth = true
TagCreatePage.getLayout = (page) => {
  return (
    <Default
      pageTitle={"Создание тега"}
      backRoute={`${ROUTES.tags}`}
    >
      {page}
    </Default>
  )
}

export default TagCreatePage
