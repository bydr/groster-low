import { NextPageWithRequiredAuth } from "../../_app"
import { Default } from "../../../layouts/Default/Default"
import React from "react"
import { catalogAPI } from "../../../api/catalogAPI"
import { TagResponse } from "../../../../contracts/contracts"
import { Button } from "antd"
import { ROUTES } from "../../../utils/constants"
import { TagsList } from "../../../components/Tag/List"

type ServerSidePropsType = {
  tags: TagResponse
}

export const getServerSideProps = async (): Promise<{
  props: ServerSidePropsType
}> => {
  const resultTags = await catalogAPI
    .getTags({
      server: true,
    })
    .catch((err) => {
      console.log("err ", err)
    })

  return {
    props: {
      tags: resultTags || [],
    },
  }
}

const TagsPage: NextPageWithRequiredAuth = (props): JSX.Element => {
  const { tags } = props as ServerSidePropsType

  return (
    <>
      <Button type={"primary"} href={`${ROUTES.base}${ROUTES.tags}/create`}>
        Создать новый
      </Button>
      <TagsList tags={tags} />
    </>
  )
}

TagsPage.requireAuth = true
TagsPage.getLayout = (page) => {
  return <Default pageTitle={"Тэги категорий"}>{page}</Default>
}

export default TagsPage
