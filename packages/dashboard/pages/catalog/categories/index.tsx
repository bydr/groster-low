import { useEffect } from "react"
import { NextPageWithRequiredAuth } from "../../_app"
import { Default } from "../../../layouts/Default/Default"
import { catalogAPI } from "../../../api/catalogAPI"
import { CategoryResponse } from "../../../../contracts/contracts"
import { CategoryList } from "../../../components/Category/List"
import { useAppDispatch } from "../../../hooks/redux"
import { catalogSlice } from "../../../store/reducers/catalogSlice"

type ServerSidePropsType = Required<Pick<CategoryResponse, "categories">>

export const getServerSideProps = async (): Promise<{
  props: ServerSidePropsType
}> => {
  const resultCategories = await catalogAPI
    .getCategories({
      server: true,
    })
    .catch((err) => {
      console.log("err ", err)
    })
  return {
    props: {
      categories: !!resultCategories ? resultCategories.categories || [] : [],
      // categories: resultCategories?. || [],
    },
  }
}

const CatalogSettingsPage: NextPageWithRequiredAuth = (props): JSX.Element => {
  const { categories } = props as ServerSidePropsType
  const dispatch = useAppDispatch()
  const { setCategories } = catalogSlice.actions

  useEffect(() => {
    dispatch(setCategories(categories || []))
  }, [categories, dispatch, setCategories])

  return (
    <>
      <CategoryList />
    </>
  )
}

CatalogSettingsPage.requireAuth = true
CatalogSettingsPage.getLayout = (page) => {
  return <Default pageTitle={"Категории"}>{page}</Default>
}

export default CatalogSettingsPage
