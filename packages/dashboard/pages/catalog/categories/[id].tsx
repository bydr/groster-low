import { catalogAPI } from "../../../api/catalogAPI"
import { NextPageWithRequiredAuth } from "../../_app"
import { useEffect } from "react"
import { Default } from "../../../layouts/Default/Default"
import { GetServerSidePropsContext } from "next"
import { ROUTES } from "../../../utils/constants"
import { useQuery } from "react-query"
import { Skeleton } from "antd"
import { useApp } from "../../../hooks/app"
import { CategoryForm } from "../../../components/Category/Form"

type ServerSidePropsType = {
  id?: string
}

export const getServerSideProps = ({
  params,
}: GetServerSidePropsContext): {
  props: ServerSidePropsType
} => {
  return {
    props: {
      id: params?.id as string | undefined,
    },
  }
}

const CategoryDetailPage: NextPageWithRequiredAuth = (props): JSX.Element => {
  const { id } = props as ServerSidePropsType
  const { setPageTitle } = useApp()
  const { data: dataCategory, isLoading } = useQuery(["category", id], () =>
    id !== undefined ? catalogAPI.getCategoryById({ id }) : null,
  )

  useEffect(() => {
    setPageTitle(!!dataCategory?.name ? dataCategory?.name : "Категория")
    return () => {
      setPageTitle(null)
    }
  }, [dataCategory, setPageTitle])

  return (
    <>
      <Skeleton title active avatar loading={isLoading}>
        {!!dataCategory && (
          <>
            <CategoryForm category={dataCategory} />
          </>
        )}
      </Skeleton>
    </>
  )
}

CategoryDetailPage.requireAuth = true
CategoryDetailPage.getLayout = (page) => {
  return <Default backRoute={`${ROUTES.categories}`}>{page}</Default>
}

export default CategoryDetailPage
