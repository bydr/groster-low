import { GetServerSidePropsContext } from "next"
import { useQuery } from "react-query"
import { useEffect, useState } from "react"
import { Empty, Skeleton } from "antd"
import { NextPageWithRequiredAuth } from "../_app"
import { useApp } from "../../hooks/app"
import { shopsAPI } from "../../api/shopsAPI"
import { Default } from "../../layouts/Default/Default"
import { ROUTES } from "../../utils/constants"
import { ShopForm } from "../../components/Shop/Form"
import { ApiError } from "../../../contracts/contracts"
import { EntityImage } from "../../components/EntityImage/EntityImage"
import { ShopImage } from "../../components/Shop/StyledShop"
import { useAppDispatch, useAppSelector } from "../../hooks/redux"
import { shopsSlice } from "../../store/reducers/shopsSlice"

type ServerSidePropsType = {
  id?: string
}

export const getServerSideProps = ({
  params,
}: GetServerSidePropsContext): {
  props: ServerSidePropsType
} => {
  return {
    props: {
      id: params?.id as string | undefined,
    },
  }
}

const ShopDetailPage: NextPageWithRequiredAuth = (props): JSX.Element => {
  const { id } = props as ServerSidePropsType
  const dispatch = useAppDispatch()
  const { setCurrentShop } = shopsSlice.actions
  const { setPageTitle } = useApp()
  const currentShop = useAppSelector((state) => state.shop.currentShop)
  const [errorMessage, setErrorMessage] = useState<string | undefined>()
  const { data: dataShop, isLoading } = useQuery(
    ["category", id],
    () => (id !== undefined ? shopsAPI.getShopById({ uuid: id }) : null),
    {
      onError: (error: ApiError) => {
        setErrorMessage(error.message || "Произошла ошибка")
      },
      onSuccess: (response) => {
        setErrorMessage(undefined)
        if (!!response) {
          dispatch(setCurrentShop({ ...response, uuid: id }))
        } else {
          dispatch(setCurrentShop(null))
        }
      },
    },
  )

  useEffect(() => {
    setPageTitle(dataShop?.name || dataShop?.address || "Магазин")
    return () => {
      setPageTitle(null)
    }
  }, [dataShop?.address, dataShop?.name, setPageTitle])

  useEffect(() => {
    return () => {
      dispatch(setCurrentShop(null))
    }
  }, [dispatch, setCurrentShop])

  return (
    <>
      <Skeleton title active avatar loading={isLoading}>
        {!!currentShop && (
          <>
            {!!currentShop.image && (
              <>
                <ShopImage>
                  <EntityImage
                    width={200}
                    height={200}
                    imagePath={currentShop.image}
                  />
                </ShopImage>
              </>
            )}
            <ShopForm shop={currentShop || undefined} />
          </>
        )}
        {errorMessage !== undefined && (
          <Empty
            image={Empty.PRESENTED_IMAGE_SIMPLE}
            description={errorMessage}
          />
        )}
      </Skeleton>
    </>
  )
}

ShopDetailPage.requireAuth = true
ShopDetailPage.getLayout = (page) => {
  return <Default backRoute={`${ROUTES.shops}`}>{page}</Default>
}

export default ShopDetailPage
