import { NextPageWithRequiredAuth } from "../_app"
import { Default } from "../../layouts/Default/Default"
import React from "react"
import { ShopList } from "../../components/Shop/List"
import { useAppDispatch } from "../../hooks/redux"
import { shopsSlice } from "../../store/reducers/shopsSlice"
import { useQuery } from "react-query"
import { shopsAPI } from "../../api/shopsAPI"

const ShopsSettingsPage: NextPageWithRequiredAuth = (): JSX.Element => {
  const dispatch = useAppDispatch()
  const { setShops } = shopsSlice.actions
  const { isLoading } = useQuery("shops", () => shopsAPI.getAdminShops(), {
    onSuccess: (response) => {
      dispatch(setShops(response || []))
    },
  })

  return (
    <>
      <ShopList loading={isLoading} />
    </>
  )
}

ShopsSettingsPage.requireAuth = true
ShopsSettingsPage.getLayout = (page) => {
  return <Default pageTitle={"Магазины"}>{page}</Default>
}

export default ShopsSettingsPage
