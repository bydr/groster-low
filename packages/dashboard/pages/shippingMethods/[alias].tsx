import { GetServerSidePropsContext } from "next"
import { NextPageWithRequiredAuth } from "../_app"
import React from "react"
import { Default } from "../../layouts/Default/Default"
import { ROUTES } from "../../utils/constants"
import { ShippingMethod } from "../../components/ShippingMethod"

type ServerSidePropsType = {
  alias?: string
}

export const getServerSideProps = ({
  params,
}: GetServerSidePropsContext): {
  props: ServerSidePropsType
} => {
  return {
    props: {
      alias: params?.alias as string | undefined,
    },
  }
}

const ShippingMethodDetailPage: NextPageWithRequiredAuth = (
  props,
): JSX.Element => {
  const { alias } = props as ServerSidePropsType

  return (
    <>
      <ShippingMethod alias={alias} />
    </>
  )
}

ShippingMethodDetailPage.requireAuth = true
ShippingMethodDetailPage.getLayout = (page) => {
  return <Default backRoute={`${ROUTES.shippingMethods}`}>{page}</Default>
}

export default ShippingMethodDetailPage
