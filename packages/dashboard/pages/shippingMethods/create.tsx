import { NextPageWithRequiredAuth } from "../_app"
import { Default } from "../../layouts/Default/Default"
import { ROUTES } from "../../utils/constants"
import { ShippingMethodForm } from "../../components/ShippingMethod/Form"

const ShippingMethodCreatePage: NextPageWithRequiredAuth = (): JSX.Element => {
  return (
    <>
      <ShippingMethodForm />
    </>
  )
}

ShippingMethodCreatePage.requireAuth = true
ShippingMethodCreatePage.getLayout = (page) => {
  return (
    <Default
      pageTitle={"Создание метода доставки"}
      backRoute={`${ROUTES.shippingMethods}`}
    >
      {page}
    </Default>
  )
}

export default ShippingMethodCreatePage
