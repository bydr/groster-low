import React from "react"
import { NextPageWithRequiredAuth } from "../_app"
import { Default } from "../../layouts/Default/Default"
import { useAppDispatch } from "../../hooks/redux"
import { useQuery } from "react-query"
import { shippingMethodsAPI } from "../../api/shippingMethodsAPI"
import { shippingMethodsSlice } from "../../store/reducers/shippingMethodsSlice"
import { Button } from "antd"
import { ROUTES } from "../../utils/constants"
import { ShippingMethodList } from "../../components/ShippingMethod/List"

const ShippingMethodsPage: NextPageWithRequiredAuth = () => {
  const dispatch = useAppDispatch()
  const { setMethods } = shippingMethodsSlice.actions
  const { isLoading } = useQuery(
    "shippingMethods",
    () => shippingMethodsAPI.getShippingMethods(),
    {
      onSuccess: (response) => {
        dispatch(setMethods(response || null))
      },
    },
  )

  return (
    <>
      <Button
        type={"primary"}
        href={`${ROUTES.base}${ROUTES.shippingMethods}/create`}
      >
        Создать новый
      </Button>
      <ShippingMethodList loading={isLoading} />
    </>
  )
}

ShippingMethodsPage.requireAuth = true
ShippingMethodsPage.getLayout = (page) => {
  return <Default pageTitle={"Методы доставки"}>{page}</Default>
}

export default ShippingMethodsPage
