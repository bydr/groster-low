import { NextPageWithRequiredAuth } from "./_app"
import { Default } from "../layouts/Default/Default"
import React from "react"
import { Settings } from "../components/Settings"

const GeneralSettingsPage: NextPageWithRequiredAuth = (): JSX.Element => {
  return (
    <>
      <Settings />
    </>
  )
}

GeneralSettingsPage.requireAuth = true
GeneralSettingsPage.getLayout = (page) => {
  return <Default pageTitle={"Общие настройки"}>{page}</Default>
}

export default GeneralSettingsPage
