import globals from "../styles/globals"
import "../styles/antd.less" // Add this line

import type { AppProps } from "next/app"
import { Hydrate, QueryClient, QueryClientProvider } from "react-query"
import { ReactElement, ReactNode, useState } from "react"
import { NextPage } from "next"
import { AuthGuard } from "../hoc/AuthGuard"
import { Provider } from "react-redux"
import { Default } from "../layouts/Default/Default"
import { store } from "../store/store"
import { Provider as AuthProvider } from "../hooks/auth"
import { Provider as AppProvider } from "../hooks/app"
import Head from "next/head"

export type RequiredAuth = { requireAuth: boolean }

export type NextPageWithLayout = NextPage & {
  getLayout?: (page: ReactElement) => ReactNode
}

export type NextPageWithRequiredAuth = NextPageWithLayout & RequiredAuth

type AppPropsWithLayout = AppProps & {
  Component: NextPageWithRequiredAuth
}

type PagePropsType = {
  dehydratedState: unknown
}

function MyApp({ Component, pageProps }: AppPropsWithLayout): JSX.Element {
  const withLayout =
    Component.getLayout || ((page) => <Default>{page}</Default>)

  const [queryClient] = useState(
    () =>
      new QueryClient({
        defaultOptions: {
          queries: {
            refetchInterval: false,
            refetchOnWindowFocus: false,
            refetchIntervalInBackground: false,
            retry: 3,
            retryDelay: 100,
          },
          mutations: {
            retry: 3,
            retryDelay: 100,
          },
        },
      }),
  )

  return (
    <>
      <Head>
        <title>Админ панель - Гростер</title>
        <meta httpEquiv="X-UA-Compatible" content="IE=edge" />
        <meta
          name="viewport"
          content="width=device-width, initial-scale=1, maximum-scale=5"
        />
      </Head>
      <Provider store={store}>
        <QueryClientProvider client={queryClient}>
          <Hydrate state={(pageProps as PagePropsType)?.dehydratedState}>
            <AppProvider>
              <AuthProvider>
                {Component.requireAuth ? (
                  <>
                    <AuthGuard>
                      {withLayout(
                        <Component {...pageProps} className={globals} />,
                      )}
                    </AuthGuard>
                  </>
                ) : (
                  <>
                    {withLayout(
                      <Component {...pageProps} className={globals} />,
                    )}
                  </>
                )}
              </AuthProvider>
            </AppProvider>
          </Hydrate>
        </QueryClientProvider>
      </Provider>
    </>
  )
}
export default MyApp
