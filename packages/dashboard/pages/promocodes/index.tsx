import { NextPageWithRequiredAuth } from "../_app"
import { useAppDispatch } from "../../hooks/redux"
import { useQuery } from "react-query"
import { Default } from "../../layouts/Default/Default"
import React from "react"
import { promocodesSlice } from "../../store/reducers/promocodeSlice"
import { promocodeAPI } from "../../api/promocodeAPI"
import { PromocodeList } from "../../components/Promocode/List"

const PromocodesPage: NextPageWithRequiredAuth = () => {
  const dispatch = useAppDispatch()
  const { setPromocodes } = promocodesSlice.actions
  const { isLoading } = useQuery(
    "promocodes",
    () => promocodeAPI.getPromocodes(),
    {
      onSuccess: (response) => {
        dispatch(setPromocodes(response || null))
      },
    },
  )

  return (
    <>
      <PromocodeList loading={isLoading} />
    </>
  )
}

PromocodesPage.requireAuth = true
PromocodesPage.getLayout = (page) => {
  return <Default pageTitle={"Промокоды"}>{page}</Default>
}

export default PromocodesPage
