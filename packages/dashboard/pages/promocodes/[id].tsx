import { useEffect, useState } from "react"
import { NextPageWithRequiredAuth } from "../_app"
import { Default } from "../../layouts/Default/Default"
import { ROUTES } from "../../utils/constants"
import { Empty, Skeleton } from "antd"
import { useQuery } from "react-query"
import { promocodeAPI } from "../../api/promocodeAPI"
import { GetServerSidePropsContext } from "next"
import { CodeResponse } from "../../../contracts/contracts"
import { useApp } from "../../hooks/app"
import { PromocodeForm } from "../../components/Promocode/Form"

type ServerSidePropsType = {
  id?: string
}

export const getServerSideProps = ({
  params,
}: GetServerSidePropsContext): {
  props: ServerSidePropsType
} => {
  return {
    props: {
      id: params?.id as string | undefined,
    },
  }
}

const PromocodeDetailPage: NextPageWithRequiredAuth = (props): JSX.Element => {
  const { id } = props as ServerSidePropsType
  const [currentPromocode, setCurrentPromocode] =
    useState<null | CodeResponse>()
  const { setPageTitle } = useApp()

  const { isLoading } = useQuery(
    ["promocode", id],
    () =>
      id !== undefined
        ? promocodeAPI.getPromocodeById({
            id: +id,
          })
        : null,
    {
      onSuccess: (response) => {
        setCurrentPromocode({ ...response })
      },
    },
  )

  useEffect(() => {
    setPageTitle(currentPromocode?.name || "Промокод")
    return () => {
      setPageTitle(null)
    }
  }, [currentPromocode, setPageTitle])

  return (
    <>
      <Skeleton title active avatar loading={isLoading}>
        {!!currentPromocode && (
          <>
            <PromocodeForm promocode={currentPromocode} />
          </>
        )}
        {currentPromocode === null && (
          <Empty
            image={Empty.PRESENTED_IMAGE_SIMPLE}
            description={"Промокод не найден"}
          />
        )}
      </Skeleton>
    </>
  )
}

PromocodeDetailPage.requireAuth = true
PromocodeDetailPage.getLayout = (page) => {
  return <Default backRoute={`${ROUTES.promocodes}`}>{page}</Default>
}
export default PromocodeDetailPage
