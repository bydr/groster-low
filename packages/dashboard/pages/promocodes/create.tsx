import { NextPageWithRequiredAuth } from "../_app"
import { Default } from "../../layouts/Default/Default"
import { ROUTES } from "../../utils/constants"
import React from "react"
import { PromocodeForm } from "../../components/Promocode/Form"

const PromocodeCreatePage: NextPageWithRequiredAuth = (): JSX.Element => {
  return (
    <>
      <PromocodeForm />
    </>
  )
}

PromocodeCreatePage.requireAuth = true
PromocodeCreatePage.getLayout = (page) => {
  return (
    <Default
      pageTitle={"Создание промокода"}
      backRoute={`${ROUTES.promocodes}`}
    >
      {page}
    </Default>
  )
}

export default PromocodeCreatePage
