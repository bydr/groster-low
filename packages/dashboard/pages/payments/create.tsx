import { NextPageWithRequiredAuth } from "../_app"
import { Default } from "../../layouts/Default/Default"
import { ROUTES } from "../../utils/constants"
import React from "react"
import { PaymentForm } from "../../components/Payment/Form"

const PaymentCreatePage: NextPageWithRequiredAuth = (): JSX.Element => {
  return (
    <>
      <PaymentForm />
    </>
  )
}

PaymentCreatePage.requireAuth = true
PaymentCreatePage.getLayout = (page) => {
  return (
    <Default
      pageTitle={"Создание метода оплаты"}
      backRoute={`${ROUTES.payments}`}
    >
      {page}
    </Default>
  )
}

export default PaymentCreatePage
