import React from "react"
import { NextPageWithRequiredAuth } from "../_app"
import { Default } from "../../layouts/Default/Default"
import { PaymentList } from "../../components/Payment/List"
import { ROUTES } from "../../utils/constants"
import { Button } from "antd"
import { useQuery } from "react-query"
import { paymentsAPI } from "../../api/paymentsAPI"
import { useAppDispatch } from "../../hooks/redux"
import { paymentsSlice } from "../../store/reducers/paymentsSlice"

const PaymentsSettingsPage: NextPageWithRequiredAuth = (): JSX.Element => {
  const dispatch = useAppDispatch()
  const { setMethods } = paymentsSlice.actions

  const { isLoading } = useQuery(
    "payments",
    () => paymentsAPI.getAdminPaymentMethods(),
    {
      onSuccess: (response) => {
        dispatch(setMethods(response || []))
      },
    },
  )

  return (
    <>
      <Button type={"primary"} href={`${ROUTES.base}${ROUTES.payments}/create`}>
        Создать новый
      </Button>
      <PaymentList loading={isLoading} />
    </>
  )
}

PaymentsSettingsPage.requireAuth = true
PaymentsSettingsPage.getLayout = (page) => {
  return <Default pageTitle={"Методы оплаты"}>{page}</Default>
}

export default PaymentsSettingsPage
