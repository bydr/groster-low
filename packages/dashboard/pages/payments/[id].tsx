import { GetServerSidePropsContext } from "next"
import { NextPageWithRequiredAuth } from "../_app"
import { useApp } from "../../hooks/app"
import { useEffect, useState } from "react"
import { useQuery } from "react-query"
import { Empty, Skeleton } from "antd"
import { Default } from "../../layouts/Default/Default"
import { ROUTES } from "../../utils/constants"
import { paymentsAPI } from "../../api/paymentsAPI"
import { useAppDispatch, useAppSelector } from "../../hooks/redux"
import { AdminPaymentMethod } from "../../../contracts/contracts"
import { PaymentForm } from "../../components/Payment/Form"
import { paymentsSlice } from "../../store/reducers/paymentsSlice"

type ServerSidePropsType = {
  id?: string
}

export const getServerSideProps = ({
  params,
}: GetServerSidePropsContext): {
  props: ServerSidePropsType
} => {
  return {
    props: {
      id: params?.id as string | undefined,
    },
  }
}

const PaymentDetailPage: NextPageWithRequiredAuth = (props): JSX.Element => {
  const { id } = props as ServerSidePropsType
  const { setPageTitle } = useApp()
  const dispatch = useAppDispatch()
  const { setMethods } = paymentsSlice.actions
  const paymentsMethods = useAppSelector((state) => state.payments.methods)
  const [currentMethod, setCurrentMethod] = useState<
    AdminPaymentMethod | null | undefined
  >(undefined)

  useQuery(
    ["payments", paymentsMethods],
    () =>
      paymentsMethods === null ? paymentsAPI.getAdminPaymentMethods() : null,
    {
      onSuccess: (response) => {
        console.log("success ", response)
        if (response !== null) {
          dispatch(setMethods(response || []))
        }
      },
    },
  )

  useEffect(() => {
    setCurrentMethod((paymentsMethods || {})[id || ""] || null)
  }, [id, paymentsMethods])

  useEffect(() => {
    setPageTitle(currentMethod?.name || "Метод оплаты")
    return () => {
      setPageTitle(null)
    }
  }, [currentMethod, setPageTitle])

  return (
    <>
      <Skeleton title active avatar loading={currentMethod === undefined}>
        {!!currentMethod && (
          <>
            <PaymentForm payment={currentMethod} />
          </>
        )}
        {currentMethod === null && (
          <Empty
            image={Empty.PRESENTED_IMAGE_SIMPLE}
            description={"Метод оплаты не найден"}
          />
        )}
      </Skeleton>
    </>
  )
}

PaymentDetailPage.requireAuth = true
PaymentDetailPage.getLayout = (page) => {
  return <Default backRoute={`${ROUTES.payments}`}>{page}</Default>
}

export default PaymentDetailPage
