import httpProxyMiddleware, {
  NextHttpProxyMiddlewareOptions,
} from "next-http-proxy-middleware"
import { NextApiRequest, NextApiResponse } from "next"

export const config = {
  api: {
    externalResolver: true,
  },
}

const options: NextHttpProxyMiddlewareOptions = {
  target: "https://api.groster.me",
  changeOrigin: true,
  pathRewrite: [
    {
      patternStr: `^/dashboard/api/v1`,
      replaceStr: `/api/v1`,
    },
    {
      patternStr: "^/dashboard/api",
      replaceStr: "",
    },
  ],
}

// eslint-disable-next-line import/no-anonymous-default-export
export default async (
  req: NextApiRequest,
  res: NextApiResponse,
): Promise<unknown> => {
  // console.log("req ", req)
  return await httpProxyMiddleware(req, res, options)
}
