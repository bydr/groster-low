import Document, { Head, Html, Main, NextScript } from "next/document"
import { ReactElement } from "react"

class MyDocument extends Document {
  render(): ReactElement {
    // noinspection HtmlRequiredTitleElement
    return (
      <Html lang="ru">
        <Head>
          <link
            rel="shortcut icon"
            href={`${process.env.NEXT_PUBLIC_BASE_URL}/favicon.ico`}
          />
          <link
            rel="apple-touch-icon"
            sizes="180x180"
            href={`${process.env.NEXT_PUBLIC_BASE_URL}/apple-touch-icon.png`}
          />
          <link
            rel="icon"
            type="image/png"
            sizes="32x32"
            href={`${process.env.NEXT_PUBLIC_BASE_URL}/favicon-32x32.png`}
          />
          <link
            rel="icon"
            type="image/png"
            sizes="16x16"
            href={`${process.env.NEXT_PUBLIC_BASE_URL}/favicon-16x16.png`}
          />
          <link
            rel="manifest"
            href={`${process.env.NEXT_PUBLIC_BASE_URL}/site.webmanifest`}
          />
          <link
            rel="mask-icon"
            href={`${process.env.NEXT_PUBLIC_BASE_URL}/safari-pinned-tab.svg`}
            color="#be2be9"
          />
          <meta name="msapplication-TileColor" content="#fecc00" />
          <meta name="theme-color" content="#be2be9" />
        </Head>
        <body>
          <Main />
          <NextScript />
        </body>
      </Html>
    )
  }
}

export default MyDocument
