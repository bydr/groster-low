import { Button, Col, Row, Typography } from "antd"
import { Empty } from "../layouts/Empty/Empty"
import { NextPageWithLayout } from "./_app"
import { ROUTES } from "../utils/constants"

const Custom404: NextPageWithLayout = (): JSX.Element => {
  return (
    <>
      <Row
        justify={"center"}
        align={"middle"}
        style={{
          height: "100vh",
        }}
      >
        <Col>
          <Typography.Title level={1} style={{ margin: 0 }}>
            404
          </Typography.Title>
          <Typography.Title level={3} style={{ marginTop: 0 }}>
            Страница не найдена
          </Typography.Title>
          <br />
          <Button type={"primary"} href={`${ROUTES.base}${ROUTES.home}`}>
            На главную
          </Button>
        </Col>
      </Row>
    </>
  )
}

Custom404.getLayout = (page) => {
  return <Empty>{page}</Empty>
}

export default Custom404
