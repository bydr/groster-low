import { NextPageWithRequiredAuth } from "./_app"
import { Default } from "../layouts/Default/Default"
import React from "react"
import { Button, Popover } from "antd"
import { IconText } from "../components/IconText"
import {
  CheckCircleOutlined,
  CopyOutlined,
  SelectOutlined,
} from "@ant-design/icons"
import { useClipboardCopy } from "../hooks/clipboardCopy"
import { useAuth } from "../hooks/auth"

const HomePage: NextPageWithRequiredAuth = (): JSX.Element => {
  const { handleCopyClick, isCopied } = useClipboardCopy()
  const { logout } = useAuth()

  return (
    <>
      <Popover
        key={"copy"}
        content={
          <IconText
            icon={CheckCircleOutlined}
            text={`Ссылка скопирована`}
            isSuccess
          />
        }
        visible={isCopied}
      >
        <Button
          type={"ghost"}
          onClick={() => {
            handleCopyClick(`${process.env.NEXT_PUBLIC_SITE_BASE_URL}/anyquery`)
          }}
        >
          <IconText
            icon={CopyOutlined}
            text={`Ссылка на фид для поиска Diginetica`}
          />
        </Button>
      </Popover>
      <br />
      <br />
      <Button
        ghost
        type="primary"
        href={`${process.env.NEXT_PUBLIC_SITE_BASE_URL}`}
        icon={<SelectOutlined />}
        target={"_blank"}
      >
        Перейти на сайт
      </Button>
      <br />
      <br />
      <Button onClick={logout} type={"ghost"} danger>
        Выйти
      </Button>
    </>
  )
}

HomePage.requireAuth = true
HomePage.getLayout = (page) => {
  return <Default pageTitle={"Главная"}>{page}</Default>
}

export default HomePage
