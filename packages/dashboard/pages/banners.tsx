import { NextPageWithRequiredAuth } from "./_app"
import { Default } from "../layouts/Default/Default"
import React from "react"
import { Banners } from "../components/Banners"

const BannersPage: NextPageWithRequiredAuth = (): JSX.Element => {
  return (
    <>
      <Banners />
    </>
  )
}

BannersPage.requireAuth = true
BannersPage.getLayout = (page) => {
  return <Default pageTitle={"Баннеры"}>{page}</Default>
}

export default BannersPage
