import React from "react"
import { NextPageWithRequiredAuth } from "../_app"
import { useQuery } from "react-query"
import { faqAPI } from "../../api/faqAPI"
import { useAppDispatch } from "../../hooks/redux"
import { faqSlice } from "../../store/reducers/faqSlice"
import { Default } from "../../layouts/Default/Default"
import { FaqList } from "../../components/Faq/List"

const FaqPage: NextPageWithRequiredAuth = () => {
  const dispatch = useAppDispatch()
  const { setBlocks, setQuestions } = faqSlice.actions

  useQuery("blocksFaq", () => faqAPI.getBlocks(), {
    onSuccess: (response) => {
      dispatch(setBlocks(response || []))
    },
  })
  useQuery("questionsFaq", () => faqAPI.getQuestions(), {
    onSuccess: (response) => {
      dispatch(setQuestions(response || []))
    },
  })

  return (
    <>
      <FaqList />
    </>
  )
}

FaqPage.requireAuth = true
FaqPage.getLayout = (page) => {
  return <Default pageTitle={"Вопрос ответ"}>{page}</Default>
}

export default FaqPage
