import { createSlice, PayloadAction } from "@reduxjs/toolkit"
import { setUser, User } from "../../hooks/auth"

const initialState = {
  user: null as Required<User> | null,
  isAuth: false as boolean | false,
  isInit: false as boolean | false,
}
export const accountSlice = createSlice({
  name: "profile",
  initialState,
  reducers: {
    setIsAuth(state, action: PayloadAction<boolean>) {
      state.isAuth = action.payload
    },
    setIsInit(state, action: PayloadAction<boolean>) {
      state.isInit = action.payload
    },
    setUserProfile(state, action: PayloadAction<User | null>) {
      if (action.payload !== null) {
        state.user = {
          email: action.payload.email || null,
          accessToken: action.payload.accessToken || null,
          phone: action.payload.phone || null,
          cart: action.payload.cart || null,
          refreshToken: action.payload.refreshToken || null,
          fio: action.payload.fio || null,
          isAdmin: !!action.payload.isAdmin,
        }
      } else {
        state.user = action.payload
      }

      setUser(action.payload)
    },
  },
})

export default accountSlice.reducer
