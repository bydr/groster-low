import { createSlice, PayloadAction } from "@reduxjs/toolkit"
import { Shop, ShopAdmin, ShopListResponse } from "../../../contracts/contracts"

const initialState = {
  shops: null as Record<string, ShopAdmin> | null,
  currentShop: null as (ShopAdmin & { uuid?: string }) | null,
}

export const shopsSlice = createSlice({
  name: "shops",
  initialState,
  reducers: {
    setShops: (state, action: PayloadAction<ShopListResponse | null>) => {
      if (action.payload !== null) {
        const methods = {}
        for (const shop of action.payload) {
          if (shop.uuid !== undefined) {
            methods[shop.uuid] = shop
          }
        }
        state.shops = methods
      } else {
        state.shops = action.payload
      }
    },
    updateShop: (state, action: PayloadAction<Shop & { uuid: string }>) => {
      if (state.shops !== null) {
        state.shops[action.payload.uuid] = {
          ...(state.shops[action.payload.uuid] || {}),
          ...action.payload,
        }
      }
    },
    setCurrentShop: (
      state,
      action: PayloadAction<null | (ShopAdmin & { uuid?: string })>,
    ) => {
      state.currentShop = action.payload
    },
  },
})

export default shopsSlice.reducer
