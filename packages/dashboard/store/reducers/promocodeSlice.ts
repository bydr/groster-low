import { createSlice, PayloadAction } from "@reduxjs/toolkit"
import { Category, CodeResponse } from "../../../contracts/contracts"
import {
  CategoriesStateType,
  CategoriesTreeStateType,
  CategoryType,
} from "../../types/types"
import { createCategoriesTree } from "../../utils/helpers"

const initialState = {
  promocodes: null as CodeResponse[] | null,
  categories: null as CategoriesStateType<
    CategoryType & { checked?: boolean }
  > | null,
}

export const promocodesSlice = createSlice({
  name: "promocode",
  initialState,
  reducers: {
    setPromocodes: (state, action: PayloadAction<CodeResponse[] | null>) => {
      state.promocodes = action.payload
    },
    setCategories(state, action: PayloadAction<Category[]>) {
      const categoriesTree: CategoriesTreeStateType | null =
        createCategoriesTree(action.payload)

      state.categories = {
        compared: categoriesTree?.compared,
        tree: categoriesTree?.tree,
        fetched:
          action.payload &&
          (action.payload.reduce(
            (a, v) => ({ ...a, [v?.uuid || ""]: v }),
            {},
          ) as Record<string, Category>),
      }
    },
    updateCheckedCategories: (
      state,
      action: PayloadAction<{ categories: string[]; checked: boolean }>,
    ) => {
      if (state.categories?.fetched !== undefined) {
        const fetched = !!state.categories?.fetched
          ? { ...state.categories.fetched }
          : state.categories?.fetched

        if (fetched !== null) {
          for (const k of action.payload.categories) {
            if (!fetched[k]) {
              continue
            }
            fetched[k].checked = action.payload.checked
          }
        }

        state.categories.fetched = fetched
      }
    },
  },
})

export default promocodesSlice.reducer
