import {
  AdminDeliveryMethodList,
  ResponseLimit,
} from "../../../contracts/contracts"
import { createSlice, PayloadAction } from "@reduxjs/toolkit"

const initialState = {
  methods: null as AdminDeliveryMethodList[] | null,
  current: {
    method: null as AdminDeliveryMethodList | null,
    limits: null as ResponseLimit[] | null,
  },
}

export const shippingMethodsSlice = createSlice({
  name: "shippingMethods",
  initialState,
  reducers: {
    setMethods: (
      state,
      action: PayloadAction<null | AdminDeliveryMethodList[]>,
    ) => {
      state.methods = action.payload
    },
    updateMethod: (state, action: PayloadAction<AdminDeliveryMethodList>) => {
      if (state.methods !== null) {
        const methodInd = state.methods.findIndex(
          (item) => item.alias === action.payload.alias,
        )
        state.methods[methodInd] = { ...action.payload }
      }
    },
    setCurrentMethod: (
      state,
      action: PayloadAction<AdminDeliveryMethodList | null>,
    ) => {
      state.current.method = action.payload
    },
    setLimits: (state, action: PayloadAction<ResponseLimit[] | null>) => {
      state.current.limits = action.payload
    },
    updateLimit: (state, action: PayloadAction<ResponseLimit>) => {
      if (state.current.limits !== null) {
        state.current.limits = [
          ...state.current.limits.filter((l) => l.id !== action.payload.id),
          { ...action.payload },
        ]
      }
    },
    removeLimit: (state, action: PayloadAction<{ id: number }>) => {
      if (state.current.limits !== null) {
        state.current.limits = state.current.limits.filter(
          (l) => l.id !== action.payload.id,
        )
      }
    },
  },
})

export default shippingMethodsSlice.reducer
