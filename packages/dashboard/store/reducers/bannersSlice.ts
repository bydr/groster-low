import { createSlice, PayloadAction } from "@reduxjs/toolkit"
import { AdminResponseBanners } from "../../../contracts/contracts"

const initialState = {
  banners: null as AdminResponseBanners | null,
}

export const bannersSlice = createSlice({
  name: "banners",
  initialState,
  reducers: {
    setBanners: (state, action: PayloadAction<null | AdminResponseBanners>) => {
      state.banners = action.payload
    },
  },
})

export default bannersSlice.reducer
