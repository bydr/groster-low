import { createSlice, PayloadAction } from "@reduxjs/toolkit"
import { Category, TagResponse } from "../../../contracts/contracts"
import { CategoriesStateType, CategoriesTreeStateType } from "../../types/types"
import { createCategoriesTree } from "../../utils/helpers"

const initialState = {
  tags: null as TagResponse | null,
  categories: null as CategoriesStateType | null,
}

export const catalogSlice = createSlice({
  name: "catalog",
  initialState,
  reducers: {
    setTags: (state, action: PayloadAction<TagResponse | null>) => {
      state.tags = action.payload
    },
    setCategories(state, action: PayloadAction<Category[]>) {
      const categoriesTree: CategoriesTreeStateType | null =
        createCategoriesTree(action.payload)

      state.categories = {
        compared: categoriesTree?.compared,
        tree: categoriesTree?.tree,
        fetched:
          action.payload &&
          (action.payload.reduce(
            (a, v) => ({ ...a, [v?.uuid || ""]: v }),
            {},
          ) as Record<string, Category>),
      }
    },
  },
})

export default catalogSlice.reducer
