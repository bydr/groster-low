import { createSlice, PayloadAction } from "@reduxjs/toolkit"
import {
  ResponseAdminBlock,
  ResponseAdminFaq,
} from "../../../contracts/contracts"

const initialState = {
  blocks: null as ResponseAdminBlock | null,
  questions: null as ResponseAdminFaq | null,
}

export const faqSlice = createSlice({
  name: "faq",
  initialState,
  reducers: {
    setBlocks: (state, action: PayloadAction<null | ResponseAdminBlock>) => {
      state.blocks = action.payload
    },
    setQuestions: (state, action: PayloadAction<null | ResponseAdminFaq>) => {
      state.questions = action.payload
    },
  },
})

export default faqSlice.reducer
