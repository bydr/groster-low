import { createSlice, PayloadAction } from "@reduxjs/toolkit"
import { AdminPaymentMethod } from "../../../contracts/contracts"

const initialState = {
  methods: null as Record<string, AdminPaymentMethod> | null,
}

export const paymentsSlice = createSlice({
  name: "payments",
  initialState,
  reducers: {
    setMethods: (state, action: PayloadAction<AdminPaymentMethod[] | null>) => {
      if (action.payload !== null) {
        const methods = {}
        for (const payment of action.payload) {
          if (payment.uid !== undefined) {
            methods[payment.uid] = { ...payment }
          }
        }
        state.methods = methods
      } else {
        state.methods = action.payload
      }
    },
    updateMethod: (state, action: PayloadAction<AdminPaymentMethod>) => {
      if (state.methods !== null) {
        if (action.payload.uid !== undefined) {
          state.methods[action.payload.uid] = {
            ...(state.methods[action.payload.uid] || {}),
            ...action.payload,
          }
        }
      }
    },
  },
})

export default paymentsSlice.reducer
