import { combineReducers } from "redux"
import { configureStore } from "@reduxjs/toolkit"
import accountReducer from "./reducers/accountSlice"
import catalogReducer from "./reducers/catalogSlice"
import paymentsReducer from "./reducers/paymentsSlice"
import shopsReducer from "./reducers/shopsSlice"
import shippingMethodsReducer from "./reducers/shippingMethodsSlice"
import promocodesReducer from "./reducers/promocodeSlice"
import faqReducer from "./reducers/faqSlice"
import bannersReducer from "./reducers/bannersSlice"

const rootReducer = combineReducers({
  account: accountReducer,
  catalog: catalogReducer,
  payments: paymentsReducer,
  shop: shopsReducer,
  shippingMethods: shippingMethodsReducer,
  promocodes: promocodesReducer,
  faq: faqReducer,
  banners: bannersReducer,
})

export const store = configureStore({
  reducer: rootReducer,
  devTools: true,
})

export type RootStateType = ReturnType<typeof rootReducer>
export type AppStoreType = typeof store
export type AppDispatch = AppStoreType["dispatch"]
