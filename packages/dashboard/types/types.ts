import { Category, Shop } from "../../contracts/contracts"
import { ReactNode } from "react"

export type ShopType = Shop & {
  uuid: string
}

export interface ListItemType {
  title: string
  path: string
}

export type CategoryType = Category
export interface ICategoryTreeItem extends CategoryType {
  children: Record<string, ICategoryTreeItem>
}
export interface CategoriesTreeStateType {
  tree: Record<string, ICategoryTreeItem> | null
  compared: Record<string, number> | null
}

export interface CategoriesStateType<CT = CategoryType>
  extends CategoriesTreeStateType {
  fetched: Record<string, CT> | null
}
export type BreadcrumbCategoryType = CategoryType & {
  children: ListItemType[]
  path: string
}

export type SelectValueType = { value: string | number; label?: ReactNode }
