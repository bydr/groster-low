import { css } from "@linaria/core"
import { styled } from "@linaria/react"

const globals = css`
  :global {
    html {
      box-sizing: border-box;
    }

    html,
    body {
      padding: 0;
      margin: 0;
      scroll-behavior: smooth;
    }

    *,
    *:before,
    *:after {
      box-sizing: border-box;
      outline: none;
    }

    * {
      outline: none;
      -webkit-tap-highlight-color: rgba(0, 0, 0, 0);
    }

    img {
      max-width: 100%;
    }

    a {
      text-decoration: none;
      display: inline-block;
    }
  }
`

export const Container = styled.section`
  padding-left: 16px;
  padding-right: 16px;
`

export const StyledListWrapper = styled.div`
  position: relative;
  width: 100%;
`

export const StyledListDescription = styled.div`
  width: 100%;
  display: flex;
  flex-direction: column;
  grid-gap: 4px;
  align-items: flex-start;

  .ant-form-item {
    margin-bottom: 0;
  }
`

export const StyledCheckboxList = styled.div`
  display: inline-flex;
  flex-direction: column;
  padding: 0;
  margin-bottom: 10px;
  width: 100%;
  padding-left: 20px;

  .ant-checkbox-wrapper {
    margin-bottom: 8px;
  }

  .ant-checkbox-wrapper + .ant-checkbox-wrapper {
    margin-left: 0;
  }

  @media (max-width: 768px) {
    width: 100%;
  }
`

export default globals
