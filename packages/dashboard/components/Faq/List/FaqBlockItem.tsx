import { FC, useState } from "react"
import { BlockTreeType } from "./index"
import { Button, List, Popconfirm, Typography } from "antd"
import { FaqQuestionItem } from "./FaqQuestionItem"
import {
  cssStyledBlockItem,
  cssStyledListHide,
  cssStyledQuestionList,
} from "./StyledList"
import { useBlockFaq } from "../../../hooks/blockFaq"
import { cx } from "@linaria/core"
import { CaretDownOutlined, CaretUpOutlined } from "@ant-design/icons"

export const FaqBlockItem: FC<{
  block: BlockTreeType
  showEditModalBlock: (blockId?: number) => void
  showEditModalQuestion: (questionId?: number) => void
}> = ({ block, showEditModalBlock, showEditModalQuestion }) => {
  const {
    removeConfirm: { visible, showPopup, handleOk, handleCancel },
    isLoadingRemove,
  } = useBlockFaq()
  const [isShowChilds, setIsShopChilds] = useState<boolean>(true)
  const isQuestions = !!block.questions.length

  return (
    <>
      <List.Item
        className={cssStyledBlockItem}
        actions={[
          <Button
            type={"link"}
            key={"edit"}
            onClick={() => {
              showEditModalBlock(block.id)
            }}
          >
            Редактировать
          </Button>,
          <Popconfirm
            key={"remove"}
            title="Вы действительно хотите удалить блок?"
            okText={"Удалить"}
            cancelText={"Отмена"}
            visible={visible}
            onConfirm={() => {
              handleOk(block.id)
            }}
            okButtonProps={{
              loading: isLoadingRemove,
              type: "primary",
              danger: true,
            }}
            onCancel={handleCancel}
          >
            <Button type={"link"} danger onClick={showPopup}>
              Удалить
            </Button>
          </Popconfirm>,
          isQuestions && (
            <Button
              key={"toggleList"}
              type={isShowChilds ? "text" : "primary"}
              icon={isShowChilds ? <CaretUpOutlined /> : <CaretDownOutlined />}
              onClick={() => {
                setIsShopChilds(!isShowChilds)
              }}
            >
              {isShowChilds ? "Свернуть" : "Развернуть"}
            </Button>
          ),
        ]}
      >
        <List.Item.Meta
          title={
            <Typography.Title level={4}>
              {block.name || "Без имени"}
            </Typography.Title>
          }
        />
        {block.questions.length > 0 && (
          <>
            <List
              className={cx(
                cssStyledQuestionList,
                !isShowChilds && cssStyledListHide,
              )}
              itemLayout="vertical"
              dataSource={block.questions.sort((a, b) => {
                return (a?.title || "") > (b.title || "")
                  ? 1
                  : (a.title || "") < (b.title || "")
                  ? -1
                  : 0
              })}
              renderItem={(item) => (
                <FaqQuestionItem
                  question={item}
                  showEditModalQuestion={showEditModalQuestion}
                />
              )}
            />
          </>
        )}
      </List.Item>
    </>
  )
}
