import { css } from "@linaria/core"

export const cssStyledBlockItem = css`
  display: grid;
  grid-template-areas: "meta" "actions" "childs";
  justify-content: normal;

  > * {
    width: 100%;
  }

  .ant-list-item-meta {
    grid-area: meta;
  }

  .ant-list-item-action {
    grid-area: actions;
    margin: 0;
  }
`
export const cssStyledQuestionList = css``
export const cssStyledBlockList = css`
  .${cssStyledQuestionList} {
    padding-top: 10px;
    padding-left: 30px;
  }
`
export const cssStyledListHide = css`
  display: none !important;
`
