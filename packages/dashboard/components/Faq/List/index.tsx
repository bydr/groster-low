import { FC, useCallback, useEffect, useState } from "react"
import { useAppSelector } from "../../../hooks/redux"
import { Loader } from "../../Loader/Loader"
import { Button, List, Modal, Popconfirm } from "antd"
import { StyledListWrapper } from "../../../styles/globals"
import { AdminBlock, ResponseAdminFaq } from "../../../../contracts/contracts"
import { FaqBlockItem } from "./FaqBlockItem"
import { cssStyledBlockList } from "./StyledList"
import { useBlockFaq } from "../../../hooks/blockFaq"
import { useQuestionFaq } from "../../../hooks/questionFaq"
import { BlockForm } from "../Form/BlockForm"
import { QuestionForm } from "../Form/QuestionForm"

export type BlockTreeType = AdminBlock & { id?: number } & {
  questions: ResponseAdminFaq
}

type TreeType = {
  blocks: Record<number, BlockTreeType>
  unblocks: ResponseAdminFaq
}

export const FaqList: FC<{ loading?: boolean }> = ({ loading }) => {
  const blocks = useAppSelector((state) => state.faq.blocks)
  const questions = useAppSelector((state) => state.faq.questions)
  const [tree, setTree] = useState<TreeType | null>(null)

  const [isModalVisibleBlock, setIsModalVisibleBlock] = useState(false)
  const [isModalVisibleQuestion, setIsModalVisibleQuestion] = useState(false)
  const [currentBlockId, setCurrentBlockId] = useState<null | number>(null)
  const [currentQuestionId, setCurrentQuestionId] = useState<null | number>(
    null,
  )

  const {
    removeConfirm: removeConfirmBlock,
    isLoadingRemove: isLoadingRemoveBlock,
  } = useBlockFaq({
    onRemove: () => {
      handleCancelBlock()
    },
  })
  const {
    removeConfirm: removeConfirmQuestion,
    isLoadingRemove: isLoadingRemoveQuestion,
  } = useQuestionFaq({
    onRemove: () => {
      handleCancelQuestion()
    },
  })

  const showModalBlock = useCallback(() => {
    setIsModalVisibleBlock(true)
  }, [])
  const showModalQuestion = useCallback(() => {
    setIsModalVisibleQuestion(true)
  }, [])

  const handleCancelBlock = () => {
    setIsModalVisibleBlock(false)
    setCurrentBlockId(null)
  }
  const handleCancelQuestion = () => {
    setIsModalVisibleQuestion(false)
    setCurrentQuestionId(null)
  }

  useEffect(() => {
    const tree: TreeType = {
      blocks: {},
      unblocks: [],
    }
    let initQuests: ResponseAdminFaq = [
      ...(questions || []),
      {
        id: 11,
        block: 2,
        title: "ex1",
        answer: "ans1",
      },
      {
        id: 12,
        block: 2,
        title: "ex2",
        answer: "ans2",
      },
    ]
    if (blocks !== null) {
      for (const block of blocks) {
        if (block.id === undefined) {
          continue
        }
        const childs = initQuests.filter((q) => q.block === block.id)
        tree.blocks[block.id] = {
          ...block,
          questions: childs,
        }
        initQuests = initQuests.filter((q) => !childs.includes(q))
      }
    } else {
      tree.blocks = {}
    }
    tree.unblocks = initQuests

    setTree(tree)
  }, [blocks, questions])

  useEffect(() => {
    return () => {
      setCurrentBlockId(null)
      setCurrentQuestionId(null)
    }
  }, [setCurrentBlockId, setCurrentQuestionId])

  return (
    <>
      {/*<ButtonGroup>*/}
      <Button type={"primary"} onClick={showModalQuestion}>
        Создать вопрос
      </Button>
      <Button type={"ghost"} onClick={showModalBlock}>
        Создать блок
      </Button>
      {/*</ButtonGroup>*/}
      <StyledListWrapper>
        {loading && <Loader />}
        <>
          {tree !== null && (
            <>
              <List
                className={cssStyledBlockList}
                itemLayout="vertical"
                dataSource={Object.keys(tree.blocks)
                  .map((b) => tree.blocks[b])
                  .sort((a, b) => {
                    return (a?.name || "") > (b.name || "")
                      ? 1
                      : (a.name || "") < (b.name || "")
                      ? -1
                      : 0
                  })}
                renderItem={(item) => (
                  <FaqBlockItem
                    block={item}
                    showEditModalBlock={(id) => {
                      setCurrentBlockId(id || null)
                      showModalBlock()
                    }}
                    showEditModalQuestion={(id) => {
                      setCurrentQuestionId(id || null)
                      showModalQuestion()
                    }}
                  />
                )}
              />
              <Modal
                title={
                  currentBlockId !== null
                    ? "Редактирование блока"
                    : "Создание блока"
                }
                visible={isModalVisibleBlock}
                cancelText={"Отмена"}
                onCancel={handleCancelBlock}
                destroyOnClose={true}
                footer={[
                  <Button key="back" onClick={handleCancelBlock}>
                    Закрыть
                  </Button>,
                  <>
                    {currentBlockId !== null && (
                      <Popconfirm
                        key={"remove"}
                        title="Вы действительно хотите удалить блок?"
                        okText={"Удалить"}
                        cancelText={"Отмена"}
                        visible={removeConfirmBlock.visible}
                        onConfirm={() => {
                          removeConfirmBlock.handleOk(
                            currentBlockId || undefined,
                          )
                        }}
                        okButtonProps={{
                          loading: isLoadingRemoveBlock,
                          type: "primary",
                          danger: true,
                        }}
                        onCancel={removeConfirmBlock.handleCancel}
                      >
                        <Button danger onClick={removeConfirmBlock.showPopup}>
                          Удалить
                        </Button>
                      </Popconfirm>
                    )}
                  </>,
                ]}
              >
                <BlockForm
                  block={(blocks || []).find((b) => b.id === currentBlockId)}
                />
              </Modal>
              <Modal
                title={
                  currentBlockId !== null
                    ? "Редактирование вопроса"
                    : "Создание вопроса"
                }
                visible={isModalVisibleQuestion}
                cancelText={"Отмена"}
                onCancel={handleCancelQuestion}
                destroyOnClose={true}
                footer={[
                  <Button key="back" onClick={handleCancelQuestion}>
                    Закрыть
                  </Button>,
                  <>
                    {currentQuestionId !== null && (
                      <Popconfirm
                        key={"remove"}
                        title="Вы действительно хотите удалить вопрос?"
                        okText={"Удалить"}
                        cancelText={"Отмена"}
                        visible={removeConfirmQuestion.visible}
                        onConfirm={() => {
                          removeConfirmQuestion.handleOk(
                            currentQuestionId || undefined,
                          )
                        }}
                        okButtonProps={{
                          loading: isLoadingRemoveQuestion,
                          type: "primary",
                          danger: true,
                        }}
                        onCancel={removeConfirmQuestion.handleCancel}
                      >
                        <Button
                          danger
                          onClick={removeConfirmQuestion.showPopup}
                        >
                          Удалить
                        </Button>
                      </Popconfirm>
                    )}
                  </>,
                ]}
              >
                <QuestionForm
                  question={(questions || []).find(
                    (q) => q.id === currentQuestionId,
                  )}
                />
              </Modal>
            </>
          )}
        </>
      </StyledListWrapper>
    </>
  )
}
