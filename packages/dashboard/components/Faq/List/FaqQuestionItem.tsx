import { FC } from "react"
import { QuestionType } from "../../../../contracts/contracts"
import { Button, List, Popconfirm, Typography } from "antd"
import { StyledListDescription } from "../../../styles/globals"
import { useQuestionFaq } from "../../../hooks/questionFaq"

export const FaqQuestionItem: FC<{
  question: QuestionType
  showEditModalQuestion: (id?: number) => void
}> = ({ question, showEditModalQuestion }) => {
  const {
    removeConfirm: { visible, showPopup, handleOk, handleCancel },
    isLoadingRemove,
  } = useQuestionFaq()
  return (
    <>
      <List.Item
        actions={[
          <Button
            type={"link"}
            key={"edit"}
            onClick={() => {
              showEditModalQuestion(question.id)
            }}
          >
            Редактировать
          </Button>,
          <Popconfirm
            key={"remove"}
            title="Вы действительно хотите удалить вопрос?"
            okText={"Удалить"}
            cancelText={"Отмена"}
            visible={visible}
            onConfirm={() => {
              handleOk(question.id)
            }}
            okButtonProps={{
              loading: isLoadingRemove,
              type: "primary",
              danger: true,
            }}
            onCancel={handleCancel}
          >
            <Button type={"link"} danger onClick={showPopup}>
              Удалить
            </Button>
          </Popconfirm>,
        ]}
      >
        <List.Item.Meta
          title={question.title || "Без имени"}
          description={
            <>
              <StyledListDescription>
                <Typography.Text>{question.answer}</Typography.Text>
              </StyledListDescription>
            </>
          }
        />
      </List.Item>
    </>
  )
}
