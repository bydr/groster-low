import { FC, useEffect, useState } from "react"
import { Button, Form, Input, InputNumber, Select, Space } from "antd"
import { QuestionType } from "../../../../contracts/contracts"
import { ErrorMessage } from "../../Messages/ErrorMessage"
import { useQuestionFaq } from "../../../hooks/questionFaq"
import { SelectValueType } from "../../../types/types"
import { useAppSelector } from "../../../hooks/redux"

const { Option } = Select

export const QuestionForm: FC<{
  question?: QuestionType
}> = ({ question }) => {
  const blocks = useAppSelector((state) => state.faq.blocks)
  const [form] = Form.useForm<
    Omit<QuestionType, "block"> & { block?: SelectValueType }
  >()
  const [mode, setMode] = useState<"create" | "edit">(
    question !== undefined ? "edit" : "create",
  )
  const [blocksSelect, setBlocksSelect] = useState<SelectValueType[]>([])
  const { create, edit, isLoadingEdit, errorMessage, isLoadingCreate } =
    useQuestionFaq({
      onCreate: () => {
        form.resetFields()
      },
    })

  useEffect(() => {
    setMode(question !== undefined ? "edit" : "create")
  }, [question])

  useEffect(() => {
    setBlocksSelect(
      (blocks || [])
        .filter((b) => b.id !== undefined)
        .map((b) => ({
          label: b.name || "Без имени",
          value: b.id as number,
        })),
    )
  }, [blocks])

  useEffect(() => {
    if (!!question) {
      form.setFieldsValue({
        ...question,
        block: blocksSelect.find((b) => b.value === question.block),
      })
    }
  }, [blocksSelect, form, question])

  return (
    <>
      <Form
        form={form}
        name="basic"
        layout={"vertical"}
        autoComplete="off"
        onFinish={(values) => {
          console.log("values ", values)
          if (mode === "create") {
            create({
              ...values,
              block: !!values.block?.value ? +values.block?.value : undefined,
            })
          }
          if (mode === "edit") {
            if (question?.id) {
              edit({
                ...values,
                block: !!values.block?.value ? +values.block?.value : undefined,
                id: question.id,
              })
            }
          }
        }}
      >
        <Form.Item
          label="Вопрос"
          name={"title"}
          rules={[
            {
              required: true,
              message: "Поле обязательно для заполнения",
            },
          ]}
        >
          <Input type={"text"} />
        </Form.Item>
        <Form.Item
          label="Ответ"
          name={"answer"}
          rules={[
            {
              required: true,
              message: "Поле обязательно для заполнения",
            },
          ]}
        >
          <Input.TextArea />
        </Form.Item>

        <Form.Item
          label="Блок"
          name={"block"}
          rules={[
            {
              required: true,
              message: "Поле обязательно для заполнения",
            },
          ]}
        >
          <Select
            labelInValue
            onChange={(value: SelectValueType) => {
              console.log("value ", value)
              form.setFieldsValue({
                block: { ...value },
              })
            }}
          >
            {blocksSelect.map((v, i) => (
              <Option key={i} value={v.value}>
                {v.label}
              </Option>
            ))}
          </Select>
        </Form.Item>

        <Form.Item
          label="Приоритет отображения"
          name={"weight"}
          help="Чем больше значение, тем приоритетнее отображение пункта на странице"
          initialValue={0}
        >
          <InputNumber />
        </Form.Item>
        <br />

        <Form.Item>
          <Space>
            <Button
              type="primary"
              htmlType="submit"
              loading={isLoadingEdit || isLoadingCreate}
            >
              {mode === "create" && "Создать"}
              {mode === "edit" && "Сохранить изменения"}
            </Button>
          </Space>
        </Form.Item>

        {errorMessage && (
          <>
            <ErrorMessage>{errorMessage}</ErrorMessage>
          </>
        )}
      </Form>
    </>
  )
}
