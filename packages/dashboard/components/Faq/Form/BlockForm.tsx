import { FC, useEffect, useState } from "react"
import { Button, Form, Input, InputNumber, Space } from "antd"
import { AdminBlock } from "../../../../contracts/contracts"
import { useBlockFaq } from "../../../hooks/blockFaq"
import { ErrorMessage } from "../../Messages/ErrorMessage"

export const BlockForm: FC<{ block?: AdminBlock & { id?: number } }> = ({
  block,
}) => {
  const [form] = Form.useForm<AdminBlock & { id?: number }>()
  const [mode, setMode] = useState<"create" | "edit">(
    block !== undefined ? "edit" : "create",
  )
  const { create, edit, isLoadingEdit, errorMessage, isLoadingCreate } =
    useBlockFaq({
      onCreate: () => {
        form.resetFields()
      },
    })

  useEffect(() => {
    setMode(block !== undefined ? "edit" : "create")
  }, [block])

  useEffect(() => {
    if (!!block) {
      form.setFieldsValue({ ...block })
    }
  }, [form, block])

  return (
    <>
      <Form
        form={form}
        name="basic"
        layout={"vertical"}
        autoComplete="off"
        onFinish={(values) => {
          console.log("values ", values)
          if (mode === "create") {
            create({
              ...values,
            })
          }
          if (mode === "edit") {
            if (block?.id) {
              edit({
                ...values,
                id: block.id,
              })
            }
          }
        }}
      >
        <Form.Item
          label="Название"
          name={"name"}
          rules={[
            {
              required: true,
              message: "Поле обязательно для заполнения",
            },
          ]}
        >
          <Input type={"text"} />
        </Form.Item>
        <Form.Item
          label="Приоритет отображения"
          name={"weight"}
          help="Чем больше значение, тем приоритетнее отображение пункта на странице"
          initialValue={0}
        >
          <InputNumber />
        </Form.Item>
        <br />

        <Form.Item>
          <Space>
            <Button
              type="primary"
              htmlType="submit"
              loading={isLoadingEdit || isLoadingCreate}
            >
              {mode === "create" && "Создать"}
              {mode === "edit" && "Сохранить изменения"}
            </Button>
          </Space>
        </Form.Item>

        {errorMessage && (
          <>
            <ErrorMessage>{errorMessage}</ErrorMessage>
          </>
        )}
      </Form>
    </>
  )
}
