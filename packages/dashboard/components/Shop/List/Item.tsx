import { FC } from "react"
import { useShop } from "../../../hooks/shop"
import { Button, Form, List, Popover, Skeleton, Switch, Typography } from "antd"
import { CustomLink } from "../../Link/Link"
import { ROUTES } from "../../../utils/constants"
import { IconText } from "../../IconText"
import {
  CheckCircleOutlined,
  CopyOutlined,
  SelectOutlined,
} from "@ant-design/icons"
import { StyledContact, StyledSchedule } from "../StyledShop"
import { ShopAdmin } from "../../../../contracts/contracts"
import { EntityImage } from "../../EntityImage/EntityImage"
import { StyledListDescription } from "../../../styles/globals"

export const ShopItem: FC<{
  shop: ShopAdmin & { uuid?: string }
  loading?: boolean
}> = ({ shop, loading }) => {
  const { coordsIsCopied, copyCoordsHandle, isLoadingEdit, enable, disable } =
    useShop({
      shop: shop,
    })

  return (
    <List.Item
      actions={[
        <CustomLink key={"edit"} href={`${ROUTES.shops}/${shop.uuid}`}>
          Подробнее
        </CustomLink>,
        <CustomLink
          key={"external"}
          href={`${process.env.NEXT_PUBLIC_SITE_BASE_URL}/stores/${shop.uuid}`}
          target={"_blank"}
        >
          <IconText icon={SelectOutlined} text={`На сайте`} />
        </CustomLink>,
        !!shop.coordinates &&
          !!shop.coordinates.longitude &&
          !!shop.coordinates.latitude && (
            <Popover
              content={
                <IconText
                  icon={CheckCircleOutlined}
                  text={`Координаты скопированы`}
                  isSuccess
                />
              }
              visible={coordsIsCopied}
            >
              <Button
                key={"copy"}
                type={"link"}
                onClick={() => {
                  copyCoordsHandle()
                }}
              >
                <IconText icon={CopyOutlined} text={`Скопировать координаты`} />
              </Button>
            </Popover>
          ),
      ]}
    >
      <Skeleton loading={loading} active avatar>
        <List.Item.Meta
          title={shop.name || shop.address || "Без имени"}
          avatar={
            <EntityImage
              imagePath={shop.image || undefined}
              width={80}
              height={80}
              quality={50}
            />
          }
          description={
            <>
              <StyledListDescription>
                {shop.schedule && (
                  <>
                    <StyledSchedule>
                      <Typography.Text>{shop.schedule}</Typography.Text>
                    </StyledSchedule>
                  </>
                )}
                {shop.phone && (
                  <StyledContact>
                    <CustomLink href={`tel:${shop.phone}`}>
                      {shop.phone}
                    </CustomLink>
                  </StyledContact>
                )}
                <Form.Item label={"Отображать на сайте"}>
                  <Switch
                    checked={shop.is_active}
                    onChange={(checked) => {
                      if (!checked) {
                        disable()
                      } else {
                        enable()
                      }
                    }}
                    loading={isLoadingEdit}
                  />
                </Form.Item>
              </StyledListDescription>
            </>
          }
        />
      </Skeleton>
    </List.Item>
  )
}
