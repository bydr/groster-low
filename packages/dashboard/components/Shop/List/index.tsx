import { FC } from "react"
import { Empty, List } from "antd"
import { ShopItem } from "./Item"
import { useAppSelector } from "../../../hooks/redux"
import { Loader } from "../../Loader/Loader"
import { StyledListWrapper } from "styles/globals"

export const ShopList: FC<{ loading?: boolean }> = ({ loading }) => {
  const shops = useAppSelector((state) => state.shop.shops)

  return (
    <>
      <StyledListWrapper>
        {loading && <Loader />}
        {shops !== null && (
          <>
            {Object.keys(shops).length > 0 ? (
              <>
                <List
                  itemLayout="vertical"
                  dataSource={Object.keys(shops)
                    .map((key) => shops[key])
                    .sort((a, b) => {
                      return (a?.address || "") > (b.address || "")
                        ? 1
                        : (a.address || "") < (b.address || "")
                        ? -1
                        : 0
                    })
                    .sort(function (x, y) {
                      return x.is_active === y.is_active
                        ? 0
                        : x.is_active
                        ? -1
                        : 1
                    })}
                  renderItem={(item) => <ShopItem shop={item} />}
                  loading={loading}
                />
              </>
            ) : (
              <>
                <Empty
                  image={Empty.PRESENTED_IMAGE_SIMPLE}
                  description={"Магазины не найдены"}
                />
              </>
            )}
          </>
        )}
      </StyledListWrapper>
    </>
  )
}
