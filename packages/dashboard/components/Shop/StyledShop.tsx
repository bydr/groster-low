import { styled } from "@linaria/react"

export const StyledSchedule = styled.div``
export const StyledContact = styled.div``
export const ShopImage = styled.div`
  position: relative;
  width: auto;
  display: inline-flex;
  flex-direction: row;
  justify-content: flex-start;
`
