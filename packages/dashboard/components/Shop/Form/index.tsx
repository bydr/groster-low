import { FC, useEffect, useState } from "react"
import { Button, Form, Input, Switch, Upload } from "antd"
import {
  BASE_URL,
  BASE_VERSION_URL,
  MASK_PHONE,
} from "../../../utils/constants"
import { MaskInput } from "../../MaskInput"
import { useShop } from "../../../hooks/shop"
import { UploadOutlined } from "@ant-design/icons"
import { ErrorMessage } from "../../Messages/ErrorMessage"
import { ShopAdmin } from "../../../../contracts/contracts"
import { UploadChangeParam } from "antd/lib/upload"
import { UploadFile } from "antd/lib/upload/interface"
import { getBase64 } from "utils/helpers"

export const ShopForm: FC<{
  shop?: ShopAdmin & { uuid?: string }
}> = ({ shop }) => {
  const [form] = Form.useForm<
    Omit<ShopAdmin & { uuid?: string }, "coordinates"> & {
      coordinates?: string
    }
  >()
  const [mode, setMode] = useState<"create" | "edit">(
    shop !== undefined ? "edit" : "create",
  )
  const { coordsString, edit, errorMessage, isLoadingEdit } = useShop({
    shop: shop,
  })
  const [imageUrl, setImageUrl] = useState<string | null>(null)
  const [fileList, setFileList] = useState<UploadFile[]>([])

  useEffect(() => {
    form.setFieldsValue({
      image: imageUrl !== null ? imageUrl?.split(",")[1] : undefined,
    })
  }, [form, imageUrl])

  const handleChange = (info: UploadChangeParam) => {
    setFileList(info.fileList)
    if (info.file.status === "removed") {
      setImageUrl(null)
      return
    }
    if (info.file.status === "error") {
      console.log("error file")
      return
    }
    if (info.file.status === "uploading") {
      console.log("uploading file")
      return
    }
    if (info.file.status === "done") {
      // Get this url from response in real world.
      console.log("done file")
      getBase64({
        img: info.file.originFileObj,
        callback: (imageUrl: string | null) => {
          console.log("callback image ", imageUrl)
          setImageUrl(imageUrl)
        },
      })
    }
  }

  useEffect(() => {
    setMode(shop !== undefined ? "edit" : "create")
  }, [shop])

  useEffect(() => {
    if (shop !== undefined) {
      form.setFieldsValue({
        phone: shop.phone || "",
        address: shop.address || "",
        email: shop.email || "",
        description: shop.description || "",
        schedule: shop.schedule || "",
        image: shop.image || "",
        coordinates: coordsString,
        is_active: shop.is_active,
      })
    }
    setImageUrl(null)
    setFileList([])
  }, [form, shop, coordsString])

  return (
    <>
      <Form
        form={form}
        name="basic"
        layout={"vertical"}
        autoComplete="off"
        onFinish={(values) => {
          console.log("values ", values)
          const splitCoords = values.coordinates
            ?.split(",")
            .map((item) => item.replaceAll(" ", ""))
          const coords: { latitude?: string; longitude?: string } = {
            latitude: (splitCoords || [])[0] || "",
            longitude: (splitCoords || [])[1] || "",
          }

          if (mode === "edit") {
            if (!!shop?.uuid) {
              edit({
                uuid: shop.uuid,
                coordinates: { ...coords },
                description: values.description,
                email: values.email,
                phone: values.phone,
                address: values.address,
                schedule: values.schedule,
                is_active: values.is_active,
                image: values.image,
              })
            }
          }
        }}
      >
        <Form.Item
          label={"Отображать на сайте"}
          name={"is_active"}
          valuePropName={"checked"}
        >
          <Switch />
        </Form.Item>
        <Form.Item
          label="Адрес"
          name={"address"}
          rules={[
            {
              required: true,
              message: "Поле обязательно для заполнения",
            },
          ]}
        >
          <Input type={"text"} />
        </Form.Item>
        <Form.Item label="E-mail" name={"email"}>
          <Input type={"email"} />
        </Form.Item>
        <Form.Item
          label="Телефон"
          name="phone"
          rules={[
            {
              required: true,
              message: "Поле обязательно для заполнения",
            },
          ]}
        >
          <MaskInput
            type={"text"}
            mask={MASK_PHONE}
            maskPlaceholder={"_"}
            alwaysShowMask
          />
        </Form.Item>
        <Form.Item
          label={"Широта и долгота"}
          name={"coordinates"}
          rules={[
            {
              required: true,
              message: "Поле обязательно для заполнения",
            },
            {
              validator(_, value: string) {
                const splitCoords = (value || "")
                  .split(",")
                  .map((item) => item.replaceAll(" ", ""))
                  .filter((item) => !!item && item.length > 0)
                if (!!splitCoords && splitCoords.length === 2) {
                  return Promise.resolve()
                }
                return Promise.reject(new Error("Не верный формат координат"))
              },
            },
          ]}
        >
          <Input />
        </Form.Item>
        <Form.Item label="Описание" name={"description"}>
          <Input.TextArea />
        </Form.Item>
        <Form.Item label="График работы" name={"schedule"}>
          <Input.TextArea />
        </Form.Item>

        <Form.Item label={"Изображение магазина"} name={"image"}>
          <Upload
            action={`${BASE_URL}${BASE_VERSION_URL}/mock`}
            listType="picture"
            maxCount={1}
            method={"POST"}
            withCredentials={false}
            supportServerRender={true}
            onChange={handleChange}
            fileList={fileList}
          >
            <Button icon={<UploadOutlined />}>Загрузить изображение</Button>
          </Upload>
        </Form.Item>

        <br />

        {errorMessage && (
          <>
            <ErrorMessage>{errorMessage}</ErrorMessage>
          </>
        )}

        <Form.Item>
          <Button type="primary" htmlType="submit" loading={isLoadingEdit}>
            {mode === "create" && "Создать"}
            {mode === "edit" && "Сохранить изменения"}
          </Button>
        </Form.Item>
      </Form>
    </>
  )
}
