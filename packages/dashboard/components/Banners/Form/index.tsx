import { FC, useCallback, useEffect, useState } from "react"
import { RequestBanner } from "../../../../contracts/contracts"
import {
  AutoComplete,
  Button,
  Form,
  Input,
  InputNumber,
  Select,
  Space,
  Upload,
} from "antd"
import { ErrorMessage } from "../../Messages/ErrorMessage"
import { useBanner } from "../../../hooks/banner"
import { UploadFile } from "antd/lib/upload/interface"
import { UploadChangeParam } from "antd/lib/upload"
import { getBase64 } from "utils/helpers"
import {
  DesktopOutlined,
  MobileOutlined,
  TabletOutlined,
  UploadOutlined,
} from "@ant-design/icons"
import { SelectValueType } from "../../../types/types"
import { IconText } from "../../IconText"
import { BASE_URL, BASE_VERSION_URL } from "../../../utils/constants"

const { Option } = Select

const BannerTypeValues: SelectValueType[] = [
  {
    value: 1,
    label: "слайдер главная",
  },
  {
    value: 2,
    label: "одиночный главная",
  },
  {
    value: 3,
    label: "одиночный в списке товаров",
  },
  {
    value: 4,
    label: "одиночный в каталоге под товарами",
  },
]

export const BannerForm: FC<{
  banner?: RequestBanner & { id?: number }
}> = ({ banner }) => {
  const [form] = Form.useForm<
    Omit<RequestBanner, "type"> & { type?: SelectValueType }
  >()
  const [mode, setMode] = useState<"create" | "edit">(
    banner !== undefined ? "edit" : "create",
  )
  const { create, edit, isLoadingEdit, errorMessage, isLoadingCreate } =
    useBanner()
  const [error, setError] = useState<undefined | string>(undefined)

  useEffect(() => {
    setMode(banner !== undefined ? "edit" : "create")
  }, [banner])

  const [imageDesktopUrl, setImageDesktopUrl] = useState<string | null>(null)
  const [fileDesktopList, setFileDesktopList] = useState<UploadFile[]>([])

  const [imageTabletUrl, setImageTabletUrl] = useState<string | null>(null)
  const [fileTabletList, setFileTabletList] = useState<UploadFile[]>([])

  const [imageMobileUrl, setImageMobileUrl] = useState<string | null>(null)
  const [fileMobileList, setFileMobileList] = useState<UploadFile[]>([])

  const setImageValueField = useCallback(
    (fieldForm: "desktop" | "tablet" | "mobile", value: string | null) => {
      form.setFieldsValue({
        [fieldForm]: value !== null ? value?.split(",")[1] : undefined,
      })
    },
    [form],
  )

  useEffect(() => {
    if (!!banner) {
      form.setFieldsValue({
        ...banner,
        type: BannerTypeValues.find((b) => b.value === banner.type),
      })
      if (!!banner.desktop) {
        setImageDesktopUrl(banner.desktop)
        setFileDesktopList([
          {
            status: "done",
            url: `https://${banner.desktop}`,
            uid: `https://${banner.desktop}`,
            name: "Баннер ПК",
            thumbUrl: `https://${banner.desktop}`,
          },
        ])
      }
      if (!!banner.tablet) {
        setImageTabletUrl(banner.tablet)
        setFileTabletList([
          {
            status: "done",
            url: `https://${banner.tablet}`,
            uid: `https://${banner.tablet}`,
            name: "Баннер планшет",
            thumbUrl: `https://${banner.tablet}`,
          },
        ])
      }
      if (!!banner.mobile) {
        setImageMobileUrl(banner.mobile)
        setFileMobileList([
          {
            status: "done",
            url: `https://${banner.mobile}`,
            uid: `https://${banner.mobile}`,
            name: "Баннер мобильный",
            thumbUrl: `https://${banner.mobile}`,
          },
        ])
      }
    }
  }, [form, banner])

  useEffect(() => {
    setImageValueField("desktop", imageDesktopUrl)
  }, [form, imageDesktopUrl, setImageValueField])

  useEffect(() => {
    setImageValueField("tablet", imageTabletUrl)
  }, [form, imageTabletUrl, setImageValueField])

  useEffect(() => {
    setImageValueField("mobile", imageMobileUrl)
  }, [form, imageMobileUrl, setImageValueField])

  const handleChange = (
    info: UploadChangeParam,
    setterUrlMethod: (val: string | null) => void,
    setterListMethod: (val: UploadFile[]) => void,
  ) => {
    setterListMethod(info.fileList)
    if (info.file.status === "removed") {
      setterUrlMethod(null)
      return
    }
    if (info.file.status === "error") {
      console.log("error file")
      return
    }
    if (info.file.status === "uploading") {
      console.log("uploading file")
      return
    }
    if (info.file.status === "done") {
      // Get this url from response in real world.
      console.log("done file")
      getBase64({
        img: info.file.originFileObj,
        callback: (imageUrl: string | null) => {
          console.log("callback image ", imageUrl)
          setterUrlMethod(imageUrl)
        },
      })
    }
  }

  return (
    <>
      <Form
        form={form}
        name="basic"
        layout={"vertical"}
        autoComplete="off"
        onFinish={(values) => {
          console.log("values ", values)

          if (!imageDesktopUrl || !values.desktop) {
            setError("Загрузите как минимум изображение для Пк")
            return
          }
          setError(undefined)
          if (mode === "create") {
            create({
              ...values,
              type: !!values.type?.value
                ? (+values.type?.value as 1 | 2 | 3 | 4)
                : undefined,
            })
          }
          if (mode === "edit") {
            if (banner?.id) {
              edit({
                ...values,
                type: !!values.type?.value
                  ? (+values.type?.value as 1 | 2 | 3 | 4)
                  : undefined,
                id: banner.id,
              })
            }
          }
        }}
      >
        <Form.Item
          label="Url"
          name={"url"}
          rules={[
            {
              required: true,
              message: "Поле обязательно для заполнения",
            },
          ]}
        >
          <AutoComplete
            dropdownMatchSelectWidth={500}
            options={[
              {
                label: "Не нашли нужного",
                value: "#notfindneed",
              },
            ]}
          >
            <Input type={"text"} />
          </AutoComplete>
        </Form.Item>

        <Form.Item
          label={
            <IconText icon={DesktopOutlined} text={"Изображение для ПК"} />
          }
          name={"desktop"}
        >
          <Upload
            action={`${BASE_URL}${BASE_VERSION_URL}/mock`}
            listType="picture"
            maxCount={1}
            method={"POST"}
            withCredentials={false}
            supportServerRender={true}
            onChange={(info) => {
              handleChange(info, setImageDesktopUrl, setFileDesktopList)
            }}
            fileList={fileDesktopList}
          >
            <Button icon={<UploadOutlined />}>Загрузить изображение</Button>
          </Upload>
        </Form.Item>

        <Form.Item
          label={
            <IconText icon={TabletOutlined} text={"Изображение для планшета"} />
          }
          name={"tablet"}
        >
          <Upload
            action={`${BASE_URL}${BASE_VERSION_URL}/mock`}
            listType="picture"
            maxCount={1}
            method={"POST"}
            withCredentials={false}
            supportServerRender={true}
            onChange={(info) => {
              handleChange(info, setImageTabletUrl, setFileTabletList)
            }}
            fileList={fileTabletList}
          >
            <Button icon={<UploadOutlined />}>Загрузить изображение</Button>
          </Upload>
        </Form.Item>

        <Form.Item
          label={
            <IconText icon={MobileOutlined} text={"Изображение для телефона"} />
          }
          name={"mobile"}
        >
          <Upload
            action={`${BASE_URL}${BASE_VERSION_URL}/mock`}
            listType="picture"
            maxCount={1}
            method={"POST"}
            withCredentials={false}
            supportServerRender={true}
            onChange={(info) => {
              handleChange(info, setImageMobileUrl, setFileMobileList)
            }}
            fileList={fileMobileList}
          >
            <Button icon={<UploadOutlined />}>Загрузить изображение</Button>
          </Upload>
        </Form.Item>

        <Form.Item
          label="Тип"
          name={"type"}
          rules={[
            {
              required: true,
              message: "Поле обязательно для заполнения",
            },
          ]}
        >
          <Select
            labelInValue
            onChange={(value: SelectValueType) => {
              console.log("value ", value)
              form.setFieldsValue({
                type: { ...value },
              })
            }}
          >
            {BannerTypeValues.map((v, i) => (
              <Option key={i} value={v.value}>
                {v.label}
              </Option>
            ))}
          </Select>
        </Form.Item>

        <Form.Item
          label="Приоритет отображения"
          name={"weight"}
          help="Порядок вывода баннера. Чем меньше значение, тем раньше выводятся"
          initialValue={0}
        >
          <InputNumber />
        </Form.Item>
        <br />

        <Form.Item>
          <Space>
            <Button
              type="primary"
              htmlType="submit"
              loading={isLoadingEdit || isLoadingCreate}
              disabled={isLoadingEdit || isLoadingCreate}
            >
              {mode === "create" && "Создать"}
              {mode === "edit" && "Сохранить изменения"}
            </Button>
          </Space>
        </Form.Item>

        {errorMessage && (
          <>
            <ErrorMessage>{errorMessage}</ErrorMessage>
          </>
        )}
        {error && (
          <>
            <ErrorMessage>{error}</ErrorMessage>
          </>
        )}
      </Form>
    </>
  )
}
