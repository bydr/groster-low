import { FC } from "react"
import { useQuery } from "react-query"
import { bannersAPI } from "../../api/bannersAPI"
import { useAppDispatch } from "../../hooks/redux"
import { bannersSlice } from "../../store/reducers/bannersSlice"
import { BannerList } from "./List"

export const Banners: FC = () => {
  const dispatch = useAppDispatch()
  const { setBanners } = bannersSlice.actions
  useQuery("banners", () => bannersAPI.getAdminBanners(), {
    onSuccess: (response) => {
      console.log("banners ", response)
      dispatch(setBanners(response || []))
    },
  })
  return (
    <>
      <BannerList />
    </>
  )
}
