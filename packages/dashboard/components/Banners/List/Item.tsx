import { FC } from "react"
import { Button, List, Popconfirm } from "antd"
import { RequestBanner } from "../../../../contracts/contracts"
import { EntityImage } from "../../EntityImage/EntityImage"
import { BannerImageItem, BannerImages } from "./StyledBannerItem"
import { IconText } from "../../IconText"
import {
  DesktopOutlined,
  MobileOutlined,
  TabletOutlined,
} from "@ant-design/icons"
import { useBanner } from "../../../hooks/banner"

export const BannerItem: FC<{
  banner?: RequestBanner & { id?: number }
  showModal: (banner?: RequestBanner & { id?: number }) => void
}> = ({ banner, showModal }) => {
  const { isLoadingRemove, removeConfirm } = useBanner()
  return (
    <>
      <List.Item
        actions={[
          <Button
            type={"link"}
            key={"edit"}
            onClick={() => {
              showModal(banner)
            }}
          >
            Редактировать
          </Button>,
          <Popconfirm
            key={"remove"}
            title="Вы действительно хотите удалить баннер?"
            okText={"Удалить"}
            cancelText={"Отмена"}
            visible={removeConfirm.visible}
            onConfirm={() => {
              removeConfirm.handleOk(banner?.id)
            }}
            okButtonProps={{
              loading: isLoadingRemove,
              type: "primary",
              danger: true,
            }}
            onCancel={removeConfirm.handleCancel}
          >
            <Button type={"link"} danger onClick={removeConfirm.showPopup}>
              Удалить
            </Button>
          </Popconfirm>,
        ]}
      >
        <List.Item.Meta
          title={`Баннер ${banner?.id}`}
          description={
            <>
              <BannerImages>
                <BannerImageItem>
                  <IconText icon={DesktopOutlined} text={"Пк"} />
                  <EntityImage
                    imagePath={banner?.desktop}
                    width={200}
                    height={100}
                    quality={50}
                    objectPosition={"left"}
                  />
                </BannerImageItem>
                <BannerImageItem>
                  <IconText icon={TabletOutlined} text={"Планшет"} />
                  <EntityImage
                    imagePath={banner?.tablet}
                    width={200}
                    height={100}
                    quality={50}
                    objectPosition={"left"}
                  />
                </BannerImageItem>
                <BannerImageItem>
                  <IconText icon={MobileOutlined} text={"Телефон"} />
                  <EntityImage
                    imagePath={banner?.mobile}
                    width={200}
                    height={100}
                    quality={50}
                    objectPosition={"left"}
                  />
                </BannerImageItem>
              </BannerImages>
            </>
          }
        />
      </List.Item>
    </>
  )
}
