import { FC, useCallback, useState } from "react"
import { Loader } from "../../Loader/Loader"
import { Button, Empty, List, Popconfirm } from "antd"
import { StyledListWrapper } from "../../../styles/globals"
import { useAppSelector } from "../../../hooks/redux"
import { BannerItem } from "./Item"
import { BannerForm } from "../Form"
import Modal from "antd/lib/modal/Modal"
import { useBanner } from "../../../hooks/banner"
import { RequestBanner } from "../../../../contracts/contracts"

export const BannerList: FC<{ loading?: boolean }> = ({ loading }) => {
  const banners = useAppSelector((state) => state.banners.banners)
  const { removeConfirm, isLoadingRemove } = useBanner()
  const [isModalVisible, setIsModalVisible] = useState(false)
  const [currentBanner, setCurrentBanner] = useState<
    null | (RequestBanner & { id?: number })
  >(null)

  const showModal = useCallback(() => {
    setIsModalVisible(true)
  }, [])

  const cancelHandle = () => {
    setIsModalVisible(false)
    setCurrentBanner(null)
  }

  return (
    <>
      <Button type={"primary"} onClick={showModal}>
        Создать новый
      </Button>
      <StyledListWrapper>
        {loading && <Loader />}
        {banners !== null && (
          <>
            {banners.length > 0 ? (
              <>
                <List
                  itemLayout="vertical"
                  dataSource={[...banners]}
                  renderItem={(item) => (
                    <BannerItem
                      banner={item}
                      showModal={(banner) => {
                        setCurrentBanner(banner || null)
                        showModal()
                      }}
                    />
                  )}
                />
                <Modal
                  key={"edit"}
                  title={
                    currentBanner === null
                      ? "Создание баннера"
                      : "Редактирование баннера"
                  }
                  visible={isModalVisible}
                  cancelText={"Отмена"}
                  onCancel={cancelHandle}
                  destroyOnClose={true}
                  footer={[
                    <Button key="back" onClick={cancelHandle}>
                      Закрыть
                    </Button>,
                    <>
                      {currentBanner !== null && (
                        <>
                          <Popconfirm
                            key={"remove"}
                            title="Вы действительно хотите удалить баннер?"
                            okText={"Удалить"}
                            cancelText={"Отмена"}
                            visible={removeConfirm.visible}
                            onConfirm={() => {
                              removeConfirm.handleOk(currentBanner?.id)
                            }}
                            okButtonProps={{
                              loading: isLoadingRemove,
                              type: "primary",
                              danger: true,
                            }}
                            onCancel={removeConfirm.handleCancel}
                          >
                            <Button danger onClick={removeConfirm.showPopup}>
                              Удалить
                            </Button>
                          </Popconfirm>
                        </>
                      )}
                    </>,
                  ]}
                >
                  <BannerForm banner={currentBanner || undefined} />
                </Modal>
              </>
            ) : (
              <>
                <Empty
                  image={Empty.PRESENTED_IMAGE_SIMPLE}
                  description={"Баннеры не найдены"}
                />
              </>
            )}
          </>
        )}
      </StyledListWrapper>
    </>
  )
}
