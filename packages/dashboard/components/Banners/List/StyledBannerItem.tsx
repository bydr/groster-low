import { styled } from "@linaria/react"
import { StyledEntityImage } from "../../EntityImage/Styled"
import { StyledTextIcon } from "../../IconText/StyledIconText"

export const BannerImageItem = styled.div`
  display: flex;
  flex-direction: column;

  ${StyledTextIcon} {
    margin-bottom: 8px;
  }
`

export const BannerImages = styled.div`
  display: flex;
  flex-direction: row;
  gap: 8px;

  ${StyledEntityImage} {
    margin: 0;
    width: auto;
  }
`
