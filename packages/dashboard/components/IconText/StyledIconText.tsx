import { css } from "@linaria/core"
import { Space } from "antd"
import { styled } from "@linaria/react"
import { colors } from "../../styles/vars"

export const cssIsSuccess = css``
export const StyledTextIcon = styled(Space)`
  &.${cssIsSuccess} {
    color: ${colors.green};
  }
`
