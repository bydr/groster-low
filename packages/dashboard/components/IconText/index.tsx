import type { FC, ReactNode } from "react"
import { createElement } from "react"
import { cssIsSuccess, StyledTextIcon } from "./StyledIconText"
import { cx } from "@linaria/core"

export const IconText: FC<{
  icon: string | FC
  text: ReactNode
  isSuccess?: boolean
}> = ({ icon, text, isSuccess }) => (
  <StyledTextIcon className={cx(isSuccess && cssIsSuccess)}>
    {createElement(icon)}
    {text}
  </StyledTextIcon>
)
