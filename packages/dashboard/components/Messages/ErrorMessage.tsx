import { StyledError } from "./StyledMessages"
import { FC } from "react"

export const ErrorMessage: FC = ({ children }) => {
  return <StyledError type={"danger"}>{children}</StyledError>
}
