import { styled } from "@linaria/react"
import { Typography } from "antd"

export const StyledError = styled(Typography.Text)`
  display: inline-block;
  margin-bottom: 24px;
  margin-top: 24px;
  font-weight: 500;
`
