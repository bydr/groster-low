import React from "react"
import { notification } from "antd"
import { CheckCircleOutlined, CloseCircleOutlined } from "@ant-design/icons"

export const openNotification = ({
  message,
  description,
  iconVariant,
}: {
  message?: string
  description?: React.ReactNode
  iconVariant: "error" | "success"
}) => {
  notification.open({
    message: message,
    description: description,
    icon:
      iconVariant === "success" ? (
        <CheckCircleOutlined style={{ color: `#26AA6B` }} />
      ) : (
        <CloseCircleOutlined style={{ color: `#FF003D` }} />
      ),
  })
}
