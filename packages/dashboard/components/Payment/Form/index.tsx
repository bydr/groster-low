import { FC, useEffect, useState } from "react"
import { Button, Form, Input, InputNumber, Space, Switch } from "antd"
import { ErrorMessage } from "../../Messages/ErrorMessage"
import {
  AdminPaymentMethod,
  PaymentMethodCreateRequest,
} from "../../../../contracts/contracts"
import { usePayment } from "../../../hooks/payment"

export const PaymentForm: FC<{ payment?: AdminPaymentMethod }> = ({
  payment,
}) => {
  const [form] = Form.useForm<PaymentMethodCreateRequest>()
  const [mode, setMode] = useState<"create" | "edit">(
    payment !== undefined ? "edit" : "create",
  )
  const { create, edit, isLoadingEdit, errorMessage } = usePayment({
    payment: payment,
  })

  useEffect(() => {
    setMode(payment !== undefined ? "edit" : "create")
  }, [payment])

  useEffect(() => {
    if (!!payment) {
      form.setFieldsValue({ ...payment })
    }
  }, [form, payment])

  return (
    <>
      <Form
        form={form}
        name="basic"
        layout={"vertical"}
        autoComplete="off"
        onFinish={(values) => {
          console.log("values ", values)
          if (mode === "create") {
            create({
              ...values,
            })
          }
          if (mode === "edit") {
            if (payment?.uid) {
              edit({
                ...values,
                uid: payment.uid,
              })
            }
          }
        }}
      >
        <Form.Item
          label={"Отображать на сайте"}
          name={"is_active"}
          valuePropName={"checked"}
        >
          <Switch />
        </Form.Item>
        <Form.Item
          label="Название"
          name={"name"}
          rules={[
            {
              required: true,
              message: "Поле обязательно для заполнения",
            },
          ]}
        >
          <Input type={"text"} />
        </Form.Item>
        <Form.Item
          label="Приоритет отображения"
          name={"weight"}
          help="Чем больше значение, тем приоритетнее отображение пункта на странице"
          initialValue={0}
        >
          <InputNumber />
        </Form.Item>
        <br />

        <Form.Item>
          <Space>
            <Button type="primary" htmlType="submit" loading={isLoadingEdit}>
              {mode === "create" && "Создать"}
              {mode === "edit" && "Сохранить изменения"}
            </Button>
          </Space>
        </Form.Item>

        {errorMessage && (
          <>
            <ErrorMessage>{errorMessage}</ErrorMessage>
          </>
        )}
      </Form>
    </>
  )
}
