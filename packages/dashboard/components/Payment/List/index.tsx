import { FC } from "react"
import { PaymentMethodItem } from "./Item"
import { Empty, List } from "antd"
import { useAppSelector } from "../../../hooks/redux"
import { Loader } from "../../Loader/Loader"
import { StyledListWrapper } from "../../../styles/globals"

export const PaymentList: FC<{ loading?: boolean }> = ({ loading }) => {
  const paymentsMethods = useAppSelector((state) => state.payments.methods)

  return (
    <>
      <StyledListWrapper>
        {loading && <Loader />}
        {paymentsMethods !== null && (
          <>
            {Object.keys(paymentsMethods).length > 0 ? (
              <>
                <List
                  itemLayout="vertical"
                  dataSource={Object.keys(paymentsMethods)
                    .map((key) => paymentsMethods[key])
                    .sort((a, b) => {
                      return (a?.name || "") > (b.name || "")
                        ? 1
                        : (a.name || "") < (b.name || "")
                        ? -1
                        : 0
                    })
                    .sort(function (x, y) {
                      return x.is_active === y.is_active
                        ? 0
                        : x.is_active
                        ? -1
                        : 1
                    })}
                  renderItem={(item) => (
                    <PaymentMethodItem paymentMethod={item} />
                  )}
                />
              </>
            ) : (
              <>
                <Empty
                  image={Empty.PRESENTED_IMAGE_SIMPLE}
                  description={"Методы оплаты не найдены"}
                />
              </>
            )}
          </>
        )}
      </StyledListWrapper>
    </>
  )
}
