import { FC } from "react"
import { CustomLink } from "../../Link/Link"
import { ROUTES } from "../../../utils/constants"
import { Form, List, Switch } from "antd"
import { AdminPaymentMethod } from "../../../../contracts/contracts"
import { usePayment } from "../../../hooks/payment"
import { StyledListDescription } from "../../../styles/globals"

export const PaymentMethodItem: FC<{
  paymentMethod: AdminPaymentMethod
}> = ({ paymentMethod }) => {
  const { enable, disable, isLoadingEdit } = usePayment({
    payment: paymentMethod,
  })
  return (
    <>
      <List.Item
        actions={[
          <CustomLink
            key={"edit"}
            href={`${ROUTES.payments}/${paymentMethod.uid}`}
          >
            Подробнее
          </CustomLink>,
        ]}
      >
        <List.Item.Meta
          title={paymentMethod.name || "Без имени"}
          description={
            <>
              <StyledListDescription>
                <Form.Item label={"Отображать на сайте"}>
                  <Switch
                    checked={paymentMethod.is_active}
                    onChange={(checked) => {
                      if (!checked) {
                        disable()
                      } else {
                        enable()
                      }
                    }}
                    loading={isLoadingEdit}
                  />
                </Form.Item>
              </StyledListDescription>
            </>
          }
        ></List.Item.Meta>
      </List.Item>
    </>
  )
}
