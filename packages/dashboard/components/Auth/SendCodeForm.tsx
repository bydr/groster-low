import { FC } from "react"
import { Button, Form, Typography } from "antd"
import type { RequestSendCode } from "../../../contracts/contracts"
import { MaskInput } from "../MaskInput"
import { MASK_PHONE } from "../../utils/constants"

export const SendCodeForm: FC<{
  sendMutate: (data: RequestSendCode) => void
  isLoading?: boolean
  error?: string
}> = ({ sendMutate, isLoading, error }) => {
  const { Text } = Typography

  const [form] = Form.useForm<RequestSendCode>()

  return (
    <>
      <Form
        form={form}
        name="basic"
        layout={"vertical"}
        autoComplete="off"
        onFinish={(values) => {
          if (!!values) {
            sendMutate({
              contact: values.contact,
            })
          }
        }}
      >
        <Form.Item
          label="Телефон"
          name="contact"
          rules={[{ required: true, message: "Заполните поле!" }]}
        >
          <MaskInput
            type={"text"}
            mask={MASK_PHONE}
            placeholder={"+7 (___) ___ __-__"}
            maskPlaceholder={"_"}
            alwaysShowMask
          />
        </Form.Item>

        <Form.Item>
          <Button type="primary" htmlType="submit" loading={isLoading}>
            Получить код
          </Button>
        </Form.Item>

        {error && (
          <>
            <Text type={"danger"}>{error}</Text>
          </>
        )}
      </Form>
    </>
  )
}
