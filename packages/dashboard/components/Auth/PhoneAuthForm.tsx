import type { FC } from "react"
import { useEffect, useState } from "react"
import { Button, Form, Typography } from "antd"
import { useAuth } from "../../hooks/auth"
import { authAPI } from "../../api/authAPI"
import { useMutation } from "react-query"
import { ApiError, LoginPhoneDataRequest } from "../../../contracts/contracts"
import { MaskInput } from "../MaskInput"
import { MASK_PHONE } from "../../utils/constants"

export const PhoneAuthForm: FC<{
  initialPhone?: string
  lifeTime: number
  timeFormatMinutes: string
  resendHandler: () => void
}> = ({ initialPhone, lifeTime, timeFormatMinutes, resendHandler }) => {
  const { Text } = Typography
  const { login } = useAuth()
  const { loginPhone } = authAPI
  const [error, setError] = useState<string | undefined>()

  const [form] = Form.useForm<LoginPhoneDataRequest>()

  useEffect(() => {
    form.setFieldsValue({
      phone: initialPhone,
    })
  }, [form, initialPhone])

  const { mutate: loginMutate, isLoading } = useMutation(loginPhone, {
    onSuccess: (result, request) => {
      login({
        phone: request.phone,
        accessToken: result.access_token || null,
        refreshToken: result.refresh_token || null,
        cart: result.info?.cart || null,
        isAdmin: result.info?.is_admin,
        fio: result.info?.fio,
      })
    },
    onError: (error: ApiError) => {
      setError(error.message || "Произошла ошибка!")
    },
    onMutate: () => {
      setError(undefined)
    },
  })

  return (
    <>
      <Form
        form={form}
        name="basic"
        layout={"vertical"}
        autoComplete="off"
        onFinish={(values) => {
          if (!!values) {
            loginMutate({
              phone: values.phone,
              code: values.code,
            })
          }
        }}
      >
        <Form.Item
          label="Телефон"
          name="phone"
          rules={[{ required: true, message: "Заполните поле!" }]}
        >
          <MaskInput
            type={"text"}
            mask={MASK_PHONE}
            maskPlaceholder={"_"}
            alwaysShowMask
            disabled={!!initialPhone}
          />
        </Form.Item>

        <Form.Item
          label="Код"
          name="code"
          rules={[
            { required: true, message: "Заполните поле!" },
            {
              min: 6,
              message: "Длина кода 6 символов",
            },
            {
              max: 6,
              message: "Длина кода 6 символов",
            },
          ]}
        >
          <MaskInput
            type={"text"}
            mask={"999999"}
            maskPlaceholder={"_"}
            alwaysShowMask
          />
        </Form.Item>

        <Button
          type={"ghost"}
          htmlType={"button"}
          disabled={lifeTime > 0}
          onClick={(e) => {
            e.preventDefault()
            resendHandler()
          }}
        >
          {lifeTime > 0
            ? `Повтор через ${timeFormatMinutes}`
            : "Отправить повторно"}
        </Button>
        <br />
        <Form.Item>
          <Button type="primary" htmlType="submit" loading={isLoading}>
            Войти
          </Button>
        </Form.Item>

        {error && (
          <>
            <Text type={"danger"}>{error}</Text>
          </>
        )}
      </Form>
    </>
  )
}
