import type { FC } from "react"
import { useState } from "react"
import { Button, Form, Input, Typography } from "antd"
import { useAuth } from "../../hooks/auth"
import { ApiError, LoginEmailDataRequest } from "../../../contracts/contracts"
import { useMutation } from "react-query"
import { authAPI } from "../../api/authAPI"

export const EmailAuthForm: FC = () => {
  const { Text } = Typography
  const { login } = useAuth()
  const { loginEmail } = authAPI
  const [error, setError] = useState<string | undefined>()

  const [form] = Form.useForm<LoginEmailDataRequest>()

  const { mutate: loginMutate, isLoading } = useMutation(loginEmail, {
    onSuccess: (result, request) => {
      if (!!result.info?.is_admin) {
        login({
          email: request.email,
          accessToken: result.access_token || null,
          refreshToken: result.refresh_token || null,
          cart: result.info?.cart || null,
          isAdmin: result.info?.is_admin,
          fio: result.info?.fio,
        })
      } else {
        setError("Недостаточно прав!")
      }
    },
    onError: (error: ApiError) => {
      setError(error.message || "Произошла ошибка!")
    },
    onMutate: () => {
      setError(undefined)
    },
  })

  return (
    <Form
      form={form}
      name="basic"
      layout={"vertical"}
      autoComplete="off"
      onFinish={(values) => {
        if (!!values) {
          loginMutate({
            email: values.email,
            password: values.password,
          })
        }
      }}
    >
      <Form.Item
        label="Email"
        name="email"
        rules={[{ required: true, message: "Заполните поле!" }]}
      >
        <Input type={"email"} />
      </Form.Item>

      <Form.Item
        label="Пароль"
        name="password"
        rules={[{ required: true, message: "Заполните поле!" }]}
      >
        <Input.Password />
      </Form.Item>

      <Form.Item>
        <Button type="primary" htmlType="submit" loading={isLoading}>
          Войти
        </Button>
      </Form.Item>

      {error && (
        <>
          <Text type={"danger"}>{error}</Text>
        </>
      )}
    </Form>
  )
}
