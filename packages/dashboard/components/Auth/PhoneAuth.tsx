import type { FC } from "react"
import { useState } from "react"
import { SendCodeForm } from "./SendCodeForm"
import { useTimer } from "../../hooks/timer"
import { PhoneAuthForm } from "./PhoneAuthForm"
import { useMutation } from "react-query"
import { authAPI } from "../../api/authAPI"
import { ApiError } from "../../../contracts/contracts"

export const PhoneAuth: FC = () => {
  const [isSended, setIsSended] = useState<boolean>(false)
  const { resetTimer, timeFormatMinutes, setLifeTime, lifeTime } = useTimer()
  const { sendCode } = authAPI
  const [error, setError] = useState<string | undefined>()

  const {
    mutate: sendMutate,
    isLoading,
    variables,
  } = useMutation(sendCode, {
    onSuccess: (result) => {
      if (!result?.is_free) {
        setLifeTime(result.lifetime)
        setIsSended(true)
      } else {
        setError("Пользователь не найден!")
        setIsSended(false)
      }
    },
    onError: (error: ApiError) => {
      setError(error.message || "Произошла ошибка!")
    },
    onMutate: () => {
      setError(undefined)
      setIsSended(false)
    },
  })

  return (
    <>
      {isSended ? (
        <PhoneAuthForm
          lifeTime={lifeTime}
          timeFormatMinutes={timeFormatMinutes}
          resendHandler={resetTimer}
          initialPhone={variables?.contact || undefined}
        />
      ) : (
        <SendCodeForm
          sendMutate={sendMutate}
          error={error}
          isLoading={isLoading}
        />
      )}
    </>
  )
}
