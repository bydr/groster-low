import { Col, Radio, Row } from "antd"
import { FC, useState } from "react"
import { EmailAuthForm } from "./EmailAuthForm"
import { PhoneAuth } from "./PhoneAuth"
import { Container } from "../../styles/globals"

export const Auth: FC = () => {
  const [mode, setMode] = useState<"phone" | "email">("phone")

  return (
    <>
      <Container>
        <Row
          justify={"center"}
          align={"middle"}
          gutter={0}
          style={{
            height: "100vh",
          }}
        >
          <Col
            span={24}
            md={{
              span: 8,
            }}
          >
            <Radio.Group
              onChange={() => {
                if (mode === "phone") {
                  setMode("email")
                }
                if (mode === "email") {
                  setMode("phone")
                }
              }}
              value={mode}
              style={{ marginBottom: 8, width: "100%", textAlign: "center" }}
            >
              <Radio.Button value="phone">По телефону</Radio.Button>
              <Radio.Button value="email">По почте</Radio.Button>
            </Radio.Group>

            {mode === "phone" && (
              <>
                <PhoneAuth />
              </>
            )}
            {mode === "email" && (
              <>
                <EmailAuthForm />
              </>
            )}
          </Col>
        </Row>
      </Container>
    </>
  )
}
