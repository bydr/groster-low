import { FC } from "react"
import { CustomLink } from "../../Link/Link"
import { ROUTES } from "../../../utils/constants"
import { Button, List, Popover, Typography } from "antd"
import { CodeResponse } from "../../../../contracts/contracts"
import { StyledListDescription } from "../../../styles/globals"
import { usePromocode } from "../../../hooks/promocode"
import { IconText } from "../../IconText"
import { CheckCircleOutlined, CopyOutlined } from "@ant-design/icons"

export const PromocodeItem: FC<{
  promocode: CodeResponse
}> = ({ promocode }) => {
  const {
    valueAsString,
    startAsString,
    finishAsString,
    reusableAsString,
    copyCode,
    isCopiedCode,
  } = usePromocode({
    promocode,
  })

  return (
    <>
      <List.Item
        actions={[
          <CustomLink
            key={"edit"}
            href={`${ROUTES.promocodes}/${promocode.id}`}
          >
            Подробнее
          </CustomLink>,
          <Popover
            key={"copy"}
            content={
              <IconText
                icon={CheckCircleOutlined}
                text={`Код скопирован`}
                isSuccess
              />
            }
            visible={isCopiedCode}
          >
            <Button
              type={"link"}
              onClick={() => {
                copyCode()
              }}
            >
              <IconText icon={CopyOutlined} text={`Скопировать код`} />
            </Button>
          </Popover>,
        ]}
      >
        <List.Item.Meta
          title={promocode.name || "Без имени"}
          description={
            <>
              <StyledListDescription>
                {valueAsString && (
                  <>
                    <Typography.Text>
                      Скидка: <b>{valueAsString}</b>
                    </Typography.Text>
                  </>
                )}
                <Typography.Text>
                  Действует с <b>{startAsString}</b> по <b>{finishAsString}</b>
                </Typography.Text>
                <Typography.Text>
                  Использование: <b>{reusableAsString}</b>
                </Typography.Text>
              </StyledListDescription>
            </>
          }
        ></List.Item.Meta>
      </List.Item>
    </>
  )
}
