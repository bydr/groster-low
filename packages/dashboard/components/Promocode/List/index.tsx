import { FC } from "react"
import { PromocodeItem } from "./Item"
import { Empty, List } from "antd"
import { useAppSelector } from "../../../hooks/redux"
import { Loader } from "../../Loader/Loader"
import { StyledListWrapper } from "../../../styles/globals"

export const PromocodeList: FC<{ loading?: boolean }> = ({ loading }) => {
  const promocodes = useAppSelector((state) => state.promocodes.promocodes)

  return (
    <>
      <StyledListWrapper>
        {loading && <Loader />}
        {promocodes !== null && (
          <>
            {promocodes.length > 0 ? (
              <>
                <List
                  itemLayout="vertical"
                  dataSource={[...promocodes].sort((a, b) => {
                    return (a?.name || "") > (b.name || "")
                      ? 1
                      : (a.name || "") < (b.name || "")
                      ? -1
                      : 0
                  })}
                  renderItem={(item) => <PromocodeItem promocode={item} />}
                />
              </>
            ) : (
              <>
                <Empty
                  image={Empty.PRESENTED_IMAGE_SIMPLE}
                  description={"Промокодов не найдено"}
                />
              </>
            )}
          </>
        )}
      </StyledListWrapper>
    </>
  )
}
