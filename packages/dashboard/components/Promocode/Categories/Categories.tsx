import { FC, useEffect, useState } from "react"
import { ICategoryTreeItem } from "../../../types/types"
import { CheckboxChangeEvent } from "antd/es/checkbox"
import { useAppDispatch, useAppSelector } from "../../../hooks/redux"
import { cssChildList, StyledCategoryItem } from "../Form/StyledForm"
import { Checkbox } from "antd"
import { StyledCheckboxList } from "../../../styles/globals"
import { promocodesSlice } from "../../../store/reducers/promocodeSlice"

const CategoryItem: FC<{
  category: ICategoryTreeItem
}> = ({ category }) => {
  const categoriesFetched = useAppSelector(
    (state) => state.promocodes.categories?.fetched,
  )
  const [isParent, setIsParent] = useState(false)

  const dispatch = useAppDispatch()
  const { updateCheckedCategories } = promocodesSlice.actions

  const onChangeCategoryHandle = (e: CheckboxChangeEvent) => {
    dispatch(
      updateCheckedCategories({
        categories: [e.target.name || ""],
        checked: e.target.checked,
      }),
    )
  }

  useEffect(() => {
    setIsParent(!category.parent)
  }, [category.parent])

  return (
    <>
      <StyledCategoryItem>
        <Checkbox
          name={category.uuid}
          checked={(categoriesFetched || {})[category.uuid || ""]?.checked}
          onChange={onChangeCategoryHandle}
        >
          {isParent ? (
            <>
              <b>{category.name}</b>
            </>
          ) : (
            <>{category.name}</>
          )}
        </Checkbox>
        {Object.keys(category.children).length > 0 && (
          <>
            <StyledCheckboxList className={cssChildList}>
              {Object.keys(category.children).map((c) => {
                return (
                  <CategoryItem
                    key={c}
                    category={category.children[c] || null}
                  />
                )
              })}
            </StyledCheckboxList>
          </>
        )}
      </StyledCategoryItem>
    </>
  )
}

export const Categories: FC<{
  categories: Record<string, ICategoryTreeItem> | null
}> = ({ categories }) => {
  return (
    <>
      {!!categories && (
        <>
          {Object.keys(categories).map((key) => {
            if (!!categories[key].uuid) {
              return (
                <CategoryItem
                  category={categories[key]}
                  key={categories[key].uuid}
                />
              )
            }
          })}
        </>
      )}
    </>
  )
}
