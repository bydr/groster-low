import { css } from "@linaria/core"
import { styled } from "@linaria/react"

export const cssChildList = css`
  margin-top: 10px;
`

export const cssFieldHidden = css`
  margin: 0;

  .ant-form-item-control-input {
    min-height: initial;
    margin: 0;
  }
`
export const StyledCategoryItem = styled.div`
  display: inline-flex;
  flex-direction: column;
  width: calc(100% / 3);

  & & {
    width: 100%;
  }

  @media (max-width: 768px) {
    width: 100%;
  }
`
