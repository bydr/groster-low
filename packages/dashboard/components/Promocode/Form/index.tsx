import { FC, useEffect, useState } from "react"
import { Button, DatePicker, Form, Input, Select, Switch } from "antd"
import { CodeResponse } from "../../../../contracts/contracts"
import { DATE_FORMAT } from "../../../utils/constants"
import { SelectValueType } from "../../../types/types"
import { useAppDispatch, useAppSelector } from "../../../hooks/redux"
import { useQuery } from "react-query"
import { promocodeAPI } from "../../../api/promocodeAPI"
import { promocodesSlice } from "../../../store/reducers/promocodeSlice"
import { catalogAPI } from "../../../api/catalogAPI"
import { CaretDownOutlined, CaretUpOutlined } from "@ant-design/icons"
import { cssFieldHidden } from "./StyledForm"
import moment, { Moment } from "moment"
import "moment/locale/ru"
import { Categories } from "../Categories/Categories"

const { Option } = Select

const PromocodeTypeValues: SelectValueType[] = [
  {
    value: 1,
    label: "По процентам",
  },
  {
    value: 2,
    label: "В рублях",
  },
]

export const PromocodeForm: FC<{ promocode?: CodeResponse }> = ({
  promocode,
}) => {
  const dispatch = useAppDispatch()
  const { setPromocodes, setCategories, updateCheckedCategories } =
    promocodesSlice.actions
  const promocodes = useAppSelector((state) => state.promocodes.promocodes)
  const categories = useAppSelector(
    (state) => state.promocodes.categories?.tree,
  )
  const categoriesFetched = useAppSelector(
    (state) => state.promocodes.categories?.fetched,
  )
  const [isShowCategories, setIsShowCategories] = useState(false)

  useQuery(
    "promocodes",
    () => (promocodes === null ? promocodeAPI.getPromocodes() : null),
    {
      onSuccess: (response) => {
        dispatch(setPromocodes(response))
      },
    },
  )
  useQuery(
    "categories",
    () => (!categories ? catalogAPI.getCategories() : null),
    {
      onSuccess: (response) => {
        if (!!response) {
          dispatch(setCategories(response?.categories || []))
        }
      },
    },
  )

  const [form] = Form.useForm<
    Omit<CodeResponse, "type" | "start" | "finish"> & {
      type?: SelectValueType
      rangePeriod?: Moment[]
    }
  >()
  const watchTypeValue = Form.useWatch("type", form)

  useEffect(() => {
    if (!!promocode) {
      form.setFieldsValue({
        ...promocode,
        type: PromocodeTypeValues.find((v) => v.value === promocode.type),
        rangePeriod: [
          moment(promocode.start, DATE_FORMAT),
          moment(promocode.finish, DATE_FORMAT),
        ],
        reusable: promocode.reusable || false,
        categories: promocode.categories || [],
        value:
          promocode.value !== undefined
            ? promocode.type === 2
              ? promocode.value / 100
              : promocode.value
            : undefined,
      })
    }
  }, [form, promocode])

  useEffect(() => {
    dispatch(
      updateCheckedCategories({
        categories: promocode?.categories || [],
        checked: true,
      }),
    )
  }, [dispatch, promocode?.categories, updateCheckedCategories, categories])

  useEffect(() => {
    if (categoriesFetched) {
      const checkedKeys = Object.keys(categoriesFetched).filter(
        (key) => !!categoriesFetched[key].checked,
      )
      console.log("checkedKeys ", checkedKeys)
      form.setFieldsValue({
        categories: checkedKeys,
      })
    }
  }, [categoriesFetched, form])

  return (
    <>
      <Form
        form={form}
        name="basic"
        layout={"vertical"}
        autoComplete="off"
        scrollToFirstError={{
          behavior: "smooth",
          scrollMode: "always",
        }}
      >
        <Form.Item
          label="Название"
          name={"name"}
          rules={[
            {
              required: true,
              message: "Поле обязательно для заполнения",
            },
          ]}
        >
          <Input type={"text"} />
        </Form.Item>
        <Form.Item
          label="Код"
          name={"code"}
          rules={[
            {
              required: true,
              message: "Поле обязательно для заполнения",
            },
            {
              validator: (_, value) => {
                if (
                  !(promocodes || []).find((p) => {
                    return p.code === value && p.id !== promocode?.id
                  })
                ) {
                  return Promise.resolve()
                }
                return Promise.reject(
                  new Error("Такой промокод уже существует"),
                )
              },
            },
          ]}
        >
          <Input type={"text"} />
        </Form.Item>

        <Form.Item
          label="Тип"
          name={"type"}
          rules={[
            {
              required: true,
              message: "Поле обязательно для заполнения",
            },
          ]}
        >
          <Select
            labelInValue
            onChange={(value: SelectValueType) => {
              console.log("value ", value)
              form.setFieldsValue({
                type: { ...value },
              })
            }}
          >
            {PromocodeTypeValues.map((v, i) => (
              <Option key={i} value={v.value}>
                {v.label}
              </Option>
            ))}
          </Select>
        </Form.Item>
        <Form.Item
          label="Значение"
          name={"value"}
          rules={[
            {
              required: true,
              message: "Поле обязательно для заполнения",
            },
          ]}
        >
          <Input
            addonBefore={
              watchTypeValue?.value === 1
                ? "%"
                : watchTypeValue?.value === 2
                ? "₽"
                : undefined
            }
            type={"text"}
          />
        </Form.Item>
        <Form.Item
          label={"Многоразовый"}
          name={"reusable"}
          valuePropName={"checked"}
          initialValue={false}
        >
          <Switch />
        </Form.Item>

        <Form.Item
          label={"Период действия"}
          name={"rangePeriod"}
          rules={[
            {
              required: true,
              message: "Поле обязательно для заполнения",
            },
          ]}
        >
          <DatePicker.RangePicker placeholder={["От", "До"]} />
        </Form.Item>
        <Form.Item
          name={"categories"}
          rules={[
            {
              required: true,
              message: "Выберите как минимум 1 категорию",
            },
          ]}
          className={cssFieldHidden}
        >
          <Input
            style={{
              opacity: 0,
              visibility: "hidden",
              position: "absolute",
              minHeight: "initial",
            }}
          />
        </Form.Item>
        <Button
          icon={isShowCategories ? <CaretUpOutlined /> : <CaretDownOutlined />}
          type={!isShowCategories ? "ghost" : "primary"}
          onClick={() => {
            setIsShowCategories(!isShowCategories)
          }}
          style={{
            margin: "0 0 20px 0",
          }}
        >
          {isShowCategories ? "Свернуть" : "Выбрать категории"}
        </Button>
        <br />
        {isShowCategories && (
          <>
            <Categories categories={categories || null} />
          </>
        )}
      </Form>
    </>
  )
}
