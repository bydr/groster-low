import { FC } from "react"
import { Tabs } from "antd"
import { Data, DataPropsType } from "./Data"
import { Limits } from "./Limits"

const { TabPane } = Tabs

export const ShippingMethod: FC<DataPropsType> = ({ alias }) => {
  return (
    <>
      <Tabs defaultActiveKey="1">
        <TabPane tab="Данные" key="1">
          <Data alias={alias} />
        </TabPane>
        <TabPane tab="Ограничения" key="2">
          <Limits methodAlias={alias || ""} />
        </TabPane>
      </Tabs>
    </>
  )
}
