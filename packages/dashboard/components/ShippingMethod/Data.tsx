import { FC, useEffect } from "react"
import { ShippingMethodForm } from "./Form"
import { Empty, Skeleton } from "antd"
import { useApp } from "../../hooks/app"
import { useQuery } from "react-query"
import { shippingMethodsAPI } from "../../api/shippingMethodsAPI"
import { shippingMethodsSlice } from "../../store/reducers/shippingMethodsSlice"
import { useAppDispatch, useAppSelector } from "../../hooks/redux"

export type DataPropsType = {
  alias?: string
}

export const Data: FC<DataPropsType> = ({ alias }) => {
  const { setPageTitle } = useApp()
  const dispatch = useAppDispatch()
  const currentMethod = useAppSelector(
    (state) => state.shippingMethods.current.method,
  )
  const { setCurrentMethod } = shippingMethodsSlice.actions
  const { isLoading } = useQuery(
    ["shippingMethod", alias],
    () =>
      alias !== undefined
        ? shippingMethodsAPI.getShippingMethodByAlias({
            alias: alias,
          })
        : null,
    {
      onSuccess: (response) => {
        dispatch(
          setCurrentMethod({
            ...response,
          }),
        )
      },
    },
  )

  useEffect(() => {
    setPageTitle(currentMethod?.name || "Метод оплаты")
    return () => {
      setPageTitle(null)
    }
  }, [currentMethod, setPageTitle])

  useEffect(() => {
    return () => {
      dispatch(setCurrentMethod(null))
    }
  }, [dispatch, setCurrentMethod])

  return (
    <>
      <Skeleton title active avatar loading={isLoading}>
        {!!currentMethod && (
          <>
            <ShippingMethodForm shippingMethod={currentMethod || undefined} />
          </>
        )}
        {currentMethod === null && (
          <Empty
            image={Empty.PRESENTED_IMAGE_SIMPLE}
            description={"Метод доставки не найден"}
          />
        )}
      </Skeleton>
    </>
  )
}
