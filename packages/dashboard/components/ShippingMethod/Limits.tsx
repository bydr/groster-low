import { FC, useEffect, useState } from "react"
import { useQuery } from "react-query"
import { shippingMethodsAPI } from "../../api/shippingMethodsAPI"
import { useAppDispatch } from "../../hooks/redux"
import { shippingMethodsSlice } from "../../store/reducers/shippingMethodsSlice"
import { LimitList } from "./Limit/List"
import { Button, Modal } from "antd"
import { LimitForm } from "./Limit/Form"

export const Limits: FC<{ methodAlias: string }> = ({ methodAlias }) => {
  const dispatch = useAppDispatch()
  const { setLimits } = shippingMethodsSlice.actions
  const { isLoading } = useQuery(
    "limits",
    () => shippingMethodsAPI.getMethodLimit({ alias: methodAlias }),
    {
      onSuccess: (response) => {
        dispatch(setLimits(response || []))
      },
      onError: () => {
        dispatch(setLimits([]))
      },
    },
  )

  const [isModalVisible, setIsModalVisible] = useState(false)

  const showModal = () => {
    setIsModalVisible(true)
  }

  const handleOk = () => {
    setIsModalVisible(false)
  }

  const handleCancel = () => {
    setIsModalVisible(false)
  }

  useEffect(() => {
    return () => {
      dispatch(setLimits(null))
    }
  }, [dispatch, setLimits])

  return (
    <>
      <Button type={"primary"} onClick={showModal}>
        Создать
      </Button>
      <LimitList loading={isLoading} />
      <Modal
        title="Создание ограничения"
        visible={isModalVisible}
        onOk={handleOk}
        onCancel={handleCancel}
        cancelText={"Отмена"}
        okText={"Ок"}
        destroyOnClose={true}
        footer={[
          <Button key="back" onClick={handleCancel}>
            Закрыть
          </Button>,
        ]}
      >
        <LimitForm />
      </Modal>
    </>
  )
}
