import { FC } from "react"
import { ResponseLimit } from "../../../../../contracts/contracts"
import { CURRENCY } from "../../../../utils/constants"
import { Button, List, Popconfirm, Typography } from "antd"
import { StyledListDescription } from "../../../../styles/globals"
import { useShippingLimit } from "../../../../hooks/shippingLimit"

export const LimitItem: FC<{
  limit: ResponseLimit
  showEditModal: () => void
  methodAlias?: string
  setCurrentLimitId: (id: number | null) => void
}> = ({ limit, showEditModal, setCurrentLimitId, methodAlias }) => {
  const {
    orderMinCostRub,
    shippingCostRub,
    removeConfirm: { handleOk, handleCancel, visible, showPopup },
    isLoadingRemove,
  } = useShippingLimit({
    limit: limit,
    methodAlias: methodAlias,
  })

  return (
    <>
      <List.Item
        actions={[
          <Button
            type={"link"}
            key={"edit"}
            onClick={() => {
              showEditModal()
              setCurrentLimitId(limit.id || null)
            }}
          >
            Редактировать
          </Button>,
          <Popconfirm
            key={"remove"}
            title="Вы действительно хотите удалить ограничение?"
            okText={"Удалить"}
            cancelText={"Отмена"}
            visible={visible}
            onConfirm={() => {
              handleOk()
            }}
            okButtonProps={{
              loading: isLoadingRemove,
              type: "primary",
              danger: true,
            }}
            onCancel={handleCancel}
          >
            <Button type={"link"} danger onClick={showPopup}>
              Удалить
            </Button>
          </Popconfirm>,
        ]}
      >
        <List.Item.Meta
          title={[
            `Мин. сумма заказа ${orderMinCostRub} ${CURRENCY}`,
            `${
              limit.region !== undefined && limit.region.length > 0
                ? `для региона ${limit.region}`
                : ""
            }`,
          ]
            .filter((item) => item !== undefined && item.length > 0)
            .join(" ")}
          description={
            <>
              <StyledListDescription>
                <Typography.Text>{`Мин. сумма заказа: ${orderMinCostRub} ${CURRENCY}`}</Typography.Text>
                <Typography.Text>{`Регион: ${
                  limit.region || "Все"
                }`}</Typography.Text>
                <Typography.Text>{`Стоимость доставки: ${shippingCostRub} ${CURRENCY}`}</Typography.Text>
              </StyledListDescription>
            </>
          }
        />
      </List.Item>
    </>
  )
}
