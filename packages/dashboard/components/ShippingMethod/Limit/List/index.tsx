import { FC, useCallback, useState } from "react"
import { Loader } from "../../../Loader/Loader"
import { Button, Empty, List, Modal, Popconfirm } from "antd"
import { StyledListWrapper } from "../../../../styles/globals"
import { LimitItem } from "./Item"
import { useAppSelector } from "../../../../hooks/redux"
import { LimitForm } from "../Form"
import { useShippingLimit } from "../../../../hooks/shippingLimit"

export const LimitList: FC<{ loading?: boolean }> = ({ loading }) => {
  const limits = useAppSelector((state) => state.shippingMethods.current.limits)
  const currentMethod = useAppSelector(
    (state) => state.shippingMethods.current.method,
  )
  const [isModalVisible, setIsModalVisible] = useState(false)
  const [currentLimitId, setCurrentLimitId] = useState<null | number>(null)
  const { removeConfirm, isLoadingRemove } = useShippingLimit({
    methodAlias: currentMethod?.alias,
    onRemove: () => {
      handleCancel()
    },
  })

  const showModal = useCallback(() => {
    setIsModalVisible(true)
  }, [])

  const handleCancel = () => {
    setIsModalVisible(false)
    setCurrentLimitId(null)
  }

  return (
    <>
      <StyledListWrapper>
        {loading && <Loader />}
        {limits !== null && (
          <>
            {limits.length > 0 ? (
              <>
                <List
                  itemLayout="vertical"
                  dataSource={[...limits].sort((a, b) => {
                    return (a?.id || "") > (b.id || "")
                      ? 1
                      : (a.id || "") < (b.id || "")
                      ? -1
                      : 0
                  })}
                  renderItem={(item) => (
                    <LimitItem
                      limit={item}
                      methodAlias={currentMethod?.alias || undefined}
                      showEditModal={showModal}
                      setCurrentLimitId={setCurrentLimitId}
                    />
                  )}
                />
                <Modal
                  title="Редактирование ограничения"
                  visible={isModalVisible}
                  cancelText={"Отмена"}
                  onCancel={handleCancel}
                  destroyOnClose={true}
                  footer={[
                    <Button key="back" onClick={handleCancel}>
                      Закрыть
                    </Button>,
                    <Popconfirm
                      key={"remove"}
                      title="Вы действительно хотите удалить ограничение?"
                      okText={"Удалить"}
                      cancelText={"Отмена"}
                      visible={removeConfirm.visible}
                      onConfirm={() => {
                        removeConfirm.handleOk(currentLimitId || undefined)
                      }}
                      okButtonProps={{
                        loading: isLoadingRemove,
                        type: "primary",
                        danger: true,
                      }}
                      onCancel={removeConfirm.handleCancel}
                    >
                      <Button danger onClick={removeConfirm.showPopup}>
                        Удалить
                      </Button>
                    </Popconfirm>,
                  ]}
                >
                  <LimitForm
                    limit={
                      currentLimitId !== null
                        ? limits.find((l) => l.id === currentLimitId)
                        : undefined
                    }
                  />
                </Modal>
              </>
            ) : (
              <>
                <Empty
                  image={Empty.PRESENTED_IMAGE_SIMPLE}
                  description={"Ограничения не найдены"}
                />
              </>
            )}
          </>
        )}
      </StyledListWrapper>
    </>
  )
}
