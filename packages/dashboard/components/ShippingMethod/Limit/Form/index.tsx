import { FC, useEffect, useState } from "react"
import { AutoComplete, Button, Form, Input, Space, Typography } from "antd"
import { Limit, ResponseLimit } from "../../../../../contracts/contracts"
import { convertRubToPenny } from "../../../../utils/helpers"
import { useShippingLimit } from "../../../../hooks/shippingLimit"
import { useAppSelector } from "../../../../hooks/redux"
import { ErrorMessage } from "../../../Messages/ErrorMessage"
import { fetchSuggestAddresses } from "../../../../api/dadataAPI"
import { useMutation } from "react-query"
import { useDebounce } from "../../../../hooks/debounce"

type HintType = {
  city_with_type: string | null
  region_with_type: string | null
}

export const LimitForm: FC<{ limit?: ResponseLimit }> = ({ limit }) => {
  const [form] = Form.useForm<Limit>()
  const [mode, setMode] = useState<"create" | "edit">(
    limit !== undefined ? "edit" : "create",
  )
  const currentShippingMethod = useAppSelector(
    (state) => state.shippingMethods.current.method,
  )
  const {
    shippingCostRub,
    orderMinCostRub,
    create,
    edit,
    errorMessage,
    isLoadingEdit,
    isLoadingCreate,
    isLoadingRemove,
  } = useShippingLimit({
    limit: limit,
    methodAlias: currentShippingMethod?.alias,
    onCreate: () => {
      form.resetFields()
    },
  })

  const [inputCity, setInputCity] = useState<string | undefined>()
  const debouncedCity = useDebounce<string>(inputCity || "")
  const [hints, setHints] = useState<HintType[] | null>(null)

  const { mutate: suggestAddressesMutate } = useMutation(
    fetchSuggestAddresses,
    {
      onSuccess: (data) => {
        if (!!data && !!data.suggestions) {
          setHints(
            data.suggestions.map((s) => {
              return {
                city_with_type: s?.data?.city_with_type || null,
                region_with_type: s?.data?.region_with_type || null,
              } as HintType
            }),
          )
        } else {
          // setHints([])
        }
      },
      onError: (error) => {
        console.log("error address ", error)
      },
    },
  )

  useEffect(() => {
    if (!!debouncedCity && debouncedCity.length > 0) {
      suggestAddressesMutate({
        address: debouncedCity,
        fromBound: {
          value: "region",
        },
        toBound: {
          value: "city",
        },
      })
    }
  }, [debouncedCity, suggestAddressesMutate])

  useEffect(() => {
    setMode(limit !== undefined ? "edit" : "create")
  }, [limit])

  useEffect(() => {
    if (!!limit) {
      form.setFieldsValue({
        ...limit,
        order_min_cost: orderMinCostRub,
        shipping_cost: shippingCostRub,
      })
    }
  }, [form, limit, orderMinCostRub, shippingCostRub])
  return (
    <>
      <Form
        form={form}
        name="basic"
        layout={"vertical"}
        autoComplete="off"
        onFinish={(values) => {
          console.log("values ", values)
          if (mode === "create") {
            create({
              shipping_cost: convertRubToPenny(values.shipping_cost),
              order_min_cost: convertRubToPenny(values.order_min_cost),
              region: values.region,
            })
          }
          if (mode === "edit") {
            edit({
              shipping_cost: convertRubToPenny(values.shipping_cost),
              order_min_cost: convertRubToPenny(values.order_min_cost),
              region: values.region,
              id: limit?.id,
            })
          }
        }}
      >
        <Form.Item
          label={"Мин. стоимость заказа для применения ограничения в рублях"}
          name={"order_min_cost"}
          rules={[
            {
              required: true,
              message: "Поле обязательно для заполнения",
            },
            {
              pattern: /^\d+$/,
              message: "Неверное значение",
            },
          ]}
        >
          <Input />
        </Form.Item>
        <Form.Item
          label={`Стоимость доставки в рублях`}
          rules={[
            {
              required: true,
              message: "Поле обязательно для заполнения",
            },
            {
              pattern: /^\d+$/,
              message: "Неверное значение",
            },
          ]}
          name={"shipping_cost"}
        >
          <Input />
        </Form.Item>
        <Form.Item
          label={"Регион применения"}
          name={"region"}
          rules={[
            {
              validator: (_, value) => {
                if (value.length === 0) {
                  return Promise.resolve()
                }
                if (
                  hints !== null &&
                  hints.find((h) => {
                    return (
                      h.region_with_type === value || h.city_with_type === value
                    )
                  })
                ) {
                  return Promise.resolve()
                }
                return Promise.reject(new Error("Выберите из списка"))
              },
            },
          ]}
        >
          <AutoComplete
            options={
              hints !== null
                ? hints.map((h, index) => {
                    return {
                      key: index,
                      value:
                        h.city_with_type !== null
                          ? h.city_with_type
                          : h.region_with_type,
                      label:
                        h.city_with_type !== null ? (
                          <Typography.Text>
                            {h.city_with_type}{" "}
                            {h.region_with_type !== null && (
                              <>
                                <Typography.Text type="secondary">
                                  {h.region_with_type}
                                </Typography.Text>
                              </>
                            )}
                          </Typography.Text>
                        ) : (
                          <Typography>{h.region_with_type}</Typography>
                        ),
                    }
                  })
                : undefined
            }
            onChange={(value) => {
              setInputCity(value)
            }}
          />
        </Form.Item>
        <Form.Item>
          <Space>
            <Button
              type="primary"
              htmlType="submit"
              loading={isLoadingCreate || isLoadingEdit || isLoadingRemove}
            >
              {mode === "create" && "Создать"}
              {mode === "edit" && "Сохранить изменения"}
            </Button>
          </Space>
        </Form.Item>

        {errorMessage && (
          <>
            <ErrorMessage>{errorMessage}</ErrorMessage>
          </>
        )}
      </Form>
    </>
  )
}
