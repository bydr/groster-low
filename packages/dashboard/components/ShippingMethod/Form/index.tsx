import { FC, useEffect, useState } from "react"
import { Button, Form, Input, InputNumber, Space, Switch } from "antd"
import { ErrorMessage } from "../../Messages/ErrorMessage"
import {
  ShippingMethodCreateRequest,
  ShippingMethodDetail,
} from "../../../../contracts/contracts"
import { useShippingMethod } from "../../../hooks/shippingMethod"

export const ShippingMethodForm: FC<{
  shippingMethod?: ShippingMethodDetail
}> = ({ shippingMethod }) => {
  const [form] = Form.useForm<ShippingMethodCreateRequest>()
  const [mode, setMode] = useState<"create" | "edit">(
    shippingMethod !== undefined ? "edit" : "create",
  )
  const { create, edit, isLoadingEdit, isLoadingCreate, errorMessage } =
    useShippingMethod({
      shippingMethod: shippingMethod,
    })

  useEffect(() => {
    setMode(shippingMethod !== undefined ? "edit" : "create")
  }, [shippingMethod])

  useEffect(() => {
    if (!!shippingMethod) {
      form.setFieldsValue({ ...shippingMethod })
    }
  }, [form, shippingMethod])

  return (
    <>
      <Form
        form={form}
        name="basic"
        layout={"vertical"}
        autoComplete="off"
        onFinish={(values) => {
          console.log("values ", values)
          if (mode === "create") {
            create({
              ...values,
            })
          }
          if (mode === "edit") {
            if (shippingMethod?.alias) {
              edit({
                ...values,
                alias: shippingMethod.alias,
              })
            }
          }
        }}
      >
        <Form.Item
          label={"Отображать на сайте"}
          name={"is_active"}
          valuePropName={"checked"}
        >
          <Switch />
        </Form.Item>
        <Form.Item
          label="Алиас"
          name={"alias"}
          help={"уникальный идентификатор метода"}
          rules={[
            {
              required: true,
              message: "Поле обязательно для заполнения",
            },
            {
              pattern: /^[a-zA-Z0-9!@#\$%\^\&*\)\(+=._-]+$/g,
              message: "Неверный формат",
            },
          ]}
        >
          <Input
            type={"text"}
            readOnly={mode === "edit"}
            disabled={mode === "edit"}
          />
        </Form.Item>
        <br />
        <Form.Item
          label="Название"
          name={"name"}
          rules={[
            {
              required: true,
              message: "Поле обязательно для заполнения",
            },
          ]}
        >
          <Input type={"text"} />
        </Form.Item>
        <Form.Item label="Описание" name={"description"}>
          <Input.TextArea />
        </Form.Item>
        <Form.Item
          label="Стоимость"
          name={"cost"}
          help="Стоимость заказа, при которой доставка станет бесплатной (отображается в оформлении заказа)"
        >
          <Input type={"text"} />
        </Form.Item>
        <br />
        <Form.Item
          label="Приоритет отображения"
          name={"weight"}
          help="Чем больше значение, тем приоритетнее отображение пункта на странице"
          initialValue={0}
        >
          <InputNumber />
        </Form.Item>
        <br />
        <Form.Item
          label={"Заполнять адрес при оформлении"}
          name={"use_address"}
          valuePropName={"checked"}
        >
          <Switch />
        </Form.Item>
        <Form.Item
          label={"Быстрая доставка"}
          name={"is_fast"}
          valuePropName={"checked"}
          help={`Указывать иконку быстрой доставки`}
        >
          <Switch />
        </Form.Item>
        <br />
        <Form.Item>
          <Space>
            <Button
              type="primary"
              htmlType="submit"
              loading={isLoadingEdit || isLoadingCreate}
            >
              {mode === "create" && "Создать"}
              {mode === "edit" && "Сохранить изменения"}
            </Button>
          </Space>
        </Form.Item>

        {errorMessage && (
          <>
            <ErrorMessage>{errorMessage}</ErrorMessage>
          </>
        )}
      </Form>
    </>
  )
}
