import { FC } from "react"
import { useAppSelector } from "../../../hooks/redux"
import { Empty, List } from "antd"
import { ShippingMethodItem } from "./Item"
import { Loader } from "../../Loader/Loader"
import { StyledListWrapper } from "styles/globals"

export const ShippingMethodList: FC<{ loading?: boolean }> = ({ loading }) => {
  const shippingMethods = useAppSelector(
    (state) => state.shippingMethods.methods,
  )

  return (
    <>
      <StyledListWrapper>
        {loading && <Loader />}
        {shippingMethods !== null && (
          <>
            {shippingMethods.length > 0 ? (
              <>
                <List
                  itemLayout="vertical"
                  dataSource={[...shippingMethods]
                    .sort((a, b) => {
                      return (a?.name || "") > (b.name || "")
                        ? 1
                        : (a.name || "") < (b.name || "")
                        ? -1
                        : 0
                    })
                    .sort(function (x, y) {
                      return x.is_active === y.is_active
                        ? 0
                        : x.is_active
                        ? -1
                        : 1
                    })}
                  renderItem={(item) => (
                    <ShippingMethodItem shippingMethod={item} />
                  )}
                />
              </>
            ) : (
              <>
                <Empty
                  image={Empty.PRESENTED_IMAGE_SIMPLE}
                  description={"Методы оплаты не найдены"}
                />
              </>
            )}
          </>
        )}
      </StyledListWrapper>
    </>
  )
}
