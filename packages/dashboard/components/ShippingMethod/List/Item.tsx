import { FC } from "react"
import { Form, List, Switch, Typography } from "antd"
import { CustomLink } from "../../Link/Link"
import { CURRENCY, ROUTES } from "../../../utils/constants"
import { AdminDeliveryMethodList } from "../../../../contracts/contracts"
import { useShippingMethod } from "../../../hooks/shippingMethod"
import { StyledListDescription } from "../../../styles/globals"

export const ShippingMethodItem: FC<{
  shippingMethod: AdminDeliveryMethodList
}> = ({ shippingMethod }) => {
  const { enable, disable, isLoadingEdit } = useShippingMethod({
    shippingMethod: shippingMethod,
  })
  return (
    <>
      <List.Item
        actions={[
          <CustomLink
            key={"edit"}
            href={`${ROUTES.shippingMethods}/${shippingMethod.alias}`}
          >
            Подробнее
          </CustomLink>,
        ]}
      >
        <List.Item.Meta
          title={shippingMethod.name || "Без имени"}
          description={
            <>
              <StyledListDescription>
                {shippingMethod.cost && (
                  <>
                    <Typography.Text>
                      Бесплатная доставка при стоимости заказа:{" "}
                      <b>
                        {" "}
                        {shippingMethod.cost} {CURRENCY}
                      </b>
                    </Typography.Text>
                  </>
                )}
                <Form.Item label={"Отображать на сайте"}>
                  <Switch
                    checked={shippingMethod.is_active}
                    onChange={(checked) => {
                      if (!checked) {
                        disable()
                      } else {
                        enable()
                      }
                    }}
                    loading={isLoadingEdit}
                  />
                </Form.Item>
              </StyledListDescription>
            </>
          }
        />
      </List.Item>
    </>
  )
}
