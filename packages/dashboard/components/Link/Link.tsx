import Link, { LinkProps } from "next/link"
import { BaseHTMLAttributes, FC } from "react"

export const CustomLink: FC<
  Omit<LinkProps, "passHref"> & BaseHTMLAttributes<HTMLAnchorElement>
> = ({ children, target, ...props }) => {
  return (
    <>
      <Link passHref {...props}>
        <a target={target}>{children}</a>
      </Link>
    </>
  )
}
