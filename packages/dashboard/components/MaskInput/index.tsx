import { InputProps } from "antd"
import type { BaseHTMLAttributes, FC, InputHTMLAttributes } from "react"
import { memo } from "react"
import InputMask, { Props } from "react-input-mask"

const InputWrapper: FC<
  BaseHTMLAttributes<HTMLInputElement> & InputHTMLAttributes<HTMLInputElement>
> = memo((props) => {
  return <input {...props} className={"ant-input"} />
})
InputWrapper.displayName = "InputWrapper"

export const MaskInput: FC<Props & Omit<InputProps, "size">> = ({
  mask,
  maskPlaceholder,
  disabled,
  alwaysShowMask,
  ...props
}) => {
  return (
    <InputMask
      autoComplete="off"
      mask={mask}
      disabled={disabled}
      maskPlaceholder={maskPlaceholder}
      alwaysShowMask={alwaysShowMask}
      type={"text"}
      {...props}
    >
      {(inputProps: BaseHTMLAttributes<HTMLInputElement>) => (
        <InputWrapper {...inputProps} value={props.value} disabled={disabled} />
      )}
    </InputMask>
  )
}
