import { FC } from "react"
import { LoadingOutlined } from "@ant-design/icons"
import { Spin } from "antd"
import { StyledLoader } from "./StyledLoader"

const antIcon = <LoadingOutlined style={{ fontSize: 50 }} spin />

export const Loader: FC = () => {
  return (
    <>
      <StyledLoader>
        <Spin indicator={antIcon} />
      </StyledLoader>
    </>
  )
}
