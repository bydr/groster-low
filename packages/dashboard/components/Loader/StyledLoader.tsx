import { styled } from "@linaria/react"

export const StyledLoader = styled.div`
  position: absolute;
  top: 0;
  width: 100%;
  min-height: 100px;
  display: flex;
  justify-content: center;
`
