import { FC } from "react"
import { List } from "antd"
import { CustomLink } from "../../Link/Link"
import { ROUTES } from "../../../utils/constants"
import { TagResponse } from "../../../../contracts/contracts"
import { SelectOutlined } from "@ant-design/icons"
import { IconText } from "../../IconText"

export const TagsList: FC<{ tags: TagResponse }> = ({ tags }) => {
  return (
    <>
      {tags.length > 0 && (
        <>
          <List
            itemLayout="vertical"
            dataSource={tags.sort((a, b) => {
              return (a?.name || "") > (b.name || "")
                ? 1
                : (a.name || "") < (b.name || "")
                ? -1
                : 0
            })}
            renderItem={(item) => (
              <List.Item
                actions={[
                  <CustomLink key={"edit"} href={`${ROUTES.tags}/${item.id}`}>
                    Подробнее
                  </CustomLink>,
                  <CustomLink
                    key={"external"}
                    href={
                      !!item.url
                        ? item.url.includes("http")
                          ? item.url
                          : `${process.env.NEXT_PUBLIC_SITE_BASE_URL}${item.url}`
                        : ""
                    }
                    target={"_blank"}
                  >
                    <IconText icon={SelectOutlined} text={`На сайте`} />
                  </CustomLink>,
                ]}
              >
                <List.Item.Meta title={item.name || "Без имени"} />
              </List.Item>
            )}
          />
        </>
      )}
    </>
  )
}
