import { FC, useEffect, useState } from "react"
import { Button, Form, Input, Popconfirm, Space } from "antd"
import { ApiError, Tag } from "../../../../contracts/contracts"
import { useMutation } from "react-query"
import { catalogAPI } from "../../../api/catalogAPI"
import { ROUTES } from "../../../utils/constants"
import { openNotification } from "../../Notification"
import { useRouter } from "next/router"
import { ErrorMessage } from "components/Messages/ErrorMessage"

export const TagForm: FC<{ tag?: Tag & { id?: string } }> = ({ tag }) => {
  const [form] = Form.useForm<Tag>()
  const [mode, setMode] = useState<"create" | "edit">(
    tag !== undefined ? "edit" : "create",
  )
  const router = useRouter()
  const [error, setError] = useState<string | undefined>()

  useEffect(() => {
    setMode(tag !== undefined ? "edit" : "create")
  }, [tag])

  const { mutate: editMutate, isLoading: isLoadingEdit } = useMutation(
    catalogAPI.editTag,
    {
      onSuccess: () => {
        openNotification({
          message: "Тег успешно изменен",
          description: (
            <>
              Можете перейти на страницу со списком тегов.{" "}
              <Button type={"primary"} size={"small"} href={ROUTES.tags}>
                Перейти
              </Button>
            </>
          ),
          iconVariant: "success",
        })
      },
    },
  )

  const { mutate: createMutate, isLoading: isLoadingCreate } = useMutation(
    catalogAPI.createTag,
    {
      onSuccess: () => {
        openNotification({
          message: "Тег успешно создан",
          description: (
            <>
              Можете перейти на страницу со списком тегов.{" "}
              <Button
                type={"primary"}
                size={"small"}
                href={`${ROUTES.base}${ROUTES.tags}`}
              >
                Перейти
              </Button>
            </>
          ),
          iconVariant: "success",
        })
      },
      onError: (error: ApiError) => {
        const msg = error.message || "Произошла ошибка"
        setError(msg)
        openNotification({
          message: msg,
          iconVariant: "error",
        })
      },
      onMutate: () => {
        setError(undefined)
      },
    },
  )

  const { mutate: removeMutate, isLoading: isLoadingRemoved } = useMutation(
    catalogAPI.removeTag,
    {
      onSuccess: () => {
        void router.replace(ROUTES.tags)
      },
    },
  )

  const [visible, setVisible] = useState(false)

  const showPopconfirm = () => {
    setVisible(true)
  }

  const handleOk = () => {
    if (tag?.id !== undefined) {
      removeMutate(tag?.id)
    }
  }

  const handleCancel = () => {
    setVisible(false)
  }

  useEffect(() => {
    if (!!tag) {
      form.setFieldsValue({ ...tag })
    }
  }, [form, tag])

  return (
    <>
      <Form
        form={form}
        name="basic"
        layout={"vertical"}
        autoComplete="off"
        onFinish={(values) => {
          if (mode === "create") {
            createMutate({
              name: values.name,
              url: values.url,
              h1: values.h1,
              meta_description: values.meta_description,
              meta_title: values.meta_title,
              text: values.text,
            })
          }
          if (mode === "edit") {
            if (tag?.id) {
              editMutate({
                name: values.name,
                url: values.url,
                h1: values.h1,
                meta_description: values.meta_description,
                meta_title: values.meta_title,
                text: values.text,
                id: tag.id,
              })
            }
          }
        }}
      >
        <Form.Item label="Название" name={"name"}>
          <Input type={"text"} />
        </Form.Item>
        <Form.Item label="Отображаемый заголовок" name={"h1"}>
          <Input type={"text"} />
        </Form.Item>
        <Form.Item label="Ссылка" name={"url"}>
          <Input type={"text"} />
        </Form.Item>
        <Form.Item label="Описание" name={"text"}>
          <Input.TextArea />
        </Form.Item>
        <Form.Item label="Мета заголовок" name={"meta_title"}>
          <Input type={"text"} />
        </Form.Item>
        <Form.Item label="Мета описание" name={"meta_description"}>
          <Input.TextArea />
        </Form.Item>

        <Form.Item>
          <Space>
            <Button
              type="primary"
              htmlType="submit"
              loading={isLoadingCreate || isLoadingEdit}
            >
              {mode === "create" && "Создать"}
              {mode === "edit" && "Сохранить изменения"}
            </Button>

            {mode === "edit" && (
              <>
                <Popconfirm
                  title="Вы уверены что хотите удалить тег?"
                  visible={visible}
                  onConfirm={handleOk}
                  okButtonProps={{ loading: isLoadingRemoved }}
                  onCancel={handleCancel}
                  cancelText={"Отмена"}
                >
                  <Button type={"ghost"} danger onClick={showPopconfirm}>
                    Удалить
                  </Button>
                </Popconfirm>
              </>
            )}
          </Space>
        </Form.Item>

        {error && (
          <>
            <ErrorMessage>{error}</ErrorMessage>
          </>
        )}
      </Form>
    </>
  )
}
