import { FC, useEffect, useMemo, useState } from "react"
import {
  AdminCategory,
  ApiError,
  V1AdminCategoryUpdatePayload,
} from "../../../../contracts/contracts"
import { Button, Form, Input, Select, SelectProps, Typography } from "antd"
import { useMutation } from "react-query"
import { catalogAPI } from "../../../api/catalogAPI"
import { useAppDispatch, useAppSelector } from "../../../hooks/redux"
import { catalogSlice } from "../../../store/reducers/catalogSlice"
import { openNotification } from "../../Notification"
import { ROUTES } from "../../../utils/constants"

interface ItemProps {
  label: string
  value: string | number
}

export const CategoryForm: FC<{
  category: AdminCategory
}> = ({ category }) => {
  const { Text } = Typography
  const dispatch = useAppDispatch()
  const { setTags } = catalogSlice.actions
  const tags = useAppSelector((state) => state.catalog.tags)
  const [form] = Form.useForm<V1AdminCategoryUpdatePayload>()
  const [error, setError] = useState<string | undefined>()
  const [value, setValue] = useState<number[]>(category.tags || [])

  const { mutate: tagsMutate } = useMutation(() => catalogAPI.getTags(), {
    onSuccess: (response) => {
      dispatch(setTags(response || null))
    },
  })

  const selectProps: SelectProps = useMemo(
    () => ({
      mode: "multiple" as const,
      style: { width: "100%" },
      value,
      options:
        tags?.map((tag, index) => {
          return {
            value: tag.id || `${index}`,
            label: tag.name || "Tag",
          } as ItemProps
        }) || undefined,
      onChange: (newValue: number[]) => {
        setValue(newValue)
      },
      placeholder: "Выбрать теги...",
      maxTagCount: "responsive" as const,
    }),
    [tags, value],
  )

  useEffect(() => {
    if (tags === null) {
      tagsMutate()
    }
  }, [tagsMutate, tags])

  useEffect(() => {
    if (category !== undefined) {
      form.setFieldsValue({ ...category })
    }
  }, [form, category])

  const { mutate: editCategoryMutate, isLoading } = useMutation(
    catalogAPI.editCategory,
    {
      onSuccess: () => {
        openNotification({
          message: "Данные успешно изменены",
          description: (
            <>
              Можете перейти на страницу со списком категорий.{" "}
              <Button
                type={"primary"}
                size={"small"}
                href={`${ROUTES.base}${ROUTES.categories}`}
              >
                Перейти
              </Button>
            </>
          ),
          iconVariant: "success",
        })
      },
      onError: (error: ApiError) => {
        const msg = error.message || "Произошла ошибка"
        setError(msg)
        openNotification({
          message: msg,
          iconVariant: "error",
        })
      },
      onMutate: () => {
        setError(undefined)
      },
    },
  )

  return (
    <>
      <Form
        form={form}
        name="basic"
        layout={"vertical"}
        autoComplete="off"
        onFinish={(values) => {
          if (!!category.id) {
            editCategoryMutate({
              tags: value,
              id: `${category.id}`,
              name: values.name,
              meta_title: values.meta_title,
              meta_description: values.meta_description,
              alias: values.alias,
            })
          }
        }}
      >
        <Form.Item
          label="Алиас"
          name={"alias"}
          help={"Так будет выглядеть url категории. Строка без пробелов"}
          rules={[
            {
              required: true,
              message: "Поле обязательно для заполнения",
            },
            {
              pattern: /^[a-zA-Z0-9!@#\$%\^\&*\)\(+=._-]+$/g,
              message: "Неверный формат",
            },
          ]}
        >
          <Input type={"text"} />
        </Form.Item>
        <Form.Item label="Название" name={"name"}>
          <Input type={"text"} />
        </Form.Item>
        <Form.Item label="Мета заголовок" name={"meta_title"}>
          <Input type={"text"} />
        </Form.Item>
        <Form.Item label="Мета описание" name={"meta_description"}>
          <Input.TextArea />
        </Form.Item>
        <Form.Item>
          <Select {...selectProps} />
        </Form.Item>

        <Form.Item>
          <Button type="primary" htmlType="submit" loading={isLoading}>
            Сохранить изменения
          </Button>
        </Form.Item>

        {error && (
          <>
            <Text type={"danger"}>{error}</Text>
          </>
        )}
      </Form>
    </>
  )
}
