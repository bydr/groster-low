import { FC } from "react"
import { List } from "antd"
import { CategoryItem } from "./Item"
import { useAppSelector } from "../../../hooks/redux"
import { cssCategoryList } from "./StyledList"

export const CategoryList: FC = () => {
  const categoriesTree = useAppSelector(
    (state) => state.catalog.categories?.tree,
  )
  return (
    <>
      {!!categoriesTree && Object.keys(categoriesTree).length > 0 && (
        <>
          <List
            className={cssCategoryList}
            itemLayout="vertical"
            dataSource={Object.keys(categoriesTree)
              .filter((c) => categoriesTree[c].uuid !== undefined)
              .map((c) => categoriesTree[c])}
            renderItem={(item) => <CategoryItem item={item} />}
          />
        </>
      )}
    </>
  )
}
