import { css } from "@linaria/core"
import { colors } from "../../../styles/vars"

export const cssCategoryItemRolledUp = css``

export const cssCategoryListRolledUp = css`
  display: none !important;
`

export const cssCategoryItem = css`
  display: grid;
  grid-template-areas: "meta" "actions" "childs";
  justify-content: normal;

  > * {
    width: 100%;
  }

  .ant-list-item-meta {
    grid-area: meta;
  }

  .ant-list-item-action {
    grid-area: actions;
    margin: 0;

    > * {
      &:last-child {
        float: right;
      }
    }
  }

  &.${cssCategoryItemRolledUp} {
    background: ${colors.pinkLight}!important;
  }
`
export const cssCategoryList = css`
  & & {
    padding-left: 30px;
    grid-area: childs;
    margin-top: 16px;

    .${cssCategoryItem} {
      background: ${colors.grayLighter};
      padding-left: 10px;
    }
  }

  & & & {
    .${cssCategoryItem} {
      background: ${colors.grayLight};
    }
  }
`
