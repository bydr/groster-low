import { FC, useState } from "react"
import { CustomLink } from "../../Link/Link"
import { ROUTES } from "../../../utils/constants"
import { IconText } from "../../IconText"
import {
  CaretDownOutlined,
  CaretUpOutlined,
  InboxOutlined,
  SelectOutlined,
} from "@ant-design/icons"
import { Button, List } from "antd"
import { EntityImage } from "../../EntityImage/EntityImage"
import { ICategoryTreeItem } from "types/types"
import {
  cssCategoryItem,
  cssCategoryItemRolledUp,
  cssCategoryList,
  cssCategoryListRolledUp,
} from "./StyledList"
import { cx } from "@linaria/core"

export const CategoryItem: FC<{ item: ICategoryTreeItem }> = ({ item }) => {
  const isChilds = Object.keys(item.children || {}).length > 0
  const [isShowChilds, setIsShopChilds] = useState<boolean>(true)

  return (
    <>
      <List.Item
        className={cx(
          cssCategoryItem,
          !isShowChilds && cssCategoryItemRolledUp,
        )}
        actions={[
          <CustomLink key={"edit"} href={`${ROUTES.categories}/${item.id}`}>
            Подробнее
          </CustomLink>,
          <CustomLink
            key={"external"}
            href={`${process.env.NEXT_PUBLIC_SITE_BASE_URL}/catalog/${item.alias}`}
            target={"_blank"}
          >
            <IconText icon={SelectOutlined} text={`На сайте`} />
          </CustomLink>,
          <IconText
            key={"productCount"}
            icon={InboxOutlined}
            text={`Товаров: ${item.product_qty || 0}`}
          />,
          isChilds && (
            <Button
              key={"toggleList"}
              type={isShowChilds ? "text" : "primary"}
              icon={isShowChilds ? <CaretUpOutlined /> : <CaretDownOutlined />}
              onClick={() => {
                setIsShopChilds(!isShowChilds)
              }}
            >
              {isShowChilds ? "Свернуть" : "Развернуть"}
            </Button>
          ),
        ]}
      >
        <>
          <List.Item.Meta
            title={item.name || "Без имени"}
            avatar={<EntityImage imagePath={item.image} />}
          />
          {isChilds && (
            <>
              <List
                className={cx(
                  cssCategoryList,
                  !isShowChilds && cssCategoryListRolledUp,
                )}
                itemLayout="vertical"
                dataSource={Object.keys(item.children)
                  .filter((c) => item.children[c].uuid !== undefined)
                  .map((c) => item.children[c])}
                renderItem={(child) => <CategoryItem item={child} />}
              />
            </>
          )}
        </>
      </List.Item>
    </>
  )
}
