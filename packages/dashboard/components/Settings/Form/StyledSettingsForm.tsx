import { css } from "@linaria/core"
import { styled } from "@linaria/react"
import { Typography } from "antd"

export const InputSelectedHolidays = styled(Typography.Text)`
  position: absolute;
  z-index: 2;
  height: 100%;
  user-select: none;
  appearance: none;
  left: 0;
  right: 20px;
  cursor: text;

  & ~ input {
    color: transparent;
    &::placeholder {
      color: transparent;
    }
  }
`

export const cssSelectedDayHoliday = css``

export const cssDatePicker = css`
  .ant-picker-cell-selected .ant-picker-cell-inner {
    &:not(.${cssSelectedDayHoliday}) {
      background: transparent;
      color: rgba(0, 0, 0, 0.65);
    }
  }

  .ant-picker-cell-inner.${cssSelectedDayHoliday} {
    color: #fff;
    background: #bc39e5;
  }
`
