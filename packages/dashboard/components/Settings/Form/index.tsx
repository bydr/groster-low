import { FC, useEffect, useState } from "react"
import {
  Button,
  DatePicker,
  Form,
  Input,
  InputNumber,
  Space,
  TimePicker,
} from "antd"
import { ApiError, SettingsFetchedType } from "../../../../contracts/contracts"
import moment, { Moment } from "moment"
import { useMutation } from "react-query"
import { settingsAPI } from "../../../api/settingsAPI"
import { openNotification } from "../../Notification"
import { DATE_FORMAT, ROUTES, TIME_FORMAT } from "../../../utils/constants"
import { ErrorMessage } from "../../Messages/ErrorMessage"
import {
  cssDatePicker,
  cssSelectedDayHoliday,
  InputSelectedHolidays,
} from "./StyledSettingsForm"
import { cx } from "@linaria/core"
import "moment/locale/ru"

export const SettingsForm: FC<{ settings: SettingsFetchedType }> = ({
  settings,
}) => {
  const [form] = Form.useForm<
    Omit<SettingsFetchedType, "delivery_fast_time"> & {
      delivery_fast_time?: Moment
    }
  >()
  const [error, setError] = useState<string | undefined>()

  const { mutate: editSettingsMutate, isLoading } = useMutation(
    settingsAPI.editSettings,
    {
      onSuccess: () => {
        openNotification({
          message: "Настройки изменены",
          description: (
            <>
              Можете перейти на страницу настроек.{" "}
              <Button
                type={"primary"}
                size={"small"}
                href={`${ROUTES.base}${ROUTES.settings}`}
              >
                Перейти
              </Button>
            </>
          ),
          iconVariant: "success",
        })
      },
      onError: (error: ApiError) => {
        const msg = error.message || "Произошла ошибка"
        setError(msg)
        openNotification({
          message: msg,
          iconVariant: "error",
        })
      },
      onMutate: () => {
        setError(undefined)
      },
    },
  )

  useEffect(() => {
    if (!!settings) {
      form.setFieldsValue({
        ...settings,
        delivery_fast_time:
          settings.delivery_fast_time !== undefined
            ? moment(settings.delivery_fast_time, TIME_FORMAT)
            : undefined,
        holidays: [...(settings.holidays || [])],
      })
    }
  }, [form, settings])

  const [isOpenPickerHolidays, setIsOpenPickerHolidays] = useState(false)
  const [selectedHolidays, setSelectedHolidays] = useState<string[]>([
    ...(settings.holidays || []),
  ])
  useEffect(() => {
    setSelectedHolidays([...(settings.holidays || [])])
  }, [settings.holidays])

  return (
    <>
      <Form
        form={form}
        name="basic"
        layout={"vertical"}
        autoComplete="off"
        onFinish={(values) => {
          editSettingsMutate({
            ...values,
            delivery_fast_time: values.delivery_fast_time?.format(TIME_FORMAT),
            holidays: selectedHolidays,
          })
        }}
      >
        <Form.Item
          label="Дни смещения доставки при заказе с основного склада"
          name={"delivery_shift"}
        >
          <InputNumber min={0} />
        </Form.Item>
        <Form.Item
          label={`Минимальное значение остатков для указания пометки "много"`}
          name={"min_many_quantity"}
          rules={[
            {
              pattern: RegExp("^([-]?[1-9][0-9]*|0)$"),
              message: "Поле должно быть числом",
            },
          ]}
        >
          <Input />
        </Form.Item>
        <Form.Item
          label={`Время, при заказе до которого, доставка будет в этот же день`}
          name={"delivery_fast_time"}
          rules={[
            {
              type: "object" as const,
              required: true,
              message: "Please select time!",
            },
          ]}
        >
          <TimePicker format={TIME_FORMAT} />
        </Form.Item>
        <Form.Item label={`Viber`} name={"viber"}>
          <Input />
        </Form.Item>
        <Form.Item label={`WhatsApp`} name={"whatsApp"}>
          <Input />
        </Form.Item>
        <Form.Item label={`Telegram`} name={"telegram"}>
          <Input />
        </Form.Item>

        <Form.Item
          label={"Праздничные дни"}
          name={"holidays"}
          valuePropName={"data-fake"}
        >
          <DatePicker
            dropdownClassName={cssDatePicker}
            open={isOpenPickerHolidays}
            dateRender={(current) => {
              return (
                <div
                  className={cx(
                    "ant-picker-cell-inner",
                    selectedHolidays
                      .map((h: string) => moment(h).format(DATE_FORMAT))
                      .includes(current.format(DATE_FORMAT)) &&
                      cssSelectedDayHoliday,
                  )}
                >
                  {current.date()}
                </div>
              )
            }}
            value={undefined}
            onBlur={() => {
              setIsOpenPickerHolidays(false)
            }}
            onFocus={() => {
              setIsOpenPickerHolidays(true)
            }}
            onChange={(date) => {
              if (date === null) {
                setSelectedHolidays([])
              }
            }}
            onSelect={(date) => {
              if (date !== null) {
                const isSelected = selectedHolidays
                  .map((h: string) => moment(h).format(DATE_FORMAT))
                  .includes(date.format(DATE_FORMAT))
                if (isSelected) {
                  form.setFieldsValue({
                    holidays: selectedHolidays.filter(
                      (h: string) =>
                        moment(h).format(DATE_FORMAT) !==
                        date.format(DATE_FORMAT),
                    ),
                  })
                } else {
                  setSelectedHolidays([...selectedHolidays, date.toISOString()])
                }
              }
            }}
            inputRender={(value) => {
              return (
                <>
                  <InputSelectedHolidays>
                    {selectedHolidays.length || 0}
                  </InputSelectedHolidays>
                  <input {...value} />
                </>
              )
            }}
          />
        </Form.Item>

        <Form.Item>
          <Space>
            <Button type="primary" htmlType="submit" loading={isLoading}>
              Сохранить изменения
            </Button>
          </Space>
        </Form.Item>

        {error && (
          <>
            <ErrorMessage>{error}</ErrorMessage>
          </>
        )}
      </Form>
    </>
  )
}
