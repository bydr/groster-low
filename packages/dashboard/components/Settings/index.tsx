import { FC } from "react"
import { useQuery } from "react-query"
import { settingsAPI } from "../../api/settingsAPI"
import { Skeleton } from "antd"
import { SettingsForm } from "./Form"

export const Settings: FC = () => {
  const { data, isLoading } = useQuery("settings", () =>
    settingsAPI.getAdminSettings(),
  )
  return (
    <>
      {
        <Skeleton loading={isLoading} active title>
          {data !== undefined && (
            <>
              <SettingsForm settings={data} />
            </>
          )}
        </Skeleton>
      }
    </>
  )
}
