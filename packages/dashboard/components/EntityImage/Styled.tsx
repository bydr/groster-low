import { styled } from "@linaria/react"

export const StyledEntityImage = styled.div`
  display: flex;
  position: relative;
  margin: 0 0 10px 0;
  width: auto;
  align-items: center;

  img {
    object-fit: contain;
    object-position: center top;
  }

  * {
    font-size: inherit;
    line-height: inherit;
  }

  .ant-empty {
    margin: 0;

    & .ant-empty-image {
      height: 30px;
    }
  }
`
