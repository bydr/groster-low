import type { FC } from "react"
import Image, { ImageProps } from "next/image"
import { StyledEntityImage } from "./Styled"
import { Empty } from "antd"

type ImagePropsType = Omit<ImageProps, "src" | "draggable"> & {
  imagePath?: string
  imageAlt?: string
}
export const EntityImage: FC<ImagePropsType> = ({
  imagePath,
  imageAlt = "",
  priority,
  ...props
}) => {
  return (
    <StyledEntityImage>
      {imagePath !== undefined && imagePath.length > 0 ? (
        <Image
          src={imagePath.includes("http") ? imagePath : `https://${imagePath}`}
          {...props}
          draggable={false}
          alt={imageAlt}
          layout={props.layout || "fill"}
          priority={priority}
        />
      ) : (
        <Empty image={Empty.PRESENTED_IMAGE_SIMPLE} description={""} />
      )}
    </StyledEntityImage>
  )
}
