const withLinaria = require("next-linaria")
const withLess = require("next-with-less")
// const env = process.env.NODE_ENV

const securityHeaders = [
  {
    key: "X-DNS-Prefetch-Control",
    value: "on",
  },
  {
    key: "Strict-Transport-Security",
    value: "max-age=63072000; includeSubDomains; preload",
  },
  {
    key: "X-XSS-Protection",
    value: "1; mode=block",
  },
  {
    key: "X-Frame-Options",
    value: "SAMEORIGIN",
  },
  {
    key: "Permissions-Policy",
    value: "camera=(), microphone=(), geolocation=(), interest-cohort=()",
  },
  {
    key: "X-Content-Type-Options",
    value: "nosniff",
  },
  {
    key: "Referrer-Policy",
    value: "origin-when-cross-origin",
  },
]

// if (env === "production") {
//   securityHeaders.push({
//     key: "Content-Security-Policy",
//     value: "default-src 'self'",
//   })
// }

/** @type {import('next').NextConfig} */
const nextConfig = {
  basePath: "/dashboard",
  reactStrictMode: true,
  productionBrowserSourceMaps: true,
  compress: false,
  async headers() {
    return [
      {
        // Apply these headers to all routes in your application.
        source: `/(.*)`,
        headers: securityHeaders,
      },
    ]
  },
  async rewrites() {
    return [
      {
        source: "/shipping-methods/:path*",
        destination: `/shippingMethods/:path*`,
      },
    ]
  },
  experimental: { esmExternals: true },
  typescript: {
    /** @todo remove after reakit typing fix */
    ignoreBuildErrors: true,
  },
  poweredByHeader: true,
  images: {
    domains: ["static.groster.me"],
  },
}

module.exports = withLess(withLinaria(nextConfig))
