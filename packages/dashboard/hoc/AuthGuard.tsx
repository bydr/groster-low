import { useRouter } from "next/router"
import { FC, useEffect } from "react"
import { useAuth } from "../hooks/auth"
import { ROUTES } from "../utils/constants"

export const AuthGuard: FC = ({ children }) => {
  const { isAuth, isInit } = useAuth()
  const { replace, pathname } = useRouter()

  useEffect(() => {
    if (isInit) {
      if (!isAuth) {
        void replace("/")
      } else {
        if (pathname === "/") {
          void replace(ROUTES.home)
        }
      }
    }
  }, [isAuth, isInit, pathname])

  if (isAuth || pathname === "/") {
    return <>{children}</>
  }

  return null
}
