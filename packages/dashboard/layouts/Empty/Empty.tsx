import { FC } from "react"

export const Empty: FC = ({ children }) => {
  return <>{children}</>
}
