import type { FC } from "react"
import { createElement, useState, MouseEvent } from "react"
import { Layout, Menu, MenuProps, PageHeader } from "antd"
import {
  AppstoreOutlined,
  PictureOutlined,
  ProfileOutlined,
  QuestionCircleOutlined,
  SettingOutlined,
  ShopOutlined,
  ShoppingCartOutlined,
  TagOutlined,
  WalletOutlined,
  HomeOutlined,
} from "@ant-design/icons"
import { SiteLayout, StyledSider } from "./StyledDefault"
import Link from "next/link"
import Logo from "../Logo/Logo"
import { ROUTES } from "../../utils/constants"
import { useRouter } from "next/router"
import { useApp } from "../../hooks/app"
import { PromocodeIcon } from "components/Icon/Promocode"

const { Footer, Content } = Layout

const items: MenuProps["items"] = [
  {
    key: "home",
    label: <Link href={ROUTES.home}>Главная</Link>,
    icon: createElement(HomeOutlined),
  },
  {
    key: "catalog",
    title: "Каталог title ",
    label: "Каталог",
    icon: createElement(ProfileOutlined),
    children: [
      {
        key: "categories",
        label: <Link href={ROUTES.categories}>Категории</Link>,
        icon: createElement(AppstoreOutlined),
      },
      {
        key: "tags",
        label: <Link href={ROUTES.tags}>Тэги</Link>,
        icon: createElement(TagOutlined),
      },
    ],
  },
  {
    key: "shops",
    label: <Link href={ROUTES.shops}>Магазины</Link>,
    icon: createElement(ShopOutlined),
  },
  {
    key: "settings",
    label: <Link href={ROUTES.settings}>Настройки</Link>,
    icon: createElement(SettingOutlined),
  },
  {
    key: "paymentMethods",
    label: <Link href={ROUTES.payments}>Методы оплаты</Link>,
    icon: createElement(WalletOutlined),
  },
  {
    key: "shippingMethods",
    label: <Link href={ROUTES.shippingMethods}>Методы доставки</Link>,
    icon: createElement(ShoppingCartOutlined),
  },
  {
    key: "promocodes",
    label: <Link href={ROUTES.promocodes}>Промокоды</Link>,
    icon: createElement(PromocodeIcon),
  },
  {
    key: "faq",
    label: <Link href={ROUTES.faq}>Вопрос-Ответ</Link>,
    icon: createElement(QuestionCircleOutlined),
  },
  {
    key: "banners",
    label: <Link href={ROUTES.banners}>Баннеры</Link>,
    icon: createElement(PictureOutlined),
  },
]

export const Default: FC<{
  pageTitle?: string
  pageSubTitle?: string
  onBack?: (e?: MouseEvent<HTMLDivElement, MouseEvent> | undefined) => void
  backRoute?: string
}> = ({ children, pageTitle, pageSubTitle, onBack, backRoute }) => {
  const router = useRouter()

  const [collapsed, setCollapsed] = useState(false)
  const onCollapse = (collapsed: boolean) => {
    setCollapsed(collapsed)
  }

  const { pageTitle: appPageTitle } = useApp()

  return (
    <>
      <SiteLayout hasSider>
        <StyledSider
          collapsible
          collapsed={collapsed}
          onCollapse={onCollapse}
          theme={"light"}
        >
          <Logo />
          <Menu
            theme={"light"}
            mode={"inline"}
            defaultSelectedKeys={["4"]}
            items={items}
          />
        </StyledSider>
        <SiteLayout>
          <PageHeader
            className="site-page-header"
            title={pageTitle || appPageTitle}
            subTitle={pageSubTitle}
            onBack={
              onBack || !!backRoute
                ? () => {
                    void router.replace(backRoute || "")
                  }
                : undefined
            }
          />
          <Content style={{ margin: "24px 16px 0", overflow: "initial" }}>
            {children}
          </Content>
          <Footer>{new Date().getFullYear()} © Гростер</Footer>
        </SiteLayout>
      </SiteLayout>
    </>
  )
}
