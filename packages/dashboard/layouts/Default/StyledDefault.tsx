import { Layout, PageHeader } from 'antd'
import { styled } from "@linaria/react"

const { Sider } = Layout

export const SiteLayout = styled(Layout)`
  background: #fff;
`
export const StyledSider = styled(Sider)`
  min-height: 100vh;
`

export const StyledPageHeader = styled(PageHeader)`
  border-bottom: #f0f2f5;
`
