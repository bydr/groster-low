import type { FC } from "react"
import Logotype from "./logo.svg"
import Link from "next/link"
import { styled } from "@linaria/react"

export const StyledLogo = styled.div`
  position: relative;
  width: 100%;
  display: flex;
  padding: 16px;

  a {
    width: 100%;
    display: flex;
    align-items: center;
    justify-content: center;
    text-align: center;
    svg {
      width: 100%;
      max-width: 150px;
    }
  }
`

const Logo: FC = () => {
  return (
    <StyledLogo>
      <Link href={"/"} passHref>
        <a>
          <Logotype />
        </a>
      </Link>
    </StyledLogo>
  )
}

export default Logo
