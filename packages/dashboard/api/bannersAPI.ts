import { AdminResponseBanners, RequestBanner } from "../../contracts/contracts"
import { instance } from "./api"

export const bannersAPI = {
  getAdminBanners: (): Promise<AdminResponseBanners> =>
    instance.get("/admin/pictures").then((res) => res.data),
  createBanner: (data: RequestBanner): Promise<null> =>
    instance.post("/admin/pictures", data).then((res) => res.data),
  editBanner: ({
    id,
    ...data
  }: RequestBanner & { id: number }): Promise<null> =>
    instance.put(`/admin/pictures/${id}`, data).then((res) => res.data),
  removeBanner: ({ id }: { id: number }): Promise<null> =>
    instance.delete(`/admin/pictures/${id}`).then((res) => res.data),
}
