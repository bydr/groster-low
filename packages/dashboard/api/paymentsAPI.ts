import {
  AdminPaymentMethod,
  PaymentMethodCreateRequest,
} from "../../contracts/contracts"
import { instance } from "./api"

export const paymentsAPI = {
  getAdminPaymentMethods: (): Promise<AdminPaymentMethod[]> =>
    instance.get("/admin/payment-methods").then((res) => res.data),
  createPaymentMethod: (
    data: PaymentMethodCreateRequest,
  ): Promise<{ uid: string }> =>
    instance.post("/admin/payment-method/add", data).then((res) => res.data),
  editPaymentMethod: (data: AdminPaymentMethod): Promise<{ uid: string }> =>
    instance
      .put(`/admin/payment-method/${data.uid}`, data)
      .then((res) => res.data),
}
