import {
  AdminCategory,
  CategoryResponse,
  Tag,
  TagResponse,
  V1AdminCategoryUpdatePayload,
} from "../../contracts/contracts"
import { FetcherBasePropsType, instance } from "./api"
import { getAbsolutePath } from "../utils/helpers"

export const catalogAPI = {
  getTags: (data?: FetcherBasePropsType): Promise<TagResponse> =>
    instance
      .get<TagResponse>(`${getAbsolutePath(data?.server)}/catalog/tags`)
      .then((res) => res.data),
  getTagById: ({
    id,
    server,
  }: FetcherBasePropsType & { id: number | string }): Promise<Tag> =>
    instance
      .get<Tag>(`${getAbsolutePath(server)}/catalog/tags/${id}`)
      .then((res) => res.data),
  editTag: (data: Tag & { id: string }): Promise<Tag> =>
    instance.put<Tag>(`/admin/tags/${data.id}`, data).then((res) => res.data),
  createTag: (data: Tag): Promise<Tag> =>
    instance.post<Tag>(`/admin/tag/add`, data).then((res) => res.data),
  removeTag: (id: string): Promise<null> =>
    instance.delete<null>(`/admin/tags/${id}`).then((res) => res.data),
  getCategories: (data?: FetcherBasePropsType): Promise<CategoryResponse> =>
    instance
      .get<CategoryResponse>(
        `${getAbsolutePath(data?.server)}/catalog/categories`,
      )
      .then((res) => res.data),
  getCategoryById: ({
    id,
    server,
  }: FetcherBasePropsType & { id: string }): Promise<AdminCategory> =>
    instance
      .get<AdminCategory>(`${getAbsolutePath(server)}/admin/category/${id}`)
      .then((res) => res.data),
  editCategory: ({
    id,
    ...rest
  }: V1AdminCategoryUpdatePayload & {
    id: string
  }): Promise<null> =>
    instance
      .put<V1AdminCategoryUpdatePayload, null>(`/admin/category/${id}`, rest)
      .then((res) => res),
}
