type RequestOptionsType = RequestInit
const token = process.env.NEXT_PUBLIC_DADATA_API_KEY || ""

type SuggestAddressesReturnType = {
  suggestions?: {
    unrestricted_value?: string | null
    value?: string | null
    data: {
      city: string | null
      city_type: string | null
      region: string | null
      region_type: string | null
      city_with_type: string | null
      region_with_type: string | null
    } | null
  }[]
}

type BoundValueType =
  | "country"
  | "region"
  | "area"
  | "city"
  | "settlement"
  | "street"
  | "house"

export const fetchSuggestAddresses = ({
  address,
  locations,
  fromBound,
  toBound,
}: {
  address: string
  locations?: Record<"region_type_full", string | number>[]
  fromBound?: Record<"value", BoundValueType>
  toBound?: Record<"value", BoundValueType>
}): Promise<SuggestAddressesReturnType> =>
  dadataPost({
    url: "https://suggestions.dadata.ru/suggestions/api/4_1/rs/suggest/address",
    body: {
      query: address,
      locations: locations || [],
      from_bound: fromBound,
      to_bound: toBound,
      locations_boost: [{ kladr_id: "3400000100000" }],
    },
  })

export const dadataPost = <R, Q>({
  url,
  body,
}: {
  url: string
  body: Q
}): Promise<R> => {
  return dadataFetch({
    url,
    options: {
      method: "POST",
      mode: "cors",
      headers: {
        "Content-Type": "application/json",
        Accept: "application/json",
        Authorization: "Token " + token,
      },
      body: JSON.stringify(body),
    },
  })
}

export const dadataFetch = <R>({
  url,
  options,
}: {
  url: string
  options: RequestOptionsType
}): Promise<R> => {
  return fetch(`${url}`, options)
    .then((response) => response.json() as Promise<R>)
    .catch((error) => error)
}
