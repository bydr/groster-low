import { Code, CodeRequest, CodeResponse } from "../../contracts/contracts"
import { instance } from "./api"

export const promocodeAPI = {
  getPromocodes: (): Promise<Code[]> =>
    instance.get("/admin/promocodes").then((res) => res.data),
  getPromocodeById: ({ id }: { id: number }): Promise<Code> =>
    instance.get(`/admin/promocode/${id}`).then((res) => res.data),
  editPromocode: ({
    id,
    ...data
  }: Omit<CodeRequest, "id"> & { id: number }): Promise<CodeResponse> =>
    instance.put(`/admin/promocode/${id}`, data).then((res) => res.data),
  createPromocode: (data: Omit<CodeRequest, "id">): Promise<CodeResponse> =>
    instance.post(`/admin/promocode/add`, data).then((res) => res.data),
}
