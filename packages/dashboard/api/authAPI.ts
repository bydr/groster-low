import {
  LoginPhoneDataRequest,
  TokenResponse,
  LoginEmailDataRequest,
  V1AuthRefreshCreatePayload,
  RequestSendCode,
  ResponseSendCode,
} from "../../contracts/contracts"
import { instance } from "./api"

export const authAPI = {
  loginPhone: (data: LoginPhoneDataRequest): Promise<TokenResponse> =>
    instance
      .post<TokenResponse>("/auth/login_phone", data)
      .then((res) => res.data),
  loginEmail: (data: LoginEmailDataRequest): Promise<TokenResponse> =>
    instance
      .post<TokenResponse>("/auth/login_email", data)
      .then((res) => res.data),
  sendCode: (data: RequestSendCode): Promise<ResponseSendCode> =>
    instance
      .post<ResponseSendCode>("/auth/send_code", data)
      .then((res) => res.data),
  refreshToken: (data: V1AuthRefreshCreatePayload): Promise<TokenResponse> =>
    instance
      .post<V1AuthRefreshCreatePayload>("/auth/refresh", data)
      .then((res) => res.data),
}
