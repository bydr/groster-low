import {
  AdminDeliveryMethodList,
  DeliveryMethod,
  Limit,
  ResponseLimit,
  ShippingMethodDetail,
  ShippingMethodEditRequest,
} from "../../contracts/contracts"
import { instance } from "./api"

export const shippingMethodsAPI = {
  getShippingMethods: (): Promise<AdminDeliveryMethodList[]> =>
    instance.get("/admin/shipping-methods").then((res) => res.data),
  createShippingMethod: (
    data: DeliveryMethod & { alias?: string },
  ): Promise<AdminDeliveryMethodList> =>
    instance.post("/admin/shipping-method/add", data).then((res) => res.data),
  getShippingMethodByAlias: ({
    alias,
  }: {
    alias: string
  }): Promise<ShippingMethodDetail> =>
    instance.get(`/admin/shipping-method/${alias}`).then((res) => res.data),
  editShippingMethod: (
    data: ShippingMethodEditRequest,
  ): Promise<{ alias: string }> =>
    instance
      .put(`/admin/shipping-method/${data.alias}`, {
        ...data,
      })
      .then((res) => res.data),
  getMethodLimit: ({ alias }: { alias: string }): Promise<ResponseLimit[]> =>
    instance
      .get(`/admin/shipping-method/${alias}/limit`)
      .then((res) => res.data),
  createMethodLimit: ({
    alias,
    ...data
  }: Limit & { alias?: string }): Promise<{ id: number }> =>
    instance
      .post(`/admin/shipping-method/${alias}/limit`, data)
      .then((res) => res.data),
  editMethodLimit: ({
    alias,
    id,
    ...data
  }: Limit & { alias: string; id: number }): Promise<{ id: number }> =>
    instance
      .put(`/admin/shipping-method/${alias}/limit/${id}`, data)
      .then((res) => res.data),
  removeMethodLimit: ({
    alias,
    id,
  }: {
    alias: string
    id: number
  }): Promise<null> =>
    instance
      .delete(`/admin/shipping-method/${alias}/limit/${id}`)
      .then((res) => res.data),
}
