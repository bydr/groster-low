import { SettingsFetchedType } from "../../contracts/contracts"
import { instance } from "./api"

export const settingsAPI = {
  getAdminSettings: (): Promise<SettingsFetchedType> =>
    instance.get("/admin/settings").then((res) => res.data),
  editSettings: (data: SettingsFetchedType): Promise<null> =>
    instance.put("/admin/settings", data).then((res) => res.data),
}
