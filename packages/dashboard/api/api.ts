import axios, { AxiosError } from "axios"
import { getClientUser, setUser } from "../hooks/auth"
import { authAPI } from "./authAPI"

export const instance = axios.create({
  baseURL: `/dashboard${process.env.NEXT_PUBLIC_API_BASE_VERSION}`,
  withCredentials: false,
  headers: {
    "Content-Type": "application/json",
  },
})

export const response = instance.interceptors.response.use(
  (response) => {
    // Any status code that lie within the range of 2xx cause this function to trigger
    // Do something with response data
    return response
  },
  async (error: AxiosError) => {
    // Any status codes that falls outside the range of 2xx cause this function to trigger
    // Do something with response error
    if (error.response?.status === 401) {
      if (error.config.url !== "/auth/refresh") {
        const storedUser = getClientUser()
        const storedRefreshToken = storedUser?.refreshToken
        setUser(null)
        if (storedUser && storedRefreshToken) {
          const res = await authAPI
            .refreshToken({
              refresh_token: storedRefreshToken,
            })
            .catch((err) => {
              console.log("err fetchRefreshToken ", err)
            })
          if (res) {
            console.log("res refresh ", res)
            setUser({
              cart: res.info?.cart || null,
              accessToken: res.access_token || null,
              refreshToken: res.refresh_token || null,
              email: storedUser.email,
              phone: storedUser.phone,
              isAdmin: storedUser.isAdmin,
              fio: storedUser.fio,
            })
          }
        }
      } else {
        setUser(null)
        // setTokenStorage(null)
        window.location.replace("/dashboard")
      }
    }
    return Promise.reject(error.response?.data)
  },
)

export const request = instance.interceptors.request.use(
  function (response) {
    // Any status code that lie within the range of 2xx cause this function to trigger
    // Do something with response data
    const token = getClientUser()?.accessToken
    if (!!token) {
      if (response.headers == undefined) {
        response.headers = {}
      }
      response.headers["Authorization"] = token
    }

    return response
  },
  function (error: AxiosError) {
    // Any status codes that falls outside the range of 2xx cause this function to trigger
    // Do something with response error
    return Promise.reject(error)
  },
)

export type FetcherBasePropsType = {
  server?: boolean
}
