import {
  AdminBlock,
  RequestQuestion,
  ResponseAdminBlock,
  ResponseAdminFaq,
} from "../../contracts/contracts"
import { instance } from "./api"

export const faqAPI = {
  getBlocks: (): Promise<ResponseAdminBlock> =>
    instance.get("/admin/faq/blocks").then((res) => res.data),
  createBlock: (data: AdminBlock): Promise<null> =>
    instance.post("/admin/faq/block/add", data).then((res) => res.data),
  editBlock: ({ id, ...data }: AdminBlock & { id: number }): Promise<null> =>
    instance.put(`/admin/faq/block/${id}`, data).then((res) => res.data),
  removeBlock: ({ id }: { id: number }): Promise<null> =>
    instance.delete(`/admin/faq/block/${id}`).then((res) => res.data),
  getQuestions: (): Promise<ResponseAdminFaq> =>
    instance.get("/admin/faq/questions").then((res) => res.data),
  createQuestion: (data: RequestQuestion): Promise<null> =>
    instance.post("/admin/faq/question/add", data).then((res) => res.data),
  editQuestion: ({
    id,
    ...data
  }: RequestQuestion & { id: number }): Promise<null> =>
    instance.put(`/admin/faq/question/${id}`, data).then((res) => res.data),
  removeQuestion: ({ id }: { id: number }): Promise<null> =>
    instance.delete(`/admin/faq/question/${id}`).then((res) => res.data),
}
