import { FetcherBasePropsType, instance } from "./api"
import {
  ShopAdmin,
  ShopListResponse,
  ShopResponse,
} from "../../contracts/contracts"
import { getAbsolutePath } from "../utils/helpers"

export const shopsAPI = {
  getShops: (data?: FetcherBasePropsType): Promise<ShopListResponse> =>
    instance
      .get<ShopListResponse>(`${getAbsolutePath(data?.server)}/shops`)
      .then((res) => res.data),
  getAdminShops: (data?: FetcherBasePropsType): Promise<ShopListResponse> =>
    instance
      .get<ShopListResponse>(`${getAbsolutePath(data?.server)}/admin/shops`)
      .then((res) => res.data),
  getShopById: ({
    uuid,
    ...data
  }: FetcherBasePropsType & { uuid: string }): Promise<ShopResponse> =>
    instance
      .get<ShopResponse>(`${getAbsolutePath(data?.server)}/admin/shops/${uuid}`)
      .then((res) => res.data),
  editShop: ({
    uuid,
    ...data
  }: ShopAdmin & { uuid: string }): Promise<ShopAdmin & { uuid: string }> =>
    instance.put(`/admin/shops/${uuid}`, data).then((res) => res.data),
}
