import {
  createContext,
  FC,
  useCallback,
  useContext,
  useEffect,
  useMemo,
} from "react"
import { accountSlice } from "../store/reducers/accountSlice"
import { useAppDispatch, useAppSelector } from "./redux"
import Cookies from "universal-cookie"

export type User = {
  accessToken: string | null
  refreshToken: string | null
  email?: string | null
  phone?: string | null
  cart: string | null
  fio?: string | null
  isAdmin?: boolean
}

type ContextType = {
  login: (user: User) => void
  logout: () => void
  user: Required<User> | null
  isAuth: boolean
  isInit: boolean
}

const cookies = new Cookies()

export const getClientUser = (): User | null => {
  if (typeof window === "undefined") {
    return null
  }
  const user = cookies.get("user")
  return user || null
}

export const setUser = (user: User | null): void => {
  if (user === null) {
    cookies.remove("user", { path: "/" })
  } else {
    cookies.set("user", JSON.stringify(user), {
      path: "/",
    })
  }
}

const UserContext = createContext<null | ContextType>(null)

export const Provider: FC = ({ children }) => {
  const user = useAppSelector((state) => state.account.user)
  const isAuth = useAppSelector((state) => state.account.isAuth)
  const isInit = useAppSelector((state) => state.account.isInit)
  const dispatch = useAppDispatch()

  const { setUserProfile, setIsAuth, setIsInit } = accountSlice.actions

  const login = useCallback(
    (user: User) => {
      dispatch(setUserProfile(user))
      dispatch(setIsAuth(true))
    },
    [dispatch, setIsAuth, setUserProfile],
  )

  const logout = useCallback(() => {
    dispatch(setUserProfile(null))
    dispatch(setIsAuth(false))
  }, [dispatch, setIsAuth, setUserProfile])

  useEffect(() => {
    const storedUser = getClientUser()
    if (storedUser !== null) {
      if (storedUser.isAdmin) {
        login(storedUser)
      }
    }
    dispatch(setIsInit(true))
  }, [dispatch, login, setIsInit])

  const contextValue = useMemo(
    () => ({
      login,
      logout,
      user,
      isAuth,
      isInit,
    }),
    [login, logout, user, isAuth, isInit],
  )

  return (
    <UserContext.Provider value={contextValue}>{children}</UserContext.Provider>
  )
}

export const useAuth = (): ContextType => {
  const userContext = useContext(UserContext)

  if (userContext === null) {
    throw new Error("User context have to be provided")
  }

  return {
    user: userContext.user,
    login: userContext.login,
    logout: userContext.logout,
    isAuth: userContext.isAuth,
    isInit: userContext.isInit,
  } as const
}
