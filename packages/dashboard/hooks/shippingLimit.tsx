import { ApiError, Limit, ResponseLimit } from "../../contracts/contracts"
import { useCallback, useEffect, useState } from "react"
import { useAppDispatch } from "./redux"
import { useMutation } from "react-query"
import { shippingMethodsAPI } from "../api/shippingMethodsAPI"
import { convertPennyToRub } from "../utils/helpers"
import { shippingMethodsSlice } from "../store/reducers/shippingMethodsSlice"
import { openNotification } from "../components/Notification"

export type UseShippingType = (props?: {
  limit?: ResponseLimit
  methodAlias?: string
  onCreate?: () => void
  onRemove?: () => void
}) => {
  currentLimit: null | ResponseLimit
  errorMessage?: string
  create: (limit: Limit) => void
  edit: (limit: ResponseLimit) => void
  remove: (externalLimitId?: number) => void
  orderMinCostRub: number
  shippingCostRub: number
  isLoadingEdit: boolean
  isLoadingCreate: boolean
  isLoadingRemove: boolean
  removeConfirm: {
    visible: boolean
    showPopup: () => void
    handleOk: (externalLimitId?: number) => void
    handleCancel: () => void
  }
}

export const useShippingLimit: UseShippingType = ({
  limit,
  methodAlias,
  onCreate,
  onRemove,
} = {}) => {
  const [currentLimit, setCurrentLimit] = useState<null | ResponseLimit>(
    limit || null,
  )
  const [error, setError] = useState<string | undefined>()
  const { updateLimit, removeLimit } = shippingMethodsSlice.actions
  const dispatch = useAppDispatch()

  useEffect(() => {
    setCurrentLimit(limit || null)
  }, [limit])

  const { mutate: editMutate, isLoading: isLoadingEdit } = useMutation(
    shippingMethodsAPI.editMethodLimit,
    {
      onSuccess: (response, request) => {
        dispatch(
          updateLimit({
            ...request,
          }),
        )
        openNotification({
          message: "Ограничение успешно изменено",
          iconVariant: "success",
        })
      },
      onError: (error: ApiError) => {
        setError(error.message || "Произошла ошибка в изменении")
        openNotification({
          message: error.message || "Произошла ошибка",
          iconVariant: "error",
        })
      },
    },
  )

  const { mutate: createMutate, isLoading: isLoadingCreate } = useMutation(
    shippingMethodsAPI.createMethodLimit,
    {
      onSuccess: (response, request) => {
        dispatch(
          updateLimit({
            ...request,
            id: response.id,
          }),
        )
        if (onCreate) {
          onCreate()
        }
        openNotification({
          message: "Ограничение успешно создано",
          iconVariant: "success",
        })
      },
      onError: (error: ApiError) => {
        setError(error.message || "Произошла ошибка в создании")
        openNotification({
          message: error.message || "Произошла ошибка",
          iconVariant: "error",
        })
      },
    },
  )

  const { mutate: removeMutate, isLoading: isLoadingRemove } = useMutation(
    shippingMethodsAPI.removeMethodLimit,
    {
      onSuccess: (response, request) => {
        dispatch(
          removeLimit({
            id: request.id,
          }),
        )
        if (onRemove) {
          onRemove()
        }
        removeHandleCancel()
        openNotification({
          message: "Ограничение успешно удалено",
          iconVariant: "success",
        })
      },
      onError: (error: ApiError) => {
        setError(error.message || "Произошла ошибка в удалении")
        openNotification({
          message: error.message || "Произошла ошибка",
          iconVariant: "error",
        })
      },
    },
  )

  const create = useCallback(
    (limit: Limit) => {
      if (methodAlias === undefined) {
        setError("Метод не найден")
        return
      }
      createMutate({
        ...limit,
        alias: methodAlias,
      })
    },
    [createMutate, methodAlias],
  )

  const edit = useCallback(
    (limit: ResponseLimit) => {
      if (limit?.id === undefined || methodAlias === undefined) {
        setError("Метод или ограничение не найдено")
        return
      }
      editMutate({
        ...limit,
        alias: methodAlias,
        id: limit.id,
      })
    },
    [editMutate, methodAlias],
  )

  const remove = useCallback(
    (externalLimitId?: number) => {
      const limitId = externalLimitId || currentLimit?.id
      if (limitId === undefined || methodAlias === undefined) {
        setError("Метод или ограничение не найдено")
        return
      }

      removeMutate({
        id: limitId,
        alias: methodAlias,
      })
    },
    [currentLimit?.id, methodAlias, removeMutate],
  )

  const [visibleConfirmRemove, setVisibleConfirmRemove] = useState(false)

  const showConfirmRemove = () => {
    setVisibleConfirmRemove(true)
  }

  const removeHandleOk = (externalLimitId?: number) => {
    remove(externalLimitId)
  }

  const removeHandleCancel = () => {
    setVisibleConfirmRemove(false)
  }

  return {
    currentLimit,
    create: create,
    edit: edit,
    remove: remove,
    removeConfirm: {
      visible: visibleConfirmRemove,
      showPopup: showConfirmRemove,
      handleOk: removeHandleOk,
      handleCancel: removeHandleCancel,
    },
    errorMessage: error,
    orderMinCostRub: convertPennyToRub(limit?.order_min_cost),
    shippingCostRub: convertPennyToRub(limit?.shipping_cost),
    isLoadingEdit,
    isLoadingCreate,
    isLoadingRemove,
  }
}
