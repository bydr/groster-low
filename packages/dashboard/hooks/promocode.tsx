import { useMutation } from "react-query"
import { promocodeAPI } from "../api/promocodeAPI"
import { openNotification } from "../components/Notification"
import { Button } from "antd"
import { ROUTES } from "../utils/constants"
import { useCallback, useEffect, useState } from "react"
import { ApiError } from "next/dist/server/api-utils"
import { CodeResponse } from "../../contracts/contracts"
import { convertPennyToRub } from "../utils/helpers"
import moment from "moment"
import "moment/locale/ru"
import { useClipboardCopy } from "./clipboardCopy"

const DATE_FORMAT = "YYYY-MM-DD"

type UsePromocodeType = (props?: {
  promocode?: CodeResponse
  onCreate?: () => void
}) => {
  edit: (code: Omit<CodeResponse, "id"> & { id: number }) => void
  create: (code: Omit<CodeResponse, "id">) => void
  isLoadingCreate: boolean
  isLoadingEdit: boolean
  errorMessage?: string
  valueAsString?: string
  finishAsString?: string
  startAsString?: string
  reusableAsString?: string
  copyCode: () => void
  isCopiedCode: boolean
}

export const usePromocode: UsePromocodeType = (props = {}) => {
  const { promocode, onCreate } = props
  const [error, setError] = useState<string | undefined>()

  const [typeMark, setTypeMark] = useState<"%" | "₽" | undefined>()
  const [convertedVal, setConvertedVal] = useState<undefined | number>()
  const [reusableAsString, setReusableAsString] = useState<undefined | string>()
  const { isCopied, handleCopyClick } = useClipboardCopy()

  const { mutate: createMutate, isLoading: isLoadingCreate } = useMutation(
    promocodeAPI.createPromocode,
    {
      onSuccess: () => {
        if (onCreate) {
          onCreate()
        }
        openNotification({
          message: "Промокод успешно создан",
          description: (
            <>
              Можете перейти на страницу с промокодами.{" "}
              <Button
                type={"primary"}
                size={"small"}
                href={`${ROUTES.base}${ROUTES.promocodes}`}
              >
                Перейти
              </Button>
            </>
          ),
          iconVariant: "success",
        })
      },
      onError: (error: ApiError) => {
        setError(error.message || "Произошла ошибка")
      },
      onMutate: () => {
        setError(undefined)
      },
    },
  )

  const { mutate: editMutate, isLoading: isLoadingEdit } = useMutation(
    promocodeAPI.editPromocode,
    {
      onSuccess: () => {
        openNotification({
          message: "Промокод успешно изменен",
          description: (
            <>
              Можете перейти на страницу с промокодами.{" "}
              <Button
                type={"primary"}
                size={"small"}
                href={`${ROUTES.base}${ROUTES.promocodes}`}
              >
                Перейти
              </Button>
            </>
          ),
          iconVariant: "success",
        })
      },
      onError: (error: ApiError) => {
        setError(error.message || "Произошла ошибка")
      },
      onMutate: () => {
        setError(undefined)
      },
    },
  )

  const create = useCallback(
    (code: Omit<CodeResponse, "id">) => {
      createMutate({
        ...code,
        categories: !!code?.categories ? code.categories.join(",") : "",
      })
    },
    [createMutate],
  )

  const edit = useCallback(
    (code: Omit<CodeResponse, "id"> & { id: number }) => {
      if (!code.id) {
        setError("Промокод не найден")
        return
      }
      editMutate({
        ...code,
        categories: !!code?.categories ? code.categories.join(",") : "",
      })
    },
    [editMutate],
  )

  const copyCode = useCallback(() => {
    if (promocode?.code === undefined) {
      return
    }
    handleCopyClick(promocode.code)
  }, [handleCopyClick, promocode?.code])

  useEffect(() => {
    switch (promocode?.type) {
      case 1: {
        setTypeMark("%")
        setConvertedVal(promocode.value)
        break
      }
      case 2: {
        setTypeMark("₽")
        setConvertedVal(convertPennyToRub(promocode.value))
        break
      }
      default: {
        setTypeMark(undefined)
        setConvertedVal(undefined)
        break
      }
    }
  }, [promocode?.type, promocode?.value])

  useEffect(() => {
    setReusableAsString(promocode?.reusable ? "многоразовый" : "одноразовый")
  }, [promocode?.reusable])

  return {
    create,
    edit,
    errorMessage: error,
    isLoadingEdit,
    isLoadingCreate,
    valueAsString:
      convertedVal !== undefined ? `${convertedVal} ${typeMark}` : undefined,
    finishAsString:
      promocode?.finish !== undefined
        ? moment(promocode.finish, DATE_FORMAT).format("LL")
        : undefined,
    startAsString:
      promocode?.start !== undefined
        ? moment(promocode.start, DATE_FORMAT).format("LL")
        : undefined,
    reusableAsString,
    isCopiedCode: isCopied,
    copyCode: copyCode,
  }
}
