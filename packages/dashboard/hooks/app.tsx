import { createContext, FC, useContext, useMemo, useState } from "react"

type ContextType = {
  pageTitle: string | null
  setPageTitle: (title: null | string) => void
}

const AppContext = createContext<null | ContextType>(null)

export const Provider: FC = ({ children }) => {
  const [pageTitle, setPageTitle] = useState<string | null>(null)

  const contextValue = useMemo(
    () => ({
      pageTitle,
      setPageTitle,
    }),
    [pageTitle],
  )

  return (
    <AppContext.Provider value={contextValue}>{children}</AppContext.Provider>
  )
}

export const useApp = (): ContextType => {
  const appContext = useContext(AppContext)

  if (appContext === null) {
    throw new Error("App context have to be provided")
  }
  return {
    pageTitle: appContext.pageTitle,
    setPageTitle: appContext.setPageTitle,
  }
}
