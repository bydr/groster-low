import { useState } from "react"
import { useMutation, useQueryClient } from "react-query"
import { faqAPI } from "../api/faqAPI"
import { openNotification } from "../components/Notification"
import { ApiError } from "next/dist/server/api-utils"
import { AdminBlock } from "../../contracts/contracts"

export const useBlockFaq: (props?: {
  onCreate?: () => void
  onEdit?: () => void
  onRemove?: () => void
}) => {
  isLoadingCreate: boolean
  isLoadingEdit: boolean
  isLoadingRemove: boolean
  create: (data: AdminBlock) => void
  edit: (data: AdminBlock & { id: number }) => void
  remove: (data: { id: number }) => void
  errorMessage?: string
  removeConfirm: {
    visible: boolean
    showPopup: () => void
    handleOk: (id?: number) => void
    handleCancel: () => void
  }
} = (props = {}) => {
  const { onCreate, onEdit, onRemove } = props
  const [error, setError] = useState<string | undefined>()
  const queryClient = useQueryClient()

  const { mutate: createMutate, isLoading: isLoadingCreate } = useMutation(
    faqAPI.createBlock,
    {
      onSuccess: async () => {
        await queryClient.refetchQueries(["blocksFaq"])
        removeHandleCancel()
        if (onCreate) {
          onCreate()
        }
        openNotification({
          message: "Блок успешно создан",
          iconVariant: "success",
        })
      },
      onError: (error: ApiError) => {
        setError(error.message || "Произошла ошибка")
        openNotification({
          message: error.message || "Произошла ошибка",
          iconVariant: "error",
        })
      },
      onMutate: () => {
        setError(undefined)
      },
    },
  )

  const { mutate: editMutate, isLoading: isLoadingEdit } = useMutation(
    faqAPI.editBlock,
    {
      onSuccess: async () => {
        await queryClient.refetchQueries(["blocksFaq"])
        removeHandleCancel()
        if (onEdit) {
          onEdit()
        }
        openNotification({
          message: "Блок успешно изменен",
          iconVariant: "success",
        })
      },
      onError: (error: ApiError) => {
        setError(error.message || "Произошла ошибка")
        openNotification({
          message: error.message || "Произошла ошибка",
          iconVariant: "error",
        })
      },
      onMutate: () => {
        setError(undefined)
      },
    },
  )

  const { mutate: removeMutate, isLoading: isLoadingRemove } = useMutation(
    faqAPI.removeBlock,
    {
      onSuccess: async () => {
        await queryClient.refetchQueries(["blocksFaq"])
        removeHandleCancel()
        if (onRemove) {
          onRemove()
        }
        openNotification({
          message: "Блок успешно удален",
          iconVariant: "success",
        })
      },
      onError: (error: ApiError) => {
        setError(error.message || "Произошла ошибка")
        openNotification({
          message: error.message || "Произошла ошибка",
          iconVariant: "error",
        })
      },
      onMutate: () => {
        setError(undefined)
      },
    },
  )

  const [visibleConfirmRemove, setVisibleConfirmRemove] = useState(false)

  const showConfirmRemove = () => {
    setVisibleConfirmRemove(true)
  }

  const removeHandleOk = (id?: number) => {
    if (!id) {
      setError("Блок не найден")
      return
    }
    removeMutate({
      id: id,
    })
  }

  const removeHandleCancel = () => {
    setVisibleConfirmRemove(false)
  }

  return {
    isLoadingCreate,
    isLoadingEdit,
    isLoadingRemove,
    create: createMutate,
    edit: editMutate,
    remove: removeMutate,
    errorMessage: error,
    removeConfirm: {
      visible: visibleConfirmRemove,
      showPopup: showConfirmRemove,
      handleOk: removeHandleOk,
      handleCancel: removeHandleCancel,
    },
  }
}
