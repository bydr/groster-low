import {
  AdminPaymentMethod,
  ApiError,
  PaymentMethodCreateRequest,
} from "../../contracts/contracts"
import { useState } from "react"
import { useAppDispatch } from "./redux"
import { useMutation } from "react-query"
import { paymentsAPI } from "../api/paymentsAPI"
import { openNotification } from "../components/Notification"
import { Button } from "antd"
import { ROUTES } from "../utils/constants"
import { paymentsSlice } from "../store/reducers/paymentsSlice"

type UsePaymentReturnType = {
  isLoadingCreate: boolean
  isLoadingEdit: boolean
  errorMessage?: string
  enable: () => void
  disable: () => void
  create: (payment: PaymentMethodCreateRequest) => void
  edit: (payment: AdminPaymentMethod) => void
}

type UsePaymentPropsType = {
  payment?: AdminPaymentMethod
}

export const usePayment: (
  props?: UsePaymentPropsType,
) => UsePaymentReturnType = (props = {}) => {
  const { payment } = props
  const [error, setError] = useState<string | undefined>()
  const dispatch = useAppDispatch()
  const { updateMethod } = paymentsSlice.actions

  const { mutate: createMutate, isLoading: isLoadingCreate } = useMutation(
    paymentsAPI.createPaymentMethod,
    {
      onSuccess: () => {
        openNotification({
          message: "Метод оплаты успешно создан",
          description: (
            <>
              Можете перейти на страницу с методами оплаты.{" "}
              <Button
                type={"primary"}
                size={"small"}
                href={`${ROUTES.base}${ROUTES.payments}`}
              >
                Перейти
              </Button>
            </>
          ),
          iconVariant: "success",
        })
      },
      onError: (error: ApiError) => {
        setError(error.message || "Произошла ошибка")
      },
      onMutate: () => {
        setError(undefined)
      },
    },
  )
  const { mutate: editMutate, isLoading: isLoadingEdit } = useMutation(
    paymentsAPI.editPaymentMethod,
    {
      onSuccess: (response, request) => {
        dispatch(updateMethod(request))
        openNotification({
          message: "Метод оплаты успешно изменен",
          description: (
            <>
              Можете перейти на страницу с методами оплаты.{" "}
              <Button
                type={"primary"}
                size={"small"}
                href={`${ROUTES.base}${ROUTES.payments}`}
              >
                Перейти
              </Button>
            </>
          ),
          iconVariant: "success",
        })
      },
      onError: (error: ApiError) => {
        setError(error.message || "Произошла ошибка")
      },
      onMutate: () => {
        setError(undefined)
      },
    },
  )

  const enableHandle = () => {
    if (payment !== undefined) {
      editMutate({
        ...payment,
        is_active: true,
      })
    }
  }

  const disableHandle = () => {
    if (payment !== undefined) {
      editMutate({
        ...payment,
        is_active: false,
      })
    }
  }

  return {
    create: createMutate,
    isLoadingCreate: isLoadingCreate,
    errorMessage: error,
    edit: editMutate,
    isLoadingEdit,
    enable: enableHandle,
    disable: disableHandle,
  }
}
