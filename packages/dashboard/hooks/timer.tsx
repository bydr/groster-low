import { useCallback, useEffect, useMemo, useRef, useState } from "react"

export const useTimer: () => {
  lifeTime: number
  setLifeTime: (time: number) => void
  resetTimer: () => void
  timeFormatMinutes: string
} = () => {
  const [lifeTime, setLifeTime] = useState<number>(0)
  const timer = useRef<number | undefined>(undefined)

  const resetTimer = useCallback(() => {
    clearInterval(timer.current)
  }, [])

  const timeFormatMinutes = useMemo(
    () =>
      String(Math.floor(lifeTime / 60)) +
      ":" +
      ("0" + String(Math.floor(lifeTime % 60))).slice(-2),
    [lifeTime],
  )

  useEffect(() => {
    timer.current =
      lifeTime > 0
        ? Number(
            setInterval(() => {
              setLifeTime(lifeTime - 1)
            }, 1000),
          )
        : undefined
    return () => resetTimer()
  }, [lifeTime])

  return {
    lifeTime,
    setLifeTime,
    timeFormatMinutes,
    resetTimer,
  }
}
