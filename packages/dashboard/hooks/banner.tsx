import { RequestBanner } from "../../contracts/contracts"
import { useState } from "react"
import { useMutation, useQueryClient } from "react-query"
import { openNotification } from "../components/Notification"
import { ApiError } from "next/dist/server/api-utils"
import { bannersAPI } from "../api/bannersAPI"

export const useBanner: (props?: {
  onCreate?: () => void
  onEdit?: () => void
  onRemove?: () => void
}) => {
  isLoadingCreate: boolean
  isLoadingEdit: boolean
  isLoadingRemove: boolean
  create: (data: RequestBanner) => void
  edit: (data: RequestBanner & { id: number }) => void
  remove: (data: { id: number }) => void
  errorMessage?: string
  removeConfirm: {
    visible: boolean
    showPopup: () => void
    handleOk: (id?: number) => void
    handleCancel: () => void
  }
} = (props = {}) => {
  const { onCreate, onEdit, onRemove } = props
  const [error, setError] = useState<string | undefined>()
  const queryClient = useQueryClient()

  const { mutate: createMutate, isLoading: isLoadingCreate } = useMutation(
    bannersAPI.createBanner,
    {
      onSuccess: async () => {
        await queryClient.refetchQueries(["banners"])
        removeHandleCancel()
        if (onCreate) {
          onCreate()
        }
        openNotification({
          message: "Баннер успешно добавлен",
          iconVariant: "success",
        })
      },
      onError: async (error: ApiError) => {
        await queryClient.refetchQueries(["banners"])
        setError(error.message || "Произошла ошибка")
        openNotification({
          message: error.message || "Произошла ошибка",
          iconVariant: "error",
        })
      },
      onMutate: () => {
        setError(undefined)
      },
    },
  )

  const { mutate: editMutate, isLoading: isLoadingEdit } = useMutation(
    bannersAPI.editBanner,
    {
      onSuccess: async () => {
        await queryClient.refetchQueries(["banners"])
        removeHandleCancel()
        if (onEdit) {
          onEdit()
        }
        openNotification({
          message: "Баннер успешно изменен",
          iconVariant: "success",
        })
      },
      onError: (error: ApiError) => {
        setError(error.message || "Произошла ошибка")
        openNotification({
          message: error.message || "Произошла ошибка",
          iconVariant: "error",
        })
      },
      onMutate: () => {
        setError(undefined)
      },
    },
  )

  const { mutate: removeMutate, isLoading: isLoadingRemove } = useMutation(
    bannersAPI.removeBanner,
    {
      onSuccess: async () => {
        await queryClient.refetchQueries(["banners"])
        removeHandleCancel()
        if (onRemove) {
          onRemove()
        }
        openNotification({
          message: "Баннер успешно удален",
          iconVariant: "success",
        })
      },
      onError: (error: ApiError) => {
        setError(error.message || "Произошла ошибка")
        openNotification({
          message: error.message || "Произошла ошибка",
          iconVariant: "error",
        })
      },
      onMutate: () => {
        setError(undefined)
      },
    },
  )

  const [visibleConfirmRemove, setVisibleConfirmRemove] = useState(false)

  const showConfirmRemove = () => {
    setVisibleConfirmRemove(true)
  }

  const removeHandleOk = (id?: number) => {
    if (!id) {
      setError("Баннер не найден")
      return
    }
    removeMutate({
      id: id,
    })
  }

  const removeHandleCancel = () => {
    setVisibleConfirmRemove(false)
  }

  return {
    isLoadingCreate,
    isLoadingEdit,
    isLoadingRemove,
    create: createMutate,
    edit: editMutate,
    remove: removeMutate,
    errorMessage: error,
    removeConfirm: {
      visible: visibleConfirmRemove,
      showPopup: showConfirmRemove,
      handleOk: removeHandleOk,
      handleCancel: removeHandleCancel,
    },
  }
}
