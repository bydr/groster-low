import { useClipboardCopy } from "./clipboardCopy"
import { useCallback, useState } from "react"
import { ApiError, ShopAdmin } from "../../contracts/contracts"
import { useMutation } from "react-query"
import { shopsAPI } from "../api/shopsAPI"
import { openNotification } from "../components/Notification"
import { Button } from "antd"
import { ROUTES } from "../utils/constants"
import { shopsSlice } from "../store/reducers/shopsSlice"
import { useAppDispatch, useAppSelector } from "./redux"

type UseShopReturnType = {
  copyCoordsHandle: () => void
  coordsIsCopied: boolean
  isLoadingEdit: boolean
  coordsString?: string
  errorMessage?: string
  enable: () => void
  disable: () => void
  edit: (data: ShopAdmin & { uuid: string }) => void
}
type UseShopPropsType = {
  shop?: ShopAdmin & { uuid?: string }
}
export const useShop: (props: UseShopPropsType) => UseShopReturnType = ({
  shop,
}) => {
  const { isCopied, handleCopyClick } = useClipboardCopy()
  const [error, setError] = useState<string | undefined>()
  const dispatch = useAppDispatch()
  const { updateShop, setCurrentShop } = shopsSlice.actions
  const currentShop = useAppSelector((state) => state.shop.currentShop)

  const copyCoordsHandle = useCallback(() => {
    handleCopyClick(
      [shop?.coordinates?.latitude, shop?.coordinates?.longitude].join(", "),
    )
  }, [
    handleCopyClick,
    shop?.coordinates?.latitude,
    shop?.coordinates?.longitude,
  ])

  const { mutate: editShopMutate, isLoading: isLoadingEdit } = useMutation(
    shopsAPI.editShop,
    {
      onSuccess: (response) => {
        dispatch(updateShop(response))
        if (currentShop !== null) {
          dispatch(setCurrentShop({ ...response }))
        }
        openNotification({
          message: "Данные успешно изменены",
          description: (
            <>
              Можете перейти на страницу со списком магазинов.{" "}
              <Button
                type={"primary"}
                size={"small"}
                href={`${ROUTES.base}${ROUTES.shops}`}
              >
                Перейти
              </Button>
            </>
          ),
          iconVariant: "success",
        })
      },
      onError: (error: ApiError) => {
        const msg = error.message || "Произошла ошибка"
        setError(msg)
        openNotification({
          message: msg,
          iconVariant: "error",
        })
      },
      onMutate: () => {
        setError(undefined)
      },
    },
  )

  const enableHandle = () => {
    editShopMutate({
      ...shop,
      uuid: shop?.uuid || "",
      is_active: true,
    })
  }

  const disableHandle = () => {
    editShopMutate({
      ...shop,
      uuid: shop?.uuid || "",
      is_active: false,
    })
  }

  return {
    copyCoordsHandle,
    coordsIsCopied: isCopied,
    coordsString: [shop?.coordinates?.latitude, shop?.coordinates?.longitude]
      .filter((i) => i !== undefined)
      .join(","),
    isLoadingEdit: isLoadingEdit,
    errorMessage: error,
    enable: enableHandle,
    disable: disableHandle,
    edit: editShopMutate,
  }
}
