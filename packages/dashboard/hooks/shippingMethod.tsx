import {
  ApiError,
  ShippingMethodCreateRequest,
  ShippingMethodDetail,
  ShippingMethodEditRequest,
} from "../../contracts/contracts"
import { useState } from "react"
import { useAppDispatch } from "./redux"
import { useMutation } from "react-query"
import { openNotification } from "../components/Notification"
import { Button } from "antd"
import { ROUTES } from "../utils/constants"
import { shippingMethodsAPI } from "../api/shippingMethodsAPI"
import { shippingMethodsSlice } from "../store/reducers/shippingMethodsSlice"

type UseShippingMethodReturnType = {
  isLoadingCreate: boolean
  isLoadingEdit: boolean
  errorMessage?: string
  enable: () => void
  disable: () => void
  create: (data: ShippingMethodCreateRequest) => void
  edit: (data: ShippingMethodEditRequest) => void
}

type UseShippingMethodPropsType = {
  shippingMethod?: ShippingMethodDetail
}

export const useShippingMethod: (
  props?: UseShippingMethodPropsType,
) => UseShippingMethodReturnType = (props = {}) => {
  const { shippingMethod } = props
  const [error, setError] = useState<string | undefined>()
  const dispatch = useAppDispatch()
  const { updateMethod } = shippingMethodsSlice.actions

  const { mutate: createMutate, isLoading: isLoadingCreate } = useMutation(
    shippingMethodsAPI.createShippingMethod,
    {
      onSuccess: () => {
        openNotification({
          message: "Метод доставки успешно создан",
          description: (
            <>
              Можете перейти на страницу с методами доставки.{" "}
              <Button
                type={"primary"}
                size={"small"}
                href={`${ROUTES.base}${ROUTES.shippingMethods}`}
              >
                Перейти
              </Button>
            </>
          ),
          iconVariant: "success",
        })
      },
      onError: (error: ApiError) => {
        setError(error.message || "Произошла ошибка")
      },
      onMutate: () => {
        setError(undefined)
      },
    },
  )
  const { mutate: editMethodMutate, isLoading: isLoadingEdit } = useMutation(
    shippingMethodsAPI.editShippingMethod,
    {
      onSuccess: (response, request) => {
        dispatch(
          updateMethod({
            ...request,
            alias: response.alias,
          }),
        )
        openNotification({
          message: "Метод оплаты успешно изменен",
          description: (
            <>
              Можете перейти на страницу с методами доставки.{" "}
              <Button
                type={"primary"}
                size={"small"}
                href={`${ROUTES.base}${ROUTES.shippingMethods}`}
              >
                Перейти
              </Button>
            </>
          ),
          iconVariant: "success",
        })
      },
      onError: (error: ApiError) => {
        setError(error.message || "Произошла ошибка")
      },
      onMutate: () => {
        setError(undefined)
      },
    },
  )

  const enableHandle = () => {
    if (!shippingMethod || !shippingMethod.alias) {
      return
    }
    editMethodMutate({
      ...shippingMethod,
      alias: shippingMethod.alias,
      is_active: true,
    })
  }

  const disableHandle = () => {
    if (!shippingMethod || !shippingMethod.alias) {
      return
    }
    editMethodMutate({
      ...shippingMethod,
      alias: shippingMethod.alias,
      is_active: false,
    })
  }

  return {
    create: createMutate,
    isLoadingCreate: isLoadingCreate,
    errorMessage: error,
    edit: editMethodMutate,
    isLoadingEdit,
    enable: enableHandle,
    disable: disableHandle,
  }
}
