import { Category } from "../../contracts/contracts"
import { CategoriesTreeStateType, ICategoryTreeItem } from "../types/types"
import { RcFile } from "antd/lib/upload"
import { BASE_URL, BASE_VERSION_URL } from "./constants"

export const convertPennyToRub = (price?: number): number => {
  return (price || 0) / 100
}
export const convertRubToPenny = (price?: number): number => {
  return (price || 0) * 100
}

export const createCategoriesTree = (
  categories: Category[],
): CategoriesTreeStateType => {
  if (!categories) {
    return {
      tree: null,
      compared: null,
    }
  }

  const _categories = [...categories]
  const categoriesTree = {} as Record<string, ICategoryTreeItem>
  const compareChildMain = {} as Record<string, number>

  const parentTree = (
    current: ICategoryTreeItem,
    parentCategory: ICategoryTreeItem,
  ) => {
    if (
      Object.keys(parentCategory["children"]).some(
        (key) => +key === current.parent,
      )
    ) {
      if (!!current.parent && !!current.id) {
        parentCategory["children"][current.parent]["children"][current.id] =
          current
      }
      return
    }

    for (const key of Object.keys(parentCategory["children"])) {
      parentTree(current, { ...parentCategory["children"][key] })
    }
  }

  const sortedArr = _categories.sort((a, b) => {
    if ((a?.parent || 0) > (b?.parent || 0)) {
      return 1
    }
    if ((a?.parent || 0) < (b?.parent || 0)) {
      return -1
    }
    // a должно быть равным b
    return 0
  })

  for (const cat of sortedArr) {
    const category = {
      ...cat,
      children: {},
    } as ICategoryTreeItem

    if (category.id === undefined || category.parent === undefined) {
      continue
    }

    if (category.parent === 0) {
      categoriesTree[category.id] = category
      continue
    }

    if (compareChildMain[category.parent] === undefined) {
      compareChildMain[category.id] = category.parent
      if (categoriesTree[category.parent] === undefined) {
        categoriesTree[category.parent] = {
          ...categoriesTree[category.parent],
          children: {},
        }
      }
      categoriesTree[category.parent].children[category.id] = category
    } else {
      compareChildMain[category.id] = compareChildMain[category.parent]
      parentTree(category, categoriesTree[compareChildMain[category.id]])
    }
  }

  return {
    tree: categoriesTree,
    compared: compareChildMain,
  }
}

export const getBase64 = ({
  callback,
  img,
}: {
  callback: (param: string | null) => void
  img?: RcFile
}): void => {
  if (!img) {
    return
  }
  const reader = new FileReader()
  reader.addEventListener("load", () =>
    callback(reader.result instanceof ArrayBuffer ? null : reader.result),
  )
  reader.readAsDataURL(img)
}

export const getAbsolutePath = (isServer?: boolean): string =>
  isServer ? `${BASE_URL}${BASE_VERSION_URL}` : ""
