export const ROUTES = {
  base: process.env.NEXT_PUBLIC_BASE_URL || "/dashboard",
  categories: "/catalog/categories",
  tags: "/catalog/tags",
  shops: "/shops",
  settings: "/settings",
  payments: "/payments",
  shippingMethods: "/shipping-methods",
  promocodes: "/promocodes",
  faq: "/faq",
  banners: "/banners",
  home: "/home",
}

export const MASK_PHONE = "+7 (999) 999 99-99"

export const CURRENCY = "₽"

export const TIME_FORMAT = "HH:mm"
export const DATE_FORMAT = "YYYY-MM-DD"

export const BASE_URL =
  process.env.NEXT_PUBLIC_API_BASE_URL || `https://api.groster.me`
export const BASE_VERSION_URL =
  process.env.NEXT_PUBLIC_API_BASE_VERSION || "/api/v1"
