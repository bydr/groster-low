import { DetailedHTMLProps, MouseEvent, ReactElement, ReactNode } from "react"
import { IconNameType } from "../components/Icon"
import {
  BannerApiType,
  CatalogResponse,
  Category,
  Payer,
  ProductDetailResponse,
  ProductSpecificationType,
  RequestBanner,
  Shop,
  Tag,
  V1AccountOrderListOrdersListParams,
  V1AccountOrderListProductsListParams,
} from "../../contracts/contracts"
import { ButtonVariantsType } from "../components/Button/Button"
import { NextPage } from "next"
import {
  IAvailableParams,
  IFilterParamsChilds,
  IFilterParamsParents,
  PriceRangeItemType,
} from "../store/reducers/catalogSlice"
import { OrderStateNumberType } from "../components/Account/History/Orders/Order/State"
import { ImageProps } from "next/image"

export interface VariantPropsType<T> {
  variant?: T
  children?: ReactNode
}
export type ButtonTagsType = "a" | "button" | "div"

export type RadioGroupItemsType = {
  value: string | number
  message: string
}

export const VIEW_PRODUCTS_GRID = "grid"
export const VIEW_PRODUCTS_LIST = "list"
export const VIEW_PRODUCTS_CHECKOUT = "checkout"
export const VIEW_PRODUCTS_SAMPLE = "sample"
export const VIEW_PRODUCTS_CHILD = "children"
export const VIEW_PRODUCTS_SLIDE = "piece"
export const VIEW_PRODUCTS_CHECKOUT_MINI = "checkout_mini"
export const VIEW_PRODUCTS_SAMPLE_MINI = "sample_mini"
export const VIEW_PRODUCTS_ORDER = "order"
export const VIEW_PRODUCTS_SAMPLE_ORDER = "sample_order"
export type ViewProductsType =
  | typeof VIEW_PRODUCTS_GRID
  | typeof VIEW_PRODUCTS_LIST
  | typeof VIEW_PRODUCTS_CHECKOUT
  | typeof VIEW_PRODUCTS_SAMPLE
  | typeof VIEW_PRODUCTS_CHILD
  | typeof VIEW_PRODUCTS_SLIDE
  | typeof VIEW_PRODUCTS_CHECKOUT_MINI
  | typeof VIEW_PRODUCTS_SAMPLE_MINI
  | typeof VIEW_PRODUCTS_ORDER
  | typeof VIEW_PRODUCTS_SAMPLE_ORDER
export type ViewProductImageType =
  | typeof VIEW_PRODUCTS_GRID
  | typeof VIEW_PRODUCTS_LIST

export type UnitMeasureType = string
export type CurrencyType = "₽"
export type PropertiesType = {
  name?: string
  value?: string
  uuidParent?: string
  uuidValue?: string
}[]
export type AvailableStatusType = "not" | "few" | "many"

export type VariationShortType = {
  mainImage: string | null
  alias: string
  name: string
  fullName: string
  uuid: string
  totalQty: number
}

export type ProductType = ProductDetailResponse
export type DetailProductType = ProductType & {
  variation?: {
    selected?: {
      uuid?: string
      paramName?: string
      name?: string
    }
    model?: VariationShortType[]
    parent?: {
      name?: string
      description?: string
    }
  }
}
export type ProductWithChildType = DetailProductType & {
  child?: Record<string, ProductWithChildType>
}
export type SampleType = Pick<
  ProductType,
  | "name"
  | "uuid"
  | "price"
  | "unit"
  | "total_qty"
  | "alias"
  | "parent"
  | "fast_qty"
  | "images"
>

export type IconPositionType = "left" | "right"

export interface ListItemType {
  title: string
  path: string
}
export interface NavItemType extends ListItemType {
  subItems?: ListItemType[]
  isScroll?: boolean
  isLink?: boolean
}
export interface NavType {
  title: string
  items: NavItemType[]
}
export type LinkItemType = {
  icon: IconNameType
  path: string
  title?: string | ReactNode
  target?: string
  fillIcon?: string
}

export interface ICategoryTreeItem extends Category {
  children: Record<string, ICategoryTreeItem>
}
export interface CategoriesTreeStateType {
  tree: Record<string, ICategoryTreeItem> | null
  compared: Record<string, number> | null
}
export interface CategoriesStateType extends CategoriesTreeStateType {
  fetched: Record<string, Category> | null
  treeSorted: ICategoryTreeItem[]
}

export type CategoryItemPropsType = {
  updateCurrentCategoryId?: UpdateCurrentCategoryPopupIdType
  currentId: null | number
}

export type SortByAliasType =
  | "price_asc"
  | "price_desc"
  | "sort_name"
  | "popular"

export type SortType = {
  value: string
  name: string
  icon?: IconNameType
}

export type SortByType = Record<string, SortType> | null
export type SortBySelectedType = string | null

export interface ChangeQtyResponse {
  cart: string
  total_cost: number
  discount: number | null
}

export interface ChangeQtyProductResponse extends ChangeQtyResponse {
  addedQty: number
}

export interface ChangeQtySampleResponse extends ChangeQtyResponse {
  quantity: number
}

export interface SpecificationItemType extends ProductSpecificationType {
  isRemoved?: boolean
  isSampleRemoved?: boolean
  parent?: string
  child?: Record<string, SpecificationItemType>
  isAnimate?: boolean
  kit?: string[]
}

export type WithViewProductVariantType = {
  viewProductsVariant?: ViewProductsType
}

export type StoreType = {
  uuid?: string
  name?: string
  quantity?: number
  is_main?: boolean
}
export type StoreProductType = Omit<StoreType, "is_main"> & {
  isMain?: boolean
  availableStatus?: AvailableStatusType
  deliveryDate?: Date
}

export type PatternType = {
  value: RegExp
  message: string
}

export type ValidateRuleType = {
  value: number
  message: string
}

export type PayerListItemType = Payer

export type FieldVariantsType = "input" | "textarea"
export interface FieldVariantsPropsType
  extends VariantPropsType<FieldVariantsType> {
  name?: string
  id?: string
  placeholder?: string
  errorMessage?: string
  withAnimatingLabel?: boolean
  withButton?: boolean
  iconButton?: IconNameType
  iconField?: IconNameType
  textButton?: string
  isFetching?: boolean
  iconPositionButton?: IconPositionType
  variantButton?: ButtonVariantsType
  onClickButton?: (e: MouseEvent<HTMLButtonElement>) => void
  disabledButton?: boolean
  hint?: string
  classNameButton?: string
  withClean?: boolean
  cleanCb?: () => void
  required?: boolean
  isVisibleSelectedHint?: boolean
  selectedValue?: string
}

export type FieldProps = FieldVariantsPropsType &
  DetailedHTMLProps<
    React.InputHTMLAttributes<HTMLInputElement>,
    HTMLInputElement
  >

export type ShippingsType = "pickup" | "express" | "company" | "courier"

export type ReplacementListItem = {
  id: number
  title: string
  description: string
}

export type NextPageWithLayout<P = never> = NextPage<P> & {
  getLayout?: (page: ReactElement) => ReactNode
}

export type OfferOrderThankType = {
  url: string
  name: string
  price: number
  count: number
  currency: "RUB"
}

export type OrderThankType = {
  id?: string
  fio?: string
  phone?: string
  email?: string
  shippingMethod?: string
  paymentMethod?: string
  totalCost?: number
  promocode?: string | null
  discount?: number
  shippingCost?: number
  productsCost?: number
  shippingAddress?: string
  shippingDate?: string
  contact?: string
  offers?: OfferOrderThankType[]
}

export type LocationType = {
  region_type: string | null
  region: string | null
  city_type: string | null
  city: string | null
  region_kladr_id: string | null
}

export type LocationExtendsType = LocationType & {
  city_full: string | null
  region_full: string | null
}

export type LastOrderReturnType = {
  fio: string
  email: string
  phone: string
  bussiness_area: string
  shipping_method?: ShippingsType | null
  payer?: string | null
  shipping_address?: string | null
  payment_method?: string | null
  region?: string | null
  replacement?: number | null
}

export type ShopType = Shop & {
  uuid: string
}

export type RequestOrderListOrders = Omit<
  V1AccountOrderListOrdersListParams,
  "per_page"
> & { perPage: number }

export type RequestOrderListProducts = Omit<
  V1AccountOrderListProductsListParams,
  "per_page"
> & { perPage: number }

export type UpdateFilterQueryPropsType = {
  price?: PriceRangeItemType | null
  isEnabled?: boolean
  isFast?: boolean
  sortBySelected?: string
  page?: number
  perPage?: number
  checkedParamsKeysTotal?: string[]
  isScroll?: boolean
  store?: string[]
}

export type QueryFiltersCatalogType = {
  params?: string
  category?: string
  price?: string
  is_enabled?: "0" | "1"
  is_fast?: "0" | "1"
  sortby?: string
  page?: number
  per_page?: number
  store?: string
}

export type UpdateFilterHistoryQueryPropsType = {
  payer?: string
  category?: string
  year?: string
  page?: number
  perPage?: number
  state?: OrderStateNumberType
  checkedParamsKeysTotal?: string[]
}

export type QueryFiltersHistoryType = {
  page?: number
  per_page?: number
  category?: string
  year?: string
  payer?: string
  state?: OrderStateNumberType
}

export type UseFilterBaseReturnType<T, QT> = {
  query: QT | null
  availableParams: IAvailableParams | null
  params: IFilterParamsParents & IFilterParamsChilds
  checkedParamsKeysTotal: string[]
  updateFilterQuery: (props?: T) => void
  clearFilterQuery: () => void
}

export type CatalogResponseType = Omit<CatalogResponse, "params"> & {
  params?: Record<string, number>
}

export type ProductWithInOrderType = {
  inOrder?: boolean
}

export type ResponseOrderListProducts = {
  products?: string[]
  total?: number
}

export type ClearItemFilterPropsType = {
  keyFilter: string[]
  checked: boolean
  name?: string
}

export type TagType = { id?: number; name?: string; url?: string }
export type TagPayloadType = Tag

export type BannerViewPropsType = Pick<
  ImageProps,
  | "layout"
  | "width"
  | "height"
  | "src"
  | "objectFit"
  | "objectPosition"
  | "quality"
>

export type BannerType = BannerApiType & {
  image: {
    desktop: BannerViewPropsType
    tablet?: Partial<BannerViewPropsType>
    mobile?: Partial<BannerViewPropsType>
  }
  withHover?: boolean
} & Pick<ImageProps, "priority" | "quality" | "unoptimized">

export type BannerStringTypes =
  | "main_slider"
  | "main_single"
  | "catalog_products_single"
  | "catalog_under_products_single"

export type StoreBannersType = Record<BannerStringTypes, RequestBanner[]>

export type QueryCatalogType = {
  params?: string
  price?: string
  is_enabled?: "0" | "1"
  is_fast?: "0" | "1"
  sortby?: SortByAliasType
  per_page?: string
  page?: string
  category?: string
  store?: string
}

export type ShortCategoryType = Pick<Category, "uuid" | "name">

export type UpdateCurrentCategoryPopupIdType = (props: {
  id: number | null
  parent?: number
}) => void

export type ActionMapType<M extends { [index: string]: any }> = {
  [Key in keyof M]: M[Key] extends undefined
    ? {
        type: Key
      }
    : {
        type: Key
        payload: M[Key]
      }
}

export type ProductOfferType = {
  uuid: string
  name: string
  description: string
  image: string
  categories: string[]
  params: string[]
  properties: PropertiesType
  is_bestseller: boolean
  is_kit: boolean
  is_kit_child: boolean
  total_qty: number
  alias: string
  code: string
  price: number
  keywords: string | null
  business_areas: string[]
}

export type FormConfigExtendsType = {
  successMessage?: string
  cbOnSubmit?: () => void
}
