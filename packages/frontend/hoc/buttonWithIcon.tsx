import { Icon } from "../components/Icon"
import {
  cssButtonClean,
  StyledButtonComponentType,
} from "../components/Button/StyledButton"
import {
  BaseHTMLAttributes,
  forwardRef,
  HTMLAttributeAnchorTarget,
} from "react"
import { ButtonVariantsPropsType } from "../components/Button/Button"
import { Typography } from "../components/Typography/Typography"
import { ComponentLoader } from "../components/Loaders/ComponentLoader/ComponentLoader"
import NextLink from "next/link"
import { cx } from "@linaria/core"

interface ButtonWrapperPropsType extends ButtonVariantsPropsType {
  ButtonComponent: StyledButtonComponentType &
    Pick<BaseHTMLAttributes<HTMLAttributeAnchorTarget>, "target">
}

const ButtonWithIcon = forwardRef<
  HTMLButtonElement,
  ButtonWrapperPropsType &
    BaseHTMLAttributes<HTMLButtonElement> &
    Pick<BaseHTMLAttributes<HTMLAttributeAnchorTarget>, "target">
>(
  (
    {
      ButtonComponent,
      icon,
      iconPosition,
      fillIcon,
      children,
      size = "medium",
      hideTextOnBreakpoint,
      isFetching,
      as,
      href,
      withArea,
      isHiddenBg,
      withOutOffset,
      isClean,
      className,
      ...props
    },
    ref,
  ) => {
    return (
      <>
        {as === "a" && href !== undefined ? (
          <NextLink href={href} passHref>
            <ButtonComponent
              ref={ref}
              data-icon-position={iconPosition}
              data-size={size}
              data-text-hide-breakpoints={hideTextOnBreakpoint}
              data-is-fetched={isFetching ? isFetching : undefined}
              data-with-area={withArea}
              data-hidden-bg={isHiddenBg}
              data-is-offset={withOutOffset}
              as={as}
              name={"button"}
              {...props}
              className={cx(className, isClean && cssButtonClean)}
            >
              {isFetching && <ComponentLoader />}
              {!!icon && <Icon NameIcon={icon} fill={fillIcon} />}
              {!!icon && !!children ? (
                <Typography variant={"span"}>{children}</Typography>
              ) : (
                children
              )}
            </ButtonComponent>
          </NextLink>
        ) : (
          <ButtonComponent
            ref={ref}
            data-icon-position={iconPosition}
            data-size={size}
            data-text-hide-breakpoints={hideTextOnBreakpoint}
            data-is-fetched={isFetching ? isFetching : undefined}
            data-with-area={withArea}
            data-hidden-bg={isHiddenBg}
            data-is-offset={withOutOffset}
            as={as}
            name={"button"}
            {...props}
            className={cx(className, isClean && cssButtonClean)}
          >
            {isFetching && <ComponentLoader />}
            {!!icon && <Icon NameIcon={icon} fill={fillIcon} />}
            {!!icon && !!children ? (
              <Typography variant={"span"}>{children}</Typography>
            ) : (
              children
            )}
          </ButtonComponent>
        )}
      </>
    )
  },
)
ButtonWithIcon.displayName = "ButtonWithIcon"

export default ButtonWithIcon
