import {
  RequestOrderListOrders,
  RequestOrderListProducts,
  ResponseOrderListProducts,
} from "../types/types"
import { deleteFetch, get, put } from "../service/fetcher"
import {
  ApiError,
  OrderType,
  ResponseOrderListOrders,
} from "../../contracts/contracts"
import { useQuery, UseQueryResult } from "react-query"

export const fetchOrderListOrders = (
  data: RequestOrderListOrders,
): Promise<ResponseOrderListOrders> =>
  get(
    `/account/order-list/orders?payer=${data.payer || ""}&page=${
      data.page
    }&per_page=${data.perPage}&state=${data.state || ""}&year=${
      data.year || ""
    }`,
    true,
  )

export const fetchOrderListProducts = (
  data: RequestOrderListProducts,
): Promise<ResponseOrderListProducts> =>
  get(
    `/account/order-list/products?payer=${data.payer || ""}&page=${
      data.page
    }&per_page=${data.perPage}&state=${data.state || ""}&year=${
      data.year || ""
    }&category=${data.category || ""}`,
    true,
  )

export const fetchOrdersByUid = ({
  uid,
}: {
  uid: string
}): Promise<OrderType> => get(`/account/orders/${uid}`, true)

export const fetchOrderSubscribe = ({
  uid,
  ...data
}: {
  uid: string
  interval: number
}): Promise<null> => put(`/account/orders/${uid}/subscribe`, data, true)

export const fetchOrderUnSubscribe = ({
  uid,
}: {
  uid: string
}): Promise<null> => put(`/account/orders/${uid}/unsubscribe`, null, true)

export const fetchOrderCancel = ({
  uid,
}: {
  uid: string
}): Promise<OrderType> => deleteFetch(`/account/orders/${uid}`, null, true)

export const orderAPI = {
  useFetchOrderListOrders: (
    data: RequestOrderListOrders | null,
  ): UseQueryResult<ResponseOrderListOrders, ApiError> => {
    return useQuery(
      [
        "orderListOrders",
        data?.page,
        data?.perPage,
        data?.payer,
        data?.year,
        data?.state,
      ],
      () =>
        !!data
          ? fetchOrderListOrders({
              ...data,
            })
          : null,
      {
        keepPreviousData: true,
        staleTime: 5000,
      },
    )
  },
  useFetchOrderListProducts: (
    data: RequestOrderListProducts | null,
  ): UseQueryResult<ResponseOrderListProducts, ApiError> => {
    return useQuery(
      [
        "orderListProducts",
        data?.page,
        data?.perPage,
        data?.payer,
        data?.year,
        data?.state,
        data?.category,
      ],
      () =>
        !!data
          ? fetchOrderListProducts({
              ...data,
            })
          : null,
      {
        keepPreviousData: true,
        staleTime: 5000,
      },
    )
  },
}
