import { SettingsFetchedType } from "../../contracts/contracts"
import { get } from "../service/fetcher"
import { useQuery, UseQueryResult } from "react-query"

export const fetchSettingsApp = (): Promise<SettingsFetchedType> =>
  get<SettingsFetchedType>(`/settings`)

export const settingsAppAPI = {
  useSettingsApp(): UseQueryResult<SettingsFetchedType> {
    return useQuery("settings", () => fetchSettingsApp(), {
      retry: 1,
    })
  },
}
