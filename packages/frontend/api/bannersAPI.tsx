import { BannerApiType } from "../../contracts/contracts"
import { FetcherBasePropsType, get, getAbsolutePath } from "../service/fetcher"

export const fetchBanners = (
  data?: FetcherBasePropsType,
): Promise<BannerApiType[]> => get(`${getAbsolutePath(data?.server)}/pictures`)
