import { useQuery, UseQueryResult } from "react-query"
import {
  deleteFetch,
  FetcherBasePropsType,
  get,
  getAbsolutePath,
  post,
} from "../service/fetcher"
import {
  ProductDetailListType,
  ProductDetailResponse,
} from "../../contracts/contracts"

export const fetchProductsList = ({
  uuids,
  server,
}: FetcherBasePropsType & {
  uuids: string | null
}): Promise<ProductDetailListType> =>
  get<ProductDetailListType>(
    `${getAbsolutePath(server)}/product/list?uuid=${uuids}`,
  )

export const fetchProductById = ({
  server,
  uuid,
  fields,
  req,
  res,
}: FetcherBasePropsType & {
  uuid: string
  fields?: (keyof ProductDetailResponse)[]
}): Promise<ProductDetailResponse> =>
  get<ProductDetailResponse>(
    `${getAbsolutePath(server)}/product/${uuid}${
      !!fields ? `?fields=${fields.join(",")}` : ""
    }`,
    false,
    req,
    res,
  )

export const fetchProductWaiting = ({
  uuid,
  ...data
}: {
  uuid: string
  email: string
}): Promise<null> =>
  post<null, { email: string }>(`/product/waiting/${uuid}`, data, true)

export const fetchProductsWaiting = (): Promise<string[]> =>
  get(`/account/waiting-products`, true)

export const fetchProductWaitingRemove = (data: {
  product: string
}): Promise<null> =>
  deleteFetch<
    null,
    {
      product: string
    }
  >(`/account/waiting-products`, data, true)

export const productsAPI = {
  useProductsFetch: (
    uuids: string | null,
  ): UseQueryResult<ProductDetailListType | null> => {
    return useQuery(
      ["products", uuids],
      () => (!!uuids ? fetchProductsList({ uuids }) : null),
      {},
    )
  },
}
