import { deleteFetch, get, post, put } from "../service/fetcher"
import {
  CartResponse,
  ChangeQtyRequest,
  DeleteProductRequest,
  FastOrderRequest,
} from "../../contracts/contracts"
import {
  ChangeQtyProductResponse,
  ChangeQtyResponse,
  ChangeQtySampleResponse,
} from "../types/types"
import { useQuery, UseQueryResult } from "react-query"

export const fetchChangeQtyProduct = (
  changeQtyData: ChangeQtyRequest,
): Promise<ChangeQtyProductResponse> =>
  post<ChangeQtyProductResponse, ChangeQtyRequest>(
    "/cart/change-qty",
    changeQtyData,
    true,
  )

export const fetchCart = (tokenCart: string): Promise<CartResponse> =>
  get<CartResponse>(`/cart/${tokenCart}`, true)

export const fetchRemoveProduct = (
  request: DeleteProductRequest,
): Promise<ChangeQtyResponse> =>
  deleteFetch<ChangeQtyResponse, DeleteProductRequest>(
    `/cart/delete-product`,
    request,
    true,
  )

export const fetchChangeQtySample = (
  productData: ChangeQtyRequest,
): Promise<ChangeQtySampleResponse> =>
  post<ChangeQtySampleResponse, ChangeQtyRequest>(
    "/cart/sample",
    productData,
    true,
  )

export const fetchClearCart = (data: { uid: string }): Promise<boolean> =>
  put<boolean, { uid: string }>(`/cart/remove/${data.uid}`, data, true)

export const fetchRemoveCart = (data: { uid: string }): Promise<boolean> =>
  deleteFetch<boolean, { uid: string }>(`/cart/remove/${data.uid}`, data, true)

export const fetchBindUserCart = (data: { cart: string }): Promise<boolean> =>
  put(`/cart/${data.cart}/bind-user`, null, true)

export const fetchFastOrder = ({
  cart,
  ...data
}: { cart: string } & FastOrderRequest): Promise<boolean> =>
  put(`/fast-order/${cart}`, data, true)

export const fetchFreeShipping = (region: string): Promise<null> =>
  get(`/free-shipping?region=${region}`)

export const cartAPI = {
  useFreeShipping(data: {
    region: string | null
  }): UseQueryResult<null | { order_min_cost: number }> {
    return useQuery(["freeShipping", data], () =>
      !!data.region ? fetchFreeShipping(data.region) : null,
    )
  },
}
