import { NextPage } from "next"
import { StaticTemplate } from "../layouts/Static/StaticTemplate"
import React from "react"
import { Typography } from "../components/Typography/Typography"
import {
  cssListVertical,
  StyledList,
  StyledListItem,
} from "../components/List/StyledList"
import { TITLE_SITE_RU } from "../utils/constants"
import { Meta } from "../components/Meta/Meta"

const ReturnProductPage: NextPage = () => {
  return (
    <>
      <Meta
        title={`Условия возврата - ${TITLE_SITE_RU}`}
        description={`Вы можете вернуть товар ненадлежащего качества, если он имеет брак или просто Вам не подошёл в течение 14 дней. Позвоните нам или напишите на сайте или в удобном Вам мессенджере. Мы обменяем товар или вернём за него деньги. Возможна замена аналогичным товаром или зачёт цены бракованного товара в следующий заказ.`}
      />
      <StaticTemplate title={"Условия возврата"}>
        <Typography>
          Вы можете вернуть товар ненадлежащего качества, если он имеет брак или
          просто Вам не подошёл в течение 14 дней. Позвоните нам или напишите на
          сайте или в удобном Вам мессенджере. Мы обменяем товар или вернём за
          него деньги. Возможна замена аналогичным товаром или зачёт цены
          бракованного товара в следующий заказ.
        </Typography>
        <Typography variant={"h2"}>
          {" "}
          Что нужно указать в письме при обнаружении брака
        </Typography>
        <Typography>
          В случае обнаружения брака в течение 2 недель отправьте нам на
          электронную почту, в мессенджер или позвоните по телефону и сообщите
          следующую информацию:
        </Typography>
        <StyledList className={cssListVertical} isDefault>
          {[
            "номер заказа;",
            "наименование товара;",
            "количество единиц;",
            "характер брака;",
            "приложить фото, где отчётливо виден брак.",
          ].map((item, index) => (
            <StyledListItem key={index}>{item}</StyledListItem>
          ))}
        </StyledList>
        <Typography>
          Обратите внимание, что нельзя вернуть товар надлежащего качества, если
          он изготовлен по Вашему заказу или на него нанесён Ваш логотип и
          другие уникальные знаки и надписи. (Исключением является только брак)
        </Typography>
        <Typography>
          Также мы не принимаем претензии, если порча товара произошла в
          процессе эксплуатации, а также если порча произошла не по нашей вине.
        </Typography>
      </StaticTemplate>
    </>
  )
}

export default ReturnProductPage
