import { Provider as ReakitIdProvider } from "reakit"

import type { AppContext, AppProps } from "next/app"
import { Default } from "../layouts/Default/Default"

import { Provider as AppProvider } from "../hooks/app"
import { Provider as UserProvider } from "../hooks/auth"
import { Provider as CategoriesPopupProvider } from "../hooks/categoriesPopup"
import { Provider as FavoritesProvider } from "../hooks/favorites"
import { Provider as CartProvider } from "../hooks/cart"
import { Provider as HeaderProvider } from "../hooks/header"
import { Provider as ShippingsProvider } from "../hooks/shippings"
import { Provider as SearchProvider } from "../hooks/search"
import { Provider as ShopsProvider } from "../hooks/shops"
import type { NextPage } from "next"
import type { ReactElement, ReactNode } from "react"
import { useState } from "react"
import { Hydrate, QueryClient, QueryClientProvider } from "react-query"
import { ReactQueryDevtools } from "react-query/devtools"
import { Provider } from "react-redux"
import { store } from "../store/store"
import { AuthGuard } from "../hoc/AuthGuard"
import { Meta } from "../components/Meta/Meta"
import Script from "next/script"
import { GoogleReCaptchaProvider } from "react-google-recaptcha-v3"
import { Router } from "next/router"
import NProgress from "nprogress" //nprogress module
import "nprogress/nprogress.css"
import { getExpireOneYear, getIp, getIpCookieEntry } from "../utils/helpers"
import {
  breakpoints,
  colors,
  fontDefault,
  fontSizeDefault,
} from "../styles/utils/vars"

export type RequiredAuth = { requireAuth: boolean }

type NextPageWithLayout = NextPage & {
  getLayout?: (page: ReactElement) => ReactNode
}

export type NextPageWithRequiredAuth = NextPageWithLayout & RequiredAuth

type AppPropsWithLayout = AppProps & {
  Component: NextPageWithRequiredAuth
}

type PagePropsType = {
  dehydratedState: unknown
}

NProgress.configure({
  showSpinner: false,
})

Router.events.on("routeChangeStart", () => {
  NProgress.start()
})
Router.events.on("routeChangeComplete", () => {
  NProgress.done()
})
Router.events.on("routeChangeError", () => {
  NProgress.done()
})

function GrosterApp({ Component, pageProps }: AppPropsWithLayout): JSX.Element {
  const withLayout =
    Component.getLayout || ((page) => <Default>{page}</Default>)

  const [queryClient] = useState(
    () =>
      new QueryClient({
        defaultOptions: {
          queries: {
            refetchInterval: false,
            refetchOnWindowFocus: false,
            refetchIntervalInBackground: false,
            retry: 1,
            retryDelay: 100,
            keepPreviousData: true,
          },
          mutations: {
            retry: 1,
            retryDelay: 100,
          },
        },
      }),
  )

  return (
    <>
      <style jsx global={true}>{`
        #__next {
          height: 100%;
          position: relative;
        }
        html {
          font-family: ${fontDefault};
          color: ${colors.black};
          font-size: ${fontSizeDefault};
        }

        html,
        body {
          font-size: 16px;
          line-height: 187%;
          font-weight: 500;
          padding: 0;
          margin: 0;
          -webkit-text-size-adjust: none;
          text-rendering: optimizeSpeed;
          // обязательно width 100% при открытии модальных окон
          // - body становится fixed, на ios все ломается
          width: 100%;
        }

        html #nprogress .bar {
          background: ${colors.brand.purple};
          height: 8px;
        }
        html #nprogress .peg {
          box-shadow: 0 0 10px ${colors.brand.purple},
            0 0 5px ${colors.brand.purpleDarken};
        }
        html #nprogress .spinner-icon {
          border-top-color: ${colors.brand.purple};
          border-left-color: ${colors.brand.purple};
        }

        @media (max-width: ${breakpoints.md}) {
          html #nprogress .bar {
            background: ${colors.white};
          }

          html #nprogress .peg {
            box-shadow: 0 0 10px ${colors.white}, 0 0 5px ${colors.white};
          }
        }

        body {
          padding-bottom: env(safe-area-inset-bottom);
          -webkit-overflow-scrolling: touch;
          position: relative;
          top: 0;
        }

        *,
        *:before,
        *:after {
          box-sizing: border-box;
          outline: none;
        }

        * {
          outline: none;
          -webkit-tap-highlight-color: rgba(0, 0, 0, 0);
        }

        img {
          max-width: 100%;
        }

        a {
          text-decoration: none;
          color: ${colors.brand.purple};
          -webkit-tap-highlight-color: transparent;
        }

        label {
          -webkit-tap-highlight-color: transparent;
        }

        .grecaptcha-badge {
          z-index: 10;
          display: none !important;
        }

        input:focus,
        textarea:focus,
        select:focus,
        button:focus,
        img:focus {
          outline: none;
        }
      `}</style>
      <Meta />
      <Provider store={store}>
        <QueryClientProvider client={queryClient}>
          <Hydrate state={(pageProps as PagePropsType)?.dehydratedState}>
            <ReakitIdProvider>
              <AppProvider>
                <GoogleReCaptchaProvider
                  reCaptchaKey={
                    process.env.NEXT_PUBLIC_RECAPTCHA_SITE_KEY || ""
                  }
                  scriptProps={{
                    async: true,
                    defer: false,
                    appendTo: "head",
                    nonce: "reCaptcha",
                    id: "reCaptchaG",
                  }}
                >
                  <UserProvider>
                    <CategoriesPopupProvider>
                      <SearchProvider>
                        <FavoritesProvider>
                          <ShippingsProvider>
                            <CartProvider>
                              <ShopsProvider>
                                <HeaderProvider>
                                  {Component.requireAuth ? (
                                    <>
                                      <AuthGuard>
                                        {withLayout(
                                          <Component {...pageProps} />,
                                        )}
                                      </AuthGuard>
                                    </>
                                  ) : (
                                    <>
                                      {withLayout(<Component {...pageProps} />)}
                                    </>
                                  )}
                                </HeaderProvider>
                              </ShopsProvider>
                            </CartProvider>
                          </ShippingsProvider>
                        </FavoritesProvider>
                      </SearchProvider>
                    </CategoriesPopupProvider>
                  </UserProvider>
                </GoogleReCaptchaProvider>
              </AppProvider>
            </ReakitIdProvider>
          </Hydrate>
          <ReactQueryDevtools />
        </QueryClientProvider>
      </Provider>
      <Script
        id={"yandexMetrika"}
        strategy={"afterInteractive"}
        nonce={"yandexMetrikaScript"}
        dangerouslySetInnerHTML={{
          __html: `
   (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)};
   m[i].l=1*new Date();
   for (var j = 0; j < document.scripts.length; j++) {if (document.scripts[j].src === r) { return; }}
   k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)})
   (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");

   ym(56589850, "init", {
        clickmap:true,
       trackLinks:true,
       accurateTrackBounce:true,
       webvisor:true,
       ecommerce:"dataLayer"
   });`,
        }}
      />
      <Script
        src={`https://www.google.com/recaptcha/api.js?render=${process.env.NEXT_PUBLIC_RECAPTCHA_SITE_KEY}`}
        strategy="lazyOnload"
        async={true}
        id={"recaptcha"}
        nonce={"recaptchaScript"}
      />
      <Script
        src={`//cdn.diginetica.net/2243/client.js?ts=${Date.now()}`}
        async={true}
        id={"diginetica"}
        nonce={"digineticaScript"}
        strategy={"lazyOnload"}
      />
      <Script
        id={"leadhitCounter"}
        async={true}
        strategy={"lazyOnload"}
        nonce={"leadhitScript"}
        dangerouslySetInnerHTML={{
          __html: `
        var _lh_params = {
        "popup": false
      };
      lh_clid = "${process.env.NEXT_PUBLIC_LEADHIT_ID}";
      (function() {
        var lh = document.createElement('script');
        lh.type = 'text/javascript';
        lh.async = true;
        lh.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'track.leadhit.io/track.js?ver=' + Math.floor(Date.now() / 100000).toString();
        var s = document.getElementsByTagName('script')[0];
        s.parentNode.insertBefore(lh, s);
      })(); 
      `,
        }}
      />
      <Script
        src={"//code.jivo.ru/widget/TpCrW7r4KD"}
        async={true}
        id={"jivo"}
        nonce={"jivoScript"}
        strategy={"afterInteractive"}
      />
    </>
  )
}

GrosterApp.getInitialProps = async (appContext: AppContext) => {
  const {
    ctx: { req, res, query, pathname },
    Component,
  } = appContext

  let pageProps = {}

  if (Component.getInitialProps) {
    pageProps = await Component.getInitialProps({
      ...appContext,
      pathname: pathname,
      query: query,
    })
  }

  let makedCookies = res?.getHeader("Set-Cookie") || ""

  const ip = getIp(req)
  if (ip !== undefined) {
    const ipCookie = await getIpCookieEntry(req, ip)
    if (ipCookie !== undefined) {
      makedCookies += ipCookie
    }
  }

  // Content-Disposition : form-data; name="field_value"
  makedCookies += `host=${encodeURIComponent(
    req?.headers.host || "",
  )}; Path=/; Expires=${getExpireOneYear().toUTCString()}`

  res?.setHeader("Set-Cookie", makedCookies)

  return {
    pageProps,
  }
}
//
// export function reportWebVitals(metric: NextWebVitalsMetric): void {
//   console.log(metric)
// }

export default GrosterApp
