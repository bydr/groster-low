import { GetServerSidePropsContext } from "next"
import { Detail } from "../../components/Products/Detail/Detail"
import { useEffect, useState } from "react"
import {
  Side,
  StyledProductPage,
} from "../../components/Products/StyledProductPage"
import { Col, Container, Row } from "../../styles/utils/StyledGrid"
import { StyledDetailProduct } from "../../components/Products/Detail/StyledDetail"
import type { BreadcrumbsPropsType } from "../../components/Breadcrumbs/Breadcrumbs"
import { fetchProductById, fetchProductsList } from "../../api/productsAPI"
import { useCategoriesBreadcrumbs } from "../../hooks/categoriesBreadcrumbs"
import { DetailProductType, VariationShortType } from "../../types/types"
import { ROUTES, SITE_URL, TITLE_SITE_RU } from "../../utils/constants"
import { Typography } from "../../components/Typography/Typography"
import { fetchFiltersParams } from "../../api/catalogAPI"
import { Meta } from "../../components/Meta/Meta"
import { useRouter } from "next/router"
import { getBreakpointVal } from "../../styles/utils/Utils"
import { breakpoints } from "../../styles/utils/vars"
import { Button } from "../../components/Button"
import { DetailProductPageRecommends } from "../../components/LeadHit"
import dynamic, { DynamicOptions } from "next/dynamic"
import { ProductsSliderPropsType } from "../../components/Products/Slider/ProductsSlider"
import { getClientUser, useAuth } from "../../hooks/auth"
import { useQuery } from "react-query"
import { Param, ProductDetailListType } from "../../../contracts/contracts"
import { RuleSortProduct, sortProductsWeightRule } from "../../utils/helpers"

const DynamicBreadcrumbs = dynamic(
  (() =>
    import("../../components/Breadcrumbs/Breadcrumbs").then(
      (mod) => mod.Breadcrumbs,
    )) as DynamicOptions<BreadcrumbsPropsType>,
  {},
)

const ProductsSlider = dynamic((() =>
  import("../../components/Products/Slider/ProductsSlider").then(
    (mod) => mod.ProductsSlider,
  )) as DynamicOptions<ProductsSliderPropsType>)

type SSRPropsReturnType = {
  product: DetailProductType | null
  alias?: string
}

export const getServerSideProps = async ({
  params,
  res,
  req,
}: GetServerSidePropsContext): Promise<{
  props: SSRPropsReturnType
}> => {
  let productDetail = null as DetailProductType | null

  const initialProduct = await fetchProductById({
    server: true,
    uuid: (params?.product as string) || "",
    req: req,
    res: res,
  }).catch((err) => {
    console.log("err product ", err)
  })

  if (!!initialProduct) {
    productDetail = { ...initialProduct }

    // если у товара есть родитель, (пришли на вариацию)
    // то получаем данные родителя
    // для получения всего списка вариаций и др. данных, которых нет у вариации
    // иначе делаем редирект на первую вариацию
    if (!!initialProduct.parent) {
      // достаем данные родителя
      const variationParentProduct = await fetchProductById({
        server: true,
        uuid: initialProduct.parent,
        req: req,
        res: res,
      })

      productDetail.variation = {
        selected: {},
        parent: {},
      }

      const getVariationName = (
        variationProdName: string,
        parentProdName: string,
      ) => variationProdName.replace(`${parentProdName},`, "").trim()

      if (!!variationParentProduct) {
        const getVariationModel = (
          products: ProductDetailListType,
        ): VariationShortType[] =>
          products
            .map((prod) => ({
              alias: prod.alias || "",
              mainImage:
                !!prod.images && prod.images.length > 0 ? prod.images[0] : null,
              name:
                !!variationParentProduct.name && !!prod.name
                  ? getVariationName(prod.name, variationParentProduct.name)
                  : "",
              fullName: (prod.name || "").trim(),
              uuid: prod.uuid || "",
              totalQty: prod.total_qty || 0,
            }))
            .sort((a, b) => {
              const nameA = a.name
              const nameB = b.name
              return nameA > nameB ? 1 : nameA < nameB ? -1 : 0
            })

        // достаем все данные вариаций у родителя
        // нужна картинка, алиас каждой
        if (!!variationParentProduct.variations) {
          const variationsProducts = await fetchProductsList({
            uuids: variationParentProduct.variations.join(","),
            server: true,
            req: req,
          })

          if (!!variationsProducts) {
            productDetail.variation.model =
              getVariationModel(variationsProducts)
          }
        }

        // productDetail.variations = variationParentProduct.variations
        productDetail.categories = variationParentProduct.categories

        if (productDetail.variation.parent === undefined) {
          productDetail.variation.parent = {}
        }
        productDetail.variation.parent.description =
          variationParentProduct.description

        if ((productDetail.images || []).length <= 0) {
          productDetail.images = variationParentProduct.images
        }
      }

      // получаем название по которому вариируется товар
      if (
        !!productDetail &&
        !!productDetail?.params &&
        productDetail?.params.length > 0
      ) {
        const paramId = productDetail.params[0]
        let selectedParam: Param | undefined

        const paramsData = await fetchFiltersParams({ server: true })
        if (!!paramsData) {
          selectedParam = paramsData.find((p) =>
            !!p.values ? p.values.some((v) => v.uuid === paramId) : false,
          )
        }

        if (productDetail.variation.selected === undefined) {
          productDetail.variation.selected = {}
        }
        productDetail.variation.selected = {
          uuid: paramId,
          paramName: selectedParam?.name,
          name: getVariationName(
            initialProduct?.name || "",
            variationParentProduct?.name || "",
          ),
        }
      }
    } else {
      if (initialProduct.variations && initialProduct.variations.length > 0) {
        const variationsProducts = await fetchProductsList({
          uuids: initialProduct.variations.join(","),
          server: true,
        })

        let availableId = initialProduct.variations[0]

        if (!!variationsProducts) {
          const availableVariation = variationsProducts.find(
            (p) => (p.total_qty || 0) > 0,
          )
          if (
            availableVariation !== undefined &&
            availableVariation.uuid !== undefined
          ) {
            availableId = availableVariation.uuid
          }
        }

        const variationsProduct = await fetchProductById({
          uuid: availableId,
          server: true,
          req: req,
        })

        if (!!variationsProduct && !!variationsProduct.alias) {
          res.setHeader(
            "location",
            `${ROUTES.product}/${variationsProduct.alias}`,
          )
          res.statusCode = 302
        }
      }
    }
  }

  return {
    props: {
      product: productDetail,
      alias: params?.product as string | undefined,
    },
  }
}

const user = getClientUser()

export default function ProductPage({
  product: initialProduct,
  alias,
}: SSRPropsReturnType): JSX.Element {
  const router = useRouter()
  const { breadcrumbs } = useCategoriesBreadcrumbs()
  const { isAuth } = useAuth()

  const [isEmptyProductExtends, setIsEmptyProductExtends] =
    useState<boolean>(false)
  const [isFetchingProductExtends, setIsFetchingProductExtends] =
    useState<boolean>(false)

  const [product, setProduct] = useState<DetailProductType | null>(
    initialProduct,
  )

  useQuery(
    ["product", user?.accessToken, initialProduct?.uuid, isAuth],
    () =>
      fetchProductById({
        uuid: initialProduct?.uuid || alias || "",
      }),
    {
      enabled:
        (!user?.accessToken && isAuth && !!initialProduct?.uuid) ||
        (alias !== undefined && initialProduct?.uuid === undefined),
      initialData: initialProduct,
      onSuccess: (response) => {
        if (response?.price !== initialProduct?.price) {
          setProduct({
            ...initialProduct,
            price:
              response?.price !== undefined
                ? response?.price
                : initialProduct?.price,
          })
        }
      },
    },
  )

  const [analogsAll, setAnalogsAll] = useState<RuleSortProduct[]>([])

  const { data: analogsData, isFetching: isFetchingAnalogs } = useQuery(
    ["productAnalogs", analogsAll],
    () =>
      fetchProductsList({
        uuids: analogsAll.map((item) => item.uuid).join(","),
      }),
    {
      enabled: analogsAll.length > 0,
    },
  )

  const { data: companionsData, isFetching: isFetchingCompanions } = useQuery(
    ["companions", product?.companions],
    () =>
      fetchProductsList({
        uuids: (
          product?.companions
            ?.filter((i) => !!i.uuid)
            .map((item) => item.uuid) || []
        ).join(","),
      }),
    {
      enabled: (product?.companions || []).length > 0,
    },
  )

  const [companions, setCompanions] = useState<ProductDetailListType>([])
  const [analogs, setAnalogs] = useState<ProductDetailListType>([])

  useEffect(() => {
    setProduct(initialProduct)

    return () => {
      setProduct(null)
    }
  }, [initialProduct, initialProduct?.uuid])

  useEffect(() => {
    if ((analogsData || []).length > 0 || (companionsData || []).length > 0) {
      setIsEmptyProductExtends(false)
    } else {
      setIsEmptyProductExtends(true)
    }
  }, [analogsData, companionsData])

  useEffect(() => {
    if (isFetchingAnalogs || isFetchingCompanions) {
      setIsFetchingProductExtends(true)
    } else {
      setIsFetchingProductExtends(false)
    }

    return () => {
      setIsFetchingProductExtends(false)
    }
  }, [isFetchingAnalogs, isFetchingCompanions])

  useEffect(() => {
    setAnalogsAll(
      [
        ...(product?.analogs?.fast || []),
        ...(product?.analogs?.other || []),
      ].filter((uuid) => uuid !== undefined),
    )

    return () => {
      setAnalogsAll([])
    }
  }, [product?.analogs])

  useEffect(() => {
    setCompanions(
      sortProductsWeightRule({
        products: companionsData || [],
        rules: product?.companions,
      }),
    )
    return () => {
      setCompanions([])
    }
  }, [companionsData, product?.companions])

  useEffect(() => {
    setAnalogs(
      sortProductsWeightRule({
        products: analogsData || [],
        rules: analogsAll,
      }),
    )
  }, [analogsData, analogsAll])

  return (
    <>
      <Meta
        title={product?.name || undefined}
        description={
          product?.meta_description ||
          product?.description ||
          product?.name ||
          undefined
        }
        ogTags={{
          title:
            product?.meta_title || !!product?.name
              ? `${product?.name} - купить в интернет-магазине ${TITLE_SITE_RU}`
              : undefined,
          url: `${SITE_URL}${router.asPath}`,
          image:
            !!product?.images && product.images.length > 0
              ? `https://${product.images[0]}`
              : undefined,
        }}
      />
      <StyledProductPage>
        <Container>
          <Row>
            <DynamicBreadcrumbs breadcrumbs={breadcrumbs} />

            <StyledDetailProduct>
              <Side>
                {(!isEmptyProductExtends || isFetchingProductExtends) &&
                  companions.length > 0 && (
                    <ProductsSlider
                      products={companions}
                      ruleSort={product?.companions}
                      responsiveExtends={{
                        [getBreakpointVal(breakpoints.md)]: {
                          slidesPerView: 2,
                        },
                        0: {
                          slidesPerView: 1,
                        },
                      }}
                      variantArrow={"top-bottom"}
                      title={"Сопутствующие товары"}
                    />
                  )}
                <DetailProductPageRecommends />
              </Side>

              {!!product ? (
                <>
                  <Detail
                    product={product}
                    companions={companions}
                    analogs={analogs}
                    rulesAnalogs={analogsAll}
                  />
                </>
              ) : (
                <>
                  <Row>
                    <Col>
                      <Typography variant={"h1"}>Произошла ошибка</Typography>
                      <Button
                        href={ROUTES.catalog}
                        as={"a"}
                        variant={"filled"}
                        size={"large"}
                      >
                        В каталог
                      </Button>
                    </Col>
                  </Row>
                </>
              )}
            </StyledDetailProduct>
          </Row>
        </Container>
      </StyledProductPage>
    </>
  )
}
