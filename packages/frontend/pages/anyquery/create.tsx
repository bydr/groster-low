import {
  CategoriesTreeStateType,
  ICategoryTreeItem,
  NextPageWithLayout,
  ProductOfferType,
  ProductType,
  PropertiesType,
} from "../../types/types"
import { GetServerSidePropsContext } from "next"
import { fetchCategories, fetchFiltersParams } from "../../api/catalogAPI"
import { ROUTES, SITE_URL } from "../../utils/constants"
import { Category, ProductCatalogType } from "../../../contracts/contracts"
import {
  createCategoriesTree,
  getFilterParamsTree,
  getProductsSite,
  getTranslationProperties,
  toSpecialChars,
} from "../../utils/helpers"
import {
  IFilterParamsChilds,
  IFilterParamsParents,
} from "../../store/reducers/catalogSlice"
import fs from "fs"

export const DIR = `${process.cwd()}/public/docs`
export const PATH = `${DIR}/anyquery.xml`

const AnyqueryPage: NextPageWithLayout = () => {
  return <></>
}

const getParamsProduct = ({
  productProperties,
  productParams,
  filterParams,
  isBestseller,
  isKit,
  isChildKit,
  keywords,
}: {
  productProperties: PropertiesType
  productParams: string[]
  filterParams: IFilterParamsParents & IFilterParamsChilds
  isBestseller: boolean
  isKit: boolean
  isChildKit: boolean
  keywords: string | null
}): { name: string; value: string }[] => {
  let allProps: PropertiesType = [...productProperties]

  if (keywords !== null) {
    const kws = keywords.split(";")
    allProps = [
      ...allProps,
      ...kws.map((item) => {
        return {
          name: "Синоним",
          value: item.trim(),
        }
      }),
    ]
  }

  if (productParams.length > 0) {
    for (const pp of productParams) {
      const paramVal = filterParams[pp]
      if (!paramVal) {
        continue
      }
      const paramParent = filterParams[paramVal.parentUuid || ""]
      if (!paramParent) {
        continue
      }

      allProps.push({
        value: toSpecialChars(paramVal.name || ""),
        name: toSpecialChars(paramParent.name || ""),
      })
    }
  }

  allProps.push({
    value: isBestseller.toString(),
    name: "Хит",
  })

  allProps.push({
    value: isKit.toString(),
    name: "Комплект",
  })

  allProps.push({
    value: isChildKit.toString(),
    name: "Часть комплекта",
  })

  return getTranslationProperties(allProps)
}

const getMainImage = (images?: string[]): string => {
  return !!images && images.length > 0
    ? images[0].includes("http")
      ? images[0]
      : `https://${images[0]}`
    : ""
}

const reformatProductToOffer = (
  p: ProductType & Pick<ProductCatalogType, "business_areas">,
): ProductOfferType => {
  return {
    code: p.code || "",
    alias: p.alias || "",
    params: p.params || [],
    categories: p.categories || [],
    description: p.description || "",
    price: p.price || 0,
    image: getMainImage(p.images),
    is_bestseller: !!p.is_bestseller,
    is_kit_child: (p.kit_parents || []).length > 0,
    is_kit: (p.kit || []).length > 0,
    name: p.name || "",
    uuid: p.uuid || "",
    properties: p.properties || [],
    total_qty: p.total_qty || 0,
    keywords:
      p.keywords === undefined || p.keywords?.length === 0 ? null : p.keywords,
    business_areas: p.business_areas || [],
  }
}

const createCategories = async ({
  host,
}: {
  host: string
}): Promise<{
  categories: Category[]
  layout: string
}> => {
  const dataCategories = await fetchCategories({
    server: true,
  })

  const cIdsParents: number[] = []

  const categoriesTree: CategoriesTreeStateType | null = createCategoriesTree(
    dataCategories?.categories || [],
  )

  const categoriesFormat = (dataCategories?.categories || [])
    .sort((a, b) => {
      return (a?.parent || 0) > (b?.parent || 0)
        ? 1
        : (a?.parent || 0) < (b?.parent || 0)
        ? -1
        : 0
    })
    .filter((c) => {
      if (!c.product_qty) {
        return false
      }
      // сохраняем все категории 1го уровня
      // они всегда будут в начале после сортировки
      if (c.parent === 0 && c.id !== undefined) {
        cIdsParents.push(c.id)
        return true
      }
      // не выводим категории
      // без товаров
      // если у внутренних категорий нет родителя или у родителя нет товаров
      if (c.parent !== undefined) {
        return cIdsParents.includes(c.parent)
      }
    })

  const _recurs = (): string[] => {
    const layouts: string[] = []

    const _category = ({
      category,
      parent,
    }: {
      category: ICategoryTreeItem
      parent?: string
    }) => {
      if (category.uuid === undefined || !category.product_qty) {
        return
      }
      layouts.push(
        `<category id="${category.uuid}" ` +
          (parent !== undefined ? `parentId="${parent}"` : "") +
          ` url="${host}${ROUTES.catalog}/${category.alias}">${category.name}</category>`,
      )

      for (const k of Object.keys(category.children || {})) {
        if (!category.children) {
          continue
        }
        _category({
          category: category.children[k],
          parent: category.uuid,
        })
      }
    }

    for (const key of Object.keys(categoriesTree.tree || {})) {
      if (!categoriesTree.tree) {
        continue
      }

      _category({
        category: categoriesTree.tree[key],
      })
    }

    return layouts
  }

  const layout = _recurs()

  return {
    categories: categoriesFormat,
    layout: `<categories>${layout.join("")}</categories>`,
  }
}

const createOffers = async ({
  categories,
  host,
}: {
  categories: Category[]
  host: string
}) => {
  const groupFilterParams = await fetchFiltersParams({
    server: true,
  })
  const ungroupFilterParams = getFilterParamsTree({
    type: "",
    payload: groupFilterParams || [],
  })
  const products = await getProductsSite<ProductOfferType>(
    categories.map((c) => c.uuid || ""),
    reformatProductToOffer,
  )

  return `<offers>
${products
  .map((p) => {
    const params = getParamsProduct({
      filterParams: ungroupFilterParams,
      productParams: p.params,
      productProperties: p.properties,
      isBestseller: p.is_bestseller,
      isKit: p.is_kit,
      isChildKit: p.is_kit_child,
      keywords: p.keywords,
    })
      .map((prop) => {
        return `<param name="${prop.name}">${prop.value}</param>`
      })
      .join("")
    const url = `${host}${ROUTES.product}/${p.alias}`
    const price = `${p.price}`
    const isAvailable = !!p.total_qty

    const stocks = p.business_areas
      .map((uuid) => {
        return `<stock id="${uuid}">
                <available>${isAvailable}</available>
                <price>${p.price}</price>
                <url>${url}</url>
              </stock>`
      })
      .join("")

    return `<offer id="${p.uuid}" available="${isAvailable}" bid="${p.uuid}">
      <vendor></vendor>
      <vendorCode>${toSpecialChars(p.code || "")}</vendorCode>
      <name>${toSpecialChars(p.name || "")}</name>
      <description>${toSpecialChars(p.description || "")}</description>
      <url>${url}</url>
      <currencyId>RUB</currencyId>
      <categoryId>${p.categories[0] || ""}</categoryId>
      <picture>${toSpecialChars(p.image)}</picture>
      <price>${price}</price>
      ${params}
      ${stocks}
</offer>`
  })
  .join("")}
</offers>`
}

const createShop = async ({ host }: { host: string }) => {
  try {
    const categories = await createCategories({
      host,
    })
    const offers = await createOffers({
      host,
      categories: categories.categories,
    })

    return `<shop>
        <name>Гростер</name>
        <company>Groster</company>
        <url>${host}</url>
        <currencies>
            <currency id="RUB" rate="1"/>
        </currencies>
        ${categories.layout}
        ${offers}
    </shop>`
  } catch (err) {
    console.log("[createShop] ", `${err}`)
  }
}

export const getServerSideProps = async ({
  res,
  req,
}: GetServerSidePropsContext): Promise<{ props: unknown }> => {
  const reqHost = req?.headers.host
  const host = !!reqHost
    ? !reqHost.includes("http")
      ? `https://${reqHost}`
      : reqHost
    : SITE_URL

  const shop = await createShop({
    host,
  })

  const query = `<?xml version="1.0" encoding="UTF-8"?>
<yml_catalog date="${new Date().toISOString()}">
    ${shop}
</yml_catalog>
  `
  res.setHeader("Content-Type", "text/plain")
  if (!fs.existsSync(DIR)) {
    fs.mkdirSync(DIR, {
      recursive: true,
      mode: 0o755,
    })
  }
  try {
    fs.writeFileSync(PATH, query)
  } catch (err) {
    res.write(`${err}`)
    res.end()
  }
  res.write("File is created success")
  res.end()

  return {
    props: {},
  }
}

AnyqueryPage.getLayout = (page) => {
  return <>{page}</>
}
export default AnyqueryPage
