import { NextPage } from "next"
import { PageHeading } from "../../styles/utils/Utils"
import { Typography } from "../../components/Typography/Typography"
import { Container, RowGrid } from "../../styles/utils/StyledGrid"
import { Meta } from "../../components/Meta/Meta"
import { Favorites } from "../../components/Favorites"
import { TITLE_SITE_RU } from "../../utils/constants"

const FavoritesPage: NextPage = () => {
  return (
    <>
      <Meta title={`Избранное - ${TITLE_SITE_RU}`} />
      <>
        <Container>
          <RowGrid>
            <PageHeading>
              <Typography variant={"h1"}>Избранное</Typography>
            </PageHeading>
          </RowGrid>
          <RowGrid>
            <Favorites />
          </RowGrid>
        </Container>
      </>
    </>
  )
}

export default FavoritesPage
