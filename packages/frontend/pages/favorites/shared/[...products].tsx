import { useCallback, useMemo, useState } from "react"
import { GetServerSidePropsContext } from "next"
import { Container } from "../../../styles/utils/StyledGrid"
import { Typography } from "../../../components/Typography/Typography"
import { fetchProductsList } from "../../../api/productsAPI"
import { ProductType, VIEW_PRODUCTS_LIST } from "../../../types/types"
import { Button } from "../../../components/Button"
import { useFavorites } from "../../../hooks/favorites"
import { ButtonGroup } from "../../../components/Button/StyledButton"
import { useCart } from "../../../hooks/cart"
import { Samples } from "../../../components/Products/Cart/Samples/Samples"
import { Products } from "../../../components/Products/Products"
import { Product } from "../../../components/Products/Catalog/Product/Product"
import { ProductSpecificationType } from "../../../../contracts/contracts"
import {
  cssPanelTranslucent,
  Panel,
  PanelHeading,
} from "../../../styles/utils/StyledPanel"
import { useRouter } from "next/router"
import { ROUTES } from "../../../utils/constants"
import { useQuery } from "react-query"

type ServerSidePropsType = {
  sharedProductsInfo: Record<string, ProductSpecificationType> | null
}

export const getServerSideProps = ({
  params,
}: GetServerSidePropsContext): {
  props: ServerSidePropsType
} => {
  const products: Record<string, ProductSpecificationType> = {}
  if (params?.products) {
    const parsedProducts = String(params?.products).split(",")
    if (parsedProducts.length > 0) {
      parsedProducts.map((item) => {
        const itemParsed = item.split(":")
        const uuid = !!itemParsed[0] ? itemParsed[0] : ""
        if (uuid.length > 0) {
          const quantity = !!itemParsed[1] ? +itemParsed[1] : 0
          const sample = !!itemParsed[2] ? +itemParsed[2] : 0
          if (quantity > 0 || sample > 0) {
            products[uuid] = {
              uuid: uuid,
              quantity: quantity,
              sample: sample,
            }
          }
        }
      })
    }
  }

  return {
    props: {
      sharedProductsInfo: !!Object.keys(products).length ? products : null,
    },
  }
}
export default function SharedCartPage({
  sharedProductsInfo,
}: ServerSidePropsType): JSX.Element {
  const router = useRouter()

  const sharedStr = useMemo<string | null>(
    () => (!!sharedProductsInfo ? JSON.stringify(sharedProductsInfo) : null),
    [sharedProductsInfo],
  )
  const shared = useMemo<Record<string, ProductSpecificationType> | null>(
    () => (sharedStr === null ? null : JSON.parse(sharedStr)),
    [sharedStr],
  )

  const [products, setProducts] = useState<Record<string, ProductType> | null>(
    null,
  )
  const [samples, setSamples] = useState<Record<string, ProductType> | null>(
    null,
  )

  useQuery(
    ["sharedProducts", shared],
    () =>
      fetchProductsList({
        uuids: Object.keys(shared || {}).join(","),
      }),
    {
      enabled: shared !== null,
      onSuccess: (data) => {
        if (!!data && shared !== null) {
          const product: Record<string, ProductType> = {}
          const sample: Record<string, ProductType> = {}
          data.map((p) => {
            if ((shared[p.uuid || ""]?.quantity || 0) > 0) {
              product[p.uuid || ""] = { ...p }
            }
            if ((shared[p.uuid || ""]?.sample || 0) > 0) {
              sample[p.uuid || ""] = { ...p }
            }
          })

          setSamples(!!Object.keys(sample).length ? sample : null)
          setProducts(!!Object.keys(product).length ? product : null)
        } else {
          setSamples(null)
          setProducts(null)
        }
      },
    },
  )

  const { add: addToFav } = useFavorites()
  const {
    addToCartSomeProducts,
    token,
    isSomeProductsAddFetching,
    isSomeProductsAddSuccess,
  } = useCart()

  const addAllToFavoritesHandleClick = useCallback(() => {
    if (shared !== null) {
      addToFav(Object.keys(shared))
    }
  }, [addToFav, shared])

  const addToCartAllHandleClick = useCallback(() => {
    if (shared !== null) {
      void addToCartSomeProducts(shared, token)
    }
  }, [addToCartSomeProducts, shared, token])

  const samplesFormatted = useMemo(
    () =>
      Object.entries(samples || {})
        .map(([uuid, sample]) => ({
          ...sample,
          initQty: (shared || {})[uuid].sample,
        }))
        .reduce((result, item) => {
          result[item.uuid || ""] = item
          return result
        }, {}),
    [samples, shared],
  )

  return (
    <>
      <Container>
        <Typography variant={"h1"}>Делиться — значит заботиться</Typography>
        {products !== null && Object.keys(products).length > 0 && (
          <>
            <Products view={VIEW_PRODUCTS_LIST}>
              {Object.keys(products).map((key) => {
                return (
                  <Product
                    key={products[key].uuid}
                    view={VIEW_PRODUCTS_LIST}
                    product={products[key]}
                    initQty={(shared || {})[key].quantity || undefined}
                    isShared
                  />
                )
              })}
            </Products>
          </>
        )}

        {Object.keys(samplesFormatted).length > 0 && (
          <>
            <Samples samples={samplesFormatted} isShared />
          </>
        )}

        {isSomeProductsAddSuccess && (
          <>
            <Panel className={cssPanelTranslucent}>
              <PanelHeading>Товары успешно добавлены в корзину</PanelHeading>
              <Typography variant={"p14"}>
                Теперь Вы можете перейти к товарам или продолжить покупки
              </Typography>
              <ButtonGroup>
                <Button
                  variant={"filled"}
                  onClick={() => {
                    void router.push(ROUTES.cart)
                  }}
                >
                  В корзину
                </Button>
                <Button
                  variant={"outline"}
                  onClick={() => {
                    void router.push(ROUTES.catalog)
                  }}
                >
                  В каталог
                </Button>
              </ButtonGroup>
            </Panel>
          </>
        )}

        {(!!products || !!samples) && (
          <>
            <ButtonGroup>
              <Button
                variant={"filled"}
                icon={"ShoppingCart"}
                size={"large"}
                onClick={addToCartAllHandleClick}
                isFetching={isSomeProductsAddFetching}
              >
                Добавить все в корзину
              </Button>
              <Button
                variant={"translucent"}
                icon={"StarOutline"}
                size={"large"}
                onClick={addAllToFavoritesHandleClick}
              >
                Добавить все в избранное
              </Button>
            </ButtonGroup>
          </>
        )}
      </Container>
    </>
  )
}
