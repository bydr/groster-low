import { GetServerSidePropsContext } from "next"
import { fetchSearch } from "../api/digineticaAPI"
import { ProductDetailListType } from "../../contracts/contracts"
import { Search, SearchPropsType } from "../components/Search"
import { SearchTemplate } from "../layouts/Search/SearchTemplate"
import { fetchProductsList } from "../api/productsAPI"
import {
  QueryPageSearchType,
  SEARCH_PER_PAGE,
  useSearch,
} from "../hooks/search"
import { NextPageWithLayout } from "../types/types"
import { SearchLayout } from "../layouts/Search/SearchLayout"
import { useEffect } from "react"

export const getServerSideProps = async ({
  query: q,
}: GetServerSidePropsContext): Promise<{
  props: SearchPropsType
}> => {
  const query = q as QueryPageSearchType | undefined

  const initQuery = query?.query
  const filterQuery = query?.filter
  const searchResult = await fetchSearch({
    query: initQuery,
    size: SEARCH_PER_PAGE,
    filter:
      typeof filterQuery === "undefined" || typeof filterQuery === "string"
        ? filterQuery
        : filterQuery.join("&filter="),
    page: query?.page !== undefined ? +query.page : undefined,
    sort: query?.sort,
  })

  let products: ProductDetailListType = []
  if ((searchResult?.products || []).length > 0) {
    const ids: string[] = searchResult.products
      .map((p) => p.id)
      .filter((id) => id !== undefined) as string[]
    products = await fetchProductsList({
      server: true,
      uuids: ids.join(","),
    })
    //сохраняем нужны порядок,
    // тк в дигинетике приходит список товаров отсортированный по score
    const productsFormat = {}
    if (!!products) {
      ids.map((id) => {
        const found = products.find((p) => p.uuid === id)
        if (!!found) {
          productsFormat[id] = found
        }
      })
    }
    products = Object.values(productsFormat)
  }

  return {
    props: {
      initQuery: initQuery || null,
      searchResult: !!searchResult
        ? {
            ...searchResult,
            productsData: products,
            page: query?.page || null,
            sortQuery: query?.sort || null,
          }
        : null,
      filterQuery:
        filterQuery !== undefined
          ? typeof filterQuery === "string"
            ? [filterQuery]
            : filterQuery
          : null,
    },
  }
}

const SearchPage: NextPageWithLayout<SearchPropsType> = ({
  initQuery,
  searchResult,
  filterQuery,
}) => {
  const { clearSearchResult } = useSearch()

  useEffect(() => {
    return () => {
      clearSearchResult()
    }
  }, [])

  return (
    <>
      <SearchTemplate>
        <Search
          searchResult={searchResult}
          initQuery={initQuery}
          filterQuery={filterQuery}
        />
      </SearchTemplate>
    </>
  )
}

export default SearchPage

SearchPage.getLayout = (page) => {
  return <SearchLayout>{page}</SearchLayout>
}
