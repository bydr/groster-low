import { useEffect, useMemo } from "react"
import { GetServerSidePropsContext } from "next"
import { Container, RowGrid } from "../../styles/utils/StyledGrid"
import {
  cssHiddenMD,
  getBreakpointVal,
  PageHeading,
} from "../../styles/utils/Utils"
import { Typography } from "../../components/Typography/Typography"
import { useAppDispatch, useAppSelector } from "../../hooks/redux"
import {
  catalogSlice,
  INITIAL_PAGE,
  INITIAL_PER_PAGE,
  INITIAL_PER_PAGE_WITH_BANNER,
} from "../../store/reducers/catalogSlice"
import { useCategoriesBreadcrumbs } from "../../hooks/categoriesBreadcrumbs"
import { Meta } from "../../components/Meta/Meta"
import dynamic, { DynamicOptions } from "next/dynamic"
import type { BreadcrumbsPropsType } from "../../components/Breadcrumbs/Breadcrumbs"
import { BusinessAreasBannersPropsType } from "../../components/BusinessAreasBanners"
import { Advice } from "../../components/Advice"
import { SingleUnderCatalog } from "../../components/Banners/SingleUnderCatalog"
import { Catalog } from "components/Catalog/Catalog"
import { fetchCatalog } from "../../api/catalogAPI"
import { CatalogResponseType, QueryCatalogType } from "../../types/types"
import { Provider as FilterProvider } from "../../hooks/filterCatalog"
import { breakpoints } from "../../styles/utils/vars"
import { useWindowSize } from "../../hooks/windowSize"
import { fetchBanners } from "../../api/bannersAPI"
import { BannerApiType } from "../../../contracts/contracts"
import { getBannerStringType } from "../../utils/helpers"
import { useApp } from "../../hooks/app"
import { getClientUser, useAuth } from "../../hooks/auth"
import { useQuery } from "react-query"
import { useRouter } from "next/router"
import { SITE_URL } from "../../utils/constants"

const DynamicBreadcrumbs = dynamic((() =>
  import("../../components/Breadcrumbs/Breadcrumbs").then(
    (mod) => mod.Breadcrumbs,
  )) as DynamicOptions<BreadcrumbsPropsType>)

const DynamicBusinessAreasBanners = dynamic((() =>
  import("../../components/BusinessAreasBanners").then(
    (mod) => mod.BusinessAreasBanners,
  )) as DynamicOptions<BusinessAreasBannersPropsType>)

type ServerSidePropsType = {
  alias?: string
  initialCatalog: CatalogResponseType | null
  filteredCatalog: CatalogResponseType | null
  bannersSSR: BannerApiType[]
  isBannersProductsCatalog: boolean
  query?: QueryCatalogType
}

let initialCatalogCache = null as CatalogResponseType | null | void
let categoryAliasCache = undefined as string | undefined
let bannersCache = null as BannerApiType[] | null

const getConfigCatalog = (
  query: QueryCatalogType | undefined,
  perPage: number,
) => {
  return {
    categories: !!query?.category ? [query.category || ""] : undefined,
    filters: !!query?.params?.length ? query.params?.split(",") : undefined,
    priceRange: query?.price,
    isEnabled: query?.is_enabled,
    isFast: query?.is_fast,
    sortBy: query?.sortby,
    page: !!query?.page ? +query.page : INITIAL_PAGE,
    perPage: !!query?.per_page ? +query.per_page : perPage,
    store: !!query?.store?.length ? query.store?.split(",") : undefined,
  }
}

const calculatePerPage = (isBannersProductsCatalog: boolean) =>
  isBannersProductsCatalog ? INITIAL_PER_PAGE_WITH_BANNER : INITIAL_PER_PAGE

const isBannersProductCatalog = (dataBanners: BannerApiType[] | undefined) =>
  !!dataBanners && dataBanners.length > 0
    ? dataBanners.some(
        (b: BannerApiType) =>
          getBannerStringType({
            type: b.type,
          }) === "catalog_products_single",
      )
    : false

export const getServerSideProps = async ({
  params,
  query: queryUrl,
  req,
  res,
}: GetServerSidePropsContext): Promise<{
  props: ServerSidePropsType
}> => {
  const query = queryUrl as QueryCatalogType | undefined

  if (bannersCache === null) {
    bannersCache = await fetchBanners({
      server: true,
    })
  }

  const isBannersInCatalog = isBannersProductCatalog(bannersCache)

  const perPage = calculatePerPage(isBannersInCatalog)

  if (initialCatalogCache === null || params?.category !== categoryAliasCache) {
    categoryAliasCache =
      params?.category !== undefined ? `${params?.category}` : undefined
    initialCatalogCache = await fetchCatalog({
      server: true,
      categories: !!params?.category ? [`${params.category}`] : undefined,
      perPage: perPage,
      req: req,
      res: res,
    }).catch((err) => {
      console.log("err category ", err)
    })
  }

  const filteredCatalog = await fetchCatalog({
    server: true,
    req: req,
    res: res,
    ...getConfigCatalog(query, perPage),
  }).catch((err) => {
    console.log("err filter category ", err)
  })

  // console.log("initialCatalog ", initialCatalog?.total)
  // console.log("filterCatalog ", filteredCatalog?.total)

  return {
    props: {
      alias: params?.category as string | undefined,
      initialCatalog: initialCatalogCache || null,
      filteredCatalog: filteredCatalog || null,
      isBannersProductsCatalog: isBannersInCatalog,
      bannersSSR: bannersCache || [],
      query: query,
    } as ServerSidePropsType,
  } as {
    props: ServerSidePropsType
  }
}

const user = getClientUser()

export default function CategoryPage({
  alias,
  initialCatalog,
  isBannersProductsCatalog,
  bannersSSR,
  filteredCatalog,
}: ServerSidePropsType): JSX.Element {
  const { breadcrumbs } = useCategoriesBreadcrumbs()
  const { banners, updateBanners } = useApp()
  const categories = useAppSelector((state) => state.catalog.categories)
  const currentCategory = useAppSelector(
    (state) => state.catalog.currentCategory,
  )
  const currentTag = useAppSelector((state) => state.catalog.currentTag.payload)
  const dispatch = useAppDispatch()
  const { setCurrentCategory } = catalogSlice.actions
  const { width } = useWindowSize()
  const router = useRouter()
  const { isAuth } = useAuth()

  const catalogInitialMemoized = useMemo(() => initialCatalog, [initialCatalog])
  const catalogFilteredMemoized = useMemo(
    () => filteredCatalog,
    [filteredCatalog],
  )

  const {
    data: filterCatalogData,
    isIdle,
    isSuccess,
  } = useQuery(
    ["catalog", router.query, user?.accessToken, isAuth],
    () =>
      fetchCatalog({
        ...getConfigCatalog(
          router.query,
          calculatePerPage(isBannersProductsCatalog),
        ),
      }),
    {
      enabled: !user?.accessToken && isAuth,
    },
  )

  useEffect(() => {
    if (banners === null) {
      updateBanners(bannersSSR)
    }
  }, [bannersSSR, updateBanners, banners])

  useEffect(() => {
    if (categories !== null) {
      if (categories.fetched !== null) {
        for (const categoryKey in categories.fetched) {
          if ((categories?.fetched || {})[categoryKey].alias === alias) {
            dispatch(
              setCurrentCategory((categories?.fetched || {})[categoryKey]),
            )
            break
          }
        }
      }
    }
  }, [alias, categories, dispatch, setCurrentCategory])

  return (
    <>
      {currentTag !== null ? (
        <>
          <Meta
            title={currentTag.meta_title}
            description={currentTag.meta_description}
            ogTags={{
              title: currentTag.meta_title,
              image: currentCategory?.image,
              url:
                currentTag.url !== undefined
                  ? currentTag.url.includes("http")
                    ? currentTag.url
                    : `${SITE_URL}${currentTag.url}`
                  : undefined,
            }}
          />
        </>
      ) : (
        <>
          {!!currentCategory?.name && (
            <>
              <Meta
                title={currentCategory?.name}
                description={currentCategory.meta_description}
                ogTags={{
                  title: currentCategory.meta_title,
                  image: currentCategory.image,
                  url:
                    currentCategory.alias !== undefined
                      ? `${SITE_URL}${currentCategory.alias}`
                      : undefined,
                }}
              />
            </>
          )}
        </>
      )}
      <Container>
        <RowGrid>
          <DynamicBreadcrumbs breadcrumbs={breadcrumbs} />

          <PageHeading>
            <Typography variant={"h1"}>
              {!!currentTag?.h1 ? currentTag?.h1 : currentCategory?.name}
            </Typography>
          </PageHeading>

          <FilterProvider>
            <Catalog
              initialCatalog={catalogInitialMemoized || undefined}
              filteredCatalog={
                (!isIdle && isSuccess
                  ? filterCatalogData
                  : catalogFilteredMemoized) || undefined
              }
              isBannersProductsCatalog={isBannersProductsCatalog}
            />
          </FilterProvider>
        </RowGrid>
      </Container>
      {width !== undefined && width > getBreakpointVal(breakpoints.md) && (
        <>
          <DynamicBusinessAreasBanners className={cssHiddenMD} />
        </>
      )}
      {!!banners && banners.catalog_under_products_single && (
        <>
          <SingleUnderCatalog
            banner={banners.catalog_under_products_single[0]}
          />
        </>
      )}
      <Container>
        <Advice />
      </Container>
    </>
  )
}
