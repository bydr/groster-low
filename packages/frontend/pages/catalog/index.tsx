import { useEffect, useState } from "react"
import { NextPage } from "next"
import { Container, Row } from "../../styles/utils/StyledGrid"
import { Typography } from "../../components/Typography/Typography"
import Categories from "../../components/Catalog/Categories/Categories"
import { useAppDispatch, useAppSelector } from "../../hooks/redux"
import { Meta } from "../../components/Meta/Meta"
import { catalogAPI } from "../../api/catalogAPI"
import { useRouter } from "next/router"
import {
  createCategoriesTree,
  normalizeCategoriesByAreas,
} from "../../utils/helpers"
import { ICategoryTreeItem } from "../../types/types"
import { BaseLoader } from "../../components/Loaders/BaseLoader/BaseLoader"
import { TITLE_SITE_RU } from "../../utils/constants"

const CatalogPage: NextPage = () => {
  const { query } = useRouter()
  const { sfera } = query as { sfera?: string }
  const dispatch = useAppDispatch()
  const categories = useAppSelector((state) => state.catalog.categories)
  const [categoriesByAreas, setCategoriesByAreas] = useState<Record<
    string,
    ICategoryTreeItem
  > | null>(null)

  const { data, isSuccess, isLoading } = catalogAPI.useCategoriesByBusinessArea(
    {
      uuidArea: sfera || null,
    },
  )

  useEffect(() => {
    if (isSuccess && !!categories?.fetched && !!data) {
      setCategoriesByAreas(
        createCategoriesTree(
          normalizeCategoriesByAreas(data, categories.fetched),
        ).tree,
      )
    } else {
      setCategoriesByAreas(null)
    }
  }, [dispatch, setCategoriesByAreas, data, isSuccess, categories?.fetched])

  return (
    <>
      <Meta
        title={`Каталог товаров оптом интернет магазина ${TITLE_SITE_RU}`}
      />
      <Container>
        <Row>
          <Typography variant={"h1"}>Каталог</Typography>
          {!!sfera && isLoading && <BaseLoader />}
          <Categories
            categories={
              !!sfera
                ? Object.entries(categoriesByAreas || {}).map(
                    ([, category]) => category,
                  )
                : categories?.treeSorted || []
            }
            variant={"catalog"}
          />
        </Row>
      </Container>
    </>
  )
}

export default CatalogPage
