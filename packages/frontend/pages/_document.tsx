import Document, { Head, Html, Main, NextScript } from "next/document"
import { ReactElement } from "react"
import { TITLE_SITE_RU } from "../utils/constants"

class MyDocument extends Document {
  render(): ReactElement {
    // noinspection HtmlRequiredTitleElement
    return (
      <Html lang="ru">
        <Head>
          <head>
            <title>
              {TITLE_SITE_RU} - товары для бизнеса оптом по низким ценам
            </title>
          </head>
          <link rel="preconnect" href="https://fonts.googleapis.com" />
          <link
            rel="preconnect"
            href="https://fonts.gstatic.com"
            crossOrigin={"true"}
          />
          <link
            href="https://fonts.googleapis.com/css2?family=Montserrat:wght@500;600;700&display=swap"
            rel="stylesheet"
          />
          <link rel="shortcut icon" href="/favicon.ico" />
          <link
            rel="apple-touch-icon"
            sizes="180x180"
            href="/apple-touch-icon.png"
          />
          <link
            rel="icon"
            type="image/png"
            sizes="32x32"
            href="/favicon-32x32.png"
          />
          <link
            rel="icon"
            type="image/png"
            sizes="16x16"
            href="/favicon-16x16.png"
          />
          <link rel="manifest" href="/site.webmanifest" />
          <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#be2be9" />
          <meta name="msapplication-TileColor" content="#fecc00" />
          <meta name="theme-color" content="#be2be9" />
        </Head>
        <body>
          <Main />
          <NextScript />
        </body>
      </Html>
    )
  }
}

export default MyDocument
