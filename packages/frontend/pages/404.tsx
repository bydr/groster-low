import { Container, ContentContainer, Row } from "../styles/utils/StyledGrid"
import { Typography, Typography404 } from "../components/Typography/Typography"
import { ButtonGroup } from "../components/Button/StyledButton"
import { Button } from "../components/Button"
import { useRouter } from "next/router"
import { NextPageWithLayout } from "../types/types"
import { NotFoundPageRecommends } from "../components/LeadHit"

const Custom404: NextPageWithLayout = () => {
  const router = useRouter()

  return (
    <>
      <Container variantWidth={"784px"}>
        <Row>
          <ContentContainer>
            <Typography404>404</Typography404>
            <Typography variant={"h1"}>Cтраница не найдена</Typography>
            <Typography>
              Что-то пошло не так — страница, которую вы хотели открыть, не
              существует.
            </Typography>
            <ButtonGroup
              style={{
                marginTop: "40px",
              }}
            >
              <Button variant={"filled"} as={"a"} href={"/"} size={"large"}>
                Перейти на главную
              </Button>
              <Button
                variant={"link"}
                as={"a"}
                size={"large"}
                isHiddenBg
                onClick={(e) => {
                  e.preventDefault()
                  router.back()
                }}
              >
                Вернуться назад
              </Button>
            </ButtonGroup>
            <br />
            <NotFoundPageRecommends />
          </ContentContainer>
        </Row>
      </Container>
    </>
  )
}

export default Custom404
