import { NextPage } from "next"
import { StaticTemplate } from "../layouts/Static/StaticTemplate"
import React from "react"
import { Note } from "../components/Note"
import { Feedback } from "../components/Forms/Feedback"
import { Typography } from "../components/Typography/Typography"
import { Toggle } from "../components/Toggle"
import { Tab, TabList, TabPanel, Tabs } from "../components/Tabs"
import { useQuery } from "react-query"
import { fetchFaq } from "../api/feedbackAPI"
import { TITLE_SITE_RU } from "../utils/constants"
import { Meta } from "../components/Meta/Meta"
import { StickyContainer } from "../styles/utils/Utils"

const FAQPage: NextPage = () => {
  const { data: dataFaq } = useQuery("faq", () => fetchFaq())

  return (
    <>
      <Meta
        title={`Вопрос-ответ - ${TITLE_SITE_RU}`}
        description={`Свяжитесь с нами, и мы предоставим необходимую информацию.`}
      />
      <StaticTemplate title={"Вопрос-ответ"}>
        <Note
          sideChildren={
            <StickyContainer>
              <Typography variant={"h3"}>
                Не нашли ответа на свой вопрос?
              </Typography>
              <Typography>
                Свяжитесь с нами, и мы предоставим необходимую информацию.
              </Typography>
              <Feedback
                customizePlaceholderMessage={"Вопрос"}
                isRequiredMessage={true}
              />
            </StickyContainer>
          }
        >
          {dataFaq && (
            <>
              <Tabs>
                <TabList variant={"inline"} aria-label="Метод входа">
                  {dataFaq.map((item, index) => (
                    <Tab key={index}>
                      <Typography>{item.name || ""}</Typography>
                    </Tab>
                  ))}
                </TabList>
                {dataFaq.map((item, index) => {
                  return (
                    <TabPanel key={index}>
                      {!!item.questions &&
                        item.questions.map((question, i) => {
                          return (
                            <Toggle
                              key={i}
                              title={question.title || ""}
                              content={(question.answer || "")
                                .split(" ")
                                .map((word) =>
                                  word.includes("http")
                                    ? `<a href="${word}" target="_blank" title="${word}">${word}</a>`
                                    : word,
                                )
                                .join(" ")}
                            />
                          )
                        })}
                    </TabPanel>
                  )
                })}
              </Tabs>
            </>
          )}
        </Note>
      </StaticTemplate>
    </>
  )
}

export default FAQPage
