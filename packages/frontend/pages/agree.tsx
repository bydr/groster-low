import { NextPage } from "next"
import { StaticTemplate } from "../layouts/Static/StaticTemplate"
import React from "react"
import { TITLE_SITE_RU } from "../utils/constants"
import { Note } from "../components/Note"
import { Meta } from "../components/Meta/Meta"
import { AgreeContent } from "../components/Agree/AgreeContent"

const AgreePage: NextPage = () => {
  return (
    <>
      <Meta
        title={`Соглашение об обработке персональных данных - ${TITLE_SITE_RU}`}
        description={`Компания <${TITLE_SITE_RU}> гарантирует конфиденциальность получаемой информации.`}
      />
      <StaticTemplate title={"Соглашения на обработку данных"}>
        <Note>
          <AgreeContent />
        </Note>
      </StaticTemplate>
    </>
  )
}

export default AgreePage
