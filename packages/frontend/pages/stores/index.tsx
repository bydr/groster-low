import { useState } from "react"
import { NextPage } from "next"
import { StaticTemplate } from "../../layouts/Static/StaticTemplate"
import { ListMap } from "../../components/Shops/ListMap"
import {
  cssButtonToggle,
  cssModeList,
  cssModeMap,
  StyledStoresWrapper,
} from "../../components/Shops/StyledStoresPage"
import { cx } from "@linaria/core"
import { Button } from "../../components/Button"
import { Meta } from "../../components/Meta/Meta"
import { TITLE_SITE_RU } from "../../utils/constants"

const StoresPage: NextPage = () => {
  const [mode, setMode] = useState<"map" | "list">("list")
  return (
    <>
      <Meta title={`Магазины - ${TITLE_SITE_RU}`} />
      <StaticTemplate title={"Магазины"}>
        <Button
          icon={mode === "map" ? "ListBullet" : "Location"}
          variant={"link"}
          onClick={() => {
            if (mode === "map") {
              setMode("list")
            }
            if (mode === "list") {
              setMode("map")
            }
          }}
          className={cssButtonToggle}
        >
          {mode === "map" && "Списком"}
          {mode === "list" && "На карте"}
        </Button>
        <StyledStoresWrapper
          className={cx(
            mode === "map" && cssModeMap,
            mode === "list" && cssModeList,
          )}
        >
          <ListMap withRouteToDetail />
        </StyledStoresWrapper>
      </StaticTemplate>
    </>
  )
}

export default StoresPage
