import React from "react"
import { GetServerSidePropsContext } from "next"
import { ShopResponse } from "../../../contracts/contracts"
import { fetchShopDetail } from "../../api/checkoutAPI"
import { Typography } from "../../components/Typography/Typography"
import { StaticTemplate } from "../../layouts/Static/StaticTemplate"
import { Button } from "../../components/Button"
import {
  EMPTY_DATA_PLACEHOLDER,
  ROUTES,
  TITLE_SITE_RU,
} from "../../utils/constants"
import { cssButtonBackward } from "../../components/Button/StyledButton"
import { Note } from "../../components/Note"
import {
  StyledDetailContent,
  StyledDetailMap,
  StyledImageShop,
  StyledShopDetail,
} from "../../components/Shops/StyledDetail"
import {
  ListGroup,
  ListGroupTitle,
} from "../../layouts/Default/Footer/StyledFooter"
import LinkList from "../../components/LinkList/LinkList"
import { cssListItemPurple } from "../../components/LinkList/StyledLinkList"
import { ShopMap } from "../../components/Shops/ShopMap"
import { Meta } from "../../components/Meta/Meta"
import { FORMAT_PHONE, getPhoneWithOutCode } from "../../validations/phone"
import NumberFormat from "react-number-format"
import { EntityImage } from "../../components/EntityImage/EntityImage"

export const getServerSideProps = async ({
  params,
}: GetServerSidePropsContext): Promise<{
  props: {
    shop: ShopResponse | null
    id?: string
  }
}> => {
  const shop = await fetchShopDetail({
    id: params?.id !== undefined ? `${params?.id}` : "",
    server: true,
  }).catch((err) => {
    console.log("err ", err)
  })

  return {
    props: {
      shop: shop || null,
      id: (params?.id || undefined) as string | undefined,
    },
  }
}

const ShopDetailPage = ({
  shop,
  id,
}: {
  shop: ShopResponse | null
  id?: string
}): JSX.Element => {
  return (
    <>
      {!!shop ? (
        <>
          <Meta
            title={`Магазин по адресу ${shop.address} - ${TITLE_SITE_RU}`}
          />
          <StaticTemplate>
            <Button
              variant={"small"}
              size={"small"}
              isHiddenBg
              icon={"ArrowLeft"}
              iconPosition={"left"}
              as={"a"}
              href={`${ROUTES.stores}`}
              className={cssButtonBackward}
            >
              Назад
            </Button>
            <Note>
              <Typography variant={"h1"}>
                {(shop.address || "").length > 0
                  ? shop.address
                  : EMPTY_DATA_PLACEHOLDER}
              </Typography>
            </Note>
            <StyledShopDetail>
              <StyledDetailContent>
                {!!shop.description && (
                  <>
                    <Typography>{shop.description}</Typography>
                  </>
                )}

                {!!shop.image && (
                  <>
                    <StyledImageShop>
                      <EntityImage
                        imagePath={shop.image}
                        alt={shop.address || ""}
                        objectFit={"cover"}
                        layout={"fill"}
                        priority={true}
                        quality={30}
                      />
                    </StyledImageShop>
                  </>
                )}

                {!!shop.phone && (
                  <ListGroup>
                    <ListGroupTitle>Телефон</ListGroupTitle>
                    <LinkList
                      items={[
                        {
                          title: (
                            <NumberFormat
                              format={FORMAT_PHONE}
                              mask={"_"}
                              value={getPhoneWithOutCode(shop.phone || "")}
                              displayType={"text"}
                            />
                          ),
                          path: `tel:${shop.phone}`,
                          icon: "Phone",
                        },
                      ]}
                    />
                  </ListGroup>
                )}

                {!!shop.email && (
                  <ListGroup>
                    <ListGroupTitle>Отдел продаж</ListGroupTitle>
                    <LinkList
                      items={[
                        {
                          title: shop.email,
                          path: `mailto:${shop.email}`,
                          icon: "Email",
                        },
                      ]}
                      className={cssListItemPurple}
                    />
                  </ListGroup>
                )}

                {!!shop.schedule && (
                  <ListGroup>
                    <ListGroupTitle>График работы</ListGroupTitle>
                    <Typography>{shop.schedule}</Typography>
                  </ListGroup>
                )}
              </StyledDetailContent>
              <StyledDetailMap>
                <ShopMap
                  zoom={18}
                  shops={[
                    {
                      ...shop,
                      uuid: id || "",
                    },
                  ]}
                />
              </StyledDetailMap>
            </StyledShopDetail>
          </StaticTemplate>
        </>
      ) : (
        <>
          <StaticTemplate title={"Произошла ошибка"} />
        </>
      )}
    </>
  )
}

export default ShopDetailPage
