import React from "react"
import { NextPageWithRequiredAuth } from "../../_app"
import { Payers } from "../../../components/Account/Payers"
import { Meta } from "../../../components/Meta/Meta"
import { TITLE_SITE_RU } from "../../../utils/constants"
import { AccountTemplate } from "../../../layouts/Account/AccountTemplate"

const PayersPage: NextPageWithRequiredAuth = () => {
  return (
    <>
      <Meta title={`Плательщики - ${TITLE_SITE_RU}`} />
      <AccountTemplate title={"Плательщики"}>
        <Payers />
      </AccountTemplate>
    </>
  )
}

PayersPage.requireAuth = true

export default PayersPage
