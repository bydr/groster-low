import React from "react"
import { NextPageWithRequiredAuth } from "../../_app"
import { Typography } from "../../../components/Typography/Typography"
import { Meta } from "../../../components/Meta/Meta"
import { TITLE_SITE_RU } from "../../../utils/constants"
import { AccountTemplate } from "../../../layouts/Account/AccountTemplate"

const Staff: NextPageWithRequiredAuth = () => {
  return (
    <>
      <Meta title={`Сотрудники - ${TITLE_SITE_RU}`} />
      <AccountTemplate title={"Сотрудники"}>
        <Typography variant={"p14"}>В разработке...</Typography>
      </AccountTemplate>
    </>
  )
}

Staff.requireAuth = true

export default Staff
