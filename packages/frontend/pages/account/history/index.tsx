import React from "react"
import { NextPageWithRequiredAuth } from "../../_app"
import { HistoryOrders } from "../../../components/Account/History/Orders"
import { Provider as FilterProvider } from "../../../hooks/filterHistoryOrders"
import { Meta } from "../../../components/Meta/Meta"
import { TITLE_SITE_RU } from "../../../utils/constants"
import { AccountTemplate } from "../../../layouts/Account/AccountTemplate"

const HistoryOrdersPage: NextPageWithRequiredAuth = () => {
  return (
    <>
      <Meta title={`История заказов - ${TITLE_SITE_RU}`} />
      <AccountTemplate title={"История заказов"}>
        <FilterProvider>
          <HistoryOrders />
        </FilterProvider>
      </AccountTemplate>
    </>
  )
}

HistoryOrdersPage.requireAuth = true

export default HistoryOrdersPage
