import React from "react"
import { RequiredAuth } from "../../_app"
import { Detail } from "components/Account/History/Orders/Order/Detail"
import { GetServerSidePropsContext } from "next"
import { AccountTemplate } from "../../../layouts/Account/AccountTemplate"

type ServerSidePropsType = {
  uid: string | null
}

export const getServerSideProps = ({
  params,
}: GetServerSidePropsContext): { props: ServerSidePropsType } => {
  return {
    props: {
      uid: params?.uid || null,
    } as ServerSidePropsType,
  } as {
    props: ServerSidePropsType
  }
}

const OrderDetailPage = ({
  uid,
}: ServerSidePropsType & RequiredAuth): JSX.Element => {
  return (
    <>
      <AccountTemplate>
        <Detail uid={uid} />
      </AccountTemplate>
    </>
  )
}

OrderDetailPage.requireAuth = true

export default OrderDetailPage
