import React from "react"
import { NextPageWithRequiredAuth } from "../../_app"
import { Meta } from "../../../components/Meta/Meta"
import { TITLE_SITE_RU } from "../../../utils/constants"
import { AccountTemplate } from "../../../layouts/Account/AccountTemplate"
import { Recommendations } from "../../../components/Account/Recommendations"

const AccountRecommendationsPage: NextPageWithRequiredAuth = () => {
  return (
    <>
      <Meta title={`Персональные рекомендации - ${TITLE_SITE_RU}`} />
      <AccountTemplate title={"Персональные рекомендации"}>
        <Recommendations />
      </AccountTemplate>
    </>
  )
}

AccountRecommendationsPage.requireAuth = true

export default AccountRecommendationsPage
