import React from "react"
import { NextPageWithRequiredAuth } from "../../_app"
import { Addresses } from "../../../components/Account/Addresses"
import { Meta } from "../../../components/Meta/Meta"
import { TITLE_SITE_RU } from "../../../utils/constants"
import { AccountTemplate } from "../../../layouts/Account/AccountTemplate"

const AddressesPage: NextPageWithRequiredAuth = () => {
  return (
    <>
      <Meta title={`Адреса - ${TITLE_SITE_RU}`} />
      <AccountTemplate title={"Адреса"}>
        <Addresses />
      </AccountTemplate>
    </>
  )
}

AddressesPage.requireAuth = true

export default AddressesPage
