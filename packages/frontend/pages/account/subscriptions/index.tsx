import React from "react"
import { NextPageWithRequiredAuth } from "../../_app"
import { Waiting } from "../../../components/Account/Waiting/Waiting"
import { Meta } from "../../../components/Meta/Meta"
import { TITLE_SITE_RU } from "../../../utils/constants"
import { AccountTemplate } from "../../../layouts/Account/AccountTemplate"

const SubscriptionsPage: NextPageWithRequiredAuth = () => {
  return (
    <>
      <Meta title={`Подписки - ${TITLE_SITE_RU}`} />
      <AccountTemplate title={"Товары в ожидании на поступление"}>
        <Waiting />
      </AccountTemplate>
    </>
  )
}

SubscriptionsPage.requireAuth = true

export default SubscriptionsPage
