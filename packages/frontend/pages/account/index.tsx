import React from "react"
import { NextPageWithRequiredAuth } from "../_app"
import { Profile } from "../../components/Account/Profile/Profile"
import { Meta } from "../../components/Meta/Meta"
import { TITLE_SITE_RU } from "../../utils/constants"
import { AccountTemplate } from "../../layouts/Account/AccountTemplate"

const Account: NextPageWithRequiredAuth = () => {
  return (
    <>
      <Meta title={`Профиль - ${TITLE_SITE_RU}`} />
      <AccountTemplate title={"Профиль"}>
        <Profile />
      </AccountTemplate>
    </>
  )
}

Account.requireAuth = true

export default Account
