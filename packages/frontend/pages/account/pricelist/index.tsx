import React from "react"
import { NextPageWithRequiredAuth } from "../../_app"
import { Pricelist } from "../../../components/Account/Pricelist"
import { Meta } from "../../../components/Meta/Meta"
import { TITLE_SITE_RU } from "../../../utils/constants"
import { AccountTemplate } from "../../../layouts/Account/AccountTemplate"

const PricelistPage: NextPageWithRequiredAuth = () => {
  return (
    <>
      <Meta title={`Прайс-лист - ${TITLE_SITE_RU}`} />
      <AccountTemplate title={"Прайс-лист"}>
        <Pricelist />
      </AccountTemplate>
    </>
  )
}

PricelistPage.requireAuth = true

export default PricelistPage
