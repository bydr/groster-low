import React from "react"
import { NextPageWithRequiredAuth } from "../../_app"
import { Favorites } from "../../../components/Favorites"
import { Meta } from "../../../components/Meta/Meta"
import { TITLE_SITE_RU } from "../../../utils/constants"
import { AccountTemplate } from "../../../layouts/Account/AccountTemplate"

const AccountFavoritesPage: NextPageWithRequiredAuth = () => {
  return (
    <>
      <Meta title={`Избранное - ${TITLE_SITE_RU}`} />
      <AccountTemplate title={"Избранное"}>
        <Favorites />
      </AccountTemplate>
    </>
  )
}

AccountFavoritesPage.requireAuth = true

export default AccountFavoritesPage
