import { fetchBanners } from "../api/bannersAPI"
import { fetchCatalog, fetchHits } from "../api/catalogAPI"
import { Meta } from "../components/Meta/Meta"
import { Homepage, HomepagePropsType } from "../components/Homepage"
import { CatalogResponseType } from "../types/types"
import { BannerApiType, ProductCatalogType } from "../../contracts/contracts"

let newCache = null as CatalogResponseType | null
let hitsCache = null as ProductCatalogType[] | null
let bannersCache = null as BannerApiType[] | null

export const getServerSideProps = async (): Promise<{
  props: HomepagePropsType
}> => {
  if (bannersCache === null) {
    bannersCache = await fetchBanners({
      server: true,
    })
  }

  if (hitsCache === null) {
    hitsCache = await fetchHits({
      server: true,
    })
  }

  if (newCache === null) {
    newCache = await fetchCatalog({
      isEnabled: "1",
      perPage: 20,
      isNew: true,
      server: true,
    })
  }

  return {
    props: {
      banners: bannersCache || [],
      hits: hitsCache || [],
      new: newCache || null,
    },
  }
}

export default function Home({
  banners: bannersSSR,
  hits: hitsSSR,
  new: newSSR,
}: HomepagePropsType): JSX.Element {
  return (
    <>
      <Meta />
      <Homepage banners={bannersSSR} hits={hitsSSR} new={newSSR} />
    </>
  )
}
