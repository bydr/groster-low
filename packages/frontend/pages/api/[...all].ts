import httpProxyMiddleware, {
  NextHttpProxyMiddlewareOptions,
} from "next-http-proxy-middleware"
import { NextApiRequest, NextApiResponse } from "next"
import { BASE_URL } from "../../utils/constants"

export const config = {
  api: {
    externalResolver: true,
  },
}

const options: NextHttpProxyMiddlewareOptions = {
  target: BASE_URL,
}

// eslint-disable-next-line import/no-anonymous-default-export
export default async (
  req: NextApiRequest,
  res: NextApiResponse,
): Promise<unknown> => {
  return await httpProxyMiddleware(req, res, options)
}
