// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import { NextApiRequest, NextApiResponse } from "next"
import { GVerifyReturnType } from "../../api/reCaptchaAPI"

const handler = (req: NextApiRequest, res: NextApiResponse): void => {
  if (req.method === "POST") {
    try {
      void fetch("https://www.google.com/recaptcha/api/siteverify", {
        method: "POST",
        headers: {
          "Content-Type": "application/x-www-form-urlencoded",
        },
        body: `secret=${process.env.NEXT_PUBLIC_RECAPTCHA_SECRET_KEY}&response=${req.body.gRecaptchaToken}`,
      })
        .then((reCaptchaRes) => reCaptchaRes.json())
        .then((reCaptchaRes) => {
          if (reCaptchaRes?.score > 0.5) {
            // Save data to the database from here
            res.status(200).json({
              status: "success",
              message: "Enquiry submitted successfully",
            } as GVerifyReturnType)
          } else {
            res.status(200).json({
              status: "failure",
              message: "Google ReCaptcha Failure",
            } as GVerifyReturnType)
          }
        })
    } catch (err) {
      res.status(405).json({
        status: "failure",
        message: "Error submitting the enquiry form",
      } as GVerifyReturnType)
    }
  } else {
    res.status(405)
    res.end()
  }
}

export default handler
