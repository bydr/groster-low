import React from "react"
import { StaticTemplate } from "../layouts/Static/StaticTemplate"
import { NextPage } from "next"
import { Note } from "../components/Note"
import { Typography } from "../components/Typography/Typography"
import {
  cssListVertical,
  StyledList,
  StyledListItem,
} from "../components/List/StyledList"
import { TITLE_SITE_RU } from "../utils/constants"
import { Meta } from "../components/Meta/Meta"

const ProductWarrantyPage: NextPage = () => {
  return (
    <>
      <Meta
        title={`Гарантия на товар - ${TITLE_SITE_RU}`}
        description={`Гарантийный период – это срок, во время которого клиент, обнаружив
            недостаток товара имеет право потребовать от продавца или
            изготовителя принять меры по устранению дефекта. Продавец должен
            устранить недостатки, если не будет доказано, что они возникли
            вследствие нарушений покупателем правил эксплуатации.`}
      />
      <StaticTemplate title={"Гарантия на товар"}>
        <Note>
          <Typography>
            Гарантийный период – это срок, во время которого клиент, обнаружив
            недостаток товара имеет право потребовать от продавца или
            изготовителя принять меры по устранению дефекта. Продавец должен
            устранить недостатки, если не будет доказано, что они возникли
            вследствие нарушений покупателем правил эксплуатации.
          </Typography>
        </Note>
        <Note>
          <Typography variant={"h2"}>
            С какого момента начинается гарантия?
          </Typography>
          <StyledList isDefault className={cssListVertical}>
            <StyledListItem>
              с момента передачи товара потребителю, если в договоре нет
              уточнения;
            </StyledListItem>
            <StyledListItem>
              если нет возможности установить день покупки, то гарантия идёт с
              момента изготовления;
            </StyledListItem>
            <StyledListItem>
              на сезонные товары гарантия идёт с момента начала сезона;
            </StyledListItem>
            <StyledListItem>
              при заказе товара из интернет-магазина гарантия начинается со дня
              доставки.
            </StyledListItem>
          </StyledList>
          <br />
          <Typography>Обслуживание по гарантии включает в себя:</Typography>
          <StyledList isDefault className={cssListVertical}>
            <StyledListItem>
              устранение недостатков товара в сертифицированных сервисных
              центрах;
            </StyledListItem>
            <StyledListItem>
              обмен на аналогичный товар без доплаты;
            </StyledListItem>
            <StyledListItem>обмен на похожий товар с доплатой;</StyledListItem>
            <StyledListItem>
              возврат товара и перечисление денежных средств на счёт покупателя.
            </StyledListItem>
          </StyledList>
        </Note>
        <Note>
          <Typography variant={"h2"}>
            Правила обмена и возврата товара
          </Typography>
        </Note>
        <Note>
          <Typography variant={"h3"}>
            Обмен и возврат продукции надлежащего качества
          </Typography>
          <Typography>
            Продавец гарантирует, что покупатель в течение 7 дней с момента
            приобретения товара может отказаться от товара надлежащего качества,
            если:
          </Typography>
          <StyledList isDefault className={cssListVertical}>
            <StyledListItem>
              товар не поступал в эксплуатацию и имеет товарный вид, находится в
              упаковке со всеми ярлыками, а также есть документы на приобретение
              товара;
            </StyledListItem>
            <StyledListItem>
              товар не входит в перечень продуктов надлежащего качества, не
              подлежащих возврату и обмену.
            </StyledListItem>
          </StyledList>
          <Typography>
            Покупатель имеет право обменять товар надлежащего качество на другое
            торговое предложение этого товара или другой товар, идентичный по
            стоимости или на иной товар с доплатой или возвратом разницы в цене.
            (При обмене покупатель должен иметь при себе накладную)
          </Typography>
        </Note>
        <Note>
          <Typography variant={"h3"}>
            Обмен и возврат продукции ненадлежащего качества
          </Typography>
          <Typography>
            Если покупатель обнаружил недостатки товара после его приобретения,
            то он может потребовать замену у продавца. Замена должна быть
            произведена в течение 7 дней со дня предъявления требования. В
            случае, если будет назначена экспертиза на соответствие товара
            указанным нормам, то обмен должен быть произведён в течение 20 дней.
          </Typography>
          <Typography>
            Технически сложные товары ненадлежащего качества заменяются товарами
            той же марки или на аналогичный товар другой марки с перерасчётом
            стоимости. Возврат производится путем аннулирования договора
            купли-продажи и возврата суммы в размере стоимости товара.
          </Typography>
        </Note>
        <Note>
          <Typography variant={"h2"}>Возврат денежных средств</Typography>
          <Typography>
            Срок возврата денежных средств зависит от вида оплаты, который
            изначально выбрал покупатель.
          </Typography>
          <Typography>
            При наличном расчете возврат денежных средств осуществляется на
            кассе не позднее через через 10 дней после предъявления покупателем
            требования о возврате.
          </Typography>
          <Typography>
            При использовании электронных платёжных систем, возврат
            осуществляется на электронный счёт в течение 10 календарных дней.
          </Typography>
        </Note>
      </StaticTemplate>
    </>
  )
}

export default ProductWarrantyPage
