import { Typography } from "../../components/Typography/Typography"
import { Container, RowContentSidebar } from "../../styles/utils/StyledGrid"
import { Icon } from "../../components/Icon"
import { Button } from "../../components/Button"
import {
  cssIsActive,
  HeadingStep,
  HeadingSteps,
  PageHeading,
} from "../../styles/utils/Utils"
import { breakpoints, colors } from "../../styles/utils/vars"
import { Cart } from "../../components/Cart/Cart"
import { useCart } from "../../hooks/cart"
import { Popover } from "../../components/Popover/Popover"
import { CheckoutButtonGroup } from "../../components/Button/StyledButton"
import { Meta } from "../../components/Meta/Meta"
import { ROUTES, TITLE_SITE_RU } from "../../utils/constants"
import { StyledCartContainer } from "../../components/Cart/StyledCart"
import { Link } from "../../components/Link"
import { CartPageRecommends } from "../../components/LeadHit"

export default function CartPage(): JSX.Element {
  const { clearCart, shareCart, isSharedLinkCopied, products } = useCart()

  return (
    <>
      <Meta title={`Моя корзина - ${TITLE_SITE_RU}`} />
      <StyledCartContainer>
        <Container>
          <RowContentSidebar>
            <PageHeading>
              <HeadingSteps>
                <HeadingStep className={cssIsActive}>Корзина</HeadingStep>
                <Icon NameIcon={"ArrowCircleRight"} />
                <HeadingStep>
                  <Link href={ROUTES.checkout}>Оформление заказа</Link>
                </HeadingStep>
              </HeadingSteps>
              {products !== null && (
                <>
                  <CheckoutButtonGroup justifyContent={"flex-end"}>
                    <Button
                      variant={"link"}
                      icon={"Print"}
                      hideTextOnBreakpoint={breakpoints.sm}
                      onClick={() => {
                        window.print()
                      }}
                    >
                      Напечатать
                    </Button>

                    <Popover
                      disclosure={
                        <Button
                          variant={"link"}
                          icon={"Share"}
                          hideTextOnBreakpoint={breakpoints.sm}
                          onClick={shareCart}
                        >
                          Поделиться
                        </Button>
                      }
                      isShow={isSharedLinkCopied}
                      isStylingIconDisclosure={false}
                      size={"small"}
                    >
                      <Typography variant={"p12"} color={colors.green}>
                        Ссылка скопирована!
                      </Typography>
                    </Popover>

                    <Button
                      variant={"link"}
                      icon={"Delete"}
                      onClick={clearCart}
                    >
                      Очистить корзину
                    </Button>
                  </CheckoutButtonGroup>
                </>
              )}
            </PageHeading>

            <Cart />
          </RowContentSidebar>

          <CartPageRecommends />
        </Container>
      </StyledCartContainer>
    </>
  )
}
