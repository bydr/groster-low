import React from "react"
import {
  cssIsActive,
  HeadingStep,
  HeadingSteps,
  PageHeading,
} from "../../styles/utils/Utils"
import { Icon } from "../../components/Icon"
import { Container } from "../../styles/utils/StyledGrid"
import { Checkout } from "../../components/Checkout/Checkout"
import { CheckoutLayout } from "../../layouts/Checkout/CheckoutLayout"
import { NextPageWithLayout } from "../../types/types"
import { Meta } from "../../components/Meta/Meta"
import { ROUTES, TITLE_SITE_RU } from "../../utils/constants"
import { CheckoutRow } from "../../components/Checkout/StyledCheckout"
import { Link } from "../../components/Link"

const CheckoutPage: NextPageWithLayout = () => {
  return (
    <>
      <Meta title={`Оформление заказа - ${TITLE_SITE_RU}`} />
      <Container>
        <CheckoutRow>
          <PageHeading>
            <HeadingSteps>
              <HeadingStep>
                <Link href={ROUTES.cart}>Корзина</Link>
              </HeadingStep>
              <Icon NameIcon={"ArrowCircleRight"} />
              <HeadingStep className={cssIsActive}>
                Оформление заказа
              </HeadingStep>
            </HeadingSteps>
          </PageHeading>
          <Checkout />
        </CheckoutRow>
      </Container>
    </>
  )
}

CheckoutPage.getLayout = (page) => {
  return <CheckoutLayout>{page}</CheckoutLayout>
}

export default CheckoutPage
