import { NextPage } from "next"
import { StaticTemplate } from "../layouts/Static/StaticTemplate"
import React from "react"
import { Typography } from "../components/Typography/Typography"
import {
  cssListVertical,
  StyledList,
  StyledListItem,
} from "../components/List/StyledList"
import { TITLE_SITE_RU } from "../utils/constants"
import { Meta } from "../components/Meta/Meta"

const TermDeliveryPage: NextPage = () => {
  return (
    <>
      <Meta
        title={`Доставка товаров - ${TITLE_SITE_RU}`}
        description={`Наш интернет-магазин предлагает несколько вариантов доставки: ✅ курьерская; ✅ самовывоз из магазина; ✅ транспортные компании.`}
      />
      <StaticTemplate title={"Условия доставки"}>
        <Typography>
          После оформления заказа и обсуждения всех деталей, мы оформляем
          доставку товаров.
        </Typography>
        <Typography>Какие способы доставки Вы можете выбрать:</Typography>
        <StyledList className={cssListVertical} isDefault>
          {[
            "Самовывоз из магазина-партнёра;",
            "Курьером;",
            "Транспортной компанией.",
          ].map((item, index) => (
            <StyledListItem key={index}>{item}</StyledListItem>
          ))}
        </StyledList>
        <Typography>При заказе от 1000 рублей доставка бесплатная!</Typography>

        <Typography variant={"h2"}>Курьерская доставка</Typography>
        <Typography>
          Курьерская доставка доступна не во всех городах! Уточняйте у менеджера
          <br />
          Вы можете заказать доставку товара с помощью курьера, который прибудет
          по указанному адресу в будние дни и субботу с 8.00 до 23.00.
          <br />
          Вы вскрываете упаковку при курьере, осматриваете на целостность и
          соответствие указанной комплектации. Если курьер приехал на ваш адрес
          и вас не оказалось на месте бесплатное ожидание ограничено 10
          минутами. После осмотра товара вы можете отказаться частично или
          полностью от покупки.
        </Typography>

        <Typography variant={"h2"}>Самовывоз из магазина</Typography>
        <Typography>
          Вы можете забрать товар в одном из магазинов, сотрудничающих с нами.
          Список торговых точек, которые принимают заказы от нашей компании
          появится у вас в корзине. Вы просто идёте в этот магазин, обращаетесь
          к сотруднику в кассовой зоне и называете номер заказа. Забрать покупку
          может ваш друг или родственник, который знает номер и имя, на кого он
          оформлен.
        </Typography>

        <Typography variant={"h2"}>Транспортные компании</Typography>
        <Typography>
          Мы можем бесплатно доставить наши товары до любой удобной вам
          транспортной компанией, при условии заказа от 1000р. Логистические
          расходы ложатся на покупателя.
        </Typography>

        <Typography variant={"h2"}>Сроки доставки</Typography>
        <Typography>Сроки доставки:</Typography>
        <StyledList className={cssListVertical} isDefault>
          {[
            "По Волгограду и в город Волжский – в день заказа;",
            "В города Волгоградской области и другие близлежащие города – обговаривается с менеджером, срок не превышает неделю.",
            "В другие города РФ – в соответствии со сроками транспортной компании, до транспортной компании – в тот же день.",
            "Если у Вас срочный заказ – мы отправим его на такси.",
          ].map((item, index) => (
            <StyledListItem key={index}>{item}</StyledListItem>
          ))}
        </StyledList>
        <Typography>
          Условия и сроки доставки обговариваются с менеджером и зависят от
          множества факторов.
        </Typography>
      </StaticTemplate>
    </>
  )
}

export default TermDeliveryPage
