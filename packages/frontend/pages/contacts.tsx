import React from "react"
import { StaticTemplate } from "../layouts/Static/StaticTemplate"
import { NextPage } from "next"
import { Note } from "../components/Note"
import { Feedback } from "../components/Forms/Feedback"
import { Typography } from "../components/Typography/Typography"
import { ListMap } from "../components/Shops/ListMap"
import {
  ListGroup,
  ListGroupTitle,
  RowListGroup,
} from "../layouts/Default/Footer/StyledFooter"
import LinkList from "../components/LinkList/LinkList"
import { Col } from "../styles/utils/StyledGrid"
import { cssListItemPurple } from "../components/LinkList/StyledLinkList"
import { CONTACTS, TITLE_SITE_RU } from "../utils/constants"
import { Meta } from "../components/Meta/Meta"

const ContactsPage: NextPage = () => {
  return (
    <>
      <Meta
        title={`Контакты - ${TITLE_SITE_RU}`}
        description={`Основной офис компании находится в Волгограде. Обязательно приходите
            к нам на чашку ароматного кофе. Совместно обсудим ваши проекты и
            предложим рекомендации.`}
      />
      <StaticTemplate title={"Контакты"}>
        <Note
          sideChildren={
            <>
              <Typography variant={"h2"}>Обратная связь</Typography>
              <Feedback />
            </>
          }
        >
          <Typography>
            Основной офис компании находится в Волгограде. Обязательно приходите
            к нам на чашку ароматного кофе. Совместно обсудим ваши проекты и
            предложим рекомендации.
          </Typography>
          <RowListGroup>
            <Col>
              <ListGroup>
                <ListGroupTitle>Наши контакты</ListGroupTitle>
                <LinkList items={CONTACTS} />
              </ListGroup>
            </Col>
            <Col>
              <ListGroup>
                <ListGroupTitle>Отдел продаж</ListGroupTitle>
                <LinkList
                  items={[
                    {
                      title: "zakaz@groster.me",
                      path: "mailto:zakaz@groster.me",
                      icon: "Email",
                    },
                  ]}
                  className={cssListItemPurple}
                />
              </ListGroup>
              <ListGroup>
                <ListGroupTitle>Отдел Закупок</ListGroupTitle>
                <LinkList
                  items={[
                    {
                      title: "zakupki@groster.me",
                      path: "mailto:zakupki@groster.me",
                      icon: "Email",
                    },
                  ]}
                  className={cssListItemPurple}
                />
              </ListGroup>
            </Col>
            <Col
              style={{
                flex: 1.5,
              }}
            >
              <ListGroupTitle>Режим работы</ListGroupTitle>
              <Typography>
                C 8 до 18 принимаем заявки и по телефону. 24/7 в Электронном
                виде: Whats app, Viber, Telegram, интернет-магазин. Доставка
                товаров производится с 8 до 23.
              </Typography>
            </Col>
          </RowListGroup>
          <ListMap />
        </Note>
      </StaticTemplate>
    </>
  )
}
export default ContactsPage
