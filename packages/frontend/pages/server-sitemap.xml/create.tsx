import { GetServerSidePropsContext } from "next"
import fs from "fs"
import { ROUTES, SITE_URL } from "../../utils/constants"
import { NextPageWithLayout } from "../../types/types"
import { SitemapBuilder } from "next-sitemap"
import { fetchCategories } from "../../api/catalogAPI"
import { getProductsSite } from "../../utils/helpers"

export const DIR = `${process.cwd()}/public`
export const PATH = `${DIR}/docs/server-sitemap.xml`

type PayloadType = { alias: string; id: string }

type ToFormattedType = (
  data: PayloadType[],
  middlePath?: string,
) => { loc: string; lastmod: string }[]

const toFormatted: ToFormattedType = (data, middlePath = "/") => {
  return data.map((item) => ({
    loc: `${SITE_URL}${middlePath}${item.alias.toString()}`,
    lastmod: new Date().toISOString(),
  }))
}

const getCategoriesPayload: () => Promise<PayloadType[]> = async () => {
  const data = await fetchCategories({
    server: true,
  })

  return !!data.categories && data.categories.length > 0
    ? data.categories.map((c) => ({
        id: c.uuid || "",
        alias: c.alias || "",
      }))
    : []
}

const getProductsPayload: (
  categories: string[],
) => Promise<PayloadType[]> = async (categories) => {
  return await getProductsSite<PayloadType>(
    categories,
    (p): PayloadType => ({
      id: p.uuid || "",
      alias: p.alias || "",
    }),
  )
}

const SiteMapCreate: NextPageWithLayout = () => {
  return <></>
}

export const getServerSideProps = async ({
  res,
}: GetServerSidePropsContext): Promise<{ props: unknown }> => {
  const categories = await getCategoriesPayload()
  const products = await getProductsPayload(categories.map((c) => c.id))

  const fields = [
    ...toFormatted(categories, `${ROUTES.catalog}/`),
    ...toFormatted(products, `${ROUTES.product}/`),
  ]

  const builder = new SitemapBuilder()
  const generated = builder.buildSitemapXml(fields)
  console.log("sitemap-dynamic: ", generated)

  res.setHeader("Content-Type", "text/plain")
  if (!fs.existsSync(DIR)) {
    fs.mkdirSync(DIR, {
      recursive: true,
    })
  }
  try {
    fs.writeFileSync(PATH, generated)
  } catch (err) {
    res.write(`${err}`)
    res.end()
  }
  res.write("File is created success")
  res.end()

  return {
    props: {},
  }
}

SiteMapCreate.getLayout = (page) => {
  return <>{page}</>
}
export default SiteMapCreate
