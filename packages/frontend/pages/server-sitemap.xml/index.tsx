import React from "react"
import { GetServerSidePropsContext } from "next"
import * as fs from "fs"
import { NextPageWithLayout } from "../../types/types"
import { PATH } from "./create"

const SiteMap: NextPageWithLayout = () => {
  return <></>
}

export const getServerSideProps = ({
  res,
}: GetServerSidePropsContext): {
  props: unknown
} => {
  let feed = ""

  res.setHeader("Content-Type", "text/plain")
  try {
    feed = fs.readFileSync(PATH, "utf8")
  } catch (err) {
    res.write(`${err}`)
    res.end()
  }

  res.setHeader("Content-Type", "text/xml")
  res.write(feed)
  res.end()

  return {
    props: {},
  }
}

export default SiteMap
