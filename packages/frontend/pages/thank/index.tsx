import { useEffect } from "react"
import { NextPage } from "next"
import { Container, ContentContainer, Row } from "../../styles/utils/StyledGrid"
import { Typography } from "../../components/Typography/Typography"
import { cartSlice } from "../../store/reducers/cartSlice"
import { useAppDispatch } from "../../hooks/redux"
import { useCart } from "../../hooks/cart"
import { OrderThank } from "../../components/Orders/OrderThank/OrderThank"
import { Button } from "../../components/Button"
import { ROUTES, TITLE_SITE_RU } from "../../utils/constants"
import { StyledThankYouPage } from "../../components/Orders/StyledOrders"
import { Meta } from "../../components/Meta/Meta"
import Script from "next/script"
import { priceToFormat } from "../../utils/helpers"

const ThankYouPage: NextPage = () => {
  const { setOrder, clearCart } = cartSlice.actions
  const dispatch = useAppDispatch()
  const { order } = useCart()

  useEffect(() => {
    dispatch(clearCart())
  }, [clearCart, dispatch])

  useEffect(() => {
    return () => {
      dispatch(setOrder(null))
    }
  }, [dispatch, setOrder])

  return (
    <>
      <Meta title={`Благодарим за заказ - ${TITLE_SITE_RU}`} />
      <StyledThankYouPage>
        <Container variantWidth={"784px"}>
          <Row>
            <ContentContainer>
              {order !== null ? (
                <>
                  <Typography variant={"h1"}>Cпасибо за ваш заказ</Typography>
                  <OrderThank order={order} />
                  <Button variant={"filled"} size={"large"} as={"a"} href={"/"}>
                    На главную
                  </Button>
                </>
              ) : (
                <>
                  <Typography variant={"h1"}>Вперед за покупками</Typography>
                  <Button
                    variant={"filled"}
                    size={"large"}
                    as={"a"}
                    href={ROUTES.catalog}
                  >
                    В каталог
                  </Button>
                </>
              )}
            </ContentContainer>
          </Row>
        </Container>
      </StyledThankYouPage>
      {order !== null && (
        <>
          <Script
            id={"leadhitOrder"}
            dangerouslySetInnerHTML={{
              __html: `
        (function () {
    function readCookie(name) {
      if (document.cookie.length > 0) {
        offset = document.cookie.indexOf(name + "=");
        if (offset != -1) {
          offset = offset + name.length + 1;
          tail = document.cookie.indexOf(";", offset);
          if (tail == -1) tail = document.cookie.length;
          return unescape(document.cookie.substring(offset, tail));
        }
      }
      return null;
    }

    var lh_clid = "${process.env.NEXT_PUBLIC_LEADHIT_ID}"/* ID Магазина */

    /* Вместо строки в кавычках подставить конкретное значение */
    // Код заказа
    var order_id = '${order.id || ""}' // String
    // Сумма заказа
    var cart_sum = '${priceToFormat(order.totalCost || 0)}' // String
  
    var order_offers = ${JSON.stringify(
      order.offers !== undefined
        ? order.offers.map((order) => ({
            ...order,
            price: +priceToFormat(order.price || 0),
          }))
        : [],
    )} /* товары в заказе */

    var uid = readCookie('_lhtm_u');
    var vid = readCookie('_lhtm_r')?.split('|')[1];
    var url = encodeURIComponent(window.location.href);
    var path = "https://track.leadhit.io/stat/lead_form?f_orderid=" + order_id + "&url=" + url + "&action=lh_orderid&uid=" + uid + "&vid=" + vid + "&ref=direct&f_cart_sum=" + cart_sum + "&clid=" + lh_clid;

    var sc = document.createElement("script");
    sc.type = 'text/javascript';
    var headID = document.getElementsByTagName("head")[0];
    sc.src = path;
    headID.appendChild(sc);

    if (Array.isArray(order_offers) && order_offers.length > 0) {
      var requestBody = {
        'order_id': order_id,
        'cart_sum': cart_sum,
        'vid': vid,
        'uid': uid,
        'clid': lh_clid,
        'offers': order_offers
      };
      var xhr = new XMLHttpRequest();
      xhr.open('POST', 'https://track.leadhit.io/stat/lead_order', true);
      xhr.setRequestHeader('Content-type', 'application/json; charset=utf-8');
      xhr.onreadystatechange = function () {
        if (this.readyState != 4) return;
        console.log('order sended');
      };
      xhr.send(JSON.stringify(requestBody));
    }
  })();
        `,
            }}
          />
        </>
      )}
    </>
  )
}

export default ThankYouPage
