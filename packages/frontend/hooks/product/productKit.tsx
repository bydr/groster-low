import { UseProductPropsType, UseProductReturnType } from "./helpers"
import { useCallback, useEffect, useState } from "react"
import { useCart } from "../cart"
import { useMutation } from "react-query"
import { fetchChangeQtyProduct, fetchRemoveProduct } from "../../api/cartAPI"
import { usePromocode } from "../promocode"
import { ApiError } from "../../../contracts/contracts"
import { SpecificationItemType } from "../../types/types"
import { useProduct } from "./product"

export const useProductKit: (props: UseProductPropsType) => Omit<
  UseProductReturnType,
  "addToCart"
> & {
  removeChild: (uuidProduct: string | null) => void
  addChild: (uuidProduct: string | null) => void
  addToCart: () => void
  isKit: boolean
} = ({ product, options }) => {
  const [totalChilds, setTotalChilds] = useState<string[]>([])
  const [removedChilds, setRemovedChilds] = useState<string[]>([])
  const {
    token,
    updateProductPriceUnit,
    updateSpecification,
    refetchCart,
    updateToken,
    updateTotalCost,
  } = useCart()
  const { updateDiscount } = usePromocode({ cart: token })

  const {
    child,
    uuid,
    removeFromCart,
    currentCount,
    setIsFetching,
    setInCart,
    kit,
    addToCart,
    currentSpecification,
    ...productBase
  } = useProduct({
    product,
    options: {
      ...options,
      onAddToCart: {
        onSuccess: (response) => {
          if (uuid === null) {
            return
          }
          if (token === null) {
            updateToken(response.cart)
          }

          if (kit.length !== totalChilds.length) {
            refetchCart()
          } else {
            updateSpecification({
              uuid: uuid,
              quantity: response.addedQty !== 0 ? response.addedQty : undefined,
              isRemoved: false,
            })
          }

          updateTotalCost(response.total_cost)
          updateDiscount(response.discount || 0)
          setIsFetching(false)
        },
        onError: () => {
          setIsFetching(false)
          setInCart(false)
        },
        onMutate: () => {
          setIsFetching(true)
        },
      },
    },
  })

  const getCalcPriceUnit = useCallback(
    (currentSpecification: SpecificationItemType) => {
      let calcPriceUnit = 0
      const keysSpecification = Object.keys(currentSpecification.child || {})
      if (keysSpecification.length > 0) {
        keysSpecification.map((key) => {
          const specificationChild = (currentSpecification.child || {})[key]
          if (!!child) {
            const productChild = child[key]
            if (productChild) {
              if (!specificationChild.isRemoved) {
                calcPriceUnit += productChild.price || 0
              }
            }
          }
        })
      }

      return calcPriceUnit
    },
    [child],
  )

  const { mutate: changeChildQtyMutate } = useMutation(fetchChangeQtyProduct, {
    onSuccess: (response, variables) => {
      if (uuid === null) {
        return
      }
      updateSpecification({
        uuid: variables.product,
        quantity: response.addedQty !== 0 ? response.addedQty : undefined,
        isRemoved: false,
        parent: uuid,
      })
      updateTotalCost(response.total_cost)
      updateDiscount(response.discount || 0)
    },
    onError: (error: ApiError) => {
      console.log("error ", error)
    },
  })

  const { mutate: removeChildMutate } = useMutation(fetchRemoveProduct, {
    onSuccess: (response, variables) => {
      if (uuid === null) {
        return
      }
      updateSpecification({
        uuid: variables.product,
        parent: uuid,
        isRemoved: true,
      })
    },
  })

  const addChild = useCallback(
    (uuidChild: string | null) => {
      console.log("addChild ", uuidChild)
      if (!uuid || !token || currentCount === null || !uuidChild) {
        return
      }
      changeChildQtyMutate({
        product: uuidChild,
        parent: uuid,
        cart: token,
        quantity: currentCount,
      })
    },
    [changeChildQtyMutate, currentCount, token, uuid],
  )

  const removeChild = useCallback(
    (uuidChild: string | null) => {
      console.log("removeChild")
      if (!uuid || !token || !uuidChild) {
        return
      }

      if (totalChilds.length - removedChilds.length <= 1) {
        removeFromCart()
      } else {
        removeChildMutate({
          product: uuidChild,
          parent: uuid,
          cart: token,
        })
      }
    },
    [
      removeFromCart,
      totalChilds.length,
      removeChildMutate,
      removedChilds.length,
      token,
      uuid,
    ],
  )

  // изменить цену за 1 unit товара
  // например для комплекта нужно пересчитать эту цену
  // при удалении дочернего товара
  useEffect(() => {
    if (currentSpecification === null || uuid === null) {
      return
    }

    const calcPriceUnit = getCalcPriceUnit(currentSpecification)
    if (calcPriceUnit > 0) {
      updateProductPriceUnit({ uuid, priceUnit: calcPriceUnit })
    }

    const child = Object.keys(currentSpecification.child || {})
    setTotalChilds(child)
    setRemovedChilds(
      child.filter((k) => !!(currentSpecification.child || {})[k]?.isRemoved),
    )
  }, [getCalcPriceUnit, updateProductPriceUnit, uuid, currentSpecification])

  return {
    uuid,
    child,
    removeFromCart,
    currentCount,
    addToCart,
    setIsFetching,
    setInCart,
    kit,
    currentSpecification,
    ...productBase,
    removeChild: removeChild,
    addChild: addChild,
  }
}
