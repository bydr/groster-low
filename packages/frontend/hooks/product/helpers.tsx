import {
  ActionMapType,
  AvailableStatusType,
  ChangeQtyProductResponse,
  CurrencyType,
  DetailProductType,
  ProductType,
  ProductWithChildType,
  PropertiesType,
  RadioGroupItemsType,
  SpecificationItemType,
  StoreProductType,
} from "../../types/types"
import { UseShippingsReturnType } from "../shippings"
import Cookies from "universal-cookie"
import { ApiError, ChangeQtyRequest } from "../../../contracts/contracts"
import { ROUTES } from "../../utils/constants"

export const _unitsSortAsc = (
  units: RadioGroupItemsType[],
): RadioGroupItemsType[] =>
  [...units].sort((a, b) => {
    const valA = a.value || 0
    const valB = b.value || 0
    return valA > valB ? 1 : valA < valB ? -1 : 0
  })

export const _unitsSortDesc = (
  units: RadioGroupItemsType[],
): RadioGroupItemsType[] =>
  [...units].sort((a, b) => {
    const valA = a.value || 0
    const valB = b.value || 0
    return valA > valB ? -1 : valA < valB ? 1 : 0
  })

export type CleanupType = () => void

export const initialState = {
  product: null as ProductWithChildType | null,
  isInit: false as boolean,
  unit: null as number | null,
  count: null as number | null,
  counter: null as number | null,
  sampleCount: 0 as number,
  units: null as RadioGroupItemsType[] | null,
  inCart: false as boolean,
  currentSpecification: null as SpecificationItemType | null,
  totalQty: 0 as number,
  isRemoved: false as boolean,
  isAnimate: null as boolean | null,
  isCountError: false as boolean,
  containersIsOnly: false as boolean,
  unitMeasure: null as string | null,
  isVariative: false as boolean,
  path: null as string | null,
  uuid: null as string | null,
  images: [] as string[],
}

export type ProductStateType = typeof initialState

export type AddToCartOptionsType = {
  onSuccess?: (
    response: ChangeQtyProductResponse,
    variables: ChangeQtyRequest,
  ) => void
  onError?: (error: ApiError | unknown) => void
  onMutate?: () => void
}
export type UseProductPropsType = {
  product: ProductWithChildType
  options?: {
    parentKit?: string
    isSaveOnRemove?: boolean
    onAddToCart?: AddToCartOptionsType
    initQty?: number
    withLocation?: boolean
    isDetail?: boolean
  }
}
export type UseProductReturnType = {
  name: string | null
  isShowedDetail: boolean
  isFavorites: boolean
  availableStatus: AvailableStatusType | null
  units: RadioGroupItemsType[]
  totalQty: number
  currency: CurrencyType
  currentCount: number | null
  counter: number | null
  currentUnit: number | null
  priceUnit: number
  priceCalculate: number
  isFetching: boolean
  isFetchingFavorites: boolean
  inCart: boolean
  isRemoved: boolean
  isAvailable: boolean
  child: Record<string, ProductType> | null
  toggleFavorite: () => void
  properties: PropertiesType
  kit: string[]
  kitParents: string[]
  stores: StoreProductType[]
  storesAvailable: StoreProductType[]
  storesQty: number
  categories: string[]
  description: string | null
  fastQty: number
  images: string[]
  isAnimate: boolean | null
  isKit: boolean
  isBestseller: boolean
  isNew: boolean
  setIsFetching: (val: boolean) => void
  setInCart: (val: boolean) => void
  setIsShowedDetail: (isShow: boolean) => void
  updateCurrentCount: (value: number) => void
  updateCurrentUnit: (value: number) => void
  addToCart: () => void
  removeFromCart: () => void
  isCopiedPath: boolean
  copyPath: () => void
  isCopiedCode: boolean
  code?: string
  copyCode: () => void
  share: () => void
  isCountError: boolean
  setIsCountError: (isError: boolean) => void
  isInit: boolean
  isAdded: boolean
  setIsAdded: (value: boolean) => void
  cleanup: CleanupType
  currentSpecification: SpecificationItemType | null
  isVariative: boolean
  isAllowSample: boolean
} & Pick<
  UseShippingsReturnType,
  "shippingDate" | "isDateToOnlyCompany" | "isExpressShipping"
> &
  Pick<DetailProductType, "variation"> &
  Pick<ProductStateType, "uuid" | "path" | "unitMeasure">

export type UseProductType = (
  props: UseProductPropsType,
) => UseProductReturnType

export const cookies = new Cookies()

export const DEFAULT_QTY = 1

export const DEFAULT_UNIT_MEASURE = "шт"

export const getCurrentSpecification = ({
  specification,
  uuid,
  parent,
}: {
  specification: Record<string, SpecificationItemType> | null
  parent?: string
  uuid: string
}): SpecificationItemType | null => {
  if (!specification) {
    return null
  }

  if (!!parent) {
    return (specification[parent].child || {})[uuid || ""] || {}
  } else {
    return specification[uuid] || {}
  }
}

export type GetUnitsType = ({
  units,
  isOnlyContainers,
  unitMeasure,
  parentKit,
}: {
  units: { name?: string; value?: number }[] | null
  isOnlyContainers: boolean
  unitMeasure: string | undefined | null
  parentKit?: string
}) => { units: null | RadioGroupItemsType[] }

export const getUnits: GetUnitsType = ({
  unitMeasure,
  units,
  isOnlyContainers,
  parentKit,
}) => {
  let u: RadioGroupItemsType[] = []
  if (units === null) {
    return { units: null }
  }

  if (!parentKit) {
    u = [
      ...u,
      ...units.map((u) => ({
        value: u.value || "",
        message: `${u.value} в ${u.name}`,
      })),
    ]
  }

  if (!isOnlyContainers || !!parentKit) {
    u = [
      ...u,
      {
        value: DEFAULT_QTY,
        message: `${DEFAULT_QTY} ${unitMeasure || "шт"}`,
      },
    ]
  }

  return { units: _unitsSortAsc(u) }
}

export enum ProductActionTypes {
  setProduct = "SET_PRODUCT",
  setCount = "SET_COUNT",
  setCounter = "SET_COUNTER",
  setUnit = "SET_UNIT",
  setIsInit = "SET_IS_INIT",
  setUnits = "SET_INITS",
  setInCart = "SET_IN_CART",
  setCurrentSpecification = "SET_CURRENT_SPECIFICATION",
  setTotalQty = "SET_TOTAL_QTY",
  setIsRemoved = "SET_IS_REMOVED",
  setIsAnimate = "SET_IS_ANIMATE",
  setIsCountError = "SET_IS_COUNT_ERROR",
  reset = "RESET",
  resetOnRoute = "RESET_ON_ROUTE",
}

export type ProductPayload = {
  [ProductActionTypes.setProduct]: {
    product: ProductWithChildType | null
    parentKit?: string
  }
  [ProductActionTypes.setCount]: {
    count: number | null
    witError?: boolean
    isLog?: boolean
    parentKit?: string
  }
  [ProductActionTypes.setCounter]: {
    counter: number | null
  }
  [ProductActionTypes.setIsInit]: boolean
  [ProductActionTypes.setUnits]: {
    only_containers: boolean
    units: { name?: string; value?: number }[] | null
    unit: string | null
  }
  [ProductActionTypes.setUnit]: {
    unit: null | number
  }
  [ProductActionTypes.setInCart]: boolean
  [ProductActionTypes.setCurrentSpecification]: {
    specification: Record<string, SpecificationItemType> | null
    uuid: string | null
    parentKit?: string
    isLog?: boolean
  } | null
  [ProductActionTypes.setTotalQty]: { total: number; sample?: number }
  [ProductActionTypes.setIsRemoved]: boolean
  [ProductActionTypes.setIsAnimate]: boolean
  [ProductActionTypes.setIsCountError]: boolean
  [ProductActionTypes.reset]: undefined
  [ProductActionTypes.resetOnRoute]: undefined
}

export type ProductActions =
  ActionMapType<ProductPayload>[keyof ActionMapType<ProductPayload>]

export const reducer = (
  state: ProductStateType,
  action: ProductActions,
): ProductStateType => {
  switch (action.type) {
    case ProductActionTypes.setProduct: {
      const product = action.payload.product
      if (product === null) {
        return { ...initialState }
      }
      const unitMeasure = product.unit || null
      const containers = product.containers
      const variation = product.variation
      return {
        ...initialState,
        product: action.payload.product,
        unitMeasure: unitMeasure,
        units: getUnits({
          units: containers?.units || [],
          isOnlyContainers: !!containers?.only_containers,
          unitMeasure: unitMeasure,
          parentKit: action.payload.parentKit,
        }).units,
        isVariative:
          !!variation?.selected?.uuid &&
          !!variation?.model &&
          variation?.model.length > 0,
        path:
          product.alias === undefined
            ? null
            : `${ROUTES.product}/${product.alias}`,
        uuid: product.uuid || null,
        images: (product.images || []).reduce((uniq: string[], item) => {
          return uniq.includes(item) ? uniq : [...uniq, item]
        }, []),
        totalQty: product.total_qty || 0,
      }
    }
    case ProductActionTypes.setCount: {
      const isLog = !!action.payload.isLog
      let currentUnit = state.unit
      const totalQty = state.totalQty

      if (isLog) {
        console.log("[setCount] currentUnit ", currentUnit)
      }

      const _validate = (q: number | null): number | null => {
        const units = state.units
        const isInit = state.isInit
        const withError = !!action.payload.witError
        let count: number | null = q

        if (units === null) {
          return null
        }

        if (count === null) {
          return currentUnit
        }

        const unitOne = units.find((u) => +u.value === DEFAULT_QTY)?.value

        if (currentUnit === null || count % currentUnit !== 0) {
          const rightUnit = (state.units || []).find(
            (u) => (count || 0) % +u.value === 0,
          )
          const val =
            rightUnit === undefined ? +units[0].value : +rightUnit.value

          if (isLog) {
            console.log("[setCount] [currentUnit is null] units ", units)
            console.log("[setCount] [currentUnit is null] isInit ", isInit)
            console.log("[setCount] [currentUnit is null] rightUnit ", val)
          }

          currentUnit = val
        }

        if (isLog) {
          console.log(
            "[updateCounter _validate] count ",
            count,
            " isInit ",
            isInit,
          )
        }

        const isMoreTotal = count > totalQty
        const isLessUnit = count < currentUnit

        if (isMoreTotal || isLessUnit) {
          if (isMoreTotal) {
            if (isInit && withError) {
              if (isLog) {
                console.log("[updateCounter _validate] isMoreTotal")
              }
              state.isCountError = true
            }
            count = _validate(totalQty)
          }
          if (isLessUnit) {
            if (currentUnit > totalQty) {
              if (isLog) {
                console.log("[updateCounter _validate] isLessUnit")
              }

              //sort desc
              const uVal = _unitsSortDesc(units).find(
                (u) => (count || 0) >= +u.value,
              )?.value

              if (uVal !== undefined) {
                if (isLog) {
                  console.log(
                    "[updateCounter _validate] isLessUnit updateUnit ",
                    +uVal,
                  )
                }

                // такой себе forceUpdate
                // ставим -1, тк при ситуации когда unit был 1
                // и задиспатчится здесь также 1 (начальное значение)
                // переключатель не среагирует, тк не было изменений
                // в таком случае он переключится на начальное значение, тк -1 это новое значение
                currentUnit = +uVal === 1 ? -1 : +uVal
              } else {
                // когда введенное значение меньше всех юнитов в которых исчисляется товар
                // т.е. в наличии кол-во товара меньше чем юниты в которых он исчисляется
                // оставляем максимально допустимое значение
                count = totalQty
              }
            } else {
              if (unitOne !== undefined && withError) {
                if (isLog) {
                  console.log("[updateCounter _validate] unitOne ", +unitOne)
                }
                currentUnit = +unitOne
                count = +unitOne
              } else {
                count = currentUnit
              }
            }
          }
        } else {
          if (count % currentUnit !== 0) {
            if (unitOne !== undefined) {
              currentUnit = +unitOne
            } else {
              let variant = currentUnit * Math.ceil(count / currentUnit)

              if (variant > totalQty) {
                if (isInit && withError) {
                  state.isCountError = true
                }
                variant = currentUnit * Math.floor(count / currentUnit)
              }
              count = variant
            }
          } else {
            count = currentUnit * Math.floor(count / currentUnit)
          }
        }

        return count === null ? currentUnit : count
      }

      const validated = _validate(action.payload.count)

      if (isLog) {
        console.log("[setCount] return ", {
          ...state,
          unit: currentUnit,
          count: validated,
        })
      }

      return {
        ...state,
        count: validated,
        unit: currentUnit,
      }
    }
    case ProductActionTypes.setCounter: {
      return {
        ...state,
        counter: action.payload.counter,
      }
    }
    case ProductActionTypes.setUnit: {
      return {
        ...state,
        unit: action.payload.unit,
      }
    }
    case ProductActionTypes.setIsInit: {
      return {
        ...state,
        isInit: action.payload,
      }
    }
    case ProductActionTypes.setInCart: {
      return {
        ...state,
        inCart: action.payload,
      }
    }
    case ProductActionTypes.setCurrentSpecification: {
      let spec = null

      if (
        action.payload !== null &&
        action.payload.specification !== null &&
        action.payload.uuid !== null
      ) {
        spec = getCurrentSpecification({
          specification: action.payload.specification,
          uuid: action.payload.uuid,
          parent: action.payload.parentKit,
        })
      }

      const qty = spec?.quantity !== undefined ? spec?.quantity || 0 : null

      return {
        ...state,
        currentSpecification: spec,
        sampleCount: !!spec?.isSampleRemoved ? 0 : spec?.sample || 0,
        isRemoved: !!spec?.isRemoved,
        isAnimate: spec?.isAnimate === undefined ? null : !!spec?.isAnimate,
        inCart: qty !== null && qty > 0,
      }
    }
    case ProductActionTypes.setTotalQty: {
      return {
        ...state,
        totalQty:
          action.payload.total -
          (action.payload?.sample || state.sampleCount || 0),
      }
    }
    case ProductActionTypes.setIsCountError: {
      return {
        ...state,
        isCountError: action.payload,
      }
    }
    case ProductActionTypes.reset: {
      return {
        ...initialState,
      }
    }
    case ProductActionTypes.resetOnRoute: {
      return {
        ...state,
        counter: null,
        count: null,
        currentSpecification: null,
        units: null,
        unit: null,
      }
    }
    default: {
      return state
    }
  }
}
