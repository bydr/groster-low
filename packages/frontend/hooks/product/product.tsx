import {
  useCallback,
  useEffect,
  useMemo,
  useReducer,
  useRef,
  useState,
} from "react"
import { useFavorites } from "../favorites"
import { ProductWithChildType, StoreProductType } from "../../types/types"
import { useCart } from "../cart"
import { useMutation } from "react-query"
import { fetchChangeQtyProduct, fetchRemoveProduct } from "../../api/cartAPI"
import { usePromocode } from "../promocode"
import { COOKIE_HOST_NAME, CURRENCY } from "../../utils/constants"
import { useClipboardCopy } from "../clipboardCopy"
import {
  getAvailableStatus,
  getTranslationProperties,
} from "../../utils/helpers"
import { useApp } from "../app"
import { useShippings } from "../shippings"
import {
  CleanupType,
  cookies,
  DEFAULT_QTY,
  initialState,
  ProductActionTypes,
  reducer,
  UseProductReturnType,
  UseProductType,
} from "./helpers"
import { usePrevious } from "../previous"
import { useRouter } from "next/router"

export const useProduct: UseProductType = ({
  product: productInput,
  options: {
    parentKit,
    isSaveOnRemove = true,
    onAddToCart,
    initQty,
    withLocation,
  } = {},
}) => {
  const [isFetching, setIsFetching] = useState<boolean>(false)
  const initialCountRef = useRef<number | null>(initQty || null)
  const isLog = useRef<boolean>(false)

  const router = useRouter()

  const [
    {
      product,
      count,
      counter,
      unit,
      isInit,
      units,
      inCart,
      currentSpecification,
      totalQty,
      isRemoved,
      isAnimate,
      isCountError,
      isVariative,
      path,
      uuid,
      images,
      unitMeasure,
    },
    dispatch,
  ] = useReducer(reducer, {
    ...initialState,
    count: initialCountRef.current,
    counter: initialCountRef.current,
  })

  const {
    isFavorites,
    isFetching: isFetchingFavorites,
    add: addToFavorites,
    remove: removeFromFavorites,
  } = useFavorites(uuid || undefined)

  const {
    token,
    updateToken,
    updateSpecification,
    specification,
    updateTotalCost,
    removeSpecification,
    addProductInFetching,
    removeProductInFetching,
  } = useCart()

  const { settings } = useApp()
  const { minManyQuantity } = settings || {}
  const { isCopied: isCopiedPath, handleCopyClick: handleCopyPath } =
    useClipboardCopy()
  const { isCopied: isCopiedCode, handleCopyClick: handleCopyCode } =
    useClipboardCopy()
  const { updateDiscount } = usePromocode({ cart: token })

  const [isShowedDetail, setIsShowedDetail] = useState(false)

  const [isAdded, setIsAdded] = useState(false)

  const latestUnitRef = useRef<number | null>(unit)
  const isCalledCountChangedRef = useRef<boolean>(false)
  const latestIsInit = useRef(false)
  const latestCountInCart = useRef<number | null>(null)
  const latestCount = useRef<number | null>(null)

  const priceUnit = useMemo<number>(() => product?.price || 0, [product?.price])
  const priceCalc = useMemo<number>(
    () => priceUnit * (count || 1),
    [count, priceUnit],
  )

  const translationProperties = useMemo(() => {
    return getTranslationProperties(product?.properties || [])
  }, [product?.properties])

  const { getShippingsData, ...shippingRest } = useShippings({
    product: isInit
      ? {
          fastQty: product?.fast_qty || 0,
          currentCount: count || DEFAULT_QTY,
          totalQty: totalQty,
        }
      : undefined,
    withLocation: withLocation,
  })

  const [stores, setStores] = useState<StoreProductType[]>([])
  const [availableStores, setAvailableStores] = useState<StoreProductType[]>([])

  const copyPath = useCallback(() => {
    if (path === null) {
      return
    }
    const cHost = cookies.get(COOKIE_HOST_NAME)
    if (!!cHost) {
      handleCopyPath(`https://${cHost}${path}`)
    }
  }, [handleCopyPath, path])

  const copyCode = useCallback(() => {
    if (product?.code === undefined) {
      return
    }
    handleCopyCode(product.code)
  }, [handleCopyCode, product?.code])

  const share = useCallback(() => {
    if (!navigator || typeof navigator === undefined) {
      return
    }
    navigator
      .share({
        title: product?.name,
        url: path || undefined,
        text: product?.description,
      })
      .catch((err) => {
        console.log("Shared error: ", err)
      })
  }, [path, product?.description, product?.name])

  const toggleFavorite = useCallback(() => {
    if (uuid === null) {
      return
    }
    if (isFavorites) {
      removeFromFavorites(uuid)
    } else {
      addToFavorites([uuid])
    }
  }, [addToFavorites, isFavorites, removeFromFavorites, uuid])

  const { mutate: changeQtyMutate } = useMutation(fetchChangeQtyProduct, {
    onSuccess: (response, variables) => {
      if (uuid === null) {
        return
      }
      if (token === null) {
        updateToken(response.cart)
      }
      const productTotalQty = product?.total_qty || 0

      // если на момент добавления в корзину изменилось наличие на складах
      // обновляем в стейте
      if (
        variables.quantity > response.addedQty &&
        variables.quantity <= productTotalQty
      ) {
        dispatch({
          type: ProductActionTypes.setTotalQty,
          payload: {
            total: response.addedQty,
          },
        })
        setCurrentUnit(response.addedQty)
      } else {
        if (totalQty !== productTotalQty) {
          dispatch({
            type: ProductActionTypes.setTotalQty,
            payload: {
              total: productTotalQty,
            },
          })
        }
      }

      if (
        (!inCart || isRemoved) &&
        ((product?.kit || []).length > 0 ||
          (product?.kit_parents || []).length > 0)
      ) {
        setIsAdded(true)
      }

      updateSpecification({
        uuid: uuid,
        quantity: response.addedQty !== 0 ? response.addedQty : undefined,
        isRemoved: false,
        kit: product?.kit || [],
      })

      // логика для товара, который является частью комплекта
      // если добавили такой товар, но не передали его parent
      // значит в корзине товар должен добавиться как самостоятельный отдельный item
      if (variables.parent === undefined && !!parentKit) {
        updateSpecification({
          uuid: uuid,
          isAnimate: true,
        })

        const { current: countInCart } = latestCountInCart
        if (countInCart !== null) {
          dispatch({
            type: ProductActionTypes.setCount,
            payload: {
              count: countInCart,
              parentKit: parentKit,
            },
          })
          dispatch({
            type: ProductActionTypes.setCounter,
            payload: {
              counter: countInCart,
            },
          })
        }
      }
      setIsFetching(false)
      updateTotalCost(response.total_cost)
      updateDiscount(response.discount || 0)
      removeProductInFetching(uuid || "")
    },
    onError: () => {
      setIsFetching(false)
      removeProductInFetching(uuid || "")
    },
    onMutate: () => {
      setIsFetching(true)
      addProductInFetching(uuid || "")
    },
  })
  const { mutate: removeProductMutate } = useMutation(fetchRemoveProduct, {
    onSuccess: (response) => {
      if (uuid === null) {
        return
      }
      dispatch({
        type: ProductActionTypes.setIsRemoved,
        payload: true,
      })
      setIsAdded(false)

      if (isSaveOnRemove) {
        updateSpecification({
          uuid: uuid,
          isRemoved: true,
          parent: parentKit,
        })
      } else {
        if (uuid !== undefined) {
          removeSpecification(uuid)
        }
      }

      setIsFetching(false)
      updateTotalCost(response.total_cost)
      updateDiscount(response.discount || 0)
      removeProductInFetching(uuid || "")
    },
    onError: () => {
      dispatch({
        type: ProductActionTypes.setIsRemoved,
        payload: false,
      })
      dispatch({
        type: ProductActionTypes.setCount,
        payload: {
          count: unit,
          parentKit,
        },
      })
      removeProductInFetching(uuid || "")
      setIsFetching(false)
    },
    onMutate: () => {
      setIsFetching(true)
      addProductInFetching(uuid || "")
    },
  })

  const addToCart = useCallback(() => {
    const { current: countInCart } = latestCountInCart
    const { current: count } = latestCount

    if (!uuid || count === null || isFetching || !totalQty) {
      return
    }

    if (isLog.current) {
      console.log("[addToCart]")
    }

    let quantityCalc = count
    if (parentKit !== undefined) {
      if (isLog.current) {
        console.log("[addToCart] parentKit ", parentKit)
      }
      if (countInCart !== null && quantityCalc > countInCart) {
        quantityCalc = count - countInCart
      }
    }

    if (isLog.current) {
      console.log(
        "[addToCart] quantityCalc ",
        quantityCalc,
        " count ",
        count,
        " countInCart ",
        countInCart,
      )
    }

    if (quantityCalc !== countInCart || count !== countInCart || isRemoved) {
      const qty = quantityCalc > 0 ? quantityCalc : count

      if (qty > totalQty) {
        dispatch({
          type: ProductActionTypes.setIsCountError,
          payload: true,
        })
        return
      }

      changeQtyMutate(
        {
          product: uuid,
          quantity: qty,
          cart: token || undefined,
        },
        onAddToCart,
      )
    }
  }, [uuid, parentKit, isRemoved, totalQty, changeQtyMutate, token, isFetching])

  const removeFromCart = useCallback(() => {
    if (!uuid || !token) {
      return
    }
    removeProductMutate({
      cart: token,
      product: uuid,
      parent: parentKit,
    })
  }, [parentKit, token, uuid, removeProductMutate])

  const setCurrentCount = useCallback(
    (value: number, withError?: boolean) => {
      if (isLog.current) {
        console.log("[setCurrentCount] value ", value)
      }
      let _withError = withError

      const { current: latestUnit } = latestUnitRef
      const { current: isCalled } = isCalledCountChangedRef
      if (!isCalled) {
        if (
          latestUnit !== null &&
          latestUnit > totalQty &&
          value === latestUnit
        ) {
          isCalledCountChangedRef.current = true
          _withError = false
        }
      }

      dispatch({
        type: ProductActionTypes.setCounter,
        payload: {
          counter: value,
        },
      })

      if (inCart) {
        if (value < 1) {
          removeFromCart()
          return
        }
      }

      dispatch({
        type: ProductActionTypes.setCount,
        payload: {
          count: value,
          isLog: isLog.current,
          witError: _withError,
          parentKit,
        },
      })
    },
    [inCart, removeFromCart, totalQty, parentKit],
  )

  const setCurrentUnit = useCallback(
    (value: number) => {
      if (isLog.current) {
        console.log("[setCurrentUnit] value ", value)
      }

      dispatch({
        type: ProductActionTypes.setUnit,
        payload: {
          unit: value,
        },
      })

      const { current: unit } = latestUnitRef
      const { current: initialCount } = initialCountRef
      const valueCount = initialCount || value
      if (valueCount === unit || unit === -1) {
        return
      }
      setCurrentCount(valueCount, true)
    },
    [setCurrentCount],
  )

  const cleanupHandle: CleanupType = useCallback(() => {
    if (isLog.current) {
      console.log("[cleanup]")
    }
    latestUnitRef.current = null
    latestIsInit.current = false
    dispatch({
      type: ProductActionTypes.reset,
    })
  }, [])

  const initialCartHandle = useCallback(
    ({ qty }: { qty: number }) => {
      if (isLog.current) {
        console.log("[initialCartHandle] qty ", qty)
      }
      latestCountInCart.current = qty
      setCurrentCount(qty)
    },
    [setCurrentCount],
  )

  const initialDefaultHandle = useCallback(() => {
    if (isLog.current) {
      console.log("[initialDefaultHandle] ", units)
    }
    if (units === null) {
      return
    }
    latestCountInCart.current = null
    setCurrentUnit(+(units[0]?.value || DEFAULT_QTY))
  }, [units, setCurrentUnit])

  const initialHandle = useCallback(() => {
    // не была получена корзина
    if (currentSpecification === null) {
      return
    }

    if (isLog.current) {
      console.log("[initialHandle] currentSpecification ", currentSpecification)
    }

    // если quantity undefined - это значит добавлен образец, но не товар
    if (
      currentSpecification?.quantity !== undefined &&
      currentSpecification.quantity > 0 &&
      !currentSpecification.isRemoved
    ) {
      initialCartHandle({
        qty: currentSpecification.quantity,
      })
    } else {
      initialDefaultHandle()
    }

    latestIsInit.current = true
    dispatch({
      type: ProductActionTypes.setIsInit,
      payload: true,
    })
  }, [currentSpecification, initialCartHandle, initialDefaultHandle])

  const prevProduct = usePrevious<ProductWithChildType>(productInput)

  const [isTriggerAddToCart, setIsTriggerAddToCart] = useState(false)

  useEffect(() => {
    latestCount.current = count
    setIsTriggerAddToCart(true)
  }, [count])

  useEffect(() => {
    if (isLog.current) {
      console.log("[effect] unit changed ", unit)
    }
    latestUnitRef.current = unit
  }, [unit])

  useEffect(() => {
    if (prevProduct?.uuid === productInput?.uuid) {
      return
    }
    if (isLog.current) {
      console.log("productInput ", productInput)
    }
    dispatch({
      type: ProductActionTypes.setProduct,
      payload: {
        product: {
          ...productInput,
        },
        parentKit: parentKit,
      },
    })
  }, [prevProduct?.uuid, productInput, parentKit])

  useEffect(() => {
    dispatch({
      type: ProductActionTypes.setCurrentSpecification,
      payload: {
        uuid: uuid || null,
        specification,
        parentKit,
        isLog: isLog.current,
      },
    })
  }, [specification, uuid, parentKit])

  useEffect(() => {
    dispatch({
      type: ProductActionTypes.setTotalQty,
      payload: {
        sample: !!currentSpecification?.isSampleRemoved
          ? 0
          : currentSpecification?.sample || 0,
        total: product?.total_qty || 0,
      },
    })
  }, [
    currentSpecification?.isSampleRemoved,
    currentSpecification?.sample,
    product?.total_qty,
    uuid,
  ])

  useEffect(() => {
    initialHandle()
  }, [initialHandle, uuid])

  useEffect(() => {
    if (isLog.current) {
      console.log("[effect] count changed ", count)
    }
  }, [count])

  useEffect(() => {
    if (!isInit || !isTriggerAddToCart || isFetching) {
      return
    }
    if (isLog.current) {
      console.log(
        "!isInit || !isTriggerAddToCart || isFetching ",
        !isInit,
        " ",
        !isTriggerAddToCart,
        " ",
        isFetching,
      )
    }
    if (inCart && !isRemoved) {
      setIsTriggerAddToCart(false)
      addToCart()
    }
  }, [addToCart, inCart, isInit, isRemoved, isFetching, isTriggerAddToCart])

  useEffect(() => {
    if (uuid === null) {
      return
    }
    if (isAnimate) {
      setTimeout(() => {
        updateSpecification({
          uuid: uuid,
          isAnimate: false,
        })
        dispatch({
          type: ProductActionTypes.setIsAnimate,
          payload: false,
        })
      }, 3000)
    }
  }, [isAnimate, updateSpecification, uuid])

  useEffect(() => {
    if (isCountError) {
      setTimeout(() => {
        dispatch({
          type: ProductActionTypes.setIsCountError,
          payload: false,
        })
      }, 3000)
    }
  }, [isCountError])

  //formatted stores comparing with qty product
  useEffect(() => {
    if (product?.stores !== undefined && product.stores.length > 0) {
      const sortedStores = [...product.stores]
        .sort(function (x, y) {
          return x.is_main === y.is_main ? 0 : x.is_main ? -1 : 1
        })
        .map((s) => {
          const { shippingDate } = getShippingsData({
            shippingFastTime: settings?.shippingFastTime,
            shippingShift: settings?.shippingShift,
            fastQty: s.is_main ? s.quantity : undefined,
            totalQty: s.quantity,
          })
          return {
            uuid: s.uuid,
            quantity: s.quantity,
            name: s.name,
            isMain: s.is_main,
            availableStatus: getAvailableStatus(
              s.quantity || 0,
              minManyQuantity || 0,
            ),
            deliveryDate: shippingDate || undefined,
          } as StoreProductType
        })
      setStores(sortedStores)
      setAvailableStores(sortedStores.filter((store) => !!store.quantity))
    } else {
      setStores([])
      setAvailableStores([])
    }
  }, [
    getShippingsData,
    minManyQuantity,
    settings?.shippingFastTime,
    settings?.shippingShift,
    product?.stores,
    uuid,
  ])

  useEffect(() => {
    if (isLog.current) {
      console.log("totalQty ", totalQty)
    }
  }, [totalQty])

  useEffect(() => {
    const disabledShowedDialogs = () => {
      setIsAdded(false)
    }
    router.events.on("routeChangeStart", disabledShowedDialogs)
    router.events.on("routeChangeComplete", disabledShowedDialogs)

    return () => {
      router.events.off("routeChangeStart", disabledShowedDialogs)
      router.events.off("routeChangeComplete", disabledShowedDialogs)
    }
  }, [router.events])

  return {
    uuid: uuid || "",
    isShowedDetail,
    isFavorites,
    setIsShowedDetail,
    availableStatus: getAvailableStatus(
      product?.total_qty || 0,
      minManyQuantity || 0,
    ),
    toggleFavorite,
    path: path,
    units: units || [],
    updateCurrentCount: useCallback(
      (value) => {
        const { current: isInit } = latestIsInit
        if (!isInit) {
          return
        }
        if (isLog.current) {
          console.log("updateCurrentCount ext value ", value)
        }
        setCurrentCount(value, true)
      },
      [setCurrentCount],
    ),
    updateCurrentUnit: useCallback(
      (value) => {
        const { current: isInit } = latestIsInit
        if (!isInit) {
          return
        }
        if (isLog.current) {
          console.log("updateCurrentUnit ext value ", value)
        }
        setCurrentUnit(value)
      },
      [setCurrentUnit],
    ),
    totalQty: totalQty,
    priceUnit: priceUnit,
    priceCalculate: priceCalc,
    currentUnit: unit,
    currentCount: count,
    unitMeasure: unitMeasure,
    currency: CURRENCY,
    isFetching: isFetching,
    isFetchingFavorites: isFetchingFavorites,
    inCart: inCart,
    addToCart,
    removeFromCart,
    isRemoved,
    properties: translationProperties,
    child: product?.child,
    setInCart: (value) => {
      dispatch({
        type: ProductActionTypes.setInCart,
        payload: value,
      })
    },
    setIsFetching: setIsFetching,
    kit: product?.kit || [],
    isAnimate,
    storesAvailable: availableStores,
    stores: stores,
    storesQty: availableStores.length,
    categories: product?.categories || [],
    isCopiedPath: isCopiedPath,
    copyPath: copyPath,
    share: share,
    description: product?.description || null,
    fastQty: product?.fast_qty || 0,
    name: product?.name || null,
    images: images,
    isKit: (product?.kit || []).length > 0,
    kitParents: product?.kit_parents || [],
    isBestseller: !!product?.is_bestseller,
    isNew: !!product?.is_new,
    counter: counter,
    code: product?.code,
    isCopiedCode: isCopiedCode,
    copyCode: copyCode,
    isCountError,
    setIsCountError: (value) => {
      dispatch({
        type: ProductActionTypes.setIsCountError,
        payload: value,
      })
    },
    isInit: isInit && uuid !== null,
    isAdded,
    setIsAdded,
    isAvailable: !!productInput?.total_qty,
    ...shippingRest,
    cleanup: cleanupHandle,
    currentSpecification: currentSpecification,
    variation: product?.variation,
    isVariative: isVariative,
    isAllowSample: productInput.allow_sample,
  } as const as UseProductReturnType
}
