import {
  createContext,
  FC,
  RefObject,
  useCallback,
  useContext,
  useEffect,
  useMemo,
  useRef,
  useState,
} from "react"
import { accountSlice } from "../store/reducers/accountSlice"
import { useAppDispatch, useAppSelector } from "./redux"
import { NotificationType } from "../components/Notification/Notification"
import { cartSlice } from "../store/reducers/cartSlice"
import { useAddresses } from "./addresses"
import { usePayers } from "./payers"
import {
  getExpireOneYear,
  scrollBodyEnable,
  setCheckoutToStorage,
  setLeadHitStockId,
} from "../utils/helpers"
import { appSlice } from "../store/reducers/appSlice"
import Cookies from "universal-cookie"
import { GetServerSidePropsContext } from "next"
import { parseCookies } from "universal-cookie/lib/utils"
import { ServerResponse } from "http"
import { useMutation } from "react-query"
import { fetchCustomerData, fetchLastOrder } from "../api/checkoutAPI"
import { ResponseCustomerDataList } from "../../contracts/contracts"
import { LastOrderReturnType } from "../types/types"

export type User = {
  accessToken: string | null
  refreshToken: string | null
  email?: string | null
  phone?: string | null
  cart: string | null
  fio?: string | null
  isAdmin?: boolean
}

type ContextType = {
  login: (user: User, notification?: NotificationType) => void
  logout: () => void
  user: Required<User> | null
  isAuth: boolean
  isInit: boolean
  notification: NotificationType | null
  setNotification: (notification: NotificationType | null) => void
  authModalRef: RefObject<HTMLButtonElement>
  showModalAuth: () => void
  updateUser: (user: User | null) => void
  customer: null | ResponseCustomerDataList
  lastOrder: null | LastOrderReturnType
  isFetching: boolean
  isLoadingLastOrder: boolean
}

const cookies = new Cookies()

const COOKIE_USER_KEY = "user"

export const getServerUser = ({
  req,
}: Pick<GetServerSidePropsContext, "req">): User | null => {
  const cookie = parseCookies(req.headers.cookie)
  const user = cookie[COOKIE_USER_KEY]
  return !!user ? (typeof user === "string" ? JSON.parse(user) : user) : null
}

export const getClientUser = (): User | null => {
  const user = cookies.get(COOKIE_USER_KEY)
  return user || null
}

export const setUser = (user: User | null, res?: ServerResponse): void => {
  if (user === null) {
    if (res !== undefined) {
      res.setHeader("Set-Cookie", `${COOKIE_USER_KEY}=null; Path=/; Max-Age=0`)
    } else {
      cookies.remove(COOKIE_USER_KEY, { path: "/" })
    }
  } else {
    if (res !== undefined) {
      res.setHeader(
        "Set-Cookie",
        `${COOKIE_USER_KEY}=${encodeURIComponent(
          JSON.stringify(user),
        )}; Path=/; Expires=${getExpireOneYear().toUTCString()}`,
      )
    } else {
      cookies.set(COOKIE_USER_KEY, JSON.stringify(user), {
        path: "/",
        expires: getExpireOneYear(),
      })
    }
  }
}

const UserContext = createContext<null | ContextType>(null)

type LoginHandleType = (
  user: User,
  notification?: NotificationType,
  withReload?: boolean,
) => void

export const Provider: FC = ({ children }) => {
  const user = useAppSelector((state) => state.profile.user)
  const isAuth = useAppSelector((state) => state.profile.isAuth)
  const isInit = useAppSelector((state) => state.profile.isInit)
  const customer = useAppSelector((state) => state.profile.customer)
  const lastOrder = useAppSelector((state) => state.profile.lastOrder)
  const businessAreas = useAppSelector((state) => state.catalog.businessAreas)
  const dispatch = useAppDispatch()
  const {
    setUserProfile,
    setIsAuth,
    setIsInit,
    clearFavorites,
    setCustomer,
    setLastOrder,
  } = accountSlice.actions
  const { clearCart } = cartSlice.actions
  const [notificationContent, setNotificationContent] =
    useState<NotificationType | null>(null)
  const authModalRef = useRef<HTMLButtonElement>(null)

  const [isFetching, setIsFetching] = useState<boolean>(false)

  const { clear: clearAddresses } = useAddresses()
  const { clear: clearPayers } = usePayers()

  const { setModals } = appSlice.actions

  const { mutate: customerDataMutate, isLoading: isLoadingCustomerData } =
    useMutation(fetchCustomerData, {
      onSuccess: (response) => {
        dispatch(setCustomer(response || null))
      },
    })

  const { mutate: lastOrderDataMutate, isLoading: isLoadingLastOrder } =
    useMutation(fetchLastOrder, {
      onSuccess: (response) => {
        dispatch(setLastOrder(response || null))
      },
    })

  const login: LoginHandleType = useCallback(
    (user, notification) => {
      scrollBodyEnable()
      dispatch(setUserProfile(user))
      dispatch(clearFavorites())
      lastOrderDataMutate()
      customerDataMutate()
      dispatch(setIsAuth(true))
      if (notification !== undefined) {
        setNotificationContent(notification)
      }
    },
    [
      clearFavorites,
      customerDataMutate,
      dispatch,
      lastOrderDataMutate,
      setIsAuth,
      setUserProfile,
    ],
  )

  const logout = useCallback(() => {
    scrollBodyEnable()
    dispatch(setIsAuth(false))
    dispatch(setUserProfile(null))
    dispatch(clearFavorites())
    dispatch(clearCart())
    clearAddresses()
    clearPayers()
    setCheckoutToStorage(null)
  }, [
    clearAddresses,
    clearCart,
    clearFavorites,
    clearPayers,
    dispatch,
    setIsAuth,
    setUserProfile,
  ])

  const showModalAuth = useCallback(() => {
    authModalRef?.current?.click()
  }, [])

  useEffect(() => {
    const storedUser = getClientUser()
    if (storedUser !== null) {
      login(storedUser, undefined)
    } else {
      setCheckoutToStorage(null)
    }
    dispatch(setIsInit(true))
  }, [dispatch, login, setIsInit])

  useEffect(() => {
    if (!isInit) {
      dispatch(setModals([]))
    }
  }, [dispatch, isInit, setModals])

  useEffect(() => {
    setIsFetching(isLoadingCustomerData || isLoadingLastOrder)
  }, [isLoadingCustomerData, isLoadingLastOrder])

  useEffect(() => {
    if (isInit && !isLoadingLastOrder) {
      if (businessAreas === null || businessAreas.length === 0 || !isAuth) {
        setLeadHitStockId(null)
        return
      }

      const _getBusinessAreaUuid = (foundName: string) => {
        const businessArea = businessAreas.find(
          (ba) => (ba.name || "").toLowerCase() === foundName.toLowerCase(),
        )
        return businessArea?.uuid || null
      }

      // в lastOrder может не быть или
      // может быть своя сфера бизнеса,
      // которая не может быть в stock id в фиде anyquery
      // + хранится название сферы бизнеса с пробелами
      const lastOrderBusinessAreaName = lastOrder?.bussiness_area || null

      setLeadHitStockId(
        lastOrderBusinessAreaName !== null
          ? _getBusinessAreaUuid(lastOrderBusinessAreaName)
          : null,
      )
    }
  }, [
    businessAreas,
    isAuth,
    isInit,
    lastOrder?.bussiness_area,
    isLoadingLastOrder,
  ])

  const contextValue = useMemo(
    () => ({
      login,
      logout,
      user,
      isAuth,
      isInit,
      notification: notificationContent,
      authModalRef: authModalRef,
      showModalAuth: showModalAuth,
      updateUser: (user: User | null) => {
        dispatch(setUserProfile(user))
      },
      setNotification: setNotificationContent,
      customer,
      lastOrder,
      isFetching,
      isLoadingLastOrder,
    }),
    [
      login,
      logout,
      user,
      isAuth,
      isInit,
      notificationContent,
      authModalRef,
      showModalAuth,
      dispatch,
      setUserProfile,
      setNotificationContent,
      customer,
      lastOrder,
      isFetching,
      isLoadingLastOrder,
    ],
  )

  return (
    <UserContext.Provider value={contextValue}>{children}</UserContext.Provider>
  )
}

export const useAuth = (): ContextType => {
  const userContext = useContext(UserContext)

  if (userContext === null) {
    throw new Error("User context have to be provided")
  }

  return {
    user: userContext.user,
    login: userContext.login,
    logout: userContext.logout,
    isAuth: userContext.isAuth,
    isInit: userContext.isInit,
    notification: userContext.notification,
    authModalRef: userContext.authModalRef,
    showModalAuth: userContext.showModalAuth,
    updateUser: userContext.updateUser,
    setNotification: userContext.setNotification,
    customer: userContext.customer,
    lastOrder: userContext.lastOrder,
    isFetching: userContext.isFetching,
    isLoadingLastOrder: userContext.isLoadingLastOrder,
  } as const
}
