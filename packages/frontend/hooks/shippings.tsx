import { useAppDispatch, useAppSelector } from "./redux"
import { cartSlice } from "../store/reducers/cartSlice"
import {
  createContext,
  FC,
  useCallback,
  useContext,
  useEffect,
  useMemo,
  useState,
} from "react"
import { cartAPI } from "../api/cartAPI"
import { useApp } from "./app"
import { dateToISOString, timeToDate } from "../utils/helpers"
import { useMutation } from "react-query"
import {
  fetchShippingCost,
  ShippingPaymentMethodType,
} from "../api/checkoutAPI"

const UseShippingsProps = undefined as unknown as {
  product?: {
    totalQty: number
    currentCount: number
    fastQty: number
  }
  withLocation?: boolean
}

type UseShippingsType = (
  props?: typeof UseShippingsProps,
) => UseShippingsReturnType

type CalcShippingDateReturnType = {
  shippingDate: Date | null
  isExpressShipping: boolean
}

type GetShippingsDataType = (props: {
  totalQty?: number
  shippingFastTime?: string
  shippingShift?: number
  currentCount?: number
  fastQty?: number
}) => Omit<CalcShippingDateReturnType, "isDateToOnlyCompany">

type ValidateOnWeekendDateType = (date: Date) => {
  validDate: Date
  isValid: boolean
}

type UseShippingsContextReturnType = {
  leftFreeShippingCourier: number | null
  updateShippingCost: (cost: number | null) => void
  shippingCost: number | null
  recalcShippingDate: (
    data: typeof UseShippingsProps,
  ) => CalcShippingDateReturnType
  isFetchingShippingCost: boolean
  refetchShippingCost: (
    methodAlias: string,
    region: string,
    cartToken: string | null,
    shippingDate?: Date,
  ) => void
  validateOnWeekendDate: ValidateOnWeekendDateType
  getShippingsData: GetShippingsDataType
  paymentMethods: null | ShippingPaymentMethodType[]
  isDateToOnlyCompany: boolean
}

export type UseShippingsReturnType = UseShippingsContextReturnType &
  (CalcShippingDateReturnType | undefined)

const ShippingsContext = createContext<UseShippingsContextReturnType | null>(
  null,
)

export const Provider: FC = ({ children }) => {
  const dispatch = useAppDispatch()
  const shippingCost = useAppSelector((state) => state.cart.shippingCost)
  const totalCost = useAppSelector((state) => state.cart.totalCost)
  const [leftFreeShippingCourier, setLeftFreeShippingCourier] = useState<
    number | null
  >(null)
  const { setShippingCost } = cartSlice.actions
  const { location, settings, isDateToOnlyCompany } = useApp()

  const [paymentMethods, setPaymentMethods] = useState<
    null | ShippingPaymentMethodType[]
  >(null)

  const { data: dataFreeShipping } = cartAPI.useFreeShipping({
    region: location?.city_full || null,
  })

  const validateOnWeekendDate: ValidateOnWeekendDateType = useCallback(
    (date) => {
      const targetDate = new Date(date)
      let offset = 0
      let isValid = false

      const ruleOnWeekend = () => {
        const date = new Date(targetDate)
        date.setDate(date.getDate() + offset)
        const dayNumber = date.getDay()
        if (dayNumber !== 0) {
          return true
        }

        offset += 1
        return false
      }

      const ruleOnHolidays = () => {
        const date = new Date(targetDate)
        date.setDate(date.getDate() + offset)
        const holidays = (settings?.holidays || []).map((h) =>
          dateToISOString(h),
        )

        if (!holidays.includes(dateToISOString(date))) {
          return true
        }

        offset += 1
        return false
      }

      while (!isValid) {
        if (!ruleOnHolidays()) {
          isValid = false
          continue
        }
        if (!ruleOnWeekend()) {
          isValid = false
          continue
        }
        isValid = true
      }

      targetDate.setDate(targetDate.getDate() + offset)

      return {
        validDate: targetDate,
        isValid: dateToISOString(date) === dateToISOString(targetDate),
      }
    },
    [settings?.holidays],
  )

  const getShippingsData: GetShippingsDataType = useCallback(
    ({
      totalQty = 0,
      shippingShift = 1,
      currentCount = 0,
      fastQty,
      shippingFastTime,
    }) => {
      let shippingDate = null as Date | null
      let isExpressShipping = false as boolean

      if (totalQty > 0 && !!shippingFastTime) {
        const now = new Date()
        const fastDate = timeToDate(shippingFastTime)
        const tomorrow = new Date()
        tomorrow.setDate(tomorrow.getDate() + 1)

        if (fastDate !== null) {
          const isMainStore =
            fastQty !== undefined && fastQty > 0
              ? totalQty > 0 && currentCount <= fastQty
              : false
          if (now.getTime() < fastDate.getTime()) {
            shippingDate = now
            const { validDate, isValid } = validateOnWeekendDate(shippingDate)
            shippingDate = validDate
            isExpressShipping = isMainStore && isValid

            if (!isMainStore) {
              const date = new Date()
              date.setDate(shippingDate.getDate() + shippingShift)
              shippingDate = validateOnWeekendDate(date).validDate
            }
          } else {
            isExpressShipping = false
            shippingDate = tomorrow
            shippingDate = validateOnWeekendDate(shippingDate).validDate

            if (!isMainStore) {
              const date = new Date(tomorrow)
              date.setDate(date.getDate() + shippingShift)
              shippingDate = validateOnWeekendDate(date).validDate
            }
          }
        }
      } else {
        isExpressShipping = false
        shippingDate = null
      }

      return {
        isExpressShipping,
        shippingDate,
      }
    },
    [validateOnWeekendDate],
  )

  // обертка диспатчер
  const updateShippingCostDispatch = useCallback(
    (cost: number | null) => {
      dispatch(setShippingCost(cost))
    },
    [dispatch, setShippingCost],
  )

  const calcShippingDate = useCallback(
    ({ product }: typeof UseShippingsProps) => {
      const { isExpressShipping, shippingDate } = getShippingsData({
        currentCount: product?.currentCount,
        fastQty: product?.fastQty,
        totalQty: product?.totalQty,
        shippingShift: settings?.shippingShift,
        shippingFastTime: settings?.shippingFastTime,
      })

      return { shippingDate, isExpressShipping }
    },
    [getShippingsData, settings?.shippingShift, settings?.shippingFastTime],
  )

  const { mutate: shippingCostMutate, isLoading: isFetchingShippingCost } =
    useMutation(fetchShippingCost, {
      onSuccess: (response) => {
        updateShippingCostDispatch(response?.shipping_cost || 0)
        setPaymentMethods(response?.payment_methods || [])
      },
    })

  const refetchShippingCost = useCallback(
    (
      methodAlias: string,
      region: string,
      cartToken: string | null,
      shippingDate?: Date,
    ) => {
      if (cartToken !== null && methodAlias.length > 0) {
        shippingCostMutate({
          shipping_method: methodAlias,
          region: region,
          cart: cartToken,
          shipping_date:
            shippingDate !== undefined ? shippingDate.toISOString() : undefined,
        })
      }
    },
    [shippingCostMutate],
  )

  useEffect(() => {
    if (!!dataFreeShipping) {
      if (dataFreeShipping.order_min_cost > 0) {
        const diff = (totalCost - dataFreeShipping.order_min_cost) * -1
        setLeftFreeShippingCourier(diff >= 0 ? diff : 0)
      }
    } else {
      setLeftFreeShippingCourier(null)
    }
  }, [dataFreeShipping, totalCost])

  const value: UseShippingsContextReturnType = useMemo(
    () => ({
      shippingCost,
      updateShippingCost: updateShippingCostDispatch,
      leftFreeShippingCourier,
      recalcShippingDate: calcShippingDate,
      isFetchingShippingCost: isFetchingShippingCost,
      refetchShippingCost: refetchShippingCost,
      validateOnWeekendDate,
      getShippingsData,
      paymentMethods,
      isDateToOnlyCompany,
    }),
    [
      calcShippingDate,
      leftFreeShippingCourier,
      shippingCost,
      updateShippingCostDispatch,
      refetchShippingCost,
      isFetchingShippingCost,
      validateOnWeekendDate,
      getShippingsData,
      paymentMethods,
      isDateToOnlyCompany,
    ],
  )

  return (
    <ShippingsContext.Provider value={value}>
      {children}
    </ShippingsContext.Provider>
  )
}

export const useShippings: UseShippingsType = (props) => {
  const shippingsContext = useContext(ShippingsContext)

  if (shippingsContext === null) {
    throw new Error("Shipping context have to be provided")
  }

  const { product } = props || {}
  const { recalcShippingDate } = shippingsContext

  const calculatedShippingData = useMemo(
    () =>
      product !== undefined
        ? recalcShippingDate({
            product: {
              fastQty: product?.fastQty || 0,
              currentCount: product?.currentCount || 0,
              totalQty: product?.totalQty || 0,
            },
          })
        : {
            isExpressShipping: false,
            shippingDate: null,
          },
    [product, recalcShippingDate],
  )

  return {
    ...shippingsContext,
    recalcShippingDate,
    ...calculatedShippingData,
  }
}
