import {
  createContext,
  FC,
  useCallback,
  useContext,
  useEffect,
  useMemo,
  useRef,
  useState,
} from "react"
import { scrollBodyDisable, scrollBodyEnable } from "../utils/helpers"

type HookReturnType = {
  isShow: boolean
  hide: () => void
  show: () => void
}

type ContextPropsType = {
  setParentRef: (ref: HTMLDivElement) => void
}

type ContextReturnType = HookReturnType & ContextPropsType

export type HookPopupPropsType = {
  ref: HTMLDivElement | null
}

type HookPopupType = (props?: HookPopupPropsType) => HookReturnType

const CategoriesPopupContext = createContext<ContextReturnType | null>(null)

export const Provider: FC = ({ children }) => {
  const [isShow, setIsShow] = useState(false)
  const containerRef = useRef<HTMLDivElement>(null)

  const hide = useCallback(() => {
    scrollBodyEnable()
    setIsShow(false)
  }, [])

  const show = useCallback(() => {
    scrollBodyDisable()
    setIsShow(true)
  }, [setIsShow])

  const setParentRef: (ref: HTMLDivElement) => void = useCallback((ref) => {
    ;(containerRef.current as HTMLDivElement) = ref
    scrollBodyDisable(containerRef.current || undefined)
  }, [])

  const contextValue: ContextReturnType = useMemo(
    () => ({
      isShow,
      show,
      hide,
      setParentRef: setParentRef,
    }),
    [isShow, show, hide, setParentRef],
  )

  return (
    <CategoriesPopupContext.Provider value={contextValue}>
      {children}
    </CategoriesPopupContext.Provider>
  )
}

export const useCategoriesPopup: HookPopupType = (props) => {
  const { ref } = props || {}
  const categoriesPopupContext = useContext(CategoriesPopupContext)
  if (categoriesPopupContext === null) {
    throw new Error("Categories context have to be provided")
  }

  useEffect(() => {
    if (!!ref) {
      categoriesPopupContext.setParentRef(ref)
    }
  }, [ref])

  return {
    isShow: categoriesPopupContext.isShow,
    show: categoriesPopupContext.show,
    hide: categoriesPopupContext.hide,
  } as const
}
