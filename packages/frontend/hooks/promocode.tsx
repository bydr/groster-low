import { useMutation } from "react-query"
import { fetchApplyPromocode, fetchCancelPromocode } from "../api/promocodeAPI"
import { useCallback, useState } from "react"
import { useAppDispatch, useAppSelector } from "./redux"
import { cartSlice } from "../store/reducers/cartSlice"
import { ApiError } from "../../contracts/contracts"

type UsePromocodeReturnType = {
  isFetching: boolean
  apply: (code: string) => void
  cancel: () => void
  error: string | null
  promocode: string | null
  discount: number
  updatePromocode: (code: string | null) => void
  updateDiscount: (discount: number) => void
}

type UsePromocodePropsType = {
  cart: string | null
}

export const usePromocode = ({
  cart,
}: UsePromocodePropsType): UsePromocodeReturnType => {
  const [isFetching, setIsFetching] = useState<boolean>(false)
  const promocode = useAppSelector((state) => state.cart.promocode)
  const discount = useAppSelector((state) => state.cart.discount)
  const [error, setError] = useState<string | null>(null)
  const { setPromocode, setDiscount, setTotalCost } = cartSlice.actions
  const dispatch = useAppDispatch()

  const updateDiscount = useCallback(
    (discount) => {
      dispatch(setDiscount(discount))
    },
    [dispatch, setDiscount],
  )
  const updatePromocode = useCallback(
    (promocode) => {
      dispatch(setPromocode(promocode))
    },
    [dispatch, setPromocode],
  )

  const applyMutation = useMutation(fetchApplyPromocode, {
    onMutate: () => {
      setError(null)
      setIsFetching(true)
    },
    onSuccess: (response, variables) => {
      setError(null)
      updatePromocode(variables.code || null)
      updateDiscount(response.discount || 0)
      if (response.total_cost !== undefined) {
        dispatch(setTotalCost(response.total_cost))
      }
      setIsFetching(false)
    },
    onError: (error: ApiError) => {
      console.log("error ", error?.message || "Произошла ошибка")
      setError(error?.message || "Произошла ошибка")
      setIsFetching(false)
    },
  })

  const cancelMutation = useMutation(fetchCancelPromocode, {
    onMutate: () => {
      setError(null)
      setIsFetching(true)
    },
    onSuccess: (response) => {
      setError(null)
      updatePromocode(null)
      updateDiscount(0)
      if (response.total_cost !== undefined) {
        dispatch(setTotalCost(response.total_cost))
      }
      setIsFetching(false)
    },
    onError: (error: ApiError) => {
      console.log("error ", error?.message || "Произошла ошибка")
      setError(error?.message || "Произошла ошибка")
      setIsFetching(false)
    },
  })

  const applyHandle = useCallback(
    (code: string) => {
      if (cart !== null) {
        applyMutation.mutate({
          cart: cart,
          code: code,
        })
      }
    },
    [applyMutation, cart],
  )

  const cancelHandle = useCallback(() => {
    if (cart !== null) {
      cancelMutation.mutate({
        cart: cart,
      })
    }
  }, [cancelMutation, cart])

  return {
    isFetching,
    apply: applyHandle,
    cancel: cancelHandle,
    promocode: promocode,
    discount: discount,
    updatePromocode: updatePromocode,
    updateDiscount: updateDiscount,
    error,
  }
}
