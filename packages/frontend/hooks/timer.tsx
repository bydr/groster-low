import { useCallback, useEffect, useMemo, useRef, useState } from "react"

type UseTimerPropsType = {
  interval?: number
}
type UseTimerType = (props?: UseTimerPropsType) => {
  lifeTime: number | null
  setLifeTime: (time: number | null) => void
  resetTimer: () => void
  timeFormatMinutes: string
}

export const useTimer: UseTimerType = ({ interval = 1000 } = {}) => {
  const [lifeTime, setLifeTime] = useState<number | null>(null)
  const timer = useRef<number | undefined>(undefined)

  const resetTimer = useCallback(() => {
    clearInterval(timer.current)
  }, [])

  const timeFormatMinutes = useMemo(
    () =>
      lifeTime !== null
        ? String(Math.floor(lifeTime / 60)) +
          ":" +
          ("0" + String(Math.floor(lifeTime % 60))).slice(-2)
        : "",
    [lifeTime],
  )

  useEffect(() => {
    timer.current =
      (lifeTime || 0) > 0
        ? Number(
            setInterval(() => {
              setLifeTime((lifeTime || 0) - 1)
            }, interval),
          )
        : undefined
    return () => resetTimer()
  }, [lifeTime, interval])

  return {
    lifeTime,
    setLifeTime,
    timeFormatMinutes,
    resetTimer,
  }
}
