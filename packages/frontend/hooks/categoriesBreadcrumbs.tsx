import { useCallback, useEffect, useState } from "react"
import { Category } from "../../contracts/contracts"
import { BreadcrumbItemType } from "../components/Breadcrumbs/BreadcrumbItem"
import { ICategoryTreeItem } from "../types/types"
import { createBreadcrumbsFromCategories } from "../utils/helpers"
import { useAppSelector } from "./redux"

export const useCategoriesBreadcrumbs: () => {
  breadcrumbs: Record<string, BreadcrumbItemType> | null
} = () => {
  const categories = useAppSelector((state) => state.catalog.categories)
  const currentCategory = useAppSelector(
    (state) => state.catalog.currentCategory,
  )
  const [breadcrumbs, setBreadcrumbs] = useState<Record<
    string,
    BreadcrumbItemType
  > | null>(null)

  const getSequence = useCallback(() => {
    let mainId: null | number = null
    let currentMainTree = {} as ICategoryTreeItem | undefined
    let middleTree = {} as ICategoryTreeItem
    const sequence = [] as Category[]
    if (
      !!categories &&
      !!currentCategory &&
      !!categories.fetched &&
      !!categories.compared
    ) {
      sequence.push(categories.fetched[currentCategory.uuid || ""]) //самый нижний уровень
      mainId = categories.compared[currentCategory.id || ""]

      if (!!categories.tree && mainId !== null) {
        currentMainTree = categories.tree[mainId] // главная ветка

        //если непосредственный родитель категории есть в compare
        //значит имеем три уровня вложенности
        const middleId = categories.compared[currentCategory.parent || ""]

        if (
          !!middleId &&
          middleId === mainId &&
          currentMainTree !== undefined
        ) {
          middleTree =
            currentMainTree.children[currentCategory.parent || ""] || {}
        }

        if (Object.keys(middleTree || {}).length > 0) {
          sequence.push(middleTree)
        }
        if (
          currentMainTree !== undefined &&
          currentMainTree.uuid !== undefined
        ) {
          sequence.push(currentMainTree)
        }
      }
    }

    return sequence.reverse()
  }, [categories, currentCategory])

  useEffect(() => {
    const sequence = getSequence()

    if (sequence.length > 0) {
      setBreadcrumbs(
        createBreadcrumbsFromCategories(sequence, categories?.fetched || null),
      )
    }
  }, [categories?.fetched, getSequence])

  return {
    breadcrumbs,
  }
}
