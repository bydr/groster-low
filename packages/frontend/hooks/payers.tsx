import { useMutation } from "react-query"
import { fetchPayers } from "../api/accountAPI"
import { PayerListItemType } from "../types/types"
import { useAppDispatch, useAppSelector } from "./redux"
import { accountSlice } from "../store/reducers/accountSlice"
import { useCallback } from "react"

type UsePayersType = () => {
  payers: null | PayerListItemType[]
  refetch: () => void
  isFetching: boolean
  clear: () => void
}

export const usePayers: UsePayersType = () => {
  const payers = useAppSelector((state) => state.profile.payers)
  const { setPayers } = accountSlice.actions
  const dispatch = useAppDispatch()

  const { mutate, isLoading } = useMutation(fetchPayers, {
    onSuccess: (response) => {
      if (!!response && response.length > 0) {
        dispatch(setPayers(response))
      } else {
        dispatch(setPayers([]))
      }
    },
  })

  const clear = useCallback(() => {
    dispatch(setPayers(null))
  }, [dispatch, setPayers])

  return {
    isFetching: isLoading,
    payers: payers,
    refetch: mutate,
    clear: clear,
  }
}
