import {
  createContext,
  ReactNode,
  RefObject,
  useCallback,
  useContext,
  useEffect,
  useMemo,
  useRef,
  useState,
} from "react"
import { useThrottle } from "./throttle"
import { useQuery } from "react-query"
import {
  DigiCategoryTypeAutoComplete,
  DigiFacetType,
  DigiProductAutoCompleteType,
  DigiProductSearchType,
  DigiSearchResponseType,
  DigiSortType,
  DigiStItemType,
  DigiTapItemType,
  fetchAutoComplete,
  PAGE,
  SORT,
} from "../api/digineticaAPI"
import { useAppDispatch, useAppSelector } from "./redux"
import {
  PayloadDistinctType,
  PayloadSliderType,
  SearchHistoryItemType,
  searchSlice,
  UpdateFilterPropsType,
} from "../store/reducers/searchSlice"
import { ProductDetailListType } from "../../contracts/contracts"
import { ExtendSearchResultType } from "../components/Search"
import { useRouter } from "next/router"
import { ROUTES } from "../utils/constants"
import { useCategoriesPopup } from "./categoriesPopup"
import { useWindowSize } from "./windowSize"
import { getBreakpointVal } from "../styles/utils/Utils"
import { breakpoints } from "../styles/utils/vars"
import { scrollBodyDisable, scrollBodyEnable } from "../utils/helpers"

export const FACET_CATEGORIES_KEY = "categories"
export const SEARCH_PER_PAGE = 20

export type QueryPageSearchType = {
  query?: string
  filter?: string | string[]
  page?: string
  sort?: DigiSortType
}

export type UpdateSearchPropsType = {
  query?: string
  page?: number
  filter?: Record<string, string[]>
  sort?: DigiSortType
  withReset?: boolean
  isAppend?: boolean
}

export type UseSearchReturnType = {
  query: string | null
  updateQueryText: (value: string) => void
  appendToHistory: (item: SearchHistoryItemType) => void
  removeFromHistory: (item: SearchHistoryItemType) => void
  clearHistory: () => void
  currentText: string | null
  history: SearchHistoryItemType[]
  correction: string | null
  isUseCorrection: boolean
  isNotResults: boolean
  zeroQueries: boolean
  products: DigiProductSearchType[]
  facets: DigiFacetType[]
  selectedFacets: DigiFacetType[]
  autoComplete: {
    products: DigiProductAutoCompleteType[]
    query: string | null
    brands: []
    contents: []
    sts: DigiStItemType[]
    categories: DigiCategoryTypeAutoComplete[]
    taps: DigiTapItemType[]
    isInit: boolean
    setIsInit: (value: boolean) => void
    ref: RefObject<HTMLDivElement>
  }
  total: number | null
  page: number | null
  productsData: ProductDetailListType
  clearSearchResult: () => void
  clearAutoComplete: () => void
  updateSearch: (data: UpdateSearchPropsType) => void
  updateFilter: (
    props: UpdateFilterPropsType<PayloadDistinctType | PayloadSliderType>,
  ) => void
  filter: null | Record<string, string[]>
  resetFilter: () => void
  showAutoComplete: () => void
  hideAutoComplete: () => void
  isSubmitting: boolean
  setIsSubmitting: (val: boolean) => void
  isShowAutoComplete: boolean
  isLoadingAutoComplete: boolean
  sort: DigiSortType | null
  setIsShowPopup: (state: boolean) => void
  isShowPopup: boolean
  inputRef: RefObject<HTMLInputElement>
}

type UseSearchReturnContextType = UseSearchReturnType & {
  setSearchResult: (value?: DigiSearchResponseType | null) => void
  setSearchFilter: (value: string[] | null) => void
}

const HISTORY_SEARCH_KEY = "searchHistory"

export const getHistorySaved = (): SearchHistoryItemType[] => {
  const history = localStorage.getItem(HISTORY_SEARCH_KEY)
  return !!history ? JSON.parse(history) : []
}

export const setHistorySaved = (
  history: SearchHistoryItemType[] | null,
): void => {
  history !== null
    ? localStorage.setItem(HISTORY_SEARCH_KEY, JSON.stringify(history))
    : localStorage.removeItem(HISTORY_SEARCH_KEY)
}

export const translationNameFacet = (name: string): string => {
  switch (name) {
    case "price":
      return "Цена"
    case "brands":
      return "Бренды"
    case FACET_CATEGORIES_KEY:
      return "Категории"
    default:
      return name
  }
}

const filterToQueryFormat = (
  filter: Record<string, string[]> | null,
): string[] => {
  const buildFilter: string[] = []
  if (filter !== null && Object.keys(filter).length > 0) {
    Object.keys(filter).map((name) => {
      buildFilter.push(`${name}:${filter[name].join(";")}`)
    })
  }

  return buildFilter
}

const PRODUCT_FETCH_SIZE_PC = 4
const PRODUCT_FETCH_SIZE_MOBILE = 2

export const SearchContext = createContext<null | UseSearchReturnContextType>(
  null,
)

export function Provider({ children }: { children?: ReactNode }): JSX.Element {
  const [, setThrottleValue] = useState<string | null>(null)
  const [throttledFunction, isReady] = useThrottle(setThrottleValue, 500)
  const [isUseCorrection, setIsUseCorrection] = useState(false)
  const [isNotResults, setIsNotResults] = useState(false)
  const router = useRouter()
  const [isShowAutoComplete, setIsShowAutoComplete] = useState(false)
  const [isSubmitting, setIsSubmitting] = useState(false)
  const [isShowPopup, setIsShowPopup] = useState<boolean>(false)
  const [isInitAutoComplete, setIsInitAutoComplete] = useState(false)
  const { hide: categoriesPopupHide } = useCategoriesPopup()

  const {
    history,
    query,
    currentText,
    autoComplete,
    correction,
    products,
    total,
    zeroQueries,
    facets,
    productsData,
    page,
    filter,
    selectedFacets,
    sort,
    isUpdate,
  } = useAppSelector((state) => state.search)

  const isOpenAutoCompleteOnFetcher = useRef<boolean>(false)
  const autoCompleteIsEmpty = useRef<boolean>(false)

  const { width } = useWindowSize()

  const dispatch = useAppDispatch()
  const {
    setQuery,
    setProductsAutoComplete,
    setCategoriesAutoComplete,
    setTapsAutoComplete,
    setStsAutoComplete,
    setHistory,
    setCorrection,
    setCurrentText,
    setProducts,
    setTotal,
    setZeroQueries,
    setQueryAutoComplete,
    setFacets,
    setProductsData,
    setPage,
    setFilter,
    updateFilter,
    setSelectedFacets,
    setSort,
    appendProductsData,
    appendProducts,
    setIsUpdate,
  } = searchSlice.actions

  const productSizeAutoComplete = useMemo(
    () =>
      width !== undefined && width <= getBreakpointVal(breakpoints.md)
        ? PRODUCT_FETCH_SIZE_MOBILE
        : PRODUCT_FETCH_SIZE_PC,
    [width],
  )

  const { isLoading: isLoadingAutoComplete } = useQuery(
    [
      "searchAutocomplete",
      currentText,
      isReady,
      productSizeAutoComplete,
      isShowAutoComplete,
    ],
    () =>
      currentText !== null && isReady && isShowAutoComplete
        ? fetchAutoComplete({
            query: currentText,
            size: productSizeAutoComplete,
          })
        : undefined,
    {
      onSuccess: (response) => {
        if (response === undefined) {
          return
        }
        if (response !== null) {
          dispatch(setQueryAutoComplete(response.query))
          dispatch(setProductsAutoComplete(response.products))
          dispatch(setCategoriesAutoComplete(response.categories))
          dispatch(
            setTapsAutoComplete(response.taps.length > 0 ? response.taps : []),
          )
          dispatch(
            setStsAutoComplete(response.sts.length > 0 ? response.sts : []),
          )

          autoCompleteIsEmpty.current =
            response.query !== null &&
            response.query.length > 0 &&
            !response.products.length &&
            !response.sts.length &&
            !response.taps.length

          if (!autoCompleteIsEmpty.current) {
            if (isOpenAutoCompleteOnFetcher.current) {
              if (!isShowAutoComplete) {
                showAutoComplete()
              }
            }
          } else {
            hideAutoComplete({
              _isUpdateQueryText: false,
            })
          }

          isOpenAutoCompleteOnFetcher.current = false
        } else {
          autoCompleteIsEmpty.current = true
          dispatch(setQueryAutoComplete(null))
          dispatch(setProductsAutoComplete([]))
          dispatch(setCategoriesAutoComplete([]))
          dispatch(setTapsAutoComplete([]))
          dispatch(setStsAutoComplete([]))
        }
      },
    },
  )

  const updateQueryText = useCallback(
    (value: string) => {
      if (autoCompleteIsEmpty.current) {
        isOpenAutoCompleteOnFetcher.current = true
      }
      dispatch(setCurrentText(value))
      throttledFunction(value)
    },
    [dispatch, setCurrentText, throttledFunction],
  )

  const autoCompleteRef = useRef<HTMLDivElement | null>(null)

  const showAutoComplete = useCallback(() => {
    categoriesPopupHide()

    if (!autoCompleteIsEmpty.current) {
      if (currentText === null) {
        updateQueryText("")
      }
      inputRef.current?.click()
      setIsShowAutoComplete(true)
    }
    scrollBodyDisable(autoCompleteRef.current || undefined)
  }, [categoriesPopupHide, currentText, updateQueryText, autoCompleteIsEmpty])

  const hideAutoComplete = useCallback(
    ({
      _isUpdateQueryText = true,
    }: { _isUpdateQueryText?: boolean } | undefined = {}) => {
      setIsShowAutoComplete(false)
      setIsShowPopup(false)

      scrollBodyEnable()

      if (!!_isUpdateQueryText) {
        if (query !== currentText && !isSubmitting) {
          updateQueryText(query || "")
        }
      }
    },
    [currentText, isSubmitting, query, updateQueryText],
  )

  const isAppendProducts = useRef<boolean>(false)

  const setSearchResult = useCallback(
    (search: ExtendSearchResultType | null) => {
      dispatch(setCorrection(search?.correction || null))
      if (isAppendProducts.current) {
        dispatch(appendProducts(search?.products || []))
        dispatch(appendProductsData(search?.productsData || []))
      } else {
        dispatch(setProducts(search?.products || []))
        dispatch(setProductsData(search?.productsData || []))
      }
      dispatch(setTotal(search?.totalHits || 0))
      dispatch(setZeroQueries(!!search?.zeroQueries))
      dispatch(setQuery(search?.query || ""))
      dispatch(setFacets(search?.facets || []))
      dispatch(setSelectedFacets(search?.selectedFacets || []))
      dispatch(setPage(!!search?.page ? +search.page : PAGE))
      dispatch(setSort(!!search?.sortQuery ? search.sortQuery : SORT))
    },
    [
      dispatch,
      setFacets,
      setProducts,
      setQuery,
      setTotal,
      setZeroQueries,
      setCorrection,
      setProductsData,
      setPage,
      setSelectedFacets,
      setSort,
      appendProducts,
      appendProductsData,
    ],
  )

  const setSearchFilter = useCallback(
    (filterQuery: string[] | null) => {
      dispatch(setIsUpdate(false))

      if (filterQuery !== null) {
        const buildFilter: Record<string, string[]> = {}
        filterQuery.map((f) => {
          const item = f.split(":")
          const name = item[0]
          const values = item[1]
          if (!name || !values) {
            return
          }

          buildFilter[name] = values.split(";")
        })
        dispatch(setFilter(buildFilter))
      } else {
        dispatch(setFilter(null))
      }
    },
    [dispatch, setFilter, setIsUpdate],
  )

  const clearSearchResult = useCallback(() => {
    setSearchResult(null)
    updateQueryText("")
    isOpenAutoCompleteOnFetcher.current = false
    autoCompleteIsEmpty.current = false
    dispatch(setIsUpdate(false))
  }, [updateQueryText, setSearchResult, setIsUpdate, dispatch])

  const clearAutoComplete = useCallback(() => {
    isOpenAutoCompleteOnFetcher.current = true
    updateQueryText("")
  }, [updateQueryText])

  const updateFilterHandle = useCallback(
    (props: UpdateFilterPropsType<PayloadDistinctType | PayloadSliderType>) => {
      dispatch(updateFilter(props))
      dispatch(setPage(PAGE))
      dispatch(setIsUpdate(true))
    },
    [setIsUpdate, dispatch, setPage, updateFilter],
  )

  const resetFilterHandle = useCallback(() => {
    dispatch(setFilter({}))
    dispatch(setPage(PAGE))
    dispatch(setIsUpdate(true))
  }, [setIsUpdate, dispatch, setFilter, setPage])

  const updateSearch = useCallback(
    ({
      page: pageIn,
      filter: filterIn,
      query: queryIn,
      sort: sortIn,
      withReset = false,
      isAppend = false,
    }: UpdateSearchPropsType) => {
      isAppendProducts.current = isAppend
      let buildQuery: QueryPageSearchType

      if (queryIn !== undefined && withReset) {
        resetFilterHandle()
        buildQuery = {
          query: queryIn,
        }
      } else {
        const p =
          pageIn !== undefined ? pageIn : page !== null ? page : undefined
        const s =
          sortIn !== undefined ? sortIn : sort !== null ? sort : undefined
        const f = filterToQueryFormat(
          filterIn !== undefined ? filterIn : filter,
        )
        buildQuery = {
          filter: f,
          query: queryIn || query || undefined,
          page: p !== undefined && p !== PAGE ? `${p}` : undefined,
          sort: s !== undefined && s !== SORT ? s : undefined,
        }
      }

      Object.keys(buildQuery).map((key) => {
        if (buildQuery[key] === undefined) {
          delete buildQuery[key]
        }
      })

      void router
        .replace(
          {
            pathname: `${ROUTES.search}`,
            query: buildQuery,
          },
          undefined,
          {
            scroll: !isAppend || !isAppendProducts.current,
            shallow: false,
          },
        )
        .catch((err) => {
          console.log("error: ", err)
        })
        .then(() => console.log("this will succeed"))
    },
    [resetFilterHandle, page, sort, filter, query],
  )

  const appendHistoryItem = useCallback(
    (item: SearchHistoryItemType) => {
      const h = [...history, item].reduce(
        (uniq: SearchHistoryItemType[], item) => {
          return !!uniq.find((el) => el.query === item.query)
            ? uniq
            : [...uniq, item]
        },
        [],
      )

      dispatch(setHistory(h))
      setHistorySaved(h)
    },
    [dispatch, history, setHistory],
  )

  const removeHistoryItem = useCallback(
    (item: SearchHistoryItemType) => {
      const h = history.filter((el) => el.query !== item.query)
      dispatch(setHistory(h))
      setHistorySaved(h)
    },
    [dispatch, history, setHistory],
  )

  const clearHistory = useCallback(() => {
    dispatch(setHistory([]))
    setHistorySaved(null)
  }, [dispatch, setHistory])

  const inputRef = useRef<HTMLInputElement>(null)

  useEffect(() => {
    router.events.on("routeChangeComplete", hideAutoComplete)

    return () => {
      router.events.off("routeChangeComplete", hideAutoComplete)
    }
  }, [hideAutoComplete, router.events])

  useEffect(() => {
    if (filter !== null && isUpdate) {
      updateSearch({
        filter: filter,
      })
    }
  }, [filter, updateSearch, isUpdate])

  useEffect(() => {
    dispatch(setHistory(getHistorySaved()))
  }, [dispatch, setHistory])

  useEffect(() => {
    setIsUseCorrection(!!correction && !!query && correction !== query)
  }, [correction, query])

  useEffect(() => {
    setIsNotResults(products.length <= 0 && zeroQueries)
  }, [products.length, zeroQueries])

  const contextValue = useMemo(
    () =>
      ({
        query: query,
        updateQueryText: updateQueryText,
        appendToHistory: appendHistoryItem,
        removeFromHistory: removeHistoryItem,
        currentText: currentText,
        history: history,
        clearHistory,
        correction: correction,
        isUseCorrection,
        setSearchResult,
        products: products,
        total: total,
        zeroQueries,
        autoComplete: {
          products: autoComplete.products,
          brands: autoComplete.brands,
          taps: autoComplete.taps,
          sts: autoComplete.sts,
          categories: autoComplete.categories,
          query: autoComplete.query,
          contents: autoComplete.contents,
          isInit: isInitAutoComplete,
          setIsInit: setIsInitAutoComplete,
          ref: autoCompleteRef,
        },
        isNotResults,
        facets,
        productsData,
        clearSearchResult,
        clearAutoComplete,
        page,
        setSearchFilter,
        updateSearch,
        updateFilter: updateFilterHandle,
        filter,
        selectedFacets,
        resetFilter: resetFilterHandle,
        setIsSubmitting,
        isSubmitting,
        hideAutoComplete,
        showAutoComplete,
        isShowAutoComplete,
        isLoadingAutoComplete,
        sort,
        setIsShowPopup,
        isShowPopup,
        inputRef,
      } as UseSearchReturnContextType),
    [
      query,
      updateQueryText,
      appendHistoryItem,
      removeHistoryItem,
      currentText,
      history,
      clearHistory,
      correction,
      isUseCorrection,
      setSearchResult,
      setSearchFilter,
      products,
      total,
      autoComplete.products,
      autoComplete.brands,
      autoComplete.taps,
      autoComplete.sts,
      autoComplete.categories,
      autoComplete.query,
      autoComplete.contents,
      zeroQueries,
      isNotResults,
      facets,
      productsData,
      clearSearchResult,
      clearAutoComplete,
      page,
      updateSearch,
      updateFilterHandle,
      filter,
      selectedFacets,
      resetFilterHandle,
      setIsSubmitting,
      isSubmitting,
      hideAutoComplete,
      showAutoComplete,
      isShowAutoComplete,
      isLoadingAutoComplete,
      sort,
      isShowPopup,
      inputRef,
      isInitAutoComplete,
    ],
  )

  return (
    <SearchContext.Provider value={contextValue}>
      {children}
    </SearchContext.Provider>
  )
}

type UseSearchPropsType = {
  initialQuery?: string | null
  search?: ExtendSearchResultType
  filterQuery?: string[] | null
  sortQuery?: DigiSortType | null
}

export const useSearch = ({
  initialQuery,
  search,
  filterQuery,
}: UseSearchPropsType | undefined = {}): UseSearchReturnType => {
  const searchContext = useContext(SearchContext)

  if (searchContext === null) {
    throw new Error("Search context have to be provided")
  }

  useEffect(() => {
    if (initialQuery !== undefined) {
      searchContext.updateQueryText(initialQuery || "")
    }
  }, [initialQuery])

  useEffect(() => {
    if (search !== undefined) {
      searchContext.setSearchResult(search)
    }
  }, [search])

  useEffect(() => {
    if (filterQuery !== undefined) {
      searchContext.setSearchFilter(filterQuery)
    }
  }, [filterQuery])

  return {
    updateQueryText: searchContext.updateQueryText,
    query: searchContext.query,
    history: searchContext.history,
    appendToHistory: searchContext.appendToHistory,
    removeFromHistory: searchContext.removeFromHistory,
    clearHistory: searchContext.clearHistory,
    correction: searchContext.correction,
    currentText: searchContext.currentText,
    isUseCorrection: searchContext.isUseCorrection,
    zeroQueries: searchContext.zeroQueries,
    products: searchContext.products,
    total: searchContext.total,
    autoComplete: searchContext.autoComplete,
    isNotResults: searchContext.isNotResults,
    facets: searchContext.facets,
    page: searchContext.page,
    productsData: searchContext.productsData,
    clearSearchResult: searchContext.clearSearchResult,
    clearAutoComplete: searchContext.clearAutoComplete,
    updateSearch: searchContext.updateSearch,
    updateFilter: searchContext.updateFilter,
    filter: searchContext.filter,
    selectedFacets: searchContext.selectedFacets,
    resetFilter: searchContext.resetFilter,
    setIsSubmitting: searchContext.setIsSubmitting,
    isSubmitting: searchContext.isSubmitting,
    hideAutoComplete: searchContext.hideAutoComplete,
    showAutoComplete: searchContext.showAutoComplete,
    isShowAutoComplete: searchContext.isShowAutoComplete,
    isLoadingAutoComplete: searchContext.isLoadingAutoComplete,
    sort: searchContext.sort,
    setIsShowPopup: searchContext.setIsShowPopup,
    isShowPopup: searchContext.isShowPopup,
    inputRef: searchContext.inputRef,
  }
}
