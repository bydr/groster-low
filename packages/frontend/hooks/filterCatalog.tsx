import { useAppDispatch, useAppSelector } from "./redux"
import { useRouter } from "next/router"
import {
  createContext,
  FC,
  useCallback,
  useContext,
  useEffect,
  useMemo,
  useRef,
  useState,
} from "react"
import {
  catalogSlice,
  IAvailableParams,
  INITIAL_PAGE,
  INITIAL_PER_PAGE,
  MANUAL_FILTER_KEYS,
  PriceRangeItemType,
  PriceRangeType,
} from "../store/reducers/catalogSlice"
import {
  QueryFiltersCatalogType,
  SortBySelectedType,
  SortByType,
  UpdateFilterQueryPropsType,
  UseFilterBaseReturnType,
} from "../types/types"
import { normalizeQuery } from "../utils/helpers"
import { COOKIE_SCROLL_POSITION_CATALOG } from "../utils/constants"

type FilterContextType = {
  priceRange: PriceRangeType
  isFast: boolean
  isEnabled: boolean
  sortBy: SortByType
  sortBySelected: SortBySelectedType
  isLoading: boolean
  storesParams: IAvailableParams | null
} & UseFilterBaseReturnType<UpdateFilterQueryPropsType, QueryFiltersCatalogType>
const FilterContext = createContext<null | FilterContextType>(null)

const DEFAULT_IS_SCROLL = true

export const Provider: FC = ({ children }) => {
  const router = useRouter()
  const dispatch = useAppDispatch()
  const {
    setCurrentPage,
    setCheckedPriceRange,
    updateCheckedParams,
    setCheckedParamsKeysTotal,
    updateCheckedStores,
  } = catalogSlice.actions
  const priceRange = useAppSelector((state) => state.catalog.filter.priceRange)
  const isEnabled = useAppSelector((state) => state.catalog.isEnabled)
  const isLoading = useAppSelector((state) => state.catalog.isLoading)
  const isFast = useAppSelector((state) => state.catalog.isFast)
  const sortBy = useAppSelector((state) => state.catalog.sortBy)
  const sortBySelected = useAppSelector((state) => state.catalog.sortBySelected)
  const itemsPerPage = useAppSelector((state) => state.catalog.itemsPerPage)

  const checkedParamsKeysTotal = useAppSelector(
    (state) => state.catalog.filter.checkedParamsKeysTotal,
  )
  const availableParams = useAppSelector(
    (state) => state.catalog.filter.availableParams,
  )
  const filterParams = useAppSelector((state) => state.catalog.filter.params)
  const storesParams = useAppSelector(
    (state) => state.catalog.filter.stores.params,
  )

  const [query, setQuery] = useState<QueryFiltersCatalogType | null>(null)
  const isScrollRef = useRef<boolean>(DEFAULT_IS_SCROLL)

  const updateFilterQuery = useCallback(
    (props?: UpdateFilterQueryPropsType | null) => {
      const queryObj = {
        category: router.query.category,
      } as QueryFiltersCatalogType

      if (props !== null) {
        const checkedTotalVal =
          props?.checkedParamsKeysTotal === undefined
            ? checkedParamsKeysTotal
            : props?.checkedParamsKeysTotal
        if (!!checkedTotalVal.length) {
          queryObj.params = checkedTotalVal
            .filter((c) =>
              storesParams !== null
                ? !storesParams[MANUAL_FILTER_KEYS.store].checkedKeys.includes(
                    c,
                  )
                : c,
            )
            .join(",")
        }

        let priceVal: PriceRangeItemType | undefined | null = props?.price
        if (priceVal === undefined) {
          priceVal = priceRange.checked || undefined
        }
        if (!!priceVal) {
          queryObj.price = !!priceVal
            ? `${priceVal.min}-${priceVal.max}`
            : undefined
        }

        const isEnabledVal =
          props?.isEnabled === undefined ? isEnabled : props.isEnabled
        const isFastVal = props?.isFast === undefined ? isFast : props.isFast
        const sortByVal =
          props?.sortBySelected === undefined
            ? sortBySelected
            : props.sortBySelected
        queryObj.is_enabled = isEnabledVal ? "1" : "0"
        queryObj.is_fast = isFastVal ? "1" : "0"
        queryObj.sortby = !!sortByVal ? sortByVal : undefined

        let page: number | undefined = props?.page
        if (!page) {
          page = INITIAL_PAGE
        }
        if (!!page) {
          queryObj.page = page
        }

        let perPage: number | undefined = props?.perPage
        if (!perPage) {
          perPage = itemsPerPage || INITIAL_PER_PAGE
        }
        if (!!perPage) {
          queryObj.per_page = perPage
        }

        queryObj.store =
          props?.store !== undefined
            ? props?.store.join(",")
            : storesParams !== null
            ? storesParams[MANUAL_FILTER_KEYS.store].checkedKeys.join(",")
            : undefined
      }

      isScrollRef.current =
        props?.isScroll !== undefined ? props?.isScroll : DEFAULT_IS_SCROLL
      setQuery(queryObj)
    },
    [
      checkedParamsKeysTotal,
      isEnabled,
      isFast,
      itemsPerPage,
      priceRange.checked,
      router.query.category,
      sortBySelected,
      storesParams,
    ],
  )

  const clearFilterQuery = useCallback(() => {
    updateFilterQuery(null)
    dispatch(
      updateCheckedParams({
        keyFilter: checkedParamsKeysTotal,
        checked: false,
      }),
    )
    dispatch(
      updateCheckedStores({
        keyFilter: checkedParamsKeysTotal,
        checked: false,
      }),
    )
    dispatch(setCheckedPriceRange(null))
    dispatch(setCurrentPage(INITIAL_PAGE))
  }, [
    updateFilterQuery,
    dispatch,
    setCheckedPriceRange,
    setCurrentPage,
    updateCheckedParams,
    checkedParamsKeysTotal,
    updateCheckedStores,
  ])

  useEffect(() => {
    if (!query) {
      return
    }

    const { current: isScroll } = isScrollRef

    if (isScroll) {
      localStorage.removeItem(COOKIE_SCROLL_POSITION_CATALOG)
    }

    void router
      .replace(
        {
          pathname: router.pathname,
          query: normalizeQuery(query),
        },
        undefined,
        {
          scroll: isScroll,
          shallow: false,
        },
      )
      .catch((err) => {
        console.log("error: ", err)
      })
      .then(() => console.log("this will succeed"))
  }, [router.pathname, query])

  useEffect(() => {
    let checkedKeys: string[] = []

    if (availableParams !== null) {
      Object.keys(availableParams).map((k) => {
        checkedKeys = [...checkedKeys, ...availableParams[k].checkedKeys]
      })
    } else {
      checkedKeys = []
    }

    if (storesParams !== null) {
      Object.keys(storesParams).map((k) => {
        checkedKeys = [...checkedKeys, ...storesParams[k].checkedKeys]
      })
    } else {
      checkedKeys = []
    }

    dispatch(
      setCheckedParamsKeysTotal(
        checkedKeys.reduce((uniq: string[], item) => {
          return uniq.includes(item) ? uniq : [...uniq, item]
        }, []),
      ),
    )
  }, [availableParams, storesParams, dispatch, setCheckedParamsKeysTotal])

  const contextValue = useMemo(
    () => ({
      updateFilterQuery,
      query,
      clearFilterQuery,
      availableParams: availableParams,
      checkedParamsKeysTotal,
      priceRange,
      isFast,
      isEnabled,
      sortBySelected,
      sortBy,
      isLoading,
      params: filterParams,
      storesParams,
    }),
    [
      updateFilterQuery,
      query,
      clearFilterQuery,
      availableParams,
      checkedParamsKeysTotal,
      priceRange,
      isFast,
      isEnabled,
      sortBySelected,
      sortBy,
      isLoading,
      filterParams,
      storesParams,
    ],
  )

  return (
    <FilterContext.Provider value={contextValue}>
      {children}
    </FilterContext.Provider>
  )
}

export const useFilterCatalog = (): FilterContextType => {
  const filterContext = useContext(FilterContext)

  if (filterContext === null) {
    throw new Error("Filters context have to be provided")
  }

  return {
    query: filterContext.query,
    updateFilterQuery: filterContext.updateFilterQuery,
    clearFilterQuery: filterContext.clearFilterQuery,
    availableParams: filterContext.availableParams,
    checkedParamsKeysTotal: filterContext.checkedParamsKeysTotal,
    isFast: filterContext.isFast,
    isEnabled: filterContext.isEnabled,
    priceRange: filterContext.priceRange,
    sortBy: filterContext.sortBy,
    sortBySelected: filterContext.sortBySelected,
    isLoading: filterContext.isLoading,
    params: filterContext.params,
    storesParams: filterContext.storesParams,
  } as const
}
