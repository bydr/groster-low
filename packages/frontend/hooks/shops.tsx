import { useAppDispatch, useAppSelector } from "./redux"
import { shopSlice } from "../store/reducers/shopSlice"
import { useMutation } from "react-query"
import { fetchShops } from "../api/checkoutAPI"
import {
  createContext,
  FC,
  useCallback,
  useContext,
  useEffect,
  useMemo,
  useState,
} from "react"
import { ShopType, StoreProductType } from "../types/types"

type UseShopsReturnType = {
  shops: ShopType[] | null
  updateShops: (shops: ShopType[] | null) => void
  isFetching: boolean
  shopsAvailable: (StoreProductType & ShopType)[]
  activatedShop: (StoreProductType & ShopType) | null
  selectedShop: (StoreProductType & ShopType) | null
  setActivatedShop: (shop: (StoreProductType & ShopType) | null) => void
  requestShops: () => void
  updateSelectedShop: (id: string) => void
}

type UseShopsPropsType = {
  shops?: StoreProductType[]
  selectedShopId?: string
}

type UseShopsType = (props?: UseShopsPropsType) => UseShopsReturnType

type ShopsContentReturnType = UseShopsReturnType & {
  initShops: (props: { shops: StoreProductType[] }) => void
  initSelectedShopId: (id?: string) => void
}

const ShopsContext = createContext<ShopsContentReturnType | null>(null)

export const Provider: FC = ({ children }) => {
  const shopsApp = useAppSelector((state) => state.shop.shops)
  const { setShops } = shopSlice.actions
  const dispatch = useAppDispatch()

  const [isFetching, setIsFetching] = useState<boolean>(false)

  const updateShops = useCallback(
    (shops: ShopType[] | null) => {
      dispatch(setShops(shops))
    },
    [dispatch, setShops],
  )

  const { mutate: requestShops } = useMutation(fetchShops, {
    onMutate: () => {
      setIsFetching(true)
    },
    onSuccess: (response) => {
      if (!!response) {
        updateShops(response)
      } else {
        updateShops(null)
      }
      setIsFetching(false)
    },
    onError: () => {
      setIsFetching(false)
    },
  })

  const [shopsAvailable, setShopsAvailable] = useState<
    (StoreProductType & ShopType)[]
  >([])
  const [activatedShop, setActivatedShop] = useState<
    (StoreProductType & ShopType) | null
  >(null)
  const [selectedShop, setSelectedShop] = useState<
    (StoreProductType & ShopType) | null
  >(null)

  const initShops: (props: { shops: StoreProductType[] }) => void = useCallback(
    ({ shops }) => {
      if (shops !== undefined) {
        if (!!shopsApp && shopsApp.length > 0 && shops.length > 0) {
          setShopsAvailable(
            shops.map((shop) => ({
              ...shopsApp.filter((s) => s.uuid === shop.uuid)[0],
              deliveryDate: shop.deliveryDate,
              isMain: shop.isMain,
              availableStatus: shop.availableStatus,
            })),
          )
        }
      } else {
        setShopsAvailable([])
      }
    },
    [shopsApp],
  )

  const initSelectedShopId: (id?: string) => void = useCallback(
    (id) => {
      if (id === undefined) {
        return
      }
      const foundShop = shopsApp?.find((s) => s.uuid === id) || null
      setSelectedShop(foundShop)
      setActivatedShop(foundShop)
    },
    [shopsApp],
  )

  const value: ShopsContentReturnType = useMemo(
    () => ({
      shops: shopsApp,
      updateShops,
      isFetching,
      requestShops,
      shopsAvailable,
      activatedShop: activatedShop,
      setActivatedShop: setActivatedShop,
      initShops,
      initSelectedShopId,
      selectedShop,
      updateSelectedShop: initSelectedShopId,
    }),
    [
      shopsApp,
      updateShops,
      isFetching,
      requestShops,
      shopsAvailable,
      activatedShop,
      initShops,
      initSelectedShopId,
      selectedShop,
    ],
  )

  return <ShopsContext.Provider value={value}>{children}</ShopsContext.Provider>
}

export const useShops: UseShopsType = ({ shops, selectedShopId } = {}) => {
  const shopsContext = useContext(ShopsContext)

  if (shopsContext === null) {
    throw new Error("Shops context have to be provided")
  }

  useEffect(() => {
    if (shops === undefined) {
      return
    }
    shopsContext.initShops({
      shops,
    })
  }, [shops])

  useEffect(() => {
    shopsContext.initSelectedShopId(selectedShopId)
  }, [selectedShopId])

  return {
    shops: shopsContext.shops,
    updateShops: shopsContext.updateShops,
    isFetching: shopsContext.isFetching,
    requestShops: shopsContext.requestShops,
    shopsAvailable: shopsContext.shopsAvailable,
    activatedShop: shopsContext.activatedShop,
    setActivatedShop: shopsContext.setActivatedShop,
    selectedShop: shopsContext.selectedShop,
    updateSelectedShop: shopsContext.updateSelectedShop,
  }
}

export const getNavigatorLink = ({
  latitude,
  longitude,
  description,
}: {
  latitude?: string
  longitude?: string
  description?: string
}): string =>
  `yandexnavi://show_point_on_map?lat=${latitude || ""}&lon=${
    longitude || ""
  }&zoom=12&no-balloon=0&desc=${description || ""}`
