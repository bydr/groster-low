import { useCallback, useEffect, useState } from "react"
import { ApiError, OrderType } from "../../contracts/contracts"
import {
  getOrderStateData,
  OrderStateNumberType,
  StateOrderDataType,
} from "../components/Account/History/Orders/Order/State"
import {
  compareSpecificationProducts,
  dateToString,
  isCurrentYear,
} from "../utils/helpers"
import { useAppDispatch, useAppSelector } from "./redux"
import { accountSlice } from "../store/reducers/accountSlice"
import { productsAPI } from "../api/productsAPI"
import { ProductWithChildType, SpecificationItemType } from "../types/types"
import { useMutation } from "react-query"
import {
  fetchOrderCancel,
  fetchOrdersByUid,
  fetchOrderSubscribe,
  fetchOrderUnSubscribe,
} from "../api/orderAPI"
import { useCart } from "./cart"

type UseOrderReturnType = {
  stateOrder?: StateOrderDataType
  createAtFormatted?: string
  order?: OrderType
  totalCost: number
  products: Record<string, ProductWithChildType> | null
  samples: Record<string, ProductWithChildType> | null
  specification: Record<string, SpecificationItemType> | null
  subscribeInterval: number | null
  subscribe: (interval: number) => void
  unsubscribe: () => void
  isFetchingSubscription: boolean
  repeat: () => void
  isFetchingRepeat: boolean
  isFetchingRepeatGlobal: boolean
  repeatIsSuccess: boolean
  isFetchingProducts: boolean
  isFetchingCancel: boolean
  cancel: () => void
  error?: string
  setRepeatIsSuccess: (value: boolean) => void
}

const ORDER_CANCEL_STATUS_NUMBER = 4

export const useOrder = ({
  order,
}: {
  order?: OrderType & { uid: string }
}): UseOrderReturnType => {
  const dispatch = useAppDispatch()
  const specification = useAppSelector(
    (state) => state.profile.history.orders.detail.specification,
  )
  const products = useAppSelector(
    (state) => state.profile.history.orders.detail.products,
  )
  const samples = useAppSelector(
    (state) => state.profile.history.orders.detail.samples,
  )
  const { setProducts, setSamples, setSpecification, updateOrderState } =
    accountSlice.actions
  const [stateOrder, setStateOrder] = useState<StateOrderDataType>()
  const [uuidProductsInCart, setUuidProductsInCart] = useState<string[] | null>(
    null,
  )
  const [subscribeInterval, setSubscribeInterval] = useState<number | null>(
    order?.subscribe_interval || null,
  )
  const {
    addToCartSomeProducts,
    token,
    formatProductsToSpecification,
    isSomeProductsAddFetching,
  } = useCart()
  const [error, setError] = useState<string | undefined>()
  const [isFetchingRepeat, setIsFetchingRepeat] = useState(false)
  const [repeatIsSuccess, setRepeatIsSuccess] = useState(false)

  const { data: dataPurchasedProducts, isFetching: isFetchingProducts } =
    productsAPI.useProductsFetch(
      uuidProductsInCart !== null ? uuidProductsInCart.join(",") : null,
    )

  const { mutate: subscribeMutate, isLoading: isLoadingSubscribe } =
    useMutation(fetchOrderSubscribe, {
      onSuccess: (response, request) => {
        setSubscribeInterval(request.interval)
      },
    })

  const { mutate: unsubscribeMutate, isLoading: isLoadingUnsubscribe } =
    useMutation(fetchOrderUnSubscribe, {
      onSuccess: () => {
        setSubscribeInterval(null)
      },
    })

  const subscribe = useCallback(
    (interval: number) => {
      if (order?.uid !== undefined) {
        subscribeMutate({
          interval: interval,
          uid: order.uid,
        })
      }
    },
    [order?.uid, subscribeMutate],
  )

  const unsubscribe = useCallback(() => {
    if (order?.uid !== undefined) {
      unsubscribeMutate({
        uid: order.uid,
      })
    }
  }, [order?.uid, unsubscribeMutate])

  const { mutate: orderDetailMutate } = useMutation(fetchOrdersByUid, {
    onSuccess: async (response) => {
      if (!!response && response.products !== undefined) {
        setIsFetchingRepeat(true)
        await addToCartSomeProducts(
          formatProductsToSpecification(response.products),
          token,
          false,
          () => {
            setRepeatIsSuccess(true)
          },
        )
      }
      setIsFetchingRepeat(false)
    },
    onMutate: () => {
      setIsFetchingRepeat(true)
    },
  })

  const { mutate: orderCancelMutate, isLoading: isFetchingCancel } =
    useMutation(fetchOrderCancel, {
      onSuccess: (_, request) => {
        setStateOrder(getOrderStateData({ state: ORDER_CANCEL_STATUS_NUMBER }))
        dispatch(
          updateOrderState({
            uid: request.uid,
            state: ORDER_CANCEL_STATUS_NUMBER,
          }),
        )
      },
      onError: (error: ApiError) => {
        setError(error?.message || "Произошла ошибка")
      },
      onMutate: () => {
        setError(undefined)
      },
    })

  const cancel = useCallback(() => {
    if (!order?.uid) {
      return
    }
    orderCancelMutate({
      uid: order.uid,
    })
  }, [order?.uid, orderCancelMutate])

  useEffect(() => {
    if (!!order?.state) {
      setStateOrder(
        getOrderStateData({ state: order.state as OrderStateNumberType }),
      )
    }
  }, [order?.state])

  useEffect(() => {
    if (!!order?.products) {
      setUuidProductsInCart(order.products.map((p) => p.uuid || ""))
      dispatch(setSpecification(order.products))
    } else {
      setUuidProductsInCart(null)
      dispatch(setSpecification(null))
    }
  }, [dispatch, order?.products, setSpecification])

  const updateProducts = useCallback(
    (productsList: ProductWithChildType[] | null) => {
      let compared = null
      if (productsList !== null) {
        compared = {}
        for (const item of productsList) {
          compared[item.uuid || ""] = { ...item }
        }
      }
      const { products, samples } = compareSpecificationProducts(
        specification,
        compared,
      )
      dispatch(setProducts(products))
      dispatch(setSamples(samples))
    },
    [dispatch, setProducts, setSamples, specification],
  )

  const repeat = useCallback(async () => {
    if (isFetchingRepeat || isSomeProductsAddFetching) {
      return
    }
    setRepeatIsSuccess(false)
    setIsFetchingRepeat(true)

    if (specification !== null) {
      await addToCartSomeProducts(specification, token, false, () => {
        setRepeatIsSuccess(true)
      })
      setIsFetchingRepeat(false)
    } else {
      if (!!order?.uid) {
        orderDetailMutate({
          uid: order.uid,
        })
      }
    }
  }, [
    addToCartSomeProducts,
    isFetchingRepeat,
    isSomeProductsAddFetching,
    order?.uid,
    orderDetailMutate,
    specification,
    token,
  ])

  useEffect(() => {
    if (dataPurchasedProducts !== undefined) {
      updateProducts(dataPurchasedProducts)
    }
  }, [dataPurchasedProducts, updateProducts])

  useEffect(() => {
    setSubscribeInterval(
      !!order?.subscribe_interval ? order?.subscribe_interval : null,
    )
  }, [order?.subscribe_interval])

  return {
    order: !!order ? { ...order } : undefined,
    totalCost: order?.total_cost || 0,
    stateOrder: stateOrder,
    createAtFormatted:
      order?.create_at !== undefined
        ? dateToString(
            new Date(order.create_at),
            !isCurrentYear(order.create_at),
          )
        : undefined,
    products,
    samples,
    specification,
    subscribeInterval,
    subscribe,
    unsubscribe,
    isFetchingSubscription: isLoadingSubscribe || isLoadingUnsubscribe,
    repeat,
    isFetchingRepeat: isFetchingRepeat,
    repeatIsSuccess: repeatIsSuccess,
    isFetchingProducts: isFetchingProducts,
    isFetchingCancel,
    cancel,
    error,
    setRepeatIsSuccess,
    isFetchingRepeatGlobal: isSomeProductsAddFetching,
  }
}
