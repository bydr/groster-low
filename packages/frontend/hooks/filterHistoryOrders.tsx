import {
  QueryFiltersHistoryType,
  UpdateFilterHistoryQueryPropsType,
  UseFilterBaseReturnType,
} from "../types/types"
import {
  createContext,
  FC,
  useCallback,
  useContext,
  useEffect,
  useMemo,
  useState,
} from "react"
import { useAppDispatch, useAppSelector } from "./redux"
import { accountSlice, INITIAL_PER_PAGE } from "../store/reducers/accountSlice"
import { useRouter } from "next/router"
import {
  IAvailableParamsKeys,
  INITIAL_PAGE,
} from "../store/reducers/catalogSlice"
import { OrderStateNumberType } from "../components/Account/History/Orders/Order/State"
import { usePayers } from "./payers"
import { ParamsResponse } from "../../contracts/contracts"
import { ViewModeType } from "../components/Account/History/Orders"
import { normalizeQuery } from "../utils/helpers"

type FilterContextType = UseFilterBaseReturnType<
  UpdateFilterHistoryQueryPropsType,
  QueryFiltersHistoryType
> & {
  currentPage: number
  routerQuery: Omit<QueryFiltersHistoryType, "per_page"> & { perPage?: number }
  viewMode: ViewModeType
  setViewMode: (mode: ViewModeType) => void
}

const getValidStateOrder = (
  value?: number | null,
): OrderStateNumberType | undefined =>
  !!value
    ? [2, 3, 4, 5].includes(value)
      ? (value as OrderStateNumberType)
      : undefined
    : undefined

const FilterContext = createContext<null | FilterContextType>(null)

export const Provider: FC = ({ children }) => {
  const router = useRouter()
  const dispatch = useAppDispatch()
  const {
    setHistoryFilterParams,
    setHistoryFilterAvailableParams,
    updateCheckedParams,
    setCurrentPage,
    setFilterState,
    setFilterCategory,
    setItemsPerPage,
  } = accountSlice.actions
  const historyFilter = useAppSelector((state) => state.profile.history.filter)
  const availableParams = useAppSelector(
    (state) => state.profile.history.filter.availableParams,
  )
  const currentPage = useAppSelector((state) => state.profile.history.page)
  const itemsPerPage = useAppSelector(
    (state) => state.profile.history.itemsPerPage,
  )
  const checkedParamsKeysTotal = useAppSelector(
    (state) => state.profile.history.filter.checkedParamsKeysTotal,
  )
  const params = useAppSelector((state) => state.profile.history.filter.params)
  const [query, setQuery] = useState<QueryFiltersHistoryType | null>(null)
  const [viewMode, setViewMode] = useState<ViewModeType>("orders")

  const { payers, refetch: refetchPayers } = usePayers()

  const { query: routerQuery } = useRouter()
  const {
    page: pageQuery,
    payer: payerQuery,
    year: yearQuery,
    state: stateQuery,
    per_page: perPageQuery,
    category: categoryQuery,
  } = routerQuery as QueryFiltersHistoryType

  const updateFilterQuery = useCallback(
    (props?: UpdateFilterHistoryQueryPropsType) => {
      const queryObj = {} as QueryFiltersHistoryType

      let payerVal: string | undefined
      if (props?.payer !== undefined) {
        payerVal = props.payer
      } else {
        payerVal =
          historyFilter.availableParams !== null
            ? historyFilter.availableParams["payer"]?.checkedKeys.join(",")
            : undefined
      }

      if (payerVal !== undefined) {
        queryObj.payer = payerVal
      }

      let yearVal: string | undefined
      if (props?.year !== undefined) {
        yearVal = props.year
      } else {
        yearVal =
          historyFilter.availableParams !== null
            ? historyFilter.availableParams["year"]?.checkedKeys[0]
            : undefined
      }

      if (yearVal !== undefined) {
        queryObj.year = yearVal
      }

      let stateVal: OrderStateNumberType | undefined
      if (!!props?.state) {
        stateVal = getValidStateOrder(props?.state)
      } else {
        stateVal = getValidStateOrder(historyFilter.state)
      }
      queryObj.state = stateVal

      let categoryVal: string | undefined
      if (!!props?.category) {
        categoryVal = props.category
      } else {
        categoryVal = historyFilter.category || undefined
      }
      queryObj.category = categoryVal

      let page: number | undefined = props?.page
      if (!page) {
        page = INITIAL_PAGE
      }
      if (!!page) {
        queryObj.page = page
      }

      let perPage: number | undefined = props?.perPage
      if (!perPage) {
        perPage = itemsPerPage
      }
      if (!!perPage) {
        queryObj.per_page = perPage
      }

      setQuery(queryObj)
    },
    [
      historyFilter.availableParams,
      historyFilter.state,
      itemsPerPage,
      historyFilter.category,
    ],
  )

  const clearFilterQuery = useCallback(() => {
    setQuery({})
    dispatch(
      updateCheckedParams({
        keyFilter: historyFilter.checkedParamsKeysTotal,
        checked: false,
      }),
    )
    dispatch(setCurrentPage(INITIAL_PAGE))
  }, [
    dispatch,
    updateCheckedParams,
    historyFilter.checkedParamsKeysTotal,
    setCurrentPage,
  ])

  useEffect(() => {
    if (payers === null) {
      refetchPayers()
    }
  }, [payers, refetchPayers])

  useEffect(() => {
    if (payers !== null) {
      const params: ParamsResponse = [
        {
          uuid: "year",
          order: 0,
          name: "Год",
          values: [
            {
              uuid: "2021",
              name: "2021",
            },
            {
              uuid: "2022",
              name: "2022",
            },
          ],
        },
      ]

      params.push({
        uuid: "payer",
        order: 0,
        name: "Плательщик",
        values: payers.map((p) => {
          return {
            name: p.name,
            uuid: p.uid,
          }
        }),
      })
      dispatch(setHistoryFilterParams(params))
    }
  }, [dispatch, payers, setHistoryFilterParams])

  useEffect(() => {
    if (Object.keys(historyFilter.params).length > 0) {
      const formatted: IAvailableParamsKeys = {}
      Object.keys(historyFilter.params).map((param) => {
        formatted[historyFilter.params[param].uuid || ""] = undefined
      })
      dispatch(setHistoryFilterAvailableParams(formatted))
    }
  }, [dispatch, historyFilter.params, setHistoryFilterAvailableParams])

  useEffect(() => {
    let keys: string[] = []
    if (payerQuery !== undefined && payerQuery.length > 0) {
      keys = payerQuery.split(",")
    }
    if (yearQuery !== undefined && yearQuery.length > 0) {
      keys.push(yearQuery)
    }
    dispatch(
      updateCheckedParams({
        keyFilter: keys,
        checked: true,
      }),
    )
  }, [
    dispatch,
    payerQuery,
    updateCheckedParams,
    yearQuery,
    historyFilter.params,
  ])

  useEffect(() => {
    dispatch(setCurrentPage(!!pageQuery ? +pageQuery : INITIAL_PAGE))
  }, [dispatch, pageQuery, setCurrentPage])

  useEffect(() => {
    dispatch(setItemsPerPage(!!perPageQuery ? +perPageQuery : INITIAL_PER_PAGE))
  }, [dispatch, perPageQuery, setItemsPerPage])

  useEffect(() => {
    dispatch(
      setFilterState(
        stateQuery !== undefined ? (+stateQuery as OrderStateNumberType) : -1,
      ),
    )
  }, [dispatch, setFilterState, stateQuery])

  useEffect(() => {
    if (!!categoryQuery) {
      dispatch(setFilterCategory(categoryQuery))
    } else {
      dispatch(setFilterCategory(null))
    }
  }, [categoryQuery, dispatch, setFilterCategory])

  useEffect(() => {
    if (!query) {
      return
    } else {
      void router
        .replace(
          {
            pathname: router.pathname,
            query: normalizeQuery(query),
          },
          undefined,
          {
            shallow: true,
            scroll: true,
          },
        )
        .catch((err) => {
          console.log("error: ", err)
        })
        .then(() => console.log("this will succeed"))
    }
  }, [router.pathname, query])

  const contextValue = useMemo(
    () => ({
      updateFilterQuery,
      query,
      clearFilterQuery,
      availableParams,
      checkedParamsKeysTotal,
      currentPage,
      routerQuery: {
        page: pageQuery,
        payer: payerQuery,
        year: yearQuery,
        state: stateQuery,
        category: categoryQuery,
        perPage: perPageQuery,
      },
      viewMode,
      setViewMode,
      params,
    }),
    [
      updateFilterQuery,
      query,
      clearFilterQuery,
      availableParams,
      checkedParamsKeysTotal,
      currentPage,
      pageQuery,
      payerQuery,
      yearQuery,
      stateQuery,
      categoryQuery,
      perPageQuery,
      viewMode,
      params,
    ],
  )

  return (
    <FilterContext.Provider value={contextValue}>
      {children}
    </FilterContext.Provider>
  )
}

export const useFilterHistoryOrders = (): FilterContextType => {
  const filterContext = useContext(FilterContext)

  if (filterContext === null) {
    throw new Error("Filters context have to be provided")
  }

  return {
    checkedParamsKeysTotal: filterContext.checkedParamsKeysTotal,
    updateFilterQuery: filterContext.updateFilterQuery,
    query: filterContext.query,
    clearFilterQuery: filterContext.clearFilterQuery,
    availableParams: filterContext.availableParams,
    routerQuery: filterContext.routerQuery,
    currentPage: filterContext.currentPage,
    viewMode: filterContext.viewMode,
    setViewMode: filterContext.setViewMode,
    params: filterContext.params,
  }
}
