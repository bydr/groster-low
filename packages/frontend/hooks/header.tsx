import { useAppDispatch, useAppSelector } from "./redux"
import {
  createContext,
  ReactNode,
  useCallback,
  useContext,
  useEffect,
  useMemo,
  useRef,
  useState,
} from "react"
import { useCategoriesPopup } from "./categoriesPopup"
import { catalogSlice } from "../store/reducers/catalogSlice"
import { catalogAPI } from "../api/catalogAPI"
import { useAuth } from "./auth"
import { useCart } from "./cart"
import { Category } from "../../contracts/contracts"
import { NotificationType } from "../components/Notification/Notification"
import { useShops } from "./shops"
import { useRouter } from "next/router"
import { scrollBodyDisable, scrollBodyEnable } from "../utils/helpers"
import { useScrollDirection } from "./scrollDirection"
import {
  cssScrollDown,
  cssScrollUp,
} from "../layouts/Default/Header/StyledHeader"

type UseHeaderReturnType = {
  isShowPopup: boolean
  isShowMenu: boolean
  isShowMiniCart: boolean
  catalogShowedToggle: () => void
  menuHide: () => void
  menuShow: () => void
  businessAreas: Category[] | null
  notificationCart: NotificationType | null
  notificationAuth: NotificationType | null
  setIsShowMiniCart: (value: boolean) => void
  setNotificationCart: (notification: NotificationType | null) => void
  setNotificationAuth: (notification: NotificationType | null) => void
}

type HeaderContextPropsType = UseHeaderReturnType

const HeaderContext = createContext<null | HeaderContextPropsType>(null)

export function Provider({
  children,
}: {
  children?: ReactNode
  host?: string
}): JSX.Element {
  const dispatch = useAppDispatch()
  const [isShowMenu, setIsShowMenu] = useState(false)
  const [isShowMiniCart, setIsShowMiniCart] = useState(false)
  const headerRef = useRef<HTMLDivElement | null>(null)
  const {
    isShow: isShowPopup,
    hide: hidePopup,
    show: showPopup,
  } = useCategoriesPopup()
  const { setCategories, setBusinessAreas } = catalogSlice.actions
  const { data: dataCategories } = catalogAPI.useCategories()
  const {
    notification: notificationAuth,
    setNotification: setNotificationAuth,
  } = useAuth()
  const {
    notification: notificationCart,
    setNotification: setNotificationCart,
  } = useCart()
  const { requestShops } = useShops()
  const businessAreas = useAppSelector((state) => state.catalog.businessAreas)
  const router = useRouter()

  const menuHide = useCallback(() => {
    setIsShowMenu(false)
    scrollBodyEnable()
  }, [])

  const menuShow = useCallback(() => {
    scrollBodyDisable(headerRef.current || undefined)
    setIsShowMenu(true)
    if (isShowPopup) {
      hidePopup()
    }
  }, [hidePopup, isShowPopup])

  const catalogShowedToggle = useCallback(() => {
    if (isShowPopup) {
      hidePopup()
    } else {
      showPopup()
    }
  }, [isShowPopup, hidePopup, showPopup])

  useEffect(() => {
    const cleanHeaderCss = () => {
      document.body.classList.remove(cssScrollUp)
      document.body.classList.remove(cssScrollDown)
    }

    const cleanShowedDialogs = () => {
      hidePopup()
      menuHide()
      setIsShowMiniCart(false)
    }

    const routeChangeHandle = () => {
      cleanHeaderCss()
      cleanShowedDialogs()
    }

    router.events.on("routeChangeStart", cleanHeaderCss)
    router.events.on("routeChangeComplete", routeChangeHandle)

    return () => {
      router.events.off("routeChangeStart", cleanHeaderCss)
      router.events.off("routeChangeComplete", routeChangeHandle)
    }
  }, [hidePopup, menuHide, router.events])

  useEffect(() => {
    if (!!dataCategories) {
      if (!!dataCategories.categories) {
        dispatch(setCategories(dataCategories.categories))
      }
      if (!!dataCategories.bussinessAreas) {
        dispatch(setBusinessAreas(dataCategories.bussinessAreas))
      }
    }
  }, [dataCategories, dispatch, setCategories, setBusinessAreas])

  useEffect(() => {
    requestShops()
  }, [requestShops])

  const scroll = useScrollDirection()

  useEffect(() => {
    switch (scroll) {
      case "up": {
        document.body.classList.add(cssScrollUp)
        document.body.classList.remove(cssScrollDown)
        break
      }
      case "down": {
        document.body.classList.remove(cssScrollUp)
        document.body.classList.add(cssScrollDown)
        break
      }
      case null: {
        document.body.classList.remove(cssScrollUp)
        document.body.classList.remove(cssScrollDown)
      }
    }
  }, [scroll])

  const contextValue = useMemo(
    () => ({
      businessAreas: businessAreas,
      isShowMenu: isShowMenu,
      isShowMiniCart: isShowMiniCart,
      isShowPopup: isShowPopup,
      catalogShowedToggle: catalogShowedToggle,
      notificationCart: notificationCart,
      notificationAuth: notificationAuth,
      setIsShowMiniCart: setIsShowMiniCart,
      menuHide: menuHide,
      menuShow: menuShow,
      setNotificationCart,
      setNotificationAuth,
    }),
    [
      businessAreas,
      catalogShowedToggle,
      isShowMenu,
      isShowMiniCart,
      isShowPopup,
      menuHide,
      menuShow,
      notificationAuth,
      notificationCart,
      setNotificationAuth,
      setNotificationCart,
    ],
  )

  return (
    <HeaderContext.Provider value={contextValue}>
      {children}
    </HeaderContext.Provider>
  )
}

export const useHeader = (): UseHeaderReturnType & HeaderContextPropsType => {
  const headerContext = useContext(HeaderContext)

  if (headerContext === null) {
    throw new Error("Header context have to be provided")
  }

  return {
    businessAreas: headerContext.businessAreas,
    isShowMenu: headerContext.isShowMenu,
    isShowMiniCart: headerContext.isShowMiniCart,
    isShowPopup: headerContext.isShowPopup,
    catalogShowedToggle: headerContext.catalogShowedToggle,
    notificationCart: headerContext.notificationCart,
    notificationAuth: headerContext.notificationAuth,
    setIsShowMiniCart: headerContext.setIsShowMiniCart,
    menuHide: headerContext.menuHide,
    menuShow: headerContext.menuShow,
    setNotificationCart: headerContext.setNotificationCart,
    setNotificationAuth: headerContext.setNotificationAuth,
  } as const
}
