import {
  createContext,
  FC,
  useCallback,
  useContext,
  useEffect,
  useMemo,
  useRef,
  useState,
} from "react"
import { useAppDispatch, useAppSelector } from "./redux"
import {
  ApiError,
  ChangeQtyRequest,
  ProductSpecificationType,
} from "../../contracts/contracts"
import { cartSlice } from "../store/reducers/cartSlice"
import {
  fetchBindUserCart,
  fetchCart,
  fetchChangeQtyProduct,
  fetchChangeQtySample,
  fetchClearCart,
  fetchRemoveCart,
} from "../api/cartAPI"
import { useMutation, useQuery } from "react-query"
import { useClipboardCopy } from "./clipboardCopy"
import {
  ChangeQtyResponse,
  OfferOrderThankType,
  OrderThankType,
  ProductType,
  ProductWithChildType,
  SpecificationItemType,
} from "../types/types"
import {
  COOKIE_HOST_NAME,
  ROUTES,
  SITE_URL,
  TIMEOUT_SUCCESS,
} from "../utils/constants"
import { fetchProductsList } from "../api/productsAPI"
import { useAuth } from "./auth"
import { NotificationType } from "../components/Notification/Notification"
import { usePromocode } from "./promocode"
import { useShippings } from "./shippings"
import {
  compareSpecificationProducts,
  dateToISOString,
  getExpireOneYear,
  onErrorFetcherCart,
} from "../utils/helpers"
import { useRouter } from "next/router"
import Cookies from "universal-cookie"
import { ServerResponse } from "http"

const cookies = new Cookies()

interface CartContextType {
  totalCost: number
  specification: Record<string, SpecificationItemType> | null
  products: Record<string, ProductWithChildType> | null
  samples: Record<string, ProductWithChildType> | null
  token: string | null
  isFetching: boolean
  cartCount: number | null
  cartCost: number
  isSharedLinkCopied: boolean
  notification: NotificationType | null
  setNotification: (notification: NotificationType | null) => void
  productsFetching: string[]
  order: OrderThankType | null
  nextShippingDate: Date | null
  minShippingDate: Date | null
  pickupDate: Date | null
  updateToken: (token: string | null) => void
  shareCart: () => void
  clearCart: () => void
  addToCartSomeProducts: (
    products: Record<string, ProductSpecificationType>,
    tokenCart: string | null,
    afterToCart?: boolean,
    onSuccess?: () => void,
  ) => Promise<void>
  updateSpecification: (
    specification: SpecificationItemType,
    kit?: string[],
  ) => void
  updateProductPriceUnit: ({
    uuid,
    priceUnit,
  }: {
    uuid: string
    priceUnit: number
  }) => void
  refetchCart: () => void
  updateTotalCost: (cost: number) => void
  removeSpecification: (uuid: string) => void
  addProductInFetching: (uuid: string) => void
  removeProductInFetching: (uuid: string) => void
  setOrder: (order: Omit<OrderThankType, "offers"> | null) => void
  formatProductsToSpecification: (
    products: ProductSpecificationType[],
  ) => Record<string, ProductSpecificationType>
  productsInAuth: ProductType[] | null
  specificationMerge: Record<string, SpecificationItemType> | null
  isLoadingProductsInAuth: boolean
  hasProductsInAuth: boolean
  isMergingCart: boolean
  isSomeProductsAddFinish: boolean
  isSomeProductsAddFetching: boolean
  isSomeProductsAddSuccess: boolean
  mergeProductsCarts: () => void
  cleanMergeData: () => void
  isInitializeSpeicification: boolean
}

const COOKIE_CART_TOKEN_KEY = "cart"

const getTokenStorage = (): string | null => {
  const token = cookies.get(COOKIE_CART_TOKEN_KEY)
  return token || null
}

export const setTokenStorage = (
  token: string | null,
  res?: ServerResponse,
): void => {
  if (token !== null) {
    if (res !== undefined) {
      res.setHeader(
        "Set-Cookie",
        `${COOKIE_CART_TOKEN_KEY}=${encodeURIComponent(
          JSON.stringify(token),
        )}; Path=/; Expires=${getExpireOneYear().toUTCString()}`,
      )
    } else {
      cookies.set(COOKIE_CART_TOKEN_KEY, JSON.stringify(token), {
        path: "/",
        expires: getExpireOneYear(),
      })
    }
  } else {
    if (res !== undefined) {
      res.setHeader(
        "Set-Cookie",
        `${COOKIE_CART_TOKEN_KEY}=null; Path=/; Max-Age=0`,
      )
    } else {
      cookies.remove(COOKIE_CART_TOKEN_KEY, {
        path: "/",
      })
    }
  }
}

const CartContext = createContext<CartContextType | null>(null)

// сохраняем массив ids товаров которые есть в корзине
// + проходимся по дочерним комплекта
const getEntityCartIds = (
  specification: Record<string, SpecificationItemType> | null,
): string[] => {
  if (specification === null) {
    return []
  }

  let uuids: string[] = []

  Object.entries(specification).map(([key, spec]) => {
    uuids = [...uuids, key]
    if (!!spec && !!spec.child) {
      uuids = [...uuids, ...Object.keys(spec.child)]
    }
  })

  return uuids.reduce((uniq: string[], item) => {
    return uniq.includes(item) ? uniq : [...uniq, item]
  }, [])
}

const productQty = (itemSpec: SpecificationItemType) =>
  !!itemSpec?.isRemoved ? 0 : itemSpec.quantity || 0
const sampleQty = (itemSpec: SpecificationItemType) =>
  !!itemSpec?.isSampleRemoved ? 0 : itemSpec.sample || 0

export const Provider: FC = ({ children }) => {
  const [isFetching, setIsFetching] = useState<boolean>(false)
  const dispatch = useAppDispatch()
  const specification = useAppSelector((state) => state.cart.specification)
  const datasource = useAppSelector((state) => state.cart.datasource)
  const totalCost = useAppSelector((state) => state.cart.totalCost)
  const productsCost = useAppSelector((state) => state.cart.productsCost)
  const token = useAppSelector((state) => state.cart.token)
  const order = useAppSelector((state) => state.cart.order)
  const productsFetching = useAppSelector(
    (state) => state.cart.productsFetching,
  )
  const minShippingDate = useAppSelector((state) => state.cart.minShippingDate)
  const nextShippingDate = useAppSelector(
    (state) => state.cart.nextShippingDate,
  )
  const productsMerge = useAppSelector((state) => state.cart.merge.products)
  const specificationMerge = useAppSelector(
    (state) => state.cart.merge.specification,
  )

  const [notificationContent, setNotificationContent] =
    useState<NotificationType | null>(null)
  const {
    setToken,
    setSpecification,
    updateSpecification,
    setTotalCost,
    setProductsCost,
    clearCart,
    removeProductSpecification,
    updateProductPriceUnit,
    addProductsFetching,
    removeProductsFetching,
    setOrder,
    setNextShippingDate,
    setMinShippingDate,
    setMergeProducts,
    setMergeSpecification,
    appendDataSource,
  } = cartSlice.actions

  const [isInit, setIsInit] = useState(false)
  const { updatePromocode, updateDiscount } = usePromocode({
    cart: token,
  })
  const { shippingCost, recalcShippingDate } = useShippings()
  const { replace } = useRouter()
  const { handleCopyClick, isCopied } = useClipboardCopy()
  const { isInit: isInitAuth, isAuth, user, updateUser } = useAuth()
  const [pickupDate, setPickupDate] = useState<Date | null>(null)

  const notAuthCart = useRef<string | null>(null)
  const [hasProductsInAuth, setHasProductsInAuth] = useState(false)
  const [isMergingCart, setIsMergingCart] = useState(false)

  const [isSomeProductsAddSuccess, setIsSomeProductsAddSuccess] =
    useState(false)
  const [isSomeProductsAddFinish, setIsSomeProductsAddFinish] = useState(false)
  const [isSomeProductsAddFetching, setIsSomeProductsAddFetching] =
    useState(false)

  const isInitFetchedCart = useRef<boolean>(false)

  const { refetch: refetchCart, isFetching: isFetchingDataCart } = useQuery(
    ["cart", token],
    () => (!!token ? fetchCart(token || "") : null),
    {
      enabled: !!token || isInit,
      onSuccess: (response) => {
        if (!!response) {
          dispatch(setSpecification(response.products || []))
          updateTotalCostDispatch(response.total_cost || 0)
          updateDiscount(response.discount || 0)
          updatePromocode(response.promocode || null)
        } else {
          dispatch(setSpecification([]))
          updateTotalCostDispatch(0)
          updateDiscount(0)
          updatePromocode(null)
        }
        isInitFetchedCart.current = true
      },
      onError: (error: ApiError) => {
        onErrorFetcherCart(error)
      },
    },
  )

  const { mutate: bindCartUserMutate, mutateAsync: bindCartUserMutateAsync } =
    useMutation(fetchBindUserCart, {
      onSuccess: (response, request) => {
        if (user !== null) {
          updateUser({ ...user, cart: request.cart })
        }
      },
    })

  // на основе сохраненных ids товаров
  // получаем данные этих товаров для вывода
  const { mutate: fetchProductsSource } = useMutation(fetchProductsList, {
    onSuccess: (dataProductsInCart) => {
      // получаем данные товаров и
      // сохраняем в стейте текущие товары в корзине
      if (dataProductsInCart !== undefined) {
        dispatch(appendDataSource(dataProductsInCart))
      }
    },
  })

  const {
    mutate: getProductsInAuthMutate,
    isLoading: isLoadingProductsInAuth,
  } = useMutation(fetchProductsList, {
    onSuccess: (response) => {
      dispatch(setMergeProducts(response || []))
    },
    onError: () => {
      dispatch(setMergeProducts([]))
    },
  })

  const { mutate: getCartMutate } = useMutation(fetchCart, {
    onSuccess: async (response) => {
      if (!!response && !!response.products && token !== null) {
        notAuthCart.current = token
        const withAuthCart = user?.cart

        setIsMergingCart(true)
        await bindCartUserMutateAsync({
          cart: notAuthCart.current,
        })

        if (!!withAuthCart) {
          await removeCartMutateAsync({
            uid: withAuthCart,
          })
        }
        setIsMergingCart(false)

        if (!!response.products && response.products.length > 0) {
          setHasProductsInAuth(true)
          dispatch(
            setMergeSpecification(
              formatProductsToSpecification(response.products),
            ),
          )
          getProductsInAuthMutate({
            uuids: response.products.map((p) => p.uuid).join(","),
          })
        }
      }
    },
    onError: (error: ApiError) => {
      onErrorFetcherCart(error)
    },
  })

  const formatProductsToSpecification = useCallback(
    (products: ProductSpecificationType[]) => {
      const t: Record<string, ProductSpecificationType> = {}
      products.map((p) => {
        if (p.uuid !== undefined) {
          let sample = p.sample || 0
          let quantity = p.quantity || 0

          if (!!specification && specification[p.uuid]) {
            sample += specification[p.uuid].sample || 0
            quantity += specification[p.uuid].quantity || 0
          }

          t[p.uuid || ""] = {
            uuid: p.uuid,
            sample: sample,
            quantity: quantity,
          }
        }
      })

      return t
    },
    [specification],
  )

  const updateToken = useCallback(
    (token: string | null) => {
      setTokenStorage(token)
      dispatch(setToken(token))
      if (isInitAuth && isAuth) {
        if (user !== null && user.cart === null) {
          updateUser({ ...user, cart: token })
        }
      }
    },
    [dispatch, isAuth, isInitAuth, setToken, updateUser, user],
  )

  // обертка диспатчер
  const updateSpecificationDispatch = useCallback(
    (specification: SpecificationItemType) => {
      dispatch(updateSpecification(specification))
    },
    [dispatch, updateSpecification],
  )

  // обертка диспатчер
  const updateTotalCostDispatch = useCallback(
    (cost: number) => {
      dispatch(setTotalCost(cost))
    },
    [dispatch, setTotalCost],
  )

  const shareCartHandler = useCallback(() => {
    if (specification === null) {
      return
    }
    const host = cookies.get(COOKIE_HOST_NAME)
    if (!host) {
      return
    }
    const url = `https://${host}${ROUTES.favorites}/shared/${Object.keys(
      specification,
    )
      .filter(
        (key) => !!specification[key].quantity || !!specification[key].sample,
      )
      .map((key) => {
        return `${specification[key].uuid}:${
          specification[key].isRemoved ? 0 : specification[key].quantity
        }:${specification[key].sample || 0}`
      })
      .join(",")}`
    handleCopyClick(url)
  }, [handleCopyClick, specification])

  const { mutateAsync: changeQtyProductMutateAsync } = useMutation(
    fetchChangeQtyProduct,
    {
      onSuccess: (response: ChangeQtyResponse, variables: ChangeQtyRequest) => {
        if (token === null) {
          updateToken(response.cart)
        }
        updateTotalCostDispatch(response.total_cost)
        dispatch(removeProductsFetching(variables.product))
      },
      onError: (error: ApiError, variables: ChangeQtyRequest) => {
        dispatch(removeProductsFetching(variables.product))
      },
      onMutate: (variables: ChangeQtyRequest) => {
        dispatch(addProductsFetching(variables.product))
      },
    },
  )

  const { mutateAsync: changeQtySampleMutateAsync } = useMutation(
    fetchChangeQtySample,
    {
      onSuccess: (response: ChangeQtyResponse, variables: ChangeQtyRequest) => {
        if (token === null) {
          updateToken(response.cart)
        }
        updateTotalCostDispatch(response.total_cost)
        dispatch(removeProductsFetching(variables.product))
      },
      onError: (error: ApiError, variables: ChangeQtyRequest) => {
        dispatch(removeProductsFetching(variables.product))
      },
      onMutate: (variables: ChangeQtyRequest) => {
        dispatch(addProductsFetching(variables.product))
      },
    },
  )

  const addToCartSomeProducts = useCallback(
    async (
      restProducts: Record<string, ProductSpecificationType>,
      tokenCart: string | null,
      afterToCart?: boolean,
      onSuccess?: () => void,
    ) => {
      setIsSomeProductsAddFetching(true)
      setIsSomeProductsAddSuccess(false)

      const products = { ...restProducts }
      const promises: Promise<ChangeQtyResponse>[] = []

      const _run = async (
        products: Record<string, ProductSpecificationType>,
        tokenCart: string | null,
      ) => {
        const token = tokenCart
        const productsKeys = Object.keys(products)
        if (productsKeys.length === 0) {
          return
        }

        // console.log("_run keys ", productsKeys.length)

        if (token === null) {
          // console.log("_run token null")
          const firstKey: string = productsKeys[0]
          let newToken: string | null = null

          if ((products[firstKey].quantity || 0) > 0) {
            const resAdded = await changeQtyProductMutateAsync({
              product: products[firstKey].uuid || "",
              quantity: products[firstKey].quantity || 0,
              cart: undefined,
            })
            if (!!resAdded) {
              // console.log("resAdded ", resAdded)
              newToken = resAdded.cart
            }
          } else {
            const resAdded = await changeQtySampleMutateAsync({
              product: products[firstKey].uuid || "",
              quantity: products[firstKey].sample || 0,
              cart: undefined,
            })
            if (!!resAdded) {
              newToken = resAdded.cart
            }
          }
          if (newToken !== null) {
            if (products[firstKey]) {
              delete products[firstKey]
            }
          }
          await _run(products, newToken)
        } else {
          // console.log("_run token not null")
          productsKeys.map((p) => {
            const currentSpeicification =
              specification !== null ? specification[p] || null : null
            if ((products[p].quantity || 0) > 0) {
              promises.push(
                (async () => {
                  return await changeQtyProductMutateAsync({
                    product: products[p].uuid || "",
                    quantity: products[p].quantity || 0, // здесь уже объединенное кол-во должно быть
                    cart: token,
                  })
                })(),
              )
            }
            if ((products[p].sample || 0) > 0) {
              promises.push(
                (async () => {
                  return await changeQtySampleMutateAsync({
                    product: products[p].uuid || "",
                    quantity:
                      (products[p].sample || 0) +
                      (currentSpeicification?.sample || 0),
                    cart: token,
                  })
                })(),
              )
            }
          })
        }
      }

      await _run(products, tokenCart)

      if (promises.length > 0) {
        // console.log("tokenCart ", tokenCart)
        // console.log("promises created ", promises)
        Promise.all(promises).finally(() => {
          setIsSomeProductsAddFetching(false)
          setIsSomeProductsAddSuccess(true)
          setIsSomeProductsAddFinish(true)
          setTimeout(() => {
            setIsSomeProductsAddFinish(false)
          }, TIMEOUT_SUCCESS)
          if (!!onSuccess) {
            onSuccess()
          }

          if (afterToCart) {
            void replace(ROUTES.cart)
          } else {
            void refetchCart()
          }
        })
        // console.log("promises completed ")
        // console.log("all completed")
      }
    },
    [
      changeQtyProductMutateAsync,
      changeQtySampleMutateAsync,
      specification,
      refetchCart,
      replace,
    ],
  )

  const clearCartMutation = useMutation(fetchClearCart, {
    onSuccess: () => {
      dispatch(clearCart())
      setIsFetching(false)
    },
    onError: (error: ApiError) => {
      console.log(error)
      if (error.status === 418) {
        setTokenStorage(null)
      }
      setIsFetching(false)
    },
    onMutate: () => {
      setIsFetching(true)
    },
  })

  const { mutateAsync: removeCartMutateAsync } = useMutation(fetchRemoveCart)

  const clearCartHandler = useCallback(() => {
    if (token !== null) {
      clearCartMutation.mutate({
        uid: token,
      })
    }
  }, [clearCartMutation, token])

  const updateProductPriceUnitHandle = useCallback(
    ({ uuid, priceUnit }: { uuid: string; priceUnit: number }) => {
      dispatch(updateProductPriceUnit({ uuid, priceUnit }))
    },
    [dispatch, updateProductPriceUnit],
  )

  const cartCost = useMemo(() => {
    let sum = 0
    if (specification !== null && datasource !== null) {
      for (const [key, itemSpec] of Object.entries(specification)) {
        if (!itemSpec) {
          continue
        }

        sum += !itemSpec?.isSampleRemoved
          ? (itemSpec?.sample || 0) *
            (!!datasource[key] && !!datasource[key].total_qty
              ? datasource[key]?.price || 0
              : 0)
          : 0

        sum += !itemSpec?.isRemoved
          ? (itemSpec?.quantity || 0) *
            (!!datasource[key] && !!datasource[key].total_qty
              ? datasource[key]?.price || 0
              : 0)
          : 0
      }
    }

    return sum
  }, [datasource, specification])

  const cartCount = useMemo(() => {
    let count = 0

    if (specification === null) {
      return count
    }
    for (const [, itemSpec] of Object.entries(specification)) {
      count += productQty(itemSpec) > 0 ? 1 : 0
      count += sampleQty(itemSpec) > 0 ? 1 : 0
    }
    return count
  }, [specification])

  const shippingDatesOrdered = useMemo(() => {
    const dates: string[] = []
    if (specification === null || datasource === null) {
      return []
    }

    for (const [key, itemSpec] of Object.entries(specification)) {
      const _productQty = productQty(itemSpec)
      const _sampleQty = sampleQty(itemSpec)
      if (_sampleQty === 0 && _productQty === 0) {
        continue
      }
      const entity: ProductType = (datasource || {})[key] || null
      if (!entity) {
        continue
      }
      const { shippingDate: date } = recalcShippingDate({
        product: {
          fastQty: entity?.fast_qty || 0,
          currentCount: _productQty + _sampleQty,
          totalQty: entity?.total_qty || 0,
        },
      })
      if (date !== null) {
        dates.push(dateToISOString(date))
      }
    }

    return [...dates]
      .reduce(
        (uniq: string[], item) =>
          uniq.includes(item) ? uniq : [...uniq, item],
        [],
      )
      .sort((a, b) => new Date(b).getTime() - new Date(a).getTime())
  }, [datasource, recalcShippingDate, specification])

  const onAuthHandle = useCallback(() => {
    if (!!user?.cart && token === null) {
      updateToken(user.cart)
    }

    if (!user?.cart && token !== null) {
      bindCartUserMutate({
        cart: token,
      })
    }

    if (!!user?.cart && token !== null && user.cart !== token) {
      getCartMutate(user.cart)
    }
  }, [bindCartUserMutate, getCartMutate, token, updateToken, user?.cart])

  const onNotAuthHandle = useCallback(() => {
    const tokenStorage = getTokenStorage()
    if (tokenStorage !== null) {
      updateToken(tokenStorage)
    }
  }, [updateToken])

  const removeSpecificationHandle = useCallback(
    (uuid: string) => {
      dispatch(removeProductSpecification({ uuid: uuid }))
    },
    [dispatch, removeProductSpecification],
  )

  const mergeProductsCarts = useCallback(() => {
    if (specificationMerge === null) {
      return
    }
    void addToCartSomeProducts(specificationMerge, notAuthCart.current)
  }, [addToCartSomeProducts, specificationMerge])

  const cleanMergeData = useCallback(() => {
    setHasProductsInAuth(false)
    setTimeout(() => {
      dispatch(setMergeProducts(null))
    }, 300)
  }, [dispatch, setMergeProducts])

  const setOrderHandle = useCallback(
    (payload: OrderThankType | null) => {
      if (payload === null) {
        dispatch(setOrder(payload))
        return
      }

      const createOffers = () => {
        const offers: OfferOrderThankType[] = []
        if (specification !== null && datasource !== null) {
          for (const [key, itemSpec] of Object.entries(specification)) {
            const product = datasource[key]
            if (product !== undefined) {
              offers.push({
                url: `${SITE_URL}${ROUTES.product}/${product.alias}`,
                count: productQty(itemSpec) + sampleQty(itemSpec),
                price: product.price || 0,
                currency: "RUB",
                name: product.name || "",
              })
            }
          }
        }
        return offers
      }
      dispatch(setOrder({ ...payload, offers: createOffers() }))
    },
    [dispatch, datasource, setOrder, specification],
  )

  const withOutDataSourceIds = useMemo(() => {
    const specificationIds = getEntityCartIds(specification)
    const datasourceIds = getEntityCartIds(datasource)

    return specificationIds.filter((sId) => !datasourceIds.includes(sId))
  }, [datasource, specification])

  const { products, samples } = useMemo(
    () => compareSpecificationProducts(specification, datasource),
    [specification, datasource],
  )

  useEffect(() => {
    if (isInitAuth) {
      if (isAuth) {
        onAuthHandle()
      } else {
        onNotAuthHandle()
      }

      setIsInit(true)
    }
  }, [isAuth, isInitAuth])

  // локальный пересчет total корзины
  useEffect(() => {
    dispatch(setProductsCost(cartCost))
  }, [cartCost, dispatch, setProductsCost])

  useEffect(() => {
    setIsFetching(isFetchingDataCart)
  }, [isFetchingDataCart])

  useEffect(() => {
    if (withOutDataSourceIds.length > 0) {
      fetchProductsSource({
        uuids: withOutDataSourceIds.join(","),
      })
    }
  }, [withOutDataSourceIds, fetchProductsSource])

  useEffect(() => {
    if (shippingDatesOrdered.length > 0) {
      dispatch(setNextShippingDate(dateToISOString(shippingDatesOrdered[0])))
      dispatch(
        setMinShippingDate(
          new Date(shippingDatesOrdered[shippingDatesOrdered.length - 1])
            .toISOString()
            .split("T")[0],
        ),
      )
    } else {
      dispatch(setNextShippingDate(null))
      dispatch(setMinShippingDate(null))
    }
  }, [dispatch, shippingDatesOrdered, setMinShippingDate, setNextShippingDate])

  useEffect(() => {
    setPickupDate(nextShippingDate !== null ? new Date(nextShippingDate) : null)
  }, [nextShippingDate])

  const contextValue = useMemo(
    () =>
      ({
        specification,
        updateSpecification: updateSpecificationDispatch,
        totalCost: totalCost + (shippingCost || 0),
        token,
        updateToken,
        cartCount: cartCount,
        cartCost: productsCost,
        clearCart: clearCartHandler,
        shareCart: shareCartHandler,
        isSharedLinkCopied: isCopied,
        addToCartSomeProducts: addToCartSomeProducts,
        isFetching,
        products,
        samples,
        updateProductPriceUnit: updateProductPriceUnitHandle,
        refetchCart: () => {
          void refetchCart()
        },
        notification: notificationContent,
        setNotification: setNotificationContent,
        updateTotalCost: updateTotalCostDispatch,
        shippingCost,
        productsFetching,
        minShippingDate:
          minShippingDate !== null ? new Date(minShippingDate) : null,
        nextShippingDate:
          nextShippingDate !== null ? new Date(nextShippingDate) : null,
        removeSpecification: removeSpecificationHandle,
        addProductInFetching: (uuid: string) => {
          dispatch(addProductsFetching(uuid))
        },
        removeProductInFetching: (uuid: string) => {
          dispatch(removeProductsFetching(uuid))
        },
        setOrder: setOrderHandle,
        order: order,
        formatProductsToSpecification,
        pickupDate: pickupDate,
        productsInAuth: productsMerge,
        specificationMerge: specificationMerge,
        hasProductsInAuth,
        isLoadingProductsInAuth,
        isMergingCart,
        isSomeProductsAddFinish,
        isSomeProductsAddFetching,
        isSomeProductsAddSuccess,
        mergeProductsCarts,
        cleanMergeData,
        isInitializeSpeicification: isInitFetchedCart.current,
      } as CartContextType),
    [
      specification,
      updateSpecificationDispatch,
      totalCost,
      token,
      updateToken,
      cartCount,
      clearCartHandler,
      shareCartHandler,
      isCopied,
      addToCartSomeProducts,
      isFetching,
      products,
      samples,
      updateProductPriceUnitHandle,
      refetchCart,
      notificationContent,
      setNotificationContent,
      productsCost,
      updateTotalCostDispatch,
      shippingCost,
      removeSpecificationHandle,
      productsFetching,
      dispatch,
      addProductsFetching,
      removeProductsFetching,
      order,
      formatProductsToSpecification,
      minShippingDate,
      nextShippingDate,
      pickupDate,
      productsMerge,
      hasProductsInAuth,
      isLoadingProductsInAuth,
      isMergingCart,
      isSomeProductsAddFinish,
      isSomeProductsAddFetching,
      isSomeProductsAddSuccess,
      mergeProductsCarts,
      cleanMergeData,
      setOrderHandle,
      specificationMerge,
    ],
  )

  return (
    <CartContext.Provider value={contextValue}>{children}</CartContext.Provider>
  )
}

export const useCart = (): CartContextType => {
  const cartContext = useContext(CartContext)

  if (cartContext === null) {
    throw new Error("Cart context have to be provided")
  }

  return {
    totalCost: cartContext.totalCost,
    cartCost: cartContext.cartCost,
    specification: cartContext.specification,
    token: cartContext.token,
    cartCount: cartContext.cartCount,
    isFetching: cartContext.isFetching,
    samples: cartContext.samples,
    products: cartContext.products,
    isSharedLinkCopied: cartContext.isSharedLinkCopied,
    minShippingDate: cartContext.minShippingDate,
    nextShippingDate: cartContext.nextShippingDate,
    notification: cartContext.notification,
    setNotification: cartContext.setNotification,
    productsFetching: cartContext.productsFetching,
    updateToken: cartContext.updateToken,
    updateSpecification: cartContext.updateSpecification,
    clearCart: cartContext.clearCart,
    shareCart: cartContext.shareCart,
    addToCartSomeProducts: cartContext.addToCartSomeProducts,
    updateProductPriceUnit: cartContext.updateProductPriceUnit,
    refetchCart: cartContext.refetchCart,
    updateTotalCost: cartContext.updateTotalCost,
    removeSpecification: cartContext.removeSpecification,
    addProductInFetching: cartContext.addProductInFetching,
    removeProductInFetching: cartContext.removeProductInFetching,
    setOrder: cartContext.setOrder,
    order: cartContext.order,
    formatProductsToSpecification: cartContext.formatProductsToSpecification,
    pickupDate: cartContext.pickupDate,
    productsInAuth: cartContext.productsInAuth,
    hasProductsInAuth: cartContext.hasProductsInAuth,
    specificationMerge: cartContext.specificationMerge,
    isLoadingProductsInAuth: cartContext.isLoadingProductsInAuth,
    isSomeProductsAddFinish: cartContext.isSomeProductsAddFinish,
    isSomeProductsAddFetching: cartContext.isSomeProductsAddFetching,
    isMergingCart: cartContext.isMergingCart,
    mergeProductsCarts: cartContext.mergeProductsCarts,
    cleanMergeData: cartContext.cleanMergeData,
    isSomeProductsAddSuccess: cartContext.isSomeProductsAddSuccess,
    isInitializeSpeicification: cartContext.isInitializeSpeicification,
  } as const
}
