import { useCallback, useEffect, useRef, useState } from "react"

function useThrottle(
  fn: (...args: any) => void,
  timeout?: number,
): [(...args: any) => any, boolean] {
  const [ready, setReady] = useState(true)
  const timerRef = useRef<number | undefined>(undefined)

  if (!fn || typeof fn !== "function") {
    throw new Error(
      "As a first argument, you need to pass a function to useThrottle hook.",
    )
  }

  const throttledFunction = useCallback(
    (...args) => {
      if (!ready) {
        return
      }

      setReady(false)
      fn(...args)
    },
    [ready, fn],
  )

  useEffect(() => {
    if (typeof window !== "undefined") {
      if (!ready) {
        timerRef.current = window.setTimeout(() => {
          setReady(true)
        }, timeout)

        return () => window.clearTimeout(timerRef.current)
      }
    } else {
      console.warn("useThrottle: window is undefined.")
    }
  }, [ready, timeout])

  return [throttledFunction, ready]
}

export { useThrottle }
