import {
  createContext,
  FC,
  useCallback,
  useContext,
  useEffect,
  useMemo,
  useState,
} from "react"
import { accountSlice } from "../store/reducers/accountSlice"
import { useAppDispatch, useAppSelector } from "./redux"
import { useAuth } from "./auth"
import { useMutation } from "react-query"
import {
  fetchAddToFavorites,
  fetchFavorites,
  fetchRemoveToFavorites,
} from "../api/favoritesAPI"

const getFavorites = (): string[] | null => {
  const favoritesKeys = localStorage.getItem("favorites")
  return favoritesKeys ? JSON.parse(favoritesKeys) : null
}

const setFavorites = (favoritesKeys: string[] | null) => {
  if (favoritesKeys == null) {
    return
  }
  if (favoritesKeys.length < 1) {
    localStorage.removeItem("favorites")
  } else {
    localStorage.setItem("favorites", JSON.stringify(favoritesKeys))
  }
}

type ContextPropsType = {
  add: (productUUID: string[]) => void
  remove: (productUUID: string) => void
  favoritesKeys: null | string[]
  quantity: number
  fetchingUUIds: string[]
}

const FavoritesContext = createContext<null | ContextPropsType>(null)

export const Provider: FC = ({ children }) => {
  const dispatch = useAppDispatch()
  const favoritesKeys = useAppSelector((state) => state.profile.favorites.keys)
  const toggleProgressList = useAppSelector(
    (state) => state.profile.favorites.toggleProgress,
  )
  const { isAuth, isInit } = useAuth()
  const { addToFavorites, removeFromFavorites, toggleFavoritesProgress } =
    accountSlice.actions

  const addFavoritesToState = useCallback(
    (products: string[]) => {
      dispatch(addToFavorites(products))
    },
    [addToFavorites, dispatch],
  )

  const bindFavorites = () => {
    const favoritesBeforeAuth = getFavorites()
    if (favoritesBeforeAuth !== null && favoritesBeforeAuth.length > 0) {
      addToFavoritesHandler(favoritesBeforeAuth)
      setFavorites([])
    }
  }

  const { mutate: addToFavoritesMutate } = useMutation(fetchAddToFavorites, {
    onMutate: (request) => {
      dispatch(
        toggleFavoritesProgress({
          isFetching: true,
          product: request.products,
        }),
      )
    },
    onSuccess: (response, request) => {
      addFavoritesToState(request.products.split(","))
    },
    onSettled: (data, error, request) => {
      dispatch(
        toggleFavoritesProgress({
          isFetching: false,
          product: request.products,
        }),
      )
    },
  })

  const { mutate: removeFromFavoritesMutate } = useMutation(
    fetchRemoveToFavorites,
    {
      onMutate: (request) => {
        dispatch(
          toggleFavoritesProgress({
            isFetching: true,
            product: request.product,
          }),
        )
      },
      onSuccess: (response, request) => {
        dispatch(removeFromFavorites(request.product))
      },
      onSettled: (data, error, request) => {
        dispatch(
          toggleFavoritesProgress({
            isFetching: false,
            product: request.product,
          }),
        )
      },
    },
  )

  const { mutate: getFavoritesUserMutate } = useMutation(fetchFavorites, {
    onSuccess: (response) => {
      addFavoritesToState(response !== null ? response.products : [])
      bindFavorites()
    },
  })

  const addToFavoritesHandler = useCallback(
    (productUUIDs: string[]) => {
      if (isAuth) {
        addToFavoritesMutate({ products: productUUIDs.join(",") })
      } else {
        addFavoritesToState(productUUIDs)
      }
    },
    [addFavoritesToState, addToFavoritesMutate, isAuth],
  )

  const removeFromFavoritesHandler = useCallback(
    (productUUID: string) => {
      if (isAuth) {
        removeFromFavoritesMutate({ product: productUUID })
      } else {
        localStorage.removeItem("favorites")
        dispatch(removeFromFavorites(productUUID))
      }
    },
    [dispatch, isAuth, removeFromFavorites, removeFromFavoritesMutate],
  )

  useEffect(() => {
    if (isInit) {
      if (!isAuth) {
        setFavorites(favoritesKeys)
      }
    }
  }, [favoritesKeys, isAuth, isInit])

  useEffect(() => {
    if (isInit) {
      if (!isAuth) {
        const savedFavorites = getFavorites()
        if (savedFavorites) {
          addFavoritesToState(savedFavorites)
        }
      } else {
        getFavoritesUserMutate()
      }
    }
  }, [
    addFavoritesToState,
    addToFavoritesHandler,
    dispatch,
    getFavoritesUserMutate,
    isAuth,
    isInit,
  ])

  const contextValue = useMemo(
    () => ({
      add: addToFavoritesHandler,
      remove: removeFromFavoritesHandler,
      favoritesKeys,
      quantity: favoritesKeys !== null ? favoritesKeys.length : 0,
      fetchingUUIds: toggleProgressList,
    }),
    [
      addToFavoritesHandler,
      removeFromFavoritesHandler,
      favoritesKeys,
      toggleProgressList,
    ],
  )

  return (
    <FavoritesContext.Provider value={contextValue}>
      {children}
    </FavoritesContext.Provider>
  )
}

export const useFavorites = (
  productsUUID?: string,
): Omit<ContextPropsType, "fetchingUUIds"> & {
  isFavorites: boolean
  isFetching: boolean
} => {
  const favoritesContext = useContext(FavoritesContext)

  if (favoritesContext === null) {
    throw new Error("User context have to be provided")
  }

  const [isFavorites, setIsFavorites] = useState<boolean>(false)
  const [isFetching, setIsFetching] = useState<boolean>(false)

  useEffect(() => {
    if (favoritesContext.favoritesKeys !== null) {
      setIsFavorites(
        favoritesContext.favoritesKeys.some((item) => item === productsUUID),
      )
    } else {
      setIsFavorites(false)
    }
  }, [favoritesContext.favoritesKeys, setIsFavorites, productsUUID])

  useEffect(() => {
    setIsFetching(
      favoritesContext.fetchingUUIds.some((item) => item === productsUUID),
    )
  }, [favoritesContext.fetchingUUIds, productsUUID])

  return {
    add: favoritesContext.add,
    remove: favoritesContext.remove,
    favoritesKeys: favoritesContext.favoritesKeys,
    isFavorites,
    quantity: favoritesContext.quantity,
    isFetching,
  } as const
}
