import {
  createContext,
  ReactNode,
  useCallback,
  useContext,
  useEffect,
  useMemo,
  useState,
} from "react"
import { useAppDispatch, useAppSelector } from "./redux"
import {
  LinkItemType,
  LocationExtendsType,
  LocationType,
  ShippingsType,
  StoreBannersType,
} from "../types/types"
import { appSlice, SettingsAppType } from "../store/reducers/appSlice"
import { settingsAppAPI } from "../api/settingsAPI"
import { fetchBanners } from "../api/bannersAPI"
import { BannerApiType } from "../../contracts/contracts"
import { useMutation, useQuery } from "react-query"
import {
  getLocationCookie,
  getLocationFormat,
  getLocationRegion,
  scrollBodyEnable,
  setLocationCookie,
} from "../utils/helpers"
import { useRouter } from "next/router"
import { fetchShippingMethods } from "../api/checkoutAPI"
import { usePreserveScroll } from "./preserveScroll"

export type AppContextPropsType = {
  location: LocationExtendsType | null
  settings: SettingsAppType | null
  banners: StoreBannersType | null
  updateLocation: (location: LocationType | null) => void
  updateBanners: (banners: BannerApiType[] | undefined) => void
  socials: null | Record<string, LinkItemType>
  updateIsLoadingPage: (value: boolean) => void
  isLoadingPage: boolean
  isDateToOnlyCompany: boolean
}
export type UseAppReturnType = AppContextPropsType

const AppContext = createContext<null | AppContextPropsType>(null)

export function Provider({ children }: { children?: ReactNode }): JSX.Element {
  const location = useAppSelector((state) => state.app.location)
  const settings = useAppSelector((state) => state.app.settings)
  const banners = useAppSelector((state) => state.app.banners)
  const isLoadingPage = useAppSelector((state) => state.app.isLoadingPage)
  const { setLocation } = appSlice.actions
  const { setSettings, setBanners, setIsLoadingPage } = appSlice.actions
  const dispatch = useAppDispatch()
  const [locationFormat, setLocationFormat] =
    useState<LocationExtendsType | null>(null)
  const [socials, setSocials] = useState<null | Record<string, LinkItemType>>(
    null,
  )
  const router = useRouter()

  usePreserveScroll()

  const { data: dataSettings } = settingsAppAPI.useSettingsApp()

  const { mutate: bannersMutate } = useMutation(() => fetchBanners(), {
    onSuccess: (data) => {
      updateBannersDispatch(data)
    },
  })

  const updateLocationDispatch = useCallback(
    (location: LocationType | null) => {
      dispatch(setLocation(location))
    },
    [dispatch, setLocation],
  )

  const updateBannersDispatch = useCallback(
    (banners: BannerApiType[] | undefined) => {
      if (!!banners) {
        dispatch(setBanners(banners))
      } else {
        dispatch(setBanners(null))
      }
    },
    [dispatch, setBanners],
  )

  const updateLocationHandle = useCallback(
    (location: LocationType | null) => {
      updateLocationDispatch(location)
      setLocationCookie(location)
    },
    [updateLocationDispatch],
  )

  const updateIsLoadingPage = useCallback(
    (value) => {
      dispatch(setIsLoadingPage(value))
    },
    [dispatch, setIsLoadingPage],
  )

  useEffect(() => {
    if (!!dataSettings) {
      dispatch(setSettings(dataSettings))
    }
  }, [dataSettings, dispatch, setSettings])

  useEffect(() => {
    const cLocation = getLocationCookie()
    if (!!cLocation) {
      updateLocationDispatch(cLocation)
    } else {
      updateLocationDispatch(null)
    }
  }, [updateLocationDispatch])

  useEffect(() => {
    if (location !== null) {
      setLocationFormat(getLocationFormat(location))
    } else {
      setLocationFormat(null)
    }
  }, [location])

  useEffect(() => {
    if (banners === null) {
      bannersMutate()
    }
  }, [banners, bannersMutate, dispatch, updateBannersDispatch])

  const [isDateToOnlyCompany, setIsDateToOnlyCompany] = useState(false)

  useQuery(
    ["shippingMethods", locationFormat],
    () =>
      locationFormat !== null
        ? fetchShippingMethods({
            regions: getLocationRegion(locationFormat) || "",
          })
        : null,
    {
      onSuccess: (data) => {
        setIsDateToOnlyCompany(
          !(data || []).find((m) => (m.alias as ShippingsType) === "courier"),
        )
      },
    },
  )

  useEffect(() => {
    const socials: Record<string, LinkItemType> = {}
    if (!!settings?.viber && settings.viber.length > 0) {
      socials["viber"] = {
        icon: "Viber",
        path: settings.viber,
      }
    }
    if (!!settings?.whatsApp && settings.whatsApp.length > 0) {
      socials["whatsApp"] = {
        icon: "WhatsApp",
        path: settings.whatsApp,
      }
    }
    if (!!settings?.telegram && settings.telegram.length > 0) {
      socials["telegram"] = {
        icon: "Telegram",
        path: settings.telegram,
      }
    }
    setSocials(socials)
  }, [settings?.telegram, settings?.viber, settings?.whatsApp])

  useEffect(() => {
    const onRouteChangeStart = () => {
      updateIsLoadingPage(true)
    }

    const onRouteChangeComplete = () => {
      updateIsLoadingPage(false)
      scrollBodyEnable()
    }

    router.events.on("routeChangeStart", onRouteChangeStart)
    router.events.on("routeChangeComplete", onRouteChangeComplete)

    return () => {
      router.events.off("routeChangeStart", onRouteChangeStart)
      router.events.off("routeChangeComplete", onRouteChangeComplete)
    }
  }, [updateIsLoadingPage, router])

  const contextValue = useMemo(
    () =>
      ({
        location: locationFormat,
        settings,
        updateLocation: updateLocationHandle,
        banners,
        updateBanners: updateBannersDispatch,
        socials,
        isLoadingPage,
        updateIsLoadingPage: updateIsLoadingPage,
        isDateToOnlyCompany: isDateToOnlyCompany,
      } as AppContextPropsType),
    [
      settings,
      locationFormat,
      updateLocationHandle,
      banners,
      updateBannersDispatch,
      socials,
      isLoadingPage,
      updateIsLoadingPage,
      isDateToOnlyCompany,
    ],
  )

  return (
    <AppContext.Provider value={contextValue}>{children}</AppContext.Provider>
  )
}

export const useApp = (): UseAppReturnType => {
  const appContext = useContext(AppContext)

  if (appContext === null) {
    throw new Error("App context have to be provided")
  }

  return {
    location: appContext.location,
    settings: appContext.settings,
    updateLocation: appContext.updateLocation,
    banners: appContext.banners,
    updateBanners: appContext.updateBanners,
    socials: appContext.socials,
    updateIsLoadingPage: appContext.updateIsLoadingPage,
    isLoadingPage: appContext.isLoadingPage,
    isDateToOnlyCompany: appContext.isDateToOnlyCompany,
  } as const
}
