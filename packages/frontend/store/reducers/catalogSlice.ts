import { createSlice, PayloadAction } from "@reduxjs/toolkit"
import {
  Category,
  CategoryByAreaResponse,
  Param,
  ParamsResponse,
  SortTypeResponse,
  TagResponse,
} from "../../../contracts/contracts"
import {
  CategoriesStateType,
  CategoriesTreeStateType,
  ICategoryTreeItem,
  ProductType,
  ShopType,
  SortBySelectedType,
  SortByType,
  TagPayloadType,
  TagType,
} from "../../types/types"
import {
  compareSortTypesWithIcon,
  createCategoriesTree,
  getAvailableParamsTree,
  getFilterParamsTree,
  normalizeCategoriesByAreas,
  normalizeValueToSort,
} from "../../utils/helpers"
import { EMPTY_DATA_PLACEHOLDER } from "../../utils/constants"

export const MANUAL_FILTER_KEYS = {
  store: "store",
}

export type IAvailableParamsKeys = Record<string, number | undefined>

export interface IFilterParamChild {
  uuid?: string
  name?: string
  product_qty?: number
  image?: string
  checked?: boolean
  parentUuid?: string
  disabled?: boolean
}
export type IFilterParamsChilds = Record<string, IFilterParamChild>
export type IFilterParamParent = Omit<Param, "values">
export type IFilterParamsParents = Record<string, IFilterParamParent>

export type IAvailableParam = IFilterParamParent & {
  params: IFilterParamsChilds
  checkedKeys: string[]
}
export type IAvailableParams = Record<string, IAvailableParam>

export type PriceRangeItemType = { min: number; max: number }
export type TogglePageMethodType = "switch" | "additional"

export const INITIAL_PAGE = 1
export const INITIAL_PER_PAGE = 18
export const INITIAL_PER_PAGE_WITH_BANNER = 18

export type PriceRangeType = {
  initial: PriceRangeItemType | null
  checked: PriceRangeItemType | null
}

const initialState = {
  businessAreas: null as Category[] | null,
  categories: null as CategoriesStateType | null,
  currentCategory: null as Category | null,
  categoriesByAreas: null as Record<string, ICategoryTreeItem> | null,
  tags: [] as TagType[],
  currentTag: {
    id: null as number | null,
    payload: null as TagPayloadType | null,
  },
  products: null as ProductType[] | null,
  currentProduct: null as ProductType | null,
  total: 0 as number,
  filter: {
    availableParams: null as IAvailableParams | null,
    params: {} as IFilterParamsParents & IFilterParamsChilds,
    checkedParamsKeysTotal: [] as string[],
    priceRange: {
      initial: null,
      checked: null,
    } as PriceRangeType,
    stores: {
      params: null as IAvailableParams | null,
    },
  },
  isEnabled: true as boolean,
  isFast: false as boolean,
  sortBy: null as SortByType,
  sortBySelected: null as SortBySelectedType,
  page: INITIAL_PAGE as number,
  itemsPerPage: null as number | null,
  isLoading: false,
  togglePageMethod: "switch" as TogglePageMethodType,
}

export const catalogSlice = createSlice({
  name: "catalog",
  initialState: initialState,
  reducers: {
    setCategories(state, action: PayloadAction<Category[]>) {
      const payload: CategoriesTreeStateType | null = createCategoriesTree(
        action.payload,
      )

      const treeSorted = Object.entries(payload?.tree || {})
        .sort(([, catA], [, catB]) => {
          const nameA = catA.name || ""
          const nameB = catB.name || ""
          return nameA > nameB ? 1 : nameA < nameB ? -1 : 0
        })
        .sort(([, catA], [, catB]) => {
          const weightA = catA.weight || ""
          const weightB = catB.weight || ""
          return weightA > weightB ? 1 : weightA < weightB ? -1 : 0
        })
        .map(([, category]) => category)

      state.categories = {
        compared: payload?.compared,
        tree: payload?.tree,
        treeSorted: treeSorted || [],
        fetched:
          action.payload &&
          (action.payload.reduce(
            (a, v) => ({ ...a, [v?.uuid || ""]: v }),
            {},
          ) as Record<string, Category>),
      }
    },
    setIsLoading(state, action: PayloadAction<boolean>) {
      state.isLoading = action.payload
    },
    setTags(state, action: PayloadAction<TagResponse>) {
      state.tags = action.payload
    },
    setProducts(state, action: PayloadAction<ProductType[]>) {
      if (state.togglePageMethod === "additional") {
        state.products = [...(state.products || []), ...action.payload]
      } else {
        state.products = action.payload
      }
    },
    setFilterParams(state, action: PayloadAction<ParamsResponse>) {
      state.filter.params = getFilterParamsTree(action)
    },
    appendFilterParams: (state, action: PayloadAction<ParamsResponse>) => {
      state.filter.params = {
        ...state.filter.params,
        ...getFilterParamsTree(action),
      }
    },
    setAvailableParams(
      state,
      action: PayloadAction<{
        compatibleParams: IAvailableParamsKeys | null
        queryParams?: string[]
        initialParams?: IAvailableParamsKeys | null
      } | null>,
    ) {
      if (action.payload !== null) {
        const { tree: availableTree } = getAvailableParamsTree(
          { ...state.filter.params },
          { ...action.payload.compatibleParams },
        )

        const tree = availableTree !== null ? { ...availableTree } : null
        if (tree !== null) {
          if (action.payload.queryParams !== undefined) {
            // проходимся по массиву активных значений фильтров
            for (const queryKey of action.payload.queryParams) {
              if (state.filter.params !== null) {
                // получаем по значению фильтра его родителя
                // в дереве уже сформированным в availableParams
                const activeParent =
                  tree[state.filter.params[queryKey]?.parentUuid || ""]
                // проходимся по значениям найденного родителя
                for (const paramKey of Object.keys(
                  activeParent?.params || {},
                )) {
                  const currentParam = activeParent.params[paramKey]

                  // currentParam.product_qty =
                  //   (action.payload.initialParams || {})[paramKey] || 0

                  if (queryKey === paramKey) {
                    currentParam.checked = true
                    activeParent.checkedKeys = [
                      ...activeParent.checkedKeys,
                      paramKey,
                    ].reduce((uniq: string[], item) => {
                      return uniq.includes(item) ? uniq : [...uniq, item]
                    }, [])
                  }
                }
              }
            }
          }
        }

        if (tree !== null) {
          Object.keys(tree || {}).map((key) => {
            const params = { ...tree[key].params }
            tree[key].params = {}
            Object.keys(params)
              .sort((a, b) => {
                const nameA = normalizeValueToSort(params[a].name)
                const nameB = normalizeValueToSort(params[b].name)
                return nameA > nameB ? 1 : nameA < nameB ? -1 : 0
              })
              .map((pKey) => {
                tree[key].params[pKey] = params[pKey]
              })
          })
        }
        state.filter.availableParams = tree
      } else {
        state.filter.availableParams = action.payload
      }
    },
    setCurrentCategory(state, action: PayloadAction<Category>) {
      state.currentCategory = action.payload
    },
    updateCheckedParams(
      state,
      action: PayloadAction<{
        keyFilter: string[]
        checked: boolean
      }>,
    ) {
      if (!!state.filter && !!state.filter.availableParams) {
        const { keyFilter, checked } = action.payload
        for (const key of keyFilter) {
          if (!state.filter.availableParams) {
            continue
          }

          const parent: IAvailableParam =
            state.filter.availableParams[
              state.filter.params[key]?.parentUuid || ""
            ]

          if (!parent) {
            continue
          }

          if (!!parent?.params[key]) {
            parent.params[key].checked = checked
          }

          if (checked) {
            parent.checkedKeys = [...(parent?.checkedKeys || []), key].reduce(
              (uniq: string[], item) => {
                return uniq.includes(item) ? uniq : [...uniq, item]
              },
              [],
            )
          } else {
            parent.checkedKeys =
              parent?.checkedKeys.filter((k) => k !== key) || []
          }
        }
      }
    },
    updateActualParams(
      state,
      action: PayloadAction<{
        fetchedParams: Record<string, number>
      }>,
    ) {
      if (!!action.payload.fetchedParams) {
        const paramsKeys = Object.keys(action.payload.fetchedParams)
        if (paramsKeys.length > 0) {
          if (state.filter.availableParams !== null) {
            const availableParamsCopied = {
              ...state.filter.availableParams,
            }
            paramsKeys.map((key) => {
              if (
                availableParamsCopied[
                  state.filter.params[key]?.parentUuid || ""
                ]?.params !== undefined
              ) {
                availableParamsCopied[
                  state.filter.params[key]?.parentUuid || ""
                ].params[key].product_qty = action.payload.fetchedParams[key]
              }
            })
          }
        }
      }
    },
    setInitialPriceRange(
      state,
      action: PayloadAction<PriceRangeItemType | null>,
    ) {
      state.filter.priceRange.initial = action.payload
    },
    setCheckedPriceRange(
      state,
      action: PayloadAction<PriceRangeItemType | null>,
    ) {
      state.filter.priceRange.checked = action.payload
    },
    setCheckedParamsKeysTotal(state, action: PayloadAction<string[]>) {
      state.filter.checkedParamsKeysTotal = action.payload
    },
    setIsEnabled(state, action: PayloadAction<boolean>) {
      state.isEnabled = action.payload
    },
    setIsFast(state, action: PayloadAction<boolean>) {
      state.isFast = action.payload
    },
    setSortBy(state, action: PayloadAction<SortTypeResponse>) {
      state.sortBy = compareSortTypesWithIcon(action.payload)
    },
    setSortBySelected(state, action: PayloadAction<null | string>) {
      state.sortBySelected = action.payload
    },
    setTotalCount(state, action: PayloadAction<number>) {
      state.total = action.payload
    },
    setCurrentPage(state, action: PayloadAction<number>) {
      state.page = action.payload
    },
    setItemsPerPage(state, action: PayloadAction<number>) {
      state.itemsPerPage = action.payload
    },
    setTogglePageMethod(state, action: PayloadAction<TogglePageMethodType>) {
      state.togglePageMethod = action.payload
    },
    setBusinessAreas(state, action: PayloadAction<Category[]>) {
      state.businessAreas = action.payload
    },
    setCategoriesByAreas(
      state,
      action: PayloadAction<CategoryByAreaResponse | undefined>,
    ) {
      if (!!action.payload) {
        if (state.categories !== null) {
          if (state.categories.fetched !== null) {
            state.categoriesByAreas = createCategoriesTree(
              normalizeCategoriesByAreas(
                action.payload,
                state.categories.fetched,
              ),
            ).tree
          } else {
            state.categoriesByAreas = null
          }
        } else {
          state.categoriesByAreas = null
        }
      }
    },
    setProduct(state, action: PayloadAction<ProductType | null>) {
      state.currentProduct = action.payload
    },
    setCurrentTagId(state, action: PayloadAction<number | null>) {
      state.currentTag.id = action.payload
    },
    setCurrentTagPayload(state, action: PayloadAction<TagPayloadType | null>) {
      state.currentTag.payload = action.payload
    },
    setFilterStores(state, action: PayloadAction<ShopType[]>) {
      const stores: IAvailableParams = {}
      stores["store"] = {
        uuid: MANUAL_FILTER_KEYS.store,
        name: "Склады",
        order: 0,
        checkedKeys: [],
        params: Object.fromEntries(
          action.payload.map((s) => {
            return [
              s.uuid,
              {
                uuid: s.uuid,
                name: s.address || EMPTY_DATA_PLACEHOLDER,
                parentUuid: MANUAL_FILTER_KEYS.store,
                product_qty: undefined,
                disabled: false,
                checked: false,
              },
            ]
          }),
        ),
      }
      state.filter.stores.params = stores
    },
    updateCheckedStores(
      state,
      action: PayloadAction<{
        keyFilter: string[]
        checked: boolean
      }>,
    ) {
      const { keyFilter, checked } = action.payload
      if (state.filter.stores.params !== null) {
        keyFilter.map((key) => {
          if (state.filter.stores.params !== null) {
            if (
              !!state.filter.stores.params[MANUAL_FILTER_KEYS.store].params[key]
            ) {
              state.filter.stores.params[MANUAL_FILTER_KEYS.store].params[
                key
              ].checked = checked

              if (checked) {
                state.filter.stores.params[
                  MANUAL_FILTER_KEYS.store
                ].checkedKeys = [
                  ...state.filter.stores.params[MANUAL_FILTER_KEYS.store]
                    .checkedKeys,
                  key,
                ].reduce((uniq: string[], item) => {
                  return uniq.includes(item) ? uniq : [...uniq, item]
                }, [])
              } else {
                state.filter.stores.params[
                  MANUAL_FILTER_KEYS.store
                ].checkedKeys = state.filter.stores.params[
                  MANUAL_FILTER_KEYS.store
                ].checkedKeys.filter((k) => k !== key)
              }
            }
          }
        })
      }
    },
  },
})

export default catalogSlice.reducer
