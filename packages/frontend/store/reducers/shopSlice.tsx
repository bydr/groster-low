import { createSlice, PayloadAction } from "@reduxjs/toolkit"
import { ShopType } from "../../types/types"

const initialState = {
  shops: null as ShopType[] | null,
}

export const shopSlice = createSlice({
  name: "shops",
  initialState,
  reducers: {
    setShops: (state, action: PayloadAction<ShopType[] | null>) => {
      state.shops = action.payload
    },
  },
})

export default shopSlice.reducer
