import { createSlice, PayloadAction } from "@reduxjs/toolkit"
import {
  RequestBanner,
  SettingsFetchedType,
} from "../../../contracts/contracts"
import { LocationType, StoreBannersType } from "../../types/types"
import { getBannerStringType } from "../../utils/helpers"

export type SettingsAppType = {
  shippingFastTime?: string
  shippingShift: number
  holidays: string[]
  minManyQuantity: number
  viber: string | null
  whatsApp: string | null
  telegram: string | null
}

const initialState = {
  settings: null as SettingsAppType | null,
  banners: null as StoreBannersType | null,
  openedModals: [] as string[],
  lastHistoryEntry: null as string | null,
  isLoadingPage: false as boolean | false,
  location: null as LocationType | null,
}

export const appSlice = createSlice({
  name: "app",
  initialState,
  reducers: {
    setSettings: (state, action: PayloadAction<SettingsFetchedType | null>) => {
      if (action.payload !== null) {
        state.settings = {
          shippingFastTime: action.payload.delivery_fast_time,
          shippingShift: action.payload.delivery_shift || 0,
          holidays: action.payload.holidays || [],
          minManyQuantity: action.payload.min_many_quantity || 0,
          telegram: action.payload.telegram || null,
          whatsApp: action.payload.whatsApp || null,
          viber: action.payload.viber || null,
        }
      } else {
        state.settings = action.payload
      }
    },
    setBanners: (state, action: PayloadAction<RequestBanner[] | null>) => {
      if (action.payload !== null) {
        const banners = {} as StoreBannersType
        action.payload.map((item) => {
          const typeString = getBannerStringType({
            type: item.type,
          })
          if (!!typeString) {
            if (banners[typeString] === undefined) {
              banners[typeString] = []
            }
            banners[typeString].push(item)
          }
        })
        state.banners = Object.keys(banners).length > 0 ? banners : null
      } else {
        state.banners = action.payload
      }
    },
    appendModal: (state, action: PayloadAction<string>) => {
      state.openedModals = [...state.openedModals, action.payload]
    },
    removeModal: (state, action: PayloadAction<string>) => {
      if (state.openedModals.length > 0) {
        state.openedModals = state.openedModals.filter(
          (m) => m !== action.payload,
        )
      }
    },
    setModals(state, action: PayloadAction<string[]>) {
      state.openedModals = action.payload
    },
    setLastHistoryEntry(state, action: PayloadAction<string | null>) {
      state.lastHistoryEntry = action.payload
    },
    setIsLoadingPage(state, action: PayloadAction<boolean>) {
      state.isLoadingPage = action.payload
    },
    setLocation(state, action: PayloadAction<LocationType | null>) {
      state.location = action.payload
    },
  },
})

export default appSlice.reducer
