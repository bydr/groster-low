import { createSlice, PayloadAction } from "@reduxjs/toolkit"
import {
  OrderThankType,
  ProductType,
  SpecificationItemType,
} from "../../types/types"
import { setTokenStorage } from "../../hooks/cart"

const initialState = {
  totalCost: 0 as number,
  productsCost: 0 as number,
  token: null as string | null,
  specification: null as Record<string, SpecificationItemType> | null,
  datasource: null as Record<string, ProductType> | null,
  promocode: null as string | null,
  discount: 0 as number,
  shippingCost: null as number | null,
  productsFetching: [] as string[] | [],
  order: null as OrderThankType | null,
  isInit: false as boolean,
  minShippingDate: null as string | null,
  nextShippingDate: null as string | null,
  merge: {
    products: null as ProductType[] | null,
    specification: null as Record<string, SpecificationItemType> | null,
  },
}

export const cartSlice = createSlice({
  name: "cart",
  initialState,
  reducers: {
    setToken(state, action: PayloadAction<string | null>) {
      state.token = action.payload
    },
    setSpecification(
      state,
      action: PayloadAction<SpecificationItemType[] | null>,
    ) {
      if (action.payload !== null) {
        let specification: Record<string, SpecificationItemType> = {}

        if (!!state.specification) {
          specification = { ...state.specification }
        }

        action.payload
          .sort((a, b) => {
            if (!a.parent) return -1
            if (!b.parent) return 1
            return 0
          })
          .map((p) => {
            if (p.uuid !== undefined) {
              // распределение обычных товаров
              // и товаров, являющихся дочерними комплекта
              if (!!p.parent) {
                if (specification[p.parent] !== undefined) {
                  specification[p.parent]["child"] = {
                    ...(specification[p.parent]?.child || {}),
                    ...{ [p.uuid]: p },
                  }
                }
              } else {
                specification[p.uuid] = { ...p }
              }
            }
          })

        state.specification = specification
      } else {
        state.specification = null
      }
    },
    updateSpecification(state, action: PayloadAction<SpecificationItemType>) {
      const { kit: payloadKit, ...payload } = action.payload

      if (!payload.uuid) {
        return
      }
      if (state.specification === null) {
        state.specification = {}
      }

      if (payload.parent !== undefined) {
        if (!!state.specification[payload.parent].child) {
          ;(state.specification[payload.parent].child || {})[payload.uuid] =
            payload
        }
      } else {
        state.specification[payload.uuid] = {
          ...state.specification[payload.uuid],
          ...payload,
        }

        if (!!state.specification[payload.uuid].child) {
          Object.keys(state.specification[payload.uuid].child || {}).map(
            (key) => {
              if (state.specification !== null) {
                const child = state.specification[payload.uuid || ""].child
                if (!!child && !!child[key]) {
                  if (payload.quantity !== undefined) {
                    child[key].quantity = payload.quantity
                  }
                  child[key].isRemoved = payload.isRemoved
                }
              }
            },
          )
        } else {
          if (payloadKit !== undefined && payloadKit.length > 0) {
            const childMaked = {}
            for (const kitId of payloadKit) {
              childMaked[kitId] = {
                uuid: kitId,
                quantity: payload.quantity,
              }
            }
            state.specification[payload.uuid]["child"] = childMaked
          }
        }
      }
    },
    updateProductPriceUnit(
      state,
      action: PayloadAction<{ uuid: string; priceUnit: number }>,
    ) {
      if (state.datasource !== null) {
        if (state.datasource[action.payload.uuid]) {
          state.datasource[action.payload.uuid].price = action.payload.priceUnit
        }
      }
    },
    removeProductSample(state, action: PayloadAction<{ uuid: string }>) {
      if (state.specification !== null) {
        if (state.specification[action.payload.uuid]) {
          state.specification[action.payload.uuid].sample = 0
        }
      }
    },
    removeProductSpecification(state, action: PayloadAction<{ uuid: string }>) {
      if (state.specification !== null) {
        if (!!state.specification[action.payload.uuid]) {
          delete state.specification[action.payload.uuid]
        }
      }
    },
    setTotalCost(state, action: PayloadAction<number>) {
      state.totalCost = action.payload
    },
    setProductsCost(state, action: PayloadAction<number>) {
      state.productsCost = action.payload
    },
    clearAllSpecifications(state) {
      state.specification = null
    },
    clearCart(state) {
      state.totalCost = 0
      state.productsCost = 0
      state.specification = {}
      state.datasource = null
      state.token = null
      setTokenStorage(null)
    },
    setPromocode(state, action: PayloadAction<string | null>) {
      state.promocode = action.payload
    },
    setDiscount(state, action: PayloadAction<number>) {
      state.discount = action.payload
    },
    setShippingCost(state, action: PayloadAction<number | null>) {
      state.shippingCost = action.payload
    },
    addProductsFetching(state, action: PayloadAction<string>) {
      state.productsFetching = [...state.productsFetching, action.payload]
    },
    removeProductsFetching(state, action: PayloadAction<string>) {
      state.productsFetching = state.productsFetching.filter(
        (id) => id !== action.payload,
      )
    },
    setOrder(state, action: PayloadAction<OrderThankType | null>) {
      state.order = action.payload
    },
    setIsInit(state, action: PayloadAction<boolean>) {
      state.isInit = action.payload
    },
    setNextShippingDate(state, action: PayloadAction<string | null>) {
      state.nextShippingDate = action.payload
    },
    setMinShippingDate(state, action: PayloadAction<string | null>) {
      state.minShippingDate = action.payload
    },
    setMergeProducts(state, action: PayloadAction<ProductType[] | null>) {
      state.merge.products = action.payload
    },
    setMergeSpecification(
      state,
      action: PayloadAction<Record<string, SpecificationItemType> | null>,
    ) {
      state.merge.specification = action.payload
    },
    removeProductsById: (state, action: PayloadAction<string[]>) => {
      const products =
        state.datasource !== null ? { ...state.datasource } : null
      if (products !== null) {
        for (const id of action.payload) {
          if (!!products[id]) {
            delete products[id]
          }
        }
      }
      state.datasource = products
    },
    appendDataSource: (state, action: PayloadAction<ProductType[] | null>) => {
      if (action.payload === null) {
        return
      }
      const datasource = {}
      for (const prod of action.payload) {
        datasource[prod.uuid || ""] = { ...prod }
      }
      state.datasource = { ...(state.datasource || {}), ...datasource }
    },
  },
})

export default cartSlice.reducer
