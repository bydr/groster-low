import { createSlice, PayloadAction } from "@reduxjs/toolkit"
import {
  AddressType,
  OrderList,
  OrderType,
  ParamsResponse,
  ProductDetailListType,
  ResponseCustomerDataList,
} from "../../../contracts/contracts"
import { setUser, User } from "../../hooks/auth"
import {
  LastOrderReturnType,
  PayerListItemType,
  ProductType,
  SpecificationItemType,
} from "../../types/types"
import {
  IAvailableParam,
  IAvailableParams,
  IAvailableParamsKeys,
  IFilterParamsChilds,
  IFilterParamsParents,
  TogglePageMethodType,
} from "./catalogSlice"
import {
  getAvailableParamsTree,
  getFilterParamsTree,
} from "../../utils/helpers"
import { OrderStateNumberType } from "../../components/Account/History/Orders/Order/State"

export const INITIAL_PAGE = 1
export const INITIAL_PER_PAGE = 10

const initialState = {
  favorites: {
    keys: null as string[] | null,
    products: [] as ProductDetailListType,
    toggleProgress: [] as string[] | [],
  },
  addresses: null as AddressType[] | null,
  payers: null as PayerListItemType[] | null,
  user: null as Required<User> | null,
  history: {
    orders: {
      total: 0 as number,
      items: null as Record<string, OrderType> | null,
      detail: {
        specification: null as Record<string, SpecificationItemType> | null,
        products: null as Record<string, ProductType> | null,
        samples: null as Record<string, ProductType> | null,
      },
    },
    products: {
      total: 0 as number,
      items: null as Record<string, ProductType> | null,
    },
    filter: {
      availableParams: null as IAvailableParams | null,
      params: {} as IFilterParamsParents & IFilterParamsChilds,
      checkedParamsKeysTotal: [] as string[],
      state: null as (OrderStateNumberType | -1) | null,
      category: null as string | null,
    },
    page: INITIAL_PAGE as number,
    itemsPerPage: INITIAL_PER_PAGE as number,
    togglePageMethod: "switch" as TogglePageMethodType,
  },
  priceList: {
    indeterminate: [] as string[] | [],
    selected: [] as string[] | [],
  },
  isAuth: false as boolean | false,
  isInit: false as boolean | false,
  lastOrder: null as LastOrderReturnType | null,
  customer: null as ResponseCustomerDataList | null,
}
export const accountSlice = createSlice({
  name: "profile",
  initialState,
  reducers: {
    addToFavorites(state, action: PayloadAction<string[]>) {
      const compare = () => {
        if (state.favorites.keys === null) {
          return null
        }
        const fav: string[] = [
          ...state.favorites.keys,
          ...action.payload,
        ].reduce(
          (uniq: string[], item) =>
            uniq.includes(item) ? uniq : [...uniq, item],
          [],
        )

        return !fav.length ? [] : fav
      }
      state.favorites.keys =
        state.favorites.keys !== null ? compare() : action.payload
    },
    removeFromFavorites(state, action: PayloadAction<string>) {
      if (state.favorites.keys === null) {
        return
      }
      const filter = state.favorites.keys.filter(
        (item: string) => item !== action.payload,
      )
      state.favorites.keys = !!filter.length ? filter : null
    },
    clearFavorites(state) {
      state.favorites.keys = null
      state.favorites.products = []
      state.favorites.toggleProgress = []
    },
    setFavoritesProducts(state, action: PayloadAction<ProductDetailListType>) {
      state.favorites.products = action.payload
    },
    setUserProfile(state, action: PayloadAction<User | null>) {
      if (action.payload !== null) {
        state.user = {
          email: action.payload.email || null,
          accessToken: action.payload.accessToken || null,
          phone: action.payload.phone || null,
          cart: action.payload.cart || null,
          refreshToken: action.payload.refreshToken || null,
          fio: action.payload.fio || null,
          isAdmin: !!action.payload.isAdmin,
        }
      } else {
        state.user = action.payload
      }

      setUser(action.payload)
    },
    setIsAuth(state, action: PayloadAction<boolean>) {
      state.isAuth = action.payload
    },
    setIsInit(state, action: PayloadAction<boolean>) {
      state.isInit = action.payload
    },
    toggleFavoritesProgress(
      state,
      action: PayloadAction<{ isFetching: boolean; product: string }>,
    ) {
      state.favorites.toggleProgress = action.payload.isFetching
        ? [...state.favorites.toggleProgress, action.payload.product]
        : state.favorites.toggleProgress.filter(
            (id) => id !== action.payload.product,
          )
    },
    setAddresses(state, action: PayloadAction<AddressType[] | null>) {
      state.addresses = action.payload
    },
    setPayers(state, action: PayloadAction<PayerListItemType[] | null>) {
      state.payers = action.payload
    },
    setHistoryFilterParams(state, action: PayloadAction<ParamsResponse>) {
      state.history.filter.params = getFilterParamsTree(action)
    },
    setHistoryFilterAvailableParams(
      state,
      action: PayloadAction<IAvailableParamsKeys | null>,
    ) {
      if (action.payload !== null) {
        state.history.filter.availableParams = getAvailableParamsTree(
          state.history.filter.params,
          action.payload,
        ).tree
      } else {
        state.history.filter.availableParams = action.payload
      }
    },
    updateCheckedParams(
      state,
      action: PayloadAction<{
        keyFilter: string[]
        checked: boolean
      }>,
    ) {
      if (!!state.history.filter.availableParams) {
        const { keyFilter, checked } = action.payload
        keyFilter.map((key) => {
          if (!!state.history.filter.availableParams) {
            const parent: IAvailableParam =
              state.history.filter.availableParams[
                state.history.filter.params[key].parentUuid || ""
              ]

            if (!!parent.params[key]) {
              parent.params[key].checked = checked
            }
            if (checked) {
              parent.checkedKeys = [...parent.checkedKeys, key].reduce(
                (uniq: string[], item) => {
                  return uniq.includes(item) ? uniq : [...uniq, item]
                },
                [],
              )
              state.history.filter.checkedParamsKeysTotal = [
                ...state.history.filter.checkedParamsKeysTotal,
                key,
              ].reduce((uniq: string[], item) => {
                return uniq.includes(item) ? uniq : [...uniq, item]
              }, [])
            } else {
              parent.checkedKeys = parent.checkedKeys.filter((k) => k !== key)
              state.history.filter.checkedParamsKeysTotal =
                state.history.filter.checkedParamsKeysTotal.filter(
                  (k) => k !== key,
                )
            }
          }
        })
      }
    },
    setSpecification(
      state,
      action: PayloadAction<SpecificationItemType[] | null>,
    ) {
      if (action.payload !== null) {
        let specification: Record<string, SpecificationItemType> = {}

        if (!!state.history.orders.detail.specification) {
          specification = { ...state.history.orders.detail.specification }
        }

        action.payload
          .sort((a, b) => {
            if (!a.parent) return -1
            if (!b.parent) return 1
            return 0
          })
          .map((p) => {
            if (!!p.uuid) {
              // распределение обычных товаров
              // и товаров, являющихся дочерними комплекта
              if (!!p.quantity && !!p.parent) {
                if (
                  !!specification[p.parent] &&
                  !!specification[p.parent]["child"]
                ) {
                  specification[p.parent]["child"] = {
                    ...specification[p.parent]["child"],
                    ...{ [p.uuid]: p },
                  }
                }
              } else {
                specification[p.uuid] = p
              }
            }
          })

        state.history.orders.detail.specification = specification
      } else {
        state.history.orders.detail.specification = null
      }
    },
    setProducts(
      state,
      action: PayloadAction<Record<string, ProductType> | null>,
    ) {
      state.history.orders.detail.products =
        action.payload !== null
          ? { ...state.history.orders.detail.products, ...action.payload }
          : action.payload
    },
    setSamples(
      state,
      action: PayloadAction<Record<string, ProductType> | null>,
    ) {
      state.history.orders.detail.samples =
        action.payload !== null
          ? { ...state.history.orders.detail.samples, ...action.payload }
          : action.payload
    },
    setCurrentPage(state, action: PayloadAction<number>) {
      state.history.page = action.payload
    },
    setItemsPerPage(state, action: PayloadAction<number | null>) {
      state.history.itemsPerPage = action.payload || INITIAL_PER_PAGE
    },
    setTotalOrders(state, action: PayloadAction<number>) {
      state.history.orders.total = action.payload
    },
    setTogglePageMethod(state, action: PayloadAction<TogglePageMethodType>) {
      state.history.togglePageMethod = action.payload
    },
    setFilterState(
      state,
      action: PayloadAction<(OrderStateNumberType | -1) | null>,
    ) {
      state.history.filter.state = action.payload
    },
    setFilterCategory(state, action: PayloadAction<string | null>) {
      state.history.filter.category = action.payload
    },
    setTotalProducts(state, action: PayloadAction<number>) {
      state.history.products.total = action.payload
    },
    setHistoryProducts(
      state,
      action: PayloadAction<{
        products: ProductType[] | null
        queueKeys: string[]
      }>,
    ) {
      // при выборе Категории в фильтрах
      // товары из этой категории должны выводиться первыми
      // получили нужны порядок ключей и хаотичный порядок товаров
      // сопоставляем порядок и данные товаров
      if (
        action.payload.products !== null &&
        action.payload.queueKeys.length > 0
      ) {
        const items = {}
        let products = [...action.payload.products]
        for (const uuid of action.payload.queueKeys) {
          const foundProduct = products.find((p) => p.uuid === uuid)
          if (!foundProduct) {
            continue
          }
          items[uuid] = { ...foundProduct }
          products = products.filter((p) => p.uuid !== uuid)
        }

        if (products.length > 0) {
          for (const prod of products) {
            if (!prod.uuid) {
              continue
            }
            items[prod.uuid] = { ...prod }
          }
        }

        state.history.products.items = items
      } else {
        state.history.products.items = null
      }
    },
    setOrders: (state, action: PayloadAction<OrderList[]>) => {
      if (action.payload !== null) {
        const orders = {}
        for (const order of action.payload) {
          if (order.uid == undefined) {
            continue
          }
          orders[order.uid] = { ...order }
        }
        state.history.orders.items = orders
      } else {
        state.history.orders.items = action.payload
      }
    },
    updateOrderState: (
      state,
      action: PayloadAction<{ uid: string; state: OrderStateNumberType }>,
    ) => {
      if (state.history.orders.items !== null) {
        const order = state.history.orders.items[action.payload.uid]
        if (order !== undefined) {
          state.history.orders.items[action.payload.uid].state =
            action.payload.state
        }
      }
    },
    setCategoriesPriceListIndeterminate: (
      state,
      action: PayloadAction<string[]>,
    ) => {
      state.priceList.indeterminate = action.payload
    },
    setCategoriesPriceListSelected: (
      state,
      action: PayloadAction<string[]>,
    ) => {
      state.priceList.selected = action.payload
    },
    setLastOrder: (
      state,
      action: PayloadAction<null | LastOrderReturnType>,
    ) => {
      state.lastOrder =
        action.payload !== null ? { ...action.payload, replacement: 3 } : null
    },
    setCustomer: (
      state,
      action: PayloadAction<null | ResponseCustomerDataList>,
    ) => {
      state.customer = action.payload
    },
  },
})

export default accountSlice.reducer
