import { combineReducers } from "redux"
import { configureStore } from "@reduxjs/toolkit"
import catalogReducer from "./reducers/catalogSlice"
import cartReducer from "./reducers/cartSlice"
import profileReducer from "./reducers/accountSlice"
import shopReducer from "./reducers/shopSlice"
import appReducer from "./reducers/appSlice"
import searchReducer from "./reducers/searchSlice"

const rootReducer = combineReducers({
  shop: shopReducer,
  catalog: catalogReducer,
  cart: cartReducer,
  profile: profileReducer,
  app: appReducer,
  search: searchReducer,
})

export const store = configureStore({
  reducer: rootReducer,
  devTools: true,
})

export type RootStateType = ReturnType<typeof rootReducer>
export type AppStoreType = typeof store
export type AppDispatch = AppStoreType["dispatch"]
