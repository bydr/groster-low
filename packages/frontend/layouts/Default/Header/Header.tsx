import Logo from "./Logo/Logo"
import { Button } from "../../../components/Button"
import {
  ButtonCatalogContainer,
  Controls,
  cssButtonSearchTrigger,
  cssHeaderCart,
  cssHeaderDefault,
  cssIsShowAutoComplete,
  RowBottom,
  RowTop,
  RowWrap,
  StyledHeader,
} from "./StyledHeader"
import { cssButtonMenuToggle } from "../../../components/Button/StyledButton"
import { Container } from "../../../styles/utils/StyledGrid"
import { CallInfo } from "./CallInfo"
import { AuthControls } from "./Auth/AuthControls"
import { useHeader } from "../../../hooks/header"
import dynamic, { DynamicOptions } from "next/dynamic"
import { PopupPropsType } from "../../../components/Catalog/Categories/Popup/Popup"
import { SelectLocation } from "./SelectLocation"
import { NavigationStatic } from "./NavigationStatic"
import { ButtonToFavorite } from "./ToFavorite"
import { ButtonToCart } from "./ToCart"
import { cx } from "@linaria/core"
import { useWindowSize } from "../../../hooks/windowSize"
import { breakpoints } from "../../../styles/utils/vars"
import { getBreakpointVal } from "../../../styles/utils/Utils"
import { SearchField } from "../../../components/Search/Field"
import { StyledSearchPopup } from "../../../components/Search/StyledSearch"
import { useSearch } from "../../../hooks/search"
import { FC } from "react"
import { useRouter } from "next/router"
import { ROUTES } from "../../../utils/constants"
import { Portal } from "reakit"
import { NotificationPropsType } from "../../../components/Notification/Notification"
import { BannerHeader } from "../../../components/Banners/Header"

const DynamicResponsiveMenu = dynamic((() =>
  import("./ResponsiveMenu/ResponsiveMenu").then(
    (mod) => mod.ResponsiveMenu,
  )) as DynamicOptions)

const DynamicPopup = dynamic((() =>
  import("../../../components/Catalog/Categories/Popup/Popup").then(
    (mod) => mod.Popup,
  )) as DynamicOptions<PopupPropsType>)

const Notification = dynamic((() =>
  import("../../../components/Notification/Notification").then(
    (mod) => mod.Notification,
  )) as DynamicOptions<NotificationPropsType>)

export const Header: FC = () => {
  const {
    isShowPopup,
    isShowMiniCart,
    isShowMenu,
    notificationCart,
    notificationAuth,
    setNotificationCart,
    setNotificationAuth,
    catalogShowedToggle,
    menuHide,
    menuShow,
    businessAreas,
    setIsShowMiniCart,
  } = useHeader()

  const {
    isShowPopup: isShowSearch,
    setIsShowPopup: setIsShowSearch,
    isShowAutoComplete,
  } = useSearch()

  const { width } = useWindowSize()

  const router = useRouter()

  return (
    <>
      <BannerHeader
        text={"Перейти на старую версию сайта"}
        href={"https://old.groster.me/"}
      />

      <StyledHeader
        data-isshow-catalog={isShowPopup}
        data-issshow-menu={isShowMenu}
        data-issshow-minicart={isShowMiniCart}
        className={cx(
          isShowAutoComplete && cssIsShowAutoComplete,
          cssHeaderDefault,
          router.pathname.includes(ROUTES.cart) && cssHeaderCart,
        )}
      >
        <Container>
          <RowTop>
            <RowWrap>
              <SelectLocation />
              <NavigationStatic />
            </RowWrap>
            <AuthControls />
          </RowTop>

          <RowBottom>
            <Logo />

            <ButtonCatalogContainer>
              <Button
                variant={"filled"}
                icon={isShowPopup ? "X" : "ViewBoxes"}
                data-isshow={isShowPopup}
                onClick={catalogShowedToggle}
              >
                Каталог
              </Button>
            </ButtonCatalogContainer>

            {width !== undefined && width > getBreakpointVal(breakpoints.lg) ? (
              <>
                <SearchField />
              </>
            ) : (
              <>
                <Button
                  variant={"box"}
                  icon={"Search"}
                  type={"submit"}
                  className={cx(cssButtonSearchTrigger)}
                  onClick={() => {
                    setIsShowSearch(true)
                  }}
                />
              </>
            )}

            <Controls>
              <CallInfo />
              <Button
                variant={"box"}
                icon={isShowMenu ? "X" : "Menu"}
                className={cssButtonMenuToggle}
                onClick={isShowMenu ? menuHide : menuShow}
              />
              <ButtonToFavorite />
              <ButtonToCart
                isShowMiniCart={isShowMiniCart}
                setIsShowMiniCart={(value: boolean) => {
                  if (!isShowPopup && window.outerWidth > 992) {
                    setIsShowMiniCart(value)
                  }
                }}
              />
            </Controls>

            {isShowMenu &&
              width !== undefined &&
              width <= getBreakpointVal(breakpoints.lg) && (
                <DynamicResponsiveMenu />
              )}
          </RowBottom>
        </Container>

        <DynamicPopup businessAreas={businessAreas} isShow={isShowPopup} />

        {notificationCart !== null && (
          <Notification
            isOpen={true}
            notification={notificationCart}
            setNotification={setNotificationCart}
            isAccentOutline
          />
        )}
        {notificationAuth !== null && (
          <Notification
            isOpen={true}
            notification={notificationAuth}
            setNotification={setNotificationAuth}
          />
        )}

        {isShowSearch && (
          <Portal>
            <StyledSearchPopup>
              <SearchField isResponsiveMode isFocusInit />
            </StyledSearchPopup>
          </Portal>
        )}
      </StyledHeader>
    </>
  )
}
