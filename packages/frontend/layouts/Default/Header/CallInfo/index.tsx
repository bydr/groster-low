import type { FC } from "react"
import { useEffect, useState } from "react"
import { Button } from "../../../../components/Button"
import { Typography } from "../../../../components/Typography/Typography"
import { Link } from "../../../../components/Link"
import { ModalDefaultPropsType } from "../../../../components/Modals/Modal"
import { Recall } from "../../../../components/Forms/Recall"
import { breakpoints } from "../../../../styles/utils/vars"
import { ListGroup } from "../../Footer/StyledFooter"
import LinkList from "../../../../components/LinkList/LinkList"
import { StyledCallContainer } from "./StyledCallInfo"
import { PopoverPropsType } from "../../../../components/Popover/Popover"
import { cssButtonClean } from "../../../../components/Button/StyledButton"
import dynamic, { DynamicOptions } from "next/dynamic"
import { useWindowSize } from "../../../../hooks/windowSize"
import { getBreakpointVal } from "../../../../styles/utils/Utils"
import { PHONES_ITEMS } from "../../../../utils/constants"

const DynamicPopover = dynamic((() =>
  import("../../../../components/Popover/Popover").then(
    (mod) => mod.Popover,
  )) as DynamicOptions<PopoverPropsType>)

const DynamicModal = dynamic((() =>
  import("../../../../components/Modals/Modal").then(
    (mod) => mod.Modal,
  )) as DynamicOptions<ModalDefaultPropsType>)

export const CallInfo: FC = () => {
  const { width } = useWindowSize()
  const [isResponsMode, setIsResponsMode] = useState<boolean>(false)

  useEffect(() => {
    setIsResponsMode(
      width !== undefined && width <= getBreakpointVal(breakpoints.lg),
    )
  }, [width])

  return (
    <>
      <StyledCallContainer>
        <DynamicPopover
          offset={[0, -2]}
          withHover={!isResponsMode}
          placement={"bottom-start"}
          disclosure={
            <Button className={cssButtonClean}>
              <Typography variant={"h3"}>
                {isResponsMode ? (
                  PHONES_ITEMS[0].title
                ) : (
                  <Link href={PHONES_ITEMS[0].path} variant={"black-to-purple"}>
                    {PHONES_ITEMS[0].title}
                  </Link>
                )}
              </Typography>
            </Button>
          }
        >
          <ListGroup>
            <LinkList
              items={
                isResponsMode
                  ? PHONES_ITEMS
                  : PHONES_ITEMS.filter((_, i) => i !== 0)
              }
            />
          </ListGroup>
        </DynamicPopover>

        <DynamicModal
          closeMode={"destroy"}
          disclosure={<Button>Заказать звонок</Button>}
          title={"Заказать звонок"}
        >
          <Recall />
        </DynamicModal>
      </StyledCallContainer>
    </>
  )
}
