import { styled } from "@linaria/react"
import { ButtonBase } from "../../../../components/Button/StyledButton"
import {
  getTypographyBase,
  Heading3,
} from "../../../../components/Typography/Typography"
import { colors } from "../../../../styles/utils/vars"
import { ListGroup } from "../../Footer/StyledFooter"
import { StyledPopoverDisclosure } from "../../../../components/Popover/StyledPopover"
import {
  StyledLinkList,
  StyledLinkListItem,
} from "../../../../components/LinkList/StyledLinkList"

export const StyledCallContainer = styled.div`
  display: flex;
  flex-direction: column;
  align-items: flex-end;

  ${ButtonBase} {
    ${getTypographyBase("p12")};
    color: ${colors.grayDark};
    padding: 0;
    line-height: 100%;
    background: transparent;
    margin: 0;

    &:hover {
      color: ${colors.black};
    }
  }

  ${ListGroup} {
    margin-bottom: 0;

    ${StyledLinkList} {
      margin-bottom: 0;
      ${StyledLinkListItem} {
        margin-bottom: 10px;

        &:last-child {
          margin-bottom: 0;
        }
      }
    }
  }

  ${StyledPopoverDisclosure} {
    background: transparent;
    padding: 0;
    margin: 0;

    > * {
      margin-bottom: 0;
    }
  }

  ${Heading3} {
    color: ${colors.black};
  }
`
