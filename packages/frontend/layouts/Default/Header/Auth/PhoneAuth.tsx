import { useContext, useEffect, useRef, VFC } from "react"
import { Button } from "../../../../components/Button"
import { StyledForm } from "../../../../components/Forms/StyledForms"
import { Controller, useForm } from "react-hook-form"
import { SingleError } from "../../../../components/Typography/Typography"
import {
  ApiError,
  RegisterDataRequestPhone,
} from "../../../../../contracts/contracts"
import { useMutation } from "react-query"
import { useMachine } from "@xstate/react/fsm"
import { ModalContext } from "../../../../components/Modals/Modal"
import { useAuth } from "../../../../hooks/auth"
import { BaseLoader } from "../../../../components/Loaders/BaseLoader/BaseLoader"
import { createAuthFSM } from "./fsm"
import { useTimer } from "../../../../hooks/timer"
import { CodeContainer } from "./CodeContainer"
import {
  fetchLoginPhone,
  fetchRegisterPhone,
  fetchSendCode,
} from "../../../../api/authAPI"
import { PhoneField } from "../../../../components/Field/PhoneField"
import { RULES_PHONE_VALIDATE } from "../../../../validations/phone"
import { PrivacyAgreement } from "../../../../components/Forms/PrivacyAgreement"

type SMSAuthForm = RegisterDataRequestPhone & {
  dataPrivacyAgreement: boolean
}

const authFormMachine = createAuthFSM("phoneAuth", "idInput")

export const PhoneAuth: VFC = () => {
  const [state, send] = useMachine(authFormMachine)
  const { login } = useAuth()
  const { resetTimer, timeFormatMinutes, setLifeTime, lifeTime } = useTimer()

  const {
    handleSubmit,
    register,
    getValues,
    formState: { errors },
    reset,
    control,
  } = useForm<SMSAuthForm>({
    defaultValues: {
      phone: "",
    },
  })

  const modalContext = useContext(ModalContext)

  const loginMutation = useMutation(fetchLoginPhone, {
    onMutate: () => {
      send("CHECK")
    },
    onSuccess: (result) => {
      login({
        phone: getValues("phone"),
        accessToken: result.access_token || null,
        refreshToken: result.refresh_token || null,
        cart: result.info?.cart || null,
        isAdmin: result.info?.is_admin,
        fio: result.info?.fio,
      })
      modalContext?.hide()
    },
    onError: (error: ApiError) => {
      send({
        type: "ROLLBACK",
        error: error.message,
      })
    },
  })

  const sendCodeMutation = useMutation(fetchSendCode, {
    onMutate: () => {
      send("CHECK")
    },
    onSuccess: (response) => {
      state.value === "register" && send("ROLLBACK")
      response.is_free ? send("REGISTER") : send("AUTH")
      setLifeTime(response.lifetime)
    },
    onError: (error: ApiError) => {
      send({
        type: "ROLLBACK",
        error: error.message,
      })
    },
  })

  const registerMutation = useMutation(fetchRegisterPhone, {
    onMutate: () => {
      send("CHECK")
    },
    onSuccess: (result) => {
      login({
        phone: getValues("phone"),
        accessToken: result.access_token || null,
        refreshToken: result.refresh_token || null,
        cart: result.info?.cart || null,
        isAdmin: result.info?.is_admin,
        fio: result.info?.fio,
      })
      modalContext?.hide()
    },
    onError: (error: ApiError) => {
      send({
        type: "ROLLBACK",
        error: error.message,
      })
    },
  })

  const codeRef = useRef<HTMLInputElement>(null)

  useEffect(() => {
    if (
      ["auth", "register", "registerLoading", "authLoading"].includes(
        state.value,
      )
    ) {
      codeRef.current?.focus()
    }
  }, [state.value])

  const onSubmit = handleSubmit((data) => {
    if (state.value === "idInput") {
      sendCodeMutation.mutate({ contact: getValues("phone") })
    } else {
      if (state.value === "auth") {
        loginMutation.mutate({
          phone: data.phone,
          code: data.code,
        })
      } else {
        registerMutation.mutate({
          phone: data.phone,
          code: data.code,
        })
      }
    }
  })

  return (
    <StyledForm onSubmit={onSubmit}>
      {!["idInput", "idInputLoading"].includes(state.value) && (
        <Button
          type={"button"}
          variant={"small"}
          icon={"ArrowLeft"}
          isHiddenBg
          onClick={(e) => {
            e.preventDefault()
            send("ROLLBACK")
            resetTimer()
            reset()
          }}
        >
          Назад
        </Button>
      )}

      <Controller
        rules={RULES_PHONE_VALIDATE}
        render={({ field, fieldState }) => (
          <PhoneField
            value={field.value}
            onValueChange={({ value }) => {
              field.onChange(value)
            }}
            errorMessage={fieldState?.error?.message}
            disabled={state.value === "register"}
          />
        )}
        control={control}
        name={"phone"}
      />

      {["auth", "register", "registerLoading", "authLoading"].includes(
        state.value,
      ) ? (
        <>
          <CodeContainer
            lifeTime={lifeTime || 0}
            timeFormatMinutes={timeFormatMinutes}
            resendHandler={() => {
              sendCodeMutation.mutate({ contact: getValues("phone") })
            }}
            placeholder={"Код из СМС"}
            sendMethodFSM={send}
            errorMessage={errors?.code?.message}
            register={register}
            ref={codeRef}
          />

          {["register", "registerDisabled", "registerLoading"].includes(
            state.value,
          ) ? (
            <>
              <PrivacyAgreement
                {...register("dataPrivacyAgreement", {
                  required: {
                    value: true,
                    message: "Нам необходимо ваше согласие",
                  },
                })}
                errorMessage={errors?.dataPrivacyAgreement?.message}
              />

              {state.context.error && (
                <SingleError>{state.context.error}</SingleError>
              )}
              <Button
                type={"submit"}
                variant={"filled"}
                size={"large"}
                disabled={Object.keys(errors).length > 0}
              >
                Зарегистрироваться
              </Button>
            </>
          ) : (
            <>
              {state.context.error && (
                <SingleError>{state.context.error}</SingleError>
              )}
              <Button
                type={"submit"}
                variant={"filled"}
                size={"large"}
                disabled={Object.keys(errors).length > 0}
              >
                Войти
              </Button>
            </>
          )}
        </>
      ) : (
        <>
          {state.context.error && (
            <SingleError>{state.context.error}</SingleError>
          )}

          <Button
            type={"button"}
            variant={"filled"}
            size={"large"}
            disabled={Boolean(errors.phone)}
            onClick={() => {
              void onSubmit()
            }}
          >
            Получить код
          </Button>
        </>
      )}

      {["idInputLoading", "registerLoading", "authLoading"].includes(
        state.value,
      ) && <BaseLoader />}
    </StyledForm>
  )
}
