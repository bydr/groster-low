import { FC, memo, useEffect } from "react"
import { Heading } from "./Heading"
import { Field } from "../../../../components/Field/Field"
import { Button } from "../../../../components/Button"
import { StyledForm } from "../../../../components/Forms/StyledForms"
import { SingleError } from "../../../../components/Typography/Typography"
import { BaseLoader } from "../../../../components/Loaders/BaseLoader/BaseLoader"
import { useMachine } from "@xstate/react/fsm"
import { createAuthFSM } from "./fsm"
import { CodeContainer } from "./CodeContainer"
import { useTimer } from "../../../../hooks/timer"
import { useForm } from "react-hook-form"
import {
  ApiError,
  AuthRequestUpdatePassword,
  RequestResetPassword,
} from "../../../../../contracts/contracts"
import { EMAIL_PATTERN } from "../../../../validations/email"
import { useMutation } from "react-query"
import {
  fetchResetPassword,
  fetchUpdatePassword,
} from "../../../../api/authAPI"
import { useAuth } from "../../../../hooks/auth"
import { PasswordField } from "../../../../components/Field/PasswordField"

type ResetPasswordFormType = RequestResetPassword & AuthRequestUpdatePassword

const recoveryPasswordFormMachine = createAuthFSM("phoneAuth", "idInput")

export const RecoveryPassword: FC = memo(() => {
  const [state, send] = useMachine(recoveryPasswordFormMachine)
  const { resetTimer, timeFormatMinutes, setLifeTime, lifeTime } = useTimer()
  const { login } = useAuth()

  const {
    handleSubmit: handleSubmitRecoveryPassword,
    register,
    getValues,
    formState: { errors },
  } = useForm<ResetPasswordFormType>({
    defaultValues: {
      email: "",
      password: "",
      code: "",
    },
  })

  useEffect(
    () => () => {
      resetTimer()
    },
    [resetTimer],
  )

  const resetPasswordMutation = useMutation(fetchResetPassword, {
    onMutate: () => {
      send("CHECK")
    },
    onSuccess: (response) => {
      send("REGISTER")
      setLifeTime(response.lifetime)
    },
    onError: (error: ApiError) => {
      send({
        type: "ROLLBACK",
        error: error.message,
      })
    },
  })

  const updatePasswordMutation = useMutation(fetchUpdatePassword, {
    onMutate: () => {
      send("CHECK")
    },
    onSuccess: (response) => {
      login(
        {
          email: getValues("email"),
          accessToken: response.access_token || "",
          cart: response.info?.cart || null,
          refreshToken: response.refresh_token || "",
        },
        {
          title: "Пароль изменен",
          message: `Вы успешно изменили пароль!\n Добро пожаловать!`,
        },
      )
    },
    onError: (error: ApiError) => {
      send({
        type: "ROLLBACK",
        error: error.message,
      })
    },
  })

  return (
    <>
      {["idInput", "idInputLoading"].includes(state.value) && (
        <>
          <Heading
            message={
              "Введите e-mail, на который будет отправлен код для сброса пароля"
            }
          />
        </>
      )}

      <StyledForm
        key={"RecoveryPassword"}
        onSubmit={handleSubmitRecoveryPassword((data) => {
          if (state.value === "idInput") {
            resetPasswordMutation.mutate({ email: data.email })
          } else {
            updatePasswordMutation.mutate({
              email: data.email,
              password: data.password,
              code: data.code,
            })
          }
        })}
      >
        <Field
          type={"email"}
          placeholder={"Email"}
          withAnimatingLabel
          errorMessage={errors?.email?.message}
          {...register("email", {
            required: {
              value: true,
              message: "Поле обязательно для заполнения",
            },
            pattern: {
              value: EMAIL_PATTERN,
              message: "Неверный формат email",
            },
          })}
        />

        {["registerLoading", "register"].includes(state.value) && (
          <>
            <PasswordField
              register={register}
              errorMessage={errors?.password?.message}
              placeholder={"Пароль"}
            />
            <CodeContainer
              placeholder={"Код из письма"}
              lifeTime={lifeTime || 0}
              timeFormatMinutes={timeFormatMinutes}
              resendHandler={() => {
                resetPasswordMutation.mutate({ email: getValues("email") })
              }}
              register={register}
              errorMessage={errors?.code?.message}
              sendMethodFSM={send}
            />
          </>
        )}

        {state.context.error && (
          <SingleError>{state.context.error}</SingleError>
        )}

        <Button
          type={"submit"}
          variant={"filled"}
          size={"large"}
          disabled={Object.keys(errors).length > 0}
        >
          {state.value === "idInput" ? "Продолжить" : "Сменить пароль"}
        </Button>

        {["idInputLoading", "registerLoading", "authLoading"].includes(
          state.value,
        ) && <BaseLoader />}
      </StyledForm>
    </>
  )
})

RecoveryPassword.displayName = "RecoveryPassword"
