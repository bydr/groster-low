import { assign, createMachine, StateMachine } from "@xstate/fsm"

type AuthFormEvents =
  | { type: "CHECK" }
  | { type: "ROLLBACK"; error: string }
  | { type: "REGISTER" }
  | { type: "AUTH" }
  | { type: "ENABLE" }
  | { type: "DISABLE" }

type AuthFormContext = {
  error?: string
}

type AuthFormState =
  | {
      value: "idInput"
      context: AuthFormContext
    }
  | {
      value: "idInputLoading"
      context: AuthFormContext
    }
  | {
      value: "register"
      context: AuthFormContext
    }
  | {
      value: "registerLoading"
      context: AuthFormContext
    }
  | {
      value: "auth"
      context: AuthFormContext
    }
  | {
      value: "authLoading"
      context: AuthFormContext
    }

export const createAuthFSM = (
  id: string,
  initialState: string,
): StateMachine.Machine<AuthFormContext, AuthFormEvents, AuthFormState> =>
  createMachine<AuthFormContext, AuthFormEvents, AuthFormState>(
    {
      id: id,
      initial: initialState,
      states: {
        idInput: {
          on: {
            CHECK: {
              target: "idInputLoading",
              actions: "clearError",
            },
          },
        },
        idInputLoading: {
          on: {
            REGISTER: "register",
            AUTH: "auth",
            ROLLBACK: {
              target: "idInput",
              actions: assign({
                error: (_, event) => event.error,
              }),
            },
          },
        },
        register: {
          on: {
            CHECK: {
              target: "registerLoading",
              actions: "clearError",
            },
            ROLLBACK: {
              target: "idInput",
              actions: "clearError",
            },
          },
        },
        registerLoading: {
          on: {
            ROLLBACK: {
              target: "register",
              actions: assign({
                error: (_, event) => event.error,
              }),
            },
          },
        },
        auth: {
          on: {
            CHECK: {
              target: "authLoading",
              actions: "clearError",
            },
          },
        },
        authLoading: {
          on: {
            ROLLBACK: {
              target: "auth",
              actions: assign({
                error: (_, event) => event.error,
              }),
            },
          },
        },
      },
      context: {
        error: undefined,
      },
    },
    {
      actions: {
        clearError: assign<AuthFormContext>({
          error: undefined,
        }),
      },
    },
  )
