import { ChangeEvent, forwardRef } from "react"
import { Button } from "../../../../components/Button"
import { ButtonProps } from "reakit"
import { Field } from "../../../../components/Field/Field"

export const CodeContainer = forwardRef<
  HTMLInputElement,
  Omit<ButtonProps, "ref"> & {
    lifeTime: number
    timeFormatMinutes: string
    resendHandler: () => void
    errorMessage?: string
    register: any
    placeholder?: string
    sendMethodFSM?: (state: "ENABLE" | "DISABLE") => void
  }
>(
  (
    {
      lifeTime,
      timeFormatMinutes,
      resendHandler,
      errorMessage,
      register,
      placeholder = "Код",
      sendMethodFSM,
      ...props
    },
    ref,
  ) => {
    return (
      <>
        <Field
          placeholder={placeholder}
          withAnimatingLabel
          errorMessage={errorMessage}
          ref={ref}
          {...register("code", {
            required: {
              value: true,
              message: "Поле обязательно для заполнения",
            },
            minLength: {
              value: 6,
              message: "Длина кода 6 символов",
            },
            maxLength: {
              value: 6,
              message: "Длина кода 6 символов",
            },
            onChange: (event: ChangeEvent<HTMLInputElement>) => {
              if (event.target.value.length === 6) {
                if (sendMethodFSM) {
                  sendMethodFSM("ENABLE")
                }
              } else {
                if (sendMethodFSM) {
                  sendMethodFSM("DISABLE")
                }
              }
            },
          })}
        />

        {lifeTime > 0 ? (
          <>
            <Button variant={"small"} {...props} disabled>
              Повтор через {timeFormatMinutes}
            </Button>
          </>
        ) : (
          <>
            <Button
              variant={"small"}
              onClick={(e) => {
                e.preventDefault()
                resendHandler()
              }}
              {...props}
            >
              Отправить повторно
            </Button>
          </>
        )}
      </>
    )
  },
)

CodeContainer.displayName = "CodeContainer"
