import type { FC } from "react"
import { Tab, TabList, TabPanel, Tabs } from "../../../../components/Tabs"
import { Heading } from "./Heading"
import { SignInSocials } from "./SignInSocials"
import { PhoneAuth } from "./PhoneAuth"
import { EmailAuth } from "./EmailAuth"

export const SignInForm: FC = () => {
  return (
    <>
      <Heading
        message={
          "Авторизуйтесь или зарегестрируйтесь, чтобы видеть свою историю закзов и получать персональные предложения"
        }
      />

      <SignInSocials />

      <Tabs>
        <TabList variant={"button"} aria-label="Метод входа">
          <Tab>По телефону</Tab>
          <Tab>По почте</Tab>
        </TabList>
        <TabPanel>
          <PhoneAuth />
        </TabPanel>
        <TabPanel>
          <EmailAuth />
        </TabPanel>
      </Tabs>
    </>
  )
}
