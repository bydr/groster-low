import type { FC } from "react"
import { ButtonAuthGroup } from "../StyledHeader"
import { ModalDefaultPropsType } from "../../../../components/Modals/Modal"
import { Button } from "../../../../components/Button"
import { SignInForm } from "./SignInForm"
import { WordWithInitials } from "../../../../styles/utils/WordWithInitials"
import { useAuth } from "../../../../hooks/auth"
import { cssButtonLogout } from "../../../../components/Button/StyledButton"
import { ROUTES } from "../../../../utils/constants"
import dynamic, { DynamicOptions } from "next/dynamic"

const DynamicModal = dynamic((() =>
  import("../../../../components/Modals/Modal").then(
    (mod) => mod.Modal,
  )) as DynamicOptions<ModalDefaultPropsType>)

export const AuthControls: FC<{ isShowButtonLogout?: boolean }> = ({
  isShowButtonLogout = true,
}) => {
  const { user, logout, authModalRef } = useAuth()
  return (
    <ButtonAuthGroup>
      {!user?.accessToken ? (
        <>
          <DynamicModal
            closeMode={"destroy"}
            hideOnClickOutside={false}
            disclosure={
              <Button ref={authModalRef} variant={"small"} icon={"Lock"}>
                Вход
              </Button>
            }
          >
            <SignInForm />
          </DynamicModal>
        </>
      ) : (
        <>
          <Button
            variant={"small"}
            icon={"User"}
            as={"a"}
            href={ROUTES.account}
          >
            {!user.fio ? (
              "Профиль"
            ) : (
              <>
                {user.fio.split(" ").map((f, i) => (
                  <WordWithInitials key={i} word={f} />
                ))}
              </>
            )}
          </Button>
          {isShowButtonLogout && (
            <>
              <Button
                variant={"small"}
                className={cssButtonLogout}
                onClick={() => {
                  logout()
                }}
              >
                Выход
              </Button>
            </>
          )}
        </>
      )}
    </ButtonAuthGroup>
  )
}
