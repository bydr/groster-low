import { FC, useContext, useEffect, useRef, useState } from "react"
import { useForm } from "react-hook-form"
import { Field } from "../../../../components/Field/Field"
import { Button } from "../../../../components/Button"
import { StyledForm } from "../../../../components/Forms/StyledForms"
import { useMutation } from "react-query"
import { EMAIL_PATTERN } from "../../../../validations/email"
import {
  SingleError,
  Typography,
  TypographyBase,
} from "../../../../components/Typography/Typography"
import { createAuthFSM } from "./fsm"
import { useMachine } from "@xstate/react/fsm"
import { BaseLoader } from "../../../../components/Loaders/BaseLoader/BaseLoader"
import {
  ModalContext,
  ModalDefaultPropsType,
} from "../../../../components/Modals/Modal"
import { useAuth } from "../../../../hooks/auth"
import {
  fetchCheckEmail,
  fetchLoginEmail,
  fetchRegisterEmail,
  fetchSendCode,
} from "../../../../api/authAPI"
import { useTimer } from "../../../../hooks/timer"
import { CodeContainer } from "./CodeContainer"
import { RecoveryPassword } from "./RecoveryPassword"
import { cssHidden } from "../../../../styles/utils/Utils"
import { styled } from "@linaria/react"
import { PasswordField } from "../../../../components/Field/PasswordField"
import {
  ApiError,
  RegisterDataRequestEmail,
} from "../../../../../contracts/contracts"
import { PrivacyAgreement } from "../../../../components/Forms/PrivacyAgreement"
import dynamic, { DynamicOptions } from "next/dynamic"

type EmailAuthForm = RegisterDataRequestEmail & {
  passwordConfirm?: string
  dataPrivacyAgreement: boolean
}

const ForgotPasswordWrapper = styled.div`
  display: flex;
  width: 100%;
  justify-content: flex-end;

  ${TypographyBase} {
    margin: 0;
  }
`

const DynamicModal = dynamic((() =>
  import("../../../../components/Modals/Modal").then(
    (mod) => mod.Modal,
  )) as DynamicOptions<ModalDefaultPropsType>)

const authFormMachine = createAuthFSM("phoneAuth", "idInput")

export const EmailAuth: FC = () => {
  const [state, send] = useMachine(authFormMachine)
  const { login } = useAuth()
  const { setLifeTime, lifeTime, timeFormatMinutes, resetTimer } = useTimer()
  const [isVisibleModal, setIsVisibleModal] = useState<boolean>(false)

  const {
    handleSubmit,
    register,
    formState: { errors },
    getValues,
  } = useForm<EmailAuthForm>()

  const modalContext = useContext(ModalContext)

  const sendCodeMutation = useMutation(fetchSendCode, {
    onMutate: () => {
      send("CHECK")
    },
    onSuccess: (response) => {
      state.value === "register" && send("ROLLBACK")
      response.is_free ? send("REGISTER") : send("AUTH")
      setLifeTime(response.lifetime)
    },
    onError: (error: ApiError) => {
      send({
        type: "ROLLBACK",
        error: error.message,
      })
    },
  })

  const loginMutation = useMutation(fetchLoginEmail, {
    onMutate: () => {
      send("CHECK")
    },
    onSuccess: (result) => {
      modalContext?.hide()
      login({
        email: getValues("email"),
        accessToken: result.access_token || null,
        refreshToken: result.refresh_token || null,
        cart: result.info?.cart || null,
        isAdmin: result.info?.is_admin,
        fio: result.info?.fio,
      })
    },
    onError: (error: ApiError) => {
      send({
        type: "ROLLBACK",
        error: error.message,
      })
    },
  })

  const registerMutation = useMutation(fetchRegisterEmail, {
    onMutate: () => {
      send("CHECK")
    },
    onSuccess: (result) => {
      modalContext?.hide()
      login({
        email: getValues("email"),
        accessToken: result.access_token || null,
        refreshToken: result.refresh_token || null,
        cart: result.info?.cart || null,
        isAdmin: result.info?.is_admin,
        fio: result.info?.fio,
      })
    },
    onError: (error: ApiError) => {
      send({
        type: "ROLLBACK",
        error: error.message,
      })
    },
  })

  const emailCheckMutation = useMutation(fetchCheckEmail, {
    onMutate: () => {
      send("CHECK")
    },
    onSuccess: (response, request) => {
      if (response.is_free) {
        send("REGISTER")
        sendCodeMutation.mutate({ contact: getValues("email") })
      } else {
        loginMutation.mutate({
          email: request, // email string
          password: getValues("password"),
        })
      }
    },
    onError: (error: ApiError) => {
      send({
        type: "ROLLBACK",
        error: error.message,
      })
    },
  })

  const myRefname = useRef<HTMLButtonElement>(null)

  useEffect(() => {
    if (isVisibleModal) {
      myRefname.current?.click()
    }
  }, [isVisibleModal])

  return (
    <>
      <StyledForm
        onSubmit={handleSubmit((data) => {
          if (state.value === "idInput") {
            emailCheckMutation.mutate(data.email)
          } else {
            registerMutation.mutate({
              email: data.email,
              password: data.password,
              code: data.code,
            })
          }
        })}
      >
        {!["idInput", "idInputLoading"].includes(state.value) && (
          <Button
            variant={"small"}
            icon={"ArrowLeft"}
            isHiddenBg
            onClick={(e) => {
              e.preventDefault()
              send("ROLLBACK")
              resetTimer()
            }}
          >
            Назад
          </Button>
        )}

        <Field
          placeholder={"Email"}
          withAnimatingLabel
          errorMessage={errors?.email?.message}
          {...register("email", {
            required: {
              value: true,
              message: "Поле обязательно для заполнения",
            },
            pattern: {
              value: EMAIL_PATTERN,
              message: "Неверный формат email",
            },
          })}
        />

        <PasswordField
          register={register}
          errorMessage={errors?.password?.message}
          placeholder={"Пароль"}
        />

        {["register", "registerLoading"].includes(state.value) ? (
          <>
            <PasswordField
              register={register}
              errorMessage={errors?.passwordConfirm?.message}
              placeholder={"Повторите пароль"}
              name={"passwordConfirm"}
              isConfirm
              comparisonVal={getValues("password")}
            />

            <CodeContainer
              lifeTime={lifeTime || 0}
              resendHandler={() => {
                sendCodeMutation.mutate({ contact: getValues("email") })
              }}
              timeFormatMinutes={timeFormatMinutes}
              placeholder={"Код из письма"}
              sendMethodFSM={send}
              errorMessage={errors?.code?.message}
              register={register}
            />

            <PrivacyAgreement
              {...register("dataPrivacyAgreement", {
                required: {
                  value: true,
                  message: "Нам необходимо ваше согласие",
                },
              })}
              errorMessage={errors?.dataPrivacyAgreement?.message}
            />

            {state.context.error && (
              <SingleError>{state.context.error}</SingleError>
            )}
            <Button
              type={"submit"}
              variant={"filled"}
              disabled={Object.keys(errors).length > 0}
            >
              Зарегистрироваться
            </Button>
          </>
        ) : (
          <>
            <ForgotPasswordWrapper>
              <Typography variant={"p12"}>
                <Button
                  variant={"small"}
                  isHiddenBg
                  type={"button"}
                  onClick={(e) => {
                    e.preventDefault()
                    setIsVisibleModal(true)
                  }}
                >
                  Забыли пароль?
                </Button>
              </Typography>
            </ForgotPasswordWrapper>

            {state.context.error && (
              <SingleError>{state.context.error}</SingleError>
            )}

            <Button
              type={"submit"}
              variant={"filled"}
              disabled={Object.keys(errors).length > 0}
            >
              Продолжить
            </Button>
          </>
        )}

        {["idInputLoading", "registerLoading", "authLoading"].includes(
          state.value,
        ) && <BaseLoader />}
      </StyledForm>

      {isVisibleModal && (
        <>
          <DynamicModal
            closeMode={"destroy"}
            hideOnClickOutside={false}
            disclosure={<Button ref={myRefname} className={cssHidden} />}
            title={"Восстановление пароля"}
            onClose={() => {
              setIsVisibleModal(false)
            }}
          >
            <RecoveryPassword />
          </DynamicModal>
        </>
      )}
    </>
  )
}
