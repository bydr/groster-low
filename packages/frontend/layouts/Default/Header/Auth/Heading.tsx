import { VFC } from "react"
import { Typography } from "../../../../components/Typography/Typography"
import { styled } from "@linaria/react"

const StyledHeadingForm = styled.div`
  width: 100%;
  margin-bottom: 20px;
`

export const Heading: VFC<{ message: string }> = ({ message }) => (
  <StyledHeadingForm>
    <Typography variant={"p12"}>{message}</Typography>
  </StyledHeadingForm>
)
