import type { FC } from "react"
import {
  ResponsiveMenuWrapper,
  StyledResponsiveMenu,
} from "./StyledResponsiveMenu"
import { CallInfo } from "../CallInfo"
import { StyledUtilOverlay } from "../../../../styles/utils/Utils"
import { AuthControls } from "../Auth/AuthControls"
import { NavigationStatic } from "../NavigationStatic"
import { SelectLocation } from "../SelectLocation"
import { ButtonToFavorite } from "../ToFavorite"

export const ResponsiveMenu: FC = () => {
  return (
    <ResponsiveMenuWrapper>
      <StyledResponsiveMenu>
        <NavigationStatic />
        <SelectLocation />
        <CallInfo />
        <AuthControls />
        <ButtonToFavorite />
      </StyledResponsiveMenu>
      <StyledUtilOverlay />
    </ResponsiveMenuWrapper>
  )
}
