import { styled } from "@linaria/react"
import { breakpoints, colors } from "../../../../styles/utils/vars"
import { StyledNav } from "../../../../components/Navigation/StyledNavigation"
import {
  StyledList,
  StyledListItem,
} from "../../../../components/List/StyledList"
import { ButtonAuthGroup, cssButtonSelectCity } from "../StyledHeader"
import {
  ButtonBase,
  cssButtonToFavorite,
} from "../../../../components/Button/StyledButton"
import { getTypographyBase } from "../../../../components/Typography/Typography"
import { StyledUtilOverlay } from "../../../../styles/utils/Utils"
import { StyledCallContainer } from "../CallInfo/StyledCallInfo"

export const ResponsiveMenuWrapper = styled.div`
  position: absolute;
  left: 0;
  top: calc(100% - 50px);
  height: calc(100vh - 50px);
  overflow-x: hidden;
  overflow-y: auto;
  width: 100%;
  padding-bottom: 40px;

  ${StyledUtilOverlay} {
    top: 80px;
  }
`
export const StyledResponsiveMenu = styled.div`
  background: ${colors.grayLight};
  display: grid;
  grid-template-columns: 1fr 50px;
  grid-auto-rows: max-content;
  grid-auto-flow: column;
  padding: 20px 30px;
  grid-row-gap: 20px;
  z-index: 5;
  position: relative;
  overflow-x: hidden;
  overflow-y: auto;
  height: 100%;

  ${StyledNav} {
    grid-row: 1;
    grid-column: 1/-1;

    ${StyledList} {
      display: flex;
      flex-direction: column;
      align-items: flex-start;
      justify-content: flex-start;

      ${StyledListItem} {
        margin-bottom: 16px;
      }
    }
  }

  .${cssButtonSelectCity} {
    ${getTypographyBase("p14")};
    color: ${colors.black};
    grid-row: 2;
    padding: 0;
    background: transparent;

    &:hover,
    &:active {
      background: transparent;
      border-color: transparent;
    }
  }

  ${StyledCallContainer} {
    grid-row: 3;
    display: flex;
    justify-content: flex-start;
    align-items: flex-start;

    @media (max-width: ${breakpoints.lg}) {
      display: flex;
    }
  }

  ${ButtonAuthGroup} {
    grid-row: 4;
    display: flex;
    justify-content: flex-start;
    align-items: center;
    margin-bottom: 0;

    ${ButtonBase} {
      margin-bottom: 0;
    }
  }

  ${ButtonBase}.${cssButtonToFavorite} {
    display: flex;
    grid-row: 4;
    grid-column: 2;
    margin-bottom: 0;
    background: transparent;
  }
`
