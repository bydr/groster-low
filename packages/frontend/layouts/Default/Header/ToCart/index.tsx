import { FC } from "react"
import { useCart } from "../../../../hooks/cart"
import { useRouter } from "next/router"
import { Wrapper } from "../../../../styles/utils/Utils"
import { LIMIT_COUNT_IN_CART_NUM, ROUTES } from "../../../../utils/constants"
import { Button } from "../../../../components/Button"
import { cssButtonToCart } from "../../../../components/Button/StyledButton"
import PushCounter from "../../../../styles/utils/PushCounter"
import dynamic, { DynamicOptions } from "next/dynamic"

const DynamicMiniCart = dynamic((() =>
  import("../../../../components/Cart/MiniCart").then(
    (mod) => mod.MiniCart,
  )) as DynamicOptions)

export const ButtonToCart: FC<{
  isShowMiniCart: boolean
  setIsShowMiniCart: (val: boolean) => void
}> = ({ isShowMiniCart, setIsShowMiniCart }) => {
  const { cartCount } = useCart()
  const { pathname } = useRouter()

  return (
    <>
      <Wrapper
        onMouseEnter={() => {
          if ([ROUTES.cart, ROUTES.checkout].includes(pathname)) {
            return
          }
          setIsShowMiniCart(true)
        }}
        onMouseLeave={() => {
          setIsShowMiniCart(false)
        }}
      >
        <Button
          as={"a"}
          variant={"box"}
          icon={"ShoppingCart"}
          className={cssButtonToCart}
          href={ROUTES.cart}
        >
          {cartCount !== null && cartCount > 0 && (
            <PushCounter>
              {cartCount > LIMIT_COUNT_IN_CART_NUM
                ? `${LIMIT_COUNT_IN_CART_NUM}+`
                : cartCount}
            </PushCounter>
          )}
        </Button>
        {isShowMiniCart && <DynamicMiniCart />}
      </Wrapper>
    </>
  )
}
