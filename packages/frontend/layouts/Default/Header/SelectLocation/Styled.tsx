import { styled } from "@linaria/react"
import { PopoverContainer } from "../../../../components/Popover/StyledPopover"
import { TypographyBase } from "../../../../components/Typography/Typography"
import { ButtonGroup } from "../../../../components/Button/StyledButton"

export const StyledSelectLocation = styled.div`
  position: relative;

  ${PopoverContainer} {
    ${TypographyBase} {
      margin-bottom: 10px;
    }

    [role="dialog"] {
      min-width: 260px;
    }
  }

  ${ButtonGroup} {
    > * {
      flex: 1;
    }
  }
`
