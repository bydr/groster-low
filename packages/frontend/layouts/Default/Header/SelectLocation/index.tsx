import { FC, useEffect, useState } from "react"
import { useApp } from "../../../../hooks/app"
import { COOKIE_LOCATION_DEFAULT } from "../../../../utils/constants"
import { Button } from "../../../../components/Button"
import { Typography } from "../../../../components/Typography/Typography"
import { ButtonGroup } from "../../../../components/Button/StyledButton"
import { cssButtonSelectCity } from "../StyledHeader"
import { StyledSelectLocation } from "./Styled"
import { LocationList } from "../../../../components/LocationList"
import { getLocationCookie, setLocationCookie } from "../../../../utils/helpers"
import dynamic, { DynamicOptions } from "next/dynamic"
import { ModalDefaultPropsType } from "../../../../components/Modals/Modal"
import { PopoverPropsType } from "../../../../components/Popover/Popover"

const DynamicPopover = dynamic((() =>
  import("../../../../components/Popover/Popover").then(
    (mod) => mod.Popover,
  )) as DynamicOptions<PopoverPropsType>)

const DynamicModal = dynamic((() =>
  import("../../../../components/Modals/Modal").then(
    (mod) => mod.Modal,
  )) as DynamicOptions<ModalDefaultPropsType>)

export const SelectLocation: FC = () => {
  const { location, updateLocation } = useApp()
  const [isShow, setIsShow] = useState<boolean>(false)
  const [isShowOther, setIsShowOther] = useState<boolean>(false)

  useEffect(() => {
    if (!getLocationCookie()) {
      setLocationCookie(COOKIE_LOCATION_DEFAULT)
      setIsShow(true)
    }
  }, [location])

  return (
    <>
      <StyledSelectLocation>
        <DynamicPopover
          disclosure={<Button />}
          isShow={isShow}
          size={"default"}
          isHiddenDisclosure
          hideOnClickOutside={false}
        >
          <Typography variant={"p14"}>
            Ваш город {COOKIE_LOCATION_DEFAULT.city}?
          </Typography>
          <ButtonGroup>
            <Button
              variant={"filled"}
              size={"small"}
              onClick={() => {
                updateLocation(getLocationCookie() || null)
                setIsShow(false)
              }}
            >
              Да
            </Button>
            <DynamicModal
              title={"Выберите свой город"}
              variant={"rounded-50"}
              closeMode={"destroy"}
              disclosure={
                <Button
                  variant={"outline"}
                  size={"small"}
                  onClick={() => {
                    setIsShowOther(true)
                  }}
                >
                  Другой
                </Button>
              }
              isShowModal={isShowOther}
            >
              <LocationList
                currentLocation={location || undefined}
                onSelectLocation={(location) => {
                  updateLocation(location)
                  setIsShow(false)
                }}
              />
            </DynamicModal>
          </ButtonGroup>
        </DynamicPopover>
        <DynamicModal
          closeMode={"destroy"}
          title={"Выберите свой город"}
          variant={"rounded-50"}
          disclosure={
            <Button
              variant={"small"}
              icon={"AngleBottom"}
              iconPosition={"right"}
              className={cssButtonSelectCity}
            >
              {!!location?.city ? location.city : "Город"}
            </Button>
          }
        >
          <LocationList
            currentLocation={location || undefined}
            onSelectLocation={(location) => {
              updateLocation(location)
            }}
          />
        </DynamicModal>
      </StyledSelectLocation>
    </>
  )
}
