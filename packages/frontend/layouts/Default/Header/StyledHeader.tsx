import { styled } from "@linaria/react"
import {
  ButtonBase,
  ButtonBox,
  ButtonSmall,
} from "../../../components/Button/StyledButton"
import {
  getTypographyBase,
  InitialsBody,
  InitialsWord,
} from "../../../components/Typography/Typography"
import {
  breakpoints,
  colors,
  transitionTimingFunction,
} from "../../../styles/utils/vars"
import { css } from "@linaria/core"
import { cssIcon } from "../../../components/Icon"
import { Container, RowGrid } from "../../../styles/utils/StyledGrid"
import { StyledLogo } from "./Logo/Logo"
import { StyledList, StyledListItem } from "../../../components/List/StyledList"
import { StyledBreadcrumbs } from "../../../components/Breadcrumbs/StyledBreadcrumbs"
import { StyledLinkBase } from "../../../components/Link/StyledLink"
import { StyledCallContainer } from "./CallInfo/StyledCallInfo"

export const cssScrollDown = css``
export const cssScrollUp = css``
export const cssIsSticky = css``

export const cssButtonSearchTrigger = css`
  display: none !important;
  margin: 0;
`
export const cssButtonSelectCity = css`
  &${ButtonSmall} {
    background: ${colors.grayLight};
    color: ${colors.grayDark};
    margin: 0;
    display: inline-flex;
    justify-content: space-between;
    width: auto;
    text-align: left;

    * {
      text-align: inherit;
    }

    .${cssIcon} {
      fill: ${colors.brand.purple};
    }
  }
`

export const cssHeaderDefault = css``
export const cssHeaderCart = css``

export const cssIsShowAutoComplete = css``

export const Controls = styled.div`
  background: ${colors.white};
  position: relative;
  z-index: 1;
  border-radius: 50px 0 0 50px;
  box-shadow: 65px 0 0 0 ${colors.white}, 0 0 0 65px ${colors.grayLight},
    170px 0px 0 0px ${colors.white}, 100px 0px 0 65px ${colors.grayLight};
  width: auto;
  display: inline-grid;
  grid-template-columns: 1fr 48px 48px;
  grid-column-gap: 15px;
  padding: 22px 0 22px 30px;
  align-items: center;
  justify-items: end;
  //min-width: 75%;
  grid-column: -1;

  ${ButtonBox} {
    margin: 0;
  }
  > *:first-child {
    margin-right: 20px;
  }
`
export const ButtonCatalogContainer = styled.div``
export const ButtonAuthGroup = styled.div`
  width: 100%;
  display: flex;
  justify-content: flex-end;
  z-index: 2;
  margin-bottom: 1rem;

  > ${ButtonBase} {
    background: ${colors.white};
    border-color: transparent;
    color: ${colors.grayDark};
    border-radius: 0;

    .${cssIcon} {
      fill: ${colors.brand.purple};
    }

    &:first-child {
      border-right: 2px solid ${colors.grayLight};
      border-radius: 50px 0 0 50px;
    }
    &:last-child {
      border-right-color: transparent;
      border-radius: 0 50px 50px 0;
    }
    &:first-child:last-child {
      border-right-color: transparent;
      border-radius: 50px;
    }

    &:hover,
    &:active {
      border-color: transparent;
      background: ${colors.brand.yellow};
      color: ${colors.brand.purple};
    }
  }

  ${InitialsWord} {
    &:first-child {
      ${InitialsBody} {
        display: inline;
      }
    }
  }
`

export const RowTop = styled(RowGrid)`
  grid-template-columns: 8fr 4fr;
  align-items: center;
  gap: 24px;
  z-index: 6;
  justify-content: flex-start;
  justify-items: flex-start;
  position: relative;

  ${StyledList} {
    ${getTypographyBase("p12")};
  }

  @media (max-width: ${breakpoints.lg}) {
    display: none;
  }
`
export const RowWrap = styled.div`
  display: inline-flex;
  flex: 1;
  align-items: center;
  gap: 10px 30px;
  position: relative;
`
export const RowBottom = styled(RowGrid)`
  grid-template-columns: 1fr 2fr 5fr 4fr;
  grid-column-gap: 24px;
  justify-items: end;
  align-items: center;
  z-index: 5;
  position: relative;

  @media (max-width: ${breakpoints.xl}) {
    grid-template-columns: 1fr 2fr 4fr 5fr;
  }

  @media (max-width: ${breakpoints.lg}) {
    grid-template-columns: repeat(4, 1fr) 50px;
    justify-items: initial;
    gap: 10px 10px;
    position: initial;
  }
`

export const StyledHeader = styled.header`
  padding-bottom: 10px;
  padding-top: 10px;
  z-index: 8;
  top: 0;
  background: ${colors.white};
  transition: transform 0.4s ${transitionTimingFunction};
  border-bottom: 1px solid transparent;
  will-change: transform;
  position: sticky;
  left: 0;
  right: 0;

  .${cssScrollDown} & {
    &.${cssHeaderDefault} {
      &:not([data-isshow-catalog="true"]):not([data-issshow-menu="true"]) {
        &:not(.${cssIsShowAutoComplete}) {
          transform: translateY(calc(-100% - 65px));
        }
      }
    }
  }

  .${cssScrollUp} & {
    border-color: ${colors.gray};

    ${Controls} {
      box-shadow: none;
      background: ${colors.grayLight};

      @media (max-width: ${breakpoints.md}) {
        background: transparent;
      }
    }

    &:before {
      bottom: 10px;
      top: auto;
      height: 92px;
      z-index: 5;
    }
  }

  &:before,
  &:after {
    content: "";
    position: absolute;
    left: 50%;
    margin-left: 500px;
    right: 0;
  }

  &:before {
    top: 0;
    background: ${colors.grayLight};
    bottom: -55px;
  }

  &:after {
    top: 56px;
    bottom: 10px;
    background: ${colors.white};
  }

  > ${Container} {
    position: relative;
    z-index: 5;
  }

  ${StyledLogo} {
    max-width: 172px;
    width: 172px;

    @media (max-width: ${breakpoints.xs}) {
      width: 140px;
    }
  }

  ${ButtonCatalogContainer} {
    display: flex;
    width: 100%;
    justify-content: flex-end;

    ${ButtonBase} {
      margin: 0;
      height: 48px;
      padding-right: 28px;
      padding-left: 28px;
      text-transform: uppercase;
      letter-spacing: 1px;
      font-weight: bold;

      .${cssIcon} {
        + span {
          margin-left: 14px;
        }
      }
    }
  }

  ${StyledList} {
    gap: 0 24px;
    ${StyledListItem} {
      ${StyledLinkBase} {
        color: ${colors.grayDark};

        &:hover,
        &:active {
          color: ${colors.black};
        }
      }
    }
  }

  ${Controls} {
    grid-column: 4;
    z-index: 1;
  }

  + * {
    position: relative;
  }

  &[data-isshow-catalog="true"] {
    background: ${colors.grayLight};

    &:before {
      background: ${colors.white};
      //z-index: 5;
    }

    &:after {
      background: ${colors.grayLight};
      //z-index: 5;
    }

    ${Controls} {
      background: ${colors.grayLight};
      box-shadow: 65px 0 0 0 ${colors.grayLight}, 0 0 0 65px ${colors.white};
    }

    ${ButtonAuthGroup} {
      ${ButtonBase} {
        background: ${colors.grayLight};
      }
    }

    ${ButtonCatalogContainer} {
      ${ButtonBase} {
        background: ${colors.white};
        color: ${colors.brand.purple};
      }
    }

    ${RowBottom} {
      //z-index: 5;
    }
  }

  &[data-issshow-menu="true"] {
    background: ${colors.grayLight};

    ${Controls} {
      background: ${colors.grayLight};
    }

    ~ * {
      ${StyledBreadcrumbs} {
        z-index: 3;
      }
    }
  }

  &[data-issshow-menu="true"],
  &[data-issshow-minicart="true"],
  &[data-isshow-catalog="true"] {
    z-index: 6;
  }

  &.${cssHeaderCart} {
    transform: none !important;
    border: none !important;
    position: relative !important;
  }

  @media (max-width: ${breakpoints.xl}) {
    ${Controls} {
      ${ButtonBox} {
        margin-right: 16px;
        &:last-child {
          margin-right: 0;
        }
      }
    }
  }

  @media (max-width: ${breakpoints.lg}) {
    padding-top: 22px;

    ${StyledCallContainer} {
      display: none;
    }
    .${cssButtonSearchTrigger} {
      display: flex !important;
    }

    ${Controls} {
      box-shadow: none;
    }

    &[data-isshow-catalog="true"] {
      ${Controls} {
        box-shadow: none;
      }
    }

    ${StyledLogo} {
      grid-column: 1/3;
    }
  }

  @media (max-width: ${breakpoints.lg}) {
    padding-top: 16px;
    padding-bottom: 16px;

    ${Controls} {
      padding: 0;
      box-shadow: none;
      display: flex;
      justify-content: flex-end;
      grid-column: 5/-1;
      gap: 8px;
    }

    ${ButtonCatalogContainer} {
      grid-row: 2;
      grid-column: 1/5;

      ${ButtonBase} {
        width: 100%;
      }
    }

    .${cssButtonSearchTrigger} {
      grid-row: 2;
      grid-column: 5;
    }

    &[data-issshow-menu="true"],
    &[data-issshow-minicart="true"],
    &[data-isshow-catalog="true"] {
      z-index: 11;
    }
  }

  @media (max-width: ${breakpoints.sm}) {
    ${Controls} {
      ${ButtonBox} {
        margin-right: 0;
      }
    }
  }
`
