import { FC } from "react"
import { useFavorites } from "../../../../hooks/favorites"
import { useAuth } from "../../../../hooks/auth"
import { Button } from "../../../../components/Button"
import { cssButtonToFavorite } from "../../../../components/Button/StyledButton"
import { LIMIT_COUNT_IN_CART_NUM, ROUTES } from "../../../../utils/constants"
import PushCounter from "../../../../styles/utils/PushCounter"

export const ButtonToFavorite: FC = () => {
  const { quantity } = useFavorites()
  const { isInit, isAuth } = useAuth()

  return (
    <>
      <Button
        variant={"box"}
        icon={"StarOutline"}
        className={cssButtonToFavorite}
        as={"a"}
        href={
          isInit && isAuth
            ? `${ROUTES.account}${ROUTES.favorites}`
            : ROUTES.favorites
        }
      >
        {quantity > 0 && (
          <PushCounter>
            {quantity > LIMIT_COUNT_IN_CART_NUM
              ? `${LIMIT_COUNT_IN_CART_NUM}+`
              : quantity}
          </PushCounter>
        )}
      </Button>
    </>
  )
}
