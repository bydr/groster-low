import type { FC } from "react"
import Logotype from "./logo.svg"
import { styled } from "@linaria/react"
import { breakpoints } from "../../../../styles/utils/vars"

export const StyledLogo = styled.div`
  width: 100%;

  a {
    width: 100%;
  }

  @media (max-width: ${breakpoints.md}) {
    g > path {
      &:last-child,
      &:nth-last-child(2) {
        display: none;
      }
    }
  }
`

const Logo: FC = () => {
  return (
    <StyledLogo>
      <a href={"/"}>
        <Logotype />
      </a>
    </StyledLogo>
  )
}

export default Logo
