import { NavType } from "../../../../types/types"
import { FC } from "react"
import Navigation from "../../../../components/Navigation/Navigation"
import { ROUTES } from "../../../../utils/constants"

const navItems: NavType = {
  title: "",
  items: [
    { title: "О компании", path: ROUTES.about },
    { title: "Как купить", path: ROUTES.help },
    { title: "Магазины", path: ROUTES.stores },
    { title: "Контакты", path: ROUTES.contacts },
    {
      title: "FAQ",
      path: "",
      subItems: [
        { title: "Помощь", path: ROUTES.help },
        { title: "Вопрос-ответ", path: ROUTES.faq },
        { title: "Условия оплаты", path: ROUTES.paymentTerm },
        { title: "Условия доставки", path: ROUTES.deliveryTerm },
        { title: "Гарантия на товар", path: ROUTES.productWarranty },
      ],
    },
  ],
}

export const NavigationStatic: FC = () => {
  return (
    <Navigation
      items={navItems.items}
      title={navItems.title}
      variant={"gray-dark-to-black"}
    />
  )
}
