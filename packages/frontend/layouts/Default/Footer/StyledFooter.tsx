import { styled } from "@linaria/react"
import { breakpoints, colors } from "../../../styles/utils/vars"
import { StyledNav } from "../../../components/Navigation/StyledNavigation"
import { cssIcon } from "../../../components/Icon"
import {
  getTypographyBase,
  Paragraph12,
} from "../../../components/Typography/Typography"
import { cssCLSocials } from "../../../components/LinkList/StyledLinkList"
import { StyledList, StyledListItem } from "../../../components/List/StyledList"
import { StyledSuccessOverlay } from "../../../components/Forms/SuccessOverlay/StyledSuccessOverlay"
import { Row } from "../../../styles/utils/StyledGrid"

export const RowListGroup = styled(Row)`
  @media (max-width: ${breakpoints.xl}) {
    flex-direction: column;
  }
`
export const ListGroupTitle = styled(Paragraph12)`
  color: ${colors.grayDark};
  margin: 0 0 8px 0;
`
export const ListGroup = styled.div`
  display: flex;
  flex-direction: column;
  width: 100%;
  margin-bottom: 24px;

  ${StyledListItem} {
    .${cssIcon} {
      fill: ${colors.brand.yellow};
    }
  }

  ${StyledNav} {
    flex: 1;
    margin-right: 16px;
    &:last-child {
      margin-right: 0;
    }

    ${StyledListItem} {
      color: ${colors.black};
    }
  }
`

export const PaymentIcons = styled.div`
  display: flex;
  align-items: center;
  width: 100%;

  svg.${cssIcon}, .${cssIcon} {
    margin: 0 8px 8px 0;
    width: 32px;
    height: auto;
    min-width: 32px;
  }
`
export const PaymentsGroupTitle = styled(ListGroupTitle)`
  margin: 0 0 10px 0;
`
export const FooterUnder = styled.div`
  width: 100%;
  padding: 24px 0;
  background: ${colors.white};
`
export const FooterMain = styled.div`
  width: 100%;
  padding: 0;
`
export const FooterContactList = styled.div``
export const FooterMainList = styled.div``
export const FooterContent = styled.div`
  width: 100%;
  background: ${colors.brand.yellow};
  border-radius: 0 4.39rem 4.39rem 0;
  padding: 56px 32px 56px 17px;
  position: relative;
  box-shadow: none;
  display: grid;
  grid-template-columns: 4fr 0.5fr 4fr 2.5fr;
  gap: 50px;
`
export const StyledFooter = styled.footer`
  position: relative;
  padding: 0;
  margin-top: 20px;
  background: ${colors.brand.yellow};

  &:before {
    content: "";
    position: absolute;
    background: ${colors.white};
    right: 0;
    left: 50%;
    top: -1px;
    bottom: -1px;
  }

  .${cssCLSocials} {
    display: flex;
    align-items: center;
    width: 100%;
  }

  ${FooterContactList} {
    grid-column: 4;
  }
  ${FooterMainList} {
    grid-column: 3;
  }

  ${StyledSuccessOverlay} {
    background: ${colors.brand.yellow};
  }

  ${StyledList} {
    ${getTypographyBase("p12")};
  }

  ${ListGroup} {
    display: inline-flex;
    flex-direction: row;
  }

  ${ListGroupTitle} {
    margin: 0 0 24px 0;
  }

  @media (max-width: ${breakpoints.xxl}) {
    ${FooterContent} {
      grid-template-columns: 4fr 4fr 3fr;
    }
    ${FooterContactList} {
      grid-column: 3;
    }
    ${FooterMainList} {
      grid-column: 2;
    }
  }

  @media (max-width: ${breakpoints.xl}) {
    ${FooterContent} {
      grid-template-columns: 4fr 4fr 4fr;
    }
    ${ListGroup} {
      grid-row: 1;
      grid-column: 1/-1;
      display: grid;
      grid-template-columns: 1fr 1fr 1fr;
      gap: 50px;
    }
    ${FooterContactList} {
      grid-column: 3;
      grid-row: 2;
    }
    ${FooterMainList} {
      grid-column: 1/3;
      grid-row: 2;
    }
  }

  @media (max-width: ${breakpoints.md}) {
    ${FooterContent} {
      gap: 30px;

      ${FooterMainList} {
        grid-column: 1/-1;
        grid-row: 2;
      }
      ${FooterContactList} {
        grid-column: 1/-1;
        grid-row: 3;
      }
    }
  }

  @media (max-width: ${breakpoints.sm}) {
    ${FooterContent} {
      padding: 40px 15px 40px 0;
      gap: 30px;
      ${ListGroup} {
        grid-template-columns: 1fr 1fr;
        gap: 30px;
      }
    }
  }
`
