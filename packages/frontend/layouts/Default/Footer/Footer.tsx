import type { FC } from "react"
import React from "react"
import {
  FooterContactList,
  FooterContent,
  FooterMain,
  FooterMainList,
  FooterUnder,
  ListGroup,
  ListGroupTitle,
  StyledFooter,
} from "./StyledFooter"
import { LinkItemType, NavType } from "../../../types/types"
import { Container, Row } from "../../../styles/utils/StyledGrid"
import Navigation from "../../../components/Navigation/Navigation"
import { Subscription } from "../../../components/Subscription/Subscription"
import LinkList from "../../../components/LinkList/LinkList"
import {
  cssCLSocials,
  cssListItemPurple,
} from "../../../components/LinkList/StyledLinkList"
import { Payments } from "../../../components/Payments/Payments"
import { Typography } from "../../../components/Typography/Typography"
import { colors } from "../../../styles/utils/vars"
import { CONTACTS, ROUTES, TITLE_SITE_RU } from "../../../utils/constants"
import { useApp } from "../../../hooks/app"
import { Modal } from "../../../components/Modals/Modal"
import { Button } from "../../../components/Button"
import { cssButtonClean } from "../../../components/Button/StyledButton"
import { ToCommercialDepartment } from "../../../components/Forms/ToCommercialDepartment"

const lists: NavType[] = [
  {
    title: "Компания",
    items: [
      { title: "О компании", path: ROUTES.about },
      { title: "Магазины", path: ROUTES.stores },
      { title: "Политика", path: ROUTES.policy },
      { title: "Контакты", path: ROUTES.contacts },
    ],
  },
  {
    title: "Информация",
    items: [
      { title: "Помощь", path: ROUTES.help },
      { title: "Условия оплаты", path: ROUTES.paymentTerm },
      { title: "Условия доставки", path: ROUTES.deliveryTerm },
      { title: "Гарантия на товар", path: ROUTES.productWarranty },
    ],
  },
  {
    title: "Помощь",
    items: [
      { title: "Вопрос-ответ", path: ROUTES.faq },
      { title: "Конфиденциальность", path: ROUTES.agree },
    ],
  },
]

const CONTACTS_NUMBERS: LinkItemType[] = [
  {
    icon: "Location",
    title: "Волгоград ул. Кольцевая 64",
    path: "https://yandex.ru/maps/-/CCUF4CG2kB",
    target: "_blank",
  },
]

export const Footer: FC = () => {
  const { socials } = useApp()
  return (
    <StyledFooter>
      <FooterMain>
        <Container>
          <Row>
            <FooterContent>
              <ListGroup>
                {lists.map((l, index) => (
                  <Navigation
                    key={index}
                    items={l.items}
                    title={l.title}
                    orientation={"vertical"}
                    variant={"black-to-purple"}
                  />
                ))}
              </ListGroup>
              <FooterMainList>
                <ListGroupTitle>Подписка на рассылку</ListGroupTitle>
                <Subscription />
                <Payments />
              </FooterMainList>
              <FooterContactList>
                <ListGroupTitle>Наши контакты</ListGroupTitle>
                <LinkList items={CONTACTS} />
                <LinkList
                  items={CONTACTS_NUMBERS}
                  className={cssListItemPurple}
                />
                <Modal
                  title={"Заявка в отдел коммерции"}
                  closeMode={"destroy"}
                  variant={"rounded-50"}
                  disclosure={
                    <Button
                      icon={"Chat"}
                      className={cssButtonClean}
                      variant={"link"}
                      isHiddenBg
                    >
                      <Typography variant={"p12"}>
                        Отдел коммерции, для предложений от поставщиков
                      </Typography>
                    </Button>
                  }
                >
                  <ToCommercialDepartment />
                </Modal>
                <LinkList
                  items={[
                    {
                      icon: "Email",
                      title:
                        "zakaz@groster.me - Отдел продаж, любые вопросы по вашим заказам",
                      path: "mailto:zakaz@groster.me",
                    },
                    {
                      icon: "Email",
                      title:
                        "zakupki@groster.me - Отдел закупок, для предложений от поставщиков",
                      path: "mailto:zakupki@groster.me",
                    },
                    {
                      icon: "Email",
                      title:
                        "development@groster.me - Отдел развития, для предложений по аренде площадей",
                      path: "mailto:development@groster.me",
                    },
                    {
                      icon: "Email",
                      title: "info@groster.me - Ваши жалобы и предложения",
                      path: "mailto:info@groster.me",
                    },
                  ]}
                />
                {socials !== null && (
                  <>
                    <LinkList
                      items={Object.keys(socials).map((s) => socials[s])}
                      className={cssCLSocials}
                    />
                  </>
                )}
              </FooterContactList>
            </FooterContent>
          </Row>
        </Container>
      </FooterMain>
      <FooterUnder>
        <Row>
          <Container>
            <Typography variant={"p12"} color={colors.grayDark}>
              {new Date().getFullYear()} © {TITLE_SITE_RU}{" "}
            </Typography>
          </Container>
        </Row>
      </FooterUnder>
    </StyledFooter>
  )
}
