import type { FC } from "react"
import { Footer } from "./Footer/Footer"
import { Header } from "./Header/Header"
import { useCart } from "../../hooks/cart"
import type { ModalDefaultPropsType } from "../../components/Modals/Modal"
import { BindCartContent } from "../../components/Account/BindCart/Content"
import { BaseLoader } from "../../components/Loaders/BaseLoader/BaseLoader"
import { useEffect, useState } from "react"
import dynamic, { DynamicOptions } from "next/dynamic"
import { PageWrapper } from "../../styles/utils/Utils"

const AllowCookie = dynamic(
  (() =>
    import("../../components/AllowCookie").then(
      (mod) => mod.AllowCookie,
    )) as DynamicOptions,
  {
    ssr: false,
  },
)

const DynamicModal = dynamic(
  (() =>
    import("../../components/Modals/Modal").then(
      (mod) => mod.Modal,
    )) as DynamicOptions<ModalDefaultPropsType>,
  {
    ssr: false,
  },
)

export const Default: FC = ({ children, ...rest }) => {
  const { hasProductsInAuth, isMergingCart, cleanMergeData } = useCart()

  const [isShowModal, setIsShowModal] = useState(false)

  useEffect(() => {
    setIsShowModal(hasProductsInAuth)
  }, [hasProductsInAuth])

  return (
    <PageWrapper {...rest}>
      {isMergingCart && <BaseLoader isFixed />}
      <Header />
      {children}

      <AllowCookie />

      <DynamicModal
        closeMode={"destroy"}
        isShowModal={isShowModal}
        variant={"rounded-70"}
        onClose={() => {
          cleanMergeData()
        }}
      >
        <BindCartContent
          clickCloseHandle={() => {
            setIsShowModal(false)
          }}
        />
      </DynamicModal>
      <Footer />
    </PageWrapper>
  )
}
