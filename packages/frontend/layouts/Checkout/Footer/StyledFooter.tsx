import { styled } from "@linaria/react"
import { StyledFooter as StyledFooterBase } from "../../Default/Footer/StyledFooter"
import { colors } from "../../../styles/utils/vars"

export const StyledFooter = styled(StyledFooterBase)`
  width: 100%;
  background: ${colors.grayLight};
  padding: 32px 0;

  &:before,
  &:after {
    display: none;
    content: none;
  }
`
