import type { FC } from "react"
import { StyledFooter } from "./StyledFooter"
import { Container, Row } from "../../../styles/utils/StyledGrid"
import { Payments } from "../../../components/Payments/Payments"
import { Typography } from "../../../components/Typography/Typography"
import { colors } from "../../../styles/utils/vars"
import { TITLE_SITE_RU } from "../../../utils/constants"

export const Footer: FC = () => {
  return (
    <>
      <StyledFooter>
        <Container>
          <Row>
            <Payments />
            <Typography
              variant={"p12"}
              color={colors.grayDark}
              style={{
                marginTop: "10px",
              }}
            >
              {new Date().getFullYear()} © {TITLE_SITE_RU}{" "}
            </Typography>
          </Row>
        </Container>
      </StyledFooter>
    </>
  )
}
