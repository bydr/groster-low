import { FC } from "react"
import { Header } from "./Header/Header"
import { Footer } from "./Footer/Footer"

export const CheckoutLayout: FC = ({ children }) => {
  return (
    <>
      <Header />
      {children}
      <Footer />
    </>
  )
}
