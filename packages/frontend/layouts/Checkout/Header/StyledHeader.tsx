import { styled } from "@linaria/react"
import { breakpoints, colors } from "../../../styles/utils/vars"
import {
  Controls,
  RowBottom,
  StyledHeader as StyledHeaderBase,
} from "../../Default/Header/StyledHeader"
import { cssButtonLogout } from "../../../components/Button/StyledButton"

export const StyledHeader = styled(StyledHeaderBase)`
  padding-top: 24px;
  padding-bottom: 32px;
  margin-bottom: 40px;
  position: relative;
  border: none;

  &:before {
    content: "";
    position: absolute;
    left: 8px;
    right: 8px;
    height: 100%;
    width: auto;
    background: ${colors.grayLight};
    top: 0;
    border-radius: 0 0 56px 56px;
    max-width: 1600px;
    margin: 0 auto;
  }
  &:after {
    content: none;
    display: none;
  }

  ${Controls} {
    width: 100%;
    box-shadow: none;
    background-color: transparent;
    display: flex;
    justify-content: flex-end;
    padding: 14px 0;

    > *:first-child {
      margin-right: 0;
    }

    @media (max-width: ${breakpoints.lg}) {
      display: none;
    }
  }

  .${cssButtonLogout} {
    display: none;
  }

  @media (max-width: ${breakpoints.lg}) {
    padding-top: 14px;
    padding-bottom: 16px;

    ${RowBottom} {
      display: flex;
      width: 100%;
      justify-content: center;
      text-align: center;
    }

    &:before {
      border-radius: 0 0 36px 36px;
    }
  }

  @media (max-width: ${breakpoints.sm}) {
    &:before {
      left: 0;
      right: 0;
    }
  }
`
