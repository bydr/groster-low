import { FC } from "react"
import { StyledHeader } from "./StyledHeader"
import {
  Controls,
  RowBottom,
  RowTop,
  RowWrap,
} from "../../Default/Header/StyledHeader"
import { CallInfo } from "../../Default/Header/CallInfo"
import { AuthControls } from "../../Default/Header/Auth/AuthControls"
import Logo from "../../Default/Header/Logo/Logo"
import { Container } from "../../../styles/utils/StyledGrid"
import { Notification } from "../../../components/Notification/Notification"
import { useHeader } from "../../../hooks/header"
import { SelectLocation } from "../../Default/Header/SelectLocation"
import { NavigationStatic } from "../../Default/Header/NavigationStatic"

export const Header: FC = () => {
  const { notificationCart, setNotificationCart } = useHeader()

  return (
    <StyledHeader>
      <Container>
        <RowTop>
          <RowWrap>
            <SelectLocation />
            <NavigationStatic />
          </RowWrap>
          <AuthControls isShowButtonLogout={false} />
        </RowTop>

        <RowBottom>
          <Logo />

          <Controls>
            <CallInfo />
          </Controls>
        </RowBottom>
      </Container>
      {notificationCart !== null && (
        <>
          <Notification
            isOpen={true}
            notification={notificationCart}
            setNotification={setNotificationCart}
            isAccentOutline
          />
        </>
      )}
    </StyledHeader>
  )
}
