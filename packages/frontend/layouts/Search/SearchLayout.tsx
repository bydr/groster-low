import { FC } from "react"
import { SearchHeader } from "./Header"

export const SearchLayout: FC = ({ children }) => {
  return (
    <>
      <SearchHeader />
      {children}
    </>
  )
}
