import { FC } from "react"
import { AccountRowContentSidebar } from "../../components/Account/StyledAccount"
import { Container, ContentContainer } from "../../styles/utils/StyledGrid"
import { SidebarSearch } from "../../components/Search/Sidebar"
import { useWindowSize } from "../../hooks/windowSize"
import { breakpoints } from "../../styles/utils/vars"
import { getBreakpointVal } from "../../styles/utils/Utils"
import { SearchPage } from "../../components/Search/StyledSearch"
import { BaseLoader } from "../../components/Loaders/BaseLoader/BaseLoader"
import { useApp } from "../../hooks/app"

export const SearchTemplate: FC = ({ children }) => {
  const { width } = useWindowSize()
  const { isLoadingPage } = useApp()

  return (
    <>
      {isLoadingPage && <BaseLoader isFixed />}
      <SearchPage>
        <Container>
          <AccountRowContentSidebar>
            {width !== undefined && width > getBreakpointVal(breakpoints.lg) && (
              <>
                <SidebarSearch />
              </>
            )}
            <ContentContainer>{children}</ContentContainer>
          </AccountRowContentSidebar>
        </Container>
      </SearchPage>
    </>
  )
}
