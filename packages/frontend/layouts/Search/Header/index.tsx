import { FC } from "react"
import { useWindowSize } from "../../../hooks/windowSize"
import { getBreakpointVal } from "../../../styles/utils/Utils"
import { breakpoints } from "../../../styles/utils/vars"
import { Header } from "../../Default/Header/Header"
import { SearchHeaderClean } from "./Header"

export const SearchHeader: FC = () => {
  const { width } = useWindowSize()
  return (
    <>
      {width !== undefined && width <= getBreakpointVal(breakpoints.lg) ? (
        <>
          <SearchHeaderClean />
        </>
      ) : (
        <>
          <Header />
        </>
      )}
    </>
  )
}
