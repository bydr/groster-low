import { FC } from "react"
import { Container } from "../../../styles/utils/StyledGrid"
import { StyledSearchHeader } from "./StyledHeader"
import { SearchField } from "../../../components/Search/Field"
import { cx } from "@linaria/core"
import { cssHeaderDefault } from "../../Default/Header/StyledHeader"

export const SearchHeaderClean: FC = () => {
  return (
    <>
      <StyledSearchHeader className={cx(cssHeaderDefault)}>
        <Container>
          <SearchField isResponsiveMode />
        </Container>
      </StyledSearchHeader>
    </>
  )
}
