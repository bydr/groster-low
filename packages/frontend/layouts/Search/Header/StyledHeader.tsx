import { styled } from "@linaria/react"
import { StyledHeader } from "../../Default/Header/StyledHeader"
import { StyledAutoComplete } from "../../../components/Search/AutoComplete/StyledAutoComplete"

export const StyledSearchHeader = styled(StyledHeader)`
  ${StyledAutoComplete} {
    max-height: 100vh;
    height: calc(100vh - 62px);
  }
`
