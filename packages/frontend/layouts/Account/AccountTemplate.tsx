import { FC } from "react"
import { Container, ContentContainer } from "../../styles/utils/StyledGrid"
import {
  AccountPage,
  AccountRowContentSidebar,
} from "../../components/Account/StyledAccount"
import { Typography } from "../../components/Typography/Typography"
import { PageType, SidebarLayout } from "../../components/Account/Sidebar"
import { ROUTES } from "../../utils/constants"

const ACCOUNT_PAGES: PageType[] = [
  {
    title: "История заказов",
    path: `${ROUTES.account}${ROUTES.historyOrders}`,
    order: 1,
  },
  {
    title: "Подписки",
    path: `${ROUTES.account}${ROUTES.subscriptions}`,
    order: 1,
  },
  // {
  //   title: "Сотрудники",
  //   path: `${ROUTES.account}${ROUTES.staff}`,
  //   order: 1,
  // },
  {
    title: "Плательщики",
    path: `${ROUTES.account}${ROUTES.payers}`,
    order: 1,
  },
  {
    title: "Адреса",
    path: `${ROUTES.account}${ROUTES.addresses}`,
    order: 1,
  },
  {
    title: "Избранное",
    path: `${ROUTES.account}${ROUTES.favorites}`,
    order: 1,
  },
  {
    title: "Прайс-лист",
    path: `${ROUTES.account}${ROUTES.pricelist}`,
    order: 1,
  },
  {
    title: "Персональные рекомендации",
    path: `${ROUTES.account}${ROUTES.recommendations}`,
    order: 1,
  },
  {
    title: "Профиль",
    path: `${ROUTES.account}`,
    order: 0,
  },
]

export const AccountTemplate: FC<{ title?: string }> = ({
  title,
  children,
}) => {
  return (
    <>
      <AccountPage>
        <Container>
          <AccountRowContentSidebar>
            <SidebarLayout pages={ACCOUNT_PAGES} />
            <ContentContainer>
              {title && <Typography variant={"h1"}>{title}</Typography>}
              {children}
            </ContentContainer>
          </AccountRowContentSidebar>
        </Container>
      </AccountPage>
    </>
  )
}
