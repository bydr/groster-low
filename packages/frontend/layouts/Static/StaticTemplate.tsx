import { FC } from "react"
import { AccountRowContentSidebar } from "../../components/Account/StyledAccount"
import { PageType, SidebarLayout } from "../../components/Account/Sidebar"
import { Container, ContentContainer } from "../../styles/utils/StyledGrid"
import { Typography } from "../../components/Typography/Typography"
import { ROUTES } from "../../utils/constants"

const STATIC_PAGES: PageType[] = [
  {
    title: "О компании",
    path: ROUTES.about,
  },
  {
    title: "Магазины",
    path: ROUTES.stores,
  },
  {
    title: "Контакты",
    path: ROUTES.contacts,
  },
  {
    title: "Политика",
    path: ROUTES.policy,
  },
  {
    title: "Вопрос-ответ",
    path: ROUTES.faq,
  },
  {
    title: "Условия оплаты",
    path: ROUTES.paymentTerm,
  },
  {
    title: "Условия доставки",
    path: ROUTES.deliveryTerm,
  },
  {
    title: "Условия возврата",
    path: ROUTES.return,
  },
  {
    title: "Гарантия на товар",
    path: ROUTES.productWarranty,
  },
  {
    title: "Конфиденциальность",
    path: ROUTES.agree,
  },
  {
    title: "Помощь",
    path: ROUTES.help,
  },
]

export const StaticTemplate: FC<{ title?: string }> = ({ title, children }) => {
  return (
    <>
      <Container>
        <AccountRowContentSidebar>
          <SidebarLayout pages={STATIC_PAGES} />
          <ContentContainer>
            {title && <Typography variant={"h1"}>{title}</Typography>}
            {children}
          </ContentContainer>
        </AccountRowContentSidebar>
      </Container>
    </>
  )
}
