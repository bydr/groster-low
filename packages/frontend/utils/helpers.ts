import {
  AvailableStatusType,
  BannerStringTypes,
  CategoriesTreeStateType,
  ICategoryTreeItem,
  ListItemType,
  LocationExtendsType,
  LocationType,
  ProductOfferType,
  ProductType,
  ProductWithChildType,
  PropertiesType,
  SortByAliasType,
  SortByType,
  SortType,
  SpecificationItemType,
} from "../types/types"
import {
  ApiError,
  BannerApiType,
  Category,
  CategoryByArea,
  ParamsResponse,
  ProductDetailListType,
  SortTypeResponse,
} from "../../contracts/contracts"
import { BreadcrumbItemType } from "../components/Breadcrumbs/BreadcrumbItem"
import { COOKIE_LOCATION_NAME, MONTHS_DECLINATION, ROUTES } from "./constants"
import { getClientUser, setUser } from "../hooks/auth"
import { setTokenStorage } from "../hooks/cart"
import {
  IAvailableParam,
  IAvailableParams,
  IAvailableParamsKeys,
  IFilterParamChild,
  IFilterParamParent,
  IFilterParamsChilds,
  IFilterParamsParents,
} from "../store/reducers/catalogSlice"
import { PayloadAction } from "@reduxjs/toolkit"
import { cssNoScroll } from "../styles/utils/Utils"
import Cookies from "universal-cookie"
import { CheckoutFormFieldsType } from "../components/Checkout/Checkout"
import { DaDataLocationDataType, fetchIpLocate } from "../api/dadataAPI"
import { IncomingMessage } from "http"
import { clearAllBodyScrollLocks } from "body-scroll-lock"
import { fetchCatalog } from "../api/catalogAPI"

export const SAMPLE_QUANTITY_LIMIT = 2

const cookies = new Cookies()

export const createBreadcrumbsFromCategories = (
  currentCategories: Category[],
  categories: Record<string, Category> | null,
): Record<string, BreadcrumbItemType> => {
  const _breadcrumbs = [...currentCategories]
  const out = {} as Record<string, BreadcrumbItemType>

  if (categories) {
    for (const currCategory of _breadcrumbs) {
      const children = [] as ListItemType[]
      Object.entries(categories).map(([, category]) => {
        if (!!category.product_qty) {
          if (category.parent === currCategory.id) {
            children.push({
              path: `${ROUTES.catalog}/${category.alias}`,
              title: category.name || "",
            })
          }
        }
      })
      out[`${currCategory?.uuid}`] = {
        path: `${ROUTES.catalog}/${currCategory.alias}` || "",
        title: currCategory.name || "",
        subItems: children,
      }
    }
  }

  return out
}

export const priceToFormat = (price: number): string =>
  `${+(price / 100).toFixed(2)}`

export const createCategoriesTree = (
  categories: Category[],
): CategoriesTreeStateType => {
  if (!categories) {
    return {
      tree: null,
      compared: null,
    }
  }

  const _categories = [...categories]
  const categoriesTree = {} as Record<string, ICategoryTreeItem>
  const compareChildMain = {} as Record<string, number>

  const parentTree = (
    current: ICategoryTreeItem,
    parentCategory: ICategoryTreeItem,
  ) => {
    if (
      Object.keys(parentCategory["children"]).some(
        (key) => +key === current.parent,
      )
    ) {
      if (!!current.parent && !!current.id) {
        parentCategory["children"][current.parent]["children"][current.id] =
          current
      }
      return
    }

    for (const key of Object.keys(parentCategory["children"])) {
      parentTree(current, { ...parentCategory["children"][key] })
    }
  }

  const sortedArr = _categories.sort((a, b) => {
    if ((a?.parent || 0) > (b?.parent || 0)) {
      return 1
    }
    if ((a?.parent || 0) < (b?.parent || 0)) {
      return -1
    }
    // a должно быть равным b
    return 0
  })

  for (const cat of sortedArr) {
    const category = {
      ...cat,
      children: {},
    } as ICategoryTreeItem

    if (category.id === undefined || category.parent === undefined) {
      continue
    }

    if (category.parent === 0) {
      categoriesTree[category.id] = category
      continue
    }

    if (compareChildMain[category.parent] === undefined) {
      compareChildMain[category.id] = category.parent
      if (categoriesTree[category.parent] === undefined) {
        categoriesTree[category.parent] = {
          ...categoriesTree[category.parent],
          children: {},
        }
      }
      categoriesTree[category.parent].children[category.id] = category
    } else {
      compareChildMain[category.id] = compareChildMain[category.parent]
      parentTree(category, categoriesTree[compareChildMain[category.id]])
    }
  }

  return {
    tree: categoriesTree,
    compared: compareChildMain,
  }
}

export const compareSortTypesWithIcon = (
  sortTypes: SortTypeResponse,
): SortByType => {
  const sorts: SortByType = {}

  for (const item of sortTypes) {
    if (!!item.alias && !!item.name) {
      const sort: SortType = {
        value: item.alias,
        name: item.name,
      }
      switch (item.alias as SortByAliasType) {
        case "price_asc":
          sort.icon = "ArrowUp"
          break
        case "price_desc":
          sort.icon = "ArrowDown"
          break
      }

      if (!!sort) {
        sorts[sort.value] = { ...sort }
      }
    }
  }

  return Object.keys(sorts).length > 0 ? sorts : null
}

export const timeToDate = (time: string): Date | null => {
  const parsedTime = time.split(":")
  if (parsedTime.length !== 2) {
    return null
  }

  const dHour = +parsedTime[0]
  const dMinute = +parsedTime[1]

  const date = new Date()
  date.setHours(dHour)
  date.setMinutes(dMinute)

  return date
}

type DateToStringType = (selectedDay: Date, withYear?: boolean) => string
export const dateToString: DateToStringType = (
  selectedDay,
  withYear = true,
): string => {
  const normalizeDate = new Date(selectedDay)

  return `${normalizeDate.getDate()} ${
    MONTHS_DECLINATION["ru"][normalizeDate.getMonth()]
  } ${withYear ? normalizeDate.getFullYear() : ""}`
}

export const shippingDateToString = (
  date: Date,
  messages?: {
    today?: string
    tomorrow?: string
    other?: (day: string) => string
  },
): string => {
  let str: string
  const tomorrow = new Date()
  tomorrow.setDate(tomorrow.getDate() + 1)

  switch (date.toISOString().split("T")[0]) {
    case new Date().toISOString().split("T")[0]: {
      str = messages?.today !== undefined ? messages.today : "Доставка сегодня"
      break
    }
    case tomorrow.toISOString().split("T")[0]: {
      str =
        messages?.tomorrow !== undefined
          ? messages.tomorrow
          : "Возможна доставка завтра"
      break
    }
    default: {
      str =
        messages?.other !== undefined
          ? messages.other(`${dateToString(date, false)}`)
          : `Доставка к\u00A0${dateToString(date, false)}`
    }
  }

  return str
}

type AvailableFnType = (
  totalQty: number,
  minManyQuantity: number,
) => AvailableStatusType
export const getAvailableStatus: AvailableFnType = (
  totalQty,
  minManyQuantity,
) => {
  return totalQty <= 0 ? "not" : totalQty > minManyQuantity ? "many" : "few"
}

export const onErrorFetcherCart = (error: ApiError): void => {
  if (error.status === 404) {
    const user = getClientUser()
    if (user !== null) {
      setUser({
        ...user,
        cart: null,
      })
    }
    setTokenStorage(null)
    location.reload()
  }
}

export const getFilterParamsTree = (
  action: PayloadAction<ParamsResponse>,
): IFilterParamsParents & IFilterParamsChilds => {
  const paramsAllTree = {} as IFilterParamsParents & IFilterParamsChilds
  for (const item of action.payload) {
    const newItem = { ...item }
    delete newItem.values
    paramsAllTree[item?.uuid || ""] = { ...newItem }
    if (item.values !== undefined) {
      for (const val of item.values) {
        if (val?.uuid) {
          paramsAllTree[val?.uuid] = {
            ...val,
            checked: false,
            parentUuid: item?.uuid || "",
          }
        }
      }
    }
  }

  return paramsAllTree
}

export const getAvailableParamsTree = (
  stateParams: IFilterParamsParents & IFilterParamsChilds,
  payload: IAvailableParamsKeys | null,
): {
  tree: IAvailableParams | null
} => {
  if (payload === null) {
    return {
      tree: null,
    }
  }

  const tree: IAvailableParams = {} as IAvailableParams

  Object.keys(payload).map((key) => {
    const child: IFilterParamChild = {
      ...(stateParams[key] as IFilterParamChild),
      product_qty: payload[key],
    }

    if (!!child.parentUuid) {
      const parent: IFilterParamParent = stateParams[
        child.parentUuid || ""
      ] as IFilterParamParent

      //create parent param
      const availableParam: IAvailableParam = tree[parent.uuid || ""]

      const checkedKeys = !!availableParam
        ? availableParam.checkedKeys
        : ([] as string[])

      tree[parent.uuid || ""] = {
        ...parent,
        params: !!availableParam
          ? availableParam.params
          : ({} as IFilterParamsParents),
        checkedKeys: checkedKeys,
      }

      //create child values in parent param
      tree[parent.uuid || ""].params[child.uuid || ""] = {
        ...child,
      }
    }
  })

  return {
    tree: Object.keys(tree).length > 0 ? tree : null,
  }
}

export const normalizeCategoriesByAreas = (
  areas: CategoryByArea[],
  categories: Record<string, Category>,
): Category[] => {
  const fetchedBusinessAreas = [] as Category[]

  const normalize = (
    areas: CategoryByArea[],
    categories: Record<string, Category>,
  ) => {
    areas.map((cat) => {
      if (cat.product_qty !== undefined) {
        fetchedBusinessAreas.push({
          ...categories[cat.uuid || ""],
          product_qty: cat.product_qty || 0,
        })
        if (!!cat.children && cat.children.length > 0) {
          normalize(cat.children, categories)
        }
      }
    })
  }

  normalize(areas, categories)

  return fetchedBusinessAreas
}

export const compareSpecificationProducts = (
  specification: Record<string, SpecificationItemType> | null,
  datasource: Record<string, ProductWithChildType> | null,
): {
  products: Record<string, ProductWithChildType> | null
  samples: Record<string, ProductWithChildType> | null
} => {
  // полученные products которые есть в корзине
  // разделить по виду Товары или Образцы
  // пример: обрзец товара может быть в корзине - товара при этом может не быть

  const product: Record<string, ProductWithChildType> = {}
  const sample: Record<string, ProductWithChildType> = {}

  if (specification === null || datasource === null) {
    return {
      products: null,
      samples: null,
    }
  }

  for (const [sKey, itemSpec] of Object.entries(specification)) {
    //если есть такой товар - сохранили
    if (!datasource[sKey]) {
      continue
    }

    if (!!itemSpec?.quantity) {
      product[sKey] = { ...datasource[sKey] }
    }

    // если кол-во sample больше 0, т.е. ОБРАЗЕЦ в корзине
    if (!!itemSpec?.sample || !!itemSpec?.isSampleRemoved) {
      //если есть такой товар - сохранили
      sample[specification[sKey].uuid || ""] = datasource[sKey]
    }

    //если спецификация это комплект, сохраняем дочерние
    if (!!itemSpec.child) {
      for (const [sChildKey, specChild] of Object.entries(itemSpec.child)) {
        // если есть такой товар
        if (!datasource[sChildKey]) {
          continue
        }
        if (!product[sKey]) {
          continue
        }

        // сохранили как дочерний комплекта
        product[sKey].child = {
          ...product[sKey].child,
          ...{ [sChildKey]: datasource[sChildKey] },
        }

        // если у дочернего есть образец - также сохранили
        // у составляющих комплекта тоже могут быть образцы
        // т.к. это реальные, обыкновенные товары
        if (!!specChild?.sample) {
          sample[sChildKey] = datasource[sChildKey]
        }
      }
    }
  }

  return {
    products: Object.keys(product).length > 0 ? product : null,
    samples: Object.keys(sample).length > 0 ? sample : null,
  }
}

export const normalizeQuery = (
  query: Record<string, string | number | undefined>,
): Record<string, string | number> => {
  const q = { ...query }
  Object.keys(q).map((key) => {
    if (
      q[key] === null ||
      q[key] === undefined ||
      (q[key] as string)?.length <= 0
    ) {
      delete q[key]
    }
  })

  return q as Record<string, string | number>
}

// 1 - слайдер главная
// * 2 - одиночный главная
// * 3 - одиночный в списке товаров
// * 4 - одиночный в каталоге под товарами
export const getBannerStringType = ({
  type,
}: Pick<BannerApiType, "type">): BannerStringTypes | undefined => {
  switch (type) {
    case 1: {
      return "main_slider"
    }
    case 2: {
      return "main_single"
    }
    case 3: {
      return "catalog_products_single"
    }
    case 4: {
      return "catalog_under_products_single"
    }
    default: {
      return undefined
    }
  }
}

export const scrollBodyDisable = (
  targetElement?: HTMLElement | Element,
): void => {
  console.log("targetEl ", targetElement)
  document.body.classList.add(cssNoScroll)
}
export const scrollBodyEnable = (): void => {
  clearAllBodyScrollLocks()
  document.body.classList.remove(cssNoScroll)
}

export const setLocationCookie = (location: LocationType | null): void => {
  if (!!location) {
    cookies.set(COOKIE_LOCATION_NAME, JSON.stringify(location), {
      path: "/",
    })
  } else {
    cookies.remove(COOKIE_LOCATION_NAME)
  }
}
export const getLocationCookie = (): LocationType | undefined => {
  return cookies.get(COOKIE_LOCATION_NAME)
}

export const getLocationFormat = (
  location: LocationType | null,
): LocationExtendsType | null => {
  return location !== null
    ? {
        ...location,
        city_full: [location.city_type, location.city]
          .filter((i) => i !== null)
          .join(" "),
        region_full: [location.region_type, location.region]
          .filter((i) => i !== null)
          .join(" "),
      }
    : null
}

export const getLocationRegion = (
  location: LocationExtendsType | null,
): string | null => {
  return location !== null
    ? [location?.city_full, location?.region_full]
        .filter((item) => !!item)
        .join(",")
    : null
}

export const getFormatResponseLocation = (
  payload: DaDataLocationDataType,
): LocationType => {
  return {
    region_type: payload?.region_type || null,
    region: payload?.region || null,
    city_type: payload?.city_type || null,
    city: payload?.city || null,
    region_kladr_id: payload?.region_kladr_id || null,
  }
}

export const isCurrentYear = (date: Date | string): boolean =>
  new Date(date).getFullYear() === new Date().getFullYear()

export const getTranslationProperties = (
  properties: PropertiesType,
): { name: string; value: string }[] => {
  const PROPERTIES_RU = {
    length: "длина",
    width: "ширина",
    height: "высота",
    size: "размер",
    weight: "вес",
  }

  return properties
    .filter((prop) => !!prop.value && !!prop.name)
    .map((p) => {
      return {
        value: p.value || "",
        name:
          PROPERTIES_RU[p.name || ""] !== undefined
            ? PROPERTIES_RU[p.name || ""]
            : p.name || "",
      }
    })
}

export const CHECKOUT_SAVED_TEMP_KEY = "checkoutSaved"

export type LocalStorageCheckoutType = CheckoutFormFieldsType & {
  location: LocationType | null
}

export const setCheckoutToStorage = (
  data: Omit<
    LocalStorageCheckoutType,
    "shipping_date" | "comment" | "dataPrivacyAgreement"
  > | null,
): void => {
  if (typeof localStorage === "undefined" || !localStorage) {
    return
  }
  if (data === null) {
    localStorage.removeItem(CHECKOUT_SAVED_TEMP_KEY)
  } else {
    localStorage.setItem(CHECKOUT_SAVED_TEMP_KEY, JSON.stringify(data))
  }
}

export const getCheckoutFromStorage = (): LocalStorageCheckoutType | null => {
  if (typeof localStorage === "undefined" || !localStorage) {
    return null
  }
  const data = localStorage.getItem(CHECKOUT_SAVED_TEMP_KEY)
  return !!data ? JSON.parse(data) : null
}

export const dateToISOString = (date: string | Date): string => {
  return new Date(date).toISOString().split("T")[0]
}

export const declinationOfNum = (number: number, words: string[]): string =>
  words[
    number % 100 > 4 && number % 100 < 20
      ? 2
      : [2, 0, 1, 1, 1, 2][number % 10 < 5 ? Math.abs(number) % 10 : 5]
  ]

export const getRandomInt = (max: number): number => {
  return Math.floor(Math.random() * max)
}

export const getImagePath = (path: string): string => {
  return path.includes("http") ? path : `https://${path}`
}

export const getNumberOfDays = (
  start: string | Date,
  end: string | Date,
): number => {
  const date1 = new Date(start)
  const date2 = new Date(end)

  // One day in milliseconds
  const oneDay = 1000 * 60 * 60 * 24

  // Calculating the time difference between two dates
  const diffInTime = date2.getTime() - date1.getTime()

  // Calculating the no. of days between two dates
  return Math.round(diffInTime / oneDay)
}

export const getScrollHeight = (): number =>
  Math.max(
    document.body.scrollHeight,
    document.documentElement.scrollHeight,
    document.body.offsetHeight,
    document.documentElement.offsetHeight,
    document.body.clientHeight,
    document.documentElement.clientHeight,
  )

export const getExpireOneYear = (): Date =>
  new Date(new Date().getTime() + 1000 * 60 * 60 * 24 * 365)

export const normalizeValueToSort = (str?: string): string | number => {
  const strInit = str || ""

  if (str === undefined) {
    return strInit
  } else {
    const digitals = strInit.match(/\d+/g) || []

    if (digitals.length === 0) {
      return strInit
    } else {
      if (digitals.length >= 1) {
        // если число дробное, приходит через "," получаем такое число в формате number
        if (strInit.includes(",") && digitals.length >= 2) {
          return +`${digitals[0]}.${digitals[1]}`
        } else {
          return +`${digitals[0]}`
        }
      }
    }
  }
  return strInit
}

export const setLeadHitStockId = (id: string | null): void => {
  const KEY_NAME = "_lh_stock_id"
  if (id !== null) {
    localStorage.setItem(KEY_NAME, id)
  } else {
    localStorage.removeItem(KEY_NAME)
  }
}
export type RuleSortProduct = { uuid?: string; weight?: number }
export const filterProductsByCategory = ({
  products,
  category,
}: {
  products: ProductDetailListType
  category: string
}): ProductDetailListType => {
  return products.filter((p) => (p.categories || []).includes(category))
}
export const sortProductsWeightRule = ({
  products,
  rules,
}: {
  products: ProductDetailListType
  rules?: RuleSortProduct[]
}): ProductDetailListType => {
  const weightRule = rules || []
  const _getCurrRule = (uuid?: string): RuleSortProduct | undefined =>
    weightRule.find((w) => w.uuid === uuid)

  return [...products]
    .sort((a, b) => {
      const weightA = _getCurrRule(a.uuid)?.weight
      const weightB = _getCurrRule(b.uuid)?.weight

      if (weightA !== undefined && weightB !== undefined) {
        return weightA > weightB ? 1 : weightA < weightB ? -1 : 0
      } else {
        return 0
      }
    })
    .sort((a, b) => {
      return !!a.total_qty === !!b.total_qty ? 0 : !!a.total_qty ? -1 : 1
    })
}

export const getIp = (req: IncomingMessage | undefined): string | undefined => {
  const forwarded = req?.headers["x-forwarded-for"]
  let ip =
    typeof forwarded === "string"
      ? forwarded.split(/, /)[0]
      : (req?.headers["x-real-ip"] as string | undefined)
  if (!ip) {
    ip = req?.socket?.remoteAddress
  }

  return ip
}

export const getIpCookieEntry = async (
  req: IncomingMessage | undefined,
  ip: string,
): Promise<string | undefined> => {
  const cookies = req?.headers.cookie
  if (cookies === undefined) {
    return undefined
  }

  if (cookies.search(COOKIE_LOCATION_NAME) !== -1) {
    return undefined
  }

  const result = await fetchIpLocate(ip)

  let format: string | undefined
  if (!!result && !!result.location) {
    format = JSON.stringify(getFormatResponseLocation(result.location.data))
  }

  if (!!format) {
    return `${COOKIE_LOCATION_NAME}=${encodeURIComponent(
      format,
    )}; Path=/; Expires=${getExpireOneYear().toUTCString()}`
  }

  return undefined
}

export const toSpecialChars = (string: string): string => {
  return string
    .replaceAll(/&/g, "&amp;")
    .replaceAll(/>/g, "&gt;")
    .replaceAll(/</g, "&lt;")
    .replaceAll(/"/g, "&quot;")
    .trim()
}

export async function getProductsSite<T = ProductOfferType>(
  categoryIds: string[],
  formattedMehod: (p: ProductType) => T,
): Promise<T[]> {
  let page = 1
  let products: T[] = []
  let totalPages = 0
  const PER_PAGE = 500

  const getFetcherCatalog = (page: number) =>
    fetchCatalog({
      server: true,
      categories: categoryIds,
      perPage: PER_PAGE,
      page: page,
      isEnabled: "0",
      isSearch: true,
    })

  const firstData = await getFetcherCatalog(page)

  if (!!firstData) {
    totalPages = !!firstData.total ? Math.ceil(firstData.total / PER_PAGE) : 0
    if (!!firstData.products && firstData.products.length > 0) {
      products = [
        ...products,
        ...firstData.products.map((p) => {
          return formattedMehod(p)
        }),
      ]
    }

    if (totalPages > 0) {
      while (page < totalPages) {
        page++
        const result = await getFetcherCatalog(page)
        if (!!result && !!result.products) {
          products = [
            ...products,
            ...result.products.map((p) => {
              return formattedMehod(p)
            }),
          ]
        } else {
          break
        }
      }
    }
  }

  return products
}
