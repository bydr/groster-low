const SITE_URL = process.env.NEXT_PUBLIC_BASE_URL || "https://groster.me"

module.exports = {
  siteUrl: SITE_URL,
  exclude: [
    "/404",
    "/anyquery",
    "/anyquery/*",
    "/account",
    "/account/*",
    "/*.xml",
  ],
  generateRobotsTxt: true,
  robotsTxtOptions: {
    policies: [
      {
        userAgent: "*",
        disallow: [
          "/404",
          "/anyquery",
          "/anyquery/*",
          "/account",
          "/account/*",
        ],
      },
      { userAgent: "*", allow: "/" },
    ],
    additionalSitemaps: [
      `${SITE_URL}/sitemap.xml`,
      `${SITE_URL}/server-sitemap.xml`,
    ],
  },
}
