import type { FC } from "react"
import {
  InitialsBody,
  InitialsSymbol,
  InitialsWord,
} from "../../components/Typography/Typography"

type WordWithInitialsPropsType = {
  word: string
}
export const WordWithInitials: FC<WordWithInitialsPropsType> = ({ word }) => {
  return (
    <>
      <InitialsWord>
        <InitialsSymbol>{word.charAt(0)}</InitialsSymbol>
        <InitialsBody>{word.slice(1)}</InitialsBody>
      </InitialsWord>
    </>
  )
}
