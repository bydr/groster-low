import { styled } from "@linaria/react"
import { breakpoints } from "./vars"
import { PageHeading } from "./Utils"

const MAX_WIDTH_CONTAINER_DEFAULT = "1456px"

export const Container = styled.div<{
  variantWidth?: "784px" | "1456px" | "100%"
}>`
  width: 100%;
  padding: 0 15px;
  max-width: ${(props) => props.variantWidth || MAX_WIDTH_CONTAINER_DEFAULT};
  margin: 0 auto;

  @media (max-width: ${breakpoints.lg}) {
    padding: 0 32px;
  }
  @media (max-width: ${breakpoints.sm}) {
    padding: 0 15px;
  }
`
export const Col = styled.div`
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  justify-content: flex-start;
`
export const Row = styled.div`
  position: relative;
  display: flex;
  flex-wrap: wrap;
  width: 100%;
  gap: 0 15px;

  > ${Col} {
    flex: 1;
  }
`

export const ContentContainer = styled.div`
  width: 100%;
  display: flex;
  align-items: flex-start;
  flex-direction: column;
`
export const SidebarContainer = styled.div`
  grid-area: sidebar;
  grid-row: 2 / end;
  height: 100%;

  @media (max-width: ${breakpoints.lg}) {
    grid-row: 2;
    z-index: initial;
  }
`
export const RowGrid = styled.div`
  display: grid;
  width: 100%;
`
export const RowContentSidebar = styled(RowGrid)`
  gap: 40px 112px;
  align-items: flex-start;
  grid-template-areas:
    "title sidebar"
    "content sidebar";
  grid-template-columns: 9fr 3fr;

  ${PageHeading} {
    grid-area: title;
    margin-bottom: 20px;
    grid-row: initial;
  }

  > ${ContentContainer} {
    grid-area: content;
  }

  @media (max-width: ${breakpoints.xl}) {
    gap: 20px;
  }

  @media (max-width: ${breakpoints.lg}) {
    grid-template-columns: 1fr;
    gap: 20px;
    grid-template-areas:
      "title"
      "sidebar"
      "content";
  }
`
