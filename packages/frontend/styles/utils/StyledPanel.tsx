import { styled } from "@linaria/react"
import { colors } from "./vars"
import { StyledFieldWrapper } from "../../components/Field/StyledField"
import { Heading3, Span } from "../../components/Typography/Typography"
import { ButtonBase } from "../../components/Button/StyledButton"
import { css } from "@linaria/core"

export const cssPanelTranslucent = css``

export const PanelWrapperAbsolute = styled.div`
  position: absolute;
  left: auto;
  right: 0;
`

export const Panel = styled.div`
  position: relative;
  padding: 24px;
  background: ${colors.white};
  border-radius: 12px;
  border: 1px solid ${colors.gray};
  margin-bottom: 20px;

  > ${StyledFieldWrapper} {
    &:last-of-type {
      margin-bottom: 0;
    }
  }

  &.${cssPanelTranslucent} {
    background: ${colors.brand.purpleTransparent15};
    border-color: ${colors.brand.purpleTransparent15};
  }
`

export const PanelHeading = styled(Heading3)`
  margin-bottom: 8px;
  display: flex;
  flex-direction: row;
  justify-content: space-between;

  > ${Span} {
    flex: 1;
  }

  ${ButtonBase} {
    margin-bottom: 0;
    width: auto;
    height: auto;
    padding: 0;
  }
`
