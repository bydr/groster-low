import { css } from "@linaria/core"
import { styled } from "@linaria/react"
import { breakpoints, colors } from "./vars"
import {
  getTypographyBase,
  Heading2,
  Span,
  TypographyBase,
} from "../../components/Typography/Typography"
import { cssIcon } from "../../components/Icon"
import { ButtonBase, ButtonGroup } from "../../components/Button/StyledButton"
import { StyledHeader } from "../../layouts/Default/Header/StyledHeader"

export const cssNoWrap = css`
  white-space: nowrap;
`

export const cssIsActive = css``

export const StyledUtilOverlay = styled.div`
  background: ${colors.grayLight};
  height: 100vh;
  top: 0;
  position: fixed;
  z-index: 4;
  width: 100%;
`
export const Section = styled.section`
  padding-top: 56px;
  padding-bottom: 56px;

  @media (max-width: ${breakpoints.sm}) {
    padding-top: 40px;
    padding-bottom: 40px;
  }
`
export const SectionTitle = styled.p`
  ${getTypographyBase("h1")};
  width: 100%;
  max-width: 700px;
  margin-bottom: 40px;

  @media (max-width: ${breakpoints.xs}) {
    margin-bottom: 24px;
  }
`
export const SectionProducts = styled(Section)`
  padding-bottom: 26px;

  & + & {
    padding-top: 26px;
  }
`

export const cssHidden = css`
  display: none;
`

export const cssHiddenLG = css`
  @media (max-width: ${breakpoints.lg}) {
    display: none !important;
  }
`
export const cssHiddenMD = css`
  @media (max-width: ${breakpoints.md}) {
    display: none !important;
  }
`

export const PageHeading = styled.div`
  display: flex;
  flex-wrap: wrap;
  width: 100%;
  margin-bottom: 26px;
  align-items: flex-start;

  ${TypographyBase} {
    margin: 0;
  }

  > * {
    margin: 0;
  }

  ${ButtonGroup} {
    flex-wrap: wrap;
  }

  @media (max-width: ${breakpoints.xxl}) {
    display: grid;
    gap: 12px;
    grid-template-columns: 1fr;
    flex-direction: column;
    align-items: flex-start;

    > * {
      justify-content: flex-start;
    }

    ${ButtonGroup} {
      ${ButtonBase} {
        &:first-of-type {
          padding-left: 0;
        }
      }
    }
  }

  @media (max-width: ${breakpoints.sm}) {
    ${ButtonGroup} {
      display: grid;
      grid-template-columns: 40px 40px 1fr;
      justify-content: flex-end;
      justify-items: end;
    }
  }
`

export const HeadingStep = styled(Heading2)`
  ${getTypographyBase("h1")};
  color: ${colors.gray};

  > * {
    font-size: inherit;
    line-height: inherit;
    font-weight: inherit;

    &:not(:hover):not(:active) {
      color: inherit;
    }
  }

  &.${cssIsActive} {
    color: ${colors.black};
  }

  @media (max-width: ${breakpoints.sm}) {
    ${getTypographyBase("h6")};
    font-weight: bold;
  }
`

export const HeadingSteps = styled.div`
  display: inline-flex;
  align-items: center;

  .${cssIcon} {
    margin: 0 14px;
    fill: ${colors.brand.yellow};
  }
`

export const RowOffset = styled.div`
  margin-bottom: 20px;
  width: 100%;
`

export const Hr = styled.hr`
  appearance: none;
  border: none;
  margin: 20px 0;
  height: 1px;
  width: 100%;
  background-color: ${colors.gray};
`

export const StickyContainer = styled.div`
  position: sticky;
  top: 16px;
  margin-bottom: 20px;
`
export const IconPoint = styled(TypographyBase)`
  display: inline-flex;
  align-items: center;

  .${cssIcon} {
    margin-right: 6px;
  }
`

type GetBreakpointVal = (
  breakpoint: typeof breakpoints[keyof typeof breakpoints],
) => number
export const getBreakpointVal: GetBreakpointVal = (breakpoint) =>
  +breakpoint.replace("px", "")

export const cssElementIsLoading = css`
  opacity: 0.3 !important;
`

export const Wrapper = styled.div`
  position: relative;
`

export const getCustomizeScroll = (): string => {
  return `
    /* width */
    ::-webkit-scrollbar {
      width: 10px;
    }

    /* Track */
    ::-webkit-scrollbar-track {
      background: ${colors.grayLight};
      border-radius: 0;
    }

    /* Handle */
    ::-webkit-scrollbar-thumb {
      background: ${colors.gray};
      border-radius: 0;
    }
  `
}

export const cssSectionUnderHeader = css`
  ${StyledHeader} + *& {
    z-index: initial;
  }
`

export const Trigger = styled(Span)`
  color: ${colors.brand.purple};
  cursor: pointer;

  &:hover,
  &:active {
    color: ${colors.brand.purpleDarken};
  }
`

export const cssNoScroll = css`
  overflow: hidden !important;
  position: sticky !important;
  padding-right: 17px !important;
  -webkit-overflow-scrolling: initial !important;
  user-select: none !important;
  height: 100vh !important;
  bottom: 0;

  @media (max-width: ${breakpoints.sm}) {
    padding-right: 0 !important;
  }
`
export const PageWrapper = styled.main`
  position: relative;
  display: flex;
  flex: auto;
  flex-direction: column;
  min-height: 0;
`
