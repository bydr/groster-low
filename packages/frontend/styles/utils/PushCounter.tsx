import { styled } from "@linaria/react"
import type { FC } from "react"
import { getTypographyBase } from "../../components/Typography/Typography"
import { colors } from "./vars"

export const StyledPushCounter = styled.span`
  ${getTypographyBase("p10")};
  color: ${colors.white};
  background: ${colors.brand.orange};
  border-radius: 50px;
  padding: 2px;
  min-width: 16px;
  min-height: 16px;
  line-height: 100%;
  display: flex;
  align-items: center;
  justify-content: center;
  text-align: center;
  z-index: 2;
`
export const PushCounterOverlay = styled(StyledPushCounter)`
  background: inherit;
  min-width: 20px;
  min-height: 20px;
  z-index: 1;
`

const PushCounter: FC = ({ children }) => {
  return (
    <>
      <StyledPushCounter>{children}</StyledPushCounter>
      <PushCounterOverlay />
    </>
  )
}

export default PushCounter
