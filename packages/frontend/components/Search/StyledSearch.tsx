import { styled } from "@linaria/react"
import { Heading1, Heading3, Span } from "components/Typography/Typography"
import { breakpoints, colors } from "../../styles/utils/vars"
import { PageHeading } from "../../styles/utils/Utils"
import {
  StyledCatalogProducts,
  StyledProducts,
} from "../Products/StyledProducts"
import { StyledTags, StyledTagsContainer } from "../Tags/StyledTags"
import { AccountPage } from "../Account/StyledAccount"
import { StyledAutoComplete } from "./AutoComplete/StyledAutoComplete"
import { StyledField, StyledFieldForm } from "./Field/StyledField"
import { StyledBannerSection } from "../Banners/Banner/Styled"

export const SearchPageHeading = styled(PageHeading)`
  flex-direction: column;
  max-width: 600px;
  width: 100%;
`

export const SearchPageTitle = styled(Heading1)`
  color: ${colors.grayDark};

  > ${Span} {
    color: ${colors.black};
  }
`

export const SearchPageSubtitle = styled(Heading3)`
  color: ${colors.grayDark};

  > ${Span} {
    color: ${colors.black};
  }
`

export const StyledSearch = styled.section`
  position: relative;
  width: 100%;
  margin-bottom: 70px;

  ${StyledCatalogProducts} {
    &[data-view-products] {
      ${StyledProducts} {
        grid-template-columns: repeat(4, 1fr);

        ${StyledBannerSection} {
          grid-column: 3 / -1;
        }

        @media (max-width: ${breakpoints.xl}) {
          grid-template-columns: repeat(3, 1fr);

          ${StyledBannerSection} {
            grid-column: 2 / -1;
          }
        }

        @media (max-width: ${breakpoints.lg}) {
          grid-template-columns: repeat(2, 1fr);
        }

        @media (max-width: ${breakpoints.md}) {
          ${StyledBannerSection} {
            grid-column: 1 / -1;
          }
        }

        @media (max-width: ${breakpoints.sm}) {
          grid-template-columns: 1fr;
        }
      }
    }
  }

  ${StyledTagsContainer} {
    ${StyledTags} {
      width: 100%;
      overflow: auto;
      white-space: nowrap;
      flex-wrap: nowrap;
      padding: 4px 0 8px 0;
    }
  }

  @media (max-width: ${breakpoints.lg}) {
    display: block;
  }
`

export const StyledSearchPopup = styled.div`
  position: fixed;
  top: 0;
  left: 0;
  bottom: 0;
  right: 0;
  z-index: 9;
  background: ${colors.white};
  height: 100vh;
  padding: 0;

  ${StyledField} {
    display: flex;
    flex-direction: column;
    height: 100%;
    ${StyledFieldForm} {
      padding: 10px 10px 0 10px;
    }

    ${StyledAutoComplete} {
      position: relative;
      top: auto;
      flex: 1;
      max-height: initial;
      overflow: hidden auto;
      margin-top: 10px;
    }
  }
`

export const SearchPage = styled(AccountPage)``
