import { styled } from "@linaria/react"
import { ButtonBox } from "../../Button/StyledButton"
import { breakpoints, colors } from "../../../styles/utils/vars"
import { cssIcon } from "../../Icon"
import { css } from "@linaria/core"
import { StyledFieldWrapper, StyledInput } from "../../Field/StyledField"

export const cssSearchSubmit = css`
  font-size: 12px;
  padding: 0 8px;
`

export const cssBackIsActive = css`
  background: ${colors.grayLight};

  .${cssIcon} {
    fill: ${colors.brand.purple} !important;
  }
`

export const StyledField = styled.div`
  width: 100%;
`

export const StyledFieldForm = styled.form`
  width: 100%;
  position: relative;
  z-index: 9;
  display: flex;
  align-items: center;

  > ${ButtonBox} {
    margin-right: 8px;
    padding: 0;
    width: 34px;
    height: 34px;

    .${cssIcon} {
      fill: ${colors.grayDarkLight};
    }

    &:hover,
    &:active {
      .${cssIcon} {
        fill: ${colors.grayDarkLight};
      }
    }
  }

  ${StyledFieldWrapper} {
    margin: 0;
    z-index: 3;
    background: ${colors.white};
    width: auto;
    flex: 1;

    ${StyledInput} {
      min-height: 48px;
    }

    @media (max-width: ${breakpoints.lg}) {
      ${StyledInput} {
        min-height: 30px;
      }
    }
  }
`
