import { FC, useEffect, useMemo } from "react"
import { Field } from "../../Field/Field"
import { Button } from "../../Button"
import { useSearch } from "../../../hooks/search"
import {
  cssBackIsActive,
  cssSearchSubmit,
  StyledField,
  StyledFieldForm,
} from "./StyledField"
import { Controller, useForm } from "react-hook-form"
import { useRouter } from "next/router"
import { cx } from "@linaria/core"
import { StyledAutoCompleteOverlay } from "../AutoComplete/StyledAutoComplete"
import { cssIsActive } from "../../../styles/utils/Utils"
import dynamic, { DynamicOptions } from "next/dynamic"
import type { AutoCompletePropsType } from "../AutoComplete"

const DynamicAutoComplete = dynamic((() =>
  import("../AutoComplete").then(
    (mod) => mod.AutoComplete,
  )) as DynamicOptions<AutoCompletePropsType>)

export const SearchField: FC<{
  isResponsiveMode?: boolean
  onBackCb?: () => void
  isFocusInit?: boolean
}> = ({ isResponsiveMode = false, onBackCb, isFocusInit = false }) => {
  const {
    updateQueryText,
    appendToHistory,
    currentText,
    correction,
    query,
    updateSearch,
    hideAutoComplete,
    showAutoComplete,
    isShowAutoComplete,
    setIsSubmitting,
    inputRef,
    autoComplete: { isInit, ref },
    clearAutoComplete,
  } = useSearch()
  const router = useRouter()

  const {
    handleSubmit,
    control,
    setValue,
    formState: { submitCount, isSubmitting },
  } = useForm<{
    query: string
  }>({
    defaultValues: {
      query: currentText || "",
    },
  })

  const _isFocusInit = useMemo(() => isFocusInit, [isFocusInit])

  useEffect(() => {
    if (_isFocusInit) {
      inputRef.current?.click()
    }
  }, [_isFocusInit, inputRef])

  useEffect(() => {
    setIsSubmitting(isSubmitting)
  }, [isSubmitting, setIsSubmitting])

  const onSubmit = handleSubmit((data) => {
    hideAutoComplete()

    if (data.query.length > 0) {
      appendToHistory({
        query: data.query,
      })
    }

    updateSearch({
      query: data.query,
      withReset: true,
    })
  })

  useEffect(() => {
    setValue("query", currentText || "", {
      shouldDirty: true,
      shouldTouch: true,
    })
  }, [currentText, setValue])

  useEffect(() => {
    if (
      correction !== null &&
      correction !== currentText &&
      query !== null &&
      query === currentText
    ) {
      setValue("query", correction || "", {
        shouldDirty: false,
        shouldTouch: false,
      })
    }
  }, [correction, setValue, currentText, submitCount, query])

  return (
    <>
      <StyledField ref={ref}>
        <StyledFieldForm onSubmit={onSubmit}>
          {isResponsiveMode && (
            <>
              <Button
                variant={"box"}
                icon={isShowAutoComplete ? "X" : "ArrowLeft"}
                type={"button"}
                className={cx(isShowAutoComplete && cssBackIsActive)}
                onClick={() => {
                  if (onBackCb !== undefined) {
                    onBackCb()
                  } else {
                    if (isShowAutoComplete) {
                      hideAutoComplete()
                    } else {
                      void router.push("/")
                    }
                  }
                }}
              />
            </>
          )}

          <Controller
            control={control}
            name={"query"}
            render={({ field }) => {
              return (
                <Field
                  value={field.value}
                  name={"query"}
                  id={"id-search"}
                  placeholder={"Например, стакан ..."}
                  type={"text"}
                  variant={"input"}
                  withAnimatingLabel={false}
                  withButton
                  iconButton={isResponsiveMode ? undefined : "Search"}
                  className={"searchField"}
                  classNameButton={cssSearchSubmit}
                  textButton={isResponsiveMode ? "Найти" : undefined}
                  variantButton={isResponsiveMode ? "filled" : undefined}
                  withClean={!!field.value?.length}
                  cleanCb={clearAutoComplete}
                  onChange={(e) => {
                    field.onChange(e)
                    updateQueryText(e.target.value)
                  }}
                  onClick={() => {
                    if (isShowAutoComplete) {
                      return
                    }
                    showAutoComplete()
                  }}
                  ref={inputRef}
                />
              )
            }}
          />
        </StyledFieldForm>

        {isShowAutoComplete && (
          <>
            <DynamicAutoComplete
              onHide={() => {
                hideAutoComplete()
              }}
              fieldRef={ref}
              selectQuery={(query: string) => {
                setValue("query", query, {
                  shouldDirty: true,
                  shouldTouch: true,
                })
                void onSubmit()
              }}
              showAllResults={() => {
                void onSubmit()
              }}
            />
          </>
        )}
      </StyledField>
      <StyledAutoCompleteOverlay
        className={cx(isInit && isShowAutoComplete && cssIsActive)}
      />
    </>
  )
}
