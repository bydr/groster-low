import { FC } from "react"
import { Typography } from "../Typography/Typography"
import { breakpoints, colors } from "../../styles/utils/vars"
import {
  SearchPageHeading,
  SearchPageSubtitle,
  SearchPageTitle,
  StyledSearch,
} from "./StyledSearch"
import { SEARCH_PER_PAGE, useSearch } from "../../hooks/search"
import { DigiSearchResponseType, DigiSortType } from "../../api/digineticaAPI"
import { ProductDetailListType } from "../../../contracts/contracts"
import {
  cssTagRectification,
  StyledTags,
  StyledTagsContainer,
} from "../Tags/StyledTags"
import { Button } from "../Button"
import { VIEW_PRODUCTS_GRID } from "../../types/types"
import { CatalogProducts } from "../Products/Catalog/CatalogProducts"
import { ButtonToTop } from "../Button/ButtonToTop"
import { declinationOfNum } from "utils/helpers"
import { Paginator } from "../Paginator/Paginator"
import { Filters } from "./Filters"
import { useWindowSize } from "../../hooks/windowSize"
import { getBreakpointVal } from "../../styles/utils/Utils"

export type ExtendSearchResultType = DigiSearchResponseType & {
  productsData: ProductDetailListType
  filter?: string[]
  page: string | null
  sortQuery: DigiSortType | null
}

export type SearchPropsType = {
  initQuery: string | null
  searchResult: ExtendSearchResultType | null
  filterQuery: string[] | null
}

const PRODUCTS_STRING = ["товар", "товара", "товаров"]

export const Search: FC<SearchPropsType> = ({
  searchResult,
  initQuery,
  filterQuery,
}) => {
  const { width } = useWindowSize()

  const {
    query,
    isUseCorrection,
    correction,
    zeroQueries,
    total,
    isNotResults,
    autoComplete: { taps },
    productsData,
    page,
    updateSearch,
    clearAutoComplete,
  } = useSearch({
    initialQuery: initQuery,
    search: searchResult || undefined,
    filterQuery: filterQuery,
  })

  return (
    <>
      {query !== null && (
        <>
          {query?.length <= 0 ? (
            <StyledSearch>
              <SearchPageHeading>
                <SearchPageTitle>
                  <Typography variant={"span"}>
                    Введите поисковый запрос
                  </Typography>
                </SearchPageTitle>
              </SearchPageHeading>
            </StyledSearch>
          ) : (
            <StyledSearch>
              <SearchPageHeading>
                {isUseCorrection && (
                  <>
                    <Typography variant={"p14"}>
                      Запрос{" "}
                      <Typography variant={"span"} color={colors.brand.purple}>
                        «{query || ""}»
                      </Typography>{" "}
                      был исправлен на{" "}
                      <Typography variant={"span"} color={colors.brand.purple}>
                        «{correction || ""}»
                      </Typography>
                      .{" "}
                      {(total || 0) > 0 && (
                        <>
                          Найдено {total}{" "}
                          {declinationOfNum(total || 0, PRODUCTS_STRING)}
                        </>
                      )}
                    </Typography>
                  </>
                )}

                {isNotResults ? (
                  <>
                    <SearchPageTitle>
                      По вашему запросу{" "}
                      <Typography variant={"span"}>
                        «{isUseCorrection ? correction || "" : query || ""}»
                      </Typography>{" "}
                      ничего не найдено.
                    </SearchPageTitle>
                    <br />
                    <Button variant={"outline"} onClick={clearAutoComplete}>
                      Изменить запрос
                    </Button>
                  </>
                ) : (
                  <>
                    <SearchPageTitle>
                      Вы искали{" "}
                      <Typography variant={"span"}>
                        «{isUseCorrection ? correction || "" : query || ""}»
                      </Typography>
                    </SearchPageTitle>
                    {(total || 0) > 0 && (
                      <>
                        <SearchPageSubtitle>
                          Найдено{" "}
                          <Typography variant={"span"}>
                            {total}{" "}
                            {declinationOfNum(total || 0, PRODUCTS_STRING)}
                          </Typography>
                        </SearchPageSubtitle>
                      </>
                    )}
                  </>
                )}
              </SearchPageHeading>

              {taps.length > 0 && (
                <>
                  <StyledTagsContainer>
                    <Typography variant={"p14"}>Уточните:</Typography>
                    <StyledTags>
                      {taps.map((t, i) => (
                        <Button
                          key={i}
                          className={cssTagRectification}
                          onClick={() => {
                            updateSearch({
                              query: t.relatedSearch,
                              withReset: true,
                            })
                          }}
                        >
                          {t.relatedSearch}
                        </Button>
                      ))}
                    </StyledTags>
                  </StyledTagsContainer>
                </>
              )}

              <Filters />

              {zeroQueries && !isNotResults && (
                <>
                  <Typography variant={"p14"}>
                    Возможно вас заинтересует
                  </Typography>
                </>
              )}

              {productsData.length > 0 && (
                <>
                  <CatalogProducts
                    view={VIEW_PRODUCTS_GRID}
                    products={productsData}
                    withBanner={false}
                  />
                </>
              )}

              <Paginator
                totalCount={total || 0}
                itemsPerPage={SEARCH_PER_PAGE}
                currentPage={page || 0}
                partsPerPage={
                  width !== undefined &&
                  width > getBreakpointVal(breakpoints.sm)
                    ? undefined
                    : 3
                }
                onPageChanged={(page) => {
                  updateSearch({
                    page: page,
                  })
                }}
                onPageAdditional={(page) => {
                  updateSearch({
                    isAppend: true,
                    page: page,
                  })
                }}
              />

              <ButtonToTop />
            </StyledSearch>
          )}
        </>
      )}
    </>
  )
}
