import { FC, useContext } from "react"
import { FilterPopover } from "../../Filter/Popover/FilterPopover"
import {
  StyledFilterInner,
  StyledFilterItem,
  StyledFilterList,
  StyledFilters,
  TotalControls,
} from "../../Filter/StyledFilter"
import { Range } from "../../Range/Range"
import {
  FACET_CATEGORIES_KEY,
  translationNameFacet,
  useSearch,
} from "../../../hooks/search"
import { CustomCheckbox } from "../../Checkbox/CustomCheckbox"
import { StyledFilterCounter } from "../../Filter/Popover/StyledFilterPopover"
import { Button } from "../../Button"
import { cx } from "@linaria/core"
import { cssHiddenLG, getBreakpointVal } from "../../../styles/utils/Utils"
import { cssButtonClearFilters } from "../../Button/StyledButton"
import { useWindowSize } from "../../../hooks/windowSize"
import { breakpoints } from "../../../styles/utils/vars"
import { Typography } from "../../Typography/Typography"
import { ModalContext } from "../../Modals/Modal"
import { normalizeValueToSort } from "../../../utils/helpers"

export const FilterParams: FC<{
  isResponsive?: boolean
  visibleDefault?: boolean
}> = ({ visibleDefault, isResponsive }) => {
  const { facets, selectedFacets, updateFilter, filter, resetFilter, total } =
    useSearch()

  const { width } = useWindowSize()

  const modalContext = useContext(ModalContext)

  return (
    <>
      <StyledFilters>
        {facets
          .filter((f) =>
            width !== undefined && width <= getBreakpointVal(breakpoints.lg)
              ? true
              : f.name !== FACET_CATEGORIES_KEY,
          )
          .map((facet) => {
            switch (facet.dataType) {
              case "DISTINCT":
              case "NUM_DISTINCT":
                return (
                  <FilterPopover
                    isResponsive={isResponsive}
                    visibleDefault={visibleDefault}
                    key={facet.name}
                    title={translationNameFacet(facet.name || "")}
                    isActive={!!(filter || {})[facet.name]}
                    selectedKeys={(filter || {})[facet.name] || []}
                    onClearFilterItem={() => {
                      updateFilter({
                        type: facet.dataType,
                        payload: {
                          name: facet.name,
                          value: null,
                        },
                      })
                    }}
                    name={facet.name}
                  >
                    <StyledFilterInner>
                      <StyledFilterList>
                        {[...facet.values]
                          .sort((a, b) => {
                            const nameA = normalizeValueToSort(a.name)
                            const nameB = normalizeValueToSort(b.name)
                            return nameA > nameB ? 1 : nameA < nameB ? -1 : 0
                          })
                          .map((item) => {
                            return (
                              <StyledFilterItem key={item.id}>
                                <CustomCheckbox
                                  variant={"check"}
                                  name={item.name || ""}
                                  message={item.name || "Все"}
                                  stateCheckbox={(
                                    (filter || {})[facet.name] || []
                                  ).includes(item.id)}
                                  onChange={(e) => {
                                    updateFilter({
                                      type: facet.dataType,
                                      payload: {
                                        name: facet.name,
                                        value: item.id,
                                        checked: e.currentTarget.checked,
                                      },
                                    })
                                  }}
                                />
                                {!!item.value && (
                                  <>
                                    <StyledFilterCounter>
                                      {item.value}
                                    </StyledFilterCounter>
                                  </>
                                )}
                              </StyledFilterItem>
                            )
                          })}
                      </StyledFilterList>
                    </StyledFilterInner>
                  </FilterPopover>
                )
              case "SLIDER":
                return (
                  <FilterPopover
                    isResponsive={isResponsive}
                    visibleDefault={visibleDefault}
                    title={translationNameFacet(facet.name)}
                    isActive={
                      !!selectedFacets.find((f) => f.name === facet.name)
                    }
                    onClearFilterItem={() => {
                      updateFilter({
                        type: facet.dataType,
                        payload: {
                          name: facet.name,
                          value: null,
                        },
                      })
                    }}
                  >
                    <StyledFilterInner>
                      <Range
                        defaultValue={{
                          min:
                            facet.values.find((v) => v.id === "min")?.value ||
                            0,
                          max:
                            facet.values.find((v) => v.id === "max")?.value ||
                            0,
                        }}
                        value={
                          filter !== null && !!filter[facet.name]
                            ? {
                                min: +filter[facet.name][0] || 0,
                                max: +filter[facet.name][1] || 0,
                              }
                            : undefined
                        }
                        onAfterChange={(value) => {
                          updateFilter({
                            type: facet.dataType,
                            payload: {
                              name: facet.name,
                              value:
                                value === undefined
                                  ? null
                                  : {
                                      min: value[0],
                                      max: value[1],
                                    },
                            },
                          })
                        }}
                      />
                    </StyledFilterInner>
                  </FilterPopover>
                )
            }
          })}

        {Object.keys(filter || {}).length > 0 && (
          <Button
            variant={"link"}
            icon={"Rotate"}
            className={cx(cssHiddenLG, cssButtonClearFilters)}
            onClick={resetFilter}
          >
            Очистить фильтры
          </Button>
        )}
      </StyledFilters>
      <TotalControls>
        <Button
          variant={"filled"}
          size={"large"}
          onClick={(e) => {
            e.preventDefault()
            modalContext?.hide()
          }}
        >
          Показать{" "}
          <Typography
            variant={"p12"}
            style={{
              marginLeft: "4px",
            }}
          >
            {" "}
            ({total || 0})
          </Typography>
        </Button>
        <Button
          variant={"outline"}
          size={"large"}
          onClick={resetFilter}
          disabled={!(Object.keys(filter || {}).length > 0)}
        >
          Сбросить все
        </Button>
      </TotalControls>
    </>
  )
}
