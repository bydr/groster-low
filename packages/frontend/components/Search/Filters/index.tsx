import { FC, useRef } from "react"
import {
  cssButtonFilterDialogTrigger,
  StyledFilterColumn,
  StyledFilterGroup,
  StyledFilterRow,
  StyledFiltersContainer,
} from "../../Filter/StyledFilter"
import { useSearch } from "../../../hooks/search"
import { FilterParams } from "./FilterParams"
import { cssIsActive, getBreakpointVal } from "../../../styles/utils/Utils"
import { breakpoints } from "../../../styles/utils/vars"
import Button from "../../Button/Button"
import { useWindowSize } from "../../../hooks/windowSize"
import { Modal } from "../../Modals/Modal"
import { cx } from "@linaria/core"
import { Select } from "../../Select/Select"
import { DigiSortType } from "../../../api/digineticaAPI"
import { cssButtonFill, cssButtonFloat } from "../../Button/StyledButton"
import PushCounter from "../../../styles/utils/PushCounter"

export const Filters: FC = () => {
  const { width } = useWindowSize()
  const { facets, filter, sort, updateSearch, selectedFacets } = useSearch()
  const btnFilterResponsiveRef = useRef<HTMLButtonElement>(null)

  return (
    <>
      {facets.length > 0 && (
        <>
          <StyledFiltersContainer>
            <StyledFilterRow>
              <StyledFilterColumn>
                <StyledFilterGroup>
                  {width !== undefined &&
                    width > getBreakpointVal(breakpoints.lg) && (
                      <>
                        <FilterParams />
                      </>
                    )}

                  <Modal
                    variant={"full"}
                    title={"Фильтр"}
                    closeMode={"destroy"}
                    disclosure={
                      <Button
                        variant={"small"}
                        icon={"Filter"}
                        className={cx(
                          cssButtonFilterDialogTrigger,
                          Object.keys(filter || {}).length > 0 && cssIsActive,
                        )}
                        ref={btnFilterResponsiveRef}
                      >
                        Фильтр
                      </Button>
                    }
                  >
                    <FilterParams isResponsive visibleDefault />
                  </Modal>
                  {width !== undefined &&
                    width <= getBreakpointVal(breakpoints.lg) && (
                      <>
                        <Button
                          variant={"box"}
                          icon={"Filter"}
                          className={cx(cssButtonFloat, cssButtonFill)}
                          onClick={() => {
                            btnFilterResponsiveRef.current?.click()
                          }}
                        >
                          {Object.keys(filter || {}).length > 0 && (
                            <>
                              <PushCounter>{selectedFacets.length}</PushCounter>
                            </>
                          )}
                        </Button>
                      </>
                    )}
                </StyledFilterGroup>
              </StyledFilterColumn>

              <StyledFilterColumn>
                <StyledFilterGroup>
                  <Select
                    variant={"small"}
                    items={[
                      {
                        icon: "ArrowUp",
                        name: "Цена",
                        value: "PRICE_ASC" as DigiSortType,
                      },
                      {
                        icon: "ArrowDown",
                        name: "Цена",
                        value: "PRICE_DESC" as DigiSortType,
                      },
                      {
                        name: "По умолчанию",
                        value: "DEFAULT" as DigiSortType,
                      },
                    ]}
                    isIcon={true}
                    ariaLabel={"sorted"}
                    initialValue={sort || undefined}
                    onSelectValue={(value) => {
                      updateSearch({
                        sort: value as DigiSortType,
                      })
                    }}
                  />
                </StyledFilterGroup>
              </StyledFilterColumn>
            </StyledFilterRow>
          </StyledFiltersContainer>
        </>
      )}
    </>
  )
}
