import dynamic, { DynamicOptions } from "next/dynamic"
import { FilterPopoverType } from "../../Filter/Popover/FilterPopover"
import { FC, FormEvent, useCallback, useEffect, useState } from "react"
import { ClearItemFilterPropsType } from "../../../types/types"
import {
  IAvailableParam,
  IAvailableParams,
  IFilterParamChild,
} from "../../../store/reducers/catalogSlice"
import {
  StyledFilterImage,
  StyledFilterInner,
  StyledFilterItem,
  StyledFilterList,
} from "../../Filter/StyledFilter"
import { CustomCheckbox } from "../../Checkbox/CustomCheckbox"
import { StyledFilterCounter } from "../../Filter/Popover/StyledFilterPopover"
import { Button } from "../../Button"

const DynamicFilterPopover = dynamic((() =>
  import("../../Filter/Popover/FilterPopover").then(
    (mod) => mod.FilterPopover,
  )) as DynamicOptions<FilterPopoverType>)

export type FilterHandlersType = {
  onUpdateFiltersHandle: <T>(props?: T) => void
  onChangeCheckboxHandle: (
    e: FormEvent<HTMLInputElement>,
    isDisabledField: boolean,
  ) => void
  onClearItemHandle: ({
    keyFilter,
    checked,
    name,
  }: ClearItemFilterPropsType) => void
}

export const FilterList: FC<
  {
    filters: IAvailableParams | null
  } & FilterHandlersType
> = ({
  filters,
  onUpdateFiltersHandle,
  onClearItemHandle,
  onChangeCheckboxHandle,
}) => {
  const [filtersSorted, setFiltersSorted] = useState<IAvailableParam[]>([])

  const clearItemHandle = useCallback(
    (props: { keyFilter: string[]; checked: boolean; name?: string }) => {
      onClearItemHandle(props)
    },
    [onClearItemHandle],
  )

  const updateFiltersHandle: <T>(props?: T) => void = useCallback(
    (props) => {
      onUpdateFiltersHandle(props)
    },
    [onUpdateFiltersHandle],
  )

  const changeCheckboxHandle = useCallback(
    (e: FormEvent<HTMLInputElement>, isDisabledField: boolean) => {
      onChangeCheckboxHandle(e, isDisabledField)
    },
    [onChangeCheckboxHandle],
  )

  useEffect(() => {
    if (filters !== null) {
      setFiltersSorted(
        Object.keys(filters)
          .map((key) => filters[key])
          .sort((a, b) =>
            (a?.name || "") > (b?.name || "")
              ? 1
              : (a?.name || "") < (b?.name || "")
              ? -1
              : 0,
          )
          .sort((a, b) =>
            (a?.order || 0) > (b?.order || 0)
              ? 1
              : (a?.order || 0) < (b?.order || 0)
              ? -1
              : 0,
          ),
      )
    } else {
      setFiltersSorted([])
    }
  }, [filters])

  return (
    <>
      {filtersSorted.length > 0 &&
        filtersSorted.map((filter) => {
          return (
            <DynamicFilterPopover
              key={filter.uuid || ""}
              title={filter.name || ""}
              isActive={!!filter.checkedKeys.length}
              selectedKeys={filter.checkedKeys}
              onClearFilterItem={clearItemHandle}
              name={filter.uuid}
            >
              <StyledFilterInner>
                <StyledFilterList>
                  {Object.keys(filter.params)
                    .sort((a, b) =>
                      (filter.params[a].name || "") >
                      (filter.params[b].name || "")
                        ? 1
                        : (filter.params[a].name || "") <
                          (filter.params[b].name || "")
                        ? -1
                        : 0,
                    )
                    .map((key) => {
                      const val: IFilterParamChild = filter.params[key]

                      return (
                        <StyledFilterItem
                          key={val.uuid}
                          isDisabled={
                            val.product_qty !== undefined
                              ? val.product_qty <= 0
                              : false
                          }
                        >
                          <CustomCheckbox
                            variant={"check"}
                            name={val.uuid || ""}
                            message={val?.name || "Фильтр"}
                            stateCheckbox={val.checked}
                            onChange={(e) => changeCheckboxHandle(e, false)}
                          />
                          {!!val.product_qty && (
                            <>
                              <StyledFilterCounter>
                                {val.product_qty}
                              </StyledFilterCounter>
                            </>
                          )}
                          {val.image && (
                            <StyledFilterImage
                              style={{
                                backgroundImage: `url(${val.image})`,
                              }}
                            />
                          )}
                        </StyledFilterItem>
                      )
                    })}
                </StyledFilterList>
                <Button
                  variant={"filled"}
                  onClick={(e) => {
                    e.preventDefault()
                    updateFiltersHandle()
                  }}
                >
                  Применить
                </Button>
              </StyledFilterInner>
            </DynamicFilterPopover>
          )
        })}
    </>
  )
}
