import { FC, useCallback, useEffect, useState } from "react"
import Sidebar from "../../Sidebar/Sidebar"
import { StyledList, StyledListItem } from "../../List/StyledList"
import { FACET_CATEGORIES_KEY, useSearch } from "../../../hooks/search"
import { DigiFacetType } from "../../../api/digineticaAPI"
import { cx } from "@linaria/core"
import { cssIsActive } from "../../../styles/utils/Utils"
import {
  StyledCategoryLink,
  StyledCategoryName,
  StyledCategoryQty,
} from "../../Catalog/Categories/Category/StyledCategory"

export const SidebarSearch: FC = () => {
  const { total, facets, updateFilter, selectedFacets, resetFilter, filter } =
    useSearch()
  const [categories, setCategories] = useState<DigiFacetType | null>(null)

  const updateCategorySearch = useCallback(
    ({ cId }: { cId: string }) => {
      if (categories === null) {
        return
      }
      updateFilter({
        type: categories.dataType,
        payload: {
          name: categories.name,
          value: cId,
          checked: true,
        },
        isOnce: true,
      })
    },
    [categories, updateFilter],
  )

  useEffect(() => {
    setCategories(
      facets.find((facet) => facet.name === FACET_CATEGORIES_KEY) || null,
    )
  }, [facets])

  return (
    <>
      <Sidebar>
        <StyledList>
          <StyledListItem
            className={cx(
              Object.keys(filter || {}).length === 0 && cssIsActive,
            )}
            onClick={() => {
              resetFilter()
            }}
          >
            <StyledCategoryLink>
              <StyledCategoryName>
                Все результаты{" "}
                <StyledCategoryQty>{total || 0}</StyledCategoryQty>
              </StyledCategoryName>
            </StyledCategoryLink>
          </StyledListItem>
          {(categories?.values || []).map((c) => (
            <StyledListItem
              key={c.id}
              className={cx(
                (c.selected ||
                  (Object.keys(filter || {}).length > 0 &&
                    !!selectedFacets
                      .find((sf) => sf.name === FACET_CATEGORIES_KEY)
                      ?.values.find((v) => v.id === c.id)) ||
                  !!(filter || {})[FACET_CATEGORIES_KEY]?.includes(c.id)) &&
                  cssIsActive,
              )}
              data-id={c.id}
              onClick={() => {
                updateCategorySearch({
                  cId: c.id,
                })
              }}
            >
              <StyledCategoryLink>
                <StyledCategoryName>
                  {c.name} <StyledCategoryQty>{c.value}</StyledCategoryQty>
                </StyledCategoryName>
              </StyledCategoryLink>
            </StyledListItem>
          ))}
        </StyledList>
      </Sidebar>
    </>
  )
}
