import { styled } from "@linaria/react"
import { StyledPriceWrapper } from "../../../Products/parts/Price/StyledPrice"
import { StyledEntityImage } from "../../../EntityImage/Styled"
import { cssIcon, getSizeSVG } from "../../../Icon"
import { breakpoints, colors } from "styles/utils/vars"

export const StyledProduct = styled.div`
  display: grid;
  width: 100%;
  align-items: center;
  justify-content: space-between;
  grid-template-columns: 65px 8fr 4fr;
  gap: 4px 10px;
  padding: 4px 0;
  justify-items: flex-start;
  border-bottom: 1px solid ${colors.gray};

  &:last-child {
    border-bottom: 1px solid transparent;
  }

  ${StyledPriceWrapper} {
    width: 100%;
    justify-content: flex-end;
  }

  ${StyledEntityImage} {
    text-align: left;
    grid-row: 1 / 3;

    .${cssIcon} {
      ${getSizeSVG("32px")};
      margin: 8px 0;
    }
  }

  @media (max-width: ${breakpoints.sm}) {
    grid-template-columns: 65px 1fr;

    ${StyledPriceWrapper} {
      grid-column: 2/2;
      grid-row: 2;
      justify-content: flex-start;
    }
  }
`
export const StyledProducts = styled.div`
  width: 100%;
  display: flex;
  flex-direction: column;
`
