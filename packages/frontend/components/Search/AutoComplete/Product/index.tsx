import { FC } from "react"
import { DigiProductAutoCompleteType } from "../../../../api/digineticaAPI"
import { StyledProduct } from "./StyledProduct"
import { EntityImage } from "../../../EntityImage/EntityImage"
import { Title } from "../../../Products/parts/Title/Title"
import Price from "../../../Products/parts/Price/Price"
import { CURRENCY } from "../../../../utils/constants"

export const Product: FC<{
  product: DigiProductAutoCompleteType
  onRoute?: () => void
}> = ({ product, onRoute }) => {
  return (
    <>
      <StyledProduct>
        <EntityImage
          imagePath={product.image_url}
          quality={15}
          width={65}
          height={65}
          priority
        />
        <Title name={product.name} path={product.link_url} onRoute={onRoute} />
        <Price
          price={+product.price}
          currency={CURRENCY}
          variant={"unit"}
          unitMeasure={"шт"}
          withBg
        />
      </StyledProduct>
    </>
  )
}
