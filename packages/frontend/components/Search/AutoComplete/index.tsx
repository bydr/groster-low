import { FC, MutableRefObject, useCallback, useEffect } from "react"
import { useSearch } from "../../../hooks/search"
import {
  cssCategories,
  cssHistory,
  cssProducts,
  cssSts,
  cssTaps,
  cssWithIcon,
  StyledAutoComplete,
  StyledResult,
  StyledResultItem,
  StyledResultItemInner,
  StyledResultList,
  StyledResultListArea,
  StyledResultTitle,
} from "./StyledAutoComplete"
import { Typography } from "../../Typography/Typography"
import { cx } from "@linaria/core"
import { cssIsActive, getBreakpointVal } from "../../../styles/utils/Utils"
import { Button } from "../../Button"
import { StyledProducts } from "./Product/StyledProduct"
import { Product } from "./Product"
import { Icon } from "../../Icon"
import { Category } from "./Category"
import {
  cssTagRectification,
  StyledTags,
  StyledTagsContainer,
} from "../../Tags/StyledTags"
import { breakpoints } from "../../../styles/utils/vars"
import { useWindowSize } from "../../../hooks/windowSize"
import { BaseLoader } from "../../Loaders/BaseLoader/BaseLoader"

const LIMIT_SHOW_HISTORY = 5
const LIMIT_SHOW_HISTORY_HINTS = 3

export type AutoCompletePropsType = {
  onHide?: () => void
  fieldRef?: MutableRefObject<HTMLDivElement | null>
  selectQuery?: (query: string) => void
  showAllResults?: () => void
}

export const AutoComplete: FC<AutoCompletePropsType> = ({
  onHide,
  fieldRef,
  selectQuery,
  showAllResults,
  ...props
}) => {
  const {
    history,
    removeFromHistory,
    clearHistory,
    currentText,
    autoComplete: { products, taps, sts, categories, query, setIsInit, isInit },
    updateSearch,
    hideAutoComplete,
    isLoadingAutoComplete,
  } = useSearch()

  const { width } = useWindowSize()

  const selectItem = useCallback(
    (query: string) => {
      if (selectQuery) {
        selectQuery(query)
      }
    },
    [selectQuery],
  )

  useEffect(() => {
    setIsInit(true)
  }, [setIsInit])

  useEffect(() => {
    const handler = (event: MouseEvent | TouchEvent) => {
      if (!fieldRef?.current?.contains(event.target as Node | null)) {
        setIsInit(false)
        setTimeout(() => {
          if (onHide) {
            onHide()
          }
        }, 200)
      }
    }

    document.addEventListener("mousedown", handler)
    document.addEventListener("touchstart", handler)

    return () => {
      document.removeEventListener("mousedown", handler)
      document.removeEventListener("touchstart", handler)
    }
  }, [fieldRef, onHide, setIsInit])

  const updateCategorySearch = useCallback(
    ({ title }: { cId: string; title: string }) => {
      updateSearch({
        query: title,
        withReset: true,
      })
    },
    [updateSearch],
  )

  return (
    <>
      <StyledAutoComplete {...props} className={cx(isInit && cssIsActive)}>
        {isLoadingAutoComplete && <BaseLoader />}
        <StyledResultListArea>
          {taps.length > 0 && (
            <>
              <StyledResult className={cssTaps}>
                <StyledTagsContainer>
                  <StyledTags>
                    {taps.map((t, i) => (
                      <Button
                        key={i}
                        className={cssTagRectification}
                        onClick={() => {
                          updateSearch({
                            query: t.relatedSearch,
                            withReset: true,
                          })
                        }}
                      >
                        {t.relatedSearch}
                      </Button>
                    ))}
                  </StyledTags>
                </StyledTagsContainer>
              </StyledResult>
            </>
          )}

          {currentText !== null && currentText.length > 0 && (
            <StyledResult className={cssHistory}>
              <StyledResultList>
                {[...history]
                  .reverse()
                  .filter((item) => item.query.includes(currentText))
                  .filter((_, i) => i < LIMIT_SHOW_HISTORY_HINTS)
                  .map((item, i) => (
                    <StyledResultItem key={i} className={cssWithIcon}>
                      <StyledResultItemInner
                        onClick={(e) => {
                          e.preventDefault()
                          selectItem(item.query)
                        }}
                      >
                        <Icon NameIcon={"RefreshTime"} size={"medium"} />
                        <Typography variant={"p14"}>
                          <span
                            dangerouslySetInnerHTML={{
                              __html: item.query.replace(
                                currentText,
                                `<b>${currentText}</b>`,
                              ),
                            }}
                          />
                        </Typography>
                      </StyledResultItemInner>
                      <Button
                        variant={"box"}
                        icon={"X"}
                        onClick={(e) => {
                          e.preventDefault()
                          removeFromHistory({ query: item.query })
                        }}
                      />
                    </StyledResultItem>
                  ))}

                {taps
                  .filter((_, i) => i < 3)
                  .map((t, i) => (
                    <StyledResultItem key={i}>
                      <StyledResultItemInner
                        onClick={(e) => {
                          e.preventDefault()
                          selectItem(t.relatedSearch)
                        }}
                      >
                        <Icon NameIcon={"Search"} size={"mediumM"} />
                        <Typography variant={"p14"}>
                          <span
                            dangerouslySetInnerHTML={{
                              __html: t.relatedSearch.replace(
                                currentText,
                                `<b>${currentText}</b>`,
                              ),
                            }}
                          />
                        </Typography>
                      </StyledResultItemInner>
                    </StyledResultItem>
                  ))}
              </StyledResultList>
            </StyledResult>
          )}

          {(currentText === null || currentText.length <= 0) &&
            history.length > 0 && (
              <StyledResult className={cssHistory}>
                <StyledResultTitle>
                  <Typography variant={"span"}>История</Typography>
                  <Typography
                    variant={"p12"}
                    onClick={() => {
                      clearHistory()
                    }}
                  >
                    Очистить
                  </Typography>
                </StyledResultTitle>
                <StyledResultList>
                  {[...history]
                    .reverse()
                    .filter((item, i) => i < LIMIT_SHOW_HISTORY)
                    .map((item, i) => (
                      <StyledResultItem key={i} className={cssWithIcon}>
                        <StyledResultItemInner
                          onClick={() => {
                            selectItem(item.query)
                          }}
                        >
                          <Icon NameIcon={"RefreshTime"} size={"medium"} />
                          <Typography variant={"p14"}>{item.query}</Typography>
                        </StyledResultItemInner>
                        <Button
                          variant={"box"}
                          icon={"X"}
                          onClick={(e) => {
                            e.preventDefault()
                            removeFromHistory({ query: item.query })
                          }}
                        />
                      </StyledResultItem>
                    ))}
                </StyledResultList>
              </StyledResult>
            )}

          {sts.length > 0 && (
            <>
              <StyledResult className={cssSts}>
                <StyledResultTitle>Часто ищут</StyledResultTitle>
                <StyledResultList>
                  {sts
                    .filter((_, i) => i < 3)
                    .map((item, i) => (
                      <StyledResultItem key={i}>
                        <StyledResultItemInner
                          onClick={() => {
                            selectItem(item.st)
                          }}
                        >
                          <Icon NameIcon={"Search"} size={"mediumM"} />
                          <Typography variant={"p14"}>{item.st}</Typography>
                        </StyledResultItemInner>
                      </StyledResultItem>
                    ))}
                </StyledResultList>
              </StyledResult>
            </>
          )}

          {categories.length > 0 && query !== null && query.length > 0 && (
            <>
              <StyledResult className={cssCategories}>
                <StyledResultTitle>Найдено в категориях</StyledResultTitle>
                <StyledProducts>
                  {categories.map((c) => (
                    <Category
                      category={c}
                      key={c.id}
                      onRoute={hideAutoComplete}
                      updateCategorySearch={updateCategorySearch}
                    />
                  ))}
                </StyledProducts>
              </StyledResult>
            </>
          )}
        </StyledResultListArea>

        {((width || 0) > getBreakpointVal(breakpoints.md) ||
          (sts.length == 0 && taps.length === 0)) && (
          <>
            {products.length > 0 && (
              <>
                <StyledResult className={cssProducts}>
                  <StyledResultTitle>Популярные товары</StyledResultTitle>
                  <StyledProducts>
                    {products.map((p) => (
                      <Product
                        product={p}
                        key={p.id}
                        onRoute={() => {
                          hideAutoComplete()
                        }}
                      />
                    ))}
                  </StyledProducts>
                  {query !== null && query.length > 0 && (
                    <>
                      <Button
                        variant={"translucent"}
                        size={"small"}
                        onClick={() => {
                          if (showAllResults) {
                            showAllResults()
                          }
                        }}
                      >
                        Смотреть все результаты
                      </Button>
                    </>
                  )}
                </StyledResult>
              </>
            )}
          </>
        )}
      </StyledAutoComplete>
    </>
  )
}
