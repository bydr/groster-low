import { styled } from "@linaria/react"
import { TypographyBase } from "../../../Typography/Typography"

export const StyledCategory = styled.div`
  width: 100%;
  position: relative;

  ${TypographyBase} {
    margin: 0;
  }
`
