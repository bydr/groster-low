import { FC } from "react"
import { StyledCategory } from "./StyledCategory"
import { DigiCategoryTypeAutoComplete } from "../../../../api/digineticaAPI"
import { Typography } from "../../../Typography/Typography"
import { Trigger } from "../../../../styles/utils/Utils"

export const Category: FC<{
  category: DigiCategoryTypeAutoComplete
  onRoute?: () => void
  updateCategorySearch: (props: { cId: string; title: string }) => void
}> = ({ category, onRoute, updateCategorySearch }) => {
  return (
    <>
      <StyledCategory data-id={category.id}>
        <Typography
          variant={"p14"}
          onClick={() => {
            console.log("onRoute category")
            updateCategorySearch({
              cId: category.id,
              title: category.name,
            })
            if (onRoute) {
              onRoute()
            }
          }}
        >
          <Trigger>{category.name}</Trigger>
        </Typography>
      </StyledCategory>
    </>
  )
}
