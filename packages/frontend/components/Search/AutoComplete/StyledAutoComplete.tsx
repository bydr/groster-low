import { Panel } from "../../../styles/utils/StyledPanel"
import { styled } from "@linaria/react"
import {
  BoxShadowPopover,
  breakpoints,
  colors,
} from "../../../styles/utils/vars"
import {
  Heading5,
  Paragraph12,
  TypographyBase,
} from "../../Typography/Typography"
import { cssIsActive, getCustomizeScroll } from "../../../styles/utils/Utils"
import { ButtonBox } from "../../Button/StyledButton"
import { cssIcon } from "../../Icon"
import { css } from "@linaria/core"
import { StyledTags } from "../../Tags/StyledTags"

export const cssWithIcon = css``

export const cssHistory = css``
export const cssTaps = css``
export const cssSts = css``
export const cssCategories = css``
export const cssProducts = css``

export const StyledResultItemInner = styled.div`
  flex: 1;
  display: flex;
  align-items: center;
  cursor: pointer;
  margin: 0;
  padding: 4px 0;

  > .${cssIcon} {
    color: ${colors.grayDarkLight};
    position: absolute;
    left: 2px;
    top: 50%;
    transform: translateY(-50%);
  }
`

export const StyledResultItem = styled.li`
  width: 100%;
  margin: 0;
  padding: 0 0 0 24px;
  background: transparent;
  color: ${colors.grayDarker};
  display: flex;
  flex-direction: row;
  align-items: center;
  position: relative;

  ${TypographyBase} {
    margin: 0;
    cursor: pointer;
    color: inherit;
    flex: 1;
  }

  > ${ButtonBox} {
    width: auto;
    height: auto;
    padding: 0;
    background-color: transparent;
    z-index: 4;
    position: relative;

    .${cssIcon} {
      fill: ${colors.grayDarkLight};
    }

    &:hover,
    &:active {
      .${cssIcon} {
        fill: ${colors.grayDarker};
      }
    }
  }

  &:hover,
  &:active {
    background: ${colors.grayLight};
    color: ${colors.black};
  }
`

export const StyledResultList = styled.ul`
  width: 100%;
  list-style: none;
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  padding: 0;
  margin: 0;
`
export const StyledResultTitle = styled(Heading5)`
  padding: 0;
  margin-bottom: 4px;
  display: flex;
  width: 100%;
  justify-content: space-between;

  ${Paragraph12} {
    color: ${colors.grayDark};
    cursor: pointer;

    &:hover,
    &:active {
      color: ${colors.brand.purple};
    }
  }
`
export const StyledResult = styled.div`
  width: 100%;
  margin: 0 0 8px 0;
`

export const StyledResultListArea = styled.div`
  grid-area: list;
`

export const StyledAutoCompleteOverlay = styled.div`
  position: fixed;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  background: rgba(255, 255, 255, 0.4);
  z-index: 8;
  opacity: 0;
  visibility: hidden;
  transition: all 0.2s ease-in-out;

  &.${cssIsActive} {
    opacity: 1;
    visibility: visible;
  }
`

export const StyledAutoComplete = styled(Panel)`
  ${getCustomizeScroll()};
  position: absolute;
  top: 100%;
  left: 0;
  width: 100%;
  border-radius: 4px;
  padding: 10px 15px;
  box-shadow: ${BoxShadowPopover};
  opacity: 0;
  transition: all 0.2s ease-in-out;
  transform: translateY(-10px);
  min-height: 100px;
  max-height: calc(100vh - 150px);
  overflow: hidden auto;
  display: grid;
  grid-template-columns: 4fr 8fr;
  grid-template-areas: "list products";
  gap: 0 30px;
  z-index: 10;

  &.${cssIsActive} {
    opacity: 1;
    transform: translateY(0);
  }

  ${StyledResult} {
    &.${cssTaps} {
      grid-area: taps;
      margin-right: -15px;
      width: auto;
      display: none;

      @media (max-width: ${breakpoints.lg}) {
        display: block;
      }

      ${StyledTags} {
        grid-template-rows: 1fr 1fr;
        display: grid;
        flex-wrap: nowrap;
        white-space: nowrap;
        grid-auto-flow: column;
        width: 100%;
        overflow: auto;
        padding-bottom: 8px;
        margin-bottom: 0;
      }
    }
    &.${cssProducts} {
      grid-area: products;
    }
  }

  @media (max-width: ${breakpoints.lg}) {
    box-shadow: none;
    border: none;
    border-radius: 0;
    margin: 0;
  }

  @media (max-width: ${breakpoints.md}) {
    display: block;
    grid-template-areas: none;
    gap: initial;
  }
`
