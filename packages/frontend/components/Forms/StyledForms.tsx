import { styled } from "@linaria/react"
import { cssIcon } from "../Icon"
import { breakpoints, colors, typography } from "../../styles/utils/vars"
import { TypographyBase } from "../Typography/Typography"
import { ButtonBase } from "../Button/StyledButton"
import { Panel, PanelHeading } from "../../styles/utils/StyledPanel"
import { StyledSelectInputWrapper } from "../Select/StyledSelect"

export const SigninSocialsContainer = styled.div`
  display: flex;
  align-items: center;
  margin: 24px 0;

  .${cssIcon} {
    fill: ${colors.brand.purple};
    cursor: pointer;
    margin-left: 16px;

    &:hover,
    &:active {
      fill: ${colors.brand.purpleDarken};
    }
  }

  > * ${TypographyBase} {
    margin-bottom: 0;
  }
`
export const IconList = styled.div``

export const StyledForm = styled.form`
  width: 100%;
  position: relative;

  ${SigninSocialsContainer} {
    ${TypographyBase} {
      margin-bottom: 0;
    }
  }

  ${ButtonBase} {
    margin-bottom: 20px;

    &[type="submit"] {
      font-size: ${typography.default.fs};
      line-height: ${typography.default.lh};
      padding: 13px 32px;
      margin-top: 20px;
      margin-bottom: 0;
    }
  }
  @media (max-width: ${breakpoints.sm}) {
    ${ButtonBase} {
      &[type="submit"] {
        width: 100%;
      }
    }
  }
`
export const StyledFormWrapper = styled.div`
  width: 100%;
  position: relative;
`

export const FormPanel = styled(Panel)`
  padding: 0;
  border: none;

  ${PanelHeading} {
    margin-bottom: 26px;
  }
`

export const FormGroup = styled.div`
  width: 100%;
  display: flex;
  position: relative;

  > ${StyledSelectInputWrapper} {
    margin-bottom: 0;
  }

  > * {
    flex: 1;
    margin-right: 16px;

    &:last-of-type {
      margin-right: 0;
    }
  }

  @media (max-width: ${breakpoints.sm}) {
    flex-direction: column;

    > * {
      margin-right: 0;
      margin-left: 0;
    }
  }
`
