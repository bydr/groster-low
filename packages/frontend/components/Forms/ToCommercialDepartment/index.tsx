import { FC, useContext, useEffect, useState } from "react"
import { Controller, useForm } from "react-hook-form"
import { SuccessOverlay } from "../SuccessOverlay/SuccessOverlay"
import { FormGroup, StyledForm, StyledFormWrapper } from "../StyledForms"
import { Field } from "../../Field/Field"
import { SingleError, Typography } from "../../Typography/Typography"
import { TIMEOUT_SUCCESS } from "../../../utils/constants"
import { Button } from "../../Button"
import { useMutation } from "react-query"
import {
  fetchToCommercialDepartment,
  ToCommercialDepartmentRequestType,
} from "../../../api/feedbackAPI"
import { ApiError } from "next/dist/server/api-utils"
import { useGRecaptcha } from "../../../hooks/gReCaptcha"
import { Select, SelectItemsType } from "../../Select/Select"
import { useAppSelector } from "../../../hooks/redux"
import {
  getPhoneWithCode,
  RULES_PHONE_VALIDATE,
} from "../../../validations/phone"
import { PhoneField } from "../../Field/PhoneField"
import { ModalContext } from "../../Modals/Modal"

type ToCommercialDepartmentType = ToCommercialDepartmentRequestType & {
  dataPrivacyAgreement: boolean
}

export const ToCommercialDepartment: FC = () => {
  const categories = useAppSelector(
    (state) => state.catalog.categories?.tree || {},
  )
  const [error, setError] = useState<string | null>(null)

  const modalContext = useContext(ModalContext)

  const [categoriesItems, setCategoriesItems] = useState<SelectItemsType[]>([])

  const {
    handleSubmit,
    control,
    formState: { errors, isDirty, isSubmitting },
    reset: resetForm,
    getValues,
  } = useForm<ToCommercialDepartmentType>({
    defaultValues: {
      brand: "",
      category: "",
      fio: "",
      representation: "",
      site_company: "",
      social_brand: "",
      status: "",
      phone: "",
      email: "",
      info: "",
      dataPrivacyAgreement: false,
    },
    mode: "onChange",
  })

  const {
    reset: resetMutation,
    mutate: commercialDepartmentMutate,
    isSuccess,
    isLoading,
  } = useMutation(fetchToCommercialDepartment, {
    onMutate: () => {
      setError(null)
    },
    onSuccess: () => {
      resetForm({
        brand: "",
        category: "",
        fio: "",
        representation: "",
        site_company: "",
        social_brand: "",
        status: "",
        phone: "",
        email: "",
        info: "",
      })
      setTimeout(() => {
        resetMutation()
        modalContext?.hide()
      }, TIMEOUT_SUCCESS)
      setError(null)
    },
    onError: (err: ApiError) => {
      setError(err.message)
    },
  })

  const { run, isLoading: isLoadingReCaptcha } = useGRecaptcha({
    cbMutate: () => {
      setError(null)
    },
    cbSuccess: () => {
      commercialDepartmentMutate({
        fio: getValues("fio"),
        status: getValues("status"),
        social_brand: getValues("social_brand"),
        site_company: getValues("site_company"),
        representation: getValues("representation"),
        category: getValues("category"),
        brand: getValues("brand"),
        info: getValues("info"),
        email: getValues("email"),
        phone: getPhoneWithCode(getValues("phone")),
      })
    },
    cbError: (res) => {
      setError(res?.message || null)
    },
  })

  const onSubmit = handleSubmit(() => {
    run()
  })

  useEffect(() => {
    setCategoriesItems(
      Object.keys(categories).map((key) => {
        return {
          value: categories[key].uuid || "",
          name: categories[key].name || "",
        }
      }),
    )
  }, [categories])

  return (
    <>
      <StyledFormWrapper>
        <SuccessOverlay isSuccess={isSuccess} />
        <StyledForm onSubmit={onSubmit}>
          <Typography variant={"p14"}>
            для оформления заявки заполните <b>7&nbsp;обязательных полей</b>
          </Typography>
          <Typography variant={"h2"}>О продукте</Typography>
          <FormGroup>
            {Object.keys(categories).length > 0 && (
              <Controller
                rules={{
                  required: {
                    value: true,
                    message: "Поле обязательно для заполнения",
                  },
                }}
                render={({ field, fieldState }) => {
                  return (
                    <Select
                      ariaLabel={"Категория продукта"}
                      initialValue={field.value || ""}
                      items={categoriesItems}
                      variant={"default"}
                      onSelectValue={(value) => {
                        field.onChange(value)
                      }}
                      ref={field.ref}
                      errorMessage={fieldState.error?.message}
                      required={true}
                    />
                  )
                }}
                control={control}
                name={"category"}
              />
            )}
          </FormGroup>
          <FormGroup>
            <Controller
              name={"brand"}
              control={control}
              rules={{
                required: {
                  value: true,
                  message: "Поле обязательно для заполнения",
                },
              }}
              render={({ field, fieldState }) => (
                <Field
                  type={"text"}
                  value={field.value}
                  placeholder={"Название бренда"}
                  onChange={field.onChange}
                  errorMessage={fieldState.error?.message}
                  withAnimatingLabel
                  required={true}
                />
              )}
            />
          </FormGroup>
          <FormGroup>
            <Controller
              name={"representation"}
              control={control}
              rules={{
                required: {
                  value: true,
                  message: "Поле обязательно для заполнения",
                },
              }}
              render={({ field, fieldState }) => (
                <Field
                  variant={"textarea"}
                  type={"text"}
                  value={field.value}
                  placeholder={"Представленность в крупных сетевых магазинах"}
                  onChange={field.onChange}
                  errorMessage={fieldState.error?.message}
                  withAnimatingLabel
                  ref={field.ref}
                  required={true}
                  style={{
                    minHeight: "80px",
                  }}
                />
              )}
            />
          </FormGroup>
          <FormGroup>
            <Controller
              name={"site_company"}
              control={control}
              render={({ field, fieldState }) => (
                <Field
                  type={"text"}
                  value={field.value}
                  placeholder={"Сайт компании/бренда"}
                  onChange={field.onChange}
                  errorMessage={fieldState.error?.message}
                  withAnimatingLabel
                />
              )}
            />
          </FormGroup>
          <FormGroup>
            <Controller
              name={"social_brand"}
              control={control}
              render={({ field, fieldState }) => (
                <Field
                  type={"text"}
                  value={field.value}
                  placeholder={"Социальная сеть бренда"}
                  onChange={field.onChange}
                  errorMessage={fieldState.error?.message}
                  withAnimatingLabel
                />
              )}
            />
          </FormGroup>
          <Typography variant={"h2"}>Представитель</Typography>
          <FormGroup>
            <Controller
              name={"status"}
              control={control}
              rules={{
                required: {
                  value: true,
                  message: "Поле обязательно для заполнения",
                },
              }}
              render={({ field, fieldState }) => (
                <Select
                  ariaLabel={"Ваш статус"}
                  initialValue={field.value || ""}
                  items={[
                    {
                      name: "дистрибьютор",
                      value: "дистрибьютор",
                    },
                    {
                      name: "производитель",
                      value: "производитель",
                    },
                  ]}
                  variant={"default"}
                  onSelectValue={(value) => {
                    field.onChange(value)
                  }}
                  ref={field.ref}
                  errorMessage={fieldState.error?.message}
                  required={true}
                />
              )}
            />
          </FormGroup>
          <FormGroup>
            <Controller
              name={"fio"}
              control={control}
              rules={{
                required: {
                  value: true,
                  message: "Поле обязательно для заполнения",
                },
              }}
              render={({ field, fieldState }) => (
                <Field
                  type={"text"}
                  value={field.value}
                  placeholder={"ФИО представителя"}
                  onChange={field.onChange}
                  errorMessage={fieldState.error?.message}
                  withAnimatingLabel
                  required={true}
                />
              )}
            />
          </FormGroup>
          <FormGroup>
            <Controller
              name={"email"}
              control={control}
              rules={{
                required: {
                  value: true,
                  message: "Поле обязательно для заполнения",
                },
              }}
              render={({ field, fieldState }) => (
                <Field
                  type={"text"}
                  value={field.value}
                  placeholder={"Email"}
                  onChange={field.onChange}
                  errorMessage={fieldState.error?.message}
                  withAnimatingLabel
                  required={true}
                />
              )}
            />
          </FormGroup>
          <FormGroup>
            <Controller
              rules={RULES_PHONE_VALIDATE}
              render={({ field, fieldState }) => (
                <PhoneField
                  value={field.value}
                  onValueChange={({ value }) => {
                    field.onChange(value)
                  }}
                  errorMessage={fieldState?.error?.message}
                  required={true}
                />
              )}
              control={control}
              name={"phone"}
            />
          </FormGroup>
          <FormGroup>
            <Controller
              name={"info"}
              control={control}
              render={({ field, fieldState }) => (
                <Field
                  variant={"textarea"}
                  type={"text"}
                  value={field.value}
                  placeholder={"Дополнительная информация"}
                  onChange={field.onChange}
                  errorMessage={fieldState.error?.message}
                  withAnimatingLabel
                  ref={field.ref}
                />
              )}
            />
          </FormGroup>

          {error !== null && <SingleError>{error}</SingleError>}

          <Button
            type={"submit"}
            variant={"filled"}
            size={"large"}
            disabled={Object.keys(errors).length > 0 || !isDirty}
            isFetching={isLoading || isSubmitting || isLoadingReCaptcha}
          >
            Отправить
          </Button>
        </StyledForm>
      </StyledFormWrapper>
    </>
  )
}
