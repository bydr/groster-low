import { FC, useEffect, useState } from "react"
import { FormGroup, StyledForm } from "../StyledForms"
import { Controller, useForm } from "react-hook-form"
import { Field } from "../../Field/Field"
import { getClientUser, useAuth } from "../../../hooks/auth"
import { SingleError, Typography } from "../../Typography/Typography"
import { Button } from "../../Button"
import { Heading } from "../../Modals/StyledModal"
import { useMutation } from "react-query"
import { fetchFastOrder } from "../../../api/cartAPI"
import { useCart } from "../../../hooks/cart"
import { SuccessOverlay } from "../SuccessOverlay/SuccessOverlay"
import { ROUTES, TIMEOUT_SUCCESS } from "../../../utils/constants"
import { useRouter } from "next/router"
import { useShippings } from "../../../hooks/shippings"
import { ApiError } from "../../../../contracts/contracts"
import { useGRecaptcha } from "../../../hooks/gReCaptcha"

type FastOrderFormFieldsType = {
  contact: string
}

export const FastOrderForm: FC = () => {
  const { user, updateUser } = useAuth()
  const router = useRouter()
  const {
    token,
    productsFetching,
    setOrder,
    updateToken,
    totalCost,
    cartCost,
  } = useCart()
  const { shippingCost } = useShippings()
  const [error, setError] = useState<string | null>(null)

  const {
    handleSubmit,
    control,
    setValue,
    formState: { errors },
    getValues,
    reset: resetForm,
  } = useForm<FastOrderFormFieldsType>({
    defaultValues: {
      contact: user?.phone || user?.email || "",
    },
    mode: "onChange",
  })

  useEffect(() => {
    setValue("contact", user?.phone || user?.email || "", {
      shouldDirty: true,
    })
  }, [setValue, user?.email, user?.phone])

  const {
    mutate: fastOrderMutate,
    isLoading,
    isSuccess,
    reset: resetMutation,
  } = useMutation(fetchFastOrder, {
    onMutate: () => {
      setError(null)
    },
    onSuccess: () => {
      const user = getClientUser()
      if (user !== null) {
        updateUser({
          cart: null,
          accessToken: user.accessToken,
          email: user.email,
          refreshToken: user.refreshToken,
          phone: user.phone,
        })
      }
      updateToken(null)

      setOrder({
        fio: user?.fio || undefined,
        shippingCost: shippingCost !== null ? shippingCost : undefined,
        totalCost: totalCost,
        productsCost: cartCost,
        contact: getValues("contact") || "",
      })

      resetForm({
        contact: "",
      })
      void router.push(ROUTES.thank)

      setTimeout(() => {
        resetMutation()
      }, TIMEOUT_SUCCESS)

      setError(null)
    },
    onError: (err: ApiError) => {
      setError(err.message)
    },
  })

  const { run, isLoading: isLoadingGRecaptcha } = useGRecaptcha({
    cbMutate: () => {
      setError(null)
    },
    cbError: (err) => {
      setError(err?.message || null)
    },
    cbSuccess: () => {
      if (token === null) {
        return
      }
      fastOrderMutate({
        contact: getValues("contact"),
        cart: token,
      })
    },
  })

  const onSubmit = handleSubmit(() => {
    if (!!token) {
      run()
    }
  })

  return (
    <>
      <SuccessOverlay isSuccess={isSuccess} />
      <StyledForm onSubmit={onSubmit}>
        <Heading>
          <Typography variant={"p14"}>
            Оставьте свой контакт и мы свяжемся с вами в течение часа чтобы
            обсудить детали заказа.
          </Typography>
        </Heading>
        <FormGroup>
          <Controller
            name={"contact"}
            control={control}
            rules={{
              required: {
                value: true,
                message: "Поле обязательно для заполнения",
              },
            }}
            render={({ field, fieldState }) => (
              <Field
                type={"text"}
                value={field.value}
                placeholder={"Номер телефона или e-mail"}
                onChange={field.onChange}
                errorMessage={fieldState.error?.message}
                withAnimatingLabel
              />
            )}
          />
        </FormGroup>

        {error !== null && <SingleError>{error}</SingleError>}

        <Button
          type={"submit"}
          variant={"filled"}
          size={"large"}
          disabled={Object.keys(errors).length > 0}
          isFetching={
            isLoading || productsFetching.length > 0 || isLoadingGRecaptcha
          }
        >
          Отправить
        </Button>
      </StyledForm>
    </>
  )
}
