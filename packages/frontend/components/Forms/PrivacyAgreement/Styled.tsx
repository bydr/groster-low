import { css } from "@linaria/core"
import { getTypographyBase } from "../../Typography/Typography"

export const cssInlineLink = css`
  display: inline !important;
  ${getTypographyBase("p12")}
`
