import { forwardRef, useState } from "react"
import { CustomCheckbox } from "../../Checkbox/CustomCheckbox"
import { Typography } from "../../Typography/Typography"
import { FormGroup } from "../StyledForms"
import { Button } from "../../Button"
import { cssButtonClean } from "../../Button/StyledButton"
import type { ModalDefaultPropsType } from "../../Modals/Modal"
import { AgreeContent } from "../../Agree/AgreeContent"
import { cx } from "@linaria/core"
import { cssInlineLink } from "./Styled"
import { cssCheckBoxOverlay } from "../../Checkbox/StyledCheckbox"
import dynamic, { DynamicOptions } from "next/dynamic"

const Modal = dynamic(
  (() =>
    import("../../Modals/Modal").then(
      (mod) => mod.Modal,
    )) as DynamicOptions<ModalDefaultPropsType>,
  {
    ssr: false,
  },
)

export const PrivacyAgreement = forwardRef<
  HTMLInputElement,
  {
    errorMessage?: string
    isCheckboxOverlay?: boolean
  }
>(({ errorMessage, isCheckboxOverlay = false, ...props }, ref) => {
  const [isShow, setIsShow] = useState<boolean>(false)

  return (
    <>
      <FormGroup>
        <CustomCheckbox
          name={""}
          variant={"check"}
          className={cx(isCheckboxOverlay && cssCheckBoxOverlay)}
          message={
            <Typography variant={"p12"}>
              <Typography variant={"span"}>Согласен c</Typography>
              <Modal
                closeMode={"destroy"}
                variant={"rounded-100"}
                isShowModal={isShow}
                disclosure={
                  <Button
                    variant={"link"}
                    as={"a"}
                    type={"button"}
                    className={cx(cssButtonClean, cssInlineLink)}
                    onClick={() => {
                      setIsShow(true)
                    }}
                  >
                    {" "}
                    Правилами обработки информации
                  </Button>
                }
              >
                <AgreeContent />
                <Button
                  type={"button"}
                  variant={"filled"}
                  size={"large"}
                  onClick={(e) => {
                    e.preventDefault()
                    setIsShow(false)
                  }}
                >
                  Согласен
                </Button>
              </Modal>
            </Typography>
          }
          ref={ref}
          {...props}
          errorMessage={errorMessage}
        />
      </FormGroup>
    </>
  )
})

PrivacyAgreement.displayName = "PrivacyAgreement"
