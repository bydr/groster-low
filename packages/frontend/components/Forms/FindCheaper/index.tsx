import { FC, useEffect, useState } from "react"
import { FormGroup, StyledForm, StyledFormWrapper } from "../StyledForms"
import { Controller, useForm } from "react-hook-form"
import { Field } from "../../Field/Field"
import { PhoneField } from "../../Field/PhoneField"
import { useAuth } from "../../../hooks/auth"
import { SingleError } from "../../Typography/Typography"
import { Button } from "../../Button"
import { useMutation } from "react-query"
import { fetchFoundCheaper } from "../../../api/feedbackAPI"
import { SuccessOverlay } from "../SuccessOverlay/SuccessOverlay"
import { TIMEOUT_SUCCESS } from "../../../utils/constants"
import { ApiError } from "../../../../contracts/contracts"
import {
  getPhoneWithCode,
  RULES_PHONE_VALIDATE,
} from "../../../validations/phone"
import { PrivacyAgreement } from "../PrivacyAgreement"
import { useGRecaptcha } from "../../../hooks/gReCaptcha"

type FindCheaperFormFieldsType = {
  phone: string
  link: string
  comment: string
  dataPrivacyAgreement: boolean
}

export const FindCheaperForm: FC = () => {
  const { user } = useAuth()
  const [error, setError] = useState<string | null>(null)

  const {
    handleSubmit,
    control,
    register,
    setValue,
    formState: { errors, isDirty },
    reset: resetForm,
    getValues,
  } = useForm<FindCheaperFormFieldsType>({
    defaultValues: {
      phone: user?.phone || "",
      link: "",
      comment: "",
      dataPrivacyAgreement: false,
    },
    mode: "onChange",
  })

  useEffect(() => {
    setValue("phone", user?.phone || "")
  }, [setValue, user?.phone])

  const {
    mutate: foundCheaperMutate,
    isLoading,
    isSuccess,
    reset: resetMutation,
  } = useMutation(fetchFoundCheaper, {
    onSuccess: () => {
      resetForm({
        link: "",
        phone: "",
        comment: "",
      })
      setTimeout(() => {
        resetMutation()
      }, TIMEOUT_SUCCESS)

      setError(null)
    },
    onError: (err: ApiError) => {
      setError(err.message)
    },
  })

  const { run, isLoading: isLoadingGRecaptcha } = useGRecaptcha({
    cbMutate: () => {
      setError(null)
    },
    cbError: (err) => {
      setError(err?.message || null)
    },
    cbSuccess: () => {
      foundCheaperMutate({
        phone: getPhoneWithCode(getValues("phone")),
        link: getValues("link"),
        message: getValues("comment"),
      })
    },
  })

  const onSubmit = handleSubmit(() => {
    run()
  })

  return (
    <>
      <StyledFormWrapper>
        <SuccessOverlay isSuccess={isSuccess} />
        <StyledForm onSubmit={onSubmit}>
          <FormGroup>
            <Controller
              rules={RULES_PHONE_VALIDATE}
              render={({ field, fieldState }) => (
                <PhoneField
                  value={field.value}
                  onValueChange={({ value }) => {
                    field.onChange(value)
                  }}
                  errorMessage={fieldState?.error?.message}
                />
              )}
              control={control}
              name={"phone"}
            />
          </FormGroup>
          <FormGroup>
            <Controller
              name={"link"}
              control={control}
              render={({ field, fieldState }) => (
                <Field
                  type={"text"}
                  value={field.value}
                  placeholder={"Ссылка на товар другого магазина"}
                  onChange={field.onChange}
                  errorMessage={fieldState.error?.message}
                  withAnimatingLabel
                />
              )}
            />
          </FormGroup>
          <FormGroup>
            <Controller
              name={"comment"}
              control={control}
              render={({ field, fieldState }) => (
                <Field
                  variant={"textarea"}
                  type={"text"}
                  value={field.value}
                  placeholder={"Сообщение"}
                  onChange={field.onChange}
                  errorMessage={fieldState.error?.message}
                  withAnimatingLabel
                />
              )}
            />
          </FormGroup>

          <PrivacyAgreement
            {...register("dataPrivacyAgreement", {
              required: {
                value: true,
                message: "Нам необходимо ваше согласие",
              },
            })}
            errorMessage={errors?.dataPrivacyAgreement?.message}
          />

          {error !== null && <SingleError>{error}</SingleError>}

          <Button
            type={"submit"}
            variant={"filled"}
            size={"large"}
            disabled={Object.keys(errors).length > 0 || !isDirty}
            isFetching={isLoading || isLoadingGRecaptcha}
          >
            Отправить
          </Button>
        </StyledForm>
      </StyledFormWrapper>
    </>
  )
}
