import { FC, useEffect, useState } from "react"
import { FormGroup, StyledForm } from "../StyledForms"
import { useForm } from "react-hook-form"
import { Field } from "../../Field/Field"
import { SingleError } from "../../Typography/Typography"
import { Button } from "../../Button"
import { useAuth } from "../../../hooks/auth"
import { EMAIL_PATTERN } from "../../../validations/email"
import { useMutation } from "react-query"
import { fetchProductWaiting } from "../../../api/productsAPI"
import { SuccessOverlay } from "../SuccessOverlay/SuccessOverlay"
import { TIMEOUT_SUCCESS } from "../../../utils/constants"
import { ApiError } from "../../../../contracts/contracts"
import { PrivacyAgreement } from "../PrivacyAgreement"
import { useGRecaptcha } from "../../../hooks/gReCaptcha"

type ReportAdmissionFormFieldsType = {
  email: string
  dataPrivacyAgreement: boolean
}

export const ReportAdmission: FC<{ uuid: string }> = ({ uuid }) => {
  const { user } = useAuth()
  const [error, setError] = useState<string | null>(null)

  const {
    handleSubmit,
    register,
    setValue,
    formState: { errors, isDirty },
    reset: resetForm,
    getValues,
  } = useForm<ReportAdmissionFormFieldsType>({
    defaultValues: {
      email: user?.email || "",
      dataPrivacyAgreement: false,
    },
    mode: "onChange",
  })

  const {
    mutate: waitingProductMutate,
    reset: resetMutation,
    isSuccess,
    isLoading,
  } = useMutation(fetchProductWaiting, {
    onMutate: () => {
      setError(null)
    },
    onSuccess: () => {
      resetForm({
        email: "",
        dataPrivacyAgreement: false,
      })
      setTimeout(() => {
        resetMutation()
      }, TIMEOUT_SUCCESS)
      setError(null)
    },
    onError: (err: ApiError) => {
      setError(err.message)
    },
  })

  const { run, isLoading: isLoadingGRecaptcha } = useGRecaptcha({
    cbMutate: () => {
      setError(null)
    },
    cbError: (err) => {
      setError(err?.message || null)
    },
    cbSuccess: () => {
      waitingProductMutate({
        uuid: uuid,
        email: getValues("email"),
      })
    },
  })

  const onSubmit = handleSubmit(() => {
    run()
  })

  useEffect(() => {
    setValue("email", user?.email || "")
  }, [setValue, user?.email])

  return (
    <>
      <SuccessOverlay isSuccess={isSuccess} />
      <StyledForm onSubmit={onSubmit}>
        <FormGroup>
          <Field
            placeholder={"Email"}
            withAnimatingLabel
            errorMessage={errors?.email?.message}
            {...register("email", {
              required: {
                value: true,
                message: "Поле обязательно для заполнения",
              },
              pattern: {
                value: EMAIL_PATTERN,
                message: "Неверный формат email",
              },
            })}
          />
        </FormGroup>
        <PrivacyAgreement
          {...register("dataPrivacyAgreement", {
            required: {
              value: true,
              message: "Нам необходимо ваше согласие",
            },
          })}
          errorMessage={errors?.dataPrivacyAgreement?.message}
        />

        {error !== null && <SingleError>{error}</SingleError>}

        <Button
          type={"submit"}
          variant={"filled"}
          size={"large"}
          disabled={Object.keys(errors).length > 0 || !isDirty}
          isFetching={isLoading || isLoadingGRecaptcha}
        >
          Отправить
        </Button>
      </StyledForm>
    </>
  )
}
