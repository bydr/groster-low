import type { FC } from "react"
import { useEffect, useState } from "react"
import { useAuth } from "../../../hooks/auth"
import { Controller, useForm } from "react-hook-form"
import { useMutation } from "react-query"
import { SuccessOverlay } from "../SuccessOverlay/SuccessOverlay"
import { FormGroup, StyledForm, StyledFormWrapper } from "../StyledForms"
import { Field } from "../../Field/Field"
import { SingleError } from "../../Typography/Typography"
import { Button } from "../../Button"
import { PhoneField } from "../../Field/PhoneField"
import { fetchFeedback } from "../../../api/feedbackAPI"
import { TIMEOUT_SUCCESS } from "../../../utils/constants"
import { EMAIL_PATTERN } from "../../../validations/email"
import { ApiError } from "../../../../contracts/contracts"
import {
  getPhoneWithCode,
  RULES_PHONE_VALIDATE,
} from "../../../validations/phone"
import { useGRecaptcha } from "../../../hooks/gReCaptcha"
import { PrivacyAgreement } from "../PrivacyAgreement"

type FeedbackFormFieldsType = {
  phone: string
  name: string
  email: string
  message: string
  dataPrivacyAgreement: boolean
}

export const Feedback: FC<{
  customizePlaceholderMessage?: string
  isRequiredMessage?: boolean
}> = ({ customizePlaceholderMessage, isRequiredMessage }) => {
  const { user } = useAuth()
  const [error, setError] = useState<string | null>(null)

  const {
    handleSubmit,
    register,
    control,
    formState: { errors, isDirty, isSubmitting },
    reset: resetForm,
    getValues,
  } = useForm<FeedbackFormFieldsType>({
    defaultValues: {
      email: user?.email || "",
      phone: user?.phone || "",
      name: user?.fio || "",
      message: "",
      dataPrivacyAgreement: false,
    },
    mode: "onChange",
  })

  const {
    mutate: feedbackMutate,
    reset: resetMutation,
    isSuccess,
    isLoading,
  } = useMutation(fetchFeedback, {
    onMutate: () => {
      setError(null)
    },
    onSuccess: () => {
      resetForm()
      setTimeout(() => {
        resetMutation()
      }, TIMEOUT_SUCCESS)
      setError(null)
    },
    onError: (err: ApiError) => {
      setError(err.message)
    },
  })

  const { run, isLoading: isLoadingReCaptcha } = useGRecaptcha({
    cbMutate: () => {
      setError(null)
    },
    cbSuccess: () => {
      feedbackMutate({
        phone: getPhoneWithCode(getValues("phone")),
        name: getValues("name"),
        email: getValues("email"),
        message: getValues("message"),
      })
    },
    cbError: (res) => {
      setError(res?.message || null)
    },
  })

  const onSubmit = handleSubmit(() => {
    run()
  })

  useEffect(() => {
    resetForm({
      name: user?.fio || "",
      phone: user?.phone || "",
      email: user?.email || "",
      message: "",
    })
  }, [resetForm, user?.phone, user?.fio, user?.email])

  return (
    <>
      <StyledFormWrapper>
        <SuccessOverlay isSuccess={isSuccess} />
        <StyledForm onSubmit={onSubmit}>
          <FormGroup>
            <Controller
              name={"name"}
              control={control}
              rules={{
                required: {
                  value: true,
                  message: "Поле обязательно для заполнения",
                },
              }}
              render={({ field, fieldState }) => (
                <Field
                  type={"text"}
                  value={field.value}
                  placeholder={"Ваше имя"}
                  onChange={field.onChange}
                  errorMessage={fieldState.error?.message}
                  withAnimatingLabel
                />
              )}
            />
          </FormGroup>
          <FormGroup>
            <Controller
              rules={RULES_PHONE_VALIDATE}
              render={({ field, fieldState }) => (
                <PhoneField
                  value={field.value}
                  onValueChange={({ value }) => {
                    field.onChange(value)
                  }}
                  errorMessage={fieldState?.error?.message}
                />
              )}
              control={control}
              name={"phone"}
            />
          </FormGroup>

          <FormGroup>
            <Field
              placeholder={"Email"}
              withAnimatingLabel
              errorMessage={errors?.email?.message}
              {...register("email", {
                required: {
                  value: true,
                  message: "Поле обязательно для заполнения",
                },
                pattern: {
                  value: EMAIL_PATTERN,
                  message: "Неверный формат email",
                },
              })}
            />
          </FormGroup>
          <FormGroup>
            <Controller
              name={"message"}
              control={control}
              rules={{
                required: {
                  value: !!isRequiredMessage,
                  message: "Поле обязательно для заполнения",
                },
              }}
              render={({ field, fieldState }) => (
                <Field
                  variant={"textarea"}
                  type={"text"}
                  value={field.value}
                  placeholder={customizePlaceholderMessage || "Сообщение"}
                  onChange={field.onChange}
                  errorMessage={fieldState.error?.message}
                  withAnimatingLabel
                />
              )}
            />
          </FormGroup>

          <PrivacyAgreement
            {...register("dataPrivacyAgreement", {
              required: {
                value: true,
                message: "Нам необходимо ваше согласие",
              },
            })}
            errorMessage={errors?.dataPrivacyAgreement?.message}
          />

          {error !== null && <SingleError>{error}</SingleError>}

          <Button
            type={"submit"}
            variant={"filled"}
            size={"large"}
            disabled={Object.keys(errors).length > 0 || !isDirty}
            isFetching={isLoading || isSubmitting || isLoadingReCaptcha}
          >
            Отправить
          </Button>
        </StyledForm>
      </StyledFormWrapper>
    </>
  )
}
