import { FC, useEffect, useState } from "react"
import { FormGroup, StyledForm } from "../StyledForms"
import { Controller, useForm } from "react-hook-form"
import { Field } from "../../Field/Field"
import { PhoneField } from "../../Field/PhoneField"
import { useAuth } from "../../../hooks/auth"
import { SingleError } from "../../Typography/Typography"
import { Button } from "../../Button"
import { useMutation } from "react-query"
import { fetchNotFoundNeeded } from "../../../api/feedbackAPI"
import { SuccessOverlay } from "../SuccessOverlay/SuccessOverlay"
import { TIMEOUT_SUCCESS } from "../../../utils/constants"
import { ApiError } from "../../../../contracts/contracts"
import {
  getPhoneWithCode,
  RULES_PHONE_VALIDATE,
} from "../../../validations/phone"
import { PrivacyAgreement } from "../PrivacyAgreement"

type NotFindNeedFormFieldsType = {
  phone: string
  comment: string
  dataPrivacyAgreement: boolean
}

export const NotFindNeedForm: FC = () => {
  const { user } = useAuth()
  const [error, setError] = useState<string | null>(null)

  const {
    handleSubmit,
    control,
    register,
    setValue,
    formState: { errors, isDirty },
    reset: resetForm,
  } = useForm<NotFindNeedFormFieldsType>({
    defaultValues: {
      phone: user?.phone || "",
      comment: "",
      dataPrivacyAgreement: false,
    },
    mode: "onChange",
  })

  useEffect(() => {
    setValue("phone", user?.phone || "")
  }, [setValue, user?.phone])

  const {
    mutate: notFoundNeededMutate,
    isLoading,
    isSuccess,
    reset: resetMutation,
  } = useMutation(fetchNotFoundNeeded, {
    onSuccess: () => {
      resetForm({
        phone: "",
        comment: "",
        dataPrivacyAgreement: false,
      })
      setTimeout(() => {
        resetMutation()
      }, TIMEOUT_SUCCESS)

      setError(null)
    },
    onError: (err: ApiError) => {
      setError(err.message)
    },
  })

  const onSubmit = handleSubmit((data) => {
    notFoundNeededMutate({
      phone: getPhoneWithCode(data.phone),
      message: data.comment,
    })
  })

  return (
    <>
      <SuccessOverlay isSuccess={isSuccess} />
      <StyledForm onSubmit={onSubmit}>
        <FormGroup>
          <Controller
            rules={RULES_PHONE_VALIDATE}
            render={({ field, fieldState }) => (
              <PhoneField
                value={field.value}
                onValueChange={({ value }) => {
                  field.onChange(value)
                }}
                errorMessage={fieldState?.error?.message}
              />
            )}
            control={control}
            name={"phone"}
          />
        </FormGroup>
        <FormGroup>
          <Controller
            name={"comment"}
            control={control}
            render={({ field, fieldState }) => (
              <Field
                variant={"textarea"}
                type={"text"}
                value={field.value}
                placeholder={"Сообщение"}
                onChange={field.onChange}
                errorMessage={fieldState.error?.message}
                withAnimatingLabel
              />
            )}
          />
        </FormGroup>
        <PrivacyAgreement
          {...register("dataPrivacyAgreement", {
            required: {
              value: true,
              message: "Нам необходимо ваше согласие",
            },
          })}
          errorMessage={errors?.dataPrivacyAgreement?.message}
        />
        {error !== null && <SingleError>{error}</SingleError>}
        <Button
          type={"submit"}
          variant={"filled"}
          size={"large"}
          disabled={Object.keys(errors).length > 0 || !isDirty}
          isFetching={isLoading}
        >
          Отправить
        </Button>
      </StyledForm>
    </>
  )
}
