import type { FC } from "react"
import { useEffect, useState } from "react"
import { useAuth } from "../../../hooks/auth"
import { Controller, useForm } from "react-hook-form"
import { useMutation } from "react-query"
import { SuccessOverlay } from "../SuccessOverlay/SuccessOverlay"
import { FormGroup, StyledForm } from "../StyledForms"
import { Field } from "../../Field/Field"
import { SingleError } from "../../Typography/Typography"
import { Button } from "../../Button"
import { PhoneField } from "../../Field/PhoneField"
import { fetchRecall } from "../../../api/feedbackAPI"
import { TIMEOUT_SUCCESS } from "../../../utils/constants"
import { ApiError } from "../../../../contracts/contracts"
import {
  getPhoneWithCode,
  RULES_PHONE_VALIDATE,
} from "../../../validations/phone"
import { PrivacyAgreement } from "../PrivacyAgreement"
import { useGRecaptcha } from "../../../hooks/gReCaptcha"

type RecallFormFieldsType = {
  phone: string
  name: string
  dataPrivacyAgreement: boolean
}

export const Recall: FC = () => {
  const { user } = useAuth()
  const [error, setError] = useState<string | null>(null)

  const {
    handleSubmit,
    register,
    control,
    formState: { errors, isDirty },
    reset: resetForm,
    getValues,
  } = useForm<RecallFormFieldsType>({
    defaultValues: {
      phone: user?.phone || "",
      name: user?.fio || "",
      dataPrivacyAgreement: false,
    },
    mode: "onChange",
  })

  const {
    mutate: recallMutate,
    reset: resetMutation,
    isSuccess,
    isLoading,
  } = useMutation(fetchRecall, {
    onMutate: () => {
      setError(null)
    },
    onSuccess: () => {
      resetForm({
        name: "",
        dataPrivacyAgreement: false,
        phone: "",
      })
      setTimeout(() => {
        resetMutation()
      }, TIMEOUT_SUCCESS)
      setError(null)
    },
    onError: (err: ApiError) => {
      setError(err.message)
    },
  })

  const { run, isLoading: isLoadingGRecaptcha } = useGRecaptcha({
    cbMutate: () => {
      setError(null)
    },
    cbError: (err) => {
      setError(err?.message || null)
    },
    cbSuccess: () => {
      recallMutate({
        phone: getPhoneWithCode(getValues("phone")),
        name: getValues("name"),
      })
    },
  })

  const onSubmit = handleSubmit(() => {
    run()
  })

  useEffect(() => {
    resetForm({
      name: user?.fio || "",
      phone: user?.phone || "",
    })
  }, [resetForm, user?.phone, user?.fio])

  return (
    <>
      <SuccessOverlay isSuccess={isSuccess} />
      <StyledForm onSubmit={onSubmit}>
        <FormGroup>
          <Controller
            name={"name"}
            control={control}
            rules={{
              required: {
                value: true,
                message: "Поле обязательно для заполнения",
              },
            }}
            render={({ field, fieldState }) => (
              <Field
                type={"text"}
                value={field.value}
                placeholder={"Ваше имя"}
                onChange={field.onChange}
                errorMessage={fieldState.error?.message}
                withAnimatingLabel
              />
            )}
          />
        </FormGroup>
        <FormGroup>
          <Controller
            rules={RULES_PHONE_VALIDATE}
            render={({ field, fieldState }) => (
              <PhoneField
                value={field.value}
                onValueChange={({ value }) => {
                  field.onChange(value)
                }}
                errorMessage={fieldState?.error?.message}
              />
            )}
            control={control}
            name={"phone"}
          />
        </FormGroup>
        <PrivacyAgreement
          {...register("dataPrivacyAgreement", {
            required: {
              value: true,
              message: "Нам необходимо ваше согласие",
            },
          })}
          errorMessage={errors?.dataPrivacyAgreement?.message}
        />

        {error !== null && <SingleError>{error}</SingleError>}

        <Button
          type={"submit"}
          variant={"filled"}
          size={"large"}
          disabled={Object.keys(errors).length > 0 || !isDirty}
          isFetching={isLoading || isLoadingGRecaptcha}
        >
          Отправить
        </Button>
      </StyledForm>
    </>
  )
}
