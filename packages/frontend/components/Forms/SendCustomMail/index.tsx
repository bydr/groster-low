import { FC, useContext, useEffect, useMemo, useState } from "react"
import { FormGroup, StyledForm, StyledFormWrapper } from "../StyledForms"
import { Controller, useForm } from "react-hook-form"
import { Field } from "../../Field/Field"
import { useMutation } from "react-query"
import { fetchSendCustomEmail } from "../../../api/feedbackAPI"
import { TIMEOUT_SUCCESS } from "../../../utils/constants"
import { ApiError } from "next/dist/server/api-utils"
import { useGRecaptcha } from "../../../hooks/gReCaptcha"
import { ModalContext } from "../../Modals/Modal"
import { CustomRadioGroup } from "../../Radio/CustomRadioGroup"
import {
  FormConfigExtendsType,
  RadioGroupItemsType,
} from "../../../types/types"
import { SingleError } from "../../Typography/Typography"
import { Button } from "../../Button"
import { SuccessOverlay } from "../SuccessOverlay/SuccessOverlay"

const ITEMS: RadioGroupItemsType[] = [
  {
    value: "uncomfortable",
    message: "Неудобно",
  },
  {
    value: "func_not_working",
    message: "Не работает функционал",
  },
  {
    value: "not_usual",
    message: "Не привычно",
  },
  {
    value: "other",
    message: "Другое",
  },
]

export const SendCustomMail: FC<FormConfigExtendsType> = ({
  cbOnSubmit,
  successMessage,
}) => {
  const [error, setError] = useState<string | null>(null)

  const modalContext = useContext(ModalContext)

  const {
    handleSubmit,
    reset: resetForm,
    control,
    getValues,
    watch,
    formState: { errors, isSubmitting, isDirty },
    clearErrors,
  } = useForm<{
    message_static: string
    message_comment: string
  }>({
    defaultValues: {
      message_static: "",
      message_comment: "",
    },
    mode: "onChange",
  })

  const watchMessageStatic = watch("message_static")

  const isOther = useMemo(
    () => watchMessageStatic === "other",
    [watchMessageStatic],
  )
  const {
    reset: resetMutation,
    mutate: sendCustomEmailMutate,
    isLoading,
    isSuccess,
  } = useMutation(fetchSendCustomEmail, {
    onMutate: () => {
      setError(null)
    },
    onSuccess: () => {
      resetForm({
        message_static: "",
        message_comment: "",
      })
      setTimeout(() => {
        if (cbOnSubmit) {
          cbOnSubmit()
        }
      }, TIMEOUT_SUCCESS / 2)
      setTimeout(() => {
        resetMutation()
        modalContext?.hide()
      }, TIMEOUT_SUCCESS)
      setError(null)
    },
    onError: (err: ApiError) => {
      setError(err.message)
    },
  })

  const { run, isLoading: isLoadingReCaptcha } = useGRecaptcha({
    cbMutate: () => {
      setError(null)
    },
    cbSuccess: () => {
      sendCustomEmailMutate({
        message: isOther
          ? getValues("message_comment")
          : ITEMS.find((item) => item.value === getValues("message_static"))
              ?.message || "",
      })
    },
    cbError: (res) => {
      setError(res?.message || null)
    },
  })

  const onSubmit = handleSubmit(() => {
    run()
  })

  useEffect(() => {
    clearErrors("message_comment")
  }, [clearErrors, isOther])

  return (
    <>
      <StyledFormWrapper>
        <SuccessOverlay isSuccess={isSuccess} message={successMessage} />
        <StyledForm onSubmit={onSubmit}>
          <FormGroup>
            <Controller
              name={"message_static"}
              rules={{
                required: {
                  value: true,
                  message: "Выберите из списка или укажите свою причину",
                },
              }}
              control={control}
              render={({ field, fieldState }) => {
                return (
                  <CustomRadioGroup
                    variant={"rounds"}
                    items={ITEMS}
                    ariaLabel={"choices"}
                    onChange={field.onChange}
                    ref={field.ref}
                    errorMessage={fieldState.error?.message}
                  />
                )
              }}
            />
          </FormGroup>
          {isOther && (
            <>
              <FormGroup>
                <Controller
                  name={"message_comment"}
                  control={control}
                  rules={{
                    required: {
                      value: isOther,
                      message: "Поле обязательно для заполнения",
                    },
                  }}
                  render={({ field, fieldState }) => (
                    <Field
                      variant={"textarea"}
                      type={"text"}
                      value={field.value}
                      placeholder={"Введите свою причину"}
                      onChange={field.onChange}
                      errorMessage={fieldState.error?.message}
                      withAnimatingLabel
                      required={true}
                    />
                  )}
                />
              </FormGroup>
            </>
          )}

          {error !== null && <SingleError>{error}</SingleError>}

          <Button
            type={"submit"}
            variant={"filled"}
            size={"large"}
            disabled={Object.keys(errors).length > 0 || !isDirty}
            isFetching={isLoading || isSubmitting || isLoadingReCaptcha}
          >
            Отправить и перейти
          </Button>
        </StyledForm>
      </StyledFormWrapper>
    </>
  )
}
