import { styled } from "@linaria/react"
import { colors, transitionDefault } from "../../../styles/utils/vars"
import { cssIsActive } from "../../../styles/utils/Utils"
import { cssIcon } from "../../Icon"

export const StyledSuccessOverlay = styled.div<{ isActive?: boolean }>`
  position: absolute;
  left: 0;
  top: 0;
  z-index: 5;
  width: 100%;
  height: 100%;
  background: ${colors.white};
  display: flex;
  align-items: center;
  justify-content: center;
  text-align: center;
  flex-direction: column;
  color: ${colors.brand.purple};
  border-radius: inherit;
  transition: ${transitionDefault};
  opacity: 0;

  > * {
    color: inherit;
  }

  &.${cssIsActive} {
    opacity: 1;
  }

  .${cssIcon} {
    fill: ${colors.brand.purple};
  }
`
