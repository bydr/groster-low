import { FC } from "react"
import { Typography } from "../Typography/Typography"
import { StyledList, StyledListItem } from "../List/StyledList"
import { Link } from "../Link"
import { ListItemType } from "../../types/types"
import { SectionAdvice } from "./Styled"
import { ROUTES } from "../../utils/constants"

const adviceItems: ListItemType[] = [
  { path: ROUTES.help, title: "Покупателям" },
  { path: ROUTES.help, title: "Как сформировать счет" },
  { path: ROUTES.help, title: "Оформление заказа" },
  { path: ROUTES.help, title: "Подбор товаров" },
  { path: ROUTES.faq, title: "Вопрос-ответ" },
  { path: ROUTES.agree, title: "Конфиденциальность" },
  { path: ROUTES.faq, title: "Оформление заказа" },
  { path: ROUTES.help, title: "Помощь" },
  { path: ROUTES.paymentTerm, title: "Условия оплаты" },
  { path: ROUTES.deliveryTerm, title: "Условия доставки" },
  { path: ROUTES.productWarranty, title: "Гарантия на товар" },
  { path: ROUTES.contacts, title: "Покупателям" },
  { path: ROUTES.help, title: "Как сформировать счет" },
  { path: ROUTES.help, title: "Оформление заказа" },
  { path: ROUTES.help, title: "Подбор товаров" },
]

export const Advice: FC = () => {
  return (
    <>
      <SectionAdvice>
        <Typography variant={"h2"}>
          <b>Полезные советы</b>
        </Typography>
        <StyledList>
          {adviceItems.map(({ title, path }, index) => (
            <StyledListItem key={index}>
              <Link href={path}>
                <Typography variant={"p12"}>{title}</Typography>
              </Link>
            </StyledListItem>
          ))}
        </StyledList>
      </SectionAdvice>
    </>
  )
}
