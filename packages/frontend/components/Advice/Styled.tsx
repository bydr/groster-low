import { styled } from "@linaria/react"
import { StyledList, StyledListItem } from "../List/StyledList"
import { breakpoints } from "../../styles/utils/vars"
import { Section } from "../../styles/utils/Utils"

export const SectionAdvice = styled(Section)`
  padding-top: 16px;

  ${StyledList} {
    display: inline-grid;
    grid-template-rows: repeat(4, 1fr);
    grid-template-columns: auto;
    grid-auto-flow: column;
    grid-column-gap: 10px;

    ${StyledListItem} {
      min-width: 168px;
    }
  }

  @media (max-width: ${breakpoints.md}) {
    ${StyledList} {
      display: grid;
      grid-template-columns: 1fr 1fr;
      grid-template-rows: repeat(8, 1fr);
    }
  }
  @media (max-width: ${breakpoints.sm}) {
    display: none;
  }
`
