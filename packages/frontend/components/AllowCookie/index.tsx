import { FC, useEffect, useState } from "react"
import { Button } from "../Button"
import { Typography } from "../Typography/Typography"
import { COOKIE_ALLOWED_KEY, ROUTES } from "../../utils/constants"
import { ButtonGroup } from "../Button/StyledButton"
import { Popover } from "../Popover/Popover"
import Cookies from "universal-cookie"
import {
  StyledAllowCookie,
  StyledAllowCookieWrapper,
} from "./StyledAllowCookie"
import { Link } from "components/Link"
import { getExpireOneYear } from "../../utils/helpers"

const cookies = new Cookies()

export const AllowCookie: FC = () => {
  const [isAllowedCookie, setIsAllowedCookie] = useState<boolean | null>(null)

  useEffect(() => {
    setIsAllowedCookie(!!cookies.get(COOKIE_ALLOWED_KEY))
  }, [])

  return (
    <>
      {isAllowedCookie !== null && !isAllowedCookie && (
        <>
          <StyledAllowCookie>
            <StyledAllowCookieWrapper>
              <Popover
                disclosure={<Button />}
                size={"default"}
                isHiddenDisclosure
                hideOnClickOutside={false}
                isShow={true}
              >
                <Typography variant={"p12"}>
                  Мы используем файлы cookie, чтобы предоставить вам лучший
                  опыт. Файлы cookie помогают предоставить вам более
                  персонализированный опыт и релевантную рекламу, а нам —
                  веб-аналитику. Узнайте больше в наших{" "}
                  <Link href={ROUTES.agree}>
                    Правилах обработки информации.
                  </Link>
                </Typography>
                <ButtonGroup>
                  <Button
                    variant={"filled"}
                    onClick={() => {
                      setIsAllowedCookie(true)
                      cookies.set(COOKIE_ALLOWED_KEY, true, {
                        path: "/",
                        expires: getExpireOneYear(),
                      })
                    }}
                  >
                    Принять
                  </Button>
                </ButtonGroup>
              </Popover>
            </StyledAllowCookieWrapper>
          </StyledAllowCookie>
        </>
      )}
    </>
  )
}
