import { styled } from "@linaria/react"
import { PopoverContainer, StyledPopoverInner } from "../Popover/StyledPopover"

export const StyledAllowCookieWrapper = styled.div`
  position: relative;
  width: 100%;
  display: flex;
  padding: 0;
  max-width: 1200px;
  margin: 0 auto;
`

export const StyledAllowCookie = styled.div`
  position: fixed;
  bottom: 10px;
  width: 100%;
  z-index: 8;
  padding: 0 20px;

  ${PopoverContainer} {
    max-width: 100%;
  }

  ${StyledPopoverInner} {
    display: flex;
    flex-direction: row;
    gap: 10px;
  }
`
