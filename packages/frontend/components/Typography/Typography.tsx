import {
  breakpoints,
  colors,
  fontDefault,
  typography,
  TypographyVariantsType,
} from "../../styles/utils/vars"
import { styled } from "@linaria/react"
import { DetailedHTMLProps, FC, forwardRef, HTMLAttributes } from "react"
import { VariantPropsType } from "../../types/types"
import { cssIcon } from "../Icon"

type getTypographyBaseType = (variant: TypographyVariantsType) => string
export const getTypographyBase: getTypographyBaseType = (variant) => {
  let margin
  switch (variant) {
    case "h3": {
      margin = "0 0 16px 0"
      break
    }
    default: {
      margin = "0"
      break
    }
  }
  return `
    font-family: ${fontDefault};
    font-size: ${typography[variant].fs};
    line-height: ${typography[variant].lh};
    font-weight: ${typography[variant].fw};
    margin: ${margin};
  `
}

type TypographyPropsType = {
  color?: string
}

export const Span = styled.span<TypographyPropsType>`
  font-family: inherit;
  font-size: inherit;
  line-height: inherit;
  color: ${(props) => props.color || "inherit"};

  &[aria-disabled] {
    color: ${colors.gray};
  }
`
export const TypographyBase = styled.p<TypographyPropsType>`
  ${getTypographyBase("p14")};
  color: ${(props) => props.color || "initial"};
  margin-bottom: 1em;

  > ${Span} {
    + .${cssIcon} {
      margin-left: 6px;
    }
  }

  &[aria-disabled] {
    color: ${colors.gray};
  }
`
export const Heading1 = styled.h1`
  ${getTypographyBase("h1")};
  margin-bottom: 40px;
`
export const Heading2 = styled.h2`
  ${getTypographyBase("h2")};
  margin-bottom: 24px;
`
export const Heading3 = styled.h3`
  ${getTypographyBase("h3")};
`
export const Heading5 = styled.h5`
  ${getTypographyBase("h5")};
  margin-bottom: 10px;
`
export const Heading6 = styled.h6`
  ${getTypographyBase("h6")};
`
export const Paragraph16 = styled(TypographyBase)`
  ${getTypographyBase("default")};
`
export const Paragraph14 = styled(TypographyBase)``
export const Paragraph13 = styled(TypographyBase)`
  ${getTypographyBase("p13")};
`
export const Paragraph12 = styled(TypographyBase)`
  ${getTypographyBase("p12")};
`
export const Paragraph10 = styled(TypographyBase)`
  ${getTypographyBase("p10")};
`

export const NoWrap = styled(Span)`
  white-space: nowrap;
  flex-wrap: nowrap;
`

export const ErrorMessageDefault = styled(Paragraph12)`
  color: ${colors.red};
  margin-top: 6px;

  &:first-child {
    margin-top: 0;
  }
`

export const ErrorMessageField = styled(ErrorMessageDefault)`
  ${getTypographyBase("p12")}
  margin-top: 4px;
  word-break: break-word;
  margin-bottom: 0;
`

export const ErrorMessageContainer = styled.div`
  display: flex;
  width: 100%;
  flex-direction: column;
  align-items: flex-start;

  & ${ErrorMessageField} {
    margin: 0 0 6px 0;
  }
`

export const SingleError: FC = ({ children }) => (
  <ErrorMessageContainer>
    <ErrorMessageField>{children}</ErrorMessageField>
  </ErrorMessageContainer>
)

export const InitialsWord = styled(Span)`
  margin-right: 2px;
`
export const InitialsSymbol = styled(Span)``
export const InitialsBody = styled(Span)`
  position: relative;

  &:after {
    content: " ";
    position: relative;
  }

  @media (max-width: ${breakpoints.lg}) {
    display: none;
  }
  @media (max-width: ${breakpoints.md}) {
    display: initial;
  }
  @media (max-width: ${breakpoints.sm}) {
    display: none;
  }
`

export const Typography404 = styled.div`
  ${getTypographyBase("h1")};
  font-size: 146px;
  color: ${colors.brand.yellow};
  white-space: nowrap;

  @media (max-width: ${breakpoints.sm}) {
    font-size: 96px;
  }
`

export const Typography = forwardRef<
  HTMLHeadingElement,
  VariantPropsType<TypographyVariantsType> &
    DetailedHTMLProps<HTMLAttributes<HTMLHeadingElement>, HTMLHeadingElement>
>(({ variant, children, ...props }, ref) => {
  switch (variant) {
    case "h1": {
      return (
        <Heading1 {...props} ref={ref}>
          {children}
        </Heading1>
      )
    }
    case "h2": {
      return (
        <Heading2 {...props} ref={ref}>
          {children}
        </Heading2>
      )
    }
    case "h3": {
      return (
        <Heading3 {...props} ref={ref}>
          {children}
        </Heading3>
      )
    }
    case "h5": {
      return (
        <Heading5 {...props} ref={ref}>
          {children}
        </Heading5>
      )
    }
    case "h6": {
      return (
        <Heading6 {...props} ref={ref}>
          {children}
        </Heading6>
      )
    }
    case "p10": {
      return (
        <Paragraph10 {...props} ref={ref}>
          {children}
        </Paragraph10>
      )
    }
    case "p12": {
      return (
        <Paragraph12 {...props} ref={ref}>
          {children}
        </Paragraph12>
      )
    }
    case "p14": {
      return (
        <Paragraph14 {...props} ref={ref}>
          {children}
        </Paragraph14>
      )
    }
    case "default": {
      return (
        <Paragraph16 {...props} ref={ref}>
          {children}
        </Paragraph16>
      )
    }
    case "span": {
      return (
        <Span {...props} ref={ref}>
          {children}
        </Span>
      )
    }
    default: {
      return (
        <TypographyBase {...props} ref={ref}>
          {children}
        </TypographyBase>
      )
    }
  }
})

Typography.displayName = "Typography"
