import type { ButtonHTMLAttributes, DetailedHTMLProps } from "react"
import {
  BaseHTMLAttributes,
  forwardRef,
  HTMLAttributeAnchorTarget,
} from "react"
import StyledButton from "./StyledButton"
import {
  ButtonTagsType,
  IconPositionType,
  VariantPropsType,
} from "../../types/types"
import { IconNameType } from "../Icon"
import { breakpoints } from "../../styles/utils/vars"

export type ButtonVariantsType =
  | "filled"
  | "outline"
  | "small"
  | "link"
  | "translucent"
  | "box"
  | "arrow"

type HideTextOnBreakpointType = typeof breakpoints[keyof typeof breakpoints]

export type ButtonSizeType = "medium" | "small" | "large" | "smallM"
export type StyledButtonPropsType = {
  fullWidth?: boolean
}

export interface ButtonVariantsPropsType
  extends VariantPropsType<ButtonVariantsType> {
  disabled?: boolean
  icon?: IconNameType
  iconPosition?: IconPositionType
  fillIcon?: string
  as?: ButtonTagsType
  href?: string
  size?: ButtonSizeType
  hideTextOnBreakpoint?: HideTextOnBreakpointType
  isFetching?: boolean
  withArea?: boolean
  isHiddenBg?: boolean
  withOutOffset?: boolean
  isClean?: boolean
}

type ButtonProps = ButtonVariantsPropsType &
  DetailedHTMLProps<
    ButtonHTMLAttributes<HTMLButtonElement>,
    HTMLButtonElement
  > &
  StyledButtonPropsType &
  ButtonHTMLAttributes<HTMLButtonElement> &
  Pick<BaseHTMLAttributes<HTMLAttributeAnchorTarget>, "target">

const Button = forwardRef<HTMLButtonElement, ButtonProps>(
  ({ children, ...props }, ref) => {
    return (
      <StyledButton {...props} ref={ref}>
        {children}
      </StyledButton>
    )
  },
)
Button.displayName = "Button"

export default Button
