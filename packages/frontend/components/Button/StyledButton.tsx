import { styled } from "@linaria/react"
import {
  BoxShadowPopover,
  breakpoints,
  colors,
  fontDefault,
  sizeSVG,
  transitionDefault,
  transitionTimingFunction,
} from "../../styles/utils/vars"
import ButtonWithIcon from "../../hoc/buttonWithIcon"
import { cssIcon, getSizeSVG } from "../Icon"
import {
  getTypographyBase,
  Span,
  TypographyBase,
} from "../Typography/Typography"
import { css } from "@linaria/core"
import { ButtonVariantsPropsType, StyledButtonPropsType } from "./Button"
import {
  PushCounterOverlay,
  StyledPushCounter,
} from "../../styles/utils/PushCounter"
import { forwardRef } from "react"
import {
  StyledPrice,
  StyledPriceWrapper,
} from "../Products/parts/Price/StyledPrice"
import {
  StyledLoaderContainer,
  StyledLoaderOverlay,
} from "../Loaders/BaseLoader/StyledBaseLoader"
import { StyledBadge } from "../Products/Badges/Styled"

export const ButtonArea = styled.div`
  border-radius: 50px;
  font-size: inherit;
  font-family: inherit;
  line-height: inherit;
  padding: 2px 10px;
  display: flex;

  + ${Span} {
    flex: 1;
    text-align: center;
    margin-left: 10px;
    width: 100%;
    white-space: nowrap;
  }
`
export const ButtonBase = styled.button<StyledButtonPropsType>`
  ${getTypographyBase("p14")};
  position: relative;
  appearance: none;
  outline: none;
  background: ${colors.white};
  color: ${(props) => props.color || colors.black};
  font-weight: 600;
  padding: 0.3em 1em;
  border-radius: 50px;
  border: 2px solid transparent;
  cursor: pointer;
  font-family: ${fontDefault};
  transition: all 0.1s ${transitionTimingFunction};
  display: flex;
  align-items: center;
  justify-content: center;
  text-align: center;
  margin: 0;
  text-decoration: none;
  width: ${(props) => (props.fullWidth ? "100%" : "auto")};
  will-change: transform;

  &[disabled] {
    user-select: none !important;
    pointer-events: none !important;
    opacity: 0.3 !important;

    > .${cssIcon} {
      fill: ${colors.gray} !important;
    }
  }
  &[data-is-fetched] {
    user-select: none !important;
    pointer-events: none !important;
    opacity: 0.8 !important;

    > *:not(${StyledLoaderContainer}) {
      opacity: 0.3 !important;
    }
  }

  &[data-hidden-bg] {
    background: transparent;
  }

  &[data-is-offset="true"] {
    padding-left: 0 !important;
    padding-right: 0 !important;
  }

  > .${cssIcon} {
    fill: ${colors.brand.purple};
    transition: ${transitionDefault};

    + ${Span} {
      margin: 0 0 0 6px;
    }
  }

  ${ButtonArea} {
    color: ${colors.brand.yellow};
    background: ${colors.white};

    ${StyledPriceWrapper} {
      ${StyledPrice} {
        background: transparent;
        padding: 0;

        &[data-is-free="true"] {
          background: transparent !important;
        }
      }
    }

    + ${Span} {
      margin-right: 5px;
    }
  }

  &[data-icon-position="right"] {
    flex-direction: row-reverse;

    > .${cssIcon} {
      + span {
        margin: 0 6px 0 0;
      }
    }
  }

  ${StyledPushCounter} {
    position: absolute;
    right: 5px;
    top: 50%;
    transform: translateY(-50%);
  }
  ${PushCounterOverlay} {
    right: 3px;
  }

  > ${Span} {
    background: transparent;
  }

  &[data-size="large"] {
    ${getTypographyBase("default")};
    font-weight: 600;
    padding: 11px 20px;

    .${cssIcon} {
      ${getSizeSVG("24px")};
    }

    ${ButtonArea} {
      ${getTypographyBase("default")};
      font-weight: inherit;

      * {
        ${getTypographyBase("default")};
        font-weight: inherit;
      }
    }
  }

  &[data-size="small"] {
    ${getTypographyBase("p12")};
    font-weight: 500;
    padding: 2px 12px;
    border-width: 1px;

    .${cssIcon} {
      ${getSizeSVG("14px")};
    }
  }

  &[data-text-hide-breakpoints="${breakpoints.sm}"] {
    @media (max-width: ${breakpoints.sm}) {
      > *:not(.${cssIcon}) {
        display: none;
      }
    }
  }

  ${TypographyBase}, ${Span} {
    &:empty {
      display: none;
    }
  }

  ${StyledLoaderContainer} {
    top: -2px;
    bottom: -2px;
    left: -2px;
    right: -2px;
    height: auto;
    width: auto;
    border-radius: inherit;
    ${StyledLoaderOverlay} {
      border-radius: inherit;
    }
  }

  &:active {
    transform: scale(0.98);
  }

  &[data-with-area="true"] {
    padding: 2px 10px 2px 2px;
  }
`
export const ButtonFilled = styled(ButtonBase)`
  background: ${colors.brand.purple};
  color: ${colors.white};

  .${cssIcon} {
    fill: ${colors.brand.yellow};
  }

  ${ButtonArea} {
    background: ${colors.transparentWhite};
    color: ${colors.brand.yellow};
  }

  &:hover,
  &:active {
    background: ${colors.brand.purpleDarken};
    color: ${colors.brand.yellow};
  }
`

export const ButtonOutline = styled(ButtonBase)`
  background: transparent;
  color: ${colors.brand.purple};
  border-color: ${colors.brand.purple};

  &:hover,
  &:active {
    background: ${colors.brand.purple};
    color: ${colors.brand.yellow};
  }
`

export const ButtonSmall = styled(ButtonBase)`
  font-weight: 500;
  font-size: 0.75rem;
  line-height: 200%;
  background: ${colors.pinkLight};
  color: ${colors.brand.purple};
  padding: 2px 12px;
  border-width: 1px;

  .${cssIcon} {
    fill: ${colors.brand.yellow};
    ${getSizeSVG(sizeSVG.small)};
  }

  &:hover,
  &:active {
    background: ${colors.pink};

    > .${cssIcon} {
      fill: ${colors.brand.purple};
    }
  }

  &[data-hidden-bg] {
    background: transparent;

    &:hover,
    &:active {
      border-color: transparent;
      background: ${colors.pinkLight};

      > .${cssIcon} {
        fill: ${colors.brand.yellow};
      }
    }
  }

  &[disabled] {
    opacity: 1;
    background: ${colors.grayLight}!important;
    color: ${colors.grayDark}!important;
    user-select: none;
    pointer-events: none;

    .${cssIcon} {
      fill: ${colors.grayDark};
    }
  }
`
export const ButtonLink = styled(ButtonBase)`
  color: ${(props) => props.color || colors.black};
  background: transparent;
  border-color: transparent;
  font-weight: 500;
  text-align: left;

  &[data-size="large"] {
    font-weight: 600;
  }

  &[data-size="smallM"] {
    ${getTypographyBase("p13")}
  }

  &[data-hidden-bg] {
    color: ${(props) => props.color || colors.brand.purple};
    background: transparent;

    &:hover,
    &:active {
      background: transparent;
      color: ${(props) => props.color || colors.brand.purpleDarken};
    }
  }

  .${cssIcon} {
    fill: ${colors.brand.purple};
  }

  ${TypographyBase} & {
    display: inline-flex;
    padding: 0;
    margin: 0;
    color: ${colors.brand.purple};

    &:hover,
    &:active {
      color: ${colors.brand.purpleDarken};
    }
  }

  &:hover,
  &:active {
    color: ${colors.brand.purple};
    background: transparent;
    border-color: transparent;
    .${cssIcon} {
      fill: ${colors.brand.purple};
    }
  }
`
export const ButtonTranslucent = styled(ButtonBase)`
  background: ${colors.grayLight};
  color: ${colors.brand.purple};

  ${ButtonArea} {
    background: ${colors.white};
    color: ${colors.grayDarker};
  }

  .${cssIcon} {
    fill: ${colors.brand.yellow};
  }

  &:hover,
  &:active {
    background: ${colors.brand.purple};
    color: ${colors.white};

    > .${cssIcon} {
      fill: ${colors.white};
    }

    ${ButtonArea} {
      background: ${colors.transparentWhite};
      color: ${colors.brand.yellow};
    }
  }
`
export const ButtonBox = styled(ButtonBase)`
  width: 48px;
  height: 48px;
  display: flex;
  align-items: center;
  justify-content: center;
  text-align: center;
  border-radius: 50px;

  .${cssIcon} {
    fill: ${colors.brand.purple};
    margin: 0;
  }

  &:hover,
  &:active {
    .${cssIcon} {
      fill: ${colors.brand.yellow};
    }
  }
`

export const ButtonSliderArrow = styled(ButtonBox)`
  position: absolute;
  bottom: 100%;
  margin-bottom: 14px;

  &:first-of-type {
    right: 50px;
  }
  &:last-of-type {
    right: 0;
  }

  > .${cssIcon} {
    ${getSizeSVG("24px")};
    fill: ${colors.brand.yellow};
  }

  &:hover,
  &:active {
    background: ${colors.grayLight};
  }
`

type StyledButtonGroupPropsType = {
  justifyContent?: "flex-end" | "flex-start"
}

export const cssButtonGroupVertical = css``

export const ButtonGroup = styled.div<StyledButtonGroupPropsType>`
  width: 100%;
  display: flex;
  align-items: center;
  flex-wrap: wrap;
  flex: 1;
  justify-content: ${(props) => props.justifyContent || "flex-start"};
  margin: 0;

  &.${cssButtonGroupVertical} {
    flex-direction: column;
    align-items: flex-start;

    > *,
    > ${ButtonBase} {
      margin: 0 0 10px 0;
    }
  }

  > *,
  > ${ButtonBase} {
    margin: 0 10px 4px 0;

    &:last-child {
      margin-right: 0;
    }
  }

  @media (max-width: 550px) {
    flex-direction: column;
    > ${ButtonBase} {
      width: 100%;
      margin: 0 0 10px 0;
    }
    > * {
      margin: 0 0 10px 0;
    }
  }
`
export const CheckoutButtonGroup = styled(ButtonGroup)`
  > * {
    margin: 0;
  }
`

//helpers selectors for buttons
export const cssButtonHelpChoose = css``
export const cssButtonFastBuy = css`
  width: 100%;
`
export const cssButtonAddFavorite = css`
  &${ButtonLink} {
    .${cssIcon} {
      fill: ${colors.gray};
    }

    &[data-isfavorite="true"] {
      .${cssIcon} {
        fill: ${colors.brand.yellow};
      }
    }
  }
`
export const cssButtonMenuToggle = css`
  display: none !important;

  @media (max-width: ${breakpoints.lg}) {
    display: flex !important;
  }
`
export const cssButtonToFavorite = css``
export const cssButtonToCart = css`
  background: ${colors.brand.yellow};

  &:hover,
  &:active {
    background: ${colors.brand.purple};
  }
`
export const cssButtonInform = css`
  grid-area: toCart;
`
export const cssButtonInCart = css`
  background: ${colors.green};
  ${ButtonBase}& {
    background: ${colors.green};

    > * {
      flex: initial;
    }

    &:hover,
    &:active {
      background: ${colors.green};
      color: ${colors.white};
    }

    .${cssIcon} {
      fill: ${colors.white};
    }
  }
`
export const cssButtonCompoundToggle = css``
export const cssButtonFill = css`
  &,
  &${ButtonBase} {
    background: ${colors.brand.purple};
    color: ${colors.white};

    > .${cssIcon} {
      fill: ${colors.white};
    }

    &:hover,
    &:active {
      background: ${colors.brand.purple};
      color: ${colors.white};

      > .${cssIcon} {
        fill: ${colors.brand.yellow};
      }
    }
  }
`
export const cssButtonLogout = css``
export const cssButtonBackward = css`
  &${ButtonBase} {
    color: ${colors.grayDark};
    padding: 0;
    margin-bottom: 20px;

    &:hover,
    &:active {
      color: ${colors.black};
      background: transparent !important;
    }
  }
`
export const cssButtonClearFilters = css`
  ${ButtonBase}& {
    padding-top: 0;
    padding-bottom: 0;
  }
`
export const cssButtonClean = css`
  &,
  ${ButtonBase}& {
    padding: 0;
    margin: 0;
    font-weight: normal;
  }
`
export const cssButtonToTop = css`
  &,
  &${ButtonBase} {
    position: fixed;
    z-index: 9;
    right: 20px;
    bottom: 100px;
    background: ${colors.brand.purple};
    box-shadow: ${BoxShadowPopover};

    .${cssIcon} {
      fill: ${colors.white};
    }
  }
`
export const cssButtonFloat = css`
  &,
  &${ButtonBase} {
    position: fixed;
    left: 20px;
    bottom: 100px;
    z-index: 5;

    ${StyledBadge} {
      position: absolute;
    }
  }
`

export const cssButtonPulse = css`
  box-shadow: 0 0 0 0 ${colors.brand.purple};
  animation: pulse 2s infinite;

  @keyframes pulse {
    0% {
      transform: scale(0.95);
      box-shadow: 0 0 0 0 ${colors.brand.purpleTransparent7};
    }

    70% {
      transform: scale(1);
      box-shadow: 0 0 0 20px ${colors.brand.purpleTransparent0};
    }

    100% {
      transform: scale(0.95);
      box-shadow: 0 0 0 0 ${colors.brand.purpleTransparent0};
    }
  }
`

export type StyledButtonComponentType = typeof ButtonBase

const StyledButton = forwardRef<HTMLButtonElement, ButtonVariantsPropsType>(
  ({ variant, children, ...props }, ref) => {
    switch (variant) {
      case "filled": {
        return (
          <ButtonWithIcon ref={ref} ButtonComponent={ButtonFilled} {...props}>
            {children}
          </ButtonWithIcon>
        )
      }
      case "outline": {
        return (
          <ButtonWithIcon ref={ref} ButtonComponent={ButtonOutline} {...props}>
            {children}
          </ButtonWithIcon>
        )
      }
      case "small": {
        return (
          <ButtonWithIcon ref={ref} ButtonComponent={ButtonSmall} {...props}>
            {children}
          </ButtonWithIcon>
        )
      }
      case "link": {
        return (
          <ButtonWithIcon ref={ref} ButtonComponent={ButtonLink} {...props}>
            {children}
          </ButtonWithIcon>
        )
      }
      case "translucent": {
        return (
          <ButtonWithIcon
            ref={ref}
            ButtonComponent={ButtonTranslucent}
            {...props}
          >
            {children}
          </ButtonWithIcon>
        )
      }
      case "box": {
        return (
          <ButtonWithIcon ref={ref} ButtonComponent={ButtonBox} {...props}>
            {children}
          </ButtonWithIcon>
        )
      }
      case "arrow": {
        return (
          <ButtonWithIcon
            ref={ref}
            ButtonComponent={ButtonSliderArrow}
            {...props}
          >
            {children}
          </ButtonWithIcon>
        )
      }
      default: {
        return (
          <ButtonWithIcon ref={ref} ButtonComponent={ButtonBase} {...props}>
            {children}
          </ButtonWithIcon>
        )
      }
    }
  },
)
StyledButton.displayName = "StyledButton"

export default StyledButton
