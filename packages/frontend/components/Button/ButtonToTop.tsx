import { FC, useEffect, useState } from "react"
import { Button } from "./index"
import { cssButtonToTop } from "./StyledButton"

const getIsVisible = (): boolean => (window?.scrollY || 0) > 100

export const ButtonToTop: FC = () => {
  const [isVisible, setIsVisible] = useState(false)

  useEffect(() => {
    const scrollHandler = () => {
      setIsVisible(getIsVisible())
    }
    window.addEventListener("scroll", scrollHandler)
    return () => {
      window.removeEventListener("scroll", scrollHandler)
    }
  }, [])

  return (
    <>
      {isVisible && (
        <Button
          variant={"box"}
          icon={"ArrowUp"}
          className={cssButtonToTop}
          onClick={(e) => {
            e.preventDefault()
            window.scrollTo({ top: 0, left: 0, behavior: "smooth" })
          }}
        />
      )}
    </>
  )
}
