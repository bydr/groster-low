import { FC, FormEvent, useCallback, useEffect, useState } from "react"
import {
  IAvailableParam,
  IAvailableParams,
  IFilterParamChild,
} from "../../../store/reducers/catalogSlice"
import { FilterPopoverType } from "../Popover/FilterPopover"
import {
  StyledFilterImage,
  StyledFilterInner,
  StyledFilterItem,
  StyledFilterList,
} from "../StyledFilter"
import { CustomCheckbox } from "../../Checkbox/CustomCheckbox"
import { StyledFilterCounter } from "../Popover/StyledFilterPopover"
import dynamic, { DynamicOptions } from "next/dynamic"
import { ClearItemFilterPropsType } from "../../../types/types"

const DynamicFilterPopover = dynamic((() =>
  import("../Popover/FilterPopover").then(
    (mod) => mod.FilterPopover,
  )) as DynamicOptions<FilterPopoverType>)

export type FilterHandlersType = {
  onUpdateFiltersHandle: <T>(props?: T) => void
  onChangeCheckboxHandle: (
    e: FormEvent<HTMLInputElement>,
    isDisabledField: boolean,
  ) => void
  onClearItemHandle: ({
    keyFilter,
    checked,
    name,
  }: ClearItemFilterPropsType) => void
}

export const FilterList: FC<
  {
    filters: IAvailableParams | null
    visibleDefault?: boolean
    isResponsive?: boolean
  } & FilterHandlersType
> = ({
  filters,
  onClearItemHandle,
  onChangeCheckboxHandle,
  visibleDefault = false,
  isResponsive = false,
}) => {
  const [filtersSorted, setFiltersSorted] = useState<IAvailableParam[]>([])

  const clearItemHandle = useCallback(
    (props: { keyFilter: string[]; checked: boolean; name?: string }) => {
      onClearItemHandle(props)
    },
    [onClearItemHandle],
  )

  const changeCheckboxHandle = useCallback(
    (e: FormEvent<HTMLInputElement>, isDisabledField: boolean) => {
      onChangeCheckboxHandle(e, isDisabledField)
    },
    [onChangeCheckboxHandle],
  )

  useEffect(() => {
    if (filters !== null) {
      setFiltersSorted(
        Object.keys(filters)
          .map((key) => filters[key])
          .sort((a, b) =>
            (a?.name || "") > (b?.name || "")
              ? 1
              : (a?.name || "") < (b?.name || "")
              ? -1
              : 0,
          )
          .sort((a, b) =>
            (a?.order || 0) > (b?.order || 0)
              ? 1
              : (a?.order || 0) < (b?.order || 0)
              ? -1
              : 0,
          ),
      )
    } else {
      setFiltersSorted([])
    }
  }, [filters])

  return (
    <>
      {filtersSorted.length > 0 &&
        filtersSorted.map((filter) => {
          return (
            <DynamicFilterPopover
              key={filter.uuid || ""}
              title={filter.name || ""}
              isActive={!!filter.checkedKeys.length}
              selectedKeys={filter.checkedKeys}
              onClearFilterItem={clearItemHandle}
              name={filter.uuid}
              visibleDefault={visibleDefault}
              isResponsive={isResponsive}
            >
              <StyledFilterInner>
                <StyledFilterList>
                  {Object.keys(filter.params).map((key) => {
                    const val: IFilterParamChild = filter.params[key]
                    const isDisabled =
                      val.product_qty !== undefined
                        ? val.product_qty <= 0
                        : false

                    return (
                      <StyledFilterItem key={val.uuid} isDisabled={isDisabled}>
                        <CustomCheckbox
                          variant={"check"}
                          name={val.uuid || ""}
                          message={val?.name || "Фильтр"}
                          stateCheckbox={val.checked}
                          onChange={(e) => changeCheckboxHandle(e, isDisabled)}
                        />
                        {!!val.product_qty && (
                          <>
                            <StyledFilterCounter>
                              {val.product_qty}
                            </StyledFilterCounter>
                          </>
                        )}
                        {val.image && (
                          <StyledFilterImage
                            style={{
                              backgroundImage: `url(${val.image})`,
                            }}
                          />
                        )}
                      </StyledFilterItem>
                    )
                  })}
                </StyledFilterList>
              </StyledFilterInner>
            </DynamicFilterPopover>
          )
        })}
    </>
  )
}
