import { styled } from "@linaria/react"
import { breakpoints, colors, sizeSVG } from "../../styles/utils/vars"
import { ButtonBase, ButtonLink, ButtonSmall } from "../Button/StyledButton"
import { getTypographyBase } from "../Typography/Typography"
import { css } from "@linaria/core"
import { CheckboxBase } from "../Checkbox/StyledCheckbox"
import { cssIcon, getSizeSVG } from "../Icon"
import { cssIsActive, getCustomizeScroll } from "../../styles/utils/Utils"
import {
  PopoverContainerFilter,
  StyledPopoverDisclosureFilter,
} from "./Popover/StyledFilterPopover"
import { StyledPopoverInner } from "../Popover/StyledPopover"
import { StyledDialog } from "../Modals/StyledModal"
import { StyledFieldWrapper } from "../Field/StyledField"
import {
  StyledSelect,
  StyledSelectedTitle,
  StyledSelectInputDiv,
} from "../Select/StyledSelect"

export const StyledFilterImage = styled.div`
  width: 14px;
  height: 14px;
  min-height: 14px;
  min-width: 14px;
  border-radius: 50px;
  border: 1px solid ${colors.white};
  margin-left: 10px;
  position: relative;
  background-repeat: no-repeat;
  background-position: center;
  background-size: cover;
  background-color: ${colors.grayLight};
`
export const StyledFilterItem = styled.div<{ isDisabled?: boolean }>`
  display: flex;
  width: 100%;
  align-items: center;
  margin-bottom: 4px;
  opacity: ${(props) => (props.isDisabled ? 0.2 : 1)};

  ${CheckboxBase} {
    margin: 0;
    flex: 1;
    user-select: ${(props) => (props.isDisabled ? `none` : `initial`)};
    pointer-events: ${(props) => (props.isDisabled ? `none` : `initial`)};
    cursor: ${(props) => (props.isDisabled ? `not-allowed` : `pointer`)};
  }
`
export const StyledFilterList = styled.div`
  ${getCustomizeScroll()};
  width: auto;
  max-height: 290px;
  overflow-x: hidden;
  overflow-y: auto;
  margin-right: -14px;
  margin-left: -14px;
  padding-right: 14px;
  padding-left: 14px;
`

export const StyledFilterInner = styled.div`
  width: 100%;

  ${ButtonBase} {
    margin-top: 12px;
    margin-bottom: 0;
  }
`
export const cssFilterGroupRadios = css``
export const cssFilterGroupViewMode = css``
export const cssButtonFilterDialogTrigger = css`
  display: none !important;

  &${ButtonSmall} {
    ${getTypographyBase("p14")};
    line-height: 171%;
    margin: 0;
    padding-top: 3px;
    padding-bottom: 3px;

    .${cssIcon} {
      fill: ${colors.brand.purple};
      ${getSizeSVG(sizeSVG.smaller)};
    }

    &.${cssIsActive} {
      background: ${colors.brand.purple};
      color: ${colors.white};

      .${cssIcon} {
        fill: ${colors.white};
      }

      &:hover &:active {
        color: ${colors.brand.purple};
      }
    }
  }

  @media (max-width: ${breakpoints.lg}) {
    display: flex !important;
  }
`
export const StyledFilterRow = styled.div`
  display: flex;
  width: 100%;
  flex-direction: row;
  flex-wrap: wrap;
`
export const StyledFilterColumn = styled.div`
  position: relative;
  display: flex;
  align-items: flex-start;
  flex-wrap: wrap;
  max-width: 100%;
  gap: 10px;
  flex: 1;

  &:last-of-type {
    justify-content: flex-end;
    flex: 5;
  }
  &:first-child {
    justify-content: flex-start;
    flex: 6.5;
  }

  @media (max-width: ${breakpoints.lg}) {
    &:last-of-type {
      flex: 10;
    }
    &:first-child {
      flex: 2;
    }
  }
`

export const StyledFilterGroup = styled.div`
  display: inline-flex;
  align-items: center;
  max-width: 100%;

  ${ButtonLink} {
    margin: 0;
    padding: 5px;
    border: none;

    svg {
      fill: ${colors.black};
    }

    &[data-view-disabled="false"] {
      svg {
        fill: ${colors.black};
      }

      &:hover,
      &:active {
        svg {
          fill: ${colors.black};
        }
      }
    }

    &[data-view-disabled="true"] {
      svg {
        fill: ${colors.gray};
      }

      &:hover,
      &:active {
        svg {
          fill: ${colors.gray};
        }
      }
    }
  }

  > ${StyledFieldWrapper} {
    margin: 0;
  }
`
export const StyledFiltersContainer = styled.div`
  width: 100%;
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  margin-bottom: 20px;

  @media (max-width: ${breakpoints.lg}) {
    ${StyledFilterRow} {
      &:last-child {
        ${StyledFilterColumn} {
          &:first-of-type {
            width: auto;
          }
        }
      }
    }

    .${cssFilterGroupViewMode} {
      display: none;
    }
  }

  @media (max-width: ${breakpoints.md}) {
    .${cssFilterGroupRadios} {
      display: none;
    }
  }
`

export const TotalControls = styled.div`
  gap: 10px;
  width: 100%;
  position: sticky;
  bottom: 0;
  background: ${colors.white};
  z-index: 10;
  grid-auto-flow: column;
  display: none;
  padding: 30px 0;

  @media (max-width: ${breakpoints.xs}) {
    ${ButtonBase} {
      ${getTypographyBase("p14")};
      padding: 0.3em 1em;
    }
  }
`
export const StyledFilters = styled.div`
  background: ${colors.white};
  display: flex;
  align-items: center;
  flex-wrap: wrap;
  width: 100%;
  flex: 1;

  > ${ButtonLink} {
    display: inline-flex;
    margin-bottom: 0;
    order: 9999999;
  }

  ${PopoverContainerFilter} {
    &:first-child {
      ${StyledPopoverDisclosureFilter} {
        border-top-left-radius: 1.18em;
      }
    }
    &:last-of-type {
      ${StyledPopoverDisclosureFilter} {
        border-bottom-right-radius: 1.18em;
      }
    }
  }

  @media (max-width: ${breakpoints.lg}) {
    flex-direction: column;
    align-items: flex-start;
    display: none;

    ${PopoverContainerFilter}, ${StyledPopoverDisclosureFilter} {
      width: 100%;
      display: flex;
    }

    ${StyledPopoverDisclosureFilter} {
      border-radius: 0;
      background: transparent;
      padding: 18px 0;

      &[aria-expanded="true"] {
        background: transparent !important;
        border-radius: 0;
      }

      .${cssIcon}:last-child {
        margin: 0 0 0 auto;
      }
    }

    ${StyledPopoverInner} {
      padding: 0;
      border: none;
      margin-bottom: 18px;
    }

    ${StyledDialog} & {
      display: flex;
    }

    ${StyledFilterGroup} {
      display: flex;
      flex-direction: column;
      align-items: flex-start;
      width: 100%;
      padding: 18px 0 0 0;

      ${CheckboxBase} {
        margin-bottom: 18px;
      }
    }

    ${StyledFieldWrapper} {
      width: 100%;
    }

    ${StyledSelect} {
      &,
      &[data-variant] {
        [role="combobox"] {
          width: 100%;
          background: transparent;
          max-width: 100%;
          padding: 0;
        }
        ${StyledSelectedTitle} {
          left: 0;
        }

        ${StyledSelectInputDiv} {
          > .${cssIcon} {
            right: 0;
          }
        }

        [role="listbox"] {
          position: relative !important;
          margin-bottom: 20px;
        }
      }
    }
  }

  @media (max-width: ${breakpoints.md}) {
    ${StyledFilterList} {
      max-height: initial;
    }
  }
`
