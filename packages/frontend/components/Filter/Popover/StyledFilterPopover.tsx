import { styled } from "@linaria/react"
import { breakpoints, colors } from "../../../styles/utils/vars"
import { Paragraph10 } from "../../Typography/Typography"
import { cssIcon } from "../../Icon"
import { ButtonBase } from "../../Button/StyledButton"
import {
  PopoverContainer,
  StyledPopoverDisclosure,
  StyledPopoverInner,
} from "../../Popover/StyledPopover"
import { cssHidden } from "../../../styles/utils/Utils"

export const StyledPopoverDisclosureFilter = styled(StyledPopoverDisclosure)`
  appearance: none;
  color: ${colors.black};

  &[aria-expanded="true"] {
    background: ${colors.gray};
  }

  &:hover {
    background: ${colors.gray};
  }

  ${ButtonBase} {
    padding-right: 0;
    margin: 0;

    .${cssIcon} {
      fill: ${colors.brand.yellow};
      margin: 0;
    }

    &:hover,
    &:active {
      .${cssIcon} {
        fill: ${colors.brand.yellow};
      }
    }
  }

  @media (max-width: ${breakpoints.lg}) {
    &:hover {
      background: transparent;
    }
  }
`
export const PopoverContainerFilter = styled(PopoverContainer)`
  &[data-is-active="true"] {
    ${StyledPopoverDisclosure} {
      background: ${colors.grayDark};
      color: ${colors.white};

      ${ButtonBase} {
        .${cssIcon} {
          fill: ${colors.brand.yellow};
          transform: none;
        }
      }
    }
  }

  &[role="dialog"] {
    max-width: 380px;
  }

  @media (max-width: ${breakpoints.lg}) {
    border-bottom: 1px solid ${colors.gray};
    position: relative;
    flex-direction: column;

    [role="dialog"] {
      transform: none !important;
      inset: 100% 0 0 0 !important;
      position: relative !important;
    }

    &[data-is-active="true"] {
      ${StyledPopoverDisclosure} {
        background: transparent;
        color: ${colors.black};
      }

      .${cssIcon}.${cssHidden} {
        display: initial;
      }
    }

    ${StyledPopoverInner} {
      box-shadow: none;
    }
  }
`
export const StyledFilterCounter = styled(Paragraph10)`
  color: ${colors.grayDark};
`
