import { FC, MouseEvent, useEffect, useRef, useState } from "react"
import { Popover, usePopoverState } from "reakit/Popover"
import {
  PopoverContainerFilter,
  StyledPopoverDisclosureFilter,
} from "./StyledFilterPopover"
import { Button } from "../../Button"
import { StyledPopoverInner } from "../../Popover/StyledPopover"
import { ANIMATED_TRANSITION_DEFAULT, colors } from "../../../styles/utils/vars"
import { cssHidden } from "../../../styles/utils/Utils"
import { Icon } from "../../Icon"
import { ClearItemFilterPropsType } from "../../../types/types"

export type FilterPopoverType = {
  title: string
  isActive?: boolean
  selectedKeys?: string[]
  onClearFilterItem?: ({
    keyFilter,
    checked,
    name,
  }: ClearItemFilterPropsType) => void
  order?: number
  name?: string
  onShow?: () => void
  onHide?: () => void
  visibleDefault?: boolean
  isResponsive?: boolean
}

export const FilterPopover: FC<FilterPopoverType> = ({
  title,
  isActive = false,
  selectedKeys = [],
  name,
  onClearFilterItem,
  onShow,
  onHide,
  visibleDefault,
  isResponsive = false,
  children,
}) => {
  const [isInit, setIsInit] = useState(false)
  const popover = usePopoverState({
    animated: ANIMATED_TRANSITION_DEFAULT,
    gutter: 16,
    placement: "bottom-start",
  })
  const previousVisibleRef = useRef(!!visibleDefault)

  const clearFilters = (event: MouseEvent<HTMLButtonElement>) => {
    event.preventDefault()
    if (onClearFilterItem) {
      onClearFilterItem({
        keyFilter: selectedKeys,
        checked: false,
        name: name,
      })
    }
  }

  useEffect(() => {
    if (previousVisibleRef.current !== popover.visible) {
      if (!isInit) {
        // for responsive visible all
        if (
          visibleDefault !== undefined &&
          visibleDefault !== popover.visible
        ) {
          popover.setVisible(true)
        }
      } else {
        //default handler
        if (!popover.visible) {
          if (onHide) {
            onHide()
          }
        } else {
          if (onShow) {
            onShow()
          }
        }

        previousVisibleRef.current = popover.visible
      }
    }

    setIsInit(true)
  }, [popover.visible, isInit, visibleDefault, popover, onHide, onShow])

  return (
    <PopoverContainerFilter data-is-active={isActive}>
      <StyledPopoverDisclosureFilter {...popover}>
        {title}
        {isActive && !!selectedKeys?.length && `: ${selectedKeys?.length}`}
        {isActive && (
          <Button variant={"link"} icon={"X"} onClick={clearFilters} as={"a"} />
        )}
        <Icon
          NameIcon={"AngleBottom"}
          fill={colors.brand.purple}
          className={isActive ? cssHidden : ""}
        />
      </StyledPopoverDisclosureFilter>
      <Popover
        {...popover}
        visible={
          visibleDefault !== undefined && !isInit
            ? visibleDefault
            : popover.visible
        }
        hideOnClickOutside={!isResponsive}
        aria-label={name || "popoverFilter"}
      >
        <StyledPopoverInner>{children}</StyledPopoverInner>
      </Popover>
    </PopoverContainerFilter>
  )
}
