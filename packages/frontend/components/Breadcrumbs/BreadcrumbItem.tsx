import { DetailedHTMLProps, FC, HTMLAttributes } from "react"
import {
  BreadcrumbsSeparator,
  StyledBreadcrumbItemWrapper,
} from "./StyledBreadcrumbs"
import { Icon, IconNameType } from "../Icon"
import { NavigationItem } from "../Navigation/NavigationItem"
import { NavItemType } from "../../types/types"

export type BreadcrumbItemType = {
  path: string
  title: string
  separator?: IconNameType
  subItems?: NavItemType[]
  trigger?: string
}

const BreadcrumbItem: FC<
  BreadcrumbItemType &
    DetailedHTMLProps<HTMLAttributes<HTMLDivElement>, HTMLDivElement>
> = ({ path, title, separator = "AngleRight", subItems = [], ...props }) => {
  return (
    <StyledBreadcrumbItemWrapper {...props}>
      <BreadcrumbsSeparator>
        <Icon NameIcon={separator} />
      </BreadcrumbsSeparator>
      <NavigationItem
        title={title}
        subItems={subItems}
        path={path}
        as={"div"}
        variant={"gray-dark-to-black"}
        isScroll
        isLink
        isStylingIconDisclosure={false}
      />
    </StyledBreadcrumbItemWrapper>
  )
}

export default BreadcrumbItem
