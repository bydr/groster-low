import { styled } from "@linaria/react"
import { breakpoints, colors, sizeSVG } from "../../styles/utils/vars"
import { getTypographyBase } from "../Typography/Typography"
import { cssIcon, getSizeSVG } from "../Icon"
import { css } from "@linaria/core"
import {
  PopoverContainer,
  StyledPopoverDisclosure,
  StyledPopoverInner,
} from "../Popover/StyledPopover"
import { StyledListItem } from "../List/StyledList"
import { StyledLinkBase } from "../Link/StyledLink"
import { ButtonBase } from "../Button/StyledButton"

export const BreadcrumbsSeparator = styled.div``
export const StyledBreadcrumbItemWrapper = styled.div`
  display: inline-flex;
  align-items: center;
  background: transparent;

  ${BreadcrumbsSeparator} {
    .${cssIcon} {
      ${getSizeSVG(sizeSVG.smaller)}
      margin: 0 8px;
      fill: ${colors.brand.yellow};
    }
  }

  @media (max-width: ${breakpoints.md}) {
    [role="dialog"] {
      display: none !important;
    }
  }
`
export const cssBreadcrumbNavLink = css`
  display: none !important;

  &,
  &${ButtonBase} {
    margin-bottom: 16px;
    width: initial;
    justify-content: flex-start;
    justify-self: self-start;
    background: transparent;

    &:hover,
    &:active {
      background: transparent !important;
    }
  }

  @media (max-width: ${breakpoints.md}) {
    display: flex !important;
  }
`

export const StyledBreadcrumbs = styled.div`
  display: inline-flex;
  align-items: center;
  margin-bottom: 12px;
  position: relative;
  background: transparent;
  border-radius: 50px;
  padding-right: 8px;
  width: 100%;

  ${StyledBreadcrumbItemWrapper} {
    &:first-of-type:not(.${cssBreadcrumbNavLink}) {
      ${BreadcrumbsSeparator} {
        display: none;
      }
    }

    ${PopoverContainer} {
      margin: 0;

      ${StyledPopoverDisclosure} {
        &[aria-expanded="true"] {
          color: ${colors.black};
        }

        .${cssIcon} {
          display: none;
        }
      }

      & ${StyledPopoverInner} {
        padding: 5px 10px;

        ${StyledListItem} {
          color: ${colors.black};

          &:hover,
          &:active {
            color: ${colors.brand.purple};
          }
        }
      }
    }

    ${PopoverContainer} ${StyledPopoverDisclosure}, ${StyledListItem} {
      ${getTypographyBase("p12")};
      background: transparent;
      padding: 0;
      margin: 0;
      color: ${colors.grayDark};
      text-decoration: none;
      display: inline-flex;

      &:hover,
      &:active {
        color: ${colors.black};
      }
    }

    ${StyledListItem} > ${StyledLinkBase},  ${PopoverContainer} ${StyledPopoverDisclosure} {
      max-width: 300px;
      white-space: nowrap;
      text-overflow: ellipsis;
      overflow: hidden;
      display: inline-block;
    }
  }

  @media (max-width: ${breakpoints.md}) {
    flex-wrap: wrap;
    white-space: normal;
  }
`
