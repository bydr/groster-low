import type { FC } from "react"

import { cssBreadcrumbNavLink, StyledBreadcrumbs } from "./StyledBreadcrumbs"
import BreadcrumbItem, { BreadcrumbItemType } from "./BreadcrumbItem"
import { ROUTES } from "../../utils/constants"
import { useRouter } from "next/router"
import { Button } from "../Button"
import { cssButtonClean } from "../Button/StyledButton"
import { cx } from "@linaria/core"
import { useWindowSize } from "../../hooks/windowSize"
import { getBreakpointVal } from "../../styles/utils/Utils"
import { breakpoints } from "../../styles/utils/vars"

export type BreadcrumbsPropsType = {
  breadcrumbs: Record<string, BreadcrumbItemType> | null
}

export const Breadcrumbs: FC<BreadcrumbsPropsType> = ({ breadcrumbs }) => {
  const router = useRouter()
  const { width } = useWindowSize()
  const brdK: string[] = Object.keys(breadcrumbs || {})

  return (
    <>
      {breadcrumbs === null ? (
        <></>
      ) : (
        <>
          <StyledBreadcrumbs>
            <BreadcrumbItem path={ROUTES.catalog} title={"Каталог"} />

            {brdK.map((key) => {
              const item: BreadcrumbItemType = breadcrumbs[key]
              return (
                <BreadcrumbItem
                  key={key}
                  path={item.path}
                  title={item?.title || ""}
                  subItems={
                    width !== undefined &&
                    width <= getBreakpointVal(breakpoints.md)
                      ? []
                      : item.subItems
                  }
                />
              )
            })}
          </StyledBreadcrumbs>

          {brdK && (
            <Button
              onClick={() => {
                router.back()
              }}
              type={"button"}
              variant={"small"}
              icon={"ArrowLeft"}
              isHiddenBg
              className={cx(cssButtonClean, cssBreadcrumbNavLink)}
            >
              Назад
            </Button>
          )}
        </>
      )}
    </>
  )
}
