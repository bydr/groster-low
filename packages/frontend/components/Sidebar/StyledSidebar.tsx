import { styled } from "@linaria/react"
import { colors } from "../../styles/utils/vars"
import { StyledList, StyledListItem } from "../List/StyledList"
import { css } from "@linaria/core"
import { cssIsActive } from "../../styles/utils/Utils"
import { getTypographyBase } from "../Typography/Typography"

export const cssSidebarGray = css``
export const StyledSidebar = styled.nav`
  position: relative;
  background: ${colors.grayLight};
  border-radius: 0 4.5rem 4.5rem 0;
  padding: 40px 43px 40px 0;
  box-shadow: -100px 0 0 0 ${colors.grayLight};
  z-index: 5;

  &.${cssSidebarGray} {
    background: ${colors.white};
    box-shadow: -65px 0 0 0 ${colors.white};

    ${StyledList} {
      ${StyledListItem} {
        background: transparent;
        cursor: pointer;

        &.${cssIsActive} {
          background: ${colors.grayLight};
        }
      }
    }
  }

  ${StyledList} {
    display: flex;
    flex-direction: column;
    align-items: flex-start;

    ${StyledListItem} {
      ${getTypographyBase("p14")};
      margin-right: 0;
      padding: 2px 14px;
      background: transparent;
      border-radius: 50px;
      margin-bottom: 6px;
      cursor: pointer;

      &.${cssIsActive} {
        background: ${colors.white};
        color: ${colors.black};
      }

      &:hover:not(.${cssIsActive}) {
        background: ${colors.grayLighter};
      }

      > * {
        color: inherit;
        margin-bottom: 0;
      }
    }
  }
`
export const SidebarInner = styled.div``
