import type { FC } from "react"
import { useEffect } from "react"
import { usePayers } from "../../../hooks/payers"
import { BaseLoader } from "../../Loaders/BaseLoader/BaseLoader"
import { StyledPayers, StyledPayersList } from "./Styled"
import { Payer } from "./Payer"
import { Typography } from "../../Typography/Typography"
import Button from "../../Button/Button"
import { Modal } from "../../Modals/Modal"
import { CreatePayer } from "./Create"

export const Payers: FC = () => {
  const { payers, refetch: refetchPayers, isFetching } = usePayers()

  useEffect(() => {
    if (payers === null) {
      refetchPayers()
    }
  }, [payers, refetchPayers])

  return (
    <>
      <StyledPayers>
        {isFetching && <BaseLoader />}
        <Modal
          variant={"rounded-50"}
          title={"Добавить плательщика"}
          disclosure={
            <Button
              iconPosition={"left"}
              icon={"UserAdd"}
              variant={"link"}
              withOutOffset
            >
              Добавить плательщика
            </Button>
          }
        >
          <CreatePayer
            onSuccess={() => {
              refetchPayers()
            }}
          />
        </Modal>
        {payers !== null && (
          <>
            {payers.length > 0 ? (
              <>
                <StyledPayersList>
                  {payers.map((p) => (
                    <Payer payer={p} key={p.uid} />
                  ))}
                </StyledPayersList>
              </>
            ) : (
              <>
                <Typography variant={"p14"}>Пока нет плательщиков.</Typography>
              </>
            )}
          </>
        )}
      </StyledPayers>
    </>
  )
}
