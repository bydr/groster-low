import { styled } from "@linaria/react"
import {
  StyledEntity,
  StyledEntityContainer,
  StyledEntityList,
} from "../StyledAccount"
import { breakpoints } from "../../../styles/utils/vars"

export const StyledPayers = styled(StyledEntityContainer)``
export const StyledPayersList = styled(StyledEntityList)`
  grid-template-columns: repeat(3, 1fr);

  @media (max-width: ${breakpoints.lg}) {
    grid-template-columns: repeat(2, 1fr);
  }

  @media (max-width: ${breakpoints.sm}) {
    grid-template-columns: 1fr;
  }
`
export const StyledPayer = styled(StyledEntity)``
