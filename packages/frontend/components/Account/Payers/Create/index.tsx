import { FC, useRef, useState } from "react"
import { ShortDataForm } from "./ShortDataForm"
import { FullDataForm } from "./FullDataForm"
import { Payer } from "../../../../../contracts/contracts"

export const CreatePayer: FC<{
  onSuccess?: (uid: string) => void
}> = ({ onSuccess }) => {
  const [mode, setMode] = useState<"short" | "full">("short")
  const payerRef = useRef<Payer | null>(null)

  return (
    <>
      {mode === "short" && (
        <>
          <ShortDataForm
            onSuccess={(response) => {
              if (
                !!response &&
                response.suggestions !== undefined &&
                response.suggestions[0] !== undefined
              ) {
                const data = response.suggestions[0].data
                payerRef.current = {
                  name: data?.name?.short_with_opf || "",
                  inn: data?.inn || "",
                  kpp: data?.kpp || "",
                  ogrnip: data?.ogrn || "",
                  contact_phone: !!data?.phones
                    ? !!data.phones[0]?.data?.number &&
                      !!data.phones[0]?.data?.city_code
                      ? `${data.phones[0].data.city_code}${data.phones[0].data.number}`
                      : ""
                    : "",
                  account: data?.management?.name || "",
                }
              }
              setMode("full")
            }}
          />
        </>
      )}

      {mode === "full" && (
        <>
          <FullDataForm
            onSuccess={(uid) => {
              if (onSuccess) {
                onSuccess(uid)
              }
              setMode("short")
            }}
            onBack={() => {
              setMode("short")
            }}
            initialPayerCreate={payerRef.current || undefined}
          />
        </>
      )}
    </>
  )
}
