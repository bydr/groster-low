import { FC, useContext, useEffect, useState } from "react"
import { Payer } from "../../../../../contracts/src/api/account"
import { FormGroup, StyledForm } from "../../../Forms/StyledForms"
import { Controller, useForm } from "react-hook-form"
import { Field } from "../../../Field/Field"
import { Button } from "../../../Button"
import {
  getPureValue,
  MAXLENGTH_OGRNIP,
  MINLENGTH_OGRNIP,
} from "../../../../validations/payerFields"
import { useMutation } from "react-query"
import { fetchPayerCreate, fetchPayerEdit } from "../../../../api/accountAPI"
import { ModalContext } from "../../../Modals/Modal"
import { PhoneField } from "../../../Field/PhoneField"
import { SingleError } from "../../../Typography/Typography"
import { ApiError } from "../../../../../contracts/contracts"
import { RULE_VALIDATE } from "../../../../validations/phone"
import NumberFormat from "react-number-format"

type PayersFormFieldsType = Payer

export const FullDataForm: FC<{
  payer?: Payer
  initialPayerCreate?: Payer
  onSuccess?: (uid: string) => void
  onBack?: () => void
}> = ({ payer, onSuccess, onBack, initialPayerCreate }) => {
  const [isFetching, setIsFetching] = useState<boolean>(false)
  const [error, setError] = useState<string | null>(null)
  const [viewMode, setViewMode] = useState<"edit" | "create">(
    payer !== undefined ? "edit" : "create",
  )

  const {
    handleSubmit,
    control,
    reset,
    formState: { errors, isDirty },
    setValue,
  } = useForm<PayersFormFieldsType>({
    defaultValues: {
      name: payer?.name || "",
      ogrnip: payer?.ogrnip || "",
      bik: payer?.bik || "",
      inn: payer?.inn || "",
      account: payer?.account || "",
      contact: payer?.contact || "",
      kpp: payer?.kpp || "",
      contact_phone: payer?.contact_phone || "",
    },
    mode: "onChange",
  })
  const modalContext = useContext(ModalContext)

  useEffect(() => {
    if (initialPayerCreate !== undefined) {
      setValue("name", initialPayerCreate?.name || "")
      setValue("ogrnip", initialPayerCreate?.ogrnip || "")
      setValue("bik", initialPayerCreate?.bik || "")
      setValue("inn", initialPayerCreate?.inn || "")
      setValue("account", initialPayerCreate?.account || "")
      setValue("contact", initialPayerCreate?.contact || "")
      setValue("kpp", initialPayerCreate?.kpp || "")
      setValue("contact_phone", initialPayerCreate?.contact_phone || "")
    }
  }, [initialPayerCreate, setValue])

  useEffect(() => {
    setViewMode(payer !== undefined ? "edit" : "create")
    if (payer !== undefined) {
      reset({
        name: payer?.name || "",
        ogrnip: payer?.ogrnip || "",
        bik: payer?.bik || "",
        inn: payer?.inn || "",
        account: payer?.account || "",
        contact: payer?.contact || "",
        kpp: payer?.kpp || "",
        contact_phone: payer?.contact_phone || "",
      })
    }
  }, [payer, reset])

  const { mutate: payerCreateMutate } = useMutation(fetchPayerCreate, {
    onMutate: () => {
      setIsFetching(true)
      setError(null)
    },
    onSuccess: (response) => {
      reset()
      setIsFetching(false)
      if (onSuccess) {
        onSuccess(response.uid)
      }
      modalContext?.hide()
    },
    onError: (error: ApiError) => {
      setError(error.message)
      setIsFetching(false)
    },
  })

  const { mutate: payerEditMutate } = useMutation(fetchPayerEdit, {
    onMutate: () => {
      setIsFetching(true)
      setError(null)
    },
    onSuccess: (response) => {
      setIsFetching(false)
      if (onSuccess) {
        onSuccess(response.uid)
      }
      modalContext?.hide()
    },
    onError: (error: ApiError) => {
      setError(error.message)
      setIsFetching(false)
    },
  })

  const onSubmit = handleSubmit((data) => {
    // console.log("data payers ", data)
    if (viewMode === "edit") {
      if (payer?.uid !== undefined) {
        payerEditMutate({
          name: data.name,
          ogrnip: data.ogrnip,
          contact: data.contact,
          kpp: data.kpp,
          inn: data.inn,
          account: data.account,
          bik: data.bik,
          contact_phone: data.contact_phone,
          uid: payer?.uid || "",
        })
      }
    }

    if (viewMode === "create") {
      payerCreateMutate({
        name: data.name,
        ogrnip: data.ogrnip,
        contact: data.contact,
        kpp: data.kpp,
        inn: data.inn,
        account: data.account,
        bik: data.bik,
        contact_phone: data.contact_phone,
      })
    }
  })

  return (
    <>
      <StyledForm onSubmit={onSubmit}>
        {onBack !== undefined && (
          <>
            <Button
              type={"button"}
              variant={"small"}
              icon={"ArrowLeft"}
              isHiddenBg
              onClick={(e) => {
                e.preventDefault()
                onBack()
              }}
            >
              Назад
            </Button>
          </>
        )}
        <FormGroup>
          <Controller
            name={"name"}
            control={control}
            render={({ field, fieldState }) => (
              <Field
                type={"text"}
                value={field.value}
                placeholder={"Название организации"}
                onChange={field.onChange}
                errorMessage={fieldState.error?.message}
                withAnimatingLabel
              />
            )}
          />
        </FormGroup>
        <FormGroup>
          <Controller
            name={"ogrnip"}
            control={control}
            rules={{
              maxLength: MAXLENGTH_OGRNIP,
              minLength: MINLENGTH_OGRNIP,
            }}
            render={({ field, fieldState }) => (
              <NumberFormat
                withAnimatingLabel
                placeholder={"ОГРНИП"}
                format={"###############"}
                mask="_"
                allowEmptyFormatting={true}
                customInput={Field}
                type={"text"}
                value={field.value}
                onValueChange={({ value }) => {
                  field.onChange(value)
                }}
                errorMessage={fieldState.error?.message}
              />
            )}
          />

          <Controller
            name={"inn"}
            control={control}
            rules={{
              required: {
                value: true,
                message: "Поле обязательно для заполнения",
              },
              pattern: {
                value: /^(0|[1-9]\d*)(\.\d+)?$/,
                message: "Неверное значение",
              },
            }}
            render={({ field, fieldState }) => (
              <NumberFormat
                withAnimatingLabel
                placeholder={"ИНН"}
                format={"############"}
                mask="_"
                allowEmptyFormatting={true}
                customInput={Field}
                type={"text"}
                value={field.value}
                onValueChange={({ value }) => {
                  field.onChange(value)
                }}
                errorMessage={fieldState.error?.message}
              />
            )}
          />
        </FormGroup>
        <FormGroup>
          <Controller
            name={"account"}
            control={control}
            render={({ field, fieldState }) => (
              <Field
                type={"text"}
                value={field.value}
                placeholder={"Номер счета"}
                onChange={(e) => {
                  e.target.value = getPureValue(e.target.value)
                  field.onChange(e)
                }}
                errorMessage={fieldState.error?.message}
                withAnimatingLabel
              />
            )}
          />
          <Controller
            name={"bik"}
            control={control}
            render={({ field, fieldState }) => (
              <Field
                type={"text"}
                value={field.value}
                placeholder={"БИК"}
                onChange={(e) => {
                  e.target.value = getPureValue(e.target.value)
                  field.onChange(e)
                }}
                errorMessage={fieldState.error?.message}
                withAnimatingLabel
              />
            )}
          />
        </FormGroup>
        <FormGroup>
          <Controller
            name={"kpp"}
            control={control}
            render={({ field, fieldState }) => (
              <Field
                type={"text"}
                value={field.value}
                placeholder={"КПП"}
                onChange={(e) => {
                  e.target.value = getPureValue(e.target.value)
                  field.onChange(e)
                }}
                errorMessage={fieldState.error?.message}
                withAnimatingLabel
              />
            )}
          />
          <Controller
            rules={RULE_VALIDATE}
            render={({ field, fieldState }) => (
              <PhoneField
                value={field.value}
                onValueChange={({ value }) => {
                  field.onChange(value)
                }}
                errorMessage={fieldState?.error?.message}
              />
            )}
            control={control}
            name={"contact_phone"}
          />
        </FormGroup>
        <FormGroup>
          <Controller
            name={"contact"}
            control={control}
            render={({ field, fieldState }) => (
              <Field
                type={"text"}
                value={field.value}
                placeholder={"Контактное лицо"}
                onChange={field.onChange}
                errorMessage={fieldState.error?.message}
                withAnimatingLabel
              />
            )}
          />
        </FormGroup>

        {error !== null && <SingleError>{error}</SingleError>}

        <Button
          type={"button"}
          variant={"filled"}
          size={"large"}
          disabled={Object.keys(errors).length > 0 || !isDirty}
          isFetching={isFetching}
          onClick={() => {
            if (!(Object.keys(errors).length > 0 || !isDirty)) {
              void onSubmit()
            }
          }}
        >
          {viewMode === "create" ? "Добавить" : "Сохранить"}
        </Button>
      </StyledForm>
    </>
  )
}
