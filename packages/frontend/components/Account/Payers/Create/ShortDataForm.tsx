import { FC, useState } from "react"
import { Controller, useForm } from "react-hook-form"
import { FormGroup, StyledForm } from "../../../Forms/StyledForms"
import { Field } from "../../../Field/Field"
import { SingleError, Typography } from "../../../Typography/Typography"
import { useMutation } from "react-query"
import {
  fetchSuggestParty,
  SuggestPartyReturnType,
} from "../../../../api/dadataAPI"
import { ApiError } from "../../../../../contracts/contracts"
import { Button } from "../../../Button"

type ShortDataFormFieldType = {
  inn: string
}

export const ShortDataForm: FC<{
  onSuccess?: (response: SuggestPartyReturnType) => void
}> = ({ onSuccess }) => {
  const [error, setError] = useState<string | null>(null)

  const {
    handleSubmit,
    control,
    reset,
    formState: { errors, isDirty, isSubmitting },
  } = useForm<ShortDataFormFieldType>({
    defaultValues: {
      inn: "",
    },
    mode: "onChange",
  })

  const { mutate: suggestPartyMutate, isLoading } = useMutation(
    fetchSuggestParty,
    {
      onMutate: () => {
        setError(null)
      },
      onSuccess: (response) => {
        reset()
        if (onSuccess) {
          onSuccess(response)
        }
      },
      onError: (error: ApiError) => {
        setError(error.message)
      },
    },
  )

  const onSubmit = handleSubmit((data) => {
    suggestPartyMutate({
      inn: data.inn,
    })
  })

  return (
    <>
      <StyledForm onSubmit={onSubmit}>
        <Typography variant={"p14"}>
          Укажите ИНН. Остальные данные компании или ИП мы подгрузим сами и Вы
          сможете их отредактировать.
        </Typography>
        <FormGroup>
          <Controller
            name={"inn"}
            control={control}
            rules={{
              required: {
                value: true,
                message: "Поле обязательно для заполнения",
              },
              pattern: {
                value: /^(0|[1-9]\d*)(\.\d+)?$/,
                message: "Неверное значение",
              },
            }}
            render={({ field, fieldState }) => (
              <Field
                type={"text"}
                value={field.value}
                placeholder={"ИНН"}
                onChange={field.onChange}
                errorMessage={fieldState.error?.message}
                withAnimatingLabel
              />
            )}
          />
        </FormGroup>

        {error !== null && <SingleError>{error}</SingleError>}

        <Button
          type={"button"}
          variant={"filled"}
          size={"large"}
          disabled={Object.keys(errors).length > 0 || !isDirty}
          isFetching={isLoading || isSubmitting}
          onClick={() => {
            if (!(Object.keys(errors).length > 0 || !isDirty)) {
              void onSubmit()
            }
          }}
        >
          Продолжить
        </Button>
      </StyledForm>
    </>
  )
}
