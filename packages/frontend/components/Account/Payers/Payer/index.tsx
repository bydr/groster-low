import type { FC } from "react"
import { useEffect, useState } from "react"
import { Typography } from "../../../Typography/Typography"
import { StyledPayer } from "../Styled"
import { Panel, PanelHeading } from "../../../../styles/utils/StyledPanel"
import { PayerListItemType, PropertiesType } from "../../../../types/types"
import {
  cssName,
  StyledTable,
  TableCell,
  TableRow,
} from "../../../Products/Detail/StyledDetail"
import { Button } from "../../../Button"
import { Modal } from "../../../Modals/Modal"
import { FullDataForm } from "../Create/FullDataForm"
import { usePayers } from "../../../../hooks/payers"

export const Payer: FC<{ payer: PayerListItemType }> = ({ payer }) => {
  const [items, setItems] = useState<PropertiesType>([])
  const { refetch: refetchPayers } = usePayers()

  useEffect(() => {
    setItems([
      {
        name: "ОГРНИП",
        value: payer.ogrnip || "",
      },
      {
        name: "ИНН",
        value: payer.inn,
      },
      {
        name: "Номер счета",
        value: payer.account || "",
      },
      {
        name: "БИК",
        value: payer.bik || "",
      },
      {
        name: "КПП",
        value: payer.kpp || "",
      },
      {
        name: "Контактное лицо",
        value: payer.contact || "",
      },
      {
        name: "Телефон",
        value: payer.contact_phone || "",
      },
    ])
  }, [payer])

  return (
    <>
      <StyledPayer>
        <Panel>
          <PanelHeading>
            <Typography variant={"span"}>{payer.name}</Typography>
            <Modal
              variant={"rounded-50"}
              title={"Редактировать плательщика"}
              disclosure={<Button variant={"box"} icon={"Edit"} />}
            >
              <FullDataForm
                payer={payer}
                onSuccess={() => {
                  refetchPayers()
                }}
              />
            </Modal>
          </PanelHeading>
          <StyledTable>
            {items.map((prop, index) => (
              <>
                <TableRow key={index}>
                  <TableCell className={cssName}>{prop.name}</TableCell>
                  <TableCell>{prop.value}</TableCell>
                </TableRow>
              </>
            ))}
          </StyledTable>
        </Panel>
      </StyledPayer>
    </>
  )
}
