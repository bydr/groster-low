import { styled } from "@linaria/react"
import { StyledForm } from "../../Forms/StyledForms"
import { ButtonBase } from "../../Button/StyledButton"
import { breakpoints } from "../../../styles/utils/vars"
import { TypographyBase } from "../../Typography/Typography"

export const StyledProfile = styled.div`
  width: 100%;
  max-width: 783px;
  display: flex;
  flex-direction: column;
  align-items: flex-start;

  ${StyledForm} {
    margin-bottom: 20px;
  }

  > ${ButtonBase} {
    margin-bottom: 16px;
  }

  @media (max-width: ${breakpoints.sm}) {
    ${StyledForm} {
      margin-bottom: 20px;
    }
  }
`
export const StyledProfileShort = styled.div`
  width: 100%;
  grid-auto-flow: column;
  gap: 8px;
  display: none;

  > ${TypographyBase} {
    &:last-child {
      text-align: right;
    }
  }

  @media (max-width: ${breakpoints.lg}) {
    display: grid;
  }
`
