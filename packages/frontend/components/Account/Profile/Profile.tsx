import { FC } from "react"
import { ProfileForm } from "./ProfileForm"
import { StyledProfile } from "./Styled"
import { UpdatePassword } from "./UpdatePassword"
import { Button } from "../../Button"
import { useAuth } from "hooks/auth"

export const Profile: FC = () => {
  const { user } = useAuth()
  return (
    <>
      <StyledProfile>
        {user?.isAdmin && (
          <>
            <Button
              variant={"link"}
              as={"a"}
              icon={"InBrowser"}
              href={"/dashboard"}
            >
              Панель администратора
            </Button>
          </>
        )}
        <ProfileForm />
        <UpdatePassword />
      </StyledProfile>
    </>
  )
}
