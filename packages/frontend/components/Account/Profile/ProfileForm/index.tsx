import type { FC } from "react"
import { useState } from "react"
import { Controller, useForm, useFormState } from "react-hook-form"
import { FormGroup, StyledForm } from "../../../Forms/StyledForms"
import { Field } from "../../../Field/Field"
import { PhoneField } from "../../../Field/PhoneField"
import { EMAIL_PATTERN } from "../../../../validations/email"
import { SingleError } from "../../../Typography/Typography"
import { useAuth } from "../../../../hooks/auth"
import { Button } from "../../../Button"
import { useMutation } from "react-query"
import { fetchUpdateFio } from "../../../../api/accountAPI"
import { ApiError } from "next/dist/server/api-utils"
import { RULES_PHONE_VALIDATE } from "../../../../validations/phone"

export const ProfileForm: FC = () => {
  const { user, updateUser } = useAuth()
  const [error, setError] = useState<string | null>(null)
  const {
    handleSubmit,
    control,
    register,
    formState: { isDirty },
    reset,
  } = useForm<{ fio: string; phone: string; email: string }>({
    defaultValues: {
      fio: user?.fio || "",
      email: user?.email || "",
      phone: user?.phone || "",
    },
    mode: "onChange",
  })

  const { errors } = useFormState({
    control,
  })

  const { mutate: updateFioMutate, isLoading } = useMutation(fetchUpdateFio, {
    onSuccess: (_, request) => {
      if (user === null) {
        return
      }
      updateUser({ ...user, fio: request.fio || null })
      reset({
        email: user.email || "",
        phone: user.phone || "",
        fio: request.fio || "",
      })
    },
    onError: (err: ApiError) => {
      setError(err.message || "Произошла ошибка")
    },
    onMutate: () => {
      setError(null)
    },
  })

  const onSubmit = handleSubmit((data) => {
    if (data.fio) {
      updateFioMutate({
        fio: data.fio,
      })
    }
  })

  return (
    <>
      <StyledForm onSubmit={onSubmit}>
        <FormGroup>
          <Controller
            name={"fio"}
            control={control}
            rules={{
              required: {
                value: true,
                message: "Поле обязательно для заполнения",
              },
            }}
            render={({ field, fieldState }) => (
              <Field
                type={"text"}
                value={field.value}
                placeholder={"ФИО"}
                onChange={field.onChange}
                errorMessage={fieldState.error?.message}
                withAnimatingLabel
              />
            )}
          />
        </FormGroup>
        {!!user?.email && (
          <>
            <FormGroup>
              <Field
                placeholder={"Email"}
                withAnimatingLabel
                errorMessage={errors?.email?.message}
                disabled={true}
                {...register("email", {
                  required: {
                    value: true,
                    message: "Поле обязательно для заполнения",
                  },
                  pattern: {
                    value: EMAIL_PATTERN,
                    message: "Неверный формат email",
                  },
                })}
              />
            </FormGroup>
          </>
        )}
        {!!user?.phone && (
          <>
            <FormGroup>
              <Controller
                rules={RULES_PHONE_VALIDATE}
                render={({ field, fieldState }) => (
                  <PhoneField
                    value={field.value}
                    onValueChange={({ value }) => {
                      field.onChange(value)
                    }}
                    errorMessage={fieldState?.error?.message}
                    disabled={true}
                  />
                )}
                control={control}
                name={"phone"}
              />
            </FormGroup>
          </>
        )}

        {error !== null && <SingleError>{error}</SingleError>}

        <Button
          type={"submit"}
          variant={"filled"}
          disabled={Object.keys(errors).length > 0 || !isDirty}
          isFetching={isLoading}
        >
          Сохранить
        </Button>
      </StyledForm>
    </>
  )
}
