import { FC, useState } from "react"
import Button from "../../../Button/Button"
import { UpdatePasswordForm } from "./UpdatePasswordForm"
import { useAuth } from "../../../../hooks/auth"

export const UpdatePassword: FC = () => {
  const { user } = useAuth()
  const [isUpdatePswrdMode, setIsUpdatePswrdMode] = useState<boolean>(false)

  return (
    <>
      {!!user?.email && (
        <>
          <Button
            variant={"link"}
            as={"a"}
            isHiddenBg
            onClick={(e) => {
              e.preventDefault()
              setIsUpdatePswrdMode(!isUpdatePswrdMode)
            }}
          >
            Сменить пароль
          </Button>
          {isUpdatePswrdMode && (
            <>
              <UpdatePasswordForm />
            </>
          )}
        </>
      )}
    </>
  )
}
