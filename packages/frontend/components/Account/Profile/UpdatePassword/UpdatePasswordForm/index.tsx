import type { FC } from "react"
import { useState } from "react"
import { useMutation } from "react-query"
import { fetchUpdatePassword } from "../../../../../api/accountAPI"
import { FormGroup, StyledForm } from "../../../../Forms/StyledForms"
import { useForm, UseFormRegister } from "react-hook-form"
import { PasswordField } from "../../../../Field/PasswordField"
import { Button } from "../../../../Button"
import { SuccessOverlay } from "../../../../Forms/SuccessOverlay/SuccessOverlay"
import { SingleError } from "../../../../Typography/Typography"
import { TIMEOUT_SUCCESS } from "../../../../../utils/constants"
import { ApiError } from "../../../../../../contracts/contracts"

type UpdatePasswordFormFieldsType = {
  oldPassword: string
  newPassword: string
  passwordConfirm: string
}

export const UpdatePasswordForm: FC = () => {
  const [error, setError] = useState<string | null>(null)

  const {
    handleSubmit,
    register,
    formState: { errors, isDirty },
    reset: resetForm,
    getValues,
  } = useForm<UpdatePasswordFormFieldsType>({
    defaultValues: {
      oldPassword: "",
      newPassword: "",
      passwordConfirm: "",
    },
    mode: "onChange",
  })

  const {
    mutate: updatePasswordMutate,
    isSuccess,
    reset: resetMutation,
    isLoading,
  } = useMutation(fetchUpdatePassword, {
    onSuccess: () => {
      resetForm()

      setTimeout(() => {
        resetMutation()
      }, TIMEOUT_SUCCESS)
    },
    onError: (err: ApiError) => {
      setError(err.message)
    },
  })

  const onSubmit = handleSubmit((data) => {
    updatePasswordMutate({
      new_password: data.newPassword,
      old_password: data.oldPassword,
    })
  })

  return (
    <>
      <StyledForm onSubmit={onSubmit}>
        <SuccessOverlay
          isSuccess={isSuccess}
          message={"Пароль успешно изменен"}
        />
        <FormGroup>
          <PasswordField
            register={
              register as unknown as UseFormRegister<Record<string, string>>
            }
            errorMessage={errors?.oldPassword?.message}
            placeholder={"Старый пароль"}
            name={"oldPassword"}
          />
        </FormGroup>
        <FormGroup>
          <PasswordField
            register={
              register as unknown as UseFormRegister<Record<string, string>>
            }
            errorMessage={errors?.newPassword?.message}
            placeholder={"Новый пароль"}
            name={"newPassword"}
          />
        </FormGroup>
        <FormGroup>
          <PasswordField
            register={
              register as unknown as UseFormRegister<Record<string, string>>
            }
            errorMessage={errors?.passwordConfirm?.message}
            placeholder={"Повторите пароль"}
            name={"passwordConfirm"}
            isConfirm
            comparisonVal={getValues("newPassword")}
          />
        </FormGroup>

        {error !== null && <SingleError>{error}</SingleError>}

        <Button
          type={"submit"}
          variant={"filled"}
          disabled={Object.keys(errors).length > 0 || !isDirty}
          isFetching={isLoading}
        >
          Сменить пароль
        </Button>
      </StyledForm>
    </>
  )
}
