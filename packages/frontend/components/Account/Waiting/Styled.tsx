import { styled } from "@linaria/react"
import { StyledProducts } from "../../Products/StyledProducts"
import { breakpoints } from "../../../styles/utils/vars"

export const StyledWaitingContainer = styled.div`
  width: 100%;
  position: relative;

  ${StyledProducts} {
    grid-template-columns: repeat(4, 1fr);

    @media (max-width: ${breakpoints.xxl}) {
      grid-template-columns: repeat(3, 1fr);
    }

    @media (max-width: ${breakpoints.xl}) {
      grid-template-columns: repeat(2, 1fr);
    }

    @media (max-width: ${breakpoints.sm}) {
      grid-template-columns: 1fr;
    }
  }
`
