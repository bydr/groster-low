import { FC } from "react"
import { VIEW_PRODUCTS_GRID } from "../../../types/types"
import { Products } from "../../Products/Products"
import { fetchProductsWaiting, productsAPI } from "../../../api/productsAPI"
import { ProductWaiting } from "../../Products/Waiting/ProductWaiting"
import { StyledWaitingContainer } from "./Styled"
import { BaseLoader } from "../../Loaders/BaseLoader/BaseLoader"
import { useQuery } from "react-query"
import { Typography } from "../../Typography/Typography"

export const Waiting: FC = () => {
  const { data: productsIds } = useQuery(["waitingProductsKeys"], () =>
    fetchProductsWaiting(),
  )
  const { isFetching, data: dataProducts } = productsAPI.useProductsFetch(
    !!productsIds && productsIds.length > 0 ? productsIds.join(",") : null,
  )

  return (
    <>
      <StyledWaitingContainer>
        {isFetching && <BaseLoader />}
        <Products view={VIEW_PRODUCTS_GRID}>
          {!!dataProducts && dataProducts.length > 0 ? (
            <>
              {dataProducts.map((p) => (
                <ProductWaiting
                  product={p}
                  view={VIEW_PRODUCTS_GRID}
                  key={p.uuid || ""}
                />
              ))}
            </>
          ) : (
            <>
              {!isFetching && (
                <>
                  <Typography variant={"p14"}>
                    Здесь будут товары, о которых мы Вас оповестим как только
                    они появятся в наличии
                  </Typography>
                </>
              )}
            </>
          )}
        </Products>
      </StyledWaitingContainer>
    </>
  )
}
