import { styled } from "@linaria/react"
import {
  ContentContainer,
  RowContentSidebar,
} from "../../styles/utils/StyledGrid"
import { ButtonBase } from "../Button/StyledButton"
import { Panel, PanelHeading } from "../../styles/utils/StyledPanel"
import { getTypographyBase, TypographyBase } from "../Typography/Typography"
import { breakpoints, colors } from "../../styles/utils/vars"
import { StyledSidebar } from "../Sidebar/StyledSidebar"
import { FormGroup } from "../Forms/StyledForms"
import { StyledNav } from "../Navigation/StyledNavigation"
import { StyledFavorites } from "../Favorites/Styled"
import { StyledProducts } from "../Products/StyledProducts"

export const AccountRowContentSidebar = styled(RowContentSidebar)`
  grid-template-areas: "sidebar content";
  grid-template-columns: 18% calc(82% - 56px);
  gap: 0 56px;

  ${StyledSidebar} {
    height: 100%;

    ${StyledNav} {
      ~ ${FormGroup} {
        display: none;
      }
    }
  }

  ${StyledFavorites} {
    ${StyledProducts} {
      grid-template-columns: repeat(4, 1fr);

      @media (max-width: ${breakpoints.xl}) {
        grid-template-columns: repeat(3, 1fr);
      }

      @media (max-width: ${breakpoints.lg}) {
        grid-template-columns: repeat(2, 1fr);
      }

      @media (max-width: ${breakpoints.sm}) {
        grid-template-columns: 1fr;
      }
    }
  }

  @media (max-width: ${breakpoints.lg}) {
    grid-template-columns: repeat(auto-fit, minmax(300px, 1fr));
    grid-template-areas: "sidebar" "content";

    ${StyledSidebar} {
      box-shadow: none;
      background-color: transparent;
      border-radius: 0;
      padding: 16px 0 0 0;

      ${StyledNav} {
        display: none;

        ~ ${FormGroup} {
          display: flex;
        }
      }
    }
  }
`

export const StyledEntityList = styled.div`
  position: relative;
  display: grid;
  grid-template-columns: repeat(4, 1fr);
  gap: 24px;
  width: 100%;

  @media (max-width: ${breakpoints.xl}) {
    grid-template-columns: repeat(3, 1fr);
  }

  @media (max-width: ${breakpoints.lg}) {
    grid-template-columns: repeat(2, 1fr);
  }

  @media (max-width: ${breakpoints.sm}) {
    grid-template-columns: 1fr;
  }
`

export const StyledEntityContainer = styled.div`
  width: 100%;
  position: relative;

  > ${ButtonBase} {
    margin-bottom: 30px;
  }
`

export const StyledEntity = styled.div`
  ${Panel} {
    padding: 20px 24px;
    margin: 0;

    ${PanelHeading} {
      ${getTypographyBase("p14")};
      font-weight: 600;
      word-break: break-all;
      margin-bottom: 14px;
    }

    ${TypographyBase} {
      word-break: break-all;
    }
  }
`

export const AccountPage = styled.section`
  position: relative;
  width: 100%;
  background: ${colors.grayLight};

  &:before {
    content: "";
    position: absolute;
    top: 0;
    bottom: 0;
    height: 100%;
    right: 0;
    left: calc(50% - 700px);
    background: ${colors.white};
  }

  ${ContentContainer} {
    background: ${colors.white};
  }

  > * {
    position: relative;
    z-index: 2;
  }

  @media (max-width: ${breakpoints.xxl}) {
    background: ${colors.white};
    &:before {
      display: none;
    }
  }
`
