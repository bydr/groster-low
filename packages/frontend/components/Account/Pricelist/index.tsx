import { FC, HTMLAttributes, useCallback, useState } from "react"
import { SingleError, Typography } from "../../Typography/Typography"
import { useAppDispatch, useAppSelector } from "../../../hooks/redux"
import { CustomCheckbox } from "../../Checkbox/CustomCheckbox"
import { Button } from "../../Button"
import { StyledListCategories } from "./Styled"
import { Recall } from "../../Forms/Recall"
import { Modal } from "../../Modals/Modal"
import { ICategoryTreeItem } from "../../../types/types"
import { StyledCategory } from "../../Catalog/Categories/Category/StyledCategory"
import { StyledList } from "../../List/StyledList"
import { accountSlice } from "../../../store/reducers/accountSlice"
import { useMutation } from "react-query"
import { fetchPriceList } from "../../../api/catalogAPI"
import { ApiError } from "next/dist/server/api-utils"
import { DownloadLink } from "../History/Orders/Order/Documents/DownloadLink/DownloadLink"
import { Field } from "../../Field/Field"
import { FormGroup } from "../../Forms/StyledForms"
import { colors } from "../../../styles/utils/vars"
import { useGRecaptcha } from "../../../hooks/gReCaptcha"

const getCategoryUuidsChild = (category: ICategoryTreeItem): string[] => {
  const uuids: string[] = []

  const _process = (category: ICategoryTreeItem) => {
    if (Object.keys(category.children || {}).length <= 0) {
      return
    }
    for (const key of Object.keys(category.children)) {
      const id = category.children[key]?.uuid
      if (id !== undefined) {
        uuids.push(id)
        _process(category.children[key])
      }
    }
  }

  _process(category)

  return uuids
}

const CategoryItem: FC<
  { category: ICategoryTreeItem } & HTMLAttributes<HTMLElement> & {
      selected: string[]
      setSelected: (val: string[]) => void
    }
> = ({ category, selected, setSelected }) => {
  const childrenEntries = Object.entries(category.children)
  const isChildrenContains = childrenEntries.length > 0

  return (
    <>
      {!category ? (
        <></>
      ) : (
        <>
          {category.product_qty !== undefined && (
            <>
              <StyledCategory>
                <CustomCheckbox
                  variant={"check"}
                  name={category.uuid || ""}
                  message={category.name || ""}
                  key={category.uuid}
                  stateCheckbox={selected.includes(category.uuid || "")}
                  onChange={(e) => {
                    if (e.currentTarget.checked) {
                      let uuids: string[]
                      if (category.parent === 0) {
                        uuids = [
                          ...selected,
                          ...getCategoryUuidsChild(category),
                          category.uuid || "",
                        ]
                      } else {
                        uuids = [...selected, category.uuid || ""]
                      }
                      setSelected(uuids)
                    } else {
                      let uuids: string[]
                      if (category.parent === 0) {
                        const all = [
                          ...getCategoryUuidsChild(category),
                          category.uuid || "",
                        ]
                        uuids = selected.filter((k) => !all.includes(k))
                      } else {
                        uuids = selected.filter((k) => k !== category.uuid)
                      }
                      setSelected(uuids)
                    }
                  }}
                />
                {isChildrenContains && (
                  <StyledList as={"div"}>
                    {childrenEntries.map(([key, cat]) => {
                      return (
                        <CategoryItem
                          key={key}
                          category={cat}
                          selected={selected}
                          setSelected={setSelected}
                        />
                      )
                    })}
                  </StyledList>
                )}
              </StyledCategory>
            </>
          )}
        </>
      )}
    </>
  )
}

export const Pricelist: FC = () => {
  const [error, setError] = useState<string | undefined>()
  const [linkPriceList, setLinkPriceList] = useState<string | undefined>()
  const dispatch = useAppDispatch()
  const categoriesTree = useAppSelector(
    (state) => state.catalog.categories?.tree,
  )
  const { selected } = useAppSelector((state) => state.profile.priceList)
  const { setCategoriesPriceListSelected } = accountSlice.actions
  const [email, setEmail] = useState<string>("")
  const [successMsg, setSuccessMsg] = useState<string | null>(null)
  const [fetchingMsg, setFetchingMsg] = useState<string | null>(null)

  const {
    mutate: priceListMutate,
    isLoading,
    isSuccess,
  } = useMutation(fetchPriceList, {
    retry: false,
    onSuccess: (response, request) => {
      // console.log("response ", response)
      setEmail("")
      dispatch(setCategoriesPriceListSelected([]))
      setError(undefined)
      setLinkPriceList(
        (response?.link || "").length > 0 ? response.link : undefined,
      )
      setSuccessMsg(
        (request.email || "").length > 0
          ? `В ближайшее время прайс-лист будет сформирован и отправлен на почту <b>${request.email}</b>`
          : null,
      )
      setFetchingMsg(null)
    },
    onError: (error: ApiError) => {
      setSuccessMsg(null)
      setError(error.message || "Произошла ошибка")
      setFetchingMsg(null)
    },
    onMutate: (request) => {
      setSuccessMsg(null)
      setError(undefined)
      setLinkPriceList(undefined)

      if (!!request?.email) {
        setFetchingMsg(`<b>Можете уйти с этой страницы</b>`)
      } else {
        setFetchingMsg(null)
      }
    },
  })

  const { run, isLoading: isLoadingGRecaptcha } = useGRecaptcha({
    cbSuccess: () => {
      priceListMutate({
        categories: selected,
        email: email.length > 0 ? email : undefined,
      })
    },
    cbError: (error) => {
      setError(error.message || "Произошла ошибка")
    },
    cbMutate: () => {
      setError(undefined)
    },
  })

  const download = useCallback(() => {
    if (selected.length <= 0 || isLoading || isLoadingGRecaptcha) {
      return
    }
    run()
  }, [selected.length, isLoading, isLoadingGRecaptcha, run])

  return (
    <>
      <Modal
        disclosure={
          <Button variant={"small"} icon={"Book"}>
            Заказать презентацию
          </Button>
        }
        title={"Заказать презентацию"}
      >
        <Recall />
      </Modal>
      {!!categoriesTree && (
        <>
          <br />
          <Typography variant={"p14"}>
            Выберите категории товаров по которым вам нужен прайс-лист:
          </Typography>
          <StyledListCategories>
            {Object.entries(categoriesTree).map(([key, category]) => (
              <CategoryItem
                category={category}
                key={key}
                selected={selected}
                setSelected={(value) => {
                  dispatch(setCategoriesPriceListSelected(value))
                }}
              />
            ))}
          </StyledListCategories>
          <Typography variant={"h5"}>
            Введите email и мы отправим прайс-лист, когда он будет сформирован.
          </Typography>

          <FormGroup>
            <Field
              variant={"input"}
              placeholder={"Введите email"}
              name={"email-pricelist"}
              type={"email"}
              value={email}
              onChange={(e) => {
                if (isLoading || isLoadingGRecaptcha) {
                  return
                }
                setEmail(e.target.value || "")
              }}
              disabled={isLoading}
            />
          </FormGroup>

          <Button
            variant={"filled"}
            size={"large"}
            disabled={selected.length <= 0 || isLoading || isLoadingGRecaptcha}
            onClick={download}
            isFetching={isLoadingGRecaptcha || isLoading}
          >
            Скачать
          </Button>

          <DownloadLink
            link={linkPriceList}
            isLoading={isLoading || isLoadingGRecaptcha}
            loaderSize={"largeM"}
            loaderTextSize={"p14"}
          />

          {(isLoading || isLoadingGRecaptcha) && fetchingMsg !== null && (
            <>
              <Typography
                variant={"default"}
                color={colors.brand.purple}
                dangerouslySetInnerHTML={{
                  __html: fetchingMsg,
                }}
              />
            </>
          )}

          {successMsg !== null && isSuccess && (
            <>
              <br />
              <Typography
                variant={"default"}
                color={colors.brand.purple}
                dangerouslySetInnerHTML={{
                  __html: successMsg,
                }}
              />
            </>
          )}

          {error && <SingleError>{error}</SingleError>}
        </>
      )}
    </>
  )
}
