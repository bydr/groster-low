import { styled } from "@linaria/react"
import { breakpoints } from "../../../styles/utils/vars"

export const StyledListCategories = styled.div`
  position: relative;
  width: 100%;
  margin: 0 0 32px 0;
  display: grid;
  grid-template-columns: repeat(3, 1fr);

  @media (max-width: ${breakpoints.xxl}) {
    grid-template-columns: repeat(2, 1fr);
  }
  @media (max-width: ${breakpoints.sm}) {
    grid-template-columns: 1fr;
  }
`
