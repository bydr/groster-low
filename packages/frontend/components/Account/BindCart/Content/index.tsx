import { FC, useCallback, useContext, useMemo } from "react"
import { useAuth } from "../../../../hooks/auth"
import { useCart } from "../../../../hooks/cart"
import { Typography } from "../../../Typography/Typography"
import { VIEW_PRODUCTS_LIST } from "../../../../types/types"
import { Product } from "../../../Products/Catalog/Product/Product"
import { Products } from "../../../Products/Products"
import { BaseLoader } from "../../../Loaders/BaseLoader/BaseLoader"
import { ButtonGroup } from "../../../Button/StyledButton"
import { Button } from "../../../Button"
import { SuccessOverlay } from "../../../Forms/SuccessOverlay/SuccessOverlay"
import { ModalContext } from "../../../Modals/Modal"

const WELCOME_TEXT =
  "У Вас есть товары в корзине, добавленные ранее. <br/> Хотите добавить их в текущую корзину?"

export const BindCartContent: FC<{ clickCloseHandle?: () => void }> = ({
  clickCloseHandle,
}) => {
  const modalContext = useContext(ModalContext)
  const { user } = useAuth()
  const {
    productsInAuth,
    hasProductsInAuth,
    specificationMerge,
    isLoadingProductsInAuth,
    isMergingCart,
    mergeProductsCarts,
    isSomeProductsAddFetching,
    isSomeProductsAddFinish,
  } = useCart()

  const userName = useMemo(() => {
    let name = ""
    if (!!user?.fio) {
      const splitted = user.fio.split(" ")
      name = splitted[splitted.length - 2 || 0] || ""
    }
    return name.length === 0 ? null : name
  }, [user?.fio])

  const closeHandle = useCallback(() => {
    modalContext?.hide()
    if (clickCloseHandle) {
      clickCloseHandle()
    }
  }, [clickCloseHandle, modalContext])

  const addAllHandle = useCallback(
    (e) => {
      e.preventDefault()
      mergeProductsCarts()
    },
    [mergeProductsCarts],
  )

  return (
    <>
      {isLoadingProductsInAuth && <BaseLoader />}

      {isSomeProductsAddFetching && (
        <>
          <BaseLoader message={"Добавляем Ваши товары..."} />
        </>
      )}

      <SuccessOverlay
        isSuccess={isSomeProductsAddFinish}
        message={"Товары успешно добавлены в корзину"}
      />

      <Typography
        variant={"h1"}
        dangerouslySetInnerHTML={{
          __html:
            userName !== null
              ? `${userName}, ${WELCOME_TEXT}`
              : `${WELCOME_TEXT}`,
        }}
      />
      <ButtonGroup>
        <Button
          variant={"outline"}
          size={"large"}
          disabled={isMergingCart}
          onClick={closeHandle}
        >
          Закрыть
        </Button>
        <Button
          variant={"filled"}
          size={"large"}
          onClick={addAllHandle}
          disabled={isMergingCart}
        >
          Добавить все
        </Button>
      </ButtonGroup>
      <br />

      {hasProductsInAuth && (
        <>
          <Typography variant={"h5"}>
            Вы также можете добавить их по отдельности
          </Typography>

          {productsInAuth !== null && (
            <>
              <Products view={VIEW_PRODUCTS_LIST}>
                {productsInAuth.map((p) => (
                  <Product
                    product={p}
                    view={VIEW_PRODUCTS_LIST}
                    key={p.uuid || ""}
                    initQty={
                      (specificationMerge || {})[p.uuid || ""]?.quantity
                    }
                  />
                ))}
              </Products>

              {productsInAuth.length > 1 && (
                <ButtonGroup>
                  <Button
                    variant={"outline"}
                    size={"large"}
                    disabled={isMergingCart}
                    onClick={closeHandle}
                  >
                    Закрыть
                  </Button>
                  <Button
                    variant={"filled"}
                    size={"large"}
                    onClick={addAllHandle}
                    disabled={isMergingCart}
                  >
                    Добавить все
                  </Button>
                </ButtonGroup>
              )}
            </>
          )}
        </>
      )}
    </>
  )
}
