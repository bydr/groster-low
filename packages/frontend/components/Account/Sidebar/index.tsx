import { FC, useEffect, useState } from "react"
import Navigation from "../../Navigation/Navigation"
import { FormGroup } from "../../Forms/StyledForms"
import { Select } from "../../Select/Select"
import Sidebar from "../../Sidebar/Sidebar"
import { useRouter } from "next/router"
import { useWindowSize } from "../../../hooks/windowSize"
import { breakpoints } from "../../../styles/utils/vars"
import { getBreakpointVal } from "../../../styles/utils/Utils"
import { Typography } from "../../Typography/Typography"
import { useAuth } from "../../../hooks/auth"
import { IconText } from "../../IconText"
import { StyledProfileShort } from "../Profile/Styled"

export type PageType = {
  title: string
  path: string
  order?: number
}

export const SidebarLayout: FC<{ pages: PageType[] }> = ({ pages }) => {
  const router = useRouter()
  const [routePathParent, setRouterPathParent] = useState<string | undefined>()
  const { width } = useWindowSize()
  const { user, logout, isAuth } = useAuth()

  useEffect(() => {
    let path = pages.find((item) => router.pathname === item.path)?.path
    if (path === undefined) {
      path = pages.find((item) => router.pathname.includes(item.path))?.path
    }
    setRouterPathParent(path)
  }, [router.pathname, pages])

  return (
    <>
      <Sidebar>
        {isAuth && (
          <>
            <StyledProfileShort>
              <IconText icon={"User"}>{user?.fio || "Профиль"}</IconText>
              <Typography
                variant={"p14"}
                onClick={(e) => {
                  e.preventDefault()
                  logout()
                }}
              >
                Выйти
              </Typography>
            </StyledProfileShort>
          </>
        )}
        <Navigation
          activePath={routePathParent}
          items={[...pages].sort((a, b) =>
            (a?.order || 0) > (b?.order || 0)
              ? 1
              : (a?.order || 0) < (b?.order || 0)
              ? -1
              : 0,
          )}
        />
        {width !== undefined && width <= getBreakpointVal(breakpoints.lg) && (
          <>
            <FormGroup>
              <Select
                items={[...pages]
                  .sort((a, b) =>
                    (a?.order || 0) > (b?.order || 0)
                      ? 1
                      : (a?.order || 0) < (b?.order || 0)
                      ? -1
                      : 0,
                  )
                  .map((item) => ({
                    name: item.title,
                    value: item.path,
                  }))}
                variant={"default"}
                initialValue={routePathParent}
                onSelectValue={(value) => {
                  if (router.pathname !== value && value !== routePathParent) {
                    void router.replace(value)
                  }
                }}
              />
            </FormGroup>
          </>
        )}
      </Sidebar>
    </>
  )
}
