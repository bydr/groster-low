import type { FC } from "react"
import React from "react"
import {
  Col,
  Row,
  StyledBody,
  StyledCard,
  StyledDetail,
  StyledHeader,
  StyledLabel,
  StyledOrder,
} from "./Styled"
import { Button } from "../../../../../Button"
import {
  CURRENCY,
  ROUTES,
  TITLE_SITE_RU,
} from "../../../../../../utils/constants"
import { Typography } from "../../../../../Typography/Typography"
import { useQuery } from "react-query"
import { BaseLoader } from "../../../../../Loaders/BaseLoader/BaseLoader"
import { fetchOrdersByUid } from "../../../../../../api/orderAPI"
import { cssButtonBackward } from "../../../../../Button/StyledButton"
import { useOrder } from "../../../../../../hooks/order"
import { OrderState } from "../State"
import Price from "../../../../../Products/parts/Price/Price"
import { Controls } from "../Controls"
import { CreateAt } from "../CreateAt"
import { dateToString, priceToFormat } from "../../../../../../utils/helpers"
import { cssNoWrap } from "../../../../../../styles/utils/Utils"
import { OrderProducts } from "../../../../../Products/Order/OrderProducts"
import { Meta } from "../../../../../Meta/Meta"
import NumberFormat from "react-number-format"
import {
  FORMAT_PHONE,
  getPhoneWithOutCode,
} from "../../../../../../validations/phone"
import { Notification } from "../../../../../Notification/Notification"
import { RepeatSuccessContent } from "../RepeatSuccessContent"
import dynamic, { DynamicOptions } from "next/dynamic"
import { ModalDefaultPropsType } from "../../../../../Modals/Modal"

const DynamicModal = dynamic((() =>
  import("../../../../../Modals/Modal").then(
    (mod) => mod.Modal,
  )) as DynamicOptions<ModalDefaultPropsType>)

export const Detail: FC<{ uid: string | null }> = ({ uid }) => {
  const { data: orderData, isLoading } = useQuery(["detailOrder", uid], () =>
    !!uid ? fetchOrdersByUid({ uid }) : null,
  )
  const {
    stateOrder,
    createAtFormatted,
    order,
    totalCost,
    products,
    samples,
    specification,
    subscribeInterval,
    isFetchingSubscription,
    subscribe,
    unsubscribe,
    repeat,
    isFetchingProducts,
    cancel,
    isFetchingCancel,
    isFetchingRepeat,
    error,
    repeatIsSuccess,
    setRepeatIsSuccess,
    isFetchingRepeatGlobal,
  } = useOrder({
    order: !!orderData ? { ...orderData, uid: uid || "" } : undefined,
  })

  return (
    <>
      <Meta
        title={`Заказ ${
          !!order?.number ? `№${order?.number}` : ""
        } - ${TITLE_SITE_RU}`}
      />
      {error !== undefined && (
        <>
          <Notification
            notification={{
              title: "Произошла ошибка",
              message: error || "",
            }}
            isOpen={!!error}
          />
        </>
      )}

      <DynamicModal
        isShowModal={repeatIsSuccess}
        variant={"rounded-0"}
        title={"Успешно!"}
        closeMode={"destroy"}
        onClose={() => {
          setRepeatIsSuccess(false)
        }}
      >
        <RepeatSuccessContent />
      </DynamicModal>

      <StyledDetail>
        {isLoading && <BaseLoader />}
        {!!order ? (
          <>
            <Button
              variant={"small"}
              size={"small"}
              isHiddenBg
              icon={"ArrowLeft"}
              iconPosition={"left"}
              as={"a"}
              href={`${ROUTES.account}${ROUTES.historyOrders}`}
              className={cssButtonBackward}
            >
              Назад, к заказам
            </Button>

            <Typography variant={"h1"}>Заказ № {order.number}</Typography>

            <StyledCard>
              <StyledHeader>
                <Row>
                  <Col>
                    <CreateAt date={createAtFormatted} />
                  </Col>
                  <Col>
                    <OrderState state={stateOrder?.number} />
                  </Col>
                  <Col>
                    <Price
                      price={totalCost}
                      currency={CURRENCY}
                      variant={"total"}
                    />
                  </Col>
                </Row>
              </StyledHeader>
              <StyledBody>
                <Row>
                  <Col>
                    <StyledLabel>Получатель</StyledLabel>
                    <Typography variant={"p14"}>
                      <Typography variant={"span"}>
                        {[order.fio || null, order.email || null]
                          .filter((el) => el !== null)
                          .join(", ")}
                      </Typography>{" "}
                      <NumberFormat
                        format={FORMAT_PHONE}
                        mask={"_"}
                        value={getPhoneWithOutCode(order.phone) || ""}
                        displayType={"text"}
                      />
                    </Typography>
                  </Col>
                  <Col>
                    <StyledLabel>Доставка</StyledLabel>
                    <Typography variant={"p14"}>
                      <b>
                        {(order.shipping_cost || 0) > 0
                          ? `${priceToFormat(
                              order.shipping_cost || 0,
                            )} ${CURRENCY}`
                          : "Бесплатно"}
                        {". "}
                      </b>
                      {[order.shipping_method, order.shipping_address]
                        .filter((i) => !!i)
                        .join(". ")}{" "}
                      {!!order.shipping_date && (
                        <>
                          <Typography variant={"span"} className={cssNoWrap}>
                            {dateToString(new Date(order.shipping_date))}
                          </Typography>
                        </>
                      )}
                    </Typography>
                  </Col>
                  <Col>
                    <StyledLabel>Оплата</StyledLabel>
                    <Typography variant={"p14"}>
                      Наличными при получении
                    </Typography>
                  </Col>
                </Row>
              </StyledBody>
              <Controls
                stateName={stateOrder?.name}
                stateNumber={stateOrder?.number}
                uid={uid || undefined}
                subscribeInterval={subscribeInterval}
                isFetchingSubscription={isFetchingSubscription}
                subscribe={subscribe}
                unsubscribe={unsubscribe}
                repeat={repeat}
                cancel={cancel}
                isFetchingCancel={isFetchingCancel}
                isFetchingRepeat={isFetchingRepeat}
                isDisabledRepeat={isFetchingRepeatGlobal}
              />
            </StyledCard>

            <StyledOrder>
              {isFetchingProducts && (
                <>
                  <BaseLoader />
                </>
              )}
              <OrderProducts
                products={products}
                samples={samples}
                specification={specification}
              />
            </StyledOrder>
          </>
        ) : (
          <>
            {!isLoading && (
              <>
                <Row>
                  <Typography variant={"h1"}>Заказ не найден</Typography>
                </Row>
              </>
            )}
          </>
        )}
        <br />
      </StyledDetail>
    </>
  )
}
