import { styled } from "@linaria/react"
import { Panel } from "../../../../../../styles/utils/StyledPanel"
import { Paragraph14, Span } from "../../../../../Typography/Typography"
import { breakpoints, colors } from "../../../../../../styles/utils/vars"
import { StyledPriceWrapper } from "../../../../../Products/parts/Price/StyledPrice"
import { cssButtonMore } from "../Controls/Styled"
import { StyledProductTag } from "../../../../../Products/StyledProduct"

export const StyledLabel = styled(Paragraph14)`
  color: ${colors.grayDark};
  margin-bottom: 0;
`

export const Col = styled.div``
export const Row = styled.div`
  width: 100%;
  display: grid;
  grid-template-columns: 4fr 4fr 4fr;
  gap: 8px 20px;
`

export const StyledOrder = styled.div`
  width: 100%;
  position: relative;
`

export const StyledCard = styled(Panel)`
  width: 100%;

  .${cssButtonMore} {
    display: none !important;
  }

  ${Span} {
    word-break: break-word;
  }
`

export const StyledDetail = styled.section`
  width: 100%;
  position: relative;
  display: flex;
  flex-direction: column;
  align-items: flex-start;

  @media (max-width: ${breakpoints.sm}) {
    ${StyledCard}, ${StyledOrder} {
      margin-right: -15px;
      margin-left: -15px;
      width: auto;
      min-width: 100%;

      ${StyledProductTag} {
        &:before {
          display: none;
          content: none;
        }
      }
    }
  }
`

export const StyledHeader = styled.div`
  padding-bottom: 12px;
  border-bottom: 1px solid ${colors.gray};

  ${StyledPriceWrapper} {
    float: right;
    justify-content: flex-end;
  }

  @media (max-width: ${breakpoints.sm}) {
    ${Row} {
      grid-template-columns: 1fr 1fr;
      ${Col} {
        &:nth-child(2) {
          grid-row: 2;
          grid-column: 1/-1;
        }
      }
    }
  }
`
export const StyledBody = styled.div`
  padding-top: 12px;
  padding-bottom: 12px;

  @media (max-width: ${breakpoints.md}) {
    ${Row} {
      grid-template-columns: 1fr;
    }
  }
`
