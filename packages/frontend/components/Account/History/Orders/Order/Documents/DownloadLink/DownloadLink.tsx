import { FC } from "react"
import { IconText } from "../../../../../../IconText"
import { Typography } from "../../../../../../Typography/Typography"
import { Spin } from "../../../../../../Loaders/Spin"
import { StyledDownloadLink } from "./StyledDownloadLink"
import { TypographyVariantsType } from "../../../../../../../styles/utils/vars"
import { sizeSVGNamesType } from "../../../../../../Icon"
import { StyledLinkBase } from "../../../../../../Link/StyledLink"

export const DownloadLink: FC<{
  isLoading?: boolean
  link?: string
  loaderTextSize?: TypographyVariantsType
  loaderSize?: sizeSVGNamesType
}> = ({ link, isLoading, loaderSize, loaderTextSize = "p12" }) => {
  return (
    <>
      {isLoading && (
        <>
          <StyledDownloadLink>
            <Spin size={loaderSize} />
            <Typography variant={loaderTextSize}>
              Подготовка документов...
            </Typography>
          </StyledDownloadLink>
        </>
      )}
      {link !== undefined && (
        <>
          <StyledDownloadLink>
            <IconText icon={"Download"}>
              <StyledLinkBase
                href={link.includes("http") ? link : `https://${link}`}
                download
                rel={"nofollow"}
                target={"_blank"}
              >
                Скачать файл
              </StyledLinkBase>
            </IconText>
          </StyledDownloadLink>
        </>
      )}
    </>
  )
}
