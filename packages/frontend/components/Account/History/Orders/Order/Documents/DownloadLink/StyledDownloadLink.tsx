import { styled } from "@linaria/react"
import { colors } from "styles/utils/vars"
import { TypographyBase } from "../../../../../../Typography/Typography"

export const StyledDownloadLink = styled.div`
  display: flex;
  color: ${colors.brand.purple};
  align-items: center;
  margin: 10px 0;

  ${TypographyBase} {
    margin: 0;
  }
`
