import { Button } from "components/Button"
import { FC, ReactNode, useState } from "react"
import { Name, StyledDocument, StyledDocuments } from "./StyledDocuments"
import { useMutation } from "react-query"
import {
  fetchAttachedContractPayers,
  fetchGenerateContractPayers,
  fetchGenerateInvoicePayment,
  fetchGeneratePackingList,
} from "../../../../../../api/accountAPI"
import { DownloadLink } from "./DownloadLink/DownloadLink"
import { ApiError } from "next/dist/server/api-utils"
import { SingleError, Typography } from "../../../../../Typography/Typography"
import { OrderStateNumberType } from "../State"
import { CONTACTS } from "../../../../../../utils/constants"

const DOCUEMENT_TYPES = [
  "Подписанный договор плательщика",
  "Договор плательщика",
  "Счет",
  "ТОРГ-12",
]

const ComponentMessageError: FC = () => {
  return (
    <>
      <Typography variant={"span"}>
        Файл на стадии формирования. <br /> Свяжитесь с менеджером по телефону
      </Typography>{" "}
      <a href={CONTACTS[0].path}>
        <b>{CONTACTS[0].title}</b>
      </a>
    </>
  )
}

export const Documents: FC<{
  uid?: string
  stateNumber?: OrderStateNumberType
}> = ({ uid, stateNumber }) => {
  const [error, setError] = useState<string | ReactNode | undefined>()
  const {
    data: dataAttachedContractPayers,
    mutate: mutateAttachedContractPayers,
    isLoading: isLoadingAttachedContractPayers,
  } = useMutation(fetchAttachedContractPayers, {
    onSuccess: (res) => {
      if (res === null) {
        setError(ComponentMessageError)
      }
    },
    onError: (err: ApiError) => {
      setError(err.message || "Произошла ошибка")
    },
    onMutate: () => {
      setError(undefined)
    },
  })

  const {
    data: dataGenerateContractPayers,
    mutate: mutateGenerateContractPayers,
    isLoading: isLoadingGenerateContractPayers,
  } = useMutation(fetchGenerateContractPayers, {
    onSuccess: (res) => {
      if (res === null) {
        setError(ComponentMessageError)
      }
    },
    onError: (err: ApiError) => {
      setError(err.message || "Произошла ошибка")
    },
    onMutate: () => {
      setError(undefined)
    },
  })

  const {
    data: dataGenerateInvoicePayment,
    mutate: mutateGenerateInvoicePayment,
    isLoading: isLoadingGenerateInvoicePayment,
  } = useMutation(fetchGenerateInvoicePayment, {
    onSuccess: (res) => {
      if (res === null) {
        setError(ComponentMessageError)
      }
    },
    onError: (err: ApiError) => {
      setError(err.message || "Произошла ошибка")
    },
    onMutate: () => {
      setError(undefined)
    },
  })

  const {
    data: dataGeneratePackingList,
    mutate: mutateGeneratePackingList,
    isLoading: isLoadingGeneratePackingList,
  } = useMutation(fetchGeneratePackingList, {
    onSuccess: (res) => {
      if (res === null) {
        setError(ComponentMessageError)
      }
    },
    onError: (err: ApiError) => {
      setError(err.message || "Произошла ошибка")
    },
    onMutate: () => {
      setError(undefined)
    },
  })

  const docAttachedContractPayersHandle = (): void => {
    if (!uid) {
      return
    }
    mutateAttachedContractPayers({
      uid: uid,
    })
  }
  const docGenerateContractPayersHandle = (): void => {
    if (!uid) {
      return
    }
    mutateGenerateContractPayers({
      uid: uid,
    })
  }
  const docGenerateInvoicePaymentHandle = (): void => {
    if (!uid) {
      return
    }
    mutateGenerateInvoicePayment({
      uid: uid,
    })
  }
  const docGeneratePackingListHandle = (): void => {
    if (!uid) {
      return
    }
    mutateGeneratePackingList({
      uid: uid,
    })
  }

  return (
    <>
      <StyledDocuments>
        <StyledDocument>
          <Button
            variant={"link"}
            icon={"File"}
            onClick={() => {
              docAttachedContractPayersHandle()
            }}
          >
            <Name>{DOCUEMENT_TYPES[0]}</Name>
          </Button>
          <DownloadLink
            link={dataAttachedContractPayers?.link}
            isLoading={isLoadingAttachedContractPayers}
          />
        </StyledDocument>
        <StyledDocument>
          <Button
            variant={"link"}
            icon={"File"}
            onClick={() => {
              docGenerateContractPayersHandle()
            }}
          >
            <Name>{DOCUEMENT_TYPES[1]}</Name>
          </Button>
          <DownloadLink
            link={dataGenerateContractPayers?.link}
            isLoading={isLoadingGenerateContractPayers}
          />
        </StyledDocument>
        <StyledDocument>
          <Button
            variant={"link"}
            icon={"File"}
            onClick={() => {
              docGenerateInvoicePaymentHandle()
            }}
          >
            <Name>{DOCUEMENT_TYPES[2]}</Name>
          </Button>
          <DownloadLink
            link={dataGenerateInvoicePayment?.link}
            isLoading={isLoadingGenerateInvoicePayment}
          />
        </StyledDocument>

        {(stateNumber === 3 || stateNumber === 6) && (
          <StyledDocument>
            <Button
              variant={"link"}
              icon={"File"}
              onClick={() => {
                docGeneratePackingListHandle()
              }}
            >
              <Name>{DOCUEMENT_TYPES[3]}</Name>
            </Button>
            <DownloadLink
              link={dataGeneratePackingList?.link}
              isLoading={isLoadingGeneratePackingList}
            />
          </StyledDocument>
        )}

        {error && <SingleError>{error}</SingleError>}
      </StyledDocuments>
    </>
  )
}
