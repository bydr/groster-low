import { styled } from "@linaria/react"
import { getTypographyBase, Span } from "../../../../../Typography/Typography"
import { colors } from "../../../../../../styles/utils/vars"
import { ButtonBase } from "../../../../../Button/StyledButton"
import { StyledSpin } from "../../../../../Loaders/Spin/StyledSpin"
import { cssIcon, getSizeSVG } from "../../../../../Icon"

export const StyledDocument = styled.div`
  ${ButtonBase} {
    padding-left: 0;
    padding-right: 0;
  }
`

export const Name = styled(Span)`
  ${getTypographyBase("p14")};
`

export const Ext = styled(Span)`
  ${getTypographyBase("p12")};
  color: ${colors.grayDark};
`

export const StyledDocuments = styled.div`
  width: 100%;
  display: flex;
  flex-direction: column;

  ${StyledSpin} {
    display: inline-flex;
    width: auto;
    height: auto;
    .${cssIcon} {
      ${getSizeSVG("32px")};
    }
  }
`
