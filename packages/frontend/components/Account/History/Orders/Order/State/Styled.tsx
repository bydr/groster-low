import { styled } from "@linaria/react"
import { Paragraph14 } from "../../../../../Typography/Typography"
import { OrderStateStringType } from "./index"
import { colors } from "../../../../../../styles/utils/vars"

export const StyledOrderState = styled.div``
export const StyledOrderStateName = styled(Paragraph14)<{
  orderState: OrderStateStringType
}>`
  color: ${(props) =>
    props.orderState === "new"
      ? colors.brand.blue
      : props.orderState === "canceled"
      ? colors.red
      : props.orderState === "completed"
      ? colors.green
      : colors.black};
  font-weight: 600;
  margin-bottom: 0;
`
