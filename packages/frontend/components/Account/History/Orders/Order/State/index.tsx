import { FC, useEffect, useState } from "react"
import { StyledOrderState, StyledOrderStateName } from "./Styled"
export type OrderStateStringType =
  | "active"
  | "completed"
  | "canceled"
  | "new"
  | "courier"
export type OrderStateNumberType = 2 | 3 | 4 | 5 | 6

export type StateOrderDataType = {
  number: OrderStateNumberType
  name: OrderStateStringType
  message: string
}

export const getOrderStateData = ({
  state,
}: {
  state: OrderStateNumberType
}): StateOrderDataType => {
  switch (state) {
    case 2: {
      return {
        number: state,
        name: "new",
        message: "Новый",
      }
    }
    case 3: {
      return {
        number: state,
        name: "completed",
        message: "Завершен",
      }
    }
    case 4: {
      return {
        number: state,
        name: "canceled",
        message: "Отменен",
      }
    }
    case 5: {
      return {
        number: state,
        name: "active",
        message: "В обработке",
      }
    }
    case 6: {
      return {
        number: state,
        name: "courier",
        message: "Передан курьеру",
      }
    }
    default:
      return {
        number: state,
        name: "active",
        message: "В обработке",
      }
  }
}

export const OrderState: FC<{ state?: OrderStateNumberType }> = ({ state }) => {
  const [orderStateString, setOrderStateString] =
    useState<OrderStateStringType | null>(null)
  const [name, setName] = useState("")

  useEffect(() => {
    if (state !== undefined) {
      const stateData = getOrderStateData({
        state,
      })
      setOrderStateString(stateData.name)
      setName(stateData.message)
    }
  }, [state])

  return (
    <>
      <StyledOrderState>
        {orderStateString !== null && (
          <>
            <StyledOrderStateName orderState={orderStateString}>
              {name}
            </StyledOrderStateName>
          </>
        )}
      </StyledOrderState>
    </>
  )
}
