import { styled } from "@linaria/react"
import { Paragraph14 } from "../../../../../Typography/Typography"

export const StyledCreateAt = styled(Paragraph14)`
  margin-bottom: 0;
`
