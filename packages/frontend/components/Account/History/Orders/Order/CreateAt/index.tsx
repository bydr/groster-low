import { FC } from "react"
import { Typography } from "../../../../../Typography/Typography"
import { StyledCreateAt } from "./Styled"

export const CreateAt: FC<{ date?: string }> = ({ date }) => {
  return (
    <>
      {!!date && (
        <StyledCreateAt>
          <Typography variant={"span"}>{date}</Typography>
        </StyledCreateAt>
      )}
    </>
  )
}
