import { styled } from "@linaria/react"
import {
  getTypographyBase,
  Paragraph14,
} from "../../../../../Typography/Typography"
import { Panel } from "../../../../../../styles/utils/StyledPanel"
import { StyledPriceWrapper } from "../../../../../Products/parts/Price/StyledPrice"
import { breakpoints, colors } from "../../../../../../styles/utils/vars"
import { StyledOrderState } from "../State/Styled"
import { StyledCreateAt } from "../CreateAt/Styled"

export const StyledOrder = styled.div`
  ${getTypographyBase("p14")};
  position: relative;
  width: 100%;

  ${Panel} {
    padding: 24px;
    margin: 0;
    display: grid;
    grid-template-areas:
      "number number createAt total total total total total state state state state"
      "controls controls controls controls controls controls controls controls controls controls controls controls";
    grid-template-columns: 2fr 1fr 5fr 4fr;
    gap: 8px;
  }

  &:first-child {
    ${Panel} {
      border-bottom-right-radius: 0;
      border-bottom-left-radius: 0;
      border-bottom-width: 0;
    }
  }

  &:last-child {
    ${Panel} {
      border-top-right-radius: 0;
      border-top-left-radius: 0;
    }
  }

  &:last-child:first-child {
    ${Panel} {
      border-radius: 12px;
      border-bottom-width: 1px;
    }
  }

  &:not(:first-child):not(:last-child) {
    ${Panel} {
      border-radius: 0;
      border-bottom: 0;
    }
  }

  ${StyledPriceWrapper} {
    grid-area: total;
  }

  ${StyledOrderState} {
    grid-area: state;
  }

  &[data-state="canceled"] {
    ${Panel} {
      background: ${colors.grayLight};
    }
  }

  ${StyledCreateAt} {
    grid-area: createAt;
  }

  @media (max-width: ${breakpoints.sm}) {
    ${Panel} {
      grid-template-areas:
        "number total"
        "createAt state"
        "controls controls";
      grid-template-columns: 1fr 1fr;

      ${StyledOrderState} {
        text-align: right;
      }

      ${StyledPriceWrapper} {
        justify-content: flex-end;
      }
    }
  }
`
export const Number = styled(Paragraph14)`
  font-weight: 600;
  grid-area: number;
  margin-bottom: 0;
`
