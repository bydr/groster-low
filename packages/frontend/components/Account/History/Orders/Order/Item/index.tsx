import type { FC } from "react"
import { OrderList } from "../../../../../../../contracts/contracts"
import { Panel } from "../../../../../../styles/utils/StyledPanel"
import { OrderState } from "../State"
import Price from "../../../../../Products/parts/Price/Price"
import { CURRENCY } from "../../../../../../utils/constants"
import { useOrder } from "../../../../../../hooks/order"
import { Controls } from "../Controls"
import { CreateAt } from "../CreateAt"
import { Number, StyledOrder } from "./Styled"
import { Notification } from "../../../../../Notification/Notification"
import dynamic, { DynamicOptions } from "next/dynamic"
import { ModalDefaultPropsType } from "../../../../../Modals/Modal"
import { RepeatSuccessContent } from "../RepeatSuccessContent"

const DynamicModal = dynamic((() =>
  import("../../../../../Modals/Modal").then(
    (mod) => mod.Modal,
  )) as DynamicOptions<ModalDefaultPropsType>)

export const OrderItem: FC<{ order: OrderList }> = ({ order }) => {
  const {
    stateOrder,
    createAtFormatted,
    totalCost,
    subscribeInterval,
    subscribe,
    unsubscribe,
    isFetchingSubscription,
    repeat,
    isFetchingRepeat,
    cancel,
    isFetchingCancel,
    error,
    repeatIsSuccess,
    setRepeatIsSuccess,
    isFetchingRepeatGlobal,
  } = useOrder({ order: { ...order, uid: order.uid || "" } })

  return (
    <>
      {error !== undefined && (
        <>
          <Notification
            notification={{
              title: "Произошла ошибка",
              message: error || "",
            }}
            isOpen={!!error}
          />
        </>
      )}
      <StyledOrder data-state={stateOrder?.name}>
        <Panel>
          <Number>№ {order.number}</Number>
          <CreateAt date={createAtFormatted} />
          <Price price={totalCost} currency={CURRENCY} variant={"total"} />
          <OrderState state={stateOrder?.number} />
          <Controls
            stateName={stateOrder?.name}
            stateNumber={stateOrder?.number}
            uid={order.uid}
            subscribeInterval={subscribeInterval}
            subscribe={subscribe}
            unsubscribe={unsubscribe}
            isFetchingSubscription={isFetchingSubscription}
            repeat={repeat}
            isFetchingRepeat={isFetchingRepeat}
            cancel={cancel}
            isFetchingCancel={isFetchingCancel}
            isDisabledRepeat={isFetchingRepeatGlobal}
          />
          <DynamicModal
            isShowModal={repeatIsSuccess}
            variant={"rounded-0"}
            title={"Успешно!"}
            closeMode={"destroy"}
            onClose={() => {
              setRepeatIsSuccess(false)
            }}
          >
            <RepeatSuccessContent />
          </DynamicModal>
        </Panel>
      </StyledOrder>
    </>
  )
}
