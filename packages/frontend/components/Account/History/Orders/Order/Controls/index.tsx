import type { FC } from "react"
import { OrderStateNumberType, OrderStateStringType } from "../State"
import { Button } from "../../../../../Button"
import { ROUTES } from "../../../../../../utils/constants"
import { ControlGroup, cssButtonMore, StyledControls } from "./Styled"
import { Select } from "../../../../../Select/Select"
import { Modal } from "../../../../../Modals/Modal"
import { Documents } from "../Documents"

export const Controls: FC<{
  stateName?: OrderStateStringType
  stateNumber?: OrderStateNumberType
  uid?: string
  subscribeInterval: number | null
  subscribe: (interval: number) => void
  unsubscribe: () => void
  isFetchingSubscription: boolean
  repeat: () => void
  isFetchingRepeat?: boolean
  isFetchingCancel?: boolean
  cancel: () => void
  isDisabledRepeat?: boolean
}> = ({
  stateName,
  uid,
  subscribeInterval,
  subscribe,
  unsubscribe,
  isFetchingSubscription,
  isFetchingRepeat,
  repeat,
  isFetchingCancel,
  cancel,
  stateNumber,
  isDisabledRepeat = false,
}) => {
  return (
    <>
      {!!stateName && (
        <>
          <StyledControls>
            <ControlGroup>
              <Button
                variant={"small"}
                icon={"Info"}
                as={"a"}
                href={`${ROUTES.account}${ROUTES.historyOrders}/${uid}`}
                className={cssButtonMore}
              >
                Подробнее
              </Button>
              <Select
                items={[
                  {
                    name: "Нет",
                    value: "-1",
                  },
                  {
                    name: "Каждый день",
                    value: "1",
                  },
                  {
                    name: "Каждые две недели",
                    value: "2",
                  },
                  {
                    name: "1-е число каждгого месяца",
                    value: "3",
                  },
                ]}
                variant={"small"}
                staticPlaceholder={"Подписка:"}
                iconSelect={"CalendarEdit"}
                initialValue={
                  subscribeInterval !== null ? String(subscribeInterval) : "-1"
                }
                onSelectValue={(value) => {
                  // toggleSubscription(+value)
                  const loc = [1, 2, 3].includes(+value) ? +value : null
                  if (loc !== subscribeInterval) {
                    if (loc !== null) {
                      subscribe(loc)
                    } else {
                      unsubscribe()
                    }
                  }
                }}
                isFetching={isFetchingSubscription}
              />
            </ControlGroup>
            <ControlGroup>
              <Modal
                disclosure={
                  <Button variant={"small"} icon={"File"}>
                    Документы
                  </Button>
                }
                variant={"rounded-0"}
                title={"Документы по заказу"}
                closeMode={"destroy"}
              >
                <Documents uid={uid} stateNumber={stateNumber} />
              </Modal>
              <Button
                variant={"small"}
                icon={"Rotate"}
                onClick={() => {
                  repeat()
                }}
                isFetching={isFetchingRepeat}
                disabled={isDisabledRepeat}
              >
                Повторить заказ
              </Button>

              {!!stateName && !["completed", "canceled"].includes(stateName) && (
                <>
                  <Button variant={"filled"} icon={"Wallet"} size={"small"}>
                    Оплатить заказ
                  </Button>
                  <Button
                    variant={"small"}
                    icon={"Remove"}
                    isFetching={isFetchingCancel}
                    onClick={cancel}
                  >
                    Отменить
                  </Button>
                </>
              )}
            </ControlGroup>
          </StyledControls>
        </>
      )}
    </>
  )
}
