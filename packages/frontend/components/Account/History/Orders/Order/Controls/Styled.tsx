import { css } from "@linaria/core"
import { styled } from "@linaria/react"
import { ButtonBase } from "../../../../../Button/StyledButton"
import { breakpoints, colors } from "../../../../../../styles/utils/vars"
import {
  StyledIconSelect,
  StyledSelect,
  StyledSelectedTitle,
  StyledSelectInputDiv,
} from "../../../../../Select/StyledSelect"
import { StyledFieldWrapper } from "../../../../../Field/StyledField"
import { getTypographyBase } from "../../../../../Typography/Typography"

export const cssButtonMore = css``

export const StyledControls = styled.div`
  width: 100%;
  grid-area: controls;
  display: flex;
  flex-wrap: wrap;
  gap: 8px;
  align-items: flex-start;

  ${ButtonBase} {
    display: inline-flex;
  }

  ${StyledSelect} {
    [role="combobox"] {
      background: ${colors.pinkLight};
      position: absolute;
      left: 0;
      right: 0;
      z-index: 0;
      top: 50%;
      transform: translateY(-50%);
      height: 100%;

      &:hover,
      &:active {
        background: ${colors.pink};
      }
    }

    &[data-variant="small"] {
      ${StyledSelectedTitle} {
        ${getTypographyBase("p12")};
        color: ${colors.brand.purple};
        position: relative;
        bottom: initial;
        right: initial;
        transform: none;
        left: initial;
        margin: 0 34px 0 15px;
        line-height: 160%;
      }

      ${StyledIconSelect} {
        z-index: 1;
        position: relative;
        min-width: initial;
        margin-left: 12px;

        ~ ${StyledSelectedTitle} {
          margin-left: 4px;
        }
      }

      ${StyledSelectInputDiv} {
        min-height: 30px;
      }
    }
  }

  ${StyledFieldWrapper} {
    margin-bottom: 0;
  }

  @media (max-width: ${breakpoints.sm}) {
    flex-direction: column;
  }
`

export const ControlGroup = styled.div`
  flex: 1;
  display: flex;
  gap: 8px;
  flex-wrap: wrap;
  align-items: flex-start;

  &:last-child {
    justify-content: flex-end;
  }

  @media (max-width: ${breakpoints.sm}) {
    &:last-child {
      justify-content: flex-start;
    }
  }
`
