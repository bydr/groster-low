import type { FC } from "react"
import { Typography } from "../../../../../Typography/Typography"
import { Button } from "../../../../../Button"
import { ROUTES } from "../../../../../../utils/constants"
import { ButtonGroup } from "../../../../../Button/StyledButton"
import { useContext } from "react"
import { ModalContext } from "../../../../../Modals/Modal"

export const RepeatSuccessContent: FC = () => {
  const modalContext = useContext(ModalContext)

  return (
    <>
      <Typography variant={"p14"}>
        Товары заказа успешно добавлены. <br /> Теперь вы можете продолжить
        оформление заказа или остаться на текущей странице
      </Typography>
      <ButtonGroup>
        <Button variant={"filled"} as={"a"} href={ROUTES.checkout}>
          Оформить заказ
        </Button>
        <Button
          variant={"outline"}
          onClick={() => {
            modalContext?.hide()
          }}
        >
          Закрыть
        </Button>
      </ButtonGroup>
    </>
  )
}
