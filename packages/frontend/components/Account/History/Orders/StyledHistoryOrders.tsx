import { styled } from "@linaria/react"

const StyledListContainer = styled.section`
  width: 100%;
  position: relative;
`

export const StyledOrdersList = styled(StyledListContainer)``
export const StyledProductsList = styled(StyledListContainer)``
