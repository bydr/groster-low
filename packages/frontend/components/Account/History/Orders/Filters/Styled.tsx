import {
  StyledFilterColumn,
  StyledFilterGroup,
  StyledFilterRow,
  StyledFiltersContainer,
} from "../../../../Filter/StyledFilter"
import { styled } from "@linaria/react"
import {
  cssRadioGroup,
  RadioLabel,
  StyledRadioGroup,
} from "../../../../Radio/StyledRadio"
import { getTypographyBase } from "../../../../Typography/Typography"
import { breakpoints } from "../../../../../styles/utils/vars"
import { css } from "@linaria/core"
import { ButtonBase } from "../../../../Button/StyledButton"
import { StyledTags } from "components/Tags/StyledTags"

export const StyledFilterExtends = styled.div`
  padding: 20px 0;
`

export const cssButtonTogglerTags = css`
  &${ButtonBase} {
    font-weight: 600;
    @media (max-width: ${breakpoints.md}) {
      width: 100%;
    }
  }
`

export const StyledOrderFiltersContainer = styled(StyledFiltersContainer)`
  ${StyledFilterRow} {
    ${StyledFilterColumn} {
      width: 100% !important;
      display: grid;
      margin: 0;
      grid-template-columns: 6fr 6fr 12fr;
      gap: 0 40px;
      justify-content: flex-start;
      justify-items: flex-start;
    }
  }

  .${cssRadioGroup} {
    margin: 0;
  }

  ${StyledRadioGroup} {
    margin: 0;
    ${RadioLabel} {
      ${getTypographyBase("p14")};
      white-space: nowrap;
    }
  }

  ${StyledTags} {
    margin: 10px 0 0 0;
  }

  @media (max-width: ${breakpoints.lg}) {
    ${StyledFilterRow} {
      ${StyledFilterColumn} {
        grid-template-columns: 12fr 12fr;
        gap: 10px 24px;

        > ${StyledFilterGroup} {
          &:last-child {
            width: 100%;
            justify-content: flex-end;
          }
        }
      }
    }
  }
`
