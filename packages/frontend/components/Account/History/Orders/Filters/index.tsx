import { FC, useCallback, useEffect, useState } from "react"
import {
  cssButtonFilterDialogTrigger,
  StyledFilterColumn,
  StyledFilterGroup,
  StyledFilterRow,
} from "../../../../Filter/StyledFilter"
import Button from "../../../../Button/Button"
import { CustomRadioGroup } from "../../../../Radio/CustomRadioGroup"
import { useFilterHistoryOrders } from "../../../../../hooks/filterHistoryOrders"
import { useAppDispatch, useAppSelector } from "../../../../../hooks/redux"
import { accountSlice } from "../../../../../store/reducers/accountSlice"
import {
  ClearItemFilterPropsType,
  RadioGroupItemsType,
  ShortCategoryType,
} from "../../../../../types/types"
import { ViewModeType } from "../index"
import { cssButtonTogglerTags, StyledOrderFiltersContainer } from "./Styled"
import { OrderStateNumberType } from "../Order/State"
import { FilterParams } from "./FilterParams"
import { cx } from "@linaria/core"
import {
  cssIsActive,
  getBreakpointVal,
} from "../../../../../styles/utils/Utils"
import { useWindowSize } from "../../../../../hooks/windowSize"
import { breakpoints } from "../../../../../styles/utils/vars"
import { Select } from "../../../../Select/Select"
import { Modal } from "../../../../Modals/Modal"
import { cssTag, StyledTags } from "../../../../Tags/StyledTags"

const LIMIT_SHOW_ALL = 15

const ITEMS_VIEW: RadioGroupItemsType[] = [
  {
    value: "orders",
    message: "По заказам",
  },
  {
    value: "products",
    message: "По товарам",
  },
]

const ITEMS_STATE: RadioGroupItemsType[] = [
  {
    value: -1,
    message: "Все",
  },
  {
    value: 3,
    message: "Завершенные",
  },
  {
    value: 5,
    message: "В обработке",
  },
]

const SELECT_ITEMS_STATE = ITEMS_STATE.map((item) => ({
  name: item.message,
  value: `${item.value}`,
}))

export const Filters: FC = () => {
  const {
    viewMode,
    setViewMode,
    clearFilterQuery,
    updateFilterQuery,
    checkedParamsKeysTotal,
    availableParams,
  } = useFilterHistoryOrders()

  const dispatch = useAppDispatch()
  const historyFilter = useAppSelector((state) => state.profile.history.filter)
  const products = useAppSelector(
    (state) => state.profile.history.products.items,
  )
  const categoriesFetched = useAppSelector(
    (state) => state.catalog.categories?.fetched || null,
  )
  const { updateCheckedParams } = accountSlice.actions
  const [availableCategories, setAvailableCategories] = useState<
    ShortCategoryType[]
  >([])
  const [isShowedAll, setIsShowedAll] = useState<boolean>(false)

  const { width } = useWindowSize()

  const onClearFilterItemHandle = ({
    keyFilter,
    checked,
    name,
  }: ClearItemFilterPropsType) => {
    if (name !== undefined) {
      updateFilterQuery({
        [name]: null,
      })
    }
    dispatch(
      updateCheckedParams({
        keyFilter,
        checked,
      }),
    )
  }

  const onChangeCheckboxHandle = (
    e: React.FormEvent<HTMLInputElement>,
    isDisabledField: boolean,
  ) => {
    if (isDisabledField) {
      e.preventDefault()
      return
    }

    if (historyFilter.params[e.currentTarget.name]?.parentUuid === "year") {
      if (availableParams !== null) {
        if (availableParams["year"]?.checkedKeys?.length > 0) {
          dispatch(
            updateCheckedParams({
              keyFilter: availableParams["year"].checkedKeys,
              checked: false,
            }),
          )
        }
      }
    }

    dispatch(
      updateCheckedParams({
        keyFilter: [e.currentTarget.name],
        checked: e.currentTarget.checked,
      }),
    )
  }

  const createAvailableCategories = useCallback(() => {
    let prodCategoriesIds: string[] = []
    let categories: ShortCategoryType[] = []
    for (const p of Object.keys(products || {})) {
      prodCategoriesIds = [
        ...prodCategoriesIds,
        ...((products || {})[p].categories || []),
      ]
    }
    for (const c of Object.keys(categoriesFetched || {}).filter((c) =>
      prodCategoriesIds.includes(c),
    )) {
      categories = [
        ...categories,
        {
          ...(categoriesFetched || {})[c],
        },
      ]
    }
    setAvailableCategories(
      categories.sort((a, b) =>
        (a.name || "") > (b.name || "")
          ? 1
          : (a.name || "") < (b.name || "")
          ? -1
          : 0,
      ),
    )
  }, [categoriesFetched, products])

  const onChangeStateValue = useCallback(
    (value?: string | number) => {
      const val = value || -1
      if (historyFilter.state !== +val) {
        updateFilterQuery({
          state: +val as OrderStateNumberType | undefined,
        })
      }
    },
    [historyFilter.state, updateFilterQuery],
  )

  useEffect(() => {
    createAvailableCategories()
  }, [createAvailableCategories])

  return (
    <>
      <StyledOrderFiltersContainer>
        <StyledFilterRow>
          <StyledFilterColumn>
            {width !== undefined && width > getBreakpointVal(breakpoints.sm) && (
              <>
                <StyledFilterGroup>
                  <CustomRadioGroup
                    variant={"tabs"}
                    indexCurrent={
                      historyFilter.state !== null
                        ? ITEMS_STATE.findIndex(
                            (item) => item.value === historyFilter.state,
                          )
                        : null
                    }
                    items={ITEMS_STATE}
                    ariaLabel={"packageToggle"}
                    onChange={onChangeStateValue}
                  />
                </StyledFilterGroup>
              </>
            )}

            {width !== undefined && width > getBreakpointVal(breakpoints.lg) && (
              <>
                <StyledFilterGroup>
                  <CustomRadioGroup
                    variant={"buttons"}
                    indexDefault={ITEMS_VIEW.findIndex(
                      (item) => item.value === viewMode,
                    )}
                    items={ITEMS_VIEW}
                    ariaLabel={"packageToggle"}
                    onChange={(value) => {
                      if (!!value) {
                        setViewMode(String(value) as ViewModeType)
                      }
                    }}
                  />
                </StyledFilterGroup>
              </>
            )}

            <StyledFilterGroup>
              {width !== undefined && width > getBreakpointVal(breakpoints.lg) && (
                <>
                  <FilterParams
                    availableParams={availableParams}
                    clearFilters={clearFilterQuery}
                    checkedParamsKeysTotal={checkedParamsKeysTotal}
                    onUpdateFiltersHandle={updateFilterQuery}
                    onChangeCheckboxHandle={onChangeCheckboxHandle}
                    onClearItemHandle={onClearFilterItemHandle}
                    categoryFilter={
                      viewMode === "products" && !!historyFilter.category
                        ? historyFilter.category
                        : undefined
                    }
                  />
                </>
              )}

              {width !== undefined &&
                width <= getBreakpointVal(breakpoints.lg) && (
                  <>
                    <Modal
                      variant={"full"}
                      title={"Фильтр"}
                      disclosure={
                        <Button
                          variant={"small"}
                          icon={"Filter"}
                          className={cssButtonFilterDialogTrigger}
                        >
                          Фильтр
                        </Button>
                      }
                    >
                      <FilterParams
                        availableParams={availableParams}
                        clearFilters={clearFilterQuery}
                        checkedParamsKeysTotal={checkedParamsKeysTotal}
                        onUpdateFiltersHandle={updateFilterQuery}
                        onChangeCheckboxHandle={onChangeCheckboxHandle}
                        onClearItemHandle={onClearFilterItemHandle}
                        categoryFilter={
                          viewMode === "products" && !!historyFilter.category
                            ? historyFilter.category
                            : undefined
                        }
                      >
                        <StyledFilterGroup>
                          <CustomRadioGroup
                            variant={"buttons"}
                            indexDefault={ITEMS_VIEW.findIndex(
                              (item) => item.value === viewMode,
                            )}
                            items={ITEMS_VIEW}
                            ariaLabel={"packageToggle"}
                            onChange={(value) => {
                              if (!!value) {
                                setViewMode(String(value) as ViewModeType)
                              }
                            }}
                          />
                        </StyledFilterGroup>
                      </FilterParams>
                    </Modal>
                  </>
                )}
            </StyledFilterGroup>

            {width !== undefined && width <= getBreakpointVal(breakpoints.sm) && (
              <>
                <StyledFilterGroup>
                  <Select
                    ariaLabel={"state"}
                    items={SELECT_ITEMS_STATE}
                    variant={"small"}
                    initialValue={
                      historyFilter.state !== null
                        ? SELECT_ITEMS_STATE.find(
                            (item) => item.value === `${historyFilter.state}`,
                          )?.value
                        : undefined
                    }
                    onSelectValue={onChangeStateValue}
                  />
                </StyledFilterGroup>
              </>
            )}
          </StyledFilterColumn>
        </StyledFilterRow>
        <StyledFilterRow>
          {availableCategories.length > 0 && viewMode === "products" && (
            <>
              <StyledTags>
                {availableCategories
                  .filter((_, i) => (!isShowedAll ? i < LIMIT_SHOW_ALL : true))
                  .map((category) => {
                    if (!!category.uuid) {
                      return (
                        <>
                          <Button
                            key={category.uuid}
                            className={cx(
                              historyFilter.category === category.uuid &&
                                cssIsActive,
                              cssTag,
                            )}
                            onClick={() => {
                              updateFilterQuery({
                                category: category.uuid,
                              })
                            }}
                          >
                            {category.name}
                          </Button>
                        </>
                      )
                    }
                  })}
                {availableCategories.length > LIMIT_SHOW_ALL && (
                  <>
                    <Button
                      className={cx(
                        cssButtonTogglerTags,
                        isShowedAll && cssIsActive,
                      )}
                      onClick={() => {
                        setIsShowedAll(!isShowedAll)
                      }}
                    >
                      {isShowedAll ? "Скрыть" : "Больше"}
                    </Button>
                  </>
                )}
              </StyledTags>
            </>
          )}
        </StyledFilterRow>
      </StyledOrderFiltersContainer>
    </>
  )
}
