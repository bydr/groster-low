import { FC, MouseEvent, useContext } from "react"
import Button from "../../../../Button/Button"
import { StyledFilters, TotalControls } from "../../../../Filter/StyledFilter"
import { FilterHandlersType, FilterList } from "../../../../Filter/List/List"
import { cssHiddenLG } from "../../../../../styles/utils/Utils"
import { IAvailableParams } from "../../../../../store/reducers/catalogSlice"
import { ModalContext } from "../../../../Modals/Modal"
import { cx } from "@linaria/core"
import { cssButtonClearFilters } from "../../../../Button/StyledButton"
import { UpdateFilterHistoryQueryPropsType } from "../../../../../types/types"
import { StyledFilterExtends } from "./Styled"

export const FilterParams: FC<
  {
    availableParams: IAvailableParams | null
    clearFilters: (e: MouseEvent<HTMLButtonElement>) => void
    checkedParamsKeysTotal: string[]
    categoryFilter?: string
  } & FilterHandlersType
> = ({
  availableParams,
  onUpdateFiltersHandle,
  checkedParamsKeysTotal,
  clearFilters,
  onChangeCheckboxHandle,
  onClearItemHandle,
  categoryFilter,
  children,
}) => {
  const modalContext = useContext(ModalContext)

  return (
    <>
      <StyledFilters>
        <FilterList
          filters={availableParams}
          onUpdateFiltersHandle={onUpdateFiltersHandle}
          onChangeCheckboxHandle={onChangeCheckboxHandle}
          onClearItemHandle={onClearItemHandle}
        />
        {(checkedParamsKeysTotal.length > 0 || !!categoryFilter) && (
          <Button
            variant={"link"}
            icon={"Rotate"}
            className={cx(cssHiddenLG, cssButtonClearFilters)}
            onClick={clearFilters}
          >
            Очистить фильтры
          </Button>
        )}
      </StyledFilters>
      <StyledFilterExtends>{children}</StyledFilterExtends>
      <TotalControls>
        <Button
          variant={"outline"}
          size={"large"}
          onClick={clearFilters}
          disabled={!(checkedParamsKeysTotal.length > 0 || !!categoryFilter)}
        >
          Сбросить все
        </Button>
        <Button
          variant={"filled"}
          size={"large"}
          onClick={(e) => {
            e.preventDefault()
            modalContext?.hide()
            onUpdateFiltersHandle<UpdateFilterHistoryQueryPropsType>()
          }}
        >
          Применить
        </Button>
      </TotalControls>
    </>
  )
}
