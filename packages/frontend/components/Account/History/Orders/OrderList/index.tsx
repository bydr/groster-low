import { FC, useEffect } from "react"
import { StyledOrdersList } from "../StyledHistoryOrders"
import { OrderItem } from "../Order/Item"
import { Typography } from "../../../../Typography/Typography"
import { Paginator } from "../../../../Paginator/Paginator"
import { orderAPI } from "../../../../../api/orderAPI"
import { RequestOrderListOrders } from "../../../../../types/types"
import { useAppDispatch, useAppSelector } from "../../../../../hooks/redux"
import { accountSlice } from "../../../../../store/reducers/accountSlice"
import { useFilterHistoryOrders } from "../../../../../hooks/filterHistoryOrders"
import { BaseLoader } from "../../../../Loaders/BaseLoader/BaseLoader"

export const OrderList: FC<{ requestData: RequestOrderListOrders | null }> = ({
  requestData,
}) => {
  const { data: dataOrders, isFetching: isFetchingOrders } =
    orderAPI.useFetchOrderListOrders(requestData)
  const dispatch = useAppDispatch()
  const orders = useAppSelector((state) => state.profile.history.orders.items)
  const total = useAppSelector((state) => state.profile.history.orders.total)
  const itemsPerPage = useAppSelector(
    (state) => state.profile.history.itemsPerPage,
  )
  const { setCurrentPage, setTotalOrders, setOrders } = accountSlice.actions
  const { updateFilterQuery, currentPage } = useFilterHistoryOrders()

  const onPageChangedHandler = (page: number) => {
    dispatch(setCurrentPage(page))
    updateFilterQuery({
      page: page,
    })
  }

  useEffect(() => {
    dispatch(setTotalOrders(dataOrders?.total || 0))
    dispatch(setOrders(dataOrders?.orders || []))
  }, [
    dataOrders?.total,
    setTotalOrders,
    dispatch,
    setOrders,
    dataOrders?.orders,
  ])

  return (
    <>
      <StyledOrdersList>
        {isFetchingOrders && <BaseLoader />}
        {orders !== null && (
          <>
            {Object.keys(orders).length > 0 ? (
              <>
                {Object.keys(orders).map((o) => (
                  <OrderItem order={orders[o]} key={o || ""} />
                ))}
              </>
            ) : (
              <Typography variant={"p14"}>Заказов не найдено</Typography>
            )}
          </>
        )}
      </StyledOrdersList>
      <Paginator
        currentPage={currentPage}
        itemsPerPage={itemsPerPage}
        onPageChanged={onPageChangedHandler}
        totalCount={total}
      />
    </>
  )
}
