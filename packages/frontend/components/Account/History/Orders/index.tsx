import type { FC } from "react"
import React from "react"
import { OrderList } from "./OrderList"
import {
  INITIAL_PAGE,
  INITIAL_PER_PAGE,
} from "../../../../store/reducers/accountSlice"
import { Filters } from "./Filters"
import { ProductList } from "./ProductList"
import { useFilterHistoryOrders } from "../../../../hooks/filterHistoryOrders"
import { HistoryOrdersPageRecommends } from "../../../LeadHit"

export type ViewModeType = "products" | "orders"

export const HistoryOrders: FC = () => {
  const { routerQuery, viewMode } = useFilterHistoryOrders()
  return (
    <>
      <Filters />
      {viewMode === "orders" && (
        <>
          <OrderList
            requestData={
              viewMode === "orders"
                ? {
                    page:
                      routerQuery.page !== undefined
                        ? +routerQuery.page
                        : INITIAL_PAGE,
                    perPage:
                      routerQuery.perPage !== undefined
                        ? +routerQuery.perPage
                        : INITIAL_PER_PAGE,
                    payer: routerQuery.payer,
                    year:
                      routerQuery.year !== undefined
                        ? +routerQuery.year
                        : undefined,
                    state: routerQuery.state,
                  }
                : null
            }
          />
        </>
      )}
      {viewMode === "products" && (
        <>
          <ProductList
            requestData={
              viewMode === "products"
                ? {
                    page:
                      routerQuery.page !== undefined
                        ? +routerQuery.page
                        : INITIAL_PAGE,
                    perPage:
                      routerQuery.perPage !== undefined
                        ? +routerQuery.perPage
                        : INITIAL_PER_PAGE,
                    payer: routerQuery.payer,
                    year:
                      routerQuery.year !== undefined
                        ? +routerQuery.year
                        : undefined,
                    state: routerQuery.state,
                    category: routerQuery.category,
                  }
                : null
            }
          />
        </>
      )}
      <HistoryOrdersPageRecommends />
    </>
  )
}
