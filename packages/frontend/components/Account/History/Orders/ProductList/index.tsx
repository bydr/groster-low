import { FC, useEffect } from "react"
import {
  RequestOrderListProducts,
  VIEW_PRODUCTS_LIST,
} from "../../../../../types/types"
import { orderAPI } from "../../../../../api/orderAPI"
import { productsAPI } from "../../../../../api/productsAPI"
import { StyledProductsList } from "../StyledHistoryOrders"
import { Products } from "../../../../Products/Products"
import { BaseLoader } from "../../../../Loaders/BaseLoader/BaseLoader"
import { Product } from "../../../../Products/Catalog/Product/Product"
import { useAppDispatch, useAppSelector } from "../../../../../hooks/redux"
import { accountSlice } from "../../../../../store/reducers/accountSlice"
import { Paginator } from "../../../../Paginator/Paginator"
import { useFilterHistoryOrders } from "../../../../../hooks/filterHistoryOrders"
import { Typography } from "../../../../Typography/Typography"

export const ProductList: FC<{
  requestData: RequestOrderListProducts | null
}> = ({ requestData }) => {
  const dispatch = useAppDispatch()
  const products = useAppSelector(
    (state) => state.profile.history.products.items,
  )
  const total = useAppSelector((state) => state.profile.history.products.total)
  const currentPage = useAppSelector((state) => state.profile.history.page)
  const itemsPerPage = useAppSelector(
    (state) => state.profile.history.itemsPerPage,
  )
  const { setTotalProducts, setCurrentPage, setHistoryProducts } =
    accountSlice.actions
  const { updateFilterQuery } = useFilterHistoryOrders()

  const { data: dataProductsIds, isLoading: isFetchingProductsIds } =
    orderAPI.useFetchOrderListProducts(requestData)

  const { data: dataProducts, isLoading: isFetchingProducts } =
    productsAPI.useProductsFetch(
      !!dataProductsIds?.products ? dataProductsIds.products.join(",") : null,
    )

  const onPageChangedHandler = (page: number) => {
    dispatch(setCurrentPage(page))
    updateFilterQuery({
      page: page,
    })
  }

  useEffect(() => {
    if (!!dataProducts && !!dataProductsIds?.products?.length) {
      dispatch(
        setHistoryProducts({
          products: dataProducts || null,
          queueKeys: dataProductsIds?.products || [],
        }),
      )
    }
  }, [dataProducts, dataProductsIds, dispatch, setHistoryProducts])

  useEffect(() => {
    dispatch(setTotalProducts(dataProductsIds?.total || 0))
  }, [dataProductsIds?.total, setTotalProducts, dispatch])

  return (
    <>
      <StyledProductsList>
        {(isFetchingProductsIds || isFetchingProducts) && <BaseLoader />}
        {products !== null && (
          <>
            {Object.keys(products).length > 0 ? (
              <>
                <Products view={VIEW_PRODUCTS_LIST}>
                  {Object.keys(products).map((p) => (
                    <Product
                      product={products[p]}
                      view={VIEW_PRODUCTS_LIST}
                      key={p}
                    />
                  ))}
                </Products>
              </>
            ) : (
              <>
                <Typography variant={"p14"}>Заказов не найдено</Typography>
              </>
            )}
          </>
        )}
        <Paginator
          currentPage={currentPage}
          itemsPerPage={itemsPerPage}
          onPageChanged={onPageChangedHandler}
          totalCount={total}
        />
      </StyledProductsList>
    </>
  )
}
