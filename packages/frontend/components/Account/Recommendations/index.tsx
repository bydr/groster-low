import type { FC } from "react"
import React from "react"
import { VIEW_PRODUCTS_GRID } from "../../../types/types"
import { Typography } from "../../Typography/Typography"
import { BaseLoader } from "../../Loaders/BaseLoader/BaseLoader"
import { Products } from "../../Products/Products"
import { StyledWaitingContainer } from "../Waiting/Styled"
import { Product } from "../../Products/Catalog/Product/Product"
import { useRecommendations } from "../../../hooks/recommendations"
import { useAuth } from "../../../hooks/auth"

export const Recommendations: FC = () => {
  const { isAuth } = useAuth()
  const { isLoading, products } = useRecommendations({
    isAuth: isAuth,
  })

  return (
    <>
      <>
        <StyledWaitingContainer>
          {isLoading && <BaseLoader />}
          <Products view={VIEW_PRODUCTS_GRID}>
            {!!products && products.length > 0 ? (
              <>
                {products.map((p) => (
                  <Product
                    product={p}
                    view={VIEW_PRODUCTS_GRID}
                    key={p.uuid || ""}
                  />
                ))}
              </>
            ) : (
              <>
                {!isLoading && (
                  <>
                    <Typography variant={"p14"}>
                      Здесь будут товары, рекомендованные специально для Вас
                    </Typography>
                  </>
                )}
              </>
            )}
          </Products>
        </StyledWaitingContainer>
      </>
    </>
  )
}
