import { styled } from "@linaria/react"
import {
  StyledEntity,
  StyledEntityContainer,
  StyledEntityList,
} from "../StyledAccount"
import {
  StyledRadioBox,
  StyledRadioInner,
  StyledRadioMessage,
} from "../../Radio/StyledRadio"
import { colors } from "../../../styles/utils/vars"
import { getTypographyBase } from "../../Typography/Typography"

export const StyledAddresses = styled(StyledEntityContainer)``
export const StyledAddressesList = styled(StyledEntityList)``
export const StyledAddress = styled(StyledEntity)`
  ${StyledRadioBox} {
    width: 17px;
    height: 17px;
    min-height: 17px;
    min-width: 17px;
    border-radius: 2px;

    &:before {
      width: 7px;
      height: 7px;
      min-height: 7px;
      min-width: 7px;
    }
  }

  ${StyledRadioMessage} {
    ${getTypographyBase("p12")};
    color: ${colors.black};
  }

  ${StyledRadioInner} {
    line-height: 100%;
  }
`
