import { FC, useContext, useEffect, useState } from "react"
import { Controller, useForm } from "react-hook-form"
import { ModalContext } from "../../Modals/Modal"
import { useMutation } from "react-query"
import { fetchAddressCreate, fetchAddressEdit } from "../../../api/accountAPI"
import { AddressType, ApiError } from "../../../../contracts/contracts"
import { FormGroup, StyledForm } from "../../Forms/StyledForms"
import { Field } from "../../Field/Field"
import { SingleError, Typography } from "../../Typography/Typography"
import { Button } from "../../Button"
import { fetchSuggestAddresses } from "../../../api/dadataAPI"
import { useDebounce } from "../../../hooks/debounce"
import {
  cssListVertical,
  StyledList,
  StyledListItem,
} from "../../List/StyledList"
import { StyledHints } from "../../Hints/StyledHints"
import { useAppSelector } from "../../../hooks/redux"

type AddressesFormFieldsType = AddressType

export const AddressesForm: FC<{
  address?: AddressType
  onSuccess?: (uid: string, responseAddress: AddressType) => void
}> = ({ onSuccess, address }) => {
  const [isFetching, setIsFetching] = useState<boolean>(false)
  const [error, setError] = useState<string | null>(null)
  const [mode, setMode] = useState<"edit" | "create">(
    address !== undefined ? "edit" : "create",
  )

  const {
    handleSubmit,
    control,
    reset,
    setValue,
    formState: { errors, isDirty },
    register,
  } = useForm<AddressesFormFieldsType>({
    defaultValues: {
      name: address?.name || "",
      address: address?.address || "",
      comment: address?.comment || "",
    },
    mode: "onChange",
  })

  const modalContext = useContext(ModalContext)

  useEffect(() => {
    setMode(address !== undefined ? "edit" : "create")
    reset()
  }, [address, reset])

  const { mutate: createMutate } = useMutation(fetchAddressCreate, {
    onMutate: () => {
      setIsFetching(true)
    },
    onSuccess: (response, request) => {
      reset({
        address: "",
        name: "",
        comment: "",
      })
      setIsFetching(false)
      if (onSuccess) {
        onSuccess(response.uid, {
          uid: response.uid,
          address: request.address,
          name: request.name,
          comment: request.comment,
        })
      }
      modalContext?.hide()
    },
    onError: (error: ApiError) => {
      setError(error.message)
      setIsFetching(false)
    },
  })

  const { mutate: editMutate } = useMutation(fetchAddressEdit, {
    onMutate: () => {
      setIsFetching(true)
    },
    onSuccess: (response, request) => {
      reset({
        address: request.address,
        name: request.name,
        comment: request.comment,
      })
      setIsFetching(false)
      if (onSuccess) {
        onSuccess(response.uid, {
          uid: response.uid,
          address: request.address,
          name: request.name,
        })
      }
      modalContext?.hide()
    },
    onError: (error: ApiError) => {
      setError(error.message)
      setIsFetching(false)
    },
  })

  const [selectedAddress, setSelectedAddress] = useState<string | undefined>()
  const [inputAddress, setInputAddress] = useState<string | undefined>()
  const debouncedAddress = useDebounce<string>(inputAddress || "")
  const [hints, setHints] = useState<string[] | null>(null)
  const [isVisibleHints, setIsVisibleHints] = useState<boolean>(false)

  const location = useAppSelector((state) => state.app.location)

  const { mutate: suggestAddressesMutate, reset: suggestAddressesReset } =
    useMutation(fetchSuggestAddresses, {
      onSuccess: (data) => {
        if (!!data && !!data.suggestions) {
          setHints([
            ...data.suggestions
              .map((item) => item.unrestricted_value)
              .filter((val) => val !== null && val !== undefined),
          ] as string[])
          setIsVisibleHints(true)
        }
      },
      onError: (error) => {
        console.log("error address ", error)
      },
    })

  useEffect(() => {
    const isEmptyAddr = !((debouncedAddress || "").length > 0)

    if (isEmptyAddr) {
      return
    }

    const kladrId: string | null = !!location?.region_kladr_id
      ? location.region_kladr_id || ""
      : null

    suggestAddressesMutate({
      address: debouncedAddress,
      kladrId: kladrId !== null ? [kladrId] : undefined,
    })
  }, [debouncedAddress, location?.region_kladr_id, suggestAddressesMutate])

  useEffect(() => {
    return () => {
      suggestAddressesReset()
      setInputAddress(undefined)
      setSelectedAddress(undefined)
      setHints(null)
    }
  }, [suggestAddressesReset])

  const onSubmit = handleSubmit((data) => {
    if (mode === "edit") {
      if (!!address?.uid) {
        editMutate({
          name: data.name,
          address: data.address,
          uid: address?.uid,
          comment: data.comment,
        })
      }
    }

    if (mode === "create") {
      createMutate({
        name: data.name || "",
        address: data.address || "",
        comment: data.comment,
      })
    }
  })

  return (
    <>
      <StyledForm onSubmit={onSubmit}>
        <FormGroup>
          <Field
            type={"text"}
            placeholder={"Адрес"}
            withAnimatingLabel
            hint={"Введите город, улицу и дом"}
            errorMessage={errors.address?.message}
            variant={"input"}
            selectedValue={selectedAddress}
            isVisibleSelectedHint={true}
            {...register("address", {
              required: {
                value: true,
                message: "Поле обязательно для заполнения",
              },
              validate: (value) => {
                return (
                  (hints || []).includes(value || "") ||
                  "Выберите адрес из списка"
                )
              },
              onChange: (e) => {
                setInputAddress(e.currentTarget.value)
                setSelectedAddress(undefined)
              },
            })}
          />

          {hints !== null && isVisibleHints && (
            <>
              {hints.length > 0 ? (
                <>
                  <StyledHints>
                    <StyledList className={cssListVertical}>
                      {hints.map((h, i) => (
                        <StyledListItem
                          key={i}
                          onClick={() => {
                            setSelectedAddress(h)
                            setValue("address", h, {
                              shouldValidate: true,
                              shouldDirty: true,
                            })
                            setIsVisibleHints(false)
                          }}
                        >
                          {h}
                        </StyledListItem>
                      ))}
                    </StyledList>
                  </StyledHints>
                </>
              ) : (
                <>
                  <StyledHints>
                    <Typography variant={"p14"}>
                      Результатов не найдено
                    </Typography>
                  </StyledHints>
                </>
              )}
            </>
          )}
        </FormGroup>

        <FormGroup>
          <Controller
            name={"name"}
            control={control}
            rules={{
              required: {
                value: true,
                message: "Заполните это поле",
              },
            }}
            render={({ field, fieldState }) => (
              <Field
                type={"text"}
                value={field.value}
                placeholder={"Название"}
                onChange={field.onChange}
                errorMessage={fieldState.error?.message}
                withAnimatingLabel
              />
            )}
          />
        </FormGroup>

        <FormGroup>
          <Controller
            name={"comment"}
            control={control}
            render={({ field, fieldState }) => (
              <Field
                variant={"textarea"}
                type={"text"}
                value={field.value}
                placeholder={"Комментарий для курьера"}
                hint={
                  "Например, как вас найти, название бренда, вывески, необходимость пропуска для курьера и тп."
                }
                onChange={field.onChange}
                errorMessage={fieldState.error?.message}
                withAnimatingLabel
              />
            )}
          />
        </FormGroup>

        {error !== null && <SingleError>{error}</SingleError>}

        <Button
          type={"button"}
          variant={"filled"}
          size={"large"}
          disabled={Object.keys(errors).length > 0 || !isDirty}
          isFetching={isFetching}
          onClick={() => {
            void onSubmit()
          }}
        >
          {mode === "edit" && "Сохранить"}
          {mode === "create" && "Добавить"}
        </Button>
      </StyledForm>
    </>
  )
}
