import type { FC } from "react"
import { useEffect } from "react"
import { Typography } from "../../Typography/Typography"
import { StyledAddresses, StyledAddressesList } from "./Styled"
import { BaseLoader } from "../../Loaders/BaseLoader/BaseLoader"
import { Address } from "./Address"
import Button from "../../Button/Button"
import { AddressesForm } from "./AddressesForm"
import { Modal } from "../../Modals/Modal"
import { useAddresses } from "../../../hooks/addresses"

export const Addresses: FC = () => {
  const {
    addresses,
    refetch: refetchAddresses,
    isFetching,
    setDefaultAddress,
    isFetchingDefaulted,
  } = useAddresses()

  useEffect(() => {
    if (addresses === null) {
      refetchAddresses()
    }
  }, [addresses, refetchAddresses])

  return (
    <>
      <StyledAddresses>
        {isFetching && <BaseLoader />}

        <Modal
          variant={"rounded-50"}
          title={"Добавить адрес"}
          closeMode={"destroy"}
          disclosure={
            <Button
              iconPosition={"left"}
              icon={"PlusCircle"}
              variant={"link"}
              withOutOffset
            >
              Добавить адрес
            </Button>
          }
        >
          <AddressesForm
            onSuccess={() => {
              refetchAddresses()
            }}
          />
        </Modal>
        {addresses !== null && (
          <>
            {addresses.length > 0 ? (
              <>
                <StyledAddressesList>
                  {addresses.map((addr) => (
                    <Address
                      address={addr}
                      key={addr.uid || ""}
                      updateAddresses={() => {
                        refetchAddresses()
                      }}
                      isFetchingDefaulted={isFetchingDefaulted}
                      setAsDefault={setDefaultAddress}
                    />
                  ))}
                </StyledAddressesList>
              </>
            ) : (
              <>
                <Typography variant={"p14"}>
                  Пока нет ни одного сохранённого адреса.
                </Typography>
              </>
            )}
          </>
        )}
      </StyledAddresses>
    </>
  )
}
