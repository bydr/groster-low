import type { FC } from "react"
import { Panel, PanelHeading } from "../../../../styles/utils/StyledPanel"
import { Typography } from "../../../Typography/Typography"
import { Button } from "../../../Button"
import { Modal } from "../../../Modals/Modal"
import { AddressType } from "../../../../../contracts/contracts"
import { AddressesForm } from "../AddressesForm"
import { StyledAddress } from "../Styled"
import { CustomRadioGroup } from "../../../Radio/CustomRadioGroup"

export const Address: FC<{
  address: AddressType
  updateAddresses?: () => void
  setAsDefault?: (uid: string) => void
  isFetchingDefaulted?: boolean
}> = ({ address, updateAddresses, isFetchingDefaulted, setAsDefault }) => {
  return (
    <>
      <StyledAddress>
        <Panel>
          <PanelHeading>
            <Typography variant={"span"}>{address.name}</Typography>
            <Modal
              variant={"rounded-50"}
              title={"Редактировать адрес"}
              closeMode={"destroy"}
              disclosure={<Button variant={"box"} icon={"Edit"} />}
            >
              <AddressesForm
                address={address}
                onSuccess={() => {
                  if (updateAddresses) {
                    updateAddresses()
                  }
                }}
              />
            </Modal>
          </PanelHeading>
          <Typography variant={"p12"}>{address.address}</Typography>
          <CustomRadioGroup
            items={[
              {
                message: "Адрес по умолчанию",
                value: address.uid || "",
              },
            ]}
            variant={"rounds"}
            indexCurrent={address?.is_default ? 0 : null}
            isFetching={isFetchingDefaulted}
            onChange={(value) => {
              if (setAsDefault && !!value) {
                if (!address?.is_default) {
                  setAsDefault(`${value}`)
                }
              }
            }}
          />
        </Panel>
      </StyledAddress>
    </>
  )
}
