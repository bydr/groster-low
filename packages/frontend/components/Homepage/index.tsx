import { FC, useEffect } from "react"
import { SliderBanners } from "../Banners/Slider"
import { cssHiddenMD, cssSectionUnderHeader } from "../../styles/utils/Utils"
import { SingleHome } from "../Banners/SingleHome"
import { MainPageRecommends } from "../LeadHit"
import { useApp } from "../../hooks/app"
import dynamic, { DynamicOptions } from "next/dynamic"
import { BusinessAreasBannersPropsType } from "../BusinessAreasBanners"
import { HitsPropsType } from "./Hits"
import { BannerApiType, ProductCatalogType } from "../../../contracts/contracts"
import { CatalogResponseType } from "../../types/types"
import { NewPropsType } from "./New"

const DynamicPersonalRecommendations = dynamic((() =>
  import("./PersonalRecommendations").then(
    (mod) => mod.PersonalRecommendations,
  )) as DynamicOptions)

const DynamicBusinessAreasBanners = dynamic((() =>
  import("../../components/BusinessAreasBanners").then(
    (mod) => mod.BusinessAreasBanners,
  )) as DynamicOptions<BusinessAreasBannersPropsType>)

const DynamicBrandBasis = dynamic((() =>
  import("../../components/BrandBasis").then(
    (mod) => mod.BrandBasis,
  )) as DynamicOptions)

const DynamicHits = dynamic((() =>
  import("./Hits").then((mod) => mod.Hits)) as DynamicOptions<HitsPropsType>)

const DynamicNew = dynamic((() =>
  import("./New").then((mod) => mod.New)) as DynamicOptions<NewPropsType>)

export type SSRPropsType = {
  banners: BannerApiType[]
  hits: ProductCatalogType[]
  new: CatalogResponseType | null
}

export type HomepagePropsType = SSRPropsType

export const Homepage: FC<HomepagePropsType> = ({
  banners: bannersSSR,
  hits: hitsSSR,
  new: newSSR,
}) => {
  const { banners, updateBanners } = useApp()

  useEffect(() => {
    if (banners === null) {
      updateBanners(bannersSSR)
    }
  }, [bannersSSR, updateBanners, banners])

  return (
    <>
      <SliderBanners
        banners={banners !== null ? banners.main_slider : undefined}
        className={cssSectionUnderHeader}
      />
      <DynamicHits initial={hitsSSR} />
      <DynamicNew initial={newSSR || undefined} />
      <DynamicBusinessAreasBanners className={cssHiddenMD} />
      <DynamicPersonalRecommendations />
      <SingleHome
        banner={banners !== null ? banners.main_single[0] : undefined}
      />
      <MainPageRecommends />
      <DynamicBrandBasis />
    </>
  )
}
