import { FC } from "react"
import { Products } from "../../Products/Products"
import { CatalogResponseType, VIEW_PRODUCTS_GRID } from "../../../types/types"
import { Product } from "../../Products/Catalog/Product/Product"
import { fetchCatalog } from "../../../api/catalogAPI"
import { StyledTileProducts } from "../../Products/StyledProducts"
import { Container } from "../../../styles/utils/StyledGrid"
import { SectionProducts, SectionTitle } from "../../../styles/utils/Utils"
import { useQuery } from "react-query"

export type NewPropsType = {
  initial?: CatalogResponseType
}

export const New: FC<NewPropsType> = ({ initial }) => {
  const { data: dataNew } = useQuery(
    ["new"],
    () =>
      fetchCatalog({
        isEnabled: "1",
        perPage: 20,
        isNew: true,
      }),
    {
      initialData: initial,
    },
  )

  return (
    <>
      {!!dataNew?.products && dataNew?.products.length > 0 && (
        <SectionProducts>
          <Container>
            <SectionTitle>Новинки</SectionTitle>
            <StyledTileProducts>
              <Products view={VIEW_PRODUCTS_GRID}>
                {dataNew.products.map((p) => (
                  <Product
                    product={p}
                    view={VIEW_PRODUCTS_GRID}
                    key={p.uuid || ""}
                  />
                ))}
              </Products>
            </StyledTileProducts>
          </Container>
        </SectionProducts>
      )}
    </>
  )
}
