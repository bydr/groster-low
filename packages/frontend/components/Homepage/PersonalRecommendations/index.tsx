import { FC } from "react"
import { SectionProducts, SectionTitle } from "../../../styles/utils/Utils"
import { Container } from "../../../styles/utils/StyledGrid"
import { useAuth } from "../../../hooks/auth"
import { Products } from "../../Products/Products"
import { VIEW_PRODUCTS_GRID } from "../../../types/types"
import { Product } from "../../Products/Catalog/Product/Product"
import { StyledTileProducts } from "../../Products/StyledProducts"
import { useRecommendations } from "../../../hooks/recommendations"

export const PersonalRecommendations: FC = () => {
  const { isAuth } = useAuth()
  const { products } = useRecommendations({
    limit: 10,
    isAuth,
  })

  return (
    <>
      {isAuth && !!products?.length && (
        <SectionProducts>
          <Container>
            <SectionTitle>Персональные рекомендации</SectionTitle>
            <StyledTileProducts>
              <Products view={VIEW_PRODUCTS_GRID}>
                {products.map((p) => (
                  <Product
                    product={p}
                    view={VIEW_PRODUCTS_GRID}
                    key={p.uuid || ""}
                  />
                ))}
              </Products>
            </StyledTileProducts>
          </Container>
        </SectionProducts>
      )}
    </>
  )
}
