import { FC, useEffect, useState } from "react"
import { Products } from "../../Products/Products"
import { VIEW_PRODUCTS_GRID } from "../../../types/types"
import { Product } from "../../Products/Catalog/Product/Product"
import { fetchHits } from "../../../api/catalogAPI"
import { StyledTileProducts } from "../../Products/StyledProducts"
import { Container } from "../../../styles/utils/StyledGrid"
import { SectionProducts, SectionTitle } from "../../../styles/utils/Utils"
import { ProductCatalogType } from "../../../../contracts/contracts"
import { useMutation } from "react-query"

export type HitsPropsType = { initial?: ProductCatalogType[] }

export const Hits: FC<HitsPropsType> = ({ initial }) => {
  const [products, setProducts] = useState<ProductCatalogType[] | null>(
    initial || null,
  )
  const { mutate: hitsMutate } = useMutation(() => fetchHits(), {
    onSuccess: (data) => {
      setProducts(data || [])
    },
  })

  useEffect(() => {
    if (initial === undefined) {
      hitsMutate()
    } else {
      setProducts(initial || [])
    }
  }, [hitsMutate, initial])

  return (
    <>
      {products !== null && products.length > 0 && (
        <SectionProducts>
          <Container>
            <SectionTitle>Хиты</SectionTitle>
            <StyledTileProducts>
              <Products view={VIEW_PRODUCTS_GRID}>
                {products.map((p) => (
                  <Product
                    product={p}
                    view={VIEW_PRODUCTS_GRID}
                    key={p.uuid || ""}
                  />
                ))}
              </Products>
            </StyledTileProducts>
          </Container>
        </SectionProducts>
      )}
    </>
  )
}
