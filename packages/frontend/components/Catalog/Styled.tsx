import { styled } from "@linaria/react"
import { StyledLoaderContainer } from "../Loaders/BaseLoader/StyledBaseLoader"

export const StyledSeoContainer = styled.div`
  width: 100%;
  position: relative;
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  justify-content: flex-start;
  margin-bottom: 20px;
`

export const StyledCatalog = styled.section`
  width: 100%;
  position: relative;

  > ${StyledLoaderContainer} {
    position: fixed;
  }
`
