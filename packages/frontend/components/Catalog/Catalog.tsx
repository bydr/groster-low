import type { FC } from "react"
import { useCallback, useEffect, useMemo, useState } from "react"
import {
  CatalogResponseType,
  QueryCatalogType,
  TagType,
  VIEW_PRODUCTS_GRID,
  ViewProductsType,
} from "../../types/types"
import { Paginator } from "../Paginator/Paginator"
import { CatalogProducts } from "../Products/Catalog/CatalogProducts"
import { useAppDispatch, useAppSelector } from "../../hooks/redux"
import {
  catalogSlice,
  INITIAL_PER_PAGE,
  INITIAL_PER_PAGE_WITH_BANNER,
} from "../../store/reducers/catalogSlice"
import { useFilterCatalog } from "../../hooks/filterCatalog"
import { CatalogFilters } from "./Filters"
import { useQuery } from "react-query"
import {
  fetchFiltersParams,
  fetchSortTypes,
  fetchTagById,
  fetchTags,
} from "../../api/catalogAPI"
import { useRouter } from "next/router"
import { TagResponse } from "../../../contracts/contracts"
import { Typography } from "../Typography/Typography"
import { StyledCatalog, StyledSeoContainer } from "./Styled"
import { ROUTES } from "../../utils/constants"

export const Catalog: FC<{
  initialCatalog?: CatalogResponseType
  filteredCatalog?: CatalogResponseType
  isBannersProductsCatalog?: boolean
}> = ({
  initialCatalog: dataInitCatalog,
  filteredCatalog: dataFilterCatalog,
  isBannersProductsCatalog,
}) => {
  const [viewProducts, setViewProducts] =
    useState<ViewProductsType>(VIEW_PRODUCTS_GRID)
  const dispatch = useAppDispatch()
  const {
    setCurrentPage,
    setTogglePageMethod,
    setProducts,
    setTotalCount,
    setTags,
    setFilterParams,
    updateCheckedParams,
    updateActualParams,
    setInitialPriceRange,
    setCheckedPriceRange,
    setIsEnabled,
    setSortBy,
    setItemsPerPage,
    setIsFast,
    setAvailableParams,
    setSortBySelected,
    setCheckedParamsKeysTotal,
    setCurrentTagId,
    setCurrentTagPayload,
    setFilterStores,
    updateCheckedStores,
  } = catalogSlice.actions

  const { query: queryInitial, ...router } = useRouter()
  const query: QueryCatalogType | undefined = useMemo(
    () => queryInitial,
    [queryInitial],
  )

  const currentCategory = useAppSelector(
    (state) => state.catalog.currentCategory,
  )
  const products = useAppSelector((state) => state.catalog.products)
  const totalCount = useAppSelector((state) => state.catalog.total)
  const currentPage = useAppSelector((state) => state.catalog.page)
  const itemsPerPage = useAppSelector((state) => state.catalog.itemsPerPage)
  const tags = useAppSelector((state) => state.catalog.tags)
  const currentTag = useAppSelector((state) => state.catalog.currentTag)

  const { updateFilterQuery, sortBy, params } = useFilterCatalog()

  const onPageChangedHandler = (page: number) => {
    dispatch(setCurrentPage(page))
    updateFilterQuery({
      page: page,
      perPage: isBannersProductsCatalog
        ? INITIAL_PER_PAGE_WITH_BANNER
        : INITIAL_PER_PAGE,
    })
  }
  const onAdditionalItemsHandler = (page: number) => {
    dispatch(setCurrentPage(page))
    dispatch(setTogglePageMethod("additional"))
    updateFilterQuery({
      page: page,
      isScroll: false,
      perPage: INITIAL_PER_PAGE,
    })
  }

  useQuery("filterParams", () => fetchFiltersParams(), {
    initialData: Object.keys(params).map((k) => params[k]),
    onSuccess: (response) => {
      dispatch(setFilterParams(response || []))
    },
  })

  useQuery("sortTypes", () => fetchSortTypes(), {
    onSuccess: (response) => {
      dispatch(setSortBy(response || []))
    },
  })

  const { data: dataTags } = useQuery("tags", () => fetchTags())

  useQuery(
    ["useTagById", currentTag.id],
    () => (currentTag.id ? fetchTagById(currentTag.id) : null),
    {
      onSuccess: (response) => {
        dispatch(
          setCurrentTagPayload(
            !!response
              ? {
                  h1: response.h1,
                  name: response.name,
                  meta_description: response.meta_description,
                  meta_title: response.meta_title,
                  text: response.text,
                  url: response.url,
                }
              : null,
          ),
        )
      },
    },
  )

  const shops = useAppSelector((state) => state.shop.shops)

  const updateInitialPrice = useCallback(() => {
    let min
    let max
    if (!!query.price) {
      if (dataInitCatalog?.priceRange !== undefined) {
        min = dataInitCatalog?.priceRange?.min || 0
        max = dataInitCatalog?.priceRange?.max || 0
      }
    } else {
      if (dataFilterCatalog?.priceRange !== undefined) {
        min = dataFilterCatalog?.priceRange?.min || 0
        max = dataFilterCatalog?.priceRange?.max || 0
      }
    }
    if (min !== undefined && max !== undefined) {
      dispatch(setInitialPriceRange({ min, max }))
    }
  }, [
    dataFilterCatalog?.priceRange,
    dataInitCatalog?.priceRange,
    dispatch,
    query.price,
    setInitialPriceRange,
  ])

  const updateCheckedPrice = useCallback(() => {
    if (!!query.price && !!query.price.length) {
      const price: string[] = query.price?.split("-")
      dispatch(setCheckedPriceRange({ min: +price[0], max: +price[1] }))
    } else {
      dispatch(setCheckedPriceRange(null))
    }
  }, [query.price, dispatch, setCheckedPriceRange])

  const updateAvailableParams = useCallback(() => {
    if (!!query.params) {
      const defCatParams = { ...(dataInitCatalog?.params || {}) }
      Object.keys(defCatParams).map((key) => {
        defCatParams[key] = 0
      })
      dispatch(
        setAvailableParams({
          compatibleParams: {
            ...defCatParams,
            ...(dataFilterCatalog?.params || {}),
          },
          queryParams: query.params.split(","),
          initialParams: dataInitCatalog?.params,
        }),
      )
    } else {
      if (!!dataFilterCatalog?.params) {
        dispatch(
          setAvailableParams({
            compatibleParams: dataFilterCatalog?.params,
          }),
        )
      }
    }
  }, [
    dataFilterCatalog?.params,
    dataInitCatalog?.params,
    dispatch,
    query.params,
    setAvailableParams,
  ])

  const updateTagsCategory = useCallback(() => {
    if (!!currentCategory && !!currentCategory.tags) {
      const tagsCategory: TagResponse = []
      for (const tNum of currentCategory.tags) {
        const found: TagType | undefined = dataTags?.find(
          (tag) => tag.id === tNum,
        )
        if (!!found) {
          tagsCategory.push(found)
        }
      }
      dispatch(setTags(tagsCategory))
    } else {
      dispatch(setTags([]))
    }
  }, [currentCategory, dispatch, setTags, dataTags])

  const updateCurrentTagId = useCallback(() => {
    const path = router.asPath
    dispatch(
      setCurrentTagId(
        tags.find((tag) => {
          let result = false
          if (tag.url !== undefined) {
            if (
              ["https://", "http://"].some((item) => tag.url?.includes(item))
            ) {
              result = path.includes(
                tag.url?.substring(
                  tag.url.indexOf(ROUTES.catalog),
                  tag.url.length,
                ),
              )
            } else {
              result = path.includes(tag.url)
            }
          }
          return result
        })?.id || null,
      ),
    )
  }, [router.asPath, dispatch, setCurrentTagId, tags])

  //set KitPartProducts to state for current category
  useEffect(() => {
    if (!!dataFilterCatalog?.products && !!dataFilterCatalog?.products.length) {
      dispatch(setProducts(dataFilterCatalog.products))
      dispatch(setTogglePageMethod("switch"))
    } else {
      dispatch(setProducts([]))
    }
  }, [dataFilterCatalog?.products, dispatch, setProducts, setTogglePageMethod])

  useEffect(() => {
    if (shops === null) {
      return
    }
    dispatch(setFilterStores(shops))
  }, [shops, dispatch, setFilterStores])

  useEffect(() => {
    updateInitialPrice()
  }, [updateInitialPrice])

  useEffect(() => {
    updateCheckedPrice()
  }, [updateCheckedPrice])

  useEffect(() => {
    updateAvailableParams()
  }, [updateAvailableParams, params])

  useEffect(() => {
    if (!!query.params) {
      dispatch(
        updateCheckedParams({
          keyFilter: query.params.split(","),
          checked: true,
        }),
      )
    } else {
      dispatch(setCheckedParamsKeysTotal([]))
    }
  }, [
    dispatch,
    query.params,
    updateCheckedParams,
    dataFilterCatalog?.params,
    dataInitCatalog?.params,
    setCheckedParamsKeysTotal,
  ])

  useEffect(() => {
    // console.log("updateCheckedStores")
    if (!!query.store && shops !== null) {
      dispatch(
        updateCheckedStores({
          keyFilter: query.store.split(","),
          checked: true,
        }),
      )
    }
  }, [dispatch, query.store, updateCheckedStores, shops])

  useEffect(() => {
    if (dataFilterCatalog?.total !== undefined) {
      dispatch(setTotalCount(dataFilterCatalog.total || 0))
      if (!query.page) {
        dispatch(setCurrentPage(1))
      }
    }
  }, [
    dispatch,
    dataFilterCatalog?.total,
    setTotalCount,
    query.page,
    setCurrentPage,
  ])

  useEffect(() => {
    if (!!query.per_page) {
      dispatch(setItemsPerPage(+query.per_page))
    } else {
      dispatch(
        setItemsPerPage(
          isBannersProductsCatalog
            ? INITIAL_PER_PAGE_WITH_BANNER
            : INITIAL_PER_PAGE,
        ),
      )
    }
  }, [dispatch, setItemsPerPage, query.per_page, isBannersProductsCatalog])

  useEffect(() => {
    if (!!query.page) {
      dispatch(setCurrentPage(+query.page))
    }
  }, [dispatch, setCurrentPage, query.page])

  useEffect(() => {
    if (!!query.sortby) {
      dispatch(setSortBySelected(query.sortby))
    } else {
      if (!!sortBy) {
        dispatch(setSortBySelected(Object.keys(sortBy)[0]))
      }
    }
  }, [dispatch, query.sortby, setSortBySelected, sortBy])

  useEffect(() => {
    dispatch(setIsEnabled(query.is_enabled !== "0"))
  }, [dispatch, query.is_enabled, setIsEnabled])

  useEffect(() => {
    dispatch(
      setIsFast(query.is_fast === undefined ? false : query.is_fast !== "0"),
    )
  }, [dispatch, query.is_fast, setIsFast])

  useEffect(() => {
    updateTagsCategory()
  }, [updateTagsCategory])

  useEffect(() => {
    if (dataFilterCatalog?.params !== undefined && query.params !== undefined) {
      dispatch(
        updateActualParams({
          fetchedParams: dataFilterCatalog?.params,
        }),
      )
    }
  }, [dataFilterCatalog?.params, dispatch, updateActualParams, query.params])

  useEffect(() => {
    updateCurrentTagId()
  }, [updateCurrentTagId])

  useEffect(() => {
    return function cleanup() {
      dispatch(setAvailableParams(null))
      dispatch(setCheckedParamsKeysTotal([]))
      dispatch(setCheckedPriceRange(null))
      dispatch(setCurrentTagPayload(null))
      dispatch(setCurrentTagId(null))
    }
  }, [
    dispatch,
    setAvailableParams,
    setCheckedParamsKeysTotal,
    setCheckedPriceRange,
    setCurrentPage,
    setTotalCount,
    setCurrentTagPayload,
    setCurrentTagId,
  ])

  return (
    <>
      <StyledCatalog>
        <CatalogFilters
          viewProducts={viewProducts}
          updateViewProducts={setViewProducts}
        />
        <CatalogProducts
          view={viewProducts}
          products={products}
          page={currentPage}
        >
          <Paginator
            currentPage={currentPage}
            itemsPerPage={itemsPerPage || 0}
            onPageChanged={onPageChangedHandler}
            onPageAdditional={onAdditionalItemsHandler}
            totalCount={totalCount}
          />
        </CatalogProducts>

        {!!currentTag.payload?.text && (
          <StyledSeoContainer>
            <Typography variant={"p14"}>{currentTag.payload?.text}</Typography>
          </StyledSeoContainer>
        )}
      </StyledCatalog>
    </>
  )
}
