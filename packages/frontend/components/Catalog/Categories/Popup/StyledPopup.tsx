import { styled } from "@linaria/react"
import {
  breakpoints,
  transitionTimingFunction,
} from "../../../../styles/utils/vars"
import { SidebarInner, StyledSidebar } from "../../../Sidebar/StyledSidebar"
import {
  cssIsActive,
  getCustomizeScroll,
  StyledUtilOverlay,
} from "../../../../styles/utils/Utils"
import { StyledLoaderOverlay } from "../../../Loaders/BaseLoader/StyledBaseLoader"
import { StyledSpin } from "../../../Loaders/Spin/StyledSpin"
import { Container, Row } from "../../../../styles/utils/StyledGrid"
import { css } from "@linaria/core"
import { StyledCategories } from "../StyledCategories"

const styleCatalogArea = `
  visibility: hidden;
  opacity: 0;
  left: 0;
  display: flex;
  width: 100%;
`

export const cssScrollable = css``
export const cssScrollableIsInit = css``

export const CategoriesContainer = styled.div`
  width: 100%;
  display: grid;
  grid-template-columns: 2.3fr 9.7fr;
  grid-column-gap: 40px;
  z-index: 2;
  position: relative;
  padding-bottom: 0;

  ${StyledSpin} {
    align-items: flex-start;
  }

  ${StyledLoaderOverlay} {
    background: transparent;
  }

  @media (max-width: ${breakpoints.lg}) {
    grid-template-columns: 1fr;
  }
`

export const StyledOverlay = styled(StyledUtilOverlay)`
  ${styleCatalogArea};
  transform: translateY(15px);
  transition: transform 0.3s ease-in-out;
  will-change: scroll-position;
  opacity: 0;
  visibility: hidden;

  &.${cssIsActive} {
    opacity: 1;
    visibility: visible;
  }
`

export const StyledCatalogPopup = styled.div`
  ${getCustomizeScroll()};
  ${styleCatalogArea};
  background: transparent;
  z-index: 6;
  position: absolute;
  top: 100%;
  height: calc(100vh - 180px);
  margin: 0;
  opacity: 0;
  visibility: hidden;
  transform: translateY(-15px);
  transition: transform 0.6s ${transitionTimingFunction},
    opacity 0.3s ${transitionTimingFunction};
  will-change: transform;

  &.${cssIsActive} {
    opacity: 1;
    visibility: visible;
    transform: translateY(0);
  }

  > ${Container} {
    height: inherit;
    > ${Row} {
      height: inherit;
    }
  }

  ${CategoriesContainer} {
    height: inherit;
  }

  ${StyledSidebar} {
    height: inherit;
    padding: 40px 30px 0 0;
    border-bottom-right-radius: 0;
  }

  ${SidebarInner} {
    ${getCustomizeScroll()};
    overflow: hidden auto;
    height: 100%;
    padding: 10px 10px 10px 0;
  }

  ${StyledCategories} {
    transition: transform 0.4s ${transitionTimingFunction},
      opacity 0.2s ${transitionTimingFunction};
    will-change: transform;
  }

  &.${cssScrollable} {
    ${StyledCategories} {
      opacity: 0;
      transform: translateX(-30px);
    }

    &.${cssScrollableIsInit} {
      ${StyledCategories} {
        opacity: 1;
        transform: translateX(0);
      }
    }
  }

  @media (max-width: ${breakpoints.lg}) {
    ${StyledSidebar} {
      display: none;
    }
  }
`
