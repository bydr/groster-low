import type { FC } from "react"
import { useCallback, useEffect, useRef, useState } from "react"
import {
  CategoriesContainer,
  cssScrollable,
  cssScrollableIsInit,
  StyledCatalogPopup,
  StyledOverlay,
} from "./StyledPopup"
import { Container, Row } from "../../../../styles/utils/StyledGrid"
import Sidebar from "../../../Sidebar/Sidebar"
import Categories from "../Categories"
import { cssSidebarGray } from "../../../Sidebar/StyledSidebar"
import { Category } from "../../../../../contracts/contracts"
import { StyledList, StyledListItem } from "../../../List/StyledList"
import { Typography } from "../../../Typography/Typography"
import { cssIsActive, getBreakpointVal } from "../../../../styles/utils/Utils"
import { catalogAPI } from "../../../../api/catalogAPI"
import { BaseLoader } from "../../../Loaders/BaseLoader/BaseLoader"
import { catalogSlice } from "../../../../store/reducers/catalogSlice"
import { useAppDispatch, useAppSelector } from "../../../../hooks/redux"
import { cx } from "@linaria/core"
import { useCategoriesPopup } from "../../../../hooks/categoriesPopup"
import { UpdateCurrentCategoryPopupIdType } from "../../../../types/types"
import { useWindowSize } from "../../../../hooks/windowSize"
import { breakpoints } from "../../../../styles/utils/vars"

export type PopupPropsType = {
  businessAreas: Category[] | null
  isShow?: boolean
}

const Popup: FC<PopupPropsType> = ({ businessAreas, isShow = false }) => {
  const { setCategoriesByAreas } = catalogSlice.actions
  const categoriesAll = useAppSelector(
    (state) => state.catalog.categories?.treeSorted || [],
  )
  const categoriesByBusinessArea = useAppSelector(
    (state) => state.catalog.categoriesByAreas,
  )
  const dispatch = useAppDispatch()
  const [currentBusinessAreaId, setCurrentBusinessAreaId] = useState<number>(-1)
  const [currentBusinessAreaUuid, setCurrentBusinessAreaUuid] = useState<
    string | null
  >(null)
  const [currentCategoryId, setCurrentCategoryId] = useState<number | null>(
    null,
  )
  const { width } = useWindowSize()

  const categoriesAreaRef = useRef<HTMLDivElement | null>(null)
  const lastScrollPositionRef = useRef<number>(0)

  const [isInitScroll, setIsInitScroll] = useState<boolean | null>(null)

  const { data, isFetching, isSuccess } =
    catalogAPI.useCategoriesByBusinessArea({
      uuidArea: currentBusinessAreaUuid,
    })

  const updateBAIdentifiers = (id: number, uuid: string | null) => {
    setCurrentBusinessAreaId(id)
    setCurrentBusinessAreaUuid(uuid || null)
  }

  const updateCurrentCategoryId: UpdateCurrentCategoryPopupIdType = useCallback(
    ({ id, parent }) => {
      if (width === undefined || width > getBreakpointVal(breakpoints.lg)) {
        setIsInitScroll(null)
        lastScrollPositionRef.current = 0
        setCurrentCategoryId(null)
        return
      }
      setIsInitScroll(false)
      setTimeout(() => {
        const updatedId =
          id === null || !(parent === 0) || currentCategoryId === id ? null : id

        if (updatedId !== null) {
          lastScrollPositionRef.current =
            categoriesAreaRef.current?.scrollTop || 0
        }

        setCurrentCategoryId(updatedId)
      }, 100)
    },
    [currentCategoryId, width],
  )

  useCategoriesPopup({
    ref: categoriesAreaRef.current,
  })

  useEffect(() => {
    if (!isShow) {
      lastScrollPositionRef.current = 0
      if (categoriesAreaRef.current !== null) {
        categoriesAreaRef.current.scrollTop = 0
      }
      setCurrentCategoryId(null)
      setIsInitScroll(null)
    }
  }, [isShow])

  useEffect(() => {
    if (categoriesAreaRef.current !== null) {
      if (currentCategoryId !== null) {
        categoriesAreaRef.current.scrollTop = 0
      } else {
        categoriesAreaRef.current.scrollTop = lastScrollPositionRef.current
      }
    }
    setIsInitScroll(true)
  }, [currentCategoryId])

  useEffect(() => {
    if (isSuccess) {
      dispatch(setCategoriesByAreas(data))
    }
  }, [dispatch, setCategoriesByAreas, data, isSuccess])

  return (
    <>
      <StyledCatalogPopup
        className={cx(
          isShow && cssIsActive,
          isInitScroll !== null &&
            (isInitScroll
              ? cx(cssScrollable, cssScrollableIsInit)
              : cx(cssScrollable)),
        )}
      >
        <Container>
          <Row>
            <CategoriesContainer>
              {isFetching && <BaseLoader />}
              <Sidebar className={cssSidebarGray}>
                <StyledList>
                  <StyledListItem
                    className={currentBusinessAreaId === -1 ? cssIsActive : ""}
                  >
                    <Typography
                      variant={"p14"}
                      onClick={() => {
                        updateBAIdentifiers(-1, null)
                      }}
                    >
                      Все сферы деятельности
                    </Typography>
                  </StyledListItem>
                  {businessAreas?.map((area) => (
                    <StyledListItem
                      key={area.id}
                      className={
                        currentBusinessAreaId === area.id ? cssIsActive : ""
                      }
                      onClick={() => {
                        if (area.id) {
                          updateBAIdentifiers(area.id, area.uuid || null)
                        }
                      }}
                    >
                      <Typography variant={"p14"}>{area.name}</Typography>
                    </StyledListItem>
                  ))}
                </StyledList>
              </Sidebar>
              <Categories
                categories={
                  currentBusinessAreaId === -1
                    ? categoriesAll
                    : Object.entries(categoriesByBusinessArea || {}).map(
                        ([, c]) => c,
                      )
                }
                variant={"popup"}
                updateCurrentCategoryId={updateCurrentCategoryId}
                currentCategoryId={currentCategoryId}
                ref={categoriesAreaRef}
              />
            </CategoriesContainer>
          </Row>
        </Container>
      </StyledCatalogPopup>
      <StyledOverlay className={cx(isShow && cssIsActive)} />
    </>
  )
}

const PopupWrapper: FC<PopupPropsType> = ({ isShow, ...rest }) => {
  const [isInit, setIsInit] = useState(isShow)

  useEffect(() => {
    if (isShow) {
      setIsInit(isShow)
    }
  }, [isShow])

  return <>{isInit && <Popup {...rest} isShow={isShow} />}</>
}

export { PopupWrapper as Popup }
