import { styled } from "@linaria/react"
import { breakpoints, colors } from "../../../styles/utils/vars"
import { cssIsActive, getCustomizeScroll } from "../../../styles/utils/Utils"
import {
  StyledCategoryTitle,
  cssAngle,
  cssIsSelectedCategory,
  StyledCategory,
  StyledCategoryLink,
  StyledCategoryName,
  StyledCategoryImage,
} from "./Category/StyledCategory"
import { VariantCategoriesType } from "./Categories"
import { StyledList } from "../../List/StyledList"
import { getTypographyBase } from "../../Typography/Typography"
import { ButtonBase, cssButtonBackward } from "../../Button/StyledButton"
import { StyledEntityImage } from "../../EntityImage/Styled"

export const CategoriesInner = styled.div`
  width: 100%;
  display: grid;
  grid-template-columns: repeat(5, 1fr);

  > ${StyledCategory} {
    &:nth-child(5n) {
      border-right-color: transparent;
    }
    &:last-child {
      border-right-color: transparent;
    }

    > ${StyledList} {
      > ${StyledCategory} {
        padding-left: 0;
      }
    }
  }
`

export const StyledCategories = styled.section`
  width: 100%;
  position: relative;

  &:before {
    content: "";
  }

  ${CategoriesInner} {
    > ${StyledCategory} {
      > ${StyledCategoryTitle} {
        &[data-variant="mainDrop"] {
          > ${StyledCategoryLink} {
            display: flex;
          }
          > ${StyledCategoryName} {
            display: none;
          }
        }
      }
    }
  }

  @media (max-width: ${breakpoints.md}) {
    ${CategoriesInner} {
      > ${StyledCategory} {
        ${StyledCategoryImage} {
          margin-bottom: 0;
          margin-right: 10px;
          width: 40px;
          height: 40px;
        }
      }
    }
  }
`

export const StyledCategoriesPopup = styled(StyledCategories)`
  ${getCustomizeScroll()};
  height: inherit;
  overflow-y: scroll;
  -webkit-overflow-scrolling: touch;

  @media (max-width: ${breakpoints.xxl}) {
    ${CategoriesInner} {
      grid-template-columns: repeat(5, 1fr);
      > ${StyledCategory} {
        &:nth-child(6n) {
          border-right-color: ${colors.gray};
        }
        &:nth-child(5n) {
          border-right-color: transparent;
        }
        &:last-child {
          border-right-color: transparent;
        }
      }
    }
  }
  @media (max-width: ${breakpoints.xl}) {
    ${CategoriesInner} {
      grid-template-columns: repeat(4, 1fr);
      > ${StyledCategory} {
        &:nth-child(5n) {
          border-right-color: ${colors.gray};
        }
        &:nth-child(4n) {
          border-right-color: transparent;
        }
      }
    }
  }
  @media (max-width: ${breakpoints.lg}) {
    ${CategoriesInner} {
      grid-template-columns: repeat(3, 1fr);
      > ${StyledCategory} {
        &:nth-child(4n) {
          border-right-color: ${colors.gray};
        }
        &:nth-child(3n) {
          border-right-color: transparent;
        }

        ${StyledCategoryTitle} {
          font-weight: 500;
          margin: 0;
        }
      }
    }
  }
  @media (max-width: ${breakpoints.md}) {
    ${CategoriesInner} {
      display: flex;
      flex-direction: column;
      grid-template-columns: initial;

      &:nth-child(3n) {
        border-right-color: ${colors.gray};
      }

      > ${StyledCategoryTitle} {
        .${cssAngle} {
          display: flex;
        }
      }

      > ${StyledCategory} {
        > ${StyledCategoryTitle} {
          &[data-variant="mainDrop"] {
            > ${StyledCategoryLink} {
              display: none;
            }
            > ${StyledCategoryName} {
              display: flex;
            }
          }
        }

        .${cssButtonBackward} {
          display: flex;
        }
      }

      &.${cssIsSelectedCategory} {
        > ${StyledCategory} {
          &:not(.${cssIsActive}) {
            display: none;
          }

          &.${cssIsActive} {
            ${StyledCategoryImage} {
              width: 60px;
              height: 60px;
              margin-left: 20px;
            }
            > ${StyledCategoryTitle} {
              > ${ButtonBase} {
                display: flex;
              }

              &[data-variant="mainDrop"] {
                display: flex;
                flex-direction: column;
                align-items: flex-start;

                > ${StyledCategoryLink} {
                  display: flex;
                  padding-left: 20px;
                  margin-top: 10px;
                }
                > ${StyledCategoryName} {
                  display: none;
                }
              }
            }
          }
        }
      }
    }
  }

  @media (max-width: ${breakpoints.md}) {
    padding: 0 0 70px 0;

    ${CategoriesInner} {
      > ${StyledCategory} {
        border-right: none;
        padding: 14px 0;
        display: flex;
        width: 100%;

        ${StyledCategoryTitle} {
          flex: 1;
        }

        > ${StyledList} {
          display: none;
          margin-top: 20px;
          margin-left: 30px;

          ${StyledCategoryTitle} {
            margin-bottom: 14px;
          }
        }

        &.${cssIsActive} {
          border-color: transparent;
          display: flex;
          flex-direction: column;

          > ${StyledCategoryTitle} {
            ${getTypographyBase("p14")};
            font-weight: bold;
            display: flex;
            flex-direction: column;

            .${cssAngle} {
              display: flex;
            }
          }

          > ${StyledList} {
            display: flex;
          }
        }
      }
    }
  }
`

export const StyledCategoriesCatalog = styled(StyledCategories)`
  &:before {
    height: 1px;
    position: absolute;
    left: 0;
    bottom: 0;
    background: ${colors.white};
    width: 100%;
  }

  ${CategoriesInner} {
    grid-template-columns: 1fr 1fr 1fr;

    > ${StyledCategory} {
      padding: 30px 32px;
      display: grid;
      grid-template-columns: auto 1fr;
      grid-template-rows: auto 1fr;
      gap: 0 24px;

      &:nth-child(4n),
      &:nth-child(5n) {
        border-right-color: ${colors.gray};
      }

      &:nth-child(3n) {
        border-right-color: transparent;
      }

      > * {
        grid-column: 2;
      }

      ${StyledCategoryImage} {
        grid-row: 1 / 1;
        grid-column: 1;
        margin: 0;
      }

      .${cssAngle} {
        display: none;
      }

      > ${StyledCategoryTitle} {
        ${getTypographyBase("h3")};
        margin-bottom: 16px;
      }

      ${StyledList} {
        ${StyledCategoryTitle} {
          ${getTypographyBase("p14")};
          margin-bottom: 4px;
        }
      }
    }
  }

  @media (max-width: ${breakpoints.xl}) {
    ${CategoriesInner} {
      > ${StyledCategory} {
        display: block;
        grid-template-columns: initial;
        padding: 20px 30px;

        ${StyledEntityImage} {
          width: 80px;
          height: 80px;
        }
      }
    }
  }

  @media (max-width: ${breakpoints.lg}) {
    ${CategoriesInner} {
      > ${StyledCategory} {
        padding: 15px;

        > ${StyledCategoryTitle} {
          ${getTypographyBase("p14")};
          font-weight: bold;
          margin-bottom: 16px;
          align-items: center;
        }

        ${StyledList} {
          ${StyledCategoryTitle} {
            ${getTypographyBase("p14")};
            margin-bottom: 10px;
          }
        }
      }
    }
  }

  @media (max-width: ${breakpoints.md}) {
    ${CategoriesInner} {
      > ${StyledCategory} {
        ${StyledEntityImage} {
          width: 40px;
          height: 40px;
        }
      }
    }
  }

  @media (max-width: ${breakpoints.sm}) {
    ${CategoriesInner} {
      grid-template-columns: 1fr;

      > ${StyledCategory} {
        padding-left: 0;
        padding-right: 0;
        border-color: transparent;
        display: grid;
        grid-template-columns: auto 1fr;
        grid-row-gap: 16px;

        &:nth-child(4n),
        &:nth-child(5n) {
          border-color: transparent;
        }

        > ${StyledCategoryTitle} {
          margin-bottom: 0;
        }

        ${StyledEntityImage} {
          grid-row: 1 / 1;
          margin-bottom: 0;
        }

        > ${StyledList} {
          grid-column: 1/-1;
          grid-row: 2;
        }
      }
    }
  }
`

export const createCategoriesComponent = (
  variant: VariantCategoriesType,
): typeof StyledCategories => {
  switch (variant) {
    case "popup":
      return StyledCategoriesPopup
    case "catalog":
      return StyledCategoriesCatalog
    default:
      return StyledCategories
  }
}
