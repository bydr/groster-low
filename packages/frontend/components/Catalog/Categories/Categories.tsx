import { forwardRef } from "react"
import { CategoriesInner, createCategoriesComponent } from "./StyledCategories"
import { Category } from "./Category/Category"
import { cssIsSelectedCategory } from "./Category/StyledCategory"
import {
  ICategoryTreeItem,
  UpdateCurrentCategoryPopupIdType,
} from "../../../types/types"

export type VariantCategoriesType = "catalog" | "popup"
export type CategoriesPropsType = {
  categories: ICategoryTreeItem[]
  variant?: VariantCategoriesType
  currentCategoryId?: null | number
  updateCurrentCategoryId?: UpdateCurrentCategoryPopupIdType
}
const Categories = forwardRef<HTMLDivElement, CategoriesPropsType>(
  (
    {
      categories,
      variant = "popup",
      currentCategoryId = null,
      updateCurrentCategoryId,
    },
    ref,
  ) => {
    const CategoriesContainerComponent = createCategoriesComponent(variant)
    return (
      <>
        {categories.length > 0 && (
          <CategoriesContainerComponent ref={ref}>
            <CategoriesInner
              className={
                currentCategoryId !== null ? cssIsSelectedCategory : ""
              }
            >
              {categories.map((category) => (
                <Category
                  category={category}
                  key={category.uuid}
                  updateCurrentCategoryId={updateCurrentCategoryId}
                  currentId={currentCategoryId}
                />
              ))}
            </CategoriesInner>
          </CategoriesContainerComponent>
        )}
      </>
    )
  },
)

Categories.displayName = "Categories"

export default Categories
