import { styled } from "@linaria/react"
import {
  getTypographyBase,
  Paragraph13,
  Span,
} from "../../../Typography/Typography"
import { cssIcon, getSizeSVG } from "../../../Icon"
import { breakpoints, colors, sizeSVG } from "../../../../styles/utils/vars"
import { StyledList } from "../../../List/StyledList"
import { css } from "@linaria/core"
import { StyledLinkBase } from "../../../Link/StyledLink"
import { ButtonBase, cssButtonBackward } from "../../../Button/StyledButton"
import { StyledEntityImage } from "../../../EntityImage/Styled"

export const StyledCategoryLink = styled(Span)``
export const StyledCategoryName = styled(Span)``

export const cssIsSelectedCategory = css``

export const cssAngle = css`
  ${getSizeSVG(sizeSVG.medium)};
  display: none;
`

export const StyledCategoryImage = styled.div`
  display: block;
  position: relative;
  margin-bottom: 24px;
  border-radius: 50px;
  width: 80px;
  height: 80px;
  overflow: hidden;
  margin-left: 0;
  margin-right: 0;

  ${StyledEntityImage} {
    width: 100%;
    height: 100%;
  }
`

export const StyledCategoryTitle = styled(Paragraph13)`
  font-weight: bold;
  margin: 0 0 16px 0;
  display: flex;
  align-items: center;

  &:hover,
  &:active {
    color: ${colors.brand.purple};
  }

  .${cssIcon} {
    margin-right: 10px;
  }

  ${Span} {
    flex: 1;
  }

  @media (max-width: ${breakpoints.md}) {
    ${getTypographyBase("p14")};
  }
`

export const StyledCategory = styled.div`
  padding: 30px 10px 30px 20px;
  border-right: 1px solid ${colors.gray};
  border-bottom: 1px solid ${colors.gray};

  .${cssButtonBackward} {
    color: ${colors.grayDark};
    background: transparent;
    display: none;
    margin: 0 auto 20px 20px;

    &:hover,
    &:active {
      background: transparent;
      color: ${colors.black};
    }
  }

  & & {
    padding: 0 0 0 16px;
    border: none;
    width: 100%;

    ${StyledCategoryTitle} {
      font-weight: 500;
    }
  }

  ${StyledList} {
    flex-direction: column;
    align-items: flex-start;

    ${StyledCategoryTitle} {
      ${getTypographyBase("p13")};
      color: ${colors.black};
      margin-bottom: 4px;

      &:hover,
      &:active {
        color: ${colors.brand.purple};
      }

      @media (max-width: ${breakpoints.md}) {
        ${getTypographyBase("p14")};
      }
    }
  }

  ${StyledLinkBase} {
    color: inherit;
    width: 100%;
  }

  > ${StyledCategoryTitle} {
    > ${ButtonBase} {
      display: none;
      color: ${colors.grayDark};
    }
  }

  > ${StyledCategoryImage} {
    display: flex;
    align-items: flex-start;
  }

  @media (max-width: ${breakpoints.md}) {
    .${cssAngle} {
      display: flex;
    }
  }
`

export const StyledCategoryQty = styled(Span)`
  ${getTypographyBase("p12")};
  color: ${colors.grayDark};
  margin: 0 0 0 6px;
`
