import React, { FC } from "react"
import {
  CategoryItemPropsType,
  ICategoryTreeItem,
} from "../../../../types/types"
import {
  cssAngle,
  StyledCategoryLink,
  StyledCategoryName,
  StyledCategoryQty,
  StyledCategoryTitle,
} from "./StyledCategory"
import { Link } from "../../../Link"
import { ROUTES } from "../../../../utils/constants"
import { Icon } from "../../../Icon"
import { colors } from "../../../../styles/utils/vars"

export const Title: FC<
  {
    updateCurrentId: () => void
    parent?: number
    category: ICategoryTreeItem
    isChildrenContains: boolean
    path: string
  } & Omit<CategoryItemPropsType, "setCurrentId">
> = ({
  updateCurrentId,
  parent,
  category,
  isChildrenContains,
  currentId,
  path,
}) => {
  let variantView: "main" | "mainDrop" | "child" = "child"
  if (parent === 0) {
    variantView = "main"
    if (isChildrenContains) {
      variantView = "mainDrop"
    }
  } else {
    variantView = "child"
  }

  return (
    <StyledCategoryTitle
      data-variant={variantView}
      onClick={() => {
        if (variantView !== "mainDrop" || currentId === category.id) {
          return
        }
        updateCurrentId()
      }}
    >
      {variantView === "main" && (
        <>
          <StyledCategoryLink>
            <Link href={`${ROUTES.catalog}/${path}`} scroll>
              {category.name}
            </Link>
          </StyledCategoryLink>
        </>
      )}
      {variantView === "mainDrop" && (
        <>
          <StyledCategoryLink>
            <Link href={`${ROUTES.catalog}/${path}`} scroll>
              {category.name}
            </Link>
          </StyledCategoryLink>
          <StyledCategoryName>{category.name}</StyledCategoryName>
          {currentId !== category.id && (
            <Icon
              NameIcon={"AngleRight"}
              fill={colors.brand.yellow}
              className={cssAngle}
            />
          )}
        </>
      )}
      {variantView === "child" && (
        <>
          <StyledCategoryLink>
            <Link href={`${ROUTES.catalog}/${path}`} scroll>
              <StyledCategoryName>
                {category.name}{" "}
                <StyledCategoryQty>{category.product_qty}</StyledCategoryQty>
              </StyledCategoryName>
            </Link>
          </StyledCategoryLink>
        </>
      )}
    </StyledCategoryTitle>
  )
}
