import React, { FC } from "react"
import {
  CategoryItemPropsType,
  ICategoryTreeItem,
} from "../../../../types/types"
import { StyledList } from "../../../List/StyledList"
import { Category } from "./Category"

export type SubCategoryPropsType = CategoryItemPropsType & {
  categories: Record<string, ICategoryTreeItem>
}

export const SubCategory: FC<SubCategoryPropsType> = ({
  currentId,
  updateCurrentCategoryId,
  categories,
}) => {
  const entries = Object.entries(categories)
  return (
    <>
      {entries.length > 0 && (
        <StyledList as={"div"}>
          {Object.entries(categories).map(([, category]) => {
            return (
              <Category
                key={category.uuid || ""}
                category={category}
                currentId={currentId}
                updateCurrentCategoryId={updateCurrentCategoryId}
              />
            )
          })}
        </StyledList>
      )}
    </>
  )
}
