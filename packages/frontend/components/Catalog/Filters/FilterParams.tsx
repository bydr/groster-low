import { FC, MouseEvent, useContext } from "react"
import { FilterPopover } from "../../Filter/Popover/FilterPopover"
import Button from "../../Button/Button"
import { cssHiddenLG } from "../../../styles/utils/Utils"
import {
  StyledFilterInner,
  StyledFilters,
  TotalControls,
} from "../../Filter/StyledFilter"
import {
  catalogSlice,
  IAvailableParams,
  PriceRangeType,
} from "../../../store/reducers/catalogSlice"
import { ModalContext } from "../../Modals/Modal"
import { Range } from "../../Range/Range"
import { FilterHandlersType, FilterList } from "../../Filter/List/List"
import { cx } from "@linaria/core"
import { cssButtonClearFilters } from "../../Button/StyledButton"
import { useAppDispatch, useAppSelector } from "../../../hooks/redux"
import { UpdateFilterQueryPropsType } from "../../../types/types"

export const FilterParams: FC<
  {
    availableParams: IAvailableParams | null
    clearFilters: (e: MouseEvent<HTMLButtonElement>) => void
    priceRange: PriceRangeType
    checkedParamsKeysTotal: string[]
    isResponsive?: boolean
    visibleDefault?: boolean
  } & FilterHandlersType
> = ({
  availableParams,
  clearFilters,
  priceRange,
  checkedParamsKeysTotal,
  onClearItemHandle,
  onUpdateFiltersHandle,
  onChangeCheckboxHandle,
  isResponsive = false,
  visibleDefault,
  children,
}) => {
  const modalContext = useContext(ModalContext)
  const dispatch = useAppDispatch()
  const filterStores = useAppSelector(
    (state) => state.catalog.filter.stores.params,
  )
  const { setCheckedPriceRange, updateCheckedStores } = catalogSlice.actions

  return (
    <>
      <StyledFilters>
        <FilterPopover
          isResponsive={isResponsive}
          title={"Цена"}
          visibleDefault={visibleDefault}
          isActive={!!priceRange.checked}
          onClearFilterItem={() => {
            dispatch(setCheckedPriceRange(null))
            onUpdateFiltersHandle<UpdateFilterQueryPropsType>({
              price: null,
            })
          }}
        >
          <StyledFilterInner>
            <Range
              defaultValue={{
                min: priceRange.initial?.min || 0,
                max: priceRange.initial?.max || 0,
              }}
              value={
                !!priceRange.checked
                  ? {
                      min: priceRange.checked?.min,
                      max: priceRange.checked?.max,
                    }
                  : undefined
              }
              onAfterChange={(value) => {
                if (value !== undefined) {
                  dispatch(
                    setCheckedPriceRange({ min: value[0], max: value[1] }),
                  )
                  onUpdateFiltersHandle<UpdateFilterQueryPropsType>({
                    price: { min: value[0], max: value[1] },
                  })
                }
              }}
            />
          </StyledFilterInner>
        </FilterPopover>

        <FilterList
          isResponsive={isResponsive}
          visibleDefault={visibleDefault}
          filters={availableParams}
          onUpdateFiltersHandle={onUpdateFiltersHandle}
          onChangeCheckboxHandle={onChangeCheckboxHandle}
          onClearItemHandle={onClearItemHandle}
        />

        <FilterList
          isResponsive={isResponsive}
          visibleDefault={visibleDefault}
          filters={filterStores}
          onUpdateFiltersHandle={onUpdateFiltersHandle}
          onChangeCheckboxHandle={(e, isDisabledField) => {
            if (isDisabledField) {
              e.preventDefault()
              return
            }
            dispatch(
              updateCheckedStores({
                keyFilter: [e.currentTarget.name],
                checked: e.currentTarget.checked,
              }),
            )
            onUpdateFiltersHandle<UpdateFilterQueryPropsType>({
              store: e.currentTarget.checked
                ? [...checkedParamsKeysTotal, e.currentTarget.name]
                : checkedParamsKeysTotal.filter(
                    (c) => c !== e.currentTarget.name,
                  ),
            })
          }}
          onClearItemHandle={({ keyFilter, checked }) => {
            dispatch(
              updateCheckedStores({
                keyFilter,
                checked,
              }),
            )
            onUpdateFiltersHandle<UpdateFilterQueryPropsType>({
              store: [],
            })
          }}
        />

        {(checkedParamsKeysTotal.length > 0 || !!priceRange.checked) && (
          <Button
            variant={"link"}
            icon={"Rotate"}
            className={cx(cssHiddenLG, cssButtonClearFilters)}
            onClick={clearFilters}
          >
            Очистить фильтры
          </Button>
        )}

        {children}
      </StyledFilters>
      <TotalControls>
        <Button
          variant={"outline"}
          size={"large"}
          onClick={clearFilters}
          disabled={
            !(checkedParamsKeysTotal.length > 0 || !!priceRange.checked)
          }
        >
          Сбросить все
        </Button>
        <Button
          variant={"filled"}
          size={"large"}
          onClick={(e) => {
            e.preventDefault()
            modalContext?.hide()
            onUpdateFiltersHandle<UpdateFilterQueryPropsType>()
          }}
        >
          Применить
        </Button>
      </TotalControls>
    </>
  )
}
