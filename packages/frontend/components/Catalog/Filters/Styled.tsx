import { styled } from "@linaria/react"
import {
  StyledFilterColumn,
  StyledFilterGroup,
} from "../../Filter/StyledFilter"
import { breakpoints } from "../../../styles/utils/vars"

export const StyledFilterTagsColumn = styled(StyledFilterColumn)`
  flex: 9;
  width: 100%;

  ${StyledFilterGroup} {
    width: 100%;
  }
`
export const StyledFilterHelpColumn = styled(StyledFilterColumn)`
  flex: 3;

  @media (max-width: ${breakpoints.md}) {
    display: none;
  }
`
