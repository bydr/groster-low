import { FC, FormEvent } from "react"
import {
  cssFilterGroupRadios,
  StyledFilterGroup,
} from "../../Filter/StyledFilter"
import { CustomCheckbox } from "../../Checkbox/CustomCheckbox"
import { cssElementIsLoading } from "../../../styles/utils/Utils"
import { UpdateFilterQueryPropsType } from "../../../types/types"
import { shippingDateToString } from "../../../utils/helpers"
import { useShippings } from "../../../hooks/shippings"

export const FilterRadioGroups: FC<{
  isFast: boolean
  updateCollectFilters: (props?: UpdateFilterQueryPropsType | undefined) => void
  isLoading: boolean
  isEnabled: boolean
}> = ({ isFast, isEnabled, updateCollectFilters, isLoading }) => {
  const onChangeEnabledHandler = (e: FormEvent<HTMLInputElement>): void => {
    if (isLoading) {
      e.preventDefault()
      return
    }
    updateCollectFilters({ isEnabled: e.currentTarget.checked })
  }
  const onChangeFastHandler = (e: FormEvent<HTMLInputElement>): void => {
    if (isLoading) {
      e.preventDefault()
      return
    }
    updateCollectFilters({ isFast: e.currentTarget.checked })
  }

  const { shippingDate } = useShippings({
    product: {
      currentCount: 1,
      fastQty: 2,
      totalQty: 4,
    },
  })

  // const shippingDate =

  return (
    <>
      <StyledFilterGroup className={cssFilterGroupRadios}>
        <CustomCheckbox
          name={"isFast"}
          message={
            shippingDate !== null
              ? shippingDateToString(shippingDate, {
                  today: "Доставим сегодня",
                  tomorrow: "Доставим завтра",
                  other: (day) => `Доставка к ${day}`,
                })
              : "Быстро"
          }
          variant={"radio"}
          initialState={isFast}
          stateCheckbox={isFast}
          onChange={onChangeFastHandler}
          className={isLoading ? cssElementIsLoading : ""}
        />
        <CustomCheckbox
          name={"inStock"}
          message={"В наличии"}
          variant={"radio"}
          initialState={isEnabled}
          stateCheckbox={isEnabled}
          onChange={onChangeEnabledHandler}
          className={isLoading ? cssElementIsLoading : ""}
        />
      </StyledFilterGroup>
    </>
  )
}
