import type { FC } from "react"
import { useEffect, useState } from "react"
import Button from "../../../Button/Button"
import { useAppSelector } from "../../../../hooks/redux"
import { cx } from "@linaria/core"
import { cssIsActive, getBreakpointVal } from "../../../../styles/utils/Utils"
import { TagType } from "../../../../types/types"
import { ROUTES } from "../../../../utils/constants"
import { cssButtonTogglerTags } from "../../../Account/History/Orders/Filters/Styled"
import { useWindowSize } from "../../../../hooks/windowSize"
import { breakpoints } from "../../../../styles/utils/vars"
import {
  cssIsCategories,
  cssTagRectification,
  StyledTags,
} from "components/Tags/StyledTags"
import { useRouter } from "next/router"

const LIMIT_MOBILE = 0
const LIMIT_PC = 10

export const Tags: FC = () => {
  const router = useRouter()
  const tags = useAppSelector((state) => state.catalog.tags)
  const currentTagId = useAppSelector((state) => state.catalog.currentTag.id)
  const currentCategory = useAppSelector(
    (state) => state.catalog.currentCategory,
  )
  const categoriesFetched = useAppSelector(
    (state) => state.catalog.categories?.fetched,
  )
  const [tagsCategories, setTagsCategories] = useState<TagType[]>([])
  const [isShowedAll, setIsShowedAll] = useState<boolean>(false)

  const { width } = useWindowSize()
  const [limitShowAll, setLimitShowAll] = useState<number | null>(null)

  useEffect(() => {
    setIsShowedAll(false)
  }, [router.asPath])

  useEffect(() => {
    setLimitShowAll(
      width !== undefined && width <= getBreakpointVal(breakpoints.md)
        ? LIMIT_MOBILE
        : LIMIT_PC,
    )
  }, [width])

  useEffect(() => {
    let categories: TagType[] = [...tags]

    if (!!currentCategory && !!categoriesFetched) {
      if (currentCategory.parent === 0) {
        categories = [
          ...categories,
          ...Object.keys(categoriesFetched)
            .filter((c) => categoriesFetched[c].parent === currentCategory.id)
            .map((k) => ({
              id: categoriesFetched[k].id,
              name: categoriesFetched[k].name,
              url: `${ROUTES.catalog}/${categoriesFetched[k].alias}`,
            })),
        ]
      } else {
        categories = [
          ...categories,
          ...Object.keys(categoriesFetched)
            .filter(
              (c) => categoriesFetched[c].parent === currentCategory.parent,
            )
            .map((c) => ({
              id: categoriesFetched[c].id,
              name: categoriesFetched[c].name,
              url: `${ROUTES.catalog}/${categoriesFetched[c].alias}`,
            })),
        ]
      }

      setTagsCategories(categories)
    } else {
      setTagsCategories([])
    }
  }, [categoriesFetched, currentCategory, tags])

  return (
    <>
      {tagsCategories && tagsCategories.length > 0 && limitShowAll !== null && (
        <StyledTags className={cx(cssIsCategories)}>
          {tagsCategories.length > limitShowAll && (
            <>
              <Button
                variant={"outline"}
                className={cx(cssButtonTogglerTags, isShowedAll && cssIsActive)}
                onClick={() => {
                  setIsShowedAll(!isShowedAll)
                }}
              >
                {isShowedAll ? "Свернуть" : "Категории"}
              </Button>
            </>
          )}

          {tagsCategories
            .filter((tag) => tag.id !== currentCategory?.id)
            .filter((_, i) => (!isShowedAll ? i < limitShowAll : true))
            .map((t) => (
              <Button
                key={t.id}
                as={"a"}
                href={t.url}
                className={cx(
                  currentTagId === t.id && cssIsActive,
                  cssTagRectification,
                )}
              >
                {t.name}
              </Button>
            ))}
        </StyledTags>
      )}
    </>
  )
}
