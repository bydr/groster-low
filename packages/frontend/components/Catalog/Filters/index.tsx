import { FC, FormEvent, useCallback, useEffect, useRef, useState } from "react"
import {
  cssButtonFilterDialogTrigger,
  cssFilterGroupViewMode,
  StyledFilterColumn,
  StyledFilterGroup,
  StyledFilterRow,
  StyledFiltersContainer,
} from "../../Filter/StyledFilter"
import Button from "../../Button/Button"
import { Select, SelectItemsType } from "../../Select/Select"
import { SortType, ViewProductsType } from "../../../types/types"
import { FilterParams } from "./FilterParams"
import { Tags } from "./Tags/Tags"
import { useAppDispatch, useAppSelector } from "../../../hooks/redux"
import { catalogSlice } from "../../../store/reducers/catalogSlice"
import { cssIsActive, getBreakpointVal } from "../../../styles/utils/Utils"
import { useFilterCatalog } from "../../../hooks/filterCatalog"
import { StyledFilterHelpColumn, StyledFilterTagsColumn } from "./Styled"
import { useWindowSize } from "../../../hooks/windowSize"
import { breakpoints } from "../../../styles/utils/vars"
import dynamic, { DynamicOptions } from "next/dynamic"
import type { ModalDefaultPropsType } from "../../Modals/Modal"
import { cx } from "@linaria/core"
import {
  cssButtonFill,
  cssButtonFloat,
  cssButtonHelpChoose,
} from "../../Button/StyledButton"
import PushCounter from "../../../styles/utils/PushCounter"
import { FilterRadioGroups } from "./FilterRadioGroups"

const DynamicModal = dynamic((() =>
  import("../../Modals/Modal").then(
    (mod) => mod.Modal,
  )) as DynamicOptions<ModalDefaultPropsType>)

export type FiltersPropsType = {
  viewProducts: ViewProductsType
  updateViewProducts: (view: ViewProductsType) => void
}

export const CatalogFilters: FC<FiltersPropsType> = ({
  viewProducts,
  updateViewProducts,
}) => {
  const [sortedItems, setSortedItems] = useState<SelectItemsType[]>([])
  const dispatch = useAppDispatch()
  const categories = useAppSelector(
    (state) => state.catalog.categories?.fetched,
  )
  const currentCategory = useAppSelector(
    (state) => state.catalog.currentCategory,
  )
  const { updateCheckedParams } = catalogSlice.actions
  const {
    updateFilterQuery: updateCollectFilters,
    clearFilterQuery: clearFiltersHandler,
    isLoading,
    availableParams,
    checkedParamsKeysTotal,
    priceRange,
    isFast,
    isEnabled,
    sortBy,
    sortBySelected,
  } = useFilterCatalog()

  const btnFilterResponsiveRef = useRef<HTMLButtonElement>(null)

  useEffect(() => {
    let c: SelectItemsType[] = []
    if (!!sortBy) {
      c = Object.keys(sortBy).map((key) => {
        const item: SortType = { ...sortBy[key] }
        return {
          value: item.value,
          name: item.name,
          icon: item.icon,
        }
      })
    }
    if (c.length > 0) {
      setSortedItems(c)
    }
  }, [sortBy])

  const onChangeParamsHandler = (
    e: FormEvent<HTMLInputElement>,
    isDisabledField: boolean,
  ) => {
    if (isDisabledField) {
      e.preventDefault()
      return
    }
    dispatch(
      updateCheckedParams({
        keyFilter: [e.currentTarget.name],
        checked: e.currentTarget.checked,
      }),
    )
    updateCollectFilters({
      checkedParamsKeysTotal: e.currentTarget.checked
        ? [...checkedParamsKeysTotal, e.currentTarget.name]
        : checkedParamsKeysTotal.filter((c) => c !== e.currentTarget.name),
    })
  }

  const onClearFilterItemHandler = ({
    keyFilter,
    checked,
  }: {
    keyFilter: string[]
    checked: boolean
  }) => {
    dispatch(
      updateCheckedParams({
        keyFilter,
        checked,
      }),
    )
    updateCollectFilters({
      checkedParamsKeysTotal: checkedParamsKeysTotal.filter(
        (p) => !keyFilter.includes(p),
      ),
    })
  }

  const onSelectSortByHandler = useCallback(
    (value: string): void => {
      if (value !== sortBySelected) {
        updateCollectFilters({ sortBySelected: value })
      }
    },
    [sortBySelected, updateCollectFilters],
  )

  const { width } = useWindowSize()

  const [helpLink, setHelpLink] = useState<string | null>(null)

  useEffect(() => {
    setHelpLink(
      !!currentCategory?.uuid && !!categories
        ? (categories[currentCategory.uuid]?.selection_file || "").length > 0
          ? (categories[currentCategory.uuid].selection_file as string)
          : null
        : null,
    )
  }, [categories, currentCategory?.uuid])

  return (
    <StyledFiltersContainer>
      <StyledFilterRow>
        <StyledFilterTagsColumn>
          <StyledFilterGroup>
            <Tags />
          </StyledFilterGroup>
        </StyledFilterTagsColumn>
        {helpLink !== null && (
          <>
            <StyledFilterHelpColumn>
              <StyledFilterGroup>
                <Button
                  variant={"link"}
                  as={"a"}
                  href={
                    helpLink.includes("http") ? helpLink : `https://${helpLink}`
                  }
                  icon={"Info"}
                  className={cssButtonHelpChoose}
                  target={"_blank"}
                >
                  Помощь в выборе
                </Button>
              </StyledFilterGroup>
            </StyledFilterHelpColumn>
          </>
        )}
      </StyledFilterRow>
      <StyledFilterRow>
        <StyledFilterColumn>
          <StyledFilterGroup>
            {width !== undefined && width > getBreakpointVal(breakpoints.lg) && (
              <>
                <FilterParams
                  availableParams={availableParams}
                  clearFilters={clearFiltersHandler}
                  priceRange={priceRange}
                  checkedParamsKeysTotal={checkedParamsKeysTotal}
                  onUpdateFiltersHandle={updateCollectFilters}
                  onChangeCheckboxHandle={onChangeParamsHandler}
                  onClearItemHandle={onClearFilterItemHandler}
                />
              </>
            )}
            {width !== undefined && width <= getBreakpointVal(breakpoints.lg) && (
              <>
                <DynamicModal
                  closeMode={"destroy"}
                  variant={"full"}
                  title={"Фильтр"}
                  disclosure={
                    <Button
                      variant={"small"}
                      icon={"Filter"}
                      className={cx(
                        cssButtonFilterDialogTrigger,
                        (checkedParamsKeysTotal.length > 0 ||
                          !!priceRange.checked) &&
                          cssIsActive,
                      )}
                      ref={btnFilterResponsiveRef}
                    >
                      Фильтр
                    </Button>
                  }
                >
                  <FilterParams
                    availableParams={availableParams}
                    onUpdateFiltersHandle={updateCollectFilters}
                    clearFilters={clearFiltersHandler}
                    priceRange={priceRange}
                    checkedParamsKeysTotal={checkedParamsKeysTotal}
                    onChangeCheckboxHandle={onChangeParamsHandler}
                    onClearItemHandle={onClearFilterItemHandler}
                    isResponsive
                    visibleDefault
                  >
                    {sortedItems.length > 0 && (
                      <>
                        <StyledFilterGroup>
                          <Select
                            variant={"small"}
                            items={sortedItems}
                            isIcon={true}
                            ariaLabel={"sorted"}
                            initialValue={sortBySelected || undefined}
                            onSelectValue={onSelectSortByHandler}
                          />
                        </StyledFilterGroup>
                      </>
                    )}
                    <FilterRadioGroups
                      isFast={isFast}
                      updateCollectFilters={updateCollectFilters}
                      isLoading={isLoading}
                      isEnabled={isEnabled}
                    />
                  </FilterParams>
                </DynamicModal>
                <Button
                  variant={"box"}
                  icon={"Filter"}
                  className={cx(cssButtonFloat, cssButtonFill)}
                  onClick={() => {
                    btnFilterResponsiveRef.current?.click()
                  }}
                >
                  {(checkedParamsKeysTotal.length > 0 ||
                    !!priceRange.checked) && (
                    <>
                      <PushCounter>
                        {checkedParamsKeysTotal.length || 1}
                      </PushCounter>
                    </>
                  )}
                </Button>
              </>
            )}
          </StyledFilterGroup>
        </StyledFilterColumn>
        <StyledFilterColumn>
          <FilterRadioGroups
            isFast={isFast}
            updateCollectFilters={updateCollectFilters}
            isLoading={isLoading}
            isEnabled={isEnabled}
          />
          {width !== undefined && width > getBreakpointVal(breakpoints.md) && (
            <>
              <StyledFilterGroup className={cssFilterGroupViewMode}>
                <Button
                  variant={"link"}
                  icon={"ViewBoxes"}
                  isHiddenBg
                  data-view-disabled={viewProducts === "list"}
                  onClick={(e) => {
                    e.preventDefault()
                    updateViewProducts("grid")
                  }}
                />
                <Button
                  variant={"link"}
                  icon={"ViewSchedule"}
                  isHiddenBg
                  data-view-disabled={viewProducts === "grid"}
                  onClick={(e) => {
                    e.preventDefault()
                    updateViewProducts("list")
                  }}
                />
              </StyledFilterGroup>
            </>
          )}

          <StyledFilterGroup>
            {sortedItems.length > 0 && (
              <Select
                variant={"small"}
                items={sortedItems}
                isIcon={true}
                ariaLabel={"sorted"}
                initialValue={sortBySelected || undefined}
                onSelectValue={onSelectSortByHandler}
              />
            )}
          </StyledFilterGroup>
        </StyledFilterColumn>
      </StyledFilterRow>
    </StyledFiltersContainer>
  )
}
