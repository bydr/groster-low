import { styled } from "@linaria/react"
import { cssIcon } from "../Icon"
import { colors } from "../../styles/utils/vars"
import {
  cssButtonModalClose,
  StyledDialogContent,
  StyledModalBackdrop,
} from "../Modals/StyledModal"

export const NotificationWrapper = styled(StyledModalBackdrop)`
  .${cssButtonModalClose} {
    &:hover,
    &:active {
      .${cssIcon} {
        fill: ${colors.brand.purpleDarken};
      }
    }
  }

  ${StyledDialogContent} {
    box-shadow: 0 0 0 72px ${colors.gray};
  }
`
