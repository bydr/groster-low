import { FC, useEffect, useRef, useState } from "react"
import { useDialogState } from "reakit/Dialog"
import {
  cssButtonModalClose,
  cssOutlineAccent,
  Heading,
  StyledDialog,
  StyledDialogContent,
} from "../Modals/StyledModal"
import { Button } from "../Button"
import { Typography } from "../Typography/Typography"
import { cx } from "@linaria/core"
import { NotificationWrapper } from "./Styled"
import { scrollBodyDisable, scrollBodyEnable } from "../../utils/helpers"

export type NotificationPropsType = {
  isOpen?: boolean
  notification: NotificationType | null
  setNotification?: (notification: NotificationType | null) => void
  isAccentOutline?: boolean
}

export type NotificationType = {
  title?: string
  message: string
}

export const Notification: FC<NotificationPropsType> = ({
  isOpen,
  notification,
  setNotification,
  isAccentOutline,
}) => {
  const [isVisible, setIsVisible] = useState<boolean | null>(null)
  const dialog = useDialogState({
    animated: 1000,
  })
  const element = useRef<HTMLDivElement | null>(null)

  useEffect(() => {
    if (isOpen) {
      dialog.show()
    }
  }, [isOpen])

  useEffect(() => {
    if (dialog.visible) {
      setIsVisible(true)
    }
  }, [dialog.visible])

  useEffect(() => {
    if (!dialog.visible && notification !== null && isVisible !== null) {
      if (setNotification) {
        setNotification(null)
      }
    } else {
      setIsVisible(true)
    }
  }, [dialog.visible, notification, setNotification, isVisible])

  useEffect(() => {
    if (dialog.visible) {
      scrollBodyDisable(element.current || undefined)
    } else {
      scrollBodyEnable()
    }
    return () => {
      scrollBodyEnable()
    }
  }, [dialog.visible])

  return (
    <>
      <NotificationWrapper
        data-variant={"rounded-0"}
        {...dialog}
        role="alertdialog"
        ref={element}
      >
        <StyledDialog {...dialog} aria-label="dialog">
          <StyledDialogContent
            className={cx(isAccentOutline && cssOutlineAccent)}
          >
            {notification !== null && (
              <>
                {notification.title && (
                  <Heading>
                    <Typography variant={"h3"}>{notification.title}</Typography>
                  </Heading>
                )}
                <Typography>{notification.message}</Typography>
              </>
            )}

            <Button
              variant={"box"}
              icon={"X"}
              className={cssButtonModalClose}
              onClick={() => {
                dialog.hide()
              }}
            />
          </StyledDialogContent>
        </StyledDialog>
      </NotificationWrapper>
    </>
  )
}
