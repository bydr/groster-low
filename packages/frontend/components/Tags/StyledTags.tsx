import { styled } from "@linaria/react"
import { ButtonBase } from "../Button/StyledButton"
import { getTypographyBase, TypographyBase } from "../Typography/Typography"
import { breakpoints, colors } from "../../styles/utils/vars"
import { cssIsActive } from "../../styles/utils/Utils"
import { css } from "@linaria/core"
import { cssButtonTogglerTags } from "../Account/History/Orders/Filters/Styled"

export const cssTag = css`
  &,
  &${ButtonBase} {
    ${getTypographyBase("p12")};
    background: ${colors.grayLight};
    color: ${colors.black};
    margin: 0;

    &:hover,
    &:active {
      background: ${colors.gray};
      color: ${colors.black};
    }

    &[data-is-active="true"] {
      background: ${colors.grayDark};
      color: ${colors.white};
    }

    &.${cssIsActive} {
      background: ${colors.brand.purple};
      color: ${colors.brand.yellow};
      transform: scale(0.96);
    }
  }
`

export const cssTagRectification = css`
  &,
  &${ButtonBase} {
    background: ${colors.white};
    color: ${colors.grayDark};
    ${getTypographyBase("p12")};
    margin: 0;
    padding-top: 0.1em;
    padding-bottom: 0.1em;
    border: 1px solid ${colors.brand.yellow};
    
    &:hover,
    &:active {
      background: ${colors.white};
      color: ${colors.black};
      border-color: ${colors.brand.purple};
    }

    &[data-is-active="true"] {
      background: ${colors.grayDark};
      color: ${colors.white};
    }

    &.${cssIsActive} {
      background: ${colors.white};
      color: ${colors.brand.purple};
      border-color: ${colors.brand.purple};
      transform: scale(0.96);
    }
  }
  }
`

export const cssIsCategories = css`
  @media (max-width: ${breakpoints.md}) {
    display: flex;
    flex-direction: column;
    justify-content: flex-start;

    > .${cssTag}, .${cssTagRectification} {
      &,
      &${ButtonBase} {
        ${getTypographyBase("p13")};
        font-weight: 600;
        border-radius: 0;
        width: 100%;
        border: none;
        text-align: left;
        margin: 0;
        justify-content: flex-start;
        border-bottom: 1px solid ${colors.gray};
        padding: 8px 5px;
        color: ${colors.brand.purple};

        &:nth-last-child(2) {
          border-bottom: none;
        }
      }
    }
  }
`

export const StyledTags = styled.div`
  display: flex;
  width: 100%;
  flex-wrap: wrap;
  flex-direction: row;
  margin-bottom: 20px;
  gap: 5px;
  align-items: center;

  .${cssButtonTogglerTags} {
    ${getTypographyBase("p12")};
    padding-top: 0.1em;
    padding-bottom: 0.1em;
    font-weight: 600;
  }
`

export const StyledTagsContainer = styled.section`
  display: flex;
  align-items: flex-start;
  flex-wrap: wrap;

  ${StyledTags} {
    flex: 1;
    width: auto;
  }

  > ${TypographyBase} {
    color: ${colors.grayDark};
    margin: 0 10px 10px 0;

    @media (max-width: ${breakpoints.sm}) {
      display: none;
    }
  }
`
