import { FC } from "react"
import { priceToFormat } from "../../../utils/helpers"
import { StyledFreeShippingMessage } from "./styled"
import { ActionBar } from "../../ActionBar/ActionBar"
import { CURRENCY } from "../../../utils/constants"

export const FreeShippingCourierMessage: FC<{
  leftFreeShippingCourier: number | null
}> = ({ leftFreeShippingCourier }) => {
  return (
    <>
      {leftFreeShippingCourier !== null && (
        <>
          <StyledFreeShippingMessage>
            <ActionBar
              variant={"blue"}
              description={
                leftFreeShippingCourier > 0 ? (
                  <>
                    Купите товаров ещё на{" "}
                    {priceToFormat(leftFreeShippingCourier)}
                    {CURRENCY} и получите <b>доставку курьером бесплатно</b>.
                  </>
                ) : (
                  <>
                    <b>Доставка курьером бесплатна для Вас</b>
                  </>
                )
              }
            />
          </StyledFreeShippingMessage>
        </>
      )}
    </>
  )
}
