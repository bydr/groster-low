import { styled } from "@linaria/react"
import { StyledActionBar } from "../../ActionBar/StyledActionBar"
import { getTypographyBase, TypographyBase } from "../../Typography/Typography"

export const StyledFreeShippingMessage = styled.div`
  width: 100%;

  ${StyledActionBar} {
    width: 100%;
    padding: 20px;
    display: inline-block;

    > * {
      display: inline-block;
    }

    ${TypographyBase} {
      ${getTypographyBase("p14")};
      margin-bottom: 0;
    }
  }
`
