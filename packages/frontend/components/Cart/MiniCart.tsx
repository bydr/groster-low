import { FC } from "react"
import { Panel } from "../../styles/utils/StyledPanel"
import { StyledMiniCart } from "./StyledCart"
import { VIEW_PRODUCTS_CHECKOUT_MINI } from "../../types/types"
import dynamic, { DynamicOptions } from "next/dynamic"
import { CartPropsType } from "./Cart"

const DynamicCart = dynamic((() =>
  import("./Cart").then((mod) => mod.Cart)) as DynamicOptions<CartPropsType>)

export const MiniCart: FC = () => {
  return (
    <>
      <StyledMiniCart>
        <Panel>
          <DynamicCart
            viewProductsVariant={VIEW_PRODUCTS_CHECKOUT_MINI}
            isSaveOnRemove={false}
          />
        </Panel>
      </StyledMiniCart>
    </>
  )
}
