import { styled } from "@linaria/react"
import { StyledListItem } from "../../List/StyledList"
import { ButtonBase } from "../../Button/StyledButton"
import { getTypographyBase } from "../../Typography/Typography"

export const StyledSummary = styled.div`
  ${ButtonBase} {
    width: 100%;
  }
`

export const StyledSummaryItemBody = styled.div``

export const StyledSummaryCol = styled.div`
  ${getTypographyBase("p12")};
`

export const StyledSummaryItem = styled(StyledListItem)<{ color?: string }>`
  justify-content: space-between;
  width: 100%;
  color: ${(props) => props.color || "inherit"};

  &:last-of-type {
    margin-bottom: 0;
  }

  > * {
    color: inherit;
    margin-bottom: 0;
    &:first-child {
      max-width: 70%;
    }
  }

  ${StyledSummaryItemBody} {
    max-width: 100%;
    flex: 1;
  }
`
