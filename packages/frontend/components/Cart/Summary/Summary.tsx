import type { BaseHTMLAttributes, FC, ReactNode } from "react"
import { useEffect, useState } from "react"
import { Panel, PanelHeading } from "../../../styles/utils/StyledPanel"
import { Typography } from "../../Typography/Typography"
import { cssListVertical, StyledList } from "../../List/StyledList"
import {
  StyledSummary,
  StyledSummaryCol,
  StyledSummaryItem,
  StyledSummaryItemBody,
} from "./StyledSummary"
import { Hr, RowOffset } from "../../../styles/utils/Utils"
import { ButtonGroup, cssButtonGroupVertical } from "../../Button/StyledButton"
import { CurrencyType } from "../../../types/types"
import {
  dateToString,
  isCurrentYear,
  priceToFormat,
} from "../../../utils/helpers"
import { useCart } from "../../../hooks/cart"
import { usePromocode } from "../../../hooks/promocode"
import { colors } from "../../../styles/utils/vars"
import { useShippings } from "../../../hooks/shippings"
import Price from "../../Products/parts/Price/Price"
import { CURRENCY } from "../../../utils/constants"

const SummaryItem: FC<
  BaseHTMLAttributes<HTMLLIElement> & {
    title?: string
    value?: string | number | ReactNode
    type?: "red"
  }
> = ({ title, value, type, children, ...props }) => {
  const [color, setColor] = useState<string | null>(null)

  useEffect(() => {
    switch (type) {
      case "red": {
        setColor(colors.red)
        break
      }
      default: {
        setColor(null)
        break
      }
    }
  }, [type])

  return (
    <>
      <StyledSummaryItem
        {...props}
        color={color !== null ? color : props.color}
      >
        {title && <StyledSummaryCol>{title}</StyledSummaryCol>}
        {value && <StyledSummaryCol>{value}</StyledSummaryCol>}
        {children && (
          <>
            <StyledSummaryItemBody>{children}</StyledSummaryItemBody>
          </>
        )}
      </StyledSummaryItem>
    </>
  )
}

const Summary: FC = ({ children }) => {
  const { totalCost, token, cartCost, nextShippingDate } = useCart()
  const { shippingCost, updateShippingCost } = useShippings()
  const { discount, promocode } = usePromocode({ cart: token })
  const currency: CurrencyType = CURRENCY

  useEffect(() => {
    return function cleanup() {
      updateShippingCost(null)
    }
  }, [updateShippingCost])

  return (
    <>
      <StyledSummary>
        <Panel>
          <PanelHeading>Ваш заказ</PanelHeading>
          <StyledList className={cssListVertical}>
            {nextShippingDate !== null && (
              <>
                <SummaryItem color={colors.grayDark}>
                  <Typography variant={"p12"}>
                    Ближайшая дата доставки:{" "}
                    {dateToString(
                      nextShippingDate,
                      !isCurrentYear(nextShippingDate),
                    )}
                  </Typography>
                </SummaryItem>
              </>
            )}
            <SummaryItem
              title={"Товары"}
              value={<Price price={cartCost} currency={currency} isClean />}
            />
            {promocode !== null && (
              <>
                <SummaryItem
                  title={`Экономия с промокодом ${promocode}`}
                  value={
                    <Price
                      price={discount}
                      currency={currency}
                      isClean
                      isSale
                    />
                  }
                  type={"red"}
                />
              </>
            )}
            {shippingCost !== null && (
              <>
                <SummaryItem
                  title={`Доставка`}
                  value={
                    shippingCost > 0
                      ? `${priceToFormat(shippingCost)} ${currency}`
                      : "бесплатно"
                  }
                />
              </>
            )}
          </StyledList>
          <Hr />
          <RowOffset>
            <StyledSummaryItem as={"div"}>
              <Typography>Итого</Typography>
              <Price
                price={totalCost}
                currency={currency}
                variant={"total"}
                isClean
              />
            </StyledSummaryItem>
          </RowOffset>

          <ButtonGroup className={cssButtonGroupVertical}>
            {children}
          </ButtonGroup>
        </Panel>
      </StyledSummary>
    </>
  )
}

export default Summary
