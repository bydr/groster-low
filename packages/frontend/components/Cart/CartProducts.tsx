import { FC, useMemo } from "react"
import { Products } from "../Products/Products"
import { ProductKit } from "../Products/Cart/Product/ProductKit"
import { Product } from "../Products/Cart/Product/Product"
import { Samples } from "../Products/Cart/Samples/Samples"
import {
  ProductWithChildType,
  SpecificationItemType,
  VIEW_PRODUCTS_CHECKOUT,
  VIEW_PRODUCTS_CHECKOUT_MINI,
  VIEW_PRODUCTS_SAMPLE_MINI,
  WithViewProductVariantType,
} from "../../types/types"
import { ContentContainer } from "../../styles/utils/StyledGrid"

export const CartProducts: FC<
  WithViewProductVariantType & {
    products: Record<string, ProductWithChildType> | null
    samples: Record<string, ProductWithChildType> | null
    specification: Record<string, SpecificationItemType> | null
  } & { isSaveOnRemove?: boolean }
> = ({
  viewProductsVariant = VIEW_PRODUCTS_CHECKOUT,
  products,
  samples,
  isSaveOnRemove,
  specification,
}) => {
  const productsGrouped = useMemo(() => {
    const group = {
      available: {} as Record<string, ProductWithChildType>,
      notAvailable: {} as Record<string, ProductWithChildType>,
    }
    if (products !== null && specification !== null) {
      for (const [uuid] of Object.entries(specification || {})) {
        if (!products[uuid]) {
          continue
        }
        if (!products[uuid].total_qty) {
          group.notAvailable[uuid] = { ...products[uuid] }
        } else {
          group.available[uuid] = { ...products[uuid] }
        }
      }
    }

    return group
  }, [products, specification])

  return (
    <>
      <ContentContainer>
        {productsGrouped.available !== null &&
          Object.keys(productsGrouped.available).length > 0 && (
            <>
              <Products view={viewProductsVariant} withBorder>
                {Object.keys(productsGrouped.available).map((key) => {
                  if (
                    productsGrouped.available[key].kit !== undefined &&
                    (productsGrouped.available[key].kit || []).length > 0
                  ) {
                    return (
                      <ProductKit
                        key={productsGrouped.available[key].uuid}
                        view={viewProductsVariant}
                        product={productsGrouped.available[key]}
                        isSaveOnRemove={isSaveOnRemove}
                      />
                    )
                  } else {
                    return (
                      <Product
                        key={productsGrouped.available[key].uuid}
                        view={viewProductsVariant}
                        product={productsGrouped.available[key]}
                        isSaveOnRemove={isSaveOnRemove}
                      />
                    )
                  }
                })}
              </Products>
            </>
          )}

        <Samples
          samples={samples}
          viewProductsVariant={
            viewProductsVariant === VIEW_PRODUCTS_CHECKOUT_MINI
              ? VIEW_PRODUCTS_SAMPLE_MINI
              : undefined
          }
        />

        {productsGrouped.notAvailable !== null &&
          Object.keys(productsGrouped.notAvailable).length > 0 && (
            <>
              <Products withBorder titleList={"Нет в наличии"}>
                {Object.keys(productsGrouped.notAvailable).map((key) => {
                  return (
                    <Product
                      key={productsGrouped.notAvailable[key].uuid}
                      view={viewProductsVariant}
                      product={productsGrouped.notAvailable[key]}
                      isSaveOnRemove={isSaveOnRemove}
                    />
                  )
                })}
              </Products>
            </>
          )}
      </ContentContainer>
    </>
  )
}
