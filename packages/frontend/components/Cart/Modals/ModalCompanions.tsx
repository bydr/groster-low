import type { FC } from "react"
import { useMemo, useState } from "react"
import { Button } from "../../Button"
import { ProductsSlider } from "../../Products/Slider/ProductsSlider"
import { Modal } from "../../Modals/Modal"
import { productsAPI } from "../../../api/productsAPI"
import { getBreakpointVal } from "../../../styles/utils/Utils"
import { breakpoints } from "../../../styles/utils/vars"
import { BaseLoader } from "../../Loaders/BaseLoader/BaseLoader"
import { Typography } from "../../Typography/Typography"
import { SortedTags } from "./SortedTags"
import {
  filterProductsByCategory,
  RuleSortProduct,
  sortProductsWeightRule,
} from "../../../utils/helpers"

export type ModalCompanionsPropsType = {
  companions?: RuleSortProduct[]
}

export const ModalCompanions: FC<ModalCompanionsPropsType> = ({
  companions,
}) => {
  const [companionsUuids, setCompanionsUuids] = useState<string | null>(null)
  const [selectedCategory, setSelectedCategory] = useState<string | null>(null)

  const { data: dataCompanions, isFetching } =
    productsAPI.useProductsFetch(companionsUuids)

  const products = useMemo(
    () =>
      sortProductsWeightRule({
        products: dataCompanions || [],
        rules: companions,
      }),
    [dataCompanions, companions],
  )

  return (
    <>
      {!!companions && companions.length > 0 && (
        <Modal
          closeMode={"destroy"}
          disclosure={
            <Button
              variant={"small"}
              icon={"Transaction"}
              onClick={() => {
                setCompanionsUuids(
                  companions.map((item) => item.uuid).join(","),
                )
              }}
            >
              Смотреть сопутствующие товары
            </Button>
          }
          title={"Сопутствующие товары"}
          variant={"rounded-100"}
        >
          {isFetching && <BaseLoader />}

          <SortedTags
            products={products}
            onSelect={(uuid) => {
              setSelectedCategory(uuid)
            }}
          />

          {products.length > 0 ? (
            <ProductsSlider
              ruleSort={companions}
              products={
                selectedCategory === null
                  ? products
                  : filterProductsByCategory({
                      products: products,
                      category: selectedCategory,
                    })
              }
              variantArrow={"left-right"}
              responsiveExtends={{
                [getBreakpointVal(breakpoints.lg)]: {
                  slidesPerView: 4,
                },
                0: {
                  slidesPerView: 1,
                },
              }}
              disabledOnMobile
            />
          ) : (
            <Typography variant={"p14"}>Сопутствующих не найдено</Typography>
          )}
        </Modal>
      )}
    </>
  )
}
