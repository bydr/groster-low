import type { FC } from "react"
import { useCallback, useEffect, useMemo, useState } from "react"
import { Modal } from "../../Modals/Modal"
import { Button } from "../../Button"
import { ProductsSlider } from "../../Products/Slider/ProductsSlider"
import { productsAPI } from "../../../api/productsAPI"
import { getBreakpointVal } from "../../../styles/utils/Utils"
import { breakpoints } from "../../../styles/utils/vars"
import { BaseLoader } from "../../Loaders/BaseLoader/BaseLoader"
import { Typography } from "../../Typography/Typography"
import { SortedTags } from "./SortedTags"
import {
  filterProductsByCategory,
  RuleSortProduct,
  sortProductsWeightRule,
} from "../../../utils/helpers"

export type ModalAnalogsPropsType = {
  productAnalogs?: RuleSortProduct[]
  isShowAnalogs?: boolean
  setIsShowAnalogs?: (val: boolean) => void
}

export const ModalAnalogs: FC<ModalAnalogsPropsType> = ({
  productAnalogs,
  isShowAnalogs,
  setIsShowAnalogs,
}) => {
  const [analogsUuids, setAnalogsUuids] = useState<string | null>(null)
  const { data: dataAnalogs, isFetching } =
    productsAPI.useProductsFetch(analogsUuids)

  const [selectedCategory, setSelectedCategory] = useState<string | null>(null)

  const initAnalogs = useCallback(() => {
    if (productAnalogs !== undefined && productAnalogs.length > 0) {
      setAnalogsUuids(productAnalogs.map((item) => item.uuid).join(","))
    }
  }, [productAnalogs])

  const products = useMemo(
    () =>
      sortProductsWeightRule({
        products: dataAnalogs || [],
        rules: productAnalogs,
      }),
    [dataAnalogs, productAnalogs],
  )

  useEffect(() => {
    if (!!isShowAnalogs) {
      initAnalogs()
    }
  }, [initAnalogs, isShowAnalogs])

  return (
    <>
      {!!productAnalogs && productAnalogs.length > 0 && (
        <>
          <Modal
            isShowModal={isShowAnalogs}
            closeMode={"destroy"}
            onClose={() => {
              if (setIsShowAnalogs) {
                setIsShowAnalogs(false)
              }
            }}
            disclosure={
              <Button
                variant={"small"}
                icon={"Copy"}
                onClick={() => {
                  initAnalogs()
                }}
              >
                Смотреть аналоги
              </Button>
            }
            title={"Аналоги"}
            variant={"rounded-100"}
          >
            <SortedTags
              products={products}
              onSelect={(uuid) => {
                setSelectedCategory(uuid)
              }}
            />
            {isFetching && <BaseLoader />}
            {products.length > 0 ? (
              <ProductsSlider
                ruleSort={productAnalogs}
                products={
                  selectedCategory === null
                    ? products
                    : filterProductsByCategory({
                        products: products,
                        category: selectedCategory,
                      })
                }
                variantArrow={"left-right"}
                responsiveExtends={{
                  [getBreakpointVal(breakpoints.lg)]: {
                    slidesPerView: 4,
                  },
                  0: {
                    slidesPerView: 1,
                  },
                }}
                disabledOnMobile
              />
            ) : (
              <Typography variant={"p14"}>Аналогов не найдено</Typography>
            )}
          </Modal>
        </>
      )}
    </>
  )
}
