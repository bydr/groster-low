import { FC, useCallback, useEffect, useState } from "react"
import { ProductDetailListType } from "../../../../../contracts/contracts"
import { Button } from "../../../Button"
import { cssTag, StyledTags } from "../../../Tags/StyledTags"
import { useAppSelector } from "../../../../hooks/redux"
import { cx } from "@linaria/core"
import { cssIsActive } from "../../../../styles/utils/Utils"
import { ShortCategoryType } from "../../../../types/types"

type SortedTagsPropsType = {
  products: ProductDetailListType | null
  onSelect?: (uuid: string | null) => void
}

export const SortedTags: FC<SortedTagsPropsType> = ({ products, onSelect }) => {
  const [tagsCategories, setTagsCategories] = useState<ShortCategoryType[]>([])
  const categories = useAppSelector((state) => state.catalog.categories)
  const [selectedUuid, setSelectedUuid] = useState<string | null>(null)

  const update = useCallback((uuid: string | null) => {
    setSelectedUuid(uuid)
    if (onSelect) {
      onSelect(uuid)
    }
  }, [])

  const onClickHandle = (uuid: string) => {
    update(selectedUuid === uuid ? null : uuid)
  }

  useEffect(() => {
    return () => {
      update(null)
    }
  }, [update])

  useEffect(() => {
    if (!!products && products.length > 0) {
      if (categories !== null && categories.fetched !== null) {
        const tags: ShortCategoryType[] = []
        let productCategoriesIds: string[] = []
        for (const p of products) {
          productCategoriesIds = [
            ...productCategoriesIds,
            ...(p.categories || []),
          ]
        }
        productCategoriesIds = productCategoriesIds.reduce(
          (uniq: string[], item) => {
            return uniq.includes(item) ? uniq : [...uniq, item]
          },
          [],
        )

        productCategoriesIds.map((id) => {
          if (categories.fetched !== null) {
            const fetchProductCat = categories.fetched[id]
            if (fetchProductCat !== undefined) {
              tags.push({
                uuid: `${fetchProductCat.uuid}`,
                name: `${fetchProductCat.name}`,
              })
            }
          }
        })

        setTagsCategories(tags)
      }
    }
  }, [categories, products])

  return (
    <>
      {tagsCategories.length > 0 && (
        <>
          <StyledTags>
            {tagsCategories.map((c) => (
              <Button
                className={cx(
                  cssTag,
                  selectedUuid !== null &&
                    selectedUuid === c.uuid &&
                    cssIsActive,
                )}
                key={c.uuid || ""}
                onClick={() => {
                  onClickHandle(c.uuid || "")
                }}
              >
                {c.name}
              </Button>
            ))}
          </StyledTags>
        </>
      )}
    </>
  )
}
