import { BaseHTMLAttributes, FC, memo, useEffect, useState } from "react"
import {
  ContentContainer,
  SidebarContainer,
} from "../../styles/utils/StyledGrid"
import {
  VIEW_PRODUCTS_CHECKOUT,
  VIEW_PRODUCTS_CHECKOUT_MINI,
  WithViewProductVariantType,
} from "../../types/types"
import { StickyContainer } from "../../styles/utils/Utils"
import Summary from "./Summary/Summary"
import { useCart } from "../../hooks/cart"
import { BaseLoader } from "../Loaders/BaseLoader/BaseLoader"
import { CartProducts } from "./CartProducts"
import { Promocode } from "./Promocode/Promocode"
import { useAuth } from "../../hooks/auth"
import { Button } from "../Button"
import { ROUTES } from "../../utils/constants"
import { FreeShippingCourierMessage } from "./LeftFreeShippingCourierMessage"
import { ButtonFastOrder } from "../Products/parts/ButtonFastOrder/ButtonFastOrder"
import { Typography } from "../Typography/Typography"
import { useWindowSize } from "../../hooks/windowSize"
import { breakpoints } from "../../styles/utils/vars"
import { cssSidebarCart, cssSidebarCartResponsive } from "./StyledCart"
import { useShippings } from "../../hooks/shippings"
import { cx } from "@linaria/core"

export type CartPropsType = WithViewProductVariantType & {
  isSaveOnRemove?: boolean
}

const SidebarCart: FC<
  WithViewProductVariantType & BaseHTMLAttributes<HTMLDivElement>
> = ({ viewProductsVariant, ...props }) => {
  const { totalCost } = useCart()
  const { leftFreeShippingCourier } = useShippings()
  const { isInit, isAuth } = useAuth()
  return (
    <>
      <SidebarContainer {...props}>
        <StickyContainer>
          {isInit &&
            isAuth &&
            viewProductsVariant !== VIEW_PRODUCTS_CHECKOUT_MINI && (
              <>
                <Promocode />
              </>
            )}
          <Summary>
            {viewProductsVariant === VIEW_PRODUCTS_CHECKOUT_MINI ? (
              <>
                <Button
                  as={"a"}
                  href={ROUTES.cart}
                  variant={"filled"}
                  size={"large"}
                  fullWidth
                >
                  В корзину
                </Button>
                <Button
                  as={"a"}
                  href={ROUTES.checkout}
                  variant={"outline"}
                  size={"large"}
                  fullWidth
                  disabled={totalCost <= 0}
                >
                  Оформить заказ
                </Button>
              </>
            ) : (
              <>
                <Button
                  as={"a"}
                  href={ROUTES.checkout}
                  variant={"filled"}
                  size={"large"}
                  fullWidth
                  disabled={totalCost <= 0}
                >
                  Оформить заказ
                </Button>
                <ButtonFastOrder disabled={totalCost <= 0} size={"large"} />
              </>
            )}
          </Summary>
          <FreeShippingCourierMessage
            leftFreeShippingCourier={leftFreeShippingCourier}
          />
        </StickyContainer>
      </SidebarContainer>
    </>
  )
}

export const Cart: FC<CartPropsType> = memo(
  ({ viewProductsVariant = VIEW_PRODUCTS_CHECKOUT, isSaveOnRemove }) => {
    const { isFetching, products, samples, specification } = useCart()
    const [isEmptyCart, setIsEmptyCart] = useState(true)
    const { width } = useWindowSize()

    useEffect(() => {
      setIsEmptyCart(
        !(
          Object.keys(products || {}).length > 0 ||
          Object.keys(samples || {}).length > 0
        ),
      )
    }, [products, samples])

    return (
      <>
        {isFetching && <BaseLoader />}
        {isEmptyCart && (
          <>
            <ContentContainer>
              <Typography variant={"h5"}>Корзина пуста</Typography>
            </ContentContainer>
          </>
        )}
        {!isEmptyCart && (
          <>
            <CartProducts
              viewProductsVariant={viewProductsVariant}
              products={products}
              samples={samples}
              isSaveOnRemove={isSaveOnRemove}
              specification={specification}
            />
            <SidebarCart
              viewProductsVariant={viewProductsVariant}
              className={cssSidebarCart}
            />
            {width !== undefined && width < +breakpoints.lg.replace("px", "") && (
              <>
                <SidebarCart
                  viewProductsVariant={viewProductsVariant}
                  className={cx(cssSidebarCartResponsive, cssSidebarCart)}
                />
              </>
            )}
          </>
        )}
      </>
    )
  },
)

Cart.displayName = "Cart"
