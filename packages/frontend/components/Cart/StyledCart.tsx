import {
  Panel,
  PanelHeading,
  PanelWrapperAbsolute,
} from "../../styles/utils/StyledPanel"
import { styled } from "@linaria/react"
import {
  ContentContainer,
  SidebarContainer,
} from "../../styles/utils/StyledGrid"
import { StyledProducts } from "../Products/StyledProducts"
import { breakpoints, colors } from "../../styles/utils/vars"
import { getTypographyBase, Heading3, Heading5 } from "../Typography/Typography"
import { cssTooltipDate, StyledProductTag } from "../Products/StyledProduct"
import { getCustomizeScroll, Hr } from "../../styles/utils/Utils"
import { StyledActionBar } from "../ActionBar/StyledActionBar"
import { StyledList } from "../List/StyledList"
import { StyledSummaryItem } from "./Summary/StyledSummary"
import { css } from "@linaria/core"

export const StyledTitleCart = styled.p`
  ${getTypographyBase("h3")};
`

export const cssSidebarCart = css`
  &,
  &${SidebarContainer} {
    padding-top: 0;

    @media (max-width: ${breakpoints.lg}) {
      padding-top: 0;
    }
  }
`

export const StyledMiniCart = styled(PanelWrapperAbsolute)`
  z-index: 9;
  overflow: hidden;
  max-width: 375px;
  min-width: 375px;

  ${StyledTitleCart} {
    ${getTypographyBase("h5")};
    margin: 15px;
  }

  ${ContentContainer} {
    ${getCustomizeScroll()};
    max-height: 40vh;
    overflow-x: hidden;
    overflow-y: auto;
    padding: 0;
    border-radius: inherit;

    > ${Heading5} {
      width: auto;
      margin: 14px;
    }
  }

  ${Panel} {
    margin-bottom: 0;
  }

  > ${Panel} {
    padding: 0;
    > ${PanelHeading} {
      padding: 24px 15px;
      margin: 0;
      border-bottom: 1px solid ${colors.gray};
    }
  }

  ${StyledProductTag} {
    &[data-is-kit="true"]:not([data-is-removed="true"]) {
      background: transparent;
    }
  }

  ${StyledProducts} {
    border: none;
    margin-bottom: 0;

    & + ${StyledProducts} {
      margin-top: 20px;
    }

    > ${Heading3} {
      margin: 24px 0 0 15px;
    }
  }

  ${SidebarContainer} {
    ${Panel} {
      border-left: 0;
      border-right: 0;
      border-bottom: 0;
      border-top-left-radius: 0;
      border-top-right-radius: 0;
      padding: 15px;
    }
  }

  ${StyledActionBar} {
    width: auto;
    margin-right: 15px;
    margin-left: 15px;
    display: flex;
  }

  ${StyledList} {
    display: none;
  }

  ${Hr} {
    display: none;
  }

  ${StyledSummaryItem} {
    > * {
      ${getTypographyBase("p12")};
    }
  }

  .${cssSidebarCart} {
    &,
    &${SidebarContainer} {
      padding: 0;
    }
  }

  ${StyledProductTag} {
    .${cssTooltipDate} {
      display: none;
    }
  }

  @media (max-width: ${breakpoints.lg}) {
    display: none;
  }
`

export const StyledCartContainer = styled.section`
  width: 100%;
`

export const cssSidebarCartResponsive = css`
  grid-row: initial;
`
