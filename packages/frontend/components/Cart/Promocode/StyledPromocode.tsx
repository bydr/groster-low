import { styled } from "@linaria/react"
import { TypographyBase } from "../../Typography/Typography"
import { colors } from "../../../styles/utils/vars"
import { ButtonBase } from "../../Button/StyledButton"
import { StyledFieldWrapper } from "../../Field/StyledField"

export const StyledPromocodeItem = styled.div`
  display: flex;
  width: 100%;
  align-items: center;
  justify-content: space-between;
  border-radius: 0.5em;
  background: ${colors.grayLighter};
  padding: 0 0 0 10px;
  color: ${colors.brand.purple};

  ${TypographyBase} {
    flex: 1;
    font-weight: 600;
    margin-bottom: 0;
  }

  ${ButtonBase} {
    background-color: transparent;
  }
`

export const StyledPromocode = styled.div`
  width: 100%;

  ${StyledFieldWrapper} {
    margin-bottom: 0;
  }
`
