import type { FC } from "react"
import { Field } from "../../Field/Field"
import { Panel, PanelHeading } from "../../../styles/utils/StyledPanel"
import { StyledForm } from "../../Forms/StyledForms"
import { useForm } from "react-hook-form"
import { usePromocode } from "../../../hooks/promocode"
import { SingleError, Typography } from "../../Typography/Typography"
import { useAppSelector } from "../../../hooks/redux"
import { Button } from "../../Button"
import { StyledPromocode, StyledPromocodeItem } from "./StyledPromocode"

type PromoCodeFieldsType = {
  promocode: string
}

export const Promocode: FC = () => {
  const tokenCart = useAppSelector((state) => state.cart.token)
  const { isFetching, apply, cancel, error, promocode } = usePromocode({
    cart: tokenCart,
  })

  const {
    handleSubmit,
    register,
    formState: { errors },
  } = useForm<PromoCodeFieldsType>()

  const onSubmitHandle = handleSubmit((data) => {
    apply(data.promocode)
  })

  return (
    <>
      <StyledPromocode>
        <Panel onSubmit={onSubmitHandle}>
          <PanelHeading>Промокод</PanelHeading>
          {promocode !== null ? (
            <>
              <StyledPromocodeItem>
                <Typography variant={"p14"}>{promocode}</Typography>
                <Button
                  variant={"box"}
                  icon={"Delete"}
                  isFetching={isFetching}
                  onClick={cancel}
                />
              </StyledPromocodeItem>
            </>
          ) : (
            <>
              <StyledForm>
                <Field
                  type={"text"}
                  placeholder={"Промокод"}
                  withAnimatingLabel
                  withButton
                  iconButton={"ArrowRight"}
                  errorMessage={errors.promocode?.message}
                  {...register("promocode", {
                    required: {
                      value: true,
                      message: "Введите промокод",
                    },
                  })}
                  isFetching={isFetching}
                />
                {error !== null && <SingleError>{error}</SingleError>}
              </StyledForm>
            </>
          )}
        </Panel>
      </StyledPromocode>
    </>
  )
}
