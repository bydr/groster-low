import { css } from "@linaria/core"
import { colors } from "../../styles/utils/vars"
import { styled } from "@linaria/react"

export const cssNavIsDisabled = css``

export const cssBulletActive = css`
  background: ${colors.brand.purple};
  opacity: 1;
`
export const cssBullet = css`
  background: ${colors.gray};
  opacity: 1;
  width: 5px;
  height: 5px;
  min-height: 5px;
  min-width: 5px;
  border-radius: 50px;
  display: inline-block;
  margin: 2px;
  transition: all 0.2s ease-in-out;

  &.${cssBulletActive} {
    background: ${colors.brand.purple};
  }
`

export const StyledSequenceItem = styled.div``
export const StyledPaginationSequence = styled.div``
export const cssPaginationSequence = css`
  width: 100%;
  height: 100%;
  display: flex;
  flex-direction: row;
  position: absolute;
  top: 0;
  z-index: 2;

  ${StyledSequenceItem} {
    flex: 1;
  }
`

export const cssNavNext = css``
export const cssNavPrev = css``

export const cssCustomSwiper = css`
  &.${cssNavIsDisabled} {
    .${cssNavNext}, .${cssNavPrev} {
      display: none !important;
    }
  }
`
