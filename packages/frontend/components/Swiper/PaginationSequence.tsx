import { FC, useEffect, useMemo, useState } from "react"
import { useSwiper } from "swiper/react"
import {
  cssPaginationSequence,
  StyledPaginationSequence,
  StyledSequenceItem,
} from "./StyledSwiper"
import { useWindowSize } from "../../hooks/windowSize"
import { getBreakpointVal } from "../../styles/utils/Utils"
import { breakpoints } from "../../styles/utils/vars"
import { Link } from "../Link"

export type SwiperPaginationSequencePropsType = {
  count?: number
  path?: string
}

export const SwiperPaginationSequence: FC<
  SwiperPaginationSequencePropsType
> = ({ count = 0, path }) => {
  const swiper = useSwiper()
  const [countItems, setCountItems] = useState<number[]>([])
  const { width } = useWindowSize()

  useEffect(() => {
    const items = []
    for (let i = 1; i <= count; i++) {
      items.push(i)
    }
    setCountItems(items)
  }, [count])

  const Component = useMemo(
    () => (!!path ? Link : StyledPaginationSequence),
    [path],
  )

  return (
    <>
      {width !== undefined && width >= getBreakpointVal(breakpoints.md) && (
        <Component className={cssPaginationSequence} href={path || ""}>
          {countItems.map((_, i) => (
            <StyledSequenceItem
              key={i}
              onMouseEnter={() => {
                swiper.slideTo(i)
              }}
            />
          ))}
        </Component>
      )}
    </>
  )
}
