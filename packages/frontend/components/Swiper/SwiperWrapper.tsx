import { FC } from "react"
import type { SwiperProps } from "swiper/react"
import "swiper/css"
import "swiper/css/pagination"
import "swiper/css/free-mode"
import "swiper/css/navigation"
import "swiper/css/thumbs"
import "swiper/css/effect-fade"
import {
  SwiperPaginationSequence,
  SwiperPaginationSequencePropsType,
} from "./PaginationSequence"
import { cx } from "@linaria/core"
import { cssCustomSwiper } from "./StyledSwiper"
import dynamic, { DynamicOptions } from "next/dynamic"
import type { Swiper } from "swiper"
import { NavigationOptions } from "swiper/types/modules/navigation"

const DynamicSwiper = dynamic(
  (() =>
    import("swiper/react").then(
      (mod) => mod.Swiper,
    )) as DynamicOptions<SwiperProps>,
  {
    ssr: false,
  },
)

export type NavigationInitPropsType = {
  swiper: Swiper
} & Pick<NavigationOptions, "prevEl" | "nextEl">
type NavigationInitType = (props: NavigationInitPropsType) => void

export const navigationInit: NavigationInitType = ({
  swiper,
  nextEl,
  prevEl,
}) => {
  if (swiper.params.navigation !== undefined) {
    swiper.params.navigation["prevEl"] = prevEl
    swiper.params.navigation["nextEl"] = nextEl
  }
  swiper.navigation.init()
  swiper.navigation.update()
}

export const navigationDestroy = ({ swiper }: { swiper: Swiper }): void => {
  swiper.navigation.destroy()
}

export const SwiperWrapper: FC<
  SwiperProps & {
    paginationSequence?: boolean
    path?: string
  } & SwiperPaginationSequencePropsType
> = ({
  paginationSequence = false,
  path,
  count,
  className,
  children,
  ...props
}) => {
  return (
    <DynamicSwiper {...props} className={cx(className, cssCustomSwiper)}>
      {children}
      {paginationSequence && (
        <>
          <SwiperPaginationSequence count={count} path={path} />
        </>
      )}
    </DynamicSwiper>
  )
}
