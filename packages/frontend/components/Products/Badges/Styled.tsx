import { styled } from "@linaria/react"
import { Paragraph12 } from "../../Typography/Typography"
import { css } from "@linaria/core"
import { colors } from "../../../styles/utils/vars"

export const cssIsKit = css``
export const cssIsBestseller = css``
export const cssIsNew = css``

export const StyledName = styled(Paragraph12)`
  font-weight: bold;
  color: inherit;
  margin: 0;
  line-height: 100%;
`
export const StyledBadge = styled.div`
  color: ${colors.black};
  background: ${colors.white};
  display: inline-flex;
  padding: 4px 8px;
  border-radius: 50px;

  &.${cssIsKit} {
    background: ${colors.brand.purpleDarken};
    color: ${colors.white};
  }

  &.${cssIsBestseller} {
    background: ${colors.brand.orange};
    color: ${colors.white};
  }

  &.${cssIsNew} {
    background: ${colors.red};
    color: ${colors.white};
  }
`
export const StyledBadges = styled.div`
  width: 100%;
  display: flex;
  flex-direction: row;
  flex-wrap: wrap;

  ${StyledBadge} {
    margin-bottom: 2px;
    margin-right: 2px;
  }
`
