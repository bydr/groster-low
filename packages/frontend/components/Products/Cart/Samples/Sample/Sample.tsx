import type { FC } from "react"
import { ProductContainer, StyledProduct } from "../../../StyledProduct"
import { EntityImage } from "../../../../EntityImage/EntityImage"
import { Counter, MACROS_ERROR_COUNT } from "../../../parts/Counter/Counter"
import Price from "../../../parts/Price/Price"
import {
  SampleType,
  WithViewProductVariantType,
} from "../../../../../types/types"
import { useSample } from "../../../../../hooks/sample"
import { DeliveryMessage } from "../../../parts/DeliveryMessage/DeliveryMessage"
import { ControlsProduct } from "../../Product/Controls/ControlsProduct"
import { Title } from "../../../parts/Title/Title"
import { useShippings } from "../../../../../hooks/shippings"
import { useAppSelector } from "../../../../../hooks/redux"
import dynamic, { DynamicOptions } from "next/dynamic"
import { NoticeDiffDeliveryDatePropsType } from "../../../parts/NoticeDiffDeliveryDate"
import { ImageTile } from "../../../parts/ImageTile"
import { CURRENCY } from "../../../../../utils/constants"

const NoticeDiffDeliveryDate = dynamic((() =>
  import("../../../parts/NoticeDiffDeliveryDate").then(
    (mod) => mod.NoticeDiffDeliveryDate,
  )) as DynamicOptions<NoticeDiffDeliveryDatePropsType>)

export const Sample: FC<
  {
    sample: SampleType
    initQty?: number
    isShared?: boolean
  } & WithViewProductVariantType
> = ({ sample, viewProductsVariant, initQty, isShared }) => {
  const minShippingDateCart = useAppSelector(
    (state) => state.cart.minShippingDate,
  )

  const {
    name,
    updateCurrentCount,
    priceCalculate,
    unitMeasure,
    isRemoved,
    remove,
    add,
    isFetching,
    sampleCount: currentCount,
    inCart,
    path,
    totalQty,
    fastQty,
    images,
    counter,
    isInit,
    isCountError,
    baseTotalQty,
  } = useSample({
    sample: sample,
    options: {
      initQty,
    },
  })

  const { shippingDate } = useShippings({
    product: {
      totalQty: totalQty,
      currentCount: currentCount || 0,
      fastQty: fastQty,
    },
  })

  return (
    <>
      <StyledProduct
        viewProductsVariant={viewProductsVariant}
        isRemoved={isRemoved && !isShared}
      >
        <ProductContainer>
          <NoticeDiffDeliveryDate
            shippingDate={shippingDate}
            minShippingDateCart={minShippingDateCart}
            isAnalogs={false}
          />

          <ImageTile path={path}>
            <EntityImage
              imagePath={images[0]}
              imageAlt={name || ""}
              layout={"fill"}
              width={100}
              height={100}
              objectFit={"cover"}
              quality={15}
            />
          </ImageTile>
          <Title name={name || ""} path={path} />

          <Counter
            counter={counter}
            currentCount={currentCount}
            currentUnit={1}
            unitMeasure={unitMeasure}
            maxCount={totalQty}
            isFetching={isFetching}
            updateCountHandler={updateCurrentCount}
            productInCart={inCart}
            productIsRemove={isRemoved}
            isStatic={isShared && !inCart}
            isInitProduct={isInit}
            isCountError={isCountError}
            messageError={`Доступно только ${MACROS_ERROR_COUNT} образцов`}
          />

          <DeliveryMessage deliveryDate={shippingDate} />

          {viewProductsVariant !== "sample_mini" && (
            <>
              <Price
                variant={"total"}
                price={priceCalculate}
                currency={CURRENCY}
                messageImportant={isRemoved ? undefined : "нет в наличии"}
              />
            </>
          )}

          {!isShared && (
            <ControlsProduct
              isRemoved={isRemoved}
              removeMethod={remove}
              addMethod={() => add(true)}
              viewProductsVariant={viewProductsVariant}
              priceCalculate={priceCalculate}
              currency={CURRENCY}
              isFetching={isFetching}
              isAvailable={!!baseTotalQty}
            />
          )}
        </ProductContainer>
      </StyledProduct>
    </>
  )
}
