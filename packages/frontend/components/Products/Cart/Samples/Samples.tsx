import type { FC } from "react"
import React from "react"
import {
  ProductType,
  VIEW_PRODUCTS_SAMPLE,
  WithViewProductVariantType,
} from "../../../../types/types"
import { Sample } from "./Sample/Sample"
import { Products } from "../../Products"

export const Samples: FC<
  {
    samples: Record<string, ProductType & { initQty?: number }> | null
    isShared?: boolean
  } & WithViewProductVariantType
> = ({ samples, viewProductsVariant = VIEW_PRODUCTS_SAMPLE, isShared }) => {
  return (
    <>
      {samples !== null && Object.keys(samples).length > 0 && (
        <>
          <Products view={viewProductsVariant} titleList={"Образцы"} withBorder>
            {Object.keys(samples).map((uuid) => {
              return (
                <Sample
                  key={uuid}
                  viewProductsVariant={viewProductsVariant}
                  sample={{
                    ...samples[uuid],
                  }}
                  initQty={samples[uuid].initQty}
                  isShared={isShared}
                />
              )
            })}
          </Products>
        </>
      )}
    </>
  )
}
