import type { FC } from "react"
import { memo, useCallback } from "react"
import { ProductContainer, StyledProduct } from "../../StyledProduct"
import { EntityImage } from "../../../EntityImage/EntityImage"
import {
  ProductWithChildType,
  VIEW_PRODUCTS_CHILD,
} from "../../../../types/types"
import Price from "../../parts/Price/Price"
import { Counter } from "../../parts/Counter/Counter"
import { useProduct } from "../../../../hooks/product/product"
import { DeliveryMessage } from "../../parts/DeliveryMessage/DeliveryMessage"
import { ControlsProduct } from "./Controls/ControlsProduct"
import { ListInfo } from "../../parts/ListInfo/ListInfo"
import { UnitControl } from "../../parts/UnitControl/UnitControl"
import { Title } from "../../parts/Title/Title"
import { useShippings } from "../../../../hooks/shippings"
import { ImageTile } from "../../parts/ImageTile"
import { CURRENCY } from "../../../../utils/constants"

export const ProductChildKit: FC<{
  parent: string
  product: ProductWithChildType
  addChild: (uuid: string | null) => void
  removeChild: (uuid: string | null) => void
}> = memo(({ parent, product, addChild, removeChild }) => {
  const {
    path,
    name,
    unitMeasure,
    totalQty,
    currentCount,
    currentUnit,
    isFetching,
    updateCurrentCount,
    priceUnit,
    priceCalculate,
    uuid,
    inCart,
    storesQty,
    updateCurrentUnit,
    units,
    fastQty,
    storesAvailable,
    images,
    counter,
    isRemoved,
    isCountError,
    setIsCountError,
    isInit,
    isAvailable,
  } = useProduct({
    product: product,
    options: {
      parentKit: parent,
    },
  })
  const { shippingDate } = useShippings({
    product: {
      totalQty: totalQty,
      currentCount: currentCount || 1,
      fastQty: fastQty,
    },
  })

  const remove = useCallback(() => removeChild(uuid), [removeChild, uuid])
  const add = useCallback(() => addChild(uuid), [addChild, uuid])

  return (
    <StyledProduct
      uuid={uuid || undefined}
      viewProductsVariant={VIEW_PRODUCTS_CHILD}
      isRemoved={isRemoved}
    >
      <ProductContainer>
        <ImageTile path={path}>
          <EntityImage
            imagePath={images[0]}
            imageAlt={name || ""}
            layout={"fill"}
            width={100}
            height={100}
            objectFit={"cover"}
            quality={15}
          />
        </ImageTile>

        <Title name={name || ""} path={path} />

        <UnitControl
          setCurrentUnit={updateCurrentUnit}
          unitMeasure={unitMeasure}
          units={units}
          totalQty={totalQty}
          unitValueActive={currentUnit || undefined}
          isInitProduct={isInit}
        />

        <Counter
          counter={counter}
          currentCount={currentCount}
          currentUnit={currentUnit}
          unitMeasure={unitMeasure}
          maxCount={totalQty}
          isFetching={isFetching}
          updateCountHandler={updateCurrentCount}
          isHideMinus
          productInCart={inCart}
          productIsRemove={isRemoved}
          isCountError={isCountError}
          setIsCountError={setIsCountError}
          isInitProduct={isInit}
        />

        <DeliveryMessage deliveryDate={shippingDate} />

        <Price
          variant={"total"}
          price={priceCalculate}
          currency={CURRENCY}
          messageImportant={"нет в наличии"}
        />

        <Price price={priceUnit} currency={CURRENCY} unitMeasure={"шт"} />

        <ControlsProduct
          isRemoved={isRemoved}
          removeMethod={remove}
          addMethod={add}
          isFetching={isFetching}
          isAvailable={isAvailable}
        />

        <ListInfo storesQty={storesQty} stores={storesAvailable} />
      </ProductContainer>
    </StyledProduct>
  )
})

ProductChildKit.displayName = "ProductChildKit"
