import type { FC } from "react"
import { useEffect, useRef, useState } from "react"
import { ProductContainer, StyledProduct } from "../../StyledProduct"
import { EntityImage } from "../../../EntityImage/EntityImage"
import {
  ProductWithChildType,
  VIEW_PRODUCTS_CHECKOUT_MINI,
  ViewProductsType,
} from "../../../../types/types"
import Price from "../../parts/Price/Price"
import { Counter } from "../../parts/Counter/Counter"
import { useProduct } from "../../../../hooks/product/product"
import { UnitControl } from "../../parts/UnitControl/UnitControl"
import { DeliveryMessage } from "../../parts/DeliveryMessage/DeliveryMessage"
import { ControlsProduct } from "./Controls/ControlsProduct"
import { ControlsModalsRelated } from "./Controls/ControlsModalsRelated"
import Available from "../../../Available/Available"
import { ButtonToggleFavorite } from "../../parts/ButtonToggleFavorite/ButtonToggleFavorite"
import { ListInfo } from "../../parts/ListInfo/ListInfo"
import { Title } from "../../parts/Title/Title"
import { useShippings } from "../../../../hooks/shippings"
import { useAppSelector } from "../../../../hooks/redux"
import type { NoticeDiffDeliveryDatePropsType } from "../../parts/NoticeDiffDeliveryDate"
import dynamic, { DynamicOptions } from "next/dynamic"
import { ImageTile } from "../../parts/ImageTile"
import { CURRENCY } from "../../../../utils/constants"
import { SkeletonProduct } from "../../Skeleton/Skeleton"

const NoticeDiffDeliveryDate = dynamic((() =>
  import("../../parts/NoticeDiffDeliveryDate").then(
    (mod) => mod.NoticeDiffDeliveryDate,
  )) as DynamicOptions<NoticeDiffDeliveryDatePropsType>)

export const Product: FC<{
  product: ProductWithChildType
  view: ViewProductsType
  isSaveOnRemove?: boolean
}> = ({ product, view, isSaveOnRemove, children }) => {
  const minShippingDateCart = useAppSelector(
    (state) => state.cart.minShippingDate,
  )
  const [isShowAnalogs, setIsShowAnalogs] = useState(false)

  const {
    name,
    path,
    totalQty,
    units,
    currentCount,
    updateCurrentCount,
    updateCurrentUnit,
    priceUnit,
    priceCalculate,
    unitMeasure,
    currentUnit,
    isFetching,
    isRemoved,
    removeFromCart,
    addToCart,
    inCart,
    isAnimate,
    availableStatus,
    isFavorites,
    toggleFavorite,
    storesQty,
    isFetchingFavorites,
    fastQty,
    storesAvailable,
    images,
    counter,
    isCountError: isCountProductError,
    setIsCountError,
    isInit: isInitProduct,
    uuid,
    isAvailable,
  } = useProduct({
    product: product,
    options: {
      isSaveOnRemove: isSaveOnRemove,
    },
  })

  const { shippingDate } = useShippings({
    product: {
      totalQty: totalQty,
      currentCount: currentCount || 1,
      fastQty: fastQty,
    },
  })

  const productRef = useRef<HTMLDivElement>(null)

  useEffect(() => {
    if (isAnimate) {
      window.scrollTo({
        top:
          (productRef?.current?.offsetTop || 0) +
          (productRef?.current?.clientHeight || 0),
        left: 0,
        behavior: "smooth",
      })
    }
  }, [isAnimate])

  return (
    <>
      <StyledProduct
        uuid={uuid || undefined}
        viewProductsVariant={view}
        isRemoved={isRemoved}
        isAnimate={!!isAnimate}
        isAvailable={isAvailable}
        ref={productRef}
      >
        {!isInitProduct && (
          <>
            <SkeletonProduct />
          </>
        )}

        <ProductContainer>
          {isAvailable && (
            <NoticeDiffDeliveryDate
              onFindAnalogHandle={() => {
                setIsShowAnalogs(true)
              }}
              shippingDate={shippingDate}
              minShippingDateCart={minShippingDateCart}
              isAnalogs={!!(product?.analogs?.fast || []).length}
            />
          )}

          {view === VIEW_PRODUCTS_CHECKOUT_MINI && (
            <ButtonToggleFavorite
              isFavorites={isFavorites}
              toggleFavorite={toggleFavorite}
              isFetching={isFetchingFavorites}
            />
          )}
          <ImageTile path={path}>
            <EntityImage
              imagePath={images[0]}
              imageAlt={name || ""}
              layout={"intrinsic"}
              width={100}
              height={100}
              objectFit={"cover"}
              quality={15}
            />
          </ImageTile>

          <Title name={name || ""} path={path} />

          <ControlsModalsRelated
            viewProductsVariant={view}
            analogs={product?.analogs?.fast || []}
            companions={product.companions}
            isShowAnalogs={isShowAnalogs}
            setIsShowAnalogs={setIsShowAnalogs}
            isAvailable={isAvailable}
            uuid={uuid}
          />

          {isAvailable && (
            <>
              <UnitControl
                setCurrentUnit={updateCurrentUnit}
                unitMeasure={unitMeasure}
                units={units}
                totalQty={totalQty}
                unitValueActive={currentUnit || undefined}
                isInitProduct={isInitProduct}
              />
              <DeliveryMessage deliveryDate={shippingDate} />
              <Counter
                counter={counter}
                currentCount={currentCount}
                currentUnit={currentUnit}
                unitMeasure={unitMeasure}
                maxCount={totalQty}
                isFetching={isFetching}
                updateCountHandler={updateCurrentCount}
                productInCart={inCart}
                productIsRemove={isRemoved}
                isCountError={isCountProductError}
                setIsCountError={setIsCountError}
                isInitProduct={isInitProduct}
              />
            </>
          )}

          {view !== VIEW_PRODUCTS_CHECKOUT_MINI && (
            <>
              <Price
                variant={"total"}
                price={isAvailable ? priceCalculate : 0}
                currency={CURRENCY}
                messageImportant={
                  isRemoved && isAvailable ? undefined : "нет в наличии"
                }
              />
            </>
          )}

          <Price price={priceUnit} currency={CURRENCY} unitMeasure={"шт"} />

          <ControlsProduct
            isRemoved={isRemoved}
            removeMethod={removeFromCart}
            addMethod={addToCart}
            viewProductsVariant={view}
            currency={CURRENCY}
            priceCalculate={priceCalculate}
            isFetching={isFetching}
            isAvailable={isAvailable}
          />

          <ListInfo storesQty={storesQty} stores={storesAvailable} />

          {view === VIEW_PRODUCTS_CHECKOUT_MINI && (
            <>
              <Available status={availableStatus} />
            </>
          )}

          {children}
        </ProductContainer>
      </StyledProduct>
    </>
  )
}
