import type { FC } from "react"
import { memo, useState } from "react"
import { ProductContainer, StyledProduct } from "../../StyledProduct"
import { EntityImage } from "../../../EntityImage/EntityImage"
import {
  ProductWithChildType,
  ProductWithInOrderType,
  VIEW_PRODUCTS_CHECKOUT_MINI,
  VIEW_PRODUCTS_CHILD,
  ViewProductsType,
} from "../../../../types/types"
import Price from "../../parts/Price/Price"
import { Counter } from "../../parts/Counter/Counter"
import { Button } from "../../../Button"
import { UnitControl } from "../../parts/UnitControl/UnitControl"
import { Products } from "../../Products"
import { useProductKit } from "../../../../hooks/product/productKit"
import { ProductChildKit } from "./ProductChildKit"
import {
  cssButtonCompoundToggle,
  cssButtonFill,
} from "../../../Button/StyledButton"
import { cssHiddenCompoundKit } from "../../StyledProducts"
import { cx } from "@linaria/core"
import { DeliveryMessage } from "../../parts/DeliveryMessage/DeliveryMessage"
import { ControlsProduct } from "./Controls/ControlsProduct"
import { ControlsModalsRelated } from "./Controls/ControlsModalsRelated"
import Available from "../../../Available/Available"
import { ListInfo } from "../../parts/ListInfo/ListInfo"
import { ButtonToggleFavorite } from "../../parts/ButtonToggleFavorite/ButtonToggleFavorite"
import { Title } from "../../parts/Title/Title"
import { useShippings } from "../../../../hooks/shippings"
import dynamic, { DynamicOptions } from "next/dynamic"
import { NoticeDiffDeliveryDatePropsType } from "../../parts/NoticeDiffDeliveryDate"
import { useAppSelector } from "../../../../hooks/redux"
import { ImageTile } from "../../parts/ImageTile"
import { CURRENCY } from "../../../../utils/constants"
import { SkeletonProduct } from "../../Skeleton/Skeleton"

const NoticeDiffDeliveryDate = dynamic((() =>
  import("../../parts/NoticeDiffDeliveryDate").then(
    (mod) => mod.NoticeDiffDeliveryDate,
  )) as DynamicOptions<NoticeDiffDeliveryDatePropsType>)

export const ProductKit: FC<
  {
    product: ProductWithChildType
    view: ViewProductsType
    isSaveOnRemove?: boolean
  } & ProductWithInOrderType
> = memo(({ product, view, isSaveOnRemove }) => {
  const minShippingDateCart = useAppSelector(
    (state) => state.cart.minShippingDate,
  )
  const [isShowAnalogs, setIsShowAnalogs] = useState(false)

  const {
    uuid,
    path,
    name,
    totalQty,
    units,
    currentCount,
    updateCurrentCount,
    updateCurrentUnit,
    priceUnit,
    priceCalculate,
    unitMeasure,
    currentUnit,
    isFetching,
    isRemoved,
    removeFromCart,
    isKit,
    child,
    addChild,
    removeChild,
    addToCart,
    inCart,
    availableStatus,
    storesQty,
    isFavorites,
    toggleFavorite,
    isFetchingFavorites,
    fastQty,
    storesAvailable,
    images,
    counter,
    isCountError,
    setIsCountError,
    isInit,
    isAvailable,
  } = useProductKit({
    product: product,
    options: { isSaveOnRemove: isSaveOnRemove },
  })
  const { shippingDate } = useShippings({
    product: {
      totalQty: totalQty,
      currentCount: currentCount || 1,
      fastQty: fastQty,
    },
  })
  const [isShowCompoundKit, setIsShowCompoundKit] = useState<boolean>(true)

  return (
    <StyledProduct
      uuid={uuid || undefined}
      viewProductsVariant={view}
      isKit={isKit}
      isRemoved={isRemoved}
      isAvailable={isAvailable}
    >
      {!isInit && (
        <>
          <SkeletonProduct />
        </>
      )}

      <ProductContainer>
        <NoticeDiffDeliveryDate
          onFindAnalogHandle={() => {
            setIsShowAnalogs(true)
          }}
          shippingDate={shippingDate}
          minShippingDateCart={minShippingDateCart}
          isAnalogs={!!(product?.analogs?.fast || []).length}
        />

        {view === VIEW_PRODUCTS_CHECKOUT_MINI && (
          <ButtonToggleFavorite
            isFavorites={isFavorites}
            toggleFavorite={toggleFavorite}
            isFetching={isFetchingFavorites}
          />
        )}
        <ImageTile path={path}>
          <EntityImage
            imagePath={images[0]}
            imageAlt={name || ""}
            layout={"fill"}
            width={100}
            height={100}
            objectFit={"cover"}
            quality={15}
          />
        </ImageTile>

        <Title name={name || ""} path={path} />

        <ControlsModalsRelated
          viewProductsVariant={view}
          analogs={product?.analogs?.fast || []}
          companions={product.companions}
          setIsShowAnalogs={setIsShowAnalogs}
          isShowAnalogs={isShowAnalogs}
          isAvailable={isAvailable}
          uuid={uuid}
        />

        {isAvailable && (
          <>
            <UnitControl
              setCurrentUnit={updateCurrentUnit}
              unitMeasure={unitMeasure}
              units={units}
              totalQty={totalQty}
              unitValueActive={currentUnit || undefined}
              isInitProduct={isInit}
            />

            <Counter
              counter={counter}
              currentCount={currentCount}
              currentUnit={currentUnit}
              unitMeasure={unitMeasure}
              maxCount={totalQty}
              isFetching={isFetching}
              updateCountHandler={updateCurrentCount}
              productInCart={inCart}
              productIsRemove={isRemoved}
              isCountError={isCountError}
              setIsCountError={setIsCountError}
              isInitProduct={isInit}
            />

            {view !== VIEW_PRODUCTS_CHECKOUT_MINI && (
              <DeliveryMessage deliveryDate={shippingDate} />
            )}
          </>
        )}

        {view !== VIEW_PRODUCTS_CHECKOUT_MINI && (
          <>
            <Price
              variant={"total"}
              price={priceCalculate}
              currency={CURRENCY}
              messageImportant={isRemoved ? undefined : "нет в наличии"}
            />
          </>
        )}

        <Price price={priceUnit} currency={CURRENCY} unitMeasure={"шт"} />

        <ControlsProduct
          isRemoved={isRemoved}
          removeMethod={removeFromCart}
          addMethod={addToCart}
          viewProductsVariant={view}
          currency={CURRENCY}
          priceCalculate={priceCalculate}
          isFetching={isFetching}
          isAvailable={isAvailable}
        />

        <ListInfo storesQty={storesQty} stores={storesAvailable} />

        {view === VIEW_PRODUCTS_CHECKOUT_MINI && (
          <>
            <Available status={availableStatus} />
          </>
        )}

        {view !== VIEW_PRODUCTS_CHECKOUT_MINI && isAvailable && (
          <>
            {isKit && !!child && Object.keys(child || {}).length > 0 && (
              <>
                <Button
                  variant={"small"}
                  icon={"Transaction"}
                  className={cx(
                    cssButtonCompoundToggle,
                    !isShowCompoundKit && cssButtonFill,
                  )}
                  onClick={() => {
                    setIsShowCompoundKit(!isShowCompoundKit)
                  }}
                >
                  Состав комплекта
                </Button>
                <Products
                  view={VIEW_PRODUCTS_CHILD}
                  className={cx(!isShowCompoundKit && cssHiddenCompoundKit)}
                  withBorder
                >
                  {uuid !== null &&
                    !!child &&
                    Object.keys(child || {}).map((key) => (
                      <ProductChildKit
                        key={key}
                        parent={uuid}
                        product={(child || {})[key]}
                        removeChild={removeChild}
                        addChild={addChild}
                      />
                    ))}
                </Products>
              </>
            )}
          </>
        )}
      </ProductContainer>
    </StyledProduct>
  )
})

ProductKit.displayName = "ProductKit"
