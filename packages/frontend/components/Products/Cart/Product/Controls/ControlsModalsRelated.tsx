import { FC, useMemo } from "react"
import {
  VIEW_PRODUCTS_CHECKOUT_MINI,
  WithViewProductVariantType,
} from "../../../../../types/types"
import { ShowedVariantsButtonsContainer } from "../../../StyledProduct"
import dynamic, { DynamicOptions } from "next/dynamic"
import { ModalCompanionsPropsType } from "../../../../Cart/Modals/ModalCompanions"
import { ModalAnalogsPropsType } from "../../../../Cart/Modals/ModalAnalogs"
import { Button } from "../../../../Button"
import { ReportAdmission } from "../../../../Forms/ReportAdmission"
import type { ModalDefaultPropsType } from "../../../../Modals/Modal"
import { RuleSortProduct } from "../../../../../utils/helpers"

const Modal = dynamic((() =>
  import("../../../../Modals/Modal").then(
    (mod) => mod.Modal,
  )) as DynamicOptions<ModalDefaultPropsType>)

const ModalAnalogs = dynamic((() =>
  import("../../../../Cart/Modals/ModalAnalogs").then(
    (mod) => mod.ModalAnalogs,
  )) as DynamicOptions<ModalAnalogsPropsType>)

const ModalCompanions = dynamic((() =>
  import("../../../../Cart/Modals/ModalCompanions").then(
    (mod) => mod.ModalCompanions,
  )) as DynamicOptions<ModalCompanionsPropsType>)

export const ControlsModalsRelated: FC<
  WithViewProductVariantType & {
    analogs?: RuleSortProduct[]
    companions?: RuleSortProduct[]
    isShowAnalogs?: boolean
    setIsShowAnalogs?: (val: boolean) => void
    isAvailable?: boolean
    uuid?: string | null
  }
> = ({
  viewProductsVariant,
  analogs,
  companions,
  isShowAnalogs,
  setIsShowAnalogs,
  isAvailable,
  uuid,
}) => {
  const isVisible = useMemo(
    () =>
      ((analogs || []).length > 0 ||
        (companions || []).length > 0 ||
        !isAvailable) &&
      !!uuid,
    [analogs, companions, isAvailable, uuid],
  )
  return (
    <>
      {viewProductsVariant !== VIEW_PRODUCTS_CHECKOUT_MINI && isVisible && (
        <>
          <ShowedVariantsButtonsContainer>
            {(analogs || []).length > 0 && (
              <ModalAnalogs
                isShowAnalogs={isShowAnalogs}
                setIsShowAnalogs={setIsShowAnalogs}
                productAnalogs={analogs}
              />
            )}
            {(companions || []).length > 0 && (
              <ModalCompanions companions={companions} />
            )}

            {!isAvailable && (
              <Modal
                title={"Сообщить о поступлении"}
                variant={"rounded-0"}
                closeMode={"destroy"}
                disclosure={
                  <Button variant={"small"} icon={"Email"}>
                    Сообщить о поступлении
                  </Button>
                }
              >
                <ReportAdmission uuid={uuid || ""} />
              </Modal>
            )}
          </ShowedVariantsButtonsContainer>
        </>
      )}
    </>
  )
}
