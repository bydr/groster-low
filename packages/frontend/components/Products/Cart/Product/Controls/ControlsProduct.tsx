import { FC, MouseEvent, useState } from "react"
import { Button } from "../../../../Button"
import { ControlsButtonsContainer } from "../../../StyledProduct"
import {
  CurrencyType,
  VIEW_PRODUCTS_CHECKOUT_MINI,
  VIEW_PRODUCTS_GRID,
  VIEW_PRODUCTS_SAMPLE_MINI,
  WithViewProductVariantType,
} from "../../../../../types/types"
import { ButtonArea } from "../../../../Button/StyledButton"
import { Typography } from "../../../../Typography/Typography"
import Price from "../../../parts/Price/Price"
import { CURRENCY } from "../../../../../utils/constants"

export const ControlsProduct: FC<
  {
    isRemoved?: boolean
    removeMethod: () => void
    addMethod: () => void
    currency?: CurrencyType
    priceCalculate?: number
    isFetching?: boolean
    isAvailable?: boolean
  } & WithViewProductVariantType
> = ({
  isRemoved,
  removeMethod,
  addMethod,
  viewProductsVariant = VIEW_PRODUCTS_GRID,
  currency = CURRENCY,
  priceCalculate,
  isFetching,
  isAvailable,
}) => {
  const [isMini] = useState<boolean>(
    viewProductsVariant === VIEW_PRODUCTS_SAMPLE_MINI ||
      viewProductsVariant === VIEW_PRODUCTS_CHECKOUT_MINI,
  )

  const removeHandler = (e: MouseEvent<HTMLButtonElement>) => {
    e.preventDefault()
    removeMethod()
  }

  const addHandler = (e: MouseEvent<HTMLButtonElement>) => {
    e.preventDefault()
    addMethod()
  }

  return (
    <>
      <ControlsButtonsContainer>
        {!isRemoved ? (
          <>
            {!isMini ? (
              <>
                <Button
                  variant={"small"}
                  icon={"Delete"}
                  isHiddenBg
                  onClick={removeHandler}
                  isFetching={isFetching}
                >
                  Удалить
                </Button>
              </>
            ) : (
              <>
                <Button
                  as={"div"}
                  variant={"translucent"}
                  onClick={removeHandler}
                  withArea={true}
                  isFetching={isFetching}
                >
                  <ButtonArea>
                    <Price
                      variant={"total"}
                      currency={currency}
                      price={priceCalculate || 0}
                    />
                  </ButtonArea>
                  <Typography variant={"span"}>Удалить</Typography>
                </Button>
              </>
            )}
          </>
        ) : (
          <>
            {!!isAvailable ? (
              <>
                {!isMini ? (
                  <>
                    <Button
                      variant={"small"}
                      icon={"Undo"}
                      isHiddenBg
                      onClick={addHandler}
                      isFetching={isFetching}
                    >
                      Вернуть
                    </Button>
                  </>
                ) : (
                  <>
                    <Button
                      as={"div"}
                      variant={"translucent"}
                      onClick={addHandler}
                      withArea={true}
                      isFetching={isFetching}
                    >
                      <ButtonArea>
                        <Price
                          variant={"total"}
                          currency={currency}
                          price={priceCalculate || 0}
                        />
                      </ButtonArea>
                      <Typography variant={"span"}>Вернуть</Typography>
                    </Button>
                  </>
                )}
              </>
            ) : (
              <></>
            )}
          </>
        )}
      </ControlsButtonsContainer>
    </>
  )
}
