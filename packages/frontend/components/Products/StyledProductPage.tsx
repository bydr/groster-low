import { styled } from "@linaria/react"
import { breakpoints } from "../../styles/utils/vars"
import { StyledProductsSlider } from "./Slider/StyledProductsSlider"
import { ButtonSliderArrow } from "../Button/StyledButton"

export const StyledProductPage = styled.div`
  width: 100%;
  flex: 1;
  display: flex;
`
export const Side = styled.div`
  position: relative;

  @media (min-width: ${breakpoints.xxl}) {
    ${StyledProductsSlider} {
      margin-right: -40px;
      width: auto;
      ${ButtonSliderArrow} {
        &:first-of-type {
          right: 90px;
        }
        &:last-of-type {
          right: 40px;
        }
      }
    }
  }
`
