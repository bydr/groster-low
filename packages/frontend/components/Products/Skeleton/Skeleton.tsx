import { FC } from "react"
import {
  StyledSkeleton,
  SProductCatalog,
  SButton,
  SButtonSmall,
  SContent,
  SField,
  SImage,
  SRow,
} from "./StyledSkeleton"

export const SkeletonProduct: FC = () => {
  return (
    <>
      <StyledSkeleton>
        <SProductCatalog>
          <SImage />
          <SContent>
            <SField />
            <SField
              style={{
                width: "70%",
              }}
            />
            <SField
              style={{
                width: "80%",
              }}
            />
            <br />
            <SRow>
              <SField />
              <SField
                style={{
                  borderRadius: "50px",
                }}
              />
            </SRow>
            <SButton />
            <SButtonSmall
              style={{
                maxWidth: "180px",
              }}
            />
          </SContent>
        </SProductCatalog>
      </StyledSkeleton>
    </>
  )
}
