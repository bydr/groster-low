import React, { FC } from "react"
import {
  SButton,
  SContent,
  SField,
  SImage,
  SProductDetail,
  SRow,
  StyledSkeleton,
} from "./StyledSkeleton"

export const SkeletonDetail: FC = () => {
  return (
    <>
      <StyledSkeleton>
        <SProductDetail>
          <SImage />
          <SContent>
            <SField />
            <SField
              style={{
                width: "70%",
              }}
            />
            <SField
              style={{
                width: "80%",
              }}
            />
            <br />
            <SField />
            <SField
              style={{
                width: "80%",
              }}
            />
            <br />
            <SRow>
              <SField />
              <SField />
            </SRow>
            <SButton />
          </SContent>
        </SProductDetail>
      </StyledSkeleton>
    </>
  )
}
