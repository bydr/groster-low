import { styled } from "@linaria/react"
import { breakpoints, colors } from "../../../styles/utils/vars"
import {
  VIEW_PRODUCTS_CHECKOUT,
  VIEW_PRODUCTS_CHECKOUT_MINI,
  VIEW_PRODUCTS_CHILD,
  VIEW_PRODUCTS_LIST,
  VIEW_PRODUCTS_ORDER,
  VIEW_PRODUCTS_SAMPLE,
  VIEW_PRODUCTS_SAMPLE_MINI,
  VIEW_PRODUCTS_SAMPLE_ORDER,
} from "../../../types/types"

export const SBlock = styled.div`
  background: ${colors.grayLight};
  margin-bottom: 10px;
  max-width: 100%;
  max-height: 100%;
  border-radius: 4px;
  position: relative;
  overflow: hidden;
  z-index: 1;

  @keyframes ant-skeleton-loading {
    0% {
      transform: translate(-37.5%);
    }

    to {
      transform: translate(37.5%);
    }
  }

  &:before {
    content: "";
    position: absolute;
    z-index: 2;
    left: 0;
    top: 0;
    inset: 0 -150%;
    background: linear-gradient(
      90deg,
      rgba(238, 238, 238, 0.2) 25%,
      rgba(129, 129, 129, 0.24) 37%,
      rgba(206, 206, 206, 0.2) 63%
    );
    animation-name: ant-skeleton-loading;
    animation-timing-function: ease-in-out;
    animation-direction: alternate;
    animation-iteration-count: infinite;
    animation-duration: 1.4s;
  }
`

export const StyledSkeleton = styled.div`
  position: absolute;
  left: 0;
  top: 0;
  bottom: 0;
  right: 0;
  width: 100%;
  height: 100%;
  background: ${colors.white};
  z-index: 6;
  padding: 15px;
  margin: 0;
`

export const SImage = styled(SBlock)`
  width: 100%;
  height: 200px;
  grid-area: image;
`

export const SField = styled(SBlock)`
  height: 24px;
  width: 100%;
  grid-area: field;
`

export const SButton = styled(SBlock)`
  height: 40px;
  border-radius: 50px;
  width: 100%;
  display: flex;
  margin-right: auto;
  margin-left: auto;
`

export const SButtonSmall = styled(SButton)`
  height: 30px;
`

export const SRow = styled.div`
  width: 100%;
  display: flex;
  gap: 0 10px;
`

export const SContent = styled.div`
  grid-area: content;
`

export const SProductCatalog = styled.div`
  position: relative;

  [data-view=${VIEW_PRODUCTS_LIST}] &, [data-view=${VIEW_PRODUCTS_CHECKOUT}] &, 
  [data-view=${VIEW_PRODUCTS_CHILD}] &, [data-view=${VIEW_PRODUCTS_CHECKOUT_MINI}] &, 
  [data-view=${VIEW_PRODUCTS_SAMPLE}] &, [data-view=${VIEW_PRODUCTS_SAMPLE_MINI}] &, 
  [data-view=${VIEW_PRODUCTS_SAMPLE_ORDER}] &, [data-view=${VIEW_PRODUCTS_ORDER}] & {
    display: grid;
    grid-template-areas: "image content";
    grid-template-columns: 100px 8fr;
    gap: 8px;

    ${SImage} {
      width: 100px;
      height: 100px;
    }

    ${SContent} {
      > * {
        display: none;
        &:nth-child(1) {
          display: block;
        }
        &:nth-child(2) {
          display: block;
        }
        &:nth-child(3) {
          display: block;
        }
      }
    }
  }

  @media (max-width: ${breakpoints.sm}) {
    [data-view="list"] &,
    & {
      display: grid;
      grid-template-areas: "content image";
      grid-template-columns: 8fr 4fr;
      gap: 8px;

      ${SImage} {
        width: 100px;
        height: 100px;
      }
    }
  }
`

export const SProductDetail = styled.div`
  position: relative;
  display: grid;
  grid-template-areas: "content image";
  grid-template-columns: 8fr 4fr;
  gap: 0 40px;

  ${SImage} {
    width: 200px;
    height: 200px;
  }
`
