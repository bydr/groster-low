import { styled } from "@linaria/react"
import { getTypographyBase } from "../../Typography/Typography"
import { breakpoints, colors } from "../../../styles/utils/vars"
import { ProductContainer, StyledProductTag } from "../StyledProduct"
import { ButtonBase, ButtonSliderArrow } from "../../Button/StyledButton"
import { cssIcon } from "../../Icon"
import { StyledProducts } from "../StyledProducts"
import { VIEW_PRODUCTS_LIST } from "../../../types/types"
import {
  cssRadioGroup,
  RadioLabel,
  StyledRadioInner,
} from "../../Radio/StyledRadio"
import { cssCustomSwiper } from "../../Swiper/StyledSwiper"
import { css } from "@linaria/core"

export const SliderTitle = styled.div`
  ${getTypographyBase("h2")};
  margin-bottom: 10px;

  * {
    margin-bottom: 0;
  }
`

export const cssProductSlider = css``

export const StyledProductsSlider = styled.section`
  width: 100%;
  display: flex;
  padding-top: 24px;
  overflow: hidden;
  position: relative;

  .swiper-slide {
    border-left: 1px solid ${colors.gray};

    &:first-child,
    &.swiper-slide-active {
      border-left-color: transparent;
    }
  }

  ${StyledProductTag} {
    border: none;
    max-width: initial;

    ${ProductContainer} {
      padding-right: 16px;
      padding-left: 16px;
    }
  }

  ${ButtonSliderArrow}${ButtonBase} {
    margin: 0;
  }

  &[data-variant-arrow="left-right"] {
    width: auto;

    .${cssCustomSwiper}.${cssProductSlider} {
      padding-right: 50px;
      padding-left: 50px;

      &:before,
      &:after {
        position: absolute;
        content: "";
        top: 0;
        bottom: 0;
        z-index: 2;
        width: 50px;
        height: 100%;
        background: ${colors.white};
      }
      &:before {
        left: 0;
      }
      &:after {
        right: 0;
      }
    }

    ${ButtonSliderArrow} {
      bottom: 50%;
      transform: translateY(-50%);
      margin: 0;
      z-index: 3;

      &:first-of-type {
        left: 0;
      }
      &:last-of-type {
        right: 0;
      }

      .${cssIcon} {
        fill: ${colors.brand.purple};
      }
    }

    @media (max-width: ${breakpoints.lg}) {
      padding-left: 0;
      padding-right: 0;

      .${cssCustomSwiper}.${cssProductSlider} {
        &:before,
        &:after {
          display: none;
        }
      }
    }
  }

  &[data-variant-arrow="top-bottom"] {
    .${cssCustomSwiper}.${cssProductSlider} {
      padding-top: 50px;
      margin-top: -50px;
      ${ButtonSliderArrow} {
        top: 0;
      }
    }
  }

  @media (max-width: ${breakpoints.sm}) {
    width: auto;
    //margin-right: -15px;

    ${StyledProducts} {
      &[data-view=${VIEW_PRODUCTS_LIST}] {
        ${StyledProductTag} {
          border-bottom: 1px solid ${colors.gray};

          ${ProductContainer} {
            .${cssRadioGroup} {
              ${RadioLabel} {
                padding: 2px 10px;

                ${StyledRadioInner} {
                  &:before {
                    top: 0;
                    left: 0;
                    height: 2px;
                    width: 100%;
                  }
                }
              }
            }
          }
        }
      }
    }
  }
`

export const StyledProductsSliderInner = styled.div`
  width: 100%;
`
