import { FC, memo, ReactNode, useEffect, useRef, useState } from "react"
import {
  ProductType,
  VIEW_PRODUCTS_GRID,
  VIEW_PRODUCTS_SLIDE,
} from "../../../types/types"
import { Product } from "../Catalog/Product/Product"
import { Button } from "../../Button"
import {
  cssProductSlider,
  SliderTitle,
  StyledProductsSlider,
  StyledProductsSliderInner,
} from "./StyledProductsSlider"
import { Products } from "../Products"
import { navigationInit, SwiperWrapper } from "../../Swiper/SwiperWrapper"
import { Navigation, SwiperOptions } from "swiper"
import { SwiperSlide } from "swiper/react"
import { useWindowSize } from "../../../hooks/windowSize"
import { getBreakpointVal } from "../../../styles/utils/Utils"
import { breakpoints } from "../../../styles/utils/vars"
import { RuleSortProduct } from "../../../utils/helpers"

type ResponsiveExtendsParamType = SwiperOptions

type ResponsiveExtendsType = Record<number | string, ResponsiveExtendsParamType>

export type ProductsSliderPropsType = {
  products: ProductType[]
  title?: string | ReactNode
  variantArrow?: "left-right" | "top-bottom"
  disabledOnMobile?: boolean
  responsiveExtends?: ResponsiveExtendsType
  ruleSort?: RuleSortProduct[]
}

export const ProductsSlider: FC<ProductsSliderPropsType> = memo(
  ({
    products,
    title,
    variantArrow = "top-bottom",
    disabledOnMobile = false,
    responsiveExtends,
    ruleSort,
  }) => {
    const { width } = useWindowSize()
    const [isMobileWidth, setIsMobileWidth] = useState(false)
    const prevRef = useRef<HTMLButtonElement>(null)
    const nextRef = useRef<HTMLButtonElement>(null)

    useEffect(() => {
      if (disabledOnMobile) {
        if (width !== undefined) {
          setIsMobileWidth(width <= getBreakpointVal(breakpoints.sm))
        }
      }
    }, [disabledOnMobile, width])

    return (
      <>
        {products.length > 0 && (
          <>
            <StyledProductsSlider data-variant-arrow={variantArrow}>
              <StyledProductsSliderInner>
                {title && <SliderTitle>{title}</SliderTitle>}

                {isMobileWidth ? (
                  <>
                    <Products view={VIEW_PRODUCTS_GRID}>
                      {products.map((p, i) => (
                        <Product
                          view={VIEW_PRODUCTS_GRID}
                          key={p.uuid || i}
                          product={p}
                          sortWeight={
                            ruleSort?.find((r) => r.uuid === p.uuid)?.weight
                          }
                          categories={(p.categories || []).join(", ")}
                        />
                      ))}
                    </Products>
                  </>
                ) : (
                  <>
                    <SwiperWrapper
                      className={cssProductSlider}
                      onInit={(swiper) => {
                        navigationInit({
                          swiper,
                          prevEl: prevRef.current || undefined,
                          nextEl: nextRef.current || undefined,
                        })
                      }}
                      slidesPerView={2}
                      spaceBetween={0}
                      updateOnWindowResize={true}
                      breakpoints={responsiveExtends}
                      modules={[Navigation]}
                      speed={300}
                      autoHeight={false}
                    >
                      {products.map((p, i) => (
                        <SwiperSlide key={i}>
                          <Product
                            view={VIEW_PRODUCTS_SLIDE}
                            product={p}
                            sortWeight={
                              ruleSort?.find((r) => r.uuid === p.uuid)?.weight
                            }
                            categories={(p.categories || []).join(", ")}
                          />
                        </SwiperSlide>
                      ))}

                      <Button
                        variant={"arrow"}
                        icon={
                          variantArrow === "left-right"
                            ? "ArrowLeft"
                            : "AngleLeft"
                        }
                        ref={prevRef}
                      />
                      <Button
                        variant={"arrow"}
                        icon={
                          variantArrow === "left-right"
                            ? "ArrowRight"
                            : "AngleRight"
                        }
                        ref={nextRef}
                      />
                    </SwiperWrapper>
                  </>
                )}
              </StyledProductsSliderInner>
            </StyledProductsSlider>
          </>
        )}
      </>
    )
  },
)

ProductsSlider.displayName = "ProductsSlider"
