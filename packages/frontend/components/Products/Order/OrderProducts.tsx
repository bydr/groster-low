import { FC } from "react"
import { ContentContainer } from "styles/utils/StyledGrid"
import {
  ProductWithChildType,
  SpecificationItemType,
  VIEW_PRODUCTS_ORDER,
  VIEW_PRODUCTS_SAMPLE_ORDER,
} from "../../../types/types"
import { Products } from "../Products"
import dynamic, { DynamicOptions } from "next/dynamic"
import { OrderProductPropsType } from "./Product/Product"
import { OrderSamplePropsType } from "./Sample/Sample"

const DynamicProduct = dynamic((() =>
  import("./Product/Product").then(
    (mod) => mod.Product,
  )) as DynamicOptions<OrderProductPropsType>)

const DynamicSample = dynamic((() =>
  import("./Sample/Sample").then(
    (mod) => mod.Sample,
  )) as DynamicOptions<OrderSamplePropsType>)

export const OrderProducts: FC<{
  products: Record<string, ProductWithChildType> | null
  samples: Record<string, ProductWithChildType> | null
  specification: Record<string, SpecificationItemType> | null
}> = ({ products, samples, specification }) => {
  return (
    <>
      <ContentContainer>
        {Object.keys(products || {}).length > 0 && (
          <>
            <Products view={VIEW_PRODUCTS_ORDER} withBorder>
              {products !== null &&
                Object.keys(products).length > 0 &&
                Object.keys(products).map((uuidKey) => {
                  return (
                    <DynamicProduct
                      key={products[uuidKey].uuid}
                      product={products[uuidKey]}
                      quantity={
                        specification !== null
                          ? specification[products[uuidKey].uuid || ""]
                              ?.quantity
                          : undefined
                      }
                    />
                  )
                })}
            </Products>
          </>
        )}

        {Object.keys(samples || {}).length > 0 && (
          <>
            <Products
              view={VIEW_PRODUCTS_SAMPLE_ORDER}
              titleList={"Образцы"}
              withBorder
            >
              {samples !== null &&
                Object.keys(samples).map((uuidKey) => {
                  return (
                    <DynamicSample
                      key={uuidKey}
                      sample={{
                        price: samples[uuidKey].price,
                        uuid: samples[uuidKey].uuid,
                        unit: samples[uuidKey].unit,
                        name: samples[uuidKey].name,
                        parent: samples[uuidKey].parent,
                        alias: samples[uuidKey].alias,
                        images: samples[uuidKey].images,
                      }}
                      quantity={
                        specification !== null
                          ? specification[samples[uuidKey].uuid || ""]?.sample
                          : undefined
                      }
                    />
                  )
                })}
            </Products>
          </>
        )}
      </ContentContainer>
    </>
  )
}
