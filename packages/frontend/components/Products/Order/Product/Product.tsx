import { FC, memo } from "react"
import { ButtonToggleFavorite } from "../../parts/ButtonToggleFavorite/ButtonToggleFavorite"
import { EntityImage } from "../../../EntityImage/EntityImage"
import { Title } from "../../parts/Title/Title"
import { ProductContainer, StyledProduct } from "../../StyledProduct"
import { VIEW_PRODUCTS_ORDER } from "../../../../types/types"
import { useProduct } from "../../../../hooks/product/product"
import Price from "../../parts/Price/Price"
import { AddToCartControl } from "../../parts/AddToCartControl/AddToCartControl"
import { ListInfo } from "../../parts/ListInfo/ListInfo"
import { ProductCatalogType } from "../../../../../contracts/contracts"
import { UnitControl } from "../../parts/UnitControl/UnitControl"
import { SkeletonProduct } from "../../Skeleton/Skeleton"

export type OrderProductPropsType = {
  product: ProductCatalogType
  quantity?: number
}

export const Product: FC<OrderProductPropsType> = memo(
  ({ product, quantity }) => {
    const {
      uuid,
      isFavorites,
      availableStatus,
      toggleFavorite,
      path,
      name,
      priceCalculate,
      priceUnit,
      unitMeasure,
      currency,
      isFetching,
      addToCart,
      inCart,
      isRemoved,
      storesQty,
      isFetchingFavorites,
      storesAvailable,
      images,
      isAvailable,
      isInit,
    } = useProduct({
      product: product,
    })

    return (
      <>
        <StyledProduct
          isFavorite={isFavorites}
          viewProductsVariant={VIEW_PRODUCTS_ORDER}
          availableStatus={availableStatus || undefined}
        >
          {!isInit && (
            <>
              <SkeletonProduct />
            </>
          )}
          <ProductContainer>
            <ButtonToggleFavorite
              isFavorites={isFavorites}
              toggleFavorite={toggleFavorite}
              isFetching={isFetchingFavorites}
            />
            <EntityImage
              imagePath={images[0]}
              imageAlt={name || ""}
              layout={"intrinsic"}
              width={100}
              height={100}
              objectFit={"cover"}
              quality={40}
            />
            <Title name={name || ""} path={path} />
            <Price
              price={priceUnit}
              currency={currency}
              unitMeasure={unitMeasure}
            />
            <UnitControl
              totalQty={quantity || 0}
              units={[]}
              unitMeasure={unitMeasure}
              isInitProduct={isInit}
            />
            <AddToCartControl
              uuid={uuid}
              isAvailable={isAvailable}
              isFetching={isFetching}
              inCart={inCart && !isRemoved}
              addToCart={addToCart}
              buttonAreaContent={
                <>
                  <Price
                    variant={"total"}
                    currency={currency}
                    price={priceCalculate}
                  />
                </>
              }
            />

            <ListInfo storesQty={storesQty} stores={storesAvailable} />
          </ProductContainer>
        </StyledProduct>
      </>
    )
  },
)

Product.displayName = "Product"
