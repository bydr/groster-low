import { FC, useEffect } from "react"
import { ProductContainer, StyledProduct } from "../../StyledProduct"
import { EntityImage } from "../../../EntityImage/EntityImage"
import { Title } from "../../parts/Title/Title"
import { SampleType, VIEW_PRODUCTS_SAMPLE_ORDER } from "../../../../types/types"
import Price from "../../parts/Price/Price"
import { useSample } from "../../../../hooks/sample"
import { UnitControl } from "../../parts/UnitControl/UnitControl"
import { CURRENCY } from "../../../../utils/constants"
import { SkeletonProduct } from "../../Skeleton/Skeleton"

export type OrderSamplePropsType = {
  sample: SampleType
  quantity?: number
}

export const Sample: FC<OrderSamplePropsType> = ({ sample, quantity }) => {
  const {
    name,
    updateCurrentCount,
    priceCalculate,
    isRemoved,
    path,
    images,
    unitMeasure,
    isInit,
  } = useSample({ sample: sample })

  useEffect(() => {
    if (quantity !== undefined) {
      updateCurrentCount(quantity)
    }
  }, [quantity, updateCurrentCount])

  return (
    <>
      <StyledProduct
        viewProductsVariant={VIEW_PRODUCTS_SAMPLE_ORDER}
        isRemoved={isRemoved}
      >
        {!isInit && (
          <>
            <SkeletonProduct />
          </>
        )}
        <ProductContainer>
          <EntityImage
            imagePath={images[0]}
            imageAlt={name || ""}
            layout={"intrinsic"}
            width={100}
            height={100}
            objectFit={"cover"}
            quality={40}
          />
          <Title name={name || ""} path={path} />
          <UnitControl
            totalQty={quantity || 0}
            units={[]}
            unitMeasure={unitMeasure}
            isInitProduct={isInit}
          />
          <Price variant={"total"} price={priceCalculate} currency={CURRENCY} />
        </ProductContainer>
      </StyledProduct>
    </>
  )
}
