import { FC, useContext, useEffect } from "react"
import { Product } from "../Catalog/Product/Product"
import { ProductType, VIEW_PRODUCTS_LIST } from "../../../types/types"
import { ButtonGroup } from "../../Button/StyledButton"
import Button from "../../Button/Button"
import { ROUTES } from "../../../utils/constants"
import { Typography } from "../../Typography/Typography"
import { ModalContext } from "../../Modals/Modal"
import { KitPartProducts } from "../parts/KitPart/KitPartProducts"
import { Products } from "../Products"
import { appSlice } from "../../../store/reducers/appSlice"
import { useAppDispatch } from "../../../hooks/redux"

export const AfterAddedToCartKit: FC<{
  product: ProductType
  companions?: ProductType[]
  kitProducts?: ProductType[]
  analogs?: ProductType[]
  kitParents?: string[]
  onClose?: () => void
}> = ({ product, companions, kitProducts, analogs, kitParents, onClose }) => {
  const modalContext = useContext(ModalContext)
  const { setModals } = appSlice.actions
  const dispatch = useAppDispatch()

  useEffect(() => {
    if (modalContext?.baseId !== undefined) {
      dispatch(setModals([modalContext.baseId]))
    }
  }, [dispatch, modalContext?.baseId, setModals])

  return (
    <>
      <Product product={product} view={VIEW_PRODUCTS_LIST} isPreview={true} />
      <br />
      <ButtonGroup>
        <Button variant={"filled"} as={"a"} href={ROUTES.checkout}>
          Оформить заказ
        </Button>
        <Button
          variant={"outline"}
          onClick={() => {
            modalContext?.hide()
            if (onClose !== undefined) {
              onClose()
            }
          }}
        >
          Продолжить покупки
        </Button>
      </ButtonGroup>

      {!!kitParents?.length && (
        <>
          <br />
          <Typography variant={"h5"}>
            Комплекты, в которые входит товар
          </Typography>
          <KitPartProducts ids={kitParents} slidesPerViewStart={3} />
        </>
      )}

      {companions !== undefined && companions.length > 0 ? (
        <>
          <br />
          <Typography variant={"h5"}>Вам также могут пригодиться</Typography>
          <Products view={VIEW_PRODUCTS_LIST}>
            {companions.map((p) => (
              <Product
                product={p}
                view={VIEW_PRODUCTS_LIST}
                key={p.uuid || ""}
              />
            ))}
          </Products>
        </>
      ) : (
        <>
          {analogs !== undefined && analogs.length > 0 && (
            <>
              <br />
              <Typography variant={"h5"}>Могут Вас заинтересовать</Typography>
              <Products view={VIEW_PRODUCTS_LIST}>
                {analogs.map((p) => (
                  <Product
                    product={p}
                    view={VIEW_PRODUCTS_LIST}
                    key={p.uuid || ""}
                  />
                ))}
              </Products>
            </>
          )}
        </>
      )}

      {kitProducts !== undefined && kitProducts.length > 0 && (
        <>
          <br />
          <Typography variant={"h5"}>Состав этого комплекта</Typography>
          <Products view={VIEW_PRODUCTS_LIST}>
            {kitProducts.map((p) => (
              <Product
                product={p}
                view={VIEW_PRODUCTS_LIST}
                key={p.uuid || ""}
              />
            ))}
          </Products>
        </>
      )}
    </>
  )
}
