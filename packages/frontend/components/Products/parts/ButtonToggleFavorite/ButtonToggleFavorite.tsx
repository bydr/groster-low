import { FC } from "react"
import { cssButtonAddFavorite } from "../../../Button/StyledButton"
import Button, { ButtonVariantsPropsType } from "../../../Button/Button"

export const ButtonToggleFavorite: FC<
  {
    isFavorites: boolean
    isFetching?: boolean
    toggleFavorite: () => void
  } & Pick<ButtonVariantsPropsType, "size" | "variant">
> = ({ isFavorites, toggleFavorite, isFetching = false, size, variant }) => {
  return (
    <>
      <Button
        variant={!!variant ? variant : "link"}
        data-isfavorite={isFavorites}
        icon={isFavorites ? "Star" : "StarOutline"}
        className={cssButtonAddFavorite}
        onClick={toggleFavorite}
        isFetching={isFetching}
        size={size}
      />
    </>
  )
}
