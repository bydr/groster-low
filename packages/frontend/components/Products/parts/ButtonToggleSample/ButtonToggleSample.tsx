import { FC, MouseEvent, useMemo } from "react"
import { colors } from "../../../../styles/utils/vars"
import Button, { ButtonVariantsPropsType } from "../../../Button/Button"

export const ButtonToggleSample: FC<
  {
    sample: number
    isFetchingSample: boolean
    toggleSampleToCart: (e: MouseEvent<HTMLButtonElement>) => void
    isDisabled?: boolean
    isAllow?: boolean
  } & Pick<ButtonVariantsPropsType, "size">
> = ({
  sample,
  isFetchingSample,
  toggleSampleToCart,
  isDisabled = false,
  size,
  isAllow,
}) => {
  const isActive = useMemo(() => sample > 0, [sample])
  return (
    <>
      {!!isAllow && (
        <Button
          as={"a"}
          href={"/"}
          variant={"link"}
          icon={isActive ? "Check" : "Tag"}
          fillIcon={isActive ? colors.green : undefined}
          isFetching={isFetchingSample}
          onClick={toggleSampleToCart}
          style={{
            background: isActive ? colors.grayLight : "initial",
          }}
          disabled={isDisabled}
          size={size}
        >
          {sample > 0
            ? `Образцов в корзине ${sample > 0 && sample}`
            : "Образец"}
        </Button>
      )}
    </>
  )
}
