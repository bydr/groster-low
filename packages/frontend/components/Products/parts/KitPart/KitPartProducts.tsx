import { FC } from "react"
import { BaseLoader } from "../../../Loaders/BaseLoader/BaseLoader"
import { ProductsSlider } from "../../Slider/ProductsSlider"
import { getBreakpointVal } from "../../../../styles/utils/Utils"
import { breakpoints } from "../../../../styles/utils/vars"
import { useQuery } from "react-query"
import { fetchProductsList } from "../../../../api/productsAPI"

export const KitPartProducts: FC<{
  ids: string[]
  slidesPerViewStart?: number
}> = ({ ids, slidesPerViewStart = 4 }) => {
  const { data, isFetching } = useQuery(["kitParentsProducts", ids], () =>
    ids.length > 0
      ? fetchProductsList({
          uuids: ids.join(","),
        })
      : null,
  )

  return (
    <>
      {isFetching && <BaseLoader />}
      <ProductsSlider
        products={data || []}
        variantArrow={"left-right"}
        responsiveExtends={{
          [getBreakpointVal(breakpoints.lg)]: {
            slidesPerView: slidesPerViewStart,
          },
          0: {
            slidesPerView: 1,
          },
        }}
        disabledOnMobile
      />
    </>
  )
}
