import { FC } from "react"
import { Typography } from "../../../Typography/Typography"
import { ROUTES } from "../../../../utils/constants"
import dynamic, { DynamicOptions } from "next/dynamic"
import { ModalDefaultPropsType } from "../../../Modals/Modal"
import { KitPartProducts } from "./KitPartProducts"
import { Trigger } from "../../../../styles/utils/Utils"

const DynamicModal = dynamic(
  (() =>
    import("../../../Modals/Modal").then(
      (mod) => mod.Modal,
    )) as DynamicOptions<ModalDefaultPropsType>,
  {
    ssr: false,
  },
)

export const KitPart: FC<{
  kitParents: string[]
}> = ({ kitParents }) => {
  return (
    <>
      {kitParents.length > 0 && (
        <>
          {kitParents.length > 1 ? (
            <>
              <DynamicModal
                variant={"rounded-100"}
                closeMode={"destroy"}
                title={"Комплекты, в которые входит товар"}
                disclosure={
                  <Typography variant={"p14"}>
                    <Trigger>Является частью комплекта</Trigger>
                  </Typography>
                }
              >
                <KitPartProducts ids={kitParents} />
              </DynamicModal>
            </>
          ) : (
            <>
              <Typography>
                <a href={`${ROUTES.product}/${kitParents[0]}`}>
                  Является частью комплекта
                </a>
              </Typography>
            </>
          )}
        </>
      )}
    </>
  )
}
