import { FC } from "react"
import Button, { ButtonSizeType } from "../../../Button/Button"
import { cssButtonFastBuy } from "../../../Button/StyledButton"
import { FastOrderForm } from "../../../Forms/FastOrder"
import { ModalDefaultPropsType } from "../../../Modals/Modal"
import dynamic, { DynamicOptions } from "next/dynamic"

const DynamicModal = dynamic((() =>
  import("../../../Modals/Modal").then(
    (mod) => mod.Modal,
  )) as DynamicOptions<ModalDefaultPropsType>)

export const ButtonFastOrder: FC<{
  product?: {
    inCart: boolean
    isRemoved: boolean
    addToCart: () => void
  }
  disabled?: boolean
  size?: ButtonSizeType
}> = ({ product, disabled, size = "medium" }) => {
  return (
    <>
      <DynamicModal
        variant={"rounded-0"}
        title={"Быстрый заказ"}
        closeMode={"destroy"}
        isShowModal={false}
        disclosure={
          <Button
            variant={"outline"}
            size={size}
            className={cssButtonFastBuy}
            onClick={() => {
              if (product !== undefined) {
                if (!product.inCart || product.isRemoved) {
                  product.addToCart()
                }
              }
            }}
            disabled={disabled}
          >
            Быстрый заказ
          </Button>
        }
      >
        <FastOrderForm />
      </DynamicModal>
    </>
  )
}
