import { css } from "@linaria/core"
import { styled } from "@linaria/react"
import {
  ErrorMessageDefault,
  getTypographyBase,
} from "../../../Typography/Typography"
import {
  BoxShadowPopover,
  colors,
  sizeSVG,
} from "../../../../styles/utils/vars"
import { ButtonLink } from "../../../Button/StyledButton"
import { cssIcon, getSizeSVG } from "../../../Icon"
import { Unit } from "../Price/StyledPrice"
import { StyledPopoverInner } from "../../../Popover/StyledPopover"
import { cssIsActive } from "../../../../styles/utils/Utils"
import { StyledInput } from "../../../Field/StyledField"

export const cssIsDisabled = css`
  pointer-events: none !important;
  opacity: 0.3 !important;

  ${StyledInput} {
    pointer-events: none !important;
  }
`

export const StyledCount = styled.div`
  display: inline-flex;
  align-items: center;
`

export const plus = css``
export const minus = css``

export const StyledCounterError = styled.div`
  position: absolute;
  background: ${colors.white};
  box-shadow: ${BoxShadowPopover};
  border: 1px solid ${colors.gray};
  border-radius: 8px;
  padding: 4px 8px;
  z-index: 5;
  bottom: 100%;
  min-width: 150px;
  right: 0;
  transition: all 0.2s ease-in-out;
  transform: translateY(-15px);
  opacity: 0;
  visibility: hidden;

  &.${cssIsActive} {
    opacity: 1;
    visibility: visible;
    transform: translateY(0);
  }
`

export const CounterInput = styled.div`
  position: relative;
  display: inline-flex;
  align-items: center;
  justify-content: center;
  flex: 1;
  transition: all 0.1s ease-in-out, transform 0.5s ease-in-out 0.3s;

  ${StyledInput} {
    ${getTypographyBase("p12")};
    border: none;
    background-color: transparent;
    text-align: right;
    display: inline-flex;
    width: 100%;
    appearance: none;
    padding: 3px;
    min-width: 20px;
    margin-bottom: 0;
  }

  > * {
    display: flex;
    flex: 1;
    margin-bottom: 0;
  }
`
export const StyledCounter = styled.div`
  position: relative;
  display: inline-flex;
  align-items: center;
  border-radius: 50px;
  background: ${colors.white};
  padding: 0 5px;

  ${ButtonLink} {
    margin: 0;

    .${cssIcon} {
      ${getSizeSVG(sizeSVG.small)};
    }

    &.${minus}, &.${plus} {
      padding: 0 5px;
      border: none;
      min-height: initial;
      width: auto;
    }

    &[disabled] {
      opacity: 1 !important;
    }

    &.${plus}, &.${minus} {
      .${cssIcon} {
        fill: ${colors.brand.yellow};
      }

      &:hover,
      &:active {
        .${cssIcon} {
          fill: ${colors.brand.yellow};
        }
      }
    }
  }

  ${Unit} {
    padding: 3px;
  }

  ${StyledPopoverInner} {
    padding: 5px 8px;

    ${ErrorMessageDefault} {
      margin: 0;
    }
  }
`
