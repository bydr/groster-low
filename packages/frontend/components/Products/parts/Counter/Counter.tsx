import {
  ChangeEvent,
  FC,
  memo,
  MouseEvent,
  useCallback,
  useEffect,
  useState,
} from "react"
import Button from "../../../Button/Button"
import {
  CounterInput,
  cssIsDisabled,
  minus,
  plus,
  StyledCount,
  StyledCounter,
  StyledCounterError,
} from "./StyledCounter"
import { Unit } from "../Price/StyledPrice"
import { UnitMeasureType } from "../../../../types/types"
import {
  ErrorMessageContainer,
  ErrorMessageDefault,
  Typography,
} from "../../../Typography/Typography"
import { useDebounce } from "../../../../hooks/debounce"
import { ComponentLoader } from "../../../Loaders/ComponentLoader/ComponentLoader"
import { cx } from "@linaria/core"
import { cssIsActive } from "../../../../styles/utils/Utils"
import { StyledInput } from "../../../Field/StyledField"

type CounterPropsType = {
  currentCount: number | null
  currentUnit: number | null
  unitMeasure?: UnitMeasureType | null
  isStatic?: boolean
  maxCount?: number
  isFetching?: boolean
  isHideMinus?: boolean
  productInCart?: boolean
  productIsRemove?: boolean
  updateCountHandler?: (count: number) => void
  counter?: number | null
  isCountError?: boolean
  setIsCountError?: (isError: boolean) => void
  isInitProduct?: boolean
  messageError?: string
}

export const MACROS_ERROR_COUNT = "#count#"

const CounterInner: FC<CounterPropsType> = memo(
  ({
    unitMeasure = "шт.",
    isStatic = false,
    maxCount = 0,
    updateCountHandler,
    isFetching,
    isHideMinus,
    productInCart,
    productIsRemove,
    counter = null,
    currentUnit = null,
    currentCount,
    isCountError,
    setIsCountError,
    isInitProduct,
    messageError,
  }) => {
    const [isDisabledPlus, setIsDisabledPlus] = useState(false)
    const [count, setCount] = useState<number | null>(counter || null)
    const [isShowErrorPopup, setIsShowErrorPopup] = useState<boolean>(false)
    const debouncedCount = useDebounce(count, 750)

    const _getCounterMsg = (message: string): string => {
      return message.replace(
        MACROS_ERROR_COUNT,
        `<NoWrap>${maxCount}&nbsp;${unitMeasure}</NoWrap>`,
      )
    }

    const onChangeCountHandler = useCallback(
      (event: ChangeEvent<HTMLInputElement>) => {
        event.preventDefault()
        if (currentUnit) {
          const value = parseInt(event.currentTarget.value)
          setCount(!!value ? value : 0)
        }
      },
      [currentUnit],
    )

    const countMinus = (event: MouseEvent<HTMLButtonElement>) => {
      event.preventDefault()
      if (count === null || count < 1 || currentUnit === null) {
        return
      }
      setCount(count - currentUnit)
    }

    const countPlus = (event: MouseEvent<HTMLButtonElement>) => {
      event.preventDefault()
      if (count === null || currentUnit === null) {
        return
      }
      const value = count + currentUnit
      if (value > maxCount) {
        setIsDisabledPlus(true)
        if (setIsCountError) {
          setIsCountError(true)
        }
        return
      }
      setCount(value)
    }

    useEffect(() => {
      return () => {
        setCount(null)
        setIsShowErrorPopup(false)
      }
    }, [])

    useEffect(() => {
      if (currentCount !== null) {
        setCount(currentCount)
      }
    }, [currentCount, counter])

    useEffect(() => {
      if (debouncedCount === null || !updateCountHandler || !isInitProduct) {
        return
      }
      updateCountHandler(debouncedCount)
    }, [debouncedCount, updateCountHandler, isInitProduct])

    useEffect(() => {
      if (isCountError !== undefined) {
        setTimeout(() => {
          setIsShowErrorPopup(isCountError)
        }, 100)
      }
    }, [isCountError])

    useEffect(() => {
      setIsDisabledPlus((currentCount || 0) + (currentUnit || 0) > maxCount)
    }, [currentCount, currentUnit, maxCount])

    return (
      <>
        <StyledCounter className={cx(!maxCount && cssIsDisabled)}>
          {isFetching && <ComponentLoader />}

          {!!isCountError && (
            <>
              <StyledCounterError
                className={cx(isShowErrorPopup && cssIsActive)}
              >
                <ErrorMessageContainer>
                  <ErrorMessageDefault
                    dangerouslySetInnerHTML={{
                      __html:
                        messageError !== undefined
                          ? `${_getCounterMsg(messageError)}`
                          : `${_getCounterMsg(
                              `Доступно только ${MACROS_ERROR_COUNT} этого товара`,
                            )}`,
                    }}
                  />
                </ErrorMessageContainer>
              </StyledCounterError>
            </>
          )}

          {!isStatic ? (
            <>
              {!isHideMinus && (
                <>
                  <Button
                    variant={"link"}
                    className={minus}
                    icon={"Minus"}
                    onClick={countMinus}
                    isHiddenBg
                    disabled={
                      currentUnit === null ||
                      ((currentUnit === currentCount ||
                        maxCount < currentUnit) &&
                        (!productInCart || productIsRemove)) ||
                      (count || 0) < 1
                    }
                  />
                </>
              )}
              <CounterInput>
                <StyledInput
                  type="text"
                  inputMode={"numeric"}
                  value={count === null || count < 0 ? 0 : count}
                  onChange={onChangeCountHandler}
                  onFocus={(event) => {
                    event.currentTarget.select()
                  }}
                />
                <Unit>{unitMeasure}</Unit>
              </CounterInput>
              <Button
                variant={"link"}
                className={plus}
                icon={"Plus"}
                onClick={countPlus}
                isHiddenBg
                disabled={
                  count === null || currentUnit === null || isDisabledPlus
                }
              />
            </>
          ) : (
            <>
              <StyledCount>
                <Typography variant={"p12"}>{count}</Typography>
                <Unit>{unitMeasure}</Unit>
              </StyledCount>
            </>
          )}
        </StyledCounter>
      </>
    )
  },
)

export const Counter: FC<CounterPropsType> = ({ isInitProduct, ...rest }) => {
  const [isInit, setIsInit] = useState(!!isInitProduct)

  useEffect(() => {
    setIsInit(!!isInitProduct)
    return () => {
      setIsInit(false)
    }
  }, [isInitProduct])

  return <>{isInit && <CounterInner {...rest} isInitProduct={isInit} />}</>
}

CounterInner.displayName = "CounterInner"
