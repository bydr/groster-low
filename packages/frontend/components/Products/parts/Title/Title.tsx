import { FC } from "react"
import { ProductTitle } from "../../StyledProduct"
import { Typography } from "../../../Typography/Typography"
import { Link } from "../../../Link"

export const Title: FC<{
  name: string | null
  path: string | null
  onRoute?: () => void
}> = ({ name, path, onRoute }) => {
  return (
    <>
      <ProductTitle>
        <Link href={path || ""} scroll={true} shallow={false}>
          <Typography
            variant={"span"}
            onClick={() => {
              if (onRoute) {
                onRoute()
              }
            }}
            title={name || ""}
          >
            {name}
          </Typography>
        </Link>
      </ProductTitle>
    </>
  )
}
