import { StyledEntityImage } from "../../../EntityImage/Styled"
import { styled } from "@linaria/react"
import { css } from "@linaria/core"
import { StyledLinkBase } from "../../../Link/StyledLink"

export const cssSwiperProductImages = css``

export const StyledPreviewImage = styled.div`
  position: relative;
  width: 100%;

  > ${StyledLinkBase} {
    position: absolute;
    left: 0;
    top: 0;
    width: 100%;
    height: 100%;
    z-index: 2;
    display: flex;
  }
`

export const StyledPreviewSlider = styled(StyledEntityImage)`
  height: auto !important;
  z-index: 0;

  .${cssSwiperProductImages} {
    display: flex;
    flex-direction: column;

    .swiper-pagination {
      position: relative;
      bottom: auto;
    }
  }
`
