import { FC } from "react"
import { EntityImage } from "../../../EntityImage/EntityImage"
import {
  cssSwiperProductImages,
  StyledPreviewImage,
  StyledPreviewSlider,
} from "./StyledPreviewSlider"
import { cssBullet, cssBulletActive } from "../../../Swiper/StyledSwiper"
import { SwiperSlide } from "swiper/react"
import { EffectFade, Pagination } from "swiper"
import { SwiperWrapper } from "../../../Swiper/SwiperWrapper"
import { ImageProps } from "next/image"

export const PreviewSlider: FC<
  {
    images: string[]
    alt: string
    url?: string | null
  } & Omit<ImageProps, "src" | "draggable" | "alt">
> = ({
  images,
  alt,
  url,
  priority,
  sizes,
  objectFit,
  width,
  height,
  layout,
}) => {
  return (
    <>
      <StyledPreviewSlider>
        <SwiperWrapper
          className={cssSwiperProductImages}
          modules={[EffectFade, Pagination]}
          slidesPerView={1}
          lazy={false}
          loop={false}
          centeredSlides={true}
          pagination={{
            bulletClass: cssBullet,
            bulletActiveClass: cssBulletActive,
          }}
          paginationSequence
          count={images.length}
          effect={"fade"}
          speed={100}
          path={url || ""}
        >
          {images.map((img, i) => (
            <SwiperSlide key={i}>
              <StyledPreviewImage>
                <EntityImage
                  imagePath={img}
                  imageAlt={alt || ""}
                  layout={layout || "intrinsic"}
                  width={width || 200}
                  height={height || 200}
                  objectFit={objectFit || "cover"}
                  quality={50}
                  priority={!!priority && i === 0}
                  sizes={sizes}
                  withPreloader={true}
                  placeholder={"blur"}
                />
              </StyledPreviewImage>
            </SwiperSlide>
          ))}
        </SwiperWrapper>
      </StyledPreviewSlider>
    </>
  )
}
