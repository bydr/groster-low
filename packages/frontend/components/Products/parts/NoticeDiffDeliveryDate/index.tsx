import type { FC } from "react"
import { Popover } from "../../../Popover/Popover"
import { Button } from "../../../Button"
import { cssButtonClean, cssButtonPulse } from "../../../Button/StyledButton"
import { cssTooltipDate } from "../../StyledProduct"
import { Typography } from "../../../Typography/Typography"
import { cx } from "@linaria/core"
import {
  dateToISOString,
  declinationOfNum,
  getNumberOfDays,
} from "../../../../utils/helpers"
import { useMemo } from "react"

export type NoticeDiffDeliveryDatePropsType = {
  onFindAnalogHandle?: () => void
  shippingDate: Date | null
  minShippingDateCart: string | null
  isAnalogs: boolean
}

export const NoticeDiffDeliveryDate: FC<NoticeDiffDeliveryDatePropsType> = ({
  shippingDate,
  minShippingDateCart,
  onFindAnalogHandle,
  isAnalogs,
}) => {
  const diffDay = useMemo(() => {
    return shippingDate !== null && minShippingDateCart !== null
      ? getNumberOfDays(
          dateToISOString(minShippingDateCart),
          dateToISOString(shippingDate),
        )
      : 0
  }, [minShippingDateCart, shippingDate])

  return (
    <>
      {diffDay > 0 && (
        <>
          <Popover
            placement={"bottom-start"}
            withHover
            isStylingIconDisclosure={false}
            offset={[0, 0]}
            disclosure={
              <Button
                variant={"box"}
                icon={"Warning"}
                className={cx(cssButtonClean, cssButtonPulse)}
              />
            }
            className={cssTooltipDate}
          >
            <Typography variant={"p13"}>
              Этого товара нет на складе быстрой доставки. Для оформления
              быстрой доставки можете подобрать аналоги
              <br />
              <b>
                Cрок доставки увеличится на {diffDay}&nbsp;
                {declinationOfNum(diffDay, ["день", "дня", "дней"])}
              </b>
            </Typography>
            {isAnalogs && (
              <>
                <Button
                  variant={"filled"}
                  size={"small"}
                  isHiddenBg
                  onClick={() => {
                    if (onFindAnalogHandle) {
                      onFindAnalogHandle()
                    }
                  }}
                >
                  Выберите аналоги
                </Button>
              </>
            )}
          </Popover>
        </>
      )}
    </>
  )
}
