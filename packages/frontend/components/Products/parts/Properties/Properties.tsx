import { FC, useState } from "react"
import Property from "./Property"
import { Button } from "../../../Button"
import { Controls, Fake, StyledProperties } from "./StyledProperties"
import { PropertiesType } from "../../../../types/types"

export const COUNT_VISIBLE = 4

type CharactersPropsType = {
  items: PropertiesType | null
}

export const Properties: FC<CharactersPropsType> = ({ items, children }) => {
  const [isFull, setIsFull] = useState(false)
  const charactersShowedToggle = () => {
    setIsFull(!isFull)
  }
  const propertiesElements =
    items !== null
      ? items.map((item, index) => (
          <Property key={index} title={item.name} value={item.value} />
        ))
      : null
  return (
    <StyledProperties data-is-full={isFull}>
      {children}
      {propertiesElements}
      <Controls>
        <Fake />
        {propertiesElements && propertiesElements.length > COUNT_VISIBLE && (
          <>
            <Button
              variant={"small"}
              isHiddenBg
              onClick={charactersShowedToggle}
            >
              {!isFull ? "Показать еще" : "Скрыть"}
            </Button>
          </>
        )}
      </Controls>
    </StyledProperties>
  )
}
