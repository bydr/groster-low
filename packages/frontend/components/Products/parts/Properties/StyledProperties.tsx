import { styled } from "@linaria/react"
import { ButtonSmall } from "../../../Button/StyledButton"
import { colors } from "../../../../styles/utils/vars"
import { Paragraph12 } from "../../../Typography/Typography"

export const StyledCharacter = styled.div`
  display: flex;
  width: 100%;

  &:nth-child(4) ~ & {
    display: none;
  }
`
export const Title = styled(Paragraph12)`
  color: ${colors.grayDark};
  flex: 0 0 50%;
`
export const Value = styled(Paragraph12)`
  flex: 1;
`

export const StyledProperties = styled.div`
  width: 100%;

  ${ButtonSmall} {
    color: ${colors.brand.purple};
    border-radius: 0;
    border-bottom: 1px dashed ${colors.brand.purple};
    padding: 0;

    &:hover {
      background: transparent !important;
    }
  }

  &[data-is-full="true"] {
    ${StyledCharacter} {
      display: flex;
    }
  }
`
export const Controls = styled.div`
  display: flex;
  width: 100%;
`
export const Fake = styled.div`
  flex: 0 0 50%;
`
