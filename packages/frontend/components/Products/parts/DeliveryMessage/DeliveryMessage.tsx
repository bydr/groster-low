import { FC } from "react"
import { Typography } from "../../../Typography/Typography"
import { StyledDeliveryMessage } from "../../StyledProduct"
import { dateToString } from "../../../../utils/helpers"

const date = new Date()
date.setDate(date.getDate() + 1)

export const DeliveryMessage: FC<{ deliveryDate: Date | null }> = ({
  deliveryDate,
}) => {
  return (
    <>
      {!!deliveryDate && (
        <>
          <StyledDeliveryMessage>
            Срок доставки:{" "}
            <Typography variant={"span"}>
              {dateToString(deliveryDate, false)}
            </Typography>
          </StyledDeliveryMessage>
        </>
      )}
    </>
  )
}
