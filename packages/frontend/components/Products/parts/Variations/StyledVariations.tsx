import { css } from "@linaria/core"
import { styled } from "@linaria/react"
import { StyledFieldWrapper } from "../../../Field/StyledField"
import { Paragraph12, Span } from "../../../Typography/Typography"
import { colors, transitionDefault } from "../../../../styles/utils/vars"
import { StyledLinkBase } from "../../../Link/StyledLink"
import { StyledEntityImage } from "../../../EntityImage/Styled"
import { cssIcon, getSizeSVG } from "../../../Icon"
import { StyledSelectedTitle } from "../../../Select/StyledSelect"

export const cssBoxActive = css``
export const cssBoxImage = css``
export const cssBoxText = css``
export const StyledVariations = styled.div`
  position: relative;
  width: 100%;
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  margin: 0 0 16px 0;

  > ${StyledFieldWrapper} {
    width: 100%;
  }
`
export const VariationsSelectedName = styled(Paragraph12)`
  margin-bottom: 8px;

  > ${Span} {
    color: ${colors.grayDark};
  }
`
export const VariationsBox = styled.div`
  position: relative;
  overflow: hidden;
  display: inline-flex;
  margin-right: 4px;
  margin-bottom: 4px;
  border-radius: 4px;
  border: 2px solid ${colors.grayLight};
  padding: 2px 6px 2px 2px;
  cursor: pointer;
  transition: ${transitionDefault};

  &:hover {
    border-color: ${colors.gray};
  }

  ${StyledLinkBase} {
    position: absolute;
    left: 0;
    top: 0;
    padding: 0;
    margin: 0;
    width: 100%;
    height: 100%;
    background-color: transparent;
    z-index: 2;
    cursor: inherit;
  }

  &.${cssBoxImage} {
  }
  &.${cssBoxText} {
    min-height: 48px;
    align-items: center;
    justify-content: center;
    padding-left: 6px;
    padding-right: 6px;
  }

  &.${cssBoxActive} {
    border-color: ${colors.brand.yellow};
  }
`
export const VariationsBoxInner = styled.div`
  &,
  ${StyledVariations} & {
    width: 100%;
    display: flex;
    align-items: center;
    flex: 1;
    justify-content: flex-start;

    ${StyledEntityImage} {
      height: 40px;
      width: 40px;
      margin: 0 10px 0 0;
      border-radius: 50px;

      .${cssIcon} {
        ${getSizeSVG("32px")};
      }

      & ~ {
        ${Span} {
          flex: 1;
        }
      }
    }
  }

  ${StyledSelectedTitle} & {
    align-items: center;

    ${StyledEntityImage} {
      height: 16px;
      width: 16px;

      .${cssIcon} {
        ${getSizeSVG("16px")};
      }

      & ~ {
        .${cssIcon} {
          margin-right: 10px;
        }
      }
    }
  }
`
export const VariationsBoxList = styled.div`
  position: relative;
`

export const cssBoxDisabled = css`
  ${VariationsBoxInner}& {
    ${StyledEntityImage} {
      border: 2px solid ${colors.grayDark};
      position: relative;

      &:before {
        content: "";
        position: absolute;
        height: 2px;
        width: 100%;
        background: ${colors.grayDark};
        z-index: 2;
        transform: rotate(-45deg);
        top: 50%;
        margin-top: -1px;
        left: 0;
      }
    }

    ${StyledEntityImage} {
      opacity: 0.6;
    }
  }

  ${VariationsBox}& {
    position: relative;
    background-color: ${colors.grayLight};
    border-color: ${colors.gray};
    color: ${colors.grayDark};
    cursor: default;

    &:hover {
      border-color: ${colors.gray};
    }

    &:before {
      content: "";
      position: absolute;
      height: 2px;
      width: 100%;
      background: ${colors.gray};
      z-index: 2;
      transform: rotate(-45deg);
      top: 50%;
      margin-top: -1px;
      left: 0;
    }

    ${StyledEntityImage} {
      opacity: 0.6;
    }
  }
`
