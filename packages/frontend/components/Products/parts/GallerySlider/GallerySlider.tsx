import { FC, useCallback, useEffect, useRef, useState } from "react"
import Image from "next/image"
import {
  cssSliderThumbs,
  StyledGalleryMainImage,
  StyledGallerySlider,
  StyledThumb,
} from "./Styled"
import {
  navigationDestroy,
  navigationInit,
  NavigationInitPropsType,
  SwiperWrapper,
} from "../../../Swiper/SwiperWrapper"

// import required modules
import { EffectFade, FreeMode, Keyboard, Navigation, Thumbs } from "swiper"
import { SwiperSlide } from "swiper/react"
import { Swiper } from "swiper/types"
import { useWindowSize } from "../../../../hooks/windowSize"
import { EntityImage } from "../../../EntityImage/EntityImage"
import "swiper/css"
import "swiper/css/free-mode"
import "swiper/css/navigation"
import "swiper/css/effect-fade"
import "swiper/css/thumbs"
import { Button } from "../../../Button"
import {
  cssNavIsDisabled,
  cssNavNext,
  cssNavPrev,
} from "../../../Swiper/StyledSwiper"
import { getBreakpointVal } from "../../../../styles/utils/Utils"
import { breakpoints } from "../../../../styles/utils/vars"
import { cx } from "@linaria/core"
// import "swiper/css/effect-slide"

export const GallerySlider: FC<{ images: string[] }> = ({ images }) => {
  const [thumbsSwiper, setThumbsSwiper] = useState<null | Swiper>(null)
  const [mainSwiper, setMainSwiper] = useState<null | Swiper>(null)
  const { width } = useWindowSize()

  const [navMainIsDisabled, setNavMainIsDisabled] = useState(false)

  const prevMainRef = useRef<HTMLButtonElement>(null)
  const nextMainRef = useRef<HTMLButtonElement>(null)

  const init = useCallback(
    ({
      swiper,
      prevEl,
      nextEl,
      cbDestroy,
    }: NavigationInitPropsType & {
      cbDestroy?: () => void
      cbInit?: () => void
    }) => {
      if (width !== undefined && width <= getBreakpointVal(breakpoints.md)) {
        navigationDestroy({ swiper })
        if (!!cbDestroy) {
          cbDestroy()
        }
      } else {
        navigationInit({
          swiper,
          prevEl: prevEl,
          nextEl: nextEl,
        })
      }
    },
    [width],
  )

  useEffect(() => {
    return () => {
      thumbsSwiper?.destroy()
      mainSwiper?.destroy()
    }
  }, [mainSwiper, thumbsSwiper])

  return (
    <>
      {images.length > 0 && width !== undefined && (
        <>
          <StyledGallerySlider>
            <SwiperWrapper
              onInit={(swiper) => {
                setMainSwiper(swiper)
                init({
                  swiper,
                  prevEl: prevMainRef.current || undefined,
                  nextEl: nextMainRef.current || undefined,
                  cbDestroy: () => {
                    setNavMainIsDisabled(true)
                  },
                  cbInit: () => {
                    setNavMainIsDisabled(false)
                  },
                })
              }}
              slidesPerView={1}
              spaceBetween={0}
              thumbs={{ swiper: thumbsSwiper }}
              modules={[Keyboard, Navigation, EffectFade, Thumbs]}
              effect={"fade"}
              keyboard={{
                enabled: true,
              }}
              speed={100}
              className={cx(navMainIsDisabled && cssNavIsDisabled)}
              updateOnWindowResize={true}
            >
              {images.map((i, index) => (
                <SwiperSlide key={index}>
                  <StyledGalleryMainImage>
                    <EntityImage
                      imagePath={i}
                      alt={""}
                      objectFit={"contain"}
                      quality={60}
                      priority={false}
                      loading={"lazy"}
                      layout={"fill"}
                      withPreloader={true}
                    />
                  </StyledGalleryMainImage>
                </SwiperSlide>
              ))}
              {!navMainIsDisabled && (
                <>
                  <Button
                    variant={"arrow"}
                    icon={"AngleLeft"}
                    className={cssNavPrev}
                    ref={prevMainRef}
                  />
                  <Button
                    variant={"arrow"}
                    icon={"AngleRight"}
                    className={cssNavNext}
                    ref={nextMainRef}
                  />
                </>
              )}
            </SwiperWrapper>

            <SwiperWrapper
              onInit={(swiper) => {
                setThumbsSwiper(swiper)
              }}
              spaceBetween={10}
              modules={[Keyboard, Navigation, FreeMode, Thumbs]}
              slidesPerView={"auto"}
              breakpoints={{
                0: {
                  slidesPerView: "auto",
                },
              }}
              keyboard={{
                enabled: true,
              }}
              freeMode={true}
              watchSlidesProgress={true}
              className={cx(cssSliderThumbs)}
              updateOnWindowResize={true}
              centerInsufficientSlides={true}
            >
              {images.map((i, index) => (
                <SwiperSlide key={index}>
                  <StyledThumb>
                    <Image
                      src={`https://${i}`}
                      loading={"lazy"}
                      layout={"fixed"}
                      height={80}
                      width={80}
                      alt={""}
                    />
                  </StyledThumb>
                </SwiperSlide>
              ))}
            </SwiperWrapper>
          </StyledGallerySlider>
        </>
      )}
    </>
  )
}
