import { styled } from "@linaria/react"
import { breakpoints, colors } from "../../../../styles/utils/vars"

import { css } from "@linaria/core"
import { StyledEntityImage } from "../../../EntityImage/Styled"
import { ButtonBase, ButtonSliderArrow } from "../../../Button/StyledButton"
import { cssNavNext, cssNavPrev } from "../../../Swiper/StyledSwiper"

export const cssSliderThumbs = css`
  text-align: center;
  display: flex;
  width: 100%;
  justify-content: center;
  margin-top: 10px;
`

export const StyledThumb = styled.div`
  display: flex;
  overflow: hidden;
  text-align: center;
  align-items: center;
  justify-content: center;

  img {
    margin: 0 auto;
  }
`

export const StyledGalleryMainImage = styled.div`
  //height: 75vh;
  height: 70vh;
  width: 100%;
  text-align: center;
  display: block;
  justify-content: center;
  position: relative;

  ${StyledEntityImage} {
    height: 100%;
    width: 100%;
  }
`

export const StyledGallerySlider = styled.div`
  width: 100%;
  position: relative;
  overflow: hidden;
  z-index: 0;

  > * {
    &:first-child {
      flex: 1;
    }
  }

  ${ButtonSliderArrow}${ButtonBase} {
    z-index: 2;
    bottom: auto;
    top: 50%;
    margin-top: -24px;
    position: absolute;
    margin-bottom: 0;

    &.${cssNavPrev} {
      left: 0;
      right: auto;
    }
    &.${cssNavNext} {
      right: 0;
      left: auto;
    }
  }

  .swiper-button-next,
  .swiper-button-prev {
    &:after {
      color: ${colors.brand.yellow};
    }

    &:hover {
      &:after {
        color: ${colors.brand.purple};
      }
    }
  }

  .${cssSliderThumbs} {
    width: auto;

    .swiper-slide {
      width: 80px;
      opacity: 0.4;
      border-radius: 4px;
      border: 2px solid transparent;
    }
    .swiper-slide-thumb-active {
      opacity: 1;
      border-color: ${colors.brand.yellow};
    }
  }

  @media (max-width: ${breakpoints.md}) {
    .${cssSliderThumbs} {
      .swiper-slide {
        width: 20%;
      }
    }
  }

  @media (max-width: ${breakpoints.sm}) {
    .swiper-button-next,
    .swiper-button-prev {
      display: none;
    }

    .${cssSliderThumbs} {
      .swiper-slide {
        width: 84px;
        height: 84px;

        ${StyledThumb} {
          width: 80px;
          height: 80px;
          margin: 0 auto;
        }
      }
    }
  }
`
