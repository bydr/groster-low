import { styled } from "@linaria/react"
import { StyledLinkBase } from "../../../Link/StyledLink"
import { StyledEntityImage } from "../../../EntityImage/Styled"
import { css } from "@linaria/core"

export const cssIsSlider = css``

export const StyledImageTile = styled.div`
  width: 100%;
  position: relative;

  ${StyledLinkBase} {
    position: absolute;
    z-index: 2;
    left: 0;
    top: 0;
    bottom: 0;
    right: 0;
    background-color: transparent;
  }

  ${StyledEntityImage} {
    z-index: 1;
  }

  &.${cssIsSlider} {
    &:after {
      display: none;
      content: none;
    }
  }

  &:after {
    content: "";
    position: relative;
    display: block;
    height: 24px;
    left: 0;
    width: 100%;
    background-color: transparent;
  }
`
