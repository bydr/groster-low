import { styled } from "@linaria/react"
import { getTypographyBase, Span } from "../../../Typography/Typography"
import { colors } from "../../../../styles/utils/vars"
import { css } from "@linaria/core"

export const StyledPriceContainer = styled(Span)`
  display: inline-flex;
  flex-direction: column;
  align-items: flex-start;
  justify-content: center;
  &[data-is-free="true"] {
    background: ${colors.blueLight};
  }
`
export const Unit = styled(Span)`
  ${getTypographyBase("p10")};
  color: ${colors.grayDark};
  white-space: nowrap;
  margin-bottom: 0;
  line-height: 10px;
`
export const StyledPrice = styled(Span)`
  ${getTypographyBase("default")};
  display: inline-flex;
  align-items: baseline;
  flex-wrap: wrap;
  margin-bottom: 0;
  font-weight: 500;

  ${Unit} {
    color: inherit;
    font-weight: inherit;
    margin: 0 0 0 0.2rem;
  }
`
export const StyledPriceWrapper = styled.div`
  display: flex;
`

export const StyledSpanPriceWrapper = styled.span`
  font-size: inherit;
  ${StyledPrice} {
    ${Unit} {
      font-size: inherit;
      line-height: inherit;
      color: inherit;
    }
  }
`
export const StyledUnitPriceWrapper = styled(StyledPriceWrapper)``
export const StyledTotalPriceWrapper = styled(StyledPriceWrapper)`
  width: 100%;

  ${StyledPrice} {
    ${getTypographyBase("p14")};
    background: ${colors.grayLight};
    border-radius: 50px;
    padding: 0 12px;
    text-align: center;
    justify-content: center;
    font-weight: bold;

    ${Unit} {
      font-size: inherit;
      line-height: inherit;
      color: inherit;
    }
  }
`

export const cssPriceClean = css`
  ${StyledTotalPriceWrapper}&, ${StyledUnitPriceWrapper}&, ${StyledSpanPriceWrapper}& {
    font-size: inherit;
    line-height: inherit;
    color: inherit;
    background: transparent;
    width: auto;

    ${StyledPrice} {
      font-size: inherit;
      line-height: inherit;
      color: inherit;
      background: transparent;
      padding: 0;
      width: auto;
    }
  }
`

export const cssPriceWithBg = css`
  ${StyledTotalPriceWrapper}&, ${StyledUnitPriceWrapper}&, ${StyledSpanPriceWrapper}& {
    ${StyledPrice} {
      background: ${colors.grayLight};
      border-radius: 50px;
      padding: 0 12px;
      text-align: center;
      justify-content: center;
    }
  }
`
