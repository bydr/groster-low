import type { FC } from "react"
import React from "react"
import { CurrencyType } from "../../../../types/types"
import {
  cssPriceClean,
  cssPriceWithBg,
  StyledPrice,
  StyledPriceContainer,
  StyledPriceWrapper,
  StyledSpanPriceWrapper,
  StyledTotalPriceWrapper,
  StyledUnitPriceWrapper,
  Unit,
} from "./StyledPrice"
import { priceToFormat } from "../../../../utils/helpers"
import NumberFormat from "react-number-format"
import { cx } from "@linaria/core"
import { Span } from "../../../Typography/Typography"

type PricePropsType = {
  variant?: "unit" | "total" | "span"
  price: number
  currency: CurrencyType
  unitMeasure?: string | null
  messageImportant?: string
  isClean?: boolean
  isSale?: boolean
  prefix?: string
  withBg?: boolean
}
const Price: FC<PricePropsType> = ({
  variant = "unit",
  price,
  currency,
  unitMeasure,
  messageImportant,
  isClean,
  isSale,
  prefix,
  withBg,
  children,
}) => {
  let msg = messageImportant || "0"
  let Wrapper:
    | typeof StyledPriceWrapper
    | typeof StyledSpanPriceWrapper
    | null = null
  switch (variant) {
    case "unit": {
      Wrapper = StyledUnitPriceWrapper
      msg = messageImportant === undefined ? "" : messageImportant
      break
    }
    case "total": {
      Wrapper = StyledTotalPriceWrapper
      break
    }
    case "span": {
      Wrapper = StyledSpanPriceWrapper
      break
    }
  }

  return (
    <>
      {Wrapper !== null && (
        <Wrapper
          className={cx(isClean && cssPriceClean, withBg && cssPriceWithBg)}
        >
          <StyledPriceContainer>
            <StyledPrice>
              {price > 0 || isSale ? (
                <>
                  <NumberFormat
                    prefix={
                      prefix !== undefined
                        ? prefix
                        : isSale && price < 0
                        ? `−`
                        : undefined
                    }
                    value={priceToFormat(price)}
                    displayType={"text"}
                    thousandSeparator={" "}
                    renderText={(formattedValue: string) => (
                      <Span>{formattedValue}</Span>
                    )}
                  />
                  <Unit>
                    {currency} {unitMeasure && ` / ${unitMeasure}`}
                  </Unit>
                </>
              ) : (
                <>{msg}</>
              )}
            </StyledPrice>
            {children}
          </StyledPriceContainer>
        </Wrapper>
      )}
    </>
  )
}

export default Price
