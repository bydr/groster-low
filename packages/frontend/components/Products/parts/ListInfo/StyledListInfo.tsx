import { styled } from "@linaria/react"
import { breakpoints, colors } from "../../../../styles/utils/vars"
import { StyledList, StyledListItem } from "../../../List/StyledList"
import { getTypographyBase } from "../../../Typography/Typography"

export const StyledListInfo = styled.div`
  width: 100%;
  display: flex;
  flex-direction: column;
  list-style: none;
  margin: 0;
  padding: 0;

  ${StyledList} {
    ${getTypographyBase("p12")};
    ${StyledListItem} {
      color: ${colors.grayDark};
      margin-bottom: 4px;
      width: 100%;
      justify-content: space-between;

      &:last-child {
        margin-bottom: 0;
      }

      > * {
        width: inherit;
        display: inline;
        flex-direction: row;
        justify-content: inherit;
        margin-bottom: 0;

        > * {
          margin-right: 4px;

          &:last-child {
            margin-right: 0;
          }
        }
      }
    }
  }

  @media (max-width: ${breakpoints.sm}) {
    ${StyledList} {
      ${StyledListItem} {
        justify-content: flex-start;
      }
    }
  }
`
