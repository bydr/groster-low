import { FC } from "react"
import { Span, Typography } from "../../../Typography/Typography"
import { StyledList, StyledListItem } from "../../../List/StyledList"
import { StyledListInfo } from "./StyledListInfo"
import { ModalDefaultPropsType } from "../../../Modals/Modal"
import { ListMap } from "../../../Shops/ListMap"
import { StoreProductType } from "../../../../types/types"
import dynamic, { DynamicOptions } from "next/dynamic"
import { declinationOfNum } from "../../../../utils/helpers"
import { Trigger } from "../../../../styles/utils/Utils"

const DynamicModal = dynamic((() =>
  import("../../../Modals/Modal").then(
    (mod) => mod.Modal,
  )) as DynamicOptions<ModalDefaultPropsType>)

export const ListInfo: FC<{
  storesQty: number
  stores?: StoreProductType[]
}> = ({ storesQty, stores }) => {
  return (
    <>
      {storesQty > 0 && (
        <>
          <StyledListInfo>
            <StyledList>
              <StyledListItem>
                <Typography>
                  <Span>В наличии: </Span>
                  <DynamicModal
                    title={"Пункты самовывоза на карте"}
                    closeMode={"destroy"}
                    variant={"rounded-100"}
                    disclosure={
                      <Trigger>
                        на {storesQty}{" "}
                        {declinationOfNum(storesQty, [
                          "складе",
                          "складах",
                          "складах",
                        ])}
                      </Trigger>
                    }
                  >
                    <ListMap shops={stores || []} />
                  </DynamicModal>
                </Typography>
              </StyledListItem>
            </StyledList>
          </StyledListInfo>
        </>
      )}
    </>
  )
}
