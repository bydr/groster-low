import { styled } from "@linaria/react"
import {
  ButtonArea,
  ButtonBase,
  ButtonTranslucent,
  cssButtonInform,
} from "../../../Button/StyledButton"
import { css } from "@linaria/core"
import { cssIcon, cssIconShoppingCart } from "../../../Icon"
import { colors } from "../../../../styles/utils/vars"

export const cssAddToCartDetail = css``

export const StyledAddToCartControl = styled.div`
  grid-area: toCart;
  padding: 0;
  margin: 8px 0 4px;
  justify-content: space-between;
  width: 100%;

  ${ButtonBase} {
    width: 100%;
    margin: 0;
    padding: 2px;
    min-height: 40px;

    &.${cssButtonInform} {
      padding-right: 10px;
    }
  }

  &.${cssAddToCartDetail} {
    display: flex;

    ${ButtonArea} {
      height: 100%;
      align-items: center;
      justify-content: center;
      padding: 9px 20px;
    }

    ${ButtonBase} {
      .${cssIconShoppingCart} {
        margin-right: 5px;
        fill: ${colors.brand.yellow};
      }
    }

    ${ButtonTranslucent} {
      .${cssIcon} {
        fill: ${colors.brand.purple};
      }

      &:hover,
      &:active {
        .${cssIcon} {
          fill: ${colors.white};
        }
      }
    }

    > * {
      min-height: 56px;

      &:first-child {
        flex: 1;

        &${ButtonBase} {
          border-top-right-radius: 0.5em;
          border-bottom-right-radius: 0.5em;

          & + ${ButtonBase} {
            &${ButtonTranslucent} {
              border-top-left-radius: 0.5em;
              border-bottom-left-radius: 0.5em;
              margin-left: 4px;
            }
          }
        }
      }

      &:last-child {
        &${ButtonBase} {
          border-top-right-radius: 50px;
          border-bottom-right-radius: 50px;
        }
      }
      &:not(:first-child) {
        flex: 0;
        width: 56px;
        min-width: 56px;
        padding: 4px 10px 4px 4px;
      }
    }
  }
`
