import { FC, MouseEvent, ReactNode } from "react"
import {
  ButtonArea,
  cssButtonInCart,
  cssButtonInform,
} from "../../../Button/StyledButton"
import { Typography } from "../../../Typography/Typography"
import { Button } from "../../../Button"
import {
  cssAddToCartDetail,
  StyledAddToCartControl,
} from "./StyledAddToCartControl"
import { cx } from "@linaria/core"
import { cssIconShoppingCart, Icon } from "../../../Icon"
import { Modal } from "../../../Modals/Modal"
import { ReportAdmission } from "../../../Forms/ReportAdmission"
import { useRouter } from "next/router"
import { ROUTES } from "../../../../utils/constants"

export const AddToCartControl: FC<{
  buttonAreaContent?: ReactNode
  addToCart?: () => void
  inCart?: boolean
  isFetching?: boolean
  isAvailable: boolean
  variant?: "default" | "detail"
  uuid: string | null
  disabled?: boolean
}> = ({
  buttonAreaContent,
  addToCart,
  inCart = false,
  isFetching = false,
  isAvailable,
  children,
  variant = "default",
  uuid,
  disabled = false,
}) => {
  const router = useRouter()
  const addToCartHandler = (e: MouseEvent<HTMLButtonElement>) => {
    e.preventDefault()
    if (addToCart) {
      addToCart()
    }
  }

  return (
    <>
      {uuid !== null && (
        <>
          {isAvailable ? (
            <>
              <StyledAddToCartControl
                className={cx(variant === "detail" && cssAddToCartDetail)}
              >
                {!inCart ? (
                  <>
                    <Button
                      as={"div"}
                      variant={variant === "detail" ? "filled" : "translucent"}
                      onClick={addToCartHandler}
                      isFetching={isFetching}
                      withArea={!!buttonAreaContent}
                      size={variant === "detail" ? "large" : undefined}
                      disabled={disabled}
                    >
                      {buttonAreaContent && (
                        <ButtonArea
                          onClick={(e) => {
                            e.preventDefault()
                            e.stopPropagation()
                          }}
                        >
                          {buttonAreaContent}
                        </ButtonArea>
                      )}
                      <Typography variant={"span"}>В корзину</Typography>
                      {variant === "detail" && (
                        <Icon
                          NameIcon={"ShoppingCart"}
                          className={cssIconShoppingCart}
                          size={"default"}
                        />
                      )}
                    </Button>
                    {children}
                  </>
                ) : (
                  <>
                    <Button
                      variant={"filled"}
                      icon={"Check"}
                      iconPosition={"right"}
                      className={cssButtonInCart}
                      isFetching={isFetching}
                      size={variant === "detail" ? "large" : undefined}
                      onClick={() => {
                        void router.push(ROUTES.cart)
                      }}
                    >
                      <Typography variant={"span"}>В корзине</Typography>
                    </Button>
                  </>
                )}
              </StyledAddToCartControl>
            </>
          ) : (
            <>
              <StyledAddToCartControl
                className={cx(variant === "detail" && cssAddToCartDetail)}
              >
                <Modal
                  title={"Сообщить о поступлении"}
                  variant={"rounded-0"}
                  closeMode={"destroy"}
                  disclosure={
                    <Button
                      variant={"translucent"}
                      className={cssButtonInform}
                      size={variant === "detail" ? "large" : undefined}
                    >
                      <Typography
                        variant={"span"}
                        style={{
                          flex: 1,
                        }}
                      >
                        Сообщить о поступлении
                      </Typography>
                      {variant === "detail" && (
                        <Icon NameIcon={"Email"} size={"default"} />
                      )}
                    </Button>
                  }
                >
                  <ReportAdmission uuid={uuid} />
                </Modal>

                {children}
              </StyledAddToCartControl>
            </>
          )}
        </>
      )}
    </>
  )
}
