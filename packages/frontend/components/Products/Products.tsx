import type { BaseHTMLAttributes, FC } from "react"
import { VIEW_PRODUCTS_GRID, ViewProductsType } from "../../types/types"
import { StyledProducts } from "./StyledProducts"
import { Typography } from "../Typography/Typography"
import { memo } from "react"

export type ProductsPropsType = {
  view?: ViewProductsType
  withBorder?: boolean
  titleList?: string
}

export const Products: FC<
  ProductsPropsType & BaseHTMLAttributes<HTMLDivElement>
> = memo(
  ({
    titleList,
    view = VIEW_PRODUCTS_GRID,
    withBorder = false,
    children,
    ...props
  }) => {
    return (
      <>
        <StyledProducts
          data-view={view}
          data-with-border={withBorder}
          {...props}
        >
          {titleList && <Typography variant={"h3"}>{titleList}</Typography>}
          {children}
        </StyledProducts>
      </>
    )
  },
)

Products.displayName = "Products"
