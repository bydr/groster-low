import { FC, useMemo } from "react"
import {
  ControlsButtonsContainer,
  cssProductPreview,
  ProductContainer,
  ProductDetailInfo,
  StyledProduct,
} from "../../StyledProduct"
import Available from "../../../Available/Available"
import { Properties } from "../../parts/Properties/Properties"
import {
  VIEW_PRODUCTS_GRID,
  VIEW_PRODUCTS_SLIDE,
  ViewProductsType,
} from "../../../../types/types"
import { Counter } from "../../parts/Counter/Counter"
import { EntityImage } from "../../../EntityImage/EntityImage"
import Price from "../../parts/Price/Price"
import { AddToCartControl } from "../../parts/AddToCartControl/AddToCartControl"
import type { ProductCatalogType } from "../../../../../contracts/contracts"
import { useProduct } from "../../../../hooks/product/product"
import { UnitControl } from "../../parts/UnitControl/UnitControl"
import { useSample } from "../../../../hooks/sample"
import { ButtonToggleFavorite } from "../../parts/ButtonToggleFavorite/ButtonToggleFavorite"
import { ListInfo } from "../../parts/ListInfo/ListInfo"
import { Title } from "../../parts/Title/Title"
import { ButtonToggleSample } from "../../parts/ButtonToggleSample/ButtonToggleSample"
import { ButtonFastOrder } from "../../parts/ButtonFastOrder/ButtonFastOrder"
import { Badges } from "../../Badges"
import { PreviewSlider } from "../../parts/PreviewSlider"
import { useWindowSize } from "../../../../hooks/windowSize"
import { breakpoints } from "../../../../styles/utils/vars"
import { getBreakpointVal } from "../../../../styles/utils/Utils"
import { SkeletonProduct } from "../../Skeleton/Skeleton"
import { ImageTile } from "../../parts/ImageTile"
import { AfterAddedToCartKit } from "../../AfterAddedToCartKit"
import dynamic, { DynamicOptions } from "next/dynamic"
import type { ModalDefaultPropsType } from "../../../Modals/Modal"
import { fetchProductsList, productsAPI } from "../../../../api/productsAPI"
import Button from "../../../Button/Button"
import { cssZoomIn } from "../../Detail/StyledDetail"
import { GallerySlider } from "../../parts/GallerySlider/GallerySlider"
import { useQuery } from "react-query"
import { sortProductsWeightRule } from "../../../../utils/helpers"
import { CURRENCY } from "../../../../utils/constants"
import { cx } from "@linaria/core"

const LIMIT_ANALOGS = 7

const DynamicModal = dynamic((() =>
  import("../../../Modals/Modal").then(
    (mod) => mod.Modal,
  )) as DynamicOptions<ModalDefaultPropsType>)

export type CatalogProductPropsType = {
  product: ProductCatalogType
  view: ViewProductsType
  index?: number
}

const QUALITY_MOBILE = 15
const QUALITY_PC = 30
const PRIORITY_IND = 5

export const Product: FC<
  CatalogProductPropsType & {
    initQty?: number
    isShared?: boolean
    isPreview?: boolean
    sortWeight?: number
    categories?: string
  }
> = ({
  product,
  view,
  index,
  initQty,
  isShared,
  isPreview = false,
  sortWeight,
  categories,
}) => {
  const {
    uuid,
    isShowedDetail,
    setIsShowedDetail,
    isFavorites,
    availableStatus,
    toggleFavorite,
    path,
    name,
    units,
    updateCurrentCount,
    updateCurrentUnit,
    totalQty,
    priceCalculate,
    priceUnit,
    unitMeasure,
    currentUnit,
    currency,
    isFetching,
    addToCart,
    properties,
    inCart,
    isRemoved,
    storesQty,
    isFetchingFavorites,
    storesAvailable,
    images,
    isBestseller,
    isKit,
    counter,
    currentCount,
    isCountError: isCountProductError,
    setIsCountError,
    isInit: isInitProduct,
    isAdded,
    kit,
    kitParents,
    isAvailable,
    isNew,
    setIsAdded,
    isAllowSample,
  } = useProduct({
    product: product,
    options: {
      isSaveOnRemove: false,
      initQty: initQty,
    },
  })

  const {
    toggle,
    isFetching: isFetchingSample,
    isCountError: isCountSampleError,
    isInit: isInitSample,
    qtyInCart: qtyInCartSample,
    isDisabled: isDisabledSample,
  } = useSample({
    sample: {
      uuid: uuid || undefined,
      total_qty: product.total_qty,
      price: priceUnit,
      unit: unitMeasure || undefined,
    },
  })

  const isInitEntity = useMemo(() => {
    return isInitProduct && isInitSample && uuid !== null
  }, [isInitProduct, isInitSample, uuid])

  const makedAnalogs = useMemo(
    () =>
      [
        ...(product?.analogs?.fast || []),
        ...(product?.analogs?.other || []),
      ].filter((uuid) => uuid !== undefined),
    [product?.analogs],
  )

  const { data: kitProducts } = productsAPI.useProductsFetch(
    isAdded && kit.length > 0 ? kit.join(",") : null,
  )

  const { data: analogs } = useQuery(
    ["productAnalogs", isAdded, makedAnalogs],
    () =>
      isAdded && makedAnalogs.length > 0
        ? fetchProductsList({
            uuids: makedAnalogs.map((item) => item.uuid).join(","),
          })
        : null,
  )

  const { data: companions } = useQuery(
    ["companions", isAdded, product?.companions],
    () =>
      isAdded && (product?.companions || []).length > 0
        ? fetchProductsList({
            uuids: (product?.companions || [])
              .filter((_, i) => i < LIMIT_ANALOGS)
              .filter((i) => !!i.uuid)
              .map((item) => item.uuid)
              .join(","),
          })
        : null,
  )

  const { width } = useWindowSize()

  return (
    <>
      <StyledProduct
        uuid={uuid || undefined}
        data-alias={product.alias}
        sortWeight={sortWeight}
        categories={categories}
        isShowDetailInfo={isShowedDetail}
        isFavorite={isFavorites}
        viewProductsVariant={view}
        availableStatus={availableStatus || undefined}
        onMouseEnter={() => {
          if (!isInitEntity) {
            return
          }
          setIsShowedDetail(true)
        }}
        onMouseLeave={() => {
          if (!isInitEntity) {
            return
          }
          setIsShowedDetail(false)
        }}
        className={cx(isPreview && cssProductPreview)}
      >
        {!isInitEntity && (
          <>
            <SkeletonProduct />
          </>
        )}
        <ProductContainer>
          <Badges isBestseller={isBestseller} isKit={isKit} isNew={isNew} />
          <ButtonToggleFavorite
            isFavorites={isFavorites}
            toggleFavorite={toggleFavorite}
            isFetching={isFetchingFavorites}
          />

          {isKit && (
            <>
              <DynamicModal
                closeMode={"destroy"}
                variant={"rounded-70"}
                isShowModal={isAdded}
                title={"Добавлен в корзину"}
              >
                <AfterAddedToCartKit
                  product={product}
                  companions={sortProductsWeightRule({
                    products: companions || [],
                    rules: product?.companions,
                  })}
                  kitProducts={kitProducts || []}
                  analogs={sortProductsWeightRule({
                    products: analogs || [],
                    rules: makedAnalogs,
                  })}
                  onClose={() => {
                    setIsAdded(false)
                  }}
                />
              </DynamicModal>
            </>
          )}

          {kitParents.length > 0 && (
            <>
              <DynamicModal
                closeMode={"destroy"}
                variant={"rounded-70"}
                isShowModal={isAdded}
                title={"Добавлен в корзину"}
              >
                <AfterAddedToCartKit
                  product={product}
                  companions={sortProductsWeightRule({
                    products: companions || [],
                    rules: product?.companions,
                  })}
                  kitParents={kitParents || []}
                  analogs={sortProductsWeightRule({
                    products: analogs || [],
                    rules: makedAnalogs,
                  })}
                  onClose={() => {
                    setIsAdded(false)
                  }}
                />
              </DynamicModal>
            </>
          )}

          <ImageTile
            path={path}
            isSlider={view === VIEW_PRODUCTS_GRID && images.length > 1}
          >
            {view === VIEW_PRODUCTS_GRID && images.length > 1 ? (
              <>
                <PreviewSlider
                  alt={name || ""}
                  images={images}
                  url={path}
                  layout={"intrinsic"}
                  width={200}
                  height={200}
                  objectFit={"contain"}
                  quality={
                    width !== undefined &&
                    width <= getBreakpointVal(breakpoints.md)
                      ? QUALITY_MOBILE
                      : QUALITY_PC
                  }
                  priority={index !== undefined && index < PRIORITY_IND}
                />
              </>
            ) : (
              <>
                <EntityImage
                  imagePath={images.length > 0 ? images[0] : undefined}
                  imageAlt={name || ""}
                  layout={"intrinsic"}
                  width={200}
                  height={200}
                  objectFit={"contain"}
                  quality={
                    width !== undefined &&
                    width <= getBreakpointVal(breakpoints.md)
                      ? QUALITY_MOBILE
                      : QUALITY_PC
                  }
                  priority={index !== undefined && index < PRIORITY_IND}
                />
              </>
            )}
            {images.length > 0 && (
              <>
                <DynamicModal
                  closeMode={"destroy"}
                  variant={"full"}
                  disclosure={
                    <Button
                      icon={"ZoomIn"}
                      variant={"box"}
                      className={cssZoomIn}
                    />
                  }
                >
                  <GallerySlider images={images} />
                </DynamicModal>
              </>
            )}
          </ImageTile>

          <Title name={name || ""} path={path} />

          <Price
            price={priceUnit}
            currency={currency}
            unitMeasure={unitMeasure}
          />

          {isAvailable && (
            <>
              {!isPreview && (
                <UnitControl
                  setCurrentUnit={updateCurrentUnit}
                  unitMeasure={unitMeasure}
                  units={units}
                  totalQty={totalQty}
                  unitValueActive={currentUnit || undefined}
                  isInitProduct={isInitEntity}
                />
              )}
              {view !== VIEW_PRODUCTS_SLIDE && (
                <Counter
                  counter={counter}
                  currentCount={currentCount}
                  currentUnit={currentUnit}
                  unitMeasure={unitMeasure}
                  maxCount={totalQty}
                  isFetching={isFetching}
                  updateCountHandler={updateCurrentCount}
                  productInCart={inCart}
                  productIsRemove={isRemoved}
                  isCountError={isCountSampleError || isCountProductError}
                  setIsCountError={setIsCountError}
                  isInitProduct={isInitEntity}
                  isStatic={(isShared && !inCart) || isPreview}
                />
              )}
            </>
          )}

          {!isPreview && (
            <>
              <AddToCartControl
                uuid={uuid}
                isAvailable={isAvailable}
                isFetching={isFetching}
                inCart={inCart && !isRemoved}
                addToCart={addToCart}
                buttonAreaContent={
                  <>
                    {view !== VIEW_PRODUCTS_SLIDE && (
                      <Price
                        variant={"total"}
                        currency={currency}
                        price={priceCalculate}
                      />
                    )}
                    {view === VIEW_PRODUCTS_SLIDE && (
                      <Counter
                        counter={counter}
                        currentCount={currentCount}
                        currentUnit={currentUnit}
                        unitMeasure={unitMeasure}
                        maxCount={totalQty}
                        isFetching={isFetching}
                        updateCountHandler={updateCurrentCount}
                        productInCart={inCart}
                        productIsRemove={isRemoved}
                        isCountError={isCountSampleError || isCountProductError}
                        setIsCountError={setIsCountError}
                        isInitProduct={isInitEntity}
                      />
                    )}
                  </>
                }
              />
            </>
          )}

          {isPreview && (
            <Price
              variant={"total"}
              price={isAvailable ? priceCalculate : 0}
              currency={CURRENCY}
              messageImportant={
                isRemoved && isAvailable ? undefined : "нет в наличии"
              }
            />
          )}

          <ListInfo storesQty={storesQty} stores={storesAvailable} />

          <Available status={availableStatus} />

          {isAvailable && (
            <>
              <ProductDetailInfo>
                <ButtonFastOrder
                  product={{
                    inCart,
                    isRemoved,
                    addToCart,
                  }}
                />
                <ControlsButtonsContainer>
                  <ButtonToggleSample
                    isFetchingSample={isFetchingSample}
                    toggleSampleToCart={toggle}
                    sample={qtyInCartSample || 0}
                    isDisabled={isDisabledSample}
                    isAllow={isAllowSample}
                  />
                </ControlsButtonsContainer>
                <Properties items={properties}>
                  <ListInfo storesQty={storesQty} stores={storesAvailable} />
                </Properties>
              </ProductDetailInfo>
            </>
          )}
        </ProductContainer>
      </StyledProduct>
    </>
  )
}
