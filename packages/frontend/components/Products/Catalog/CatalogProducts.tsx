import type { FC } from "react"
import { useEffect, useState } from "react"
import { ProductType, ViewProductsType } from "../../../types/types"
import { cssWithBanners, StyledCatalogProducts } from "../StyledProducts"
import { Products } from "../Products"
import { BaseLoader } from "../../Loaders/BaseLoader/BaseLoader"
import { Product } from "./Product/Product"
import { Typography } from "../../Typography/Typography"
import { useAppSelector } from "../../../hooks/redux"
import { RequestBanner } from "../../../../contracts/contracts"
import { cx } from "@linaria/core"
import { Banner } from "../../Banners/Banner"
import { StyledBannerSection } from "../../Banners/Banner/Styled"
import { getRandomInt } from "../../../utils/helpers"

export type ProductsPropsType = {
  view: ViewProductsType
  products: ProductType[] | null
  isFetching?: boolean
  withBanner?: boolean
  page?: number
}

export const CatalogProducts: FC<ProductsPropsType> = ({
  view,
  products,
  isFetching = false,
  withBanner = true,
  page,
  children,
}) => {
  const banners = useAppSelector((state) => state.app.banners)
  const [productsBanners, setProductsBanners] = useState<
    RequestBanner[] | null
  >(null)
  const [currentBanner, setCurrentBanner] = useState<RequestBanner | null>(null)

  useEffect(() => {
    if (banners !== null) {
      setProductsBanners([...banners["catalog_products_single"]] || [])
    } else {
      setProductsBanners(null)
    }
  }, [banners])

  useEffect(() => {
    if (withBanner && productsBanners !== null && productsBanners.length > 0) {
      setCurrentBanner(
        productsBanners[
          productsBanners.length > 1 ? getRandomInt(productsBanners.length) : 0
        ],
      )
    } else {
      setCurrentBanner(null)
    }
  }, [productsBanners, withBanner, page])

  return (
    <>
      <StyledCatalogProducts
        data-view-products={view}
        className={cx(
          productsBanners !== null &&
            productsBanners.length > 0 &&
            cssWithBanners,
        )}
      >
        {isFetching && <BaseLoader />}
        {products !== null && (
          <>
            <Products view={view}>
              {products.length > 0 && (
                <>
                  {products.map((p, i) => (
                    <Product view={view} key={p.uuid} product={p} index={i} />
                  ))}
                  {children}
                </>
              )}
              {!isFetching && products.length <= 0 && (
                <>
                  <Typography variant={"p14"}>Товаров не найдено</Typography>
                </>
              )}
              {withBanner && currentBanner !== null && (
                <>
                  <StyledBannerSection>
                    <Banner
                      image={{
                        desktop: {
                          src: currentBanner.desktop || "",
                          layout: "responsive",
                          width: "534px",
                          height: "488px",
                          objectPosition: "center",
                        },
                        tablet: {
                          src: currentBanner.tablet,
                          layout: "responsive",
                          width: "534px",
                          height: "488px",
                          objectPosition: "center",
                        },
                        mobile: {
                          src: currentBanner.mobile,
                          layout: "responsive",
                          width: "534px",
                          height: "488px",
                          objectPosition: "center",
                        },
                      }}
                      weight={currentBanner.weight}
                      url={currentBanner.url}
                      withHover={false}
                    />
                  </StyledBannerSection>
                </>
              )}
            </Products>
          </>
        )}
      </StyledCatalogProducts>
    </>
  )
}
