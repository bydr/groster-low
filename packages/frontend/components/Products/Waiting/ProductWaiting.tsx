import type { FC } from "react"
import { ViewProductsType } from "../../../types/types"
import type { ProductCatalogType } from "../../../../contracts/contracts"
import { useProduct } from "../../../hooks/product/product"
import { ProductContainer, StyledProduct } from "../StyledProduct"
import { ButtonToggleFavorite } from "../parts/ButtonToggleFavorite/ButtonToggleFavorite"
import { EntityImage } from "../../EntityImage/EntityImage"
import { Title } from "../parts/Title/Title"
import { ListInfo } from "../parts/ListInfo/ListInfo"
import { cssProductWaiting } from "./Styled"
import { Button } from "../../Button"
import { StyledAddToCartControl } from "../parts/AddToCartControl/StyledAddToCartControl"
import { useMutation } from "react-query"
import { fetchProductWaitingRemove } from "../../../api/productsAPI"
import { useState } from "react"

export const ProductWaiting: FC<{
  product: ProductCatalogType
  view: ViewProductsType
}> = ({ product, view }) => {
  const {
    uuid,
    name,
    isFavorites,
    toggleFavorite,
    isFetchingFavorites,
    images,
    path,
    storesQty,
    storesAvailable,
  } = useProduct({
    product: product,
  })

  const [isUnsubscribe, setIsUnsubscribe] = useState(false)

  const { mutate: waitingRemoveMutate, isLoading: isFetchingRemoved } =
    useMutation(() => fetchProductWaitingRemove({ product: uuid || "" }), {
      onSuccess: () => {
        setIsUnsubscribe(true)
      },
    })

  return (
    <>
      <StyledProduct
        isFavorite={isFavorites}
        viewProductsVariant={view}
        className={cssProductWaiting}
        isRemoved={isUnsubscribe}
      >
        <ProductContainer>
          <ButtonToggleFavorite
            isFavorites={isFavorites}
            toggleFavorite={toggleFavorite}
            isFetching={isFetchingFavorites}
          />
          <EntityImage
            imagePath={images.length > 0 ? images[0] : undefined}
            imageAlt={name || ""}
          />
          <Title name={name || ""} path={path} />
          <StyledAddToCartControl>
            <Button
              variant={"translucent"}
              onClick={(e) => {
                e.preventDefault()
                waitingRemoveMutate()
              }}
              isFetching={isFetchingRemoved}
            >
              Отписаться
            </Button>
          </StyledAddToCartControl>
          <ListInfo storesQty={storesQty} stores={storesAvailable} />
        </ProductContainer>
      </StyledProduct>
    </>
  )
}
