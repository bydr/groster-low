import { styled } from "@linaria/react"
import { css, cx } from "@linaria/core"
import { breakpoints, colors } from "../../styles/utils/vars"
import {
  getTypographyBase,
  Paragraph12,
  Paragraph14,
  Span,
} from "../Typography/Typography"
import {
  ButtonArea,
  ButtonBase,
  ButtonTranslucent,
  cssButtonAddFavorite,
  cssButtonCompoundToggle,
} from "../Button/StyledButton"
import {
  StyledAvailable,
  StyledAvailableInner,
} from "../Available/StyledAvailable"
import { cssIcon, getSizeSVG } from "../Icon"
import {
  AvailableStatusType,
  VIEW_PRODUCTS_CHECKOUT,
  VIEW_PRODUCTS_CHECKOUT_MINI,
  VIEW_PRODUCTS_CHILD,
  VIEW_PRODUCTS_GRID,
  VIEW_PRODUCTS_LIST,
  VIEW_PRODUCTS_ORDER,
  VIEW_PRODUCTS_SAMPLE,
  VIEW_PRODUCTS_SAMPLE_MINI,
  VIEW_PRODUCTS_SAMPLE_ORDER,
  VIEW_PRODUCTS_SLIDE,
  ViewProductsType,
} from "../../types/types"
import { StyledProperties } from "./parts/Properties/StyledProperties"
import {
  cssRadioGroup,
  RadioLabel,
  StyledRadioGroup,
  StyledRadioInner,
} from "../Radio/StyledRadio"
import { CounterInput, StyledCounter } from "./parts/Counter/StyledCounter"
import { IconPoint } from "../../styles/utils/Utils"
import {
  StyledTotalPriceWrapper,
  StyledUnitPriceWrapper,
} from "./parts/Price/StyledPrice"
import { BaseHTMLAttributes, forwardRef } from "react"
import { StyledLoaderOverlay } from "../Loaders/BaseLoader/StyledBaseLoader"
import {
  PopoverContainer,
  StyledPopoverDisclosure,
} from "../Popover/StyledPopover"
import { StyledListInfo } from "./parts/ListInfo/StyledListInfo"
import { StyledListItem } from "../List/StyledList"
import { StyledEntityImage } from "../EntityImage/Styled"
import { StyledAddToCartControl } from "./parts/AddToCartControl/StyledAddToCartControl"
import { StyledBadges } from "./Badges/Styled"
import { StyledImageTile } from "./parts/ImageTile/StyledImageTile"

export const cssTooltipDate = css``

export const ProductImageSize = {
  detail: "271px",
  grid: "200px",
  list: "100px",
  small: "60px",
}

export const cssProductAnimate = css``

export const cssProductPreview = css``

export const StyledDeliveryMessage = styled(Paragraph12)`
  color: ${colors.grayDark};
  margin-bottom: 0;

  * {
    color: ${colors.black};
  }
`

export const ProductTitle = styled(Paragraph14)`
  margin-bottom: 4px;

  &:hover,
  &:active {
    color: ${colors.brand.purple};
  }
  * {
    font-size: inherit;
    line-height: inherit;
    color: inherit;
  }
`

export const ProductDetailInfo = styled.div`
  width: 100%;
  grid-area: more;
`
export const ProductContainer = styled.div`
  position: relative;
  padding: 30px 24px;
  display: grid;
  grid-template-areas:
    "image image image image image"
    "title title title title title"
    "tabs tabs tabs tabs tabs"
    "price price counter counter counter"
    "toCart toCart toCart toCart toCart"
    "available available available available available"
    "more more more more more";
  grid-template-columns: 1fr 1fr 1fr 1fr 1fr;
  background: inherit;
  border-radius: inherit;
  justify-items: self-start;
  width: 100%;

  ${ProductTitle} {
    grid-area: title;
    margin-bottom: 10px;
  }

  ${StyledImageTile} {
    grid-area: image;
  }

  ${StyledEntityImage} {
    height: ${ProductImageSize.grid};
  }

  ${IconPoint} {
    ${getTypographyBase("p12")};
    color: ${colors.grayDark};
  }
  .${cssRadioGroup}, ${IconPoint} {
    grid-area: tabs;
  }
  ${StyledUnitPriceWrapper} {
    grid-area: price;
  }
  ${StyledCounter} {
    grid-area: counter;
  }
  ${StyledAvailable} {
    grid-area: available;
  }
  .${cssRadioGroup} {
    width: 100%;
  }
  ${RadioLabel} {
    flex: 1;
    text-align: center;
    padding: 10px 5px;
    line-height: 135%;
  }
`

export const ButtonsContainer = styled.div`
  ${ButtonBase} {
    margin-right: 8px;
    &:last-of-type {
      margin-right: 0;
    }
  }

  @media (max-width: ${breakpoints.sm}) {
    flex-direction: column;
    align-items: flex-start;

    ${ButtonBase} {
      margin-right: 0;
      margin-bottom: 8px;
    }
  }
`
export const ControlsButtonsContainer = styled(ButtonsContainer)``
export const ControlsContainerDetail = styled(ButtonsContainer)`
  display: flex;
  width: 100%;
  gap: 10px 26px;

  ${ButtonBase} {
    justify-content: flex-start;
    text-align: left;
    padding-right: 0;
    padding-left: 0;
  }

  @media (max-width: ${breakpoints.xl}) {
    display: grid;
    grid-template-columns: repeat(auto-fit, minmax(160px, 1fr));
  }

  @media (max-width: ${breakpoints.sm}) {
    grid-template-columns: 1fr 50px;

    > * {
      grid-column: 1;

      &:last-child {
        grid-row: 2;
        grid-column: 2;

        &,
        ${ButtonBase} {
          justify-content: center;
          text-align: center;
        }
      }
      &:nth-last-child(2) {
        grid-row: 1;
        grid-column: 2;

        &,
        ${ButtonBase} {
          justify-content: center;
          text-align: center;
        }
      }
    }
  }
`
export const ShowedVariantsButtonsContainer = styled(ButtonsContainer)`
  display: flex;
  align-items: center;

  @media (max-width: ${breakpoints.sm}) {
    flex-direction: column;
    align-items: flex-start;
  }
`

export const cssProductPush = css``

export const StyledProductTag = styled.div`
  position: relative;
  border: 1px solid transparent;
  border-right-color: ${colors.gray};
  border-bottom-color: ${colors.gray};
  background: ${colors.white};

  &:first-child {
    border-top-right-radius: inherit;
    border-top-left-radius: inherit;
  }
  &:last-child {
    border-bottom-right-radius: inherit;
    border-bottom-left-radius: inherit;
  }

  &:before {
    content: "";
    position: absolute;
    left: 0;
    top: 0;
    width: 100%;
    height: 100%;
    transition: all 0.7s ease-in-out 0.2s;
    border-radius: inherit;
    opacity: 0;
    transform: scale(1.02);
  }

  &.${cssProductAnimate} {
    &:before {
      box-shadow: 0 0 0 6px ${colors.brand.purpleTransparent15},
        0 0 0 14px ${colors.brand.purpleTransparent15},
        0 0 0 28px ${colors.brand.purpleTransparent15},
        rgba(17, 17, 26, 0.1) 0 4px 16px, rgba(17, 17, 26, 0.1) 0 8px 24px,
        rgba(17, 17, 26, 0.1) 0 16px 56px;
      opacity: 1;
      transform: scale(1);
    }

    ${StyledCounter} {
      ${CounterInput} {
        transform: scale(1.2);

        input {
          font-weight: 600;
          color: ${colors.brand.purple};
        }
      }
    }
  }

  ${ButtonTranslucent} {
    margin: 8px 0 4px 0;
  }

  ${ControlsButtonsContainer} {
    display: flex;
    width: 100%;
    flex-direction: column;
    align-items: center;
    grid-area: tags;
    margin: 0;
  }

  .${cssButtonAddFavorite} {
    position: absolute;
    top: 15px;
    left: auto;
    right: 20px;
    z-index: 4;
    padding: 5px;
  }

  ${ProductDetailInfo} {
    display: none;
    position: absolute;
    z-index: 5;
    left: -25px;
    right: -25px;
    width: auto;
    background: ${colors.grayLight};
    padding: 0 24px;
    top: -20px;
  }

  ${StyledAvailable} {
    margin-top: 16px;
  }

  ${StyledProperties} {
    margin-bottom: 10px;
  }

  .${cssRadioGroup} {
    margin-top: 10px;
    margin-bottom: 10px;
  }

  ${IconPoint} {
    margin-bottom: 10px;
    min-height: 30px;
  }

  &.${cssProductPush} {
    z-index: 2;
    box-shadow: rgb(144 22 182 / 10%) 0 4px 16px,
      rgb(144 22 182 / 10%) 0 8px 24px, rgb(144 22 182 / 10%) 0 16px 56px;
  }

  ${ProductContainer} {
    ${StyledListInfo} {
      grid-column: 1 / -1;

      ${StyledListItem} {
        justify-content: flex-start;
      }
    }

    ${StyledBadges} {
      width: auto;
      grid-area: badges;
    }
  }

  &[data-view=${VIEW_PRODUCTS_GRID}] {
    border-radius: 0;
    ${ProductContainer} {
      border-radius: 0;

      ${StyledListInfo} {
        grid-row: 6;
        grid-column: 1 / -1;

        + ${StyledAvailable} {
          grid-row: 7;
        }
      }

      ${StyledEntityImage} {
        height: ${ProductImageSize.grid};
        width: ${ProductImageSize.grid};

        .${cssIcon} {
          ${getSizeSVG("100px")};
        }

        @media (max-width: ${breakpoints.sm}) {
          min-height: initial;

          .${cssIcon} {
            ${getSizeSVG("60px")};
          }
        }
      }

      ${StyledBadges} {
        position: absolute;
        left: 10px;
        top: 20px;
        z-index: 2;
      }

      ${IconPoint} {
        margin-top: 6px;
        margin-bottom: 6px;
      }

      .${cssRadioGroup} {
        margin-top: 6px;
        margin-bottom: 0;
      }
    }

    &[data-isshowdetail="true"] {
      ${ProductContainer} {
        ${StyledListInfo} {
          opacity: 0;
          visibility: hidden;
        }

        ${ProductDetailInfo} {
          ${StyledListInfo} {
            display: flex;
            opacity: 1;
            visibility: visible;
            position: relative;
          }
        }
      }

      @media (max-width: ${breakpoints.lg}) {
        ${ProductContainer} {
          ${StyledListInfo} {
            display: flex;
            opacity: 1;
            visibility: visible;
          }
        }
      }
    }

    &[data-available-status="not"] {
      ${StyledCounter} {
        display: none;
      }
      ${StyledUnitPriceWrapper} {
        grid-column: 1 / -1;
        align-items: center;
      }
    }

    @media (min-width: ${breakpoints.md}) {
      &[data-isshowdetail="true"] {
        background: ${colors.grayLight};

        ${ButtonTranslucent} {
          background: ${colors.brand.purple};
          color: ${colors.white};

          ${ButtonArea} {
            background: ${colors.transparentWhite};
            color: ${colors.brand.yellow};
          }
        }

        ${ProductDetailInfo} {
          display: block;
        }

        ${StyledAvailable} {
          opacity: 0;
          visibility: hidden;
        }
      }
    }
  }

  &[data-view=${VIEW_PRODUCTS_LIST}], 
  &[data-view=${VIEW_PRODUCTS_ORDER}], 
  &[data-view=${VIEW_PRODUCTS_SAMPLE_ORDER}] {
    width: 100%;
    border-right-color: transparent;

    ${ProductContainer} {
      padding: 16px;
      grid-template-columns: repeat(15, 1fr);
      grid-template-areas:
        "image image badges badges badges badges badges . . . . . . . ."
        "image image title title  title   title  title   title   title   . . .      available  available available"
        "image image tabs  tabs tabs   price   price  counter counter counter toCart toCart toCart     toCart    toCart";
      align-items: flex-start;
      gap: 5px 10px;

      ${StyledListInfo} {
        grid-column-start: 3;
        grid-row: -1;

        ${StyledListItem} {
          justify-content: flex-start;
        }
      }

      ${StyledUnitPriceWrapper} {
        align-items: center;
      }

      .${cssRadioGroup} {
        flex-direction: column;
        text-align: left;
        align-items: flex-start;
        margin: 0;

        ${StyledRadioGroup} {
          flex-direction: column;
        }

        ${RadioLabel} {
          padding: 4px 10px;
          text-align: left;

          ${StyledRadioInner} {
            &:before {
              top: 0;
              left: 0;
              height: 100%;
              width: 2px;
            }
          }
        }
      }

      ${ProductTitle} {
        margin-bottom: 0;
      }

      ${ButtonTranslucent} {
        margin: 0;
      }

      .${cssButtonAddFavorite} {
        top: 16px;
        left: 16px;
        right: auto;
      }

      ${StyledEntityImage} {
        width: ${ProductImageSize.list};
        height: ${ProductImageSize.list};
        grid-row: 1 / -1;
      }

      ${IconPoint} {
        margin-bottom: 0;
      }

      ${StyledAvailable} {
        margin-top: 0;
        justify-content: flex-end;
        width: 100%;

        .${cssIcon} {
          &:last-of-type {
            display: none;
          }
        }
      }

      ${StyledBadges} {
        position: relative;
        right: auto;
        left: auto;
      }
    }

    &.${cssProductPreview} {
      ${ProductContainer} {
        ${StyledTotalPriceWrapper} {
          grid-area: toCart;
          justify-content: flex-end;
        }
      }
    }
  }

  &[data-view=${VIEW_PRODUCTS_CHECKOUT}],
  &[data-view=${VIEW_PRODUCTS_SAMPLE}],
  &[data-view=${VIEW_PRODUCTS_CHILD}] {
    ${ProductContainer} {
      display: grid;
      grid-template-columns: repeat(18, 1fr);
      grid-template-areas: initial;
      align-items: center;
      gap: 4px 18px;
      padding: 24px;

      @media (max-width: ${breakpoints.xl}) {
        gap: 4px 10px;
      }

      > * {
        grid-column: 2 / end;
      }

      .${cssButtonCompoundToggle} {
        grid-column: 2/7;
        display: inline-flex;
      }

      ${PopoverContainer} {
        grid-column: initial;
        grid-row: initial;
      }

      ${StyledImageTile} {
        grid-column: 1;
        grid-row: 1 / 4;
      }

      ${StyledEntityImage} {
        width: ${ProductImageSize.list};
        height: ${ProductImageSize.list};
      }

      ${ProductTitle} {
        grid-column: 2/16;
        grid-row: 1;
        margin-right: 0;
        margin-bottom: 0;
      }

      .${cssRadioGroup} {
        grid-column: 2 / end;
        grid-row: 2;
        margin: 0;
      }

      ${StyledUnitPriceWrapper} {
        grid-column: 2/5;
        grid-row: 3;
      }

      ${StyledCounter} {
        grid-column: 5/9;
        grid-row: 3;
      }

      ${StyledDeliveryMessage} {
        grid-column: 11 / 17;
        grid-row: 3;
        text-align: right;
        display: inline-block;
        width: 100%;
      }

      ${StyledTotalPriceWrapper} {
        grid-column: 16 / end;
        grid-row: 1;
        display: flex;
        justify-content: flex-end;
        width: 100%;
      }

      ${ControlsButtonsContainer} {
        align-items: flex-end;
        grid-area: initial;
        grid-column: 18 / end;
        grid-row: 3;
      }

      ${IconPoint} {
        margin: 0;
      }

      ${StyledListInfo} {
        ${StyledListItem} {
          justify-content: flex-start;
        }
      }
    }
  }

  &[data-view^="checkout"],
  &[data-view^="sample"],
  &[data-view^="child"],
  &[data-view=${VIEW_PRODUCTS_GRID}] {
    &[data-is-removed="true"] {
      background: ${colors.grayLight};

      ${ProductContainer} {
        > *:not(${ControlsButtonsContainer}) {
          pointer-events: none !important;
          user-select: none !important;
          cursor: default !important;
          opacity: 0.3;
        }
      }
    }
  }

  &[data-view=${VIEW_PRODUCTS_CHECKOUT}] {
    ${ProductContainer} {
      .${cssRadioGroup} {
        grid-column: 2 / 8;
      }
      ${StyledUnitPriceWrapper} {
        grid-column: 2/4;
        grid-row: 3;
      }
      ${StyledCounter} {
        grid-column: 4/8;
        grid-row: 3;
      }
    }
  }

  &[data-view=${VIEW_PRODUCTS_CHILD}] {
    ${ProductContainer} {
      ${ControlsButtonsContainer} {
        grid-column: 13 / end;
        grid-template-areas: initial;
      }
      ${StyledUnitPriceWrapper}, ${StyledDeliveryMessage}, ${ControlsButtonsContainer} {
        grid-row: 3;
      }
      ${StyledCounter} {
        grid-row: 3;
      }
      .${cssRadioGroup} {
        grid-row: 2;
      }
    }
  }

  &[data-view=${VIEW_PRODUCTS_SAMPLE}] {
    ${ProductContainer} {
      ${StyledCounter} {
        grid-column: 2/5;
        grid-row: 2;
      }
      ${StyledDeliveryMessage} {
        grid-column: 11/17;
        grid-row: 2;
      }
      ${ControlsButtonsContainer} {
        grid-row: 2;
      }
    }
  }

  &[data-view=${VIEW_PRODUCTS_SLIDE}], &[data-view=${VIEW_PRODUCTS_GRID}] {
    ${ProductTitle} {
      line-height: 170%;

      ${Span} {
        height: 46px;
        /* text-overflow: ellipsis; */
        text-align: left;
        display: -webkit-box;
        -webkit-line-clamp: 2;
        -webkit-box-orient: vertical;
        overflow: hidden;
        line-clamp: 2;
        -webkit-line-clamp: 2;
        -webkit-box-orient: vertical;
      }
    }
  }
  &[data-view=${VIEW_PRODUCTS_SLIDE}] {
    max-width: 290px;
    border-bottom-color: transparent;

    ${ProductContainer} {
      grid-template-areas:
        "image image image image image"
        "title title title title title"
        "tabs tabs tabs tabs tabs"
        "price price available available available"
        "toCart toCart toCart toCart toCart"
        "more more more more more";

      ${StyledAvailable} {
        grid-area: available;
        margin: 0;

        ${StyledAvailableInner} {
          justify-content: flex-end;
        }
      }

      ${StyledCounter} {
        margin: -1px -10px;
      }

      ${StyledBadges} {
        position: absolute;
        left: 10px;
        top: 20px;
        z-index: 2;
      }
    }
  }

  &[data-view=${VIEW_PRODUCTS_CHECKOUT_MINI}], 
  &[data-view=${VIEW_PRODUCTS_SAMPLE_MINI}] {
    ${ProductContainer} {
      padding: 12px 15px 12px 15px;
      grid-template-columns: 1fr 1fr 1fr 1fr;
      grid-template-areas:
        "title  title   title   title"
        "tabs   tabs    tabs    available"
        "price  counter counter image"
        "toCart toCart  toCart  image";
      align-items: center;
      grid-column-gap: 6px;

      ${StyledUnitPriceWrapper} {
        grid-column: initial;
        align-items: center;
        margin-bottom: 0;
      }

      .${cssRadioGroup}, ${StyledAvailable}, ${ProductTitle} {
        margin: 0;
      }

      ${ProductTitle} {
        margin-right: 44px;
        margin-bottom: 6px;
      }

      ${StyledEntityImage} {
        width: ${ProductImageSize.list};
        height: ${ProductImageSize.list};
      }

      ${ProductDetailInfo} {
        display: none !important;
      }

      .${cssButtonAddFavorite} {
        left: auto;
        right: 15px;
        top: 15px;
      }

      ${ControlsButtonsContainer} {
        grid-area: toCart;
        align-items: flex-start;
        ${ButtonBase} {
          width: 100%;
        }
      }

      ${StyledDeliveryMessage} {
        grid-column: 1 / -1;
        grid-row: end;
      }

      ${StyledListInfo} {
        ${StyledListItem} {
          justify-content: flex-start;
        }
      }

      ${IconPoint} {
        margin-bottom: 0;
      }
    }
  }

  &[data-view=${VIEW_PRODUCTS_SAMPLE_MINI}] {
    ${StyledCounter} {
      grid-column: 1/3;
      grid-row: 3/3;
    }
  }

  &[data-view=${VIEW_PRODUCTS_ORDER}], &[data-view=${VIEW_PRODUCTS_SAMPLE_ORDER}] {
    ${ProductContainer} {
      grid-template-areas:
        "image image title title  title   title  title   title   title   toCart toCart toCart toCart     toCart    toCart"
        "image image tabs  tabs   price   price  counter counter counter . . . . . .";

      ${StyledTotalPriceWrapper} {
        grid-area: toCart;
        justify-content: flex-end;
      }

      ${StyledAddToCartControl} {
        display: flex;
        justify-content: flex-end;
        ${ButtonBase} {
          width: 100%;
          display: inline-flex;
          max-width: 260px;
        }
      }
    }
  }

  &[data-is-kit="true"] {
    background: ${colors.grayLight};

    ${StyledLoaderOverlay} {
      background: ${colors.grayLight};
    }

    ${StyledCounter} {
      background: transparent;
    }

    ${ProductContainer} {
      > [data-view=${VIEW_PRODUCTS_CHILD}] {
        width: auto;
        margin: 16px -26px 0 0;
        border-top-right-radius: 0;
        border-bottom-right-radius: 0;
        background: transparent;

        ${StyledLoaderOverlay} {
          background: ${colors.white};
        }

        @media (max-width: ${breakpoints.md}) {
          margin-left: -15px;
          margin-right: -15px;
          border-left-color: transparent;
          border-right-color: transparent;
          border-bottom-color: transparent;
          border-radius: 0;

          ${StyledCounter} {
            grid-row: 4;
          }

          ${StyledUnitPriceWrapper} {
            grid-row: 4;
          }

          ${StyledDeliveryMessage} {
            grid-row: 5;
          }

          ${StyledListInfo} {
            grid-row: 6;
          }
        }
      }
    }
  }

  &[data-is-available="false"] {
    color: ${colors.grayDark};
  }

  ${PopoverContainer}.${cssTooltipDate} {
    position: absolute;
    top: 2px;
    left: 2px;
    z-index: 4;

    ${StyledPopoverDisclosure} {
      width: 30px;
      height: 30px;

      .${cssIcon} {
        ${getSizeSVG("18px")};
      }
    }

    [role="dialog"] {
      min-width: 320px;
    }
  }

  @media (max-width: ${breakpoints.xl}) {
    &[data-view=${VIEW_PRODUCTS_CHECKOUT}],
    &[data-view=${VIEW_PRODUCTS_SAMPLE}],
    &[data-view=${VIEW_PRODUCTS_CHILD}] {
      ${ProductContainer} {
        ${StyledDeliveryMessage} {
          grid-column: 8 / 16;
        }
        ${ControlsButtonsContainer} {
          grid-column: 16 / end;
        }
        ${ProductTitle} {
          grid-column: 2/14;
        }
        ${StyledTotalPriceWrapper} {
          grid-column: 14 / end;
        }
      }
    }

    &[data-view=${VIEW_PRODUCTS_CHILD}] {
      ${ProductContainer} {
        ${ControlsButtonsContainer} {
          grid-column: 11 / end;
        }
      }
    }
  }

  @media (max-width: ${breakpoints.md}) {
    &[data-view=${VIEW_PRODUCTS_CHECKOUT}],
    &[data-view=${VIEW_PRODUCTS_SAMPLE}],
    &[data-view=${VIEW_PRODUCTS_CHILD}] {
      ${ProductContainer} {
        grid-auto-flow: column;
        grid-template-columns: repeat(6, 1fr);
        grid-template-areas: initial;
        align-items: flex-start;
        grid-row-gap: 10px;
        padding: 24px 16px;

        > * {
          grid-column: 1 / end;
        }

        ${StyledImageTile} {
          grid-row: 1;
          grid-column: 1;
        }

        ${ProductTitle} {
          grid-column: 1/4;
          grid-row: 2;
          margin-right: 0;
        }

        .${cssRadioGroup} {
          grid-column: 1 / -1;
          grid-row: 3;

          ${RadioLabel} {
            display: flex;
            flex: 1;
            text-align: center;
            justify-content: center;
          }
        }

        ${StyledUnitPriceWrapper} {
          grid-row: 4;
          grid-column: 1/3;
        }

        ${StyledCounter} {
          grid-row: 4;
          grid-column: 3 / end;
        }

        ${StyledDeliveryMessage} {
          grid-row: 5;
          grid-column: 1 / -1;
          text-align: left;
        }

        ${StyledTotalPriceWrapper} {
          grid-row: 2;
          grid-column: 4 / end;
        }

        ${ControlsButtonsContainer} {
          grid-row: 1;
          grid-column: 5 / end;
        }

        ${IconPoint} {
          grid-row: 3;
        }

        .${cssButtonCompoundToggle} {
          grid-column: 1/4;
        }
      }
    }

    &[data-view=${VIEW_PRODUCTS_CHILD}] {
      margin-right: 0;
      margin-left: 0;
      background: transparent;
      ${ProductContainer} {
        padding: 24px;

        ${ControlsButtonsContainer} {
          grid-row: 1;
          grid-column: 3 / end;
          grid-template-areas: initial;
        }

        ${StyledEntityImage} {
          width: ${ProductImageSize.small};
          height: ${ProductImageSize.small};
        }

        ${StyledDeliveryMessage} {
          grid-row: 4;
        }

        ${StyledUnitPriceWrapper} {
          grid-row: 3;
        }

        ${StyledCounter} {
          grid-row: 3;
        }
      }
    }

    &[data-view=${VIEW_PRODUCTS_SAMPLE}] {
      ${ProductContainer} {
        ${StyledCounter} {
          grid-column: 1/3;
          grid-row: 3;
        }
        ${ControlsButtonsContainer} {
          grid-row: 1;
          grid-column: 3 / end;
          grid-template-areas: initial;
        }
        ${StyledDeliveryMessage} {
          grid-row: 4;
        }
      }
    }

    &[data-view=${VIEW_PRODUCTS_LIST}] {
      ${ProductContainer} {
        grid-template-areas:
          "image image badges badges badges badges badges . . . . . . . ."
          "image image title title  title   title  title   title   title   . . .      available  available available"
          "image image tabs  tabs tabs  price   price  counter counter counter counter toCart toCart     toCart    toCart";
      }
    }
  }

  @media (max-width: ${breakpoints.sm}) {
    border-right: 0;
    border-left: 0;

    &:not([data-view=${VIEW_PRODUCTS_SLIDE}]):not([data-view=${VIEW_PRODUCTS_ORDER}]):not([data-view=${VIEW_PRODUCTS_SAMPLE_ORDER}]) {
      &[data-isshowdetail="true"],
      &[data-isshowdetail="true"]:hover {
        background: transparent;
        ${ProductContainer} {
          background: transparent !important;
        }
      }

      ${ProductContainer} {
        padding: 16px 15px 20px 15px;
        grid-template-columns: 1fr 1fr 1fr 1fr;
        grid-template-areas:
          "badges badges badges badges"
          "title  title   title   title"
          "tabs   tabs    tabs    available"
          "price  counter counter image"
          "toCart toCart  toCart  image";
        align-items: center;
        gap: 0 5px;

        ${StyledUnitPriceWrapper} {
          grid-column: initial;
          align-items: flex-start;
        }

        .${cssRadioGroup}, ${StyledAvailable} {
          margin: 0;
        }

        ${ProductTitle} {
          margin-bottom: 10px;
        }

        ${StyledEntityImage} {
          width: ${ProductImageSize.list};
          height: ${ProductImageSize.list};
        }

        ${ProductDetailInfo} {
          display: none !important;
        }

        .${cssButtonAddFavorite} {
          left: auto;
          right: 15px;
          top: 15px;
        }

        ${StyledBadges} {
          position: relative;
          top: auto;
          left: auto;
        }
      }

      &[data-available-status="not"] {
        ${ProductContainer} {
          ${StyledUnitPriceWrapper} {
            grid-column: initial;
            align-items: flex-start;
          }
        }
      }
    }

    &[data-view=${VIEW_PRODUCTS_SLIDE}] {
      max-width: initial;
    }

    &[data-view=${VIEW_PRODUCTS_GRID}],
    &[data-view=${VIEW_PRODUCTS_LIST}],
    &[data-view=${VIEW_PRODUCTS_ORDER}],
    &[data-view=${VIEW_PRODUCTS_SAMPLE_ORDER}] {
      ${ProductContainer} {
        ${ProductTitle} {
          margin-right: 44px;
        }
        .${cssButtonAddFavorite} {
          left: auto;
          right: 15px;
          top: 15px;
        }

        ${StyledListInfo} {
          + ${StyledAvailable} {
            grid-row: available;
          }
        }
      }
    }

    &[data-view=${VIEW_PRODUCTS_GRID}] {
      margin-left: -15px;
      margin-right: -10px;

      ${ProductContainer} {
        ${StyledListInfo} {
          + ${StyledAvailable} {
            grid-row: available;
          }
        }

        ${StyledBadges} {
          margin-right: 44px;
        }
      }
    }

    &[data-view=${VIEW_PRODUCTS_LIST}] {
      margin-left: -15px;
      margin-right: -10px;
      ${ProductContainer} {
        ${StyledListInfo} {
          grid-column-start: 1;
        }
      }
    }

    &[data-view=${VIEW_PRODUCTS_ORDER}],
    &[data-view=${VIEW_PRODUCTS_SAMPLE_ORDER}] {
      ${ProductContainer} {
        padding: 16px 15px 20px 15px;
        grid-template-columns: 1fr 1fr 1fr 1fr;
        grid-template-areas:
          "title  title   title   image"
          "tabs   tabs    tabs    image"
          "price price  price image"
          "toCart toCart  toCart  image";

        ${StyledListInfo} {
          grid-column: 1/-1;
        }
        ${StyledAddToCartControl} {
          justify-content: flex-start;
          ${ButtonBase} {
            ${getTypographyBase("p12")};
            width: 100%;
            font-weight: 600;
          }
        }
        ${StyledTotalPriceWrapper} {
          justify-content: flex-start;
        }

        ${ProductTitle} {
          margin-right: 0;
        }
      }
    }
  }

  @media (max-width: ${breakpoints.xs}) {
    &[data-view=${VIEW_PRODUCTS_CHECKOUT}],
    &[data-view=${VIEW_PRODUCTS_SAMPLE}],
    &[data-view=${VIEW_PRODUCTS_CHILD}] {
      ${ProductContainer} {
        .${cssButtonCompoundToggle} {
          grid-column: 1/6;
        }
      }
    }
  }
`

export type LayoutProductPropsType = {
  uuid?: string
  isShowDetailInfo?: boolean
  isFavorite?: boolean
  viewProductsVariant?: ViewProductsType
  availableStatus?: AvailableStatusType
  isKit?: boolean
  isRemoved?: boolean
  isAnimate?: boolean
  isAvailable?: boolean
  sortWeight?: number
  categories?: string
}

export const StyledProduct = forwardRef<
  HTMLDivElement,
  LayoutProductPropsType & BaseHTMLAttributes<HTMLDivElement>
>(
  (
    {
      uuid,
      isShowDetailInfo,
      isFavorite,
      viewProductsVariant = VIEW_PRODUCTS_GRID,
      availableStatus,
      isKit,
      isRemoved,
      children,
      isAnimate,
      className,
      sortWeight,
      isAvailable,
      categories,
      ...props
    },
    ref,
  ) => {
    return (
      <>
        <StyledProductTag
          {...props}
          data-id={uuid}
          data-isshowdetail={isShowDetailInfo}
          data-isfavorite={isFavorite}
          data-view={viewProductsVariant}
          data-available-status={availableStatus}
          data-is-kit={isKit}
          data-is-removed={isRemoved}
          data-sort-weight={sortWeight}
          data-is-available={isAvailable}
          data-categories={categories}
          className={cx(isAnimate && cssProductAnimate, className)}
          ref={ref}
        >
          {children}
        </StyledProductTag>
      </>
    )
  },
)

StyledProduct.displayName = "StyledProduct"
