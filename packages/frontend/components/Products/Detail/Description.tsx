import { FC, useEffect, useMemo, useRef, useState } from "react"
import {
  cssDescHidden,
  DescProduct,
  DescProductContainer,
  DescProductInner,
} from "./StyledDetail"
import { cx } from "@linaria/core"
import { Typography } from "../../Typography/Typography"
import Button from "../../Button/Button"
import { cssButtonClean } from "../../Button/StyledButton"
import { cssButtonIsActive } from "../../Paginator/StyledPaginator"
import { DetailProductType } from "../../../types/types"

type DescriptionPropsType = Pick<
  DetailProductType,
  "description" | "variationParentDescription"
>

const DESCRIPTION_LENGTH_LONG = 255

export const DescriptionProduct: FC<DescriptionPropsType> = ({
  description,
  variationParentDescription,
}) => {
  const descriptionLength: number = useMemo(
    () =>
      (+(description || "").length || 0) +
      (+(variationParentDescription || "").length || 0),
    [description, variationParentDescription],
  )
  const [descIsHidden, setDescIsHidden] = useState(true)
  const [descIsLong, setDescIsLong] = useState(
    descriptionLength > DESCRIPTION_LENGTH_LONG,
  )
  const descTextRef = useRef<HTMLHeadingElement | null>(null)

  useEffect(() => {
    setDescIsLong(descriptionLength > DESCRIPTION_LENGTH_LONG)
  }, [descriptionLength])

  return (
    <>
      {descriptionLength > 0 && (
        <>
          <DescProductContainer
            className={cx(descIsLong && descIsHidden && cssDescHidden)}
          >
            <DescProduct
              style={{
                height: descIsLong
                  ? descIsHidden
                    ? "150px"
                    : `${descTextRef.current?.offsetHeight || 0}px`
                  : undefined,
              }}
            >
              <DescProductInner ref={descTextRef}>
                {(description || "").length > 0 && (
                  <Typography variant={"p14"}>{description}</Typography>
                )}
                {(variationParentDescription || "").length > 0 && (
                  <Typography variant={"p14"}>
                    {variationParentDescription}
                  </Typography>
                )}
              </DescProductInner>
            </DescProduct>
            {descIsLong && (
              <>
                <Button
                  variant={"link"}
                  icon={"AngleBottom"}
                  size={"smallM"}
                  onClick={() => {
                    if (descIsHidden) {
                      setDescIsHidden(false)
                    } else {
                      setDescIsHidden(true)
                    }
                  }}
                  className={cx(
                    cssButtonClean,
                    !descIsHidden && cssButtonIsActive,
                  )}
                >
                  {descIsHidden ? "Подробнее" : "Свернуть"}
                </Button>
              </>
            )}
          </DescProductContainer>
        </>
      )}
    </>
  )
}
