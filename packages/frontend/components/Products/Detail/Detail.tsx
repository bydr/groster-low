import { FC, useEffect, useMemo } from "react"
import {
  Body,
  Card,
  cssZoomIn,
  Description,
  Footer,
  Gallery,
  Header,
  Info,
  Media,
  StyledDetail,
  Title,
} from "./StyledDetail"
import { Typography } from "../../Typography/Typography"
import { ActionBar } from "../../ActionBar/ActionBar"
import { breakpoints, colors } from "../../../styles/utils/vars"
import Available from "../../Available/Available"
import { Icon } from "../../Icon"
import { DetailProductType, ProductType } from "../../../types/types"
import { useProduct } from "../../../hooks/product/product"
import { useAppDispatch, useAppSelector } from "../../../hooks/redux"
import { catalogSlice } from "../../../store/reducers/catalogSlice"
import { UnitControl } from "../parts/UnitControl/UnitControl"
import { AddToCartControl } from "../parts/AddToCartControl/AddToCartControl"
import Price from "../parts/Price/Price"
import { Counter } from "../parts/Counter/Counter"
import { ButtonToggleFavorite } from "../parts/ButtonToggleFavorite/ButtonToggleFavorite"
import { Row } from "../../../styles/utils/StyledGrid"
import Button from "../../Button/Button"
import { ActionBarContainer } from "../../ActionBar/StyledActionBar"
import { ButtonToggleSample } from "../parts/ButtonToggleSample/ButtonToggleSample"
import { ControlsContainerDetail, cssTooltipDate } from "../StyledProduct"
import { useSample } from "../../../hooks/sample"
import { Popover } from "../../Popover/Popover"
import { ListInfo } from "../parts/ListInfo/ListInfo"
import { RuleSortProduct, shippingDateToString } from "../../../utils/helpers"
import { EntityImage } from "../../EntityImage/EntityImage"
import { ProductDetail } from "../../../../contracts/contracts"
import { cx } from "@linaria/core"
import { productsAPI } from "../../../api/productsAPI"
import { FindCheaperForm } from "../../Forms/FindCheaper"
import { NotFindNeedForm } from "../../Forms/NotFindNeed"
import { GallerySlider } from "../parts/GallerySlider/GallerySlider"
import { ButtonFastOrder } from "../parts/ButtonFastOrder/ButtonFastOrder"
import { Recall } from "../../Forms/Recall"
import { Badges } from "../Badges"
import dynamic, { DynamicOptions } from "next/dynamic"
import { ModalDefaultPropsType } from "../../Modals/Modal"
import { getBreakpointVal } from "../../../styles/utils/Utils"
import { PreviewSlider } from "../parts/PreviewSlider"
import { cssButtonClean, cssButtonPulse } from "components/Button/StyledButton"
import { AfterAddedToCartKit } from "../AfterAddedToCartKit"
import { StoresAvailablePropsType } from "./StoresAvailable"
import { PropertiesPropsType } from "./Properties"
import { KitPart } from "../parts/KitPart"
import Variations from "../parts/Variations"
import { DescriptionProduct } from "./Description"
import { SkeletonDetail } from "../Skeleton/SkeletonDetail"
import { ProductsSliderPropsType } from "../Slider/ProductsSlider"

const DynamicModal = dynamic((() =>
  import("../../Modals/Modal").then(
    (mod) => mod.Modal,
  )) as DynamicOptions<ModalDefaultPropsType>)

const StoresAvailable = dynamic((() =>
  import("./StoresAvailable").then(
    (mod) => mod.StoresAvailable,
  )) as DynamicOptions<StoresAvailablePropsType>)

const Properties = dynamic((() =>
  import("./Properties").then(
    (mod) => mod.Properties,
  )) as DynamicOptions<PropertiesPropsType>)

const ProductsSlider = dynamic((() =>
  import("../Slider/ProductsSlider").then(
    (mod) => mod.ProductsSlider,
  )) as DynamicOptions<ProductsSliderPropsType>)

export const Detail: FC<{
  product: ProductType | DetailProductType
  companions?: ProductDetail[]
  analogs?: ProductDetail[]
  rulesAnalogs?: RuleSortProduct[]
}> = ({
  product: productOut,
  companions = [],
  analogs = [],
  rulesAnalogs = [],
}) => {
  const {
    name,
    categories: productCategories,
    units,
    unitMeasure,
    updateCurrentUnit,
    totalQty,
    currentCount,
    isFetching,
    inCart,
    addToCart,
    currency,
    priceCalculate,
    priceUnit,
    currentUnit,
    updateCurrentCount,
    isRemoved,
    isFetchingFavorites,
    isFavorites,
    toggleFavorite,
    uuid,
    isCopiedPath,
    copyPath,
    share,
    storesQty,
    stores,
    properties,
    description,
    kit,
    storesAvailable,
    images,
    availableStatus,
    isBestseller,
    isKit,
    kitParents,
    counter,
    code,
    copyCode,
    isCopiedCode,
    isCountError: isCountProductError,
    setIsCountError,
    isInit: isInitProduct,
    isAdded,
    isAvailable,
    shippingDate,
    isExpressShipping,
    isDateToOnlyCompany,
    isNew,
    variation,
    isVariative,
    setIsAdded,
    cleanup,
    isAllowSample,
  } = useProduct({
    product: productOut,
    options: {
      isSaveOnRemove: false,
      withLocation: true,
      isDetail: true,
    },
  })
  const {
    isFetching: isFetchingSample,
    toggle,
    isCountError: isCountSampleError,
    isInit: isInitSample,
    qtyInCart: qtyInCartSample,
    isDisabled: isDisabledSample,
  } = useSample({
    sample: {
      name: name || "",
      uuid: uuid || undefined,
      total_qty: productOut.total_qty,
      price: priceUnit,
      unit: unitMeasure || undefined,
    },
  })

  useEffect(() => {
    console.log("Detail productOut ", productOut)
  }, [productOut])

  const dispatch = useAppDispatch()
  const categories = useAppSelector((state) => state.catalog.categories)
  const { setCurrentCategory } = catalogSlice.actions

  const { data: kitProducts } = productsAPI.useProductsFetch(
    kit.length > 0 ? kit.join(",") : null,
  )

  const isInitEntity = useMemo(() => {
    return isInitProduct && isInitSample
  }, [isInitProduct, isInitSample])

  useEffect(() => {
    if (productCategories.length > 0 && !!categories?.fetched) {
      const foundCategory = categories.fetched[productCategories[0]]
      if (!!foundCategory) {
        dispatch(setCurrentCategory(foundCategory))
      }
    }
  }, [categories?.fetched, dispatch, productCategories, setCurrentCategory])

  useEffect(() => {
    return () => {
      cleanup()
    }
  }, [cleanup])

  return (
    <>
      <StyledDetail>
        {!isInitEntity ? (
          <SkeletonDetail />
        ) : (
          <Card data-id={uuid}>
            <Header>
              <Badges isBestseller={isBestseller} isKit={isKit} isNew={isNew} />
              <Title>
                <Typography variant={"h1"}>{name}</Typography>
                <Available status={availableStatus} isHideDeliveryIndicator />
              </Title>

              <KitPart kitParents={kitParents} />

              <ActionBarContainer>
                {isExpressShipping && (
                  <ActionBar
                    icon={"Bolt"}
                    description={"Возможна быстрая доставка"}
                    variant={"red"}
                  />
                )}

                {shippingDate !== null && (
                  <ActionBar
                    icon={"Car"}
                    description={
                      <>
                        {shippingDateToString(shippingDate)}
                        {isDateToOnlyCompany && (
                          <Popover
                            placement={"bottom-start"}
                            withHover
                            isStylingIconDisclosure={false}
                            offset={[0, 0]}
                            disclosure={
                              <Button
                                variant={"box"}
                                icon={"Warning"}
                                className={cx(cssButtonClean, cssButtonPulse)}
                              />
                            }
                            className={cssTooltipDate}
                          >
                            <Typography variant={"p13"}>
                              Указан срок доставки до транспортной компании
                            </Typography>
                          </Popover>
                        )}
                      </>
                    }
                  />
                )}
              </ActionBarContainer>
            </Header>

            <Body>
              <Info>
                <ListInfo storesQty={storesQty} stores={storesAvailable} />

                <DescriptionProduct
                  description={description || undefined}
                  variationParentDescription={variation?.parent?.description}
                />

                {isVariative && (
                  <Variations uuid={uuid} variation={variation} />
                )}

                {isAvailable && (
                  <>
                    <UnitControl
                      units={units}
                      setCurrentUnit={updateCurrentUnit}
                      totalQty={totalQty}
                      unitMeasure={unitMeasure}
                      unitValueActive={currentUnit || undefined}
                      isInitProduct={isInitEntity}
                    />

                    <Row
                      style={{
                        justifyContent: "space-between",
                      }}
                    >
                      <Price
                        price={priceUnit}
                        currency={currency}
                        unitMeasure={unitMeasure}
                      />

                      <Counter
                        counter={counter}
                        currentCount={currentCount}
                        currentUnit={currentUnit}
                        unitMeasure={unitMeasure}
                        maxCount={totalQty}
                        isFetching={isFetching}
                        productInCart={inCart}
                        updateCountHandler={updateCurrentCount}
                        productIsRemove={isRemoved}
                        isCountError={isCountProductError || isCountSampleError}
                        setIsCountError={setIsCountError}
                        isInitProduct={isInitEntity}
                      />
                    </Row>
                  </>
                )}

                {!isAvailable && (
                  <>
                    <Typography
                      variant={"default"}
                      color={colors.grayDark}
                      style={{
                        marginBottom: "5px",
                      }}
                    >
                      Товара нет в наличии
                    </Typography>
                    <Typography
                      variant={"default"}
                      style={{
                        marginBottom: "5px",
                      }}
                    >
                      Последняя цена{" "}
                      <Price
                        price={priceUnit}
                        currency={currency}
                        unitMeasure={unitMeasure}
                        variant={"span"}
                      />
                    </Typography>
                  </>
                )}

                <AddToCartControl
                  uuid={uuid}
                  isAvailable={isAvailable}
                  inCart={inCart && !isRemoved}
                  isFetching={isFetching}
                  buttonAreaContent={
                    <>
                      <Price price={priceCalculate} currency={currency} />
                    </>
                  }
                  addToCart={addToCart}
                  variant={"detail"}
                  disabled={!totalQty}
                >
                  <ButtonToggleFavorite
                    isFavorites={isFavorites}
                    toggleFavorite={toggleFavorite}
                    isFetching={isFetchingFavorites}
                    variant={"translucent"}
                    size={"large"}
                  />
                </AddToCartControl>

                {isKit && (
                  <>
                    <DynamicModal
                      closeMode={"destroy"}
                      variant={"rounded-70"}
                      isShowModal={isAdded}
                      title={"Добавлен в корзину"}
                    >
                      <AfterAddedToCartKit
                        product={productOut}
                        companions={companions}
                        kitProducts={kitProducts || []}
                        analogs={analogs}
                        onClose={() => {
                          setIsAdded(false)
                        }}
                      />
                    </DynamicModal>
                  </>
                )}

                {kitParents.length > 0 && (
                  <>
                    <DynamicModal
                      closeMode={"destroy"}
                      variant={"rounded-70"}
                      isShowModal={isAdded}
                      title={"Добавлен в корзину"}
                    >
                      <AfterAddedToCartKit
                        product={productOut}
                        companions={companions}
                        kitParents={kitParents || []}
                        analogs={analogs}
                        onClose={() => {
                          setIsAdded(false)
                        }}
                      />
                    </DynamicModal>
                  </>
                )}

                {isAvailable && (
                  <>
                    <ButtonFastOrder
                      product={{
                        inCart,
                        isRemoved,
                        addToCart,
                      }}
                      size={"large"}
                    />
                  </>
                )}

                {code !== undefined && (
                  <>
                    <Popover
                      disclosure={
                        <Button onClick={copyCode} isClean>
                          Код товара: {code}
                        </Button>
                      }
                      isShow={isCopiedCode}
                      isStylingIconDisclosure={false}
                      size={"small"}
                    >
                      <Typography variant={"p12"} color={colors.green}>
                        Код скопирован!
                      </Typography>
                    </Popover>
                  </>
                )}
              </Info>
            </Body>

            <Media>
              <Gallery>
                {images.length > 0 ? (
                  <PreviewSlider alt={name || ""} images={images} />
                ) : (
                  <>
                    <EntityImage
                      imagePath={images[0]}
                      imageAlt={name || ""}
                      width={271}
                      height={271}
                      priority
                      withPreloader={true}
                    />
                  </>
                )}

                {images.length > 0 && (
                  <>
                    <DynamicModal
                      closeMode={"destroy"}
                      variant={"full"}
                      disclosure={
                        <Button
                          icon={"ZoomIn"}
                          variant={"box"}
                          className={cssZoomIn}
                        />
                      }
                    >
                      <GallerySlider images={images} />
                    </DynamicModal>
                  </>
                )}
              </Gallery>

              <DynamicModal
                closeMode={"destroy"}
                disclosure={
                  <Button variant={"link"} icon={"Ratio"}>
                    Заказать видео-презентацию
                  </Button>
                }
                title={"Заказать видео-презентацю"}
              >
                <Recall />
              </DynamicModal>
            </Media>

            <Footer>
              <Row>
                <ControlsContainerDetail>
                  {isAvailable && (
                    <>
                      <ButtonToggleSample
                        isFetchingSample={isFetchingSample}
                        toggleSampleToCart={toggle}
                        sample={qtyInCartSample || 0}
                        isDisabled={isDisabledSample}
                        size={"smallM"}
                        isAllow={isAllowSample}
                      />

                      <DynamicModal
                        closeMode={"destroy"}
                        variant={"rounded-0"}
                        title={"Нашли дешевле?"}
                        disclosure={
                          <Button
                            variant={"link"}
                            icon={"Flag"}
                            size={"smallM"}
                          >
                            Нашли дешевле?
                          </Button>
                        }
                      >
                        <FindCheaperForm />
                      </DynamicModal>
                    </>
                  )}

                  <DynamicModal
                    closeMode={"destroy"}
                    variant={"rounded-0"}
                    title={"Не нашли нужного?"}
                    disclosure={
                      <Button
                        variant={"link"}
                        icon={"Extension"}
                        size={"smallM"}
                      >
                        Не нашли нужного?
                      </Button>
                    }
                  >
                    <NotFindNeedForm />
                  </DynamicModal>

                  <Popover
                    disclosure={
                      <Button
                        variant={"link"}
                        icon={"Link"}
                        onClick={copyPath}
                        size={"smallM"}
                        hideTextOnBreakpoint={breakpoints.sm}
                      >
                        Ссылка
                      </Button>
                    }
                    isShow={isCopiedPath}
                    isStylingIconDisclosure={false}
                    size={"small"}
                  >
                    <Typography variant={"p12"} color={colors.green}>
                      Ссылка скопирована!
                    </Typography>
                  </Popover>

                  <Button
                    size={"smallM"}
                    variant={"link"}
                    icon={"Share"}
                    onClick={share}
                    hideTextOnBreakpoint={breakpoints.sm}
                  >
                    Поделиться
                  </Button>
                </ControlsContainerDetail>
              </Row>
            </Footer>
          </Card>
        )}

        {!!kitProducts && kitProducts.length > 0 && (
          <>
            <ProductsSlider
              responsiveExtends={{
                [getBreakpointVal(breakpoints.lg)]: {
                  slidesPerView: 3,
                },
                [getBreakpointVal(breakpoints.sm)]: {
                  slidesPerView: 2,
                },
                0: {
                  slidesPerView: 1,
                },
              }}
              products={kitProducts}
              disabledOnMobile
              title={<Typography variant={"h3"}>Состав комплекта</Typography>}
            />
          </>
        )}

        {analogs.length > 0 && (
          <>
            <ProductsSlider
              responsiveExtends={{
                [getBreakpointVal(breakpoints.lg)]: {
                  slidesPerView: 3,
                },
                [getBreakpointVal(breakpoints.sm)]: {
                  slidesPerView: 2,
                },
                0: {
                  slidesPerView: 1,
                },
              }}
              products={analogs}
              ruleSort={rulesAnalogs}
              title={<Typography variant={"h3"}>Похожие товары</Typography>}
            />
          </>
        )}

        <Properties properties={properties} params={productOut.params || []} />

        <StoresAvailable stores={stores} />

        <Description>
          <Icon NameIcon={"Car"} fill={colors.brand.yellow} />
          <Typography variant={"span"}>
            <b>Доставка курьером, 1-3 дня.</b> Осуществляется через транспортные
            компании после полной оплаты товара. Мы работаем с ПЭК,
            Байкал-Сервис, EASY WAY, КИТ. Стоимость доставки зависит от
            габаритов груза и расстояния транспортировки. Рассчитывается
            индивидуально.
          </Typography>
        </Description>
        <Description>
          <Icon NameIcon={"Verified"} fill={colors.brand.yellow} />
          <Typography variant={"span"}>
            <b>Гарантия легкого возврата.</b> До 30 дней на возврат,
            полная гарантия
          </Typography>
        </Description>
      </StyledDetail>
    </>
  )
}
