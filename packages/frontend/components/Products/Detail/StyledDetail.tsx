import { styled } from "@linaria/react"
import { StyledProductsSlider } from "../Slider/StyledProductsSlider"
import {
  getTypographyBase,
  Heading1,
  Heading3,
  Paragraph12,
} from "../../Typography/Typography"
import { css } from "@linaria/core"
import { breakpoints, colors } from "../../../styles/utils/vars"
import { cssIcon, getSizeSVG } from "../../Icon"
import { StyledPrice } from "../parts/Price/StyledPrice"
import { CounterInput, StyledCounter } from "../parts/Counter/StyledCounter"
import { cssRadioGroup, RadioLabel, StyledRadio } from "../../Radio/StyledRadio"
import { ControlsButtonsContainer, ProductImageSize } from "../StyledProduct"
import {
  ButtonBase,
  ButtonSliderArrow,
  cssButtonFastBuy,
} from "../../Button/StyledButton"
import { StyledListInfo } from "../parts/ListInfo/StyledListInfo"
import { StyledList, StyledListItem } from "../../List/StyledList"
import { Side } from "../StyledProductPage"
import { StyledAddToCartControl } from "../parts/AddToCartControl/StyledAddToCartControl"
import { StyledEntityImage } from "../../EntityImage/Styled"
import { StyledAvailable } from "../../Available/StyledAvailable"
import { cssButtonIsActive } from "../../Paginator/StyledPaginator"
import { StyledImageTile } from "../parts/ImageTile/StyledImageTile"

export const cssName = css``
export const StyledTable = styled.div`
  width: 100%;
  display: table;
`
export const TableTitle = styled(Heading3)`
  margin-bottom: 20px;
`
export const StyledTableContainer = styled.section`
  width: 100%;
  margin: 20px 0 26px 0;
  position: relative;
  display: flex;
  flex-direction: column;
  align-items: flex-start;
`

export const TableCell = styled.div`
  ${getTypographyBase("p12")};
  display: table-cell;
  padding: 10px 4px;
  vertical-align: middle;
  line-height: 100%;

  &:first-child {
    color: ${colors.grayDark};
  }
`

export const TableRow = styled.div`
  width: 100%;
  display: table-row;

  &:nth-child(2n + 1) {
    ${TableCell} {
      background: ${colors.grayLight};
    }
  }
`

export const StyledDetail = styled.section`
  width: 100%;
  position: relative;
  overflow: hidden;
  flex: 1;
  min-height: 40vh;

  ${StyledProductsSlider} {
    margin: 16px 0 0 0;
  }

  ${StyledTable} {
    ${TableCell} {
      &:first-child {
        width: 80%;
      }
    }
  }

  @media (max-width: ${breakpoints.sm}) {
    ${StyledTable} {
      ${TableRow} {
        ${TableCell} {
          &:first-child {
            width: auto;
          }
        }
      }
    }
  }
`
export const StyledDetailProduct = styled.section`
  width: 100%;
  display: flex;
  flex-direction: row-reverse;
  justify-content: flex-start;
  position: relative;
  align-items: flex-start;

  ${Side} {
    width: 35%;

    ~ ${StyledDetail} {
      grid-area: detail;
      padding-right: 56px;
      border-right: 1px solid ${colors.gray};
      margin-right: 56px;

      > ${StyledProductsSlider} {
        margin-right: -56px;
        width: auto;

        ${ButtonSliderArrow} {
          &:first-of-type {
            right: 106px;
          }
          &:last-of-type {
            right: 56px;
          }
        }
      }
    }
  }

  @media (max-width: ${breakpoints.xxl}) {
    ${Side} {
      width: 35%;

      ~ ${StyledDetail} {
        padding-right: 24px;
        margin-right: 24px;
        > ${StyledProductsSlider} {
          margin-right: -24px;
        }
      }
    }
  }

  @media (max-width: ${breakpoints.lg}) {
    display: flex;
    flex-direction: column-reverse;

    ${Side} {
      width: 100%;

      ~ ${StyledDetail} {
        padding-right: 0;
        border: none;
        margin-right: 0;

        > ${StyledProductsSlider} {
          margin-right: 0;
          ${ButtonSliderArrow} {
            &:first-of-type {
              right: 50px;
            }
            &:last-of-type {
              right: 0;
            }
          }
        }
      }
    }
  }
`
export const Title = styled.div`
  width: 100%;
  display: flex;
  align-items: flex-end;
  gap: 24px 10px;
  margin-bottom: 20px;

  ${Heading1} {
    margin: 0;
    flex: 1;
  }

  ${StyledAvailable} {
    width: auto;
  }

  @media (max-width: ${breakpoints.sm}) {
    flex-direction: column;
    align-items: flex-start;

    ${Heading1} {
      font-size: 20px;
    }
  }
`

export const Description = styled(Paragraph12)`
  display: flex;
  margin-bottom: 20px;

  > .${cssIcon} {
    margin-top: 0;
    margin-right: 10px;
  }
`
export const cssDescHidden = css``
export const DescProduct = styled.div`
  position: relative;
  width: 100%;
  transition: height 0.2s ease-in-out;
  overflow: hidden;
  will-change: scroll-position;

  & + ${ButtonBase} {
    color: ${colors.brand.purple};

    &.${cssButtonIsActive} {
      .${cssIcon} {
        transform: rotate(180deg);
      }
    }
  }
`
export const DescProductInner = styled.div`
  width: 100%;
`
export const DescProductContainer = styled.div`
  width: 100%;

  &.${cssDescHidden} {
    ${DescProduct} {
      &:before {
        content: "";
        background: ${colors.white};
        background: linear-gradient(
          to bottom,
          transparent 5%,
          ${colors.white} 65%
        );
        position: absolute;
        height: 50px;
        bottom: 0;
        left: 0;
        right: 0;
        width: 100%;
        padding: 0;
        margin: 0;
        z-index: 2;
      }

      & + ${ButtonBase} {
        margin-top: -18px;
        z-index: 3;
      }
    }
  }
`

export const Gallery = styled.div`
  position: relative;
  width: 100%;
`

export const cssZoomIn = css`
  &${ButtonBase} {
    position: absolute;
    left: 50%;
    top: 50%;
    padding: 0;
    z-index: 2;
    background: transparent;
    border: 0;
    margin-top: -34px;
    margin-left: -24px;

    .${cssIcon} {
      fill: ${colors.brand.yellow};
    }

    &:hover,
    &:active {
      transform: scale(1.15);
      background: ${colors.white};

      .${cssIcon} {
        fill: ${colors.brand.purple};
      }
    }
  }

  ${Gallery}:hover &, ${StyledImageTile}:hover & {
    background: ${colors.transparentWhite7};

    &:hover {
      background: ${colors.white};
    }
  }
`

export const Info = styled.div`
  display: flex;
  flex-direction: column;
  align-items: flex-start;

  > *,
  > ${StyledListInfo} {
    margin-bottom: 10px;
  }
`
export const Media = styled.div`
  position: relative;
  grid-area: media;
  display: flex;
  flex: 1;
  width: auto;
  overflow: hidden;
  flex-direction: column;

  > ${ButtonBase} {
    margin-left: auto;
    margin-right: auto;
  }
`
export const Body = styled.div`
  grid-area: body;
  width: 100%;
  display: flex;
  flex-direction: row;
  align-items: flex-start;

  ${Info} {
    flex: 1;
  }
  ${Media} {
    margin-left: 32px;
    width: 35%;
  }
`
export const Footer = styled.footer`
  margin: 0;
  grid-area: footer;

  ${ControlsButtonsContainer} {
    display: flex;
    align-items: flex-start;
    grid-row-gap: 10px;

    &:last-child {
      text-align: right;
    }

    &:first-child {
      text-align: left;
      justify-content: flex-start;
      flex: 1;
    }

    ${ButtonBase} {
      margin: 0;
      padding-top: 0;
      padding-bottom: 0;
    }
  }
`
export const Header = styled.header`
  width: 100%;
  margin-bottom: 0;
  grid-area: header;
`
export const Card = styled.article`
  width: 100%;
  display: grid;
  grid-template-columns: 7.5fr 4.5fr;
  grid-template-areas: "header header" "body media" "footer footer";
  gap: 0 32px;

  ${StyledPrice}, ${CounterInput} input {
    ${getTypographyBase("default")};
  }

  .${cssRadioGroup} {
    & ${RadioLabel} {
      ${getTypographyBase("p14")};
      text-align: center;
    }
  }

  ${StyledCounter} {
    background: ${colors.grayLight};
    min-height: 48px;
    padding: 0 12px;
  }

  .${cssButtonFastBuy} {
    margin-bottom: 10px;
  }

  ${StyledListInfo} {
    ${StyledList} {
      ${getTypographyBase("p14")};
      ${StyledListItem} {
        color: ${colors.black};
        justify-content: flex-start;
      }
    }
  }

  ${StyledRadio} {
    text-align: center;
  }

  ${StyledAddToCartControl} {
    width: 100%;
    margin: 0 0 10px 0;
  }

  ${StyledEntityImage} {
    max-height: ${ProductImageSize.detail};

    .${cssIcon} {
      ${getSizeSVG("100px")};
    }

    @media (max-width: ${breakpoints.sm}) {
      .${cssIcon} {
        ${getSizeSVG("60px")};
      }
    }
  }

  @media (max-width: ${breakpoints.md}) {
    ${StyledCounter} {
      ${CounterInput} {
        max-width: 110px;
      }
    }
  }

  @media (max-width: ${breakpoints.sm}) {
    grid-template-areas:
      "header media"
      "body body"
      "footer footer";
    gap: 24px 8px;
    grid-template-columns: 8fr 4fr;

    ${Media} {
      > ${ButtonBase} {
        display: none;
      }
    }
  }
`
