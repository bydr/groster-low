import { FC, useMemo } from "react"
import {
  cssName,
  StyledTable,
  StyledTableContainer,
  TableCell,
  TableRow,
  TableTitle,
} from "./StyledDetail"
import { PropertiesType } from "../../../types/types"
import { useQuery } from "react-query"
import { fetchFiltersParams } from "../../../api/catalogAPI"
import { useAppDispatch, useAppSelector } from "../../../hooks/redux"
import { catalogSlice } from "../../../store/reducers/catalogSlice"
import { EMPTY_DATA_PLACEHOLDER } from "../../../utils/constants"

export type PropertiesPropsType = {
  properties: PropertiesType
  params: string[]
}

export const Properties: FC<PropertiesPropsType> = ({ properties, params }) => {
  const filterParams = useAppSelector((state) => state.catalog.filter.params)
  const dispatch = useAppDispatch()
  const { setFilterParams } = catalogSlice.actions

  useQuery(["filterParams"], () => fetchFiltersParams(), {
    initialData: Object.keys(filterParams).map((k) => filterParams[k]),
    onSuccess: (response) => {
      dispatch(setFilterParams(response || []))
    },
  })

  const paramsProps: PropertiesType = useMemo(() => {
    let made: PropertiesType = []
    params.map((uuidParam) => {
      const filter = filterParams[uuidParam || ""]
      const filterParent =
        filterParams[filterParams[uuidParam || ""]?.parentUuid || ""]
      if (!!filter) {
        made = [
          ...made,
          {
            uuidValue: filter.uuid,
            uuidParent: filterParent.uuid,
            name: !!filterParent ? filterParent.name : EMPTY_DATA_PLACEHOLDER,
            value: filter.name,
          },
        ]
      }
    })
    return made
  }, [filterParams, params])

  return (
    <>
      {[...properties, ...paramsProps].length > 0 && (
        <>
          <StyledTableContainer>
            <TableTitle>Характеристики</TableTitle>
            <StyledTable>
              {[...properties, ...paramsProps].map((prop, index) => (
                <TableRow key={index} data-id={prop.uuidParent}>
                  <TableCell className={cssName}>{prop.name}</TableCell>
                  <TableCell data-id={prop.uuidValue}>{prop.value}</TableCell>
                </TableRow>
              ))}
            </StyledTable>
          </StyledTableContainer>
        </>
      )}
    </>
  )
}
