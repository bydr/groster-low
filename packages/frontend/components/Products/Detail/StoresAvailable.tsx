import { FC } from "react"
import {
  StyledTable,
  StyledTableContainer,
  TableCell,
  TableRow,
  TableTitle,
} from "./StyledDetail"
import { Typography } from "../../Typography/Typography"
import { colors } from "../../../styles/utils/vars"
import Available from "../../Available/Available"
import { StoreProductType } from "../../../types/types"

export type StoresAvailablePropsType = {
  stores: StoreProductType[]
}

export const StoresAvailable: FC<StoresAvailablePropsType> = ({ stores }) => {
  return (
    <>
      {stores.length > 0 && (
        <>
          <StyledTableContainer>
            <TableTitle>Наличие и доставка</TableTitle>
            <StyledTable>
              {stores.map((store) => (
                <TableRow key={store.uuid || ""}>
                  <TableCell>
                    <Typography variant={"p12"} color={colors.black}>
                      {store.name || "Магазин"}
                    </Typography>
                  </TableCell>
                  <TableCell>
                    <Available
                      status={store.availableStatus}
                      isHideDeliveryIndicator
                    />
                  </TableCell>
                </TableRow>
              ))}
            </StyledTable>
          </StyledTableContainer>
        </>
      )}
    </>
  )
}
