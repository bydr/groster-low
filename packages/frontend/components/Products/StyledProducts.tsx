import { styled } from "@linaria/react"
import { breakpoints, colors } from "../../styles/utils/vars"
import { StyledProductTag } from "./StyledProduct"
import { Heading3, TypographyBase } from "../Typography/Typography"
import {
  VIEW_PRODUCTS_CHECKOUT,
  VIEW_PRODUCTS_CHILD,
  VIEW_PRODUCTS_GRID,
  VIEW_PRODUCTS_LIST,
} from "../../types/types"
import { StyledPaginator } from "../Paginator/StyledPaginator"
import { css } from "@linaria/core"
import { StyledBannerSection } from "../Banners/Banner/Styled"

export const cssHiddenCompoundKit = css``

export const cssWithBanners = css``

export const StyledProducts = styled.div`
  position: relative;
  width: 100%;
  display: grid;
  margin: 0 0 20px 0;
  border-radius: 12px;

  &[data-with-border="true"] {
    border: 1px solid ${colors.gray};

    ${StyledProductTag} {
      border: none;
      border-bottom: 1px solid ${colors.gray};

      &:last-of-type {
        border: none;
      }
    }
  }

  &[data-view=${VIEW_PRODUCTS_CHECKOUT}] {
    overflow: initial;
  }

  > ${Heading3} {
    margin: 24px 0 0 24px;
  }

  & + & {
    margin-top: 40px;
  }

  &[data-view=${VIEW_PRODUCTS_CHILD}] {
    transition: none;

    &.${cssHiddenCompoundKit} {
      position: absolute;
      visibility: hidden;
      z-index: -1;
      opacity: 0;
    }
  }

  @media (max-width: ${breakpoints.sm}) {
    > ${TypographyBase} {
      margin-left: 15px;
    }
  }
`

export const StyledCatalogProducts = styled.section`
  position: relative;
  padding-bottom: 20px;

  &[data-view-products=${VIEW_PRODUCTS_LIST}] {
    ${StyledProducts} {
      grid-template-columns: 9fr 3fr;
      grid-column-gap: 56px;

      ${StyledProductTag} {
        grid-column: 1/1;
        grid-row: initial;
        width: 100%;
      }

      ${StyledBannerSection} {
        grid-column: 2;
        grid-row: 1 / span 3;
        border: none;
        padding: 0;
      }
    }

    &:before {
      display: none;
    }

    @media (max-width: ${breakpoints.lg}) {
      ${StyledProducts} {
        grid-template-columns: 1fr;

        ${StyledBannerSection} {
          grid-column: 1/-1;
          grid-row: 2;
          padding: 24px 0;
        }
      }
    }
  }

  &, &[data-view-products=${VIEW_PRODUCTS_GRID}] {
    ${StyledProducts} {
      grid-template-columns: repeat(5, 1fr);
    }

    &:before {
      content: "";
      position: absolute;
      right: 0;
      width: 1px;
      background: ${colors.white};
      top: 0;
      height: 100%;
      z-index: 1;
    }

    ${StyledPaginator} {
      grid-column: 1/-1;
    }

    ${StyledBannerSection} {
      grid-column: 4/-1;
      grid-row: 2/3;
      padding: 24px;
      border-bottom: 1px solid ${colors.gray};
    }

    @media (max-width: ${breakpoints.xl}) {
      ${StyledProducts} {
        grid-template-columns: repeat(4, 1fr);

        ${StyledBannerSection} {
          grid-column: 3/-1;
        }
      }
    }

    @media (max-width: ${breakpoints.lg}) {
      ${StyledProducts} {
        grid-template-columns: repeat(2, 1fr);

        ${StyledBannerSection} {
          grid-column: 1/-1;
        }
      }

      &:before {
        display: none;
      }

      ${StyledProductTag} {
        &:nth-child(2n) {
          border-right-color: transparent;
        }
      }
    }
  }

  @media (max-width: ${breakpoints.sm}) {
    &,
    &[data-view-products] {
      //margin-right: -12px;
      //margin-left: -12px;

      ${StyledProducts} {
        grid-template-columns: 1fr;
      }
    }

    ${StyledProducts} {
      &[data-view=${VIEW_PRODUCTS_GRID}] {
        ${StyledBannerSection} {
          grid-row: 3;
          padding: 24px 0;
        }
      }
    }
  }
`

export const StyledTileProducts = styled.div`
  width: 100%;
  position: relative;

  ${StyledProducts} {
    &[data-view=${VIEW_PRODUCTS_GRID}] {
      grid-template-columns: repeat(5, 1fr);

      @media (max-width: ${breakpoints.xl}) {
        grid-template-columns: repeat(4, 1fr);
      }
      @media (max-width: ${breakpoints.lg}) {
        grid-template-columns: repeat(2, 1fr);
      }
    }

    @media (max-width: ${breakpoints.sm}) {
      &[data-view] {
        grid-template-columns: 1fr;
      }

      ${StyledProductTag} {
        width: auto;
        //margin-left: -13px;
        //margin-right: -13px;
      }
    }
  }
`
