import { FC } from "react"
import { Typography } from "../Typography/Typography"
import { Icon, IconNameType } from "../Icon"
import { StyledIconText } from "./StyledIconText"

export const IconText: FC<{ icon: IconNameType }> = ({ icon, children }) => {
  return (
    <>
      <StyledIconText>
        <Icon NameIcon={icon} />
        <Typography variant={"span"}>{children}</Typography>
      </StyledIconText>
    </>
  )
}
