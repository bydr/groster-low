import { styled } from "@linaria/react"
import { Paragraph14, Span } from "../Typography/Typography"
import { cssIcon } from "../Icon"
import { colors } from "../../styles/utils/vars"

export const StyledIconText = styled(Paragraph14)`
  display: inline-flex;

  .${cssIcon} {
    margin-right: 6px;
    color: ${colors.brand.purple};
  }
  ${Span} {
    margin-top: -4px;
  }
`
