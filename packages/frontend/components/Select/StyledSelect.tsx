import { styled } from "@linaria/react"
import {
  borderRadiusControl,
  breakpoints,
  colors,
  sizeSVG,
  transitionTimingFunction,
} from "../../styles/utils/vars"
import {
  getTypographyBase,
  Paragraph10,
  Paragraph12,
  Paragraph14,
  Span,
  TypographyBase,
} from "../Typography/Typography"
import { cssIcon, getSizeSVG } from "../Icon"
import { Placeholder } from "../Field/StyledField"
import { ButtonBase } from "../Button/StyledButton"
import { getCustomizeScroll } from "../../styles/utils/Utils"
import { StyledEntityImage } from "../EntityImage/Styled"

export const StyledButtonContainer = styled.div`
  cursor: default;
  padding: 10px 0;

  &:hover,
  &:active {
    background-color: transparent;
  }

  ${ButtonBase} {
    margin: 0;
  }
`
export const StyledOptionInner = styled(Span)`
  flex: 1;
`
export const StyledSelectInputDiv = styled.div`
  display: flex;
  align-items: center;
  flex-wrap: nowrap;
  position: relative;

  [role="combobox"] {
    flex: 1;
    color: transparent;
  }

  > .${cssIcon} {
    fill: ${colors.brand.purple};
    transition: all 0.2s ${transitionTimingFunction};
    position: absolute;
    right: 16px;
    top: 50%;
    margin-top: -9px;
    user-select: none;
    pointer-events: none;
  }

  & input {
    -webkit-appearance: none;
    appearance: none;
  }

  & input:focus ~ ${Placeholder}, &[data-is-empty="false"] ${Placeholder} {
    ${getTypographyBase("p10")};
    top: 10px;
    transform: none;
    line-height: 14px;
  }
`
export const SelectedIcon = styled.span`
  position: absolute;
  display: flex;
  align-items: center;
  justify-content: center;
  text-align: center;
  right: 30px;
`
export const StyledSelectedTitle = styled(Paragraph14)`
  position: absolute;
  left: 18px;
  bottom: 50%;
  transform: translateY(50%);
  line-height: 24px;
  right: 40px;
  white-space: nowrap;
  text-overflow: ellipsis;
  overflow: hidden;
  pointer-events: none;
  user-select: none;
  margin-bottom: 0;

  @media (max-width: ${breakpoints.xs}) {
    max-width: 160px;
  }
`

export const SelectedHintBody = styled.div`
  border-radius: 4px;
  padding: 6px 16px 6px 18px;
  background: ${colors.brand.purpleTransparent15};

  ${TypographyBase} {
    margin: 0;
    padding: 0;
  }
`
export const SelectedHint = styled.div`
  width: 100%;
  ${getTypographyBase("p12")};

  > ${TypographyBase} {
    margin: 4px 0 0 0;
  }
`

export const StyledSelectInputWrapper = styled.span`
  display: flex;
  width: 100%;

  > ${StyledSelectInputDiv} {
    flex: 1;
  }

  > ${ButtonBase} {
    margin: 0;
    border-radius: 0 4px 4px 0;
    border: 1px solid ${colors.gray};
    border-left: none;
    height: auto;
  }
`

export const StyledIconSelect = styled.div`
  position: absolute;
  left: 0;
  top: 0;
  bottom: 0;
  min-width: 26px;
  display: flex;
  align-items: center;
  justify-content: flex-end;

  .${cssIcon} {
    fill: ${colors.brand.yellow};
  }
`

export const StyledSelect = styled.div`
  background: transparent;
  position: relative;
  flex-direction: column;
  display: inline-flex;
  margin: 0;
  width: 100%;

  [role="combobox"] {
    ${getTypographyBase("p14")};
    appearance: none;
    line-height: 171%;
    background-color: transparent;
    height: auto;
    outline: none;
    padding: 24px 16px 6px 18px;
    border: 1px solid ${colors.gray};
    border-radius: ${borderRadiusControl};
    user-select: none;
    cursor: pointer;
    margin-bottom: 0;

    &::selection {
      background-color: transparent;
    }
  }

  [role="listbox"] {
    ${getTypographyBase("p14")};
    ${getCustomizeScroll()};
    width: 100%;
    left: 0 !important;
    right: 0 !important;
    transform: none !important;
    top: 100% !important;
    background: ${colors.white};
    border: 1px solid ${colors.brand.yellow};
    border-top-color: transparent;
    border-radius: 0 0 ${borderRadiusControl} ${borderRadiusControl};
    z-index: 10;
    max-height: 300px;
    overflow: hidden auto;
    margin-bottom: 0;
    -webkit-overflow-scrolling: touch;

    ${Paragraph12} {
      color: ${colors.grayDark};
      margin-bottom: 0;
    }

    &:after {
      content: "";
      position: absolute;
      height: 1px;
      top: 0;
      right: 16px;
      left: 18px;
      background: ${colors.gray};
    }
  }

  ${Placeholder} {
    ${getTypographyBase("p14")}
    color: ${colors.grayDark};
    position: absolute;
    left: 18px;
    top: 14px;

    ~ ${StyledSelectedTitle} {
      transform: none;
      bottom: 8px;
    }
  }

  [aria-expanded="true"]:not(${ButtonBase}) {
    border-color: ${colors.brand.yellow};
    border-radius: ${borderRadiusControl} ${borderRadiusControl} 0 0;
    border-bottom-color: transparent;
    cursor: pointer;

    ~ svg {
      transform: rotate(180deg);
    }
  }

  ${Paragraph10} {
    line-height: 140%;
  }

  [role="option"] {
    ${getTypographyBase("p14")};
    padding: 10px 16px 10px 18px;
    display: flex;
    width: 100%;
    align-items: center;
    justify-content: space-between;
    margin-bottom: 0;
    cursor: pointer;

    &:hover {
      background: ${colors.grayLight};
    }

    &:first-child {
      margin-top: 8px;
    }

    svg {
      fill: ${colors.black};
      margin-left: 4px;
    }

    ${StyledEntityImage} {
      svg {
        margin: 0;
      }
    }

    > svg {
      fill: ${colors.brand.purple};
    }

    &[aria-disabled="true"] {
      color: ${colors.grayDarkLight};
      cursor: default;
    }
  }

  &[data-variant="small"] {
    border: none;

    [role="combobox"] {
      padding: 4px 12px 4px 14px;
      background: ${colors.grayLight};
      border-radius: 50px;
      border: none;
      min-height: initial;

      &[aria-expanded="true"] {
        border-radius: 14px 14px 2px 2px;
      }
    }

    .${cssIcon} {
      ${getSizeSVG(sizeSVG.smaller)};
    }

    ${StyledSelectInputDiv} {
      > svg {
        bottom: 50%;
        margin-top: calc(-${sizeSVG.smaller} / 2);
      }
    }

    [role="listbox"] {
      border: 1px solid ${colors.gray};

      &:after {
        display: none;
        content: none;
      }
    }

    [role="option"] {
      padding-top: 5px;
      padding-bottom: 5px;

      &:first-child {
        margin-top: 0;
      }
    }

    ${StyledSelectedTitle} {
      bottom: 50%;
      transform: translateY(50%);
    }
  }

  ${TypographyBase} {
    margin-bottom: 0;
  }

  [data-iserror="true"] & {
    ${StyledSelectInputWrapper} {
      > ${ButtonBase} {
        border-color: ${colors.red};
      }
    }
  }
`
