import { styled } from "@linaria/react"
import { Panel } from "styles/utils/StyledPanel"
import { Section } from "../../styles/utils/Utils"
import { breakpoints, colors } from "../../styles/utils/vars"
import { Col } from "../Account/History/Orders/Order/Detail/Styled"
import { StyledPicture } from "../List/ListPicture/Styled"

export const StyledPlus = styled.div`
  width: 28px;
  height: 28px;
`
export const StyledPluses = styled.div`
  display: inline-flex;
  width: auto;
  align-items: center;
  justify-content: flex-start;
  gap: 28px;
`

export const StyledBrandBasisContent = styled.div`
  width: 100%;
  display: grid;
  grid-template-columns: 1fr 1px 1fr 1fr;
  gap: 20px 56px;
  grid-template-areas: "col1 . col2 col3";

  > ${Col} {
    &:first-child {
      grid-area: col1;
    }
    &:nth-child(2) {
      grid-area: col2;
    }
    &:last-child {
      grid-area: col3;
    }
  }

  ${StyledPicture} {
    background: ${colors.white};
  }

  @media (max-width: ${breakpoints.lg}) {
    grid-template-columns: 1fr 1fr;
    grid-template-areas:
      "col1 col2"
      "col3 col3";
  }
  @media (max-width: ${breakpoints.sm}) {
    grid-template-columns: 1fr;
    grid-template-areas: "col1" "col2" "col3";
  }
`

export const StyledBrandBasis = styled(Section)`
  background: ${colors.grayLight};

  ${StyledPluses} {
    margin-bottom: 56px;
  }

  ${Panel} {
    border-color: transparent;
    padding: 24px 32px;
  }

  @media (max-width: ${breakpoints.xs}) {
    ${StyledPluses} {
      margin-bottom: 24px;
    }
  }
`
