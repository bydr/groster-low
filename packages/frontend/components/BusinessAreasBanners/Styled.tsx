import { styled } from "@linaria/react"
import { breakpoints, colors } from "../../styles/utils/vars"
import { css } from "@linaria/core"
import { StyledLinkBase } from "../Link/StyledLink"
import { Section } from "../../styles/utils/Utils"

export const SectionAreaActivity = styled(Section)`
  background: ${colors.grayLight};
`

export const tileBannersCategories = css``
export const tileBannersProducts = css``

export const TileBanner = styled.div`
  position: relative;
  border-radius: 12px;
  overflow: hidden;
  display: block;
  text-align: center;

  ${StyledLinkBase} {
    position: absolute;
    z-index: 1;
    left: 0;
    top: 0;
    height: 100%;
    width: 100%;
  }
`
export const TileBanners = styled.div`
  width: 100%;
  display: grid;
  gap: 24px;
  margin-bottom: 0;

  &.${tileBannersCategories} {
    grid-template-columns: repeat(10, 1fr);
    margin-bottom: 32px;
    ${TileBanner} {
      grid-column: span 2;
    }

    @media (max-width: ${breakpoints.lg}) {
      grid-template-columns: repeat(6, 1fr);
      margin-bottom: 24px;

      ${TileBanner} {
        &:nth-last-child(2),
        &:nth-last-child(1) {
          grid-column: span 3;

          > * {
            width: 100% !important;
          }
        }
      }
    }
  }

  &.${tileBannersProducts} {
    grid-template-columns: repeat(12, 1fr);
    gap: 32px;

    ${TileBanner} {
      grid-column: span 4;
    }

    @media (max-width: ${breakpoints.lg}) {
      gap: 24px;
    }
  }

  @media (max-width: ${breakpoints.lg}) {
  }

  @media (max-width: ${breakpoints.sm}) {
    &,
    &.${tileBannersCategories}, &.${tileBannersProducts} {
      display: flex;
      flex-direction: column;
    }
  }
`
