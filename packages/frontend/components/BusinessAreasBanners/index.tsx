import { BaseHTMLAttributes, FC } from "react"
import {
  SectionAreaActivity,
  TileBanner,
  TileBanners,
  tileBannersCategories,
  tileBannersProducts,
} from "./Styled"
import { Container } from "styles/utils/StyledGrid"
import { Link } from "../Link"
import Image from "next/image"
import activeItem1 from "./images/6fb0a1e04e530c1cc2813696ad1d06fe.png"
import activeItem2 from "./images/d7303dce470669ebcbac15a01b3e3e24.png"
import activeItem3 from "./images/da8c0e436dc7e71d7b1c65a617e5095a.png"
import activeItem4 from "./images/6e3e6fcbd50512ade56e00ff2dc6af91.png"
import activeItem5 from "./images/ddac72797b92fb6a3719b667724b8517.png"
import activeItem6 from "./images/0f58ee19e095a461aa7e5288b4d59d1f.png"
import activeItem7 from "./images/24c314a8a26e46aab81604ca100fc584.png"
import activeItem8 from "./images/f161273b1ef72f06455ac2d894c0054b.png"
import { ROUTES } from "../../utils/constants"
import { SectionTitle } from "../../styles/utils/Utils"

type ActivityItemsType = {
  path: string
  imagePath: string
}
const areaActivityItems: ActivityItemsType[] = [
  {
    path: `${ROUTES.catalog}?sfera=30b3584c-de2b-11e9-fa83-ac1f6b855a52`,
    imagePath: activeItem1.src,
  },
  {
    path: `${ROUTES.catalog}?sfera=47f84e08-dba7-11e9-fa83-ac1f6b855a52`,
    imagePath: activeItem2.src,
  },
  {
    path: `${ROUTES.catalog}/hozyaystvennye-tovary`,
    imagePath: activeItem3.src,
  },
  {
    path: `${ROUTES.catalog}?sfera=2e51c2fe-dba7-11e9-fa83-ac1f6b855a52`,
    imagePath: activeItem4.src,
  },
  {
    path: `${ROUTES.catalog}/aksessuary-dlya-bumajnyh-stakanov`,
    imagePath: activeItem5.src,
  },
]
const areaActivityItems2: ActivityItemsType[] = [
  {
    path: `${ROUTES.catalog}/kanat-tross-verevka-shpagat-nit-lenta`,
    imagePath: activeItem6.src,
  },
  { path: `${ROUTES.catalog}/eco-upakovka`, imagePath: activeItem7.src },
  { path: `${ROUTES.catalog}/steklotara-kryshki`, imagePath: activeItem8.src },
]

export type BusinessAreasBannersPropsType = BaseHTMLAttributes<HTMLDivElement>

export const BusinessAreasBanners: FC<BusinessAreasBannersPropsType> = (
  props,
) => {
  return (
    <>
      <SectionAreaActivity {...props}>
        <Container>
          <SectionTitle>Сферы деятельности</SectionTitle>
          <TileBanners className={tileBannersCategories}>
            {areaActivityItems.map((item, index) => (
              <TileBanner key={index}>
                <Image
                  src={item.imagePath}
                  layout={"responsive"}
                  width={272}
                  height={148}
                  alt={"Banner categoriesInQuery"}
                  objectFit={"contain"}
                  objectPosition={"center"}
                />
                <Link href={item.path} />
              </TileBanner>
            ))}
          </TileBanners>
          <TileBanners className={tileBannersProducts}>
            {areaActivityItems2.map((item, index) => (
              <TileBanner key={index}>
                <Image
                  layout={"responsive"}
                  src={item.imagePath}
                  width={464}
                  height={290}
                  alt={"Banner categoriesInQuery"}
                  objectFit={"contain"}
                  objectPosition={"center"}
                />
                <Link href={item.path} />
              </TileBanner>
            ))}
          </TileBanners>
        </Container>
      </SectionAreaActivity>
    </>
  )
}
