import { styled } from "@linaria/react"
import { breakpoints } from "../../styles/utils/vars"
import { StyledEntityImage } from "../EntityImage/Styled"

export const StyledImageShop = styled.div`
  width: 100%;
  display: block;
  position: relative;
  margin-bottom: 20px;
  height: 200px;

  ${StyledEntityImage} {
    height: 100%;
  }

  @media (max-width: ${breakpoints.lg}) {
    height: 270px;
  }
`

export const StyledDetailContent = styled.div`
  grid-area: content;
`
export const StyledDetailMap = styled.div`
  grid-area: map;
`

export const StyledShopDetail = styled.section`
  width: 100%;
  position: relative;
  display: grid;
  grid-template-columns: 3fr 9fr;
  grid-template-areas: "content map";
  gap: 24px 56px;

  @media (max-width: ${breakpoints.lg}) {
    grid-template-areas: "content content" "map map";
  }
`
