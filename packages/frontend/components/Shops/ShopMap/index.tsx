import type { FC } from "react"
import { StyledMapInner, StyledShopMap } from "../ListMap/StyledShopsMap"
import { Clusterer, Map, Placemark, YMaps } from "react-yandex-maps"
import { ShopType, StoreProductType } from "../../../types/types"
import { useRouter } from "next/router"
import { getNavigatorLink } from "../../../hooks/shops"
import { useWindowSize } from "../../../hooks/windowSize"
import { getBreakpointVal } from "../../../styles/utils/Utils"
import { breakpoints } from "../../../styles/utils/vars"

export type ShopMapPropsType = {
  shops: (StoreProductType & ShopType)[]
  onPlacemarkClick?: (point: StoreProductType & ShopType) => void
  onBalloonClose?: () => void
  activatedShop?: (StoreProductType & ShopType) | null
  zoom?: number
}
export const ShopMap: FC<ShopMapPropsType> = ({
  onPlacemarkClick,
  shops,
  activatedShop,
  onBalloonClose,
  zoom,
}) => {
  const router = useRouter()
  const { width } = useWindowSize()

  const onPlacemarkClickHandle = (point: StoreProductType & ShopType) => () => {
    if (onPlacemarkClick) {
      onPlacemarkClick(point)
    }
    if (width !== undefined && width <= getBreakpointVal(breakpoints.md))
      void router.push(
        getNavigatorLink({
          latitude: point?.coordinates?.latitude,
          longitude: point?.coordinates?.longitude,
          description: point.address,
        }),
      )
  }

  const getPointData = (point: StoreProductType & ShopType) => {
    return {
      balloonContentBody: `${point?.address}`,
      clusterCaption: `${point?.address}`,
    }
  }

  return (
    <>
      <StyledShopMap>
        <StyledMapInner>
          <YMaps>
            <Map
              defaultState={{
                center: [
                  +(shops[0]?.coordinates?.latitude || ""),
                  +(shops[0]?.coordinates?.longitude || ""),
                ],
                zoom: zoom !== undefined ? zoom : 14,
                behaviors: ["default", "scrollZoom"],
              }}
              state={
                !!activatedShop
                  ? {
                      center: [
                        +(activatedShop?.coordinates?.latitude || ""),
                        +(activatedShop?.coordinates?.longitude || ""),
                      ],
                      zoom: 18,
                      behaviors: ["default", "scrollZoom"],
                    }
                  : undefined
              }
              width={"100%"}
              height={"100%"}
            >
              <Clusterer
                options={{
                  preset: "islands#invertedVioletClusterIcons",
                  groupByCoordinates: false,
                }}
              >
                {shops.map((point) => {
                  return (
                    <Placemark
                      modules={[
                        "geoObject.addon.balloon",
                        "geoObject.addon.hint",
                      ]}
                      key={point.uuid}
                      geometry={[
                        +(point?.coordinates?.latitude || ""),
                        +(point?.coordinates?.longitude || ""),
                      ]}
                      onBalloonClose={onBalloonClose}
                      onClick={onPlacemarkClickHandle(point)}
                      properties={getPointData(point)}
                      options={{
                        preset: "islands#violetIcon",
                        balloonPanelMaxMapArea: Infinity,
                      }}
                    />
                  )
                })}
              </Clusterer>
            </Map>
          </YMaps>
        </StyledMapInner>
      </StyledShopMap>
    </>
  )
}
