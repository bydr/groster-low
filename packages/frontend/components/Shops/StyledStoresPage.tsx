import { styled } from "@linaria/react"
import { css } from "@linaria/core"
import { breakpoints } from "../../styles/utils/vars"
import { StyledShopList } from "./Shop/StyledShop"
import { StyledShopMap } from "./ListMap/StyledShopsMap"
import { ButtonBase } from "../Button/StyledButton"

export const cssButtonToggle = css`
  display: none;
  ${ButtonBase}& {
    position: absolute;
    right: 30px;
    padding-right: 5px;
    padding-left: 5px;
  }

  @media (max-width: ${breakpoints.lg}) {
    display: flex;
  }

  @media (max-width: ${breakpoints.xs}) {
    right: 20px;
  }
`

export const cssModeMap = css``
export const cssModeList = css``

export const StyledStoresWrapper = styled.div`
  width: 100%;

  @media (max-width: ${breakpoints.lg}) {
    &.${cssModeMap} {
      ${StyledShopList} {
        display: none;
      }
    }
    &.${cssModeList} {
      ${StyledShopMap} {
        display: none;
      }
    }
  }
`
