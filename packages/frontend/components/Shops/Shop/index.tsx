import { DetailedHTMLProps, FC, HTMLAttributes, useEffect, useRef } from "react"
import { Address, Content, Shedule, StyledShop } from "./StyledShop"
import { ShopType, StoreProductType } from "../../../types/types"
import { Icon } from "../../Icon"
import { Link } from "../../Link"
import { dateToString } from "../../../utils/helpers"
import Available from "../../Available/Available"
import { EMPTY_DATA_PLACEHOLDER, ROUTES } from "../../../utils/constants"
import { cssButtonMore } from "../../Account/History/Orders/Order/Controls/Styled"
import { Button } from "../../Button"
import { FORMAT_PHONE, getPhoneWithOutCode } from "../../../validations/phone"
import NumberFormat from "react-number-format"
import { useWindowSize } from "../../../hooks/windowSize"
import { getBreakpointVal } from "../../../styles/utils/Utils"
import { breakpoints } from "../../../styles/utils/vars"
import { getNavigatorLink } from "../../../hooks/shops"
import { cssButtonInCart } from "../../Button/StyledButton"

export const Shop: FC<
  {
    shop: StoreProductType & ShopType
    onNameClick?: (shop: StoreProductType & ShopType) => void
    withRouteToDetail?: boolean
    isSelected?: boolean
    onSelectHandle?: (id: string) => void
    enableSelectShop?: boolean
  } & DetailedHTMLProps<HTMLAttributes<HTMLDivElement>, HTMLDivElement>
> = ({
  shop: {
    phone,
    schedule,
    address,
    deliveryDate,
    availableStatus,
    coordinates,
    ...shop
  },
  onNameClick,
  children,
  withRouteToDetail,
  isSelected = false,
  onSelectHandle,
  enableSelectShop = false,
  ...props
}) => {
  const { width } = useWindowSize()
  const shopRef = useRef<HTMLDivElement | null>(null)

  useEffect(() => {
    if (isSelected) {
      const parentModal = shopRef.current?.closest(`[role="alertdialog"]`)
      if (!!parentModal) {
        parentModal.scrollTop += shopRef?.current?.offsetTop || 0
      }

      const shopList = shopRef.current?.closest(`[role="shoplist"]`)
      if (!!shopList) {
        shopList.scrollTop += shopRef?.current?.offsetTop || 0
      }
    }
  }, [isSelected])

  return (
    <>
      <StyledShop {...props} ref={shopRef}>
        <Icon NameIcon={"Location"} size={"default"} />
        <Content>
          <Address
            onClick={() => {
              if (onNameClick) {
                onNameClick({
                  ...shop,
                  phone,
                  schedule,
                  address,
                  deliveryDate,
                  availableStatus,
                  coordinates,
                })
              }
            }}
          >
            {address || EMPTY_DATA_PLACEHOLDER}
          </Address>
          {!!schedule && <Shedule>{schedule}</Shedule>}
          {!!phone && (
            <Link href={`tel:${phone}`} variant={"black-to-purple"}>
              <NumberFormat
                format={FORMAT_PHONE}
                mask={"_"}
                value={getPhoneWithOutCode(phone || "")}
                displayType={"text"}
              />
            </Link>
          )}
          {!!deliveryDate && (
            <Shedule>
              Ближайшая дата доставки {dateToString(deliveryDate)}
            </Shedule>
          )}
          <Available status={availableStatus || null} />
          {children}

          {!!withRouteToDetail && (
            <>
              <Button
                variant={"small"}
                icon={"Info"}
                as={"a"}
                href={`${ROUTES.stores}/${shop.uuid}`}
                className={cssButtonMore}
              >
                Подробнее
              </Button>
            </>
          )}
          {width !== undefined && width <= getBreakpointVal(breakpoints.md) && (
            <>
              <Button
                variant={"small"}
                icon={"NearMe"}
                as={"a"}
                href={getNavigatorLink({
                  latitude: coordinates?.latitude,
                  longitude: coordinates?.longitude,
                  description: address,
                })}
                className={cssButtonMore}
              >
                Открыть в навигаторе
              </Button>
            </>
          )}
          {enableSelectShop && (
            <>
              {!isSelected ? (
                <>
                  <Button
                    variant={"filled"}
                    size={"small"}
                    onClick={() => {
                      if (onSelectHandle) {
                        onSelectHandle(shop.uuid)
                      }
                    }}
                  >
                    Выбрать
                  </Button>
                </>
              ) : (
                <>
                  <Button
                    variant={"filled"}
                    size={"small"}
                    icon={"Check"}
                    iconPosition={"left"}
                    className={cssButtonInCart}
                  >
                    Выбрано
                  </Button>
                </>
              )}
            </>
          )}
        </Content>
      </StyledShop>
    </>
  )
}
