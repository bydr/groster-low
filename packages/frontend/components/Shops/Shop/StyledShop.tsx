import { styled } from "@linaria/react"
import { getTypographyBase, Paragraph12 } from "../../Typography/Typography"
import { breakpoints, colors } from "../../../styles/utils/vars"
import { StyledLinkBase } from "../../Link/StyledLink"
import { cssIcon } from "../../Icon"
import { cssIsActive, getCustomizeScroll } from "../../../styles/utils/Utils"
import { StyledDialog } from "../../Modals/StyledModal"
import { StyledAvailable } from "../../Available/StyledAvailable"

export const Content = styled.div`
  width: 100%;
  display: grid;
  grid-auto-flow: row;
  grid-row-gap: 8px;
  justify-items: flex-start;
`

export const Address = styled(Paragraph12)`
  color: ${colors.brand.purple};
  cursor: pointer;

  &:hover,
  &:active {
    color: ${colors.brand.purpleDarken};
  }
`

export const Shedule = styled(Paragraph12)`
  color: ${colors.grayDark};
`

export const StyledShopList = styled.div`
  width: 100%;
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  justify-content: flex-start;

  ${StyledDialog} & {
    ${getCustomizeScroll()};
    max-height: 75vh;
    overflow-x: hidden;
    overflow-y: auto;
    padding-right: 20px;

    @media (max-width: ${breakpoints.sm}) {
      max-height: 100%;
    }
  }
`

export const StyledShop = styled.div`
  width: 100%;
  position: relative;
  margin-bottom: 18px;
  display: flex;
  align-items: flex-start;
  padding: 6px 0;
  border-radius: 0.4em;

  ${StyledLinkBase} {
    ${getTypographyBase("p12")};
    margin-bottom: 0;
  }

  > .${cssIcon} {
    fill: ${colors.brand.yellow};
    margin-right: 8px;
  }

  &.${cssIsActive} {
    background: ${colors.grayLight};
  }

  ${StyledAvailable} {
    justify-content: flex-start;
  }
`
