import { styled } from "@linaria/react"
import { StyledDialog } from "../../Modals/StyledModal"
import { breakpoints } from "../../../styles/utils/vars"

export const StyledMapInner = styled.div`
  position: absolute;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
`

export const StyledShopMap = styled.div`
  min-height: 70vh;
  position: relative;
  overflow: hidden;

  ${StyledDialog} & {
    margin-right: -48px;
    margin-bottom: -20px;

    @media (max-width: ${breakpoints.sm}) {
      width: 100%;
      margin: 0;
      overflow: initial;

      & ${StyledMapInner} {
        left: -15px;
        right: -15px;
      }
    }
  }

  @media (max-width: ${breakpoints.sm}) {
    height: 300px;
    min-height: initial;
    width: 100%;
  }
`
export const StyledShopsMap = styled.div`
  width: 100%;
  display: grid;
  grid-template-columns: 4fr 8fr;
  grid-column-gap: 20px;

  @media (max-width: ${breakpoints.sm}) {
    grid-template-columns: 1fr;
    display: flex;
    flex-direction: column-reverse;
    align-items: flex-start;
    gap: 20px 0;
  }
`
