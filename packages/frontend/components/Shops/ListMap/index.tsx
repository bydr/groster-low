import { FC, memo, ReactNode } from "react"
import { StyledShopsMap } from "./StyledShopsMap"
import { StoreProductType } from "../../../types/types"
import { useShops } from "../../../hooks/shops"
import { ShopList } from "../List"
import type { ShopMapPropsType } from "../ShopMap"
import dynamic, { DynamicOptions } from "next/dynamic"

const ShopMap = dynamic(
  (() =>
    import("../ShopMap").then(
      (mod) => mod.ShopMap,
    )) as DynamicOptions<ShopMapPropsType>,
  {
    ssr: false,
  },
)

export const ListMap: FC<{
  shops?: StoreProductType[]
  withRouteToDetail?: boolean
  shopBody?: ReactNode
  selectedShopId?: string
  onSelectShop?: (id: string) => void
  enableSelectShop?: boolean
}> = memo(
  ({
    shops: shopsOut,
    withRouteToDetail,
    shopBody,
    selectedShopId,
    onSelectShop,
    enableSelectShop = false,
  }) => {
    const {
      activatedShop,
      setActivatedShop,
      shopsAvailable,
      shops,
      selectedShop,
    } = useShops({
      shops: shopsOut,
      selectedShopId: selectedShopId,
    })

    return (
      <>
        {(shops || []).length > 0 && (
          <StyledShopsMap>
            <ShopList
              onNameClick={(point) => {
                setActivatedShop(point)
              }}
              activatedShop={activatedShop}
              selectedShopId={selectedShop?.uuid}
              shops={
                shopsOut !== undefined ? shopsAvailable || [] : shops || []
              }
              withRouteToDetail={withRouteToDetail}
              shopBody={shopBody}
              onSelectShop={onSelectShop}
              enableSelectShop={enableSelectShop}
            />
            <ShopMap
              onPlacemarkClick={(point) => {
                setActivatedShop(point)
              }}
              onBalloonClose={() => {
                setActivatedShop(null)
              }}
              activatedShop={activatedShop}
              shops={
                shopsOut !== undefined ? shopsAvailable || [] : shops || []
              }
            />
          </StyledShopsMap>
        )}
      </>
    )
  },
)

ListMap.displayName = "ListMap"
