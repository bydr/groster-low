import type { FC, ReactNode } from "react"
import { Shop } from "../Shop"
import { cx } from "@linaria/core"
import { cssIsActive } from "../../../styles/utils/Utils"
import { StyledShopList } from "../Shop/StyledShop"
import { ShopType, StoreProductType } from "../../../types/types"

export const ShopList: FC<{
  onNameClick?: (point: StoreProductType & ShopType) => void
  shops: (StoreProductType & ShopType)[]
  activatedShop: (StoreProductType & ShopType) | null
  selectedShopId?: string
  withRouteToDetail?: boolean
  shopBody?: ReactNode
  onSelectShop?: (id: string) => void
  enableSelectShop?: boolean
}> = ({
  onNameClick,
  activatedShop,
  shops,
  withRouteToDetail,
  selectedShopId,
  shopBody,
  onSelectShop,
  enableSelectShop = false,
}) => {
  return (
    <>
      <StyledShopList role={"shoplist"}>
        {shops.map((s) => (
          <Shop
            key={s.uuid}
            shop={s}
            className={cx(s.uuid === activatedShop?.uuid && cssIsActive)}
            onNameClick={onNameClick}
            withRouteToDetail={withRouteToDetail}
            isSelected={
              selectedShopId !== undefined
                ? s.uuid === selectedShopId
                : undefined
            }
            onSelectHandle={onSelectShop}
            enableSelectShop={enableSelectShop}
          >
            {shopBody}
          </Shop>
        ))}
      </StyledShopList>
    </>
  )
}
