import {
  cloneElement,
  createContext,
  FC,
  FunctionComponentElement,
  useEffect,
  useRef,
  useState,
} from "react"
import {
  cssButtonModalClose,
  Header,
  Heading,
  StyledDialog,
  StyledDialogContent,
  StyledModalBackdrop,
} from "./StyledModal"
import { Button } from "../Button"
import { DisclosureProps } from "reakit"
import { DialogDisclosure, useDialogState } from "reakit/Dialog"
import { Typography } from "../Typography/Typography"
import { useAppDispatch, useAppSelector } from "../../hooks/redux"
import { appSlice } from "../../store/reducers/appSlice"
import { cx } from "@linaria/core"
import { cssIsSticky } from "../../layouts/Default/Header/StyledHeader"
import { scrollBodyDisable, scrollBodyEnable } from "../../utils/helpers"

export type ModalDefaultPropsType = {
  variant?: "rounded-0" | "full" | "rounded-100" | "rounded-50" | "rounded-70"
  title?: string
  disclosure?: FunctionComponentElement<unknown>
  onClose?: () => void
  isShowModal?: boolean
  closeMode?: "hide" | "destroy"
  hideOnClickOutside?: boolean
}

export const ModalContext = createContext<null | {
  hide: () => void
  baseId: string
}>(null)

export const Modal: FC<ModalDefaultPropsType> = ({
  title,
  variant = "rounded-0",
  disclosure,
  children,
  onClose,
  isShowModal,
  closeMode = "hide",
  hideOnClickOutside = true,
}) => {
  const openedModals = useAppSelector((state) => state.app.openedModals)
  const { appendModal, removeModal } = appSlice.actions
  const dispatch = useAppDispatch()

  const dialog = useDialogState({
    animated: 1000,
    modal: true,
  })
  const element = useRef<HTMLDivElement | null>(null)
  const bodyRef = useRef<HTMLDivElement | null>(null)

  const previousVisibleRef = useRef(dialog.visible)

  const [isSticky, setIsSticky] = useState<boolean>(false)

  useEffect(() => {
    if (previousVisibleRef.current !== dialog.visible) {
      if (!dialog.visible) {
        scrollBodyEnable()
        dispatch(removeModal(dialog.baseId))
        onClose && onClose()
      } else {
        scrollBodyDisable(element.current || undefined)
        dispatch(appendModal(dialog.baseId))
      }
    }

    previousVisibleRef.current = dialog.visible
  }, [
    appendModal,
    dialog.baseId,
    dialog.visible,
    dispatch,
    onClose,
    removeModal,
    openedModals.length,
  ])

  useEffect(() => {
    if (isShowModal !== undefined) {
      isShowModal ? dialog.show() : dialog.hide()
    }
  }, [isShowModal])

  useEffect(() => {
    return () => {
      scrollBodyEnable()
    }
  }, [])

  return (
    <>
      <ModalContext.Provider
        value={{
          hide: dialog.hide,
          baseId: dialog.baseId,
        }}
      >
        {disclosure !== undefined && (
          <>
            <DialogDisclosure
              {...dialog}
              ref={disclosure.ref}
              {...(disclosure.props || {})}
            >
              {(disclosureProps: DisclosureProps) =>
                cloneElement(disclosure, disclosureProps)
              }
            </DialogDisclosure>
          </>
        )}
        <StyledModalBackdrop
          data-variant={variant}
          {...dialog}
          role="alertdialog"
          // ref={element}
          className={cx(isSticky && cssIsSticky)}
          onScroll={() => {
            if (dialog.animating) {
              return
            }
            const offsetTop = bodyRef.current?.getBoundingClientRect().top || 0
            setIsSticky(offsetTop < 10)
          }}
        >
          <StyledDialog
            {...dialog}
            preventBodyScroll={false}
            aria-label="dialog"
            tabIndex={0}
            unstable_finalFocusRef={element}
            unstable_autoFocusOnShow={false}
            unstable_autoFocusOnHide={false}
            hideOnClickOutside={hideOnClickOutside}
          >
            {(closeMode === "hide" || dialog.visible || dialog.animating) && (
              <StyledDialogContent ref={bodyRef}>
                <Header>
                  {title && (
                    <Heading>
                      <Typography variant={"h3"}>{title}</Typography>
                    </Heading>
                  )}
                  <Button
                    variant={"box"}
                    icon={"X"}
                    className={cssButtonModalClose}
                    onClick={() => {
                      dialog.hide()
                    }}
                  />
                </Header>
                {children}
              </StyledDialogContent>
            )}
          </StyledDialog>
        </StyledModalBackdrop>
      </ModalContext.Provider>
    </>
  )
}
