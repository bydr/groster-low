import { styled } from "@linaria/react"
import { Dialog, DialogBackdrop } from "reakit/Dialog"
import {
  breakpoints,
  colors,
  sizeSVG,
  transitionTimingFunction,
} from "../../styles/utils/vars"
import { css } from "@linaria/core"
import { cssIcon, getSizeSVG } from "../Icon"
import { ButtonBase } from "../Button/StyledButton"
import { StyledFilterInner, TotalControls } from "../Filter/StyledFilter"
import { StyledForm } from "../Forms/StyledForms"
import { StyledTags } from "../Tags/StyledTags"
import { cssIsSticky } from "../../layouts/Default/Header/StyledHeader"

export const cssOutlineAccent = css`
  box-shadow: 0 0 0 72px ${colors.brand.purple};
`

export const cssButtonModalClose = css`
  &${ButtonBase} {
    position: absolute;
    left: -95px;
    top: -72px;
    background: transparent;
    margin: 0;

    &:hover,
    &:active {
      .${cssIcon} {
        fill: ${colors.white};
      }
    }

    .${cssIcon} {
      fill: ${colors.brand.yellow};
      ${getSizeSVG(sizeSVG.large)};
    }
  }

  @media (max-width: ${breakpoints.lg}) {
    &${ButtonBase} {
      left: -75px;
      top: -52px;
    }
  }

  @media (max-width: ${breakpoints.sm}) {
    &${ButtonBase} {
      left: auto;
      top: -110px;
      right: 0;
    }
  }
`

export const Heading = styled.div`
  width: 100%;
  margin-bottom: 26px;

  @media (max-width: ${breakpoints.sm}) {
    margin-bottom: 10px;
  }
`

export const Header = styled.div`
  z-index: 4;
  transition: none;
  border-radius: 0;
`

export const StyledOverlay = styled.div`
  top: 0;
  right: 0;
  bottom: 0;
  left: 0;
  background: ${colors.transparentBlack};
  position: fixed;
  padding: 0;
  height: 100%;
  width: 100%;
  opacity: 0;
  visibility: hidden;
  transition: all 0.2s ${transitionTimingFunction};
`

export const StyledDialogContent = styled.div`
  position: relative;
  width: 100%;
  max-width: 100%;
  margin: 0;
  padding: 32px 48px 20px 48px;
  background: ${colors.white};
  border-radius: 3rem 0 0 3rem;
  box-shadow: 0 0 0 72px ${colors.grayLight};
  transform: translateX(100%);
  transition: all 0.6s ${transitionTimingFunction};
  opacity: 0;
  visibility: hidden;
  box-sizing: border-box;
  background-clip: padding-box;
  will-change: transform;

  @media (max-width: ${breakpoints.lg}) {
    box-shadow: 0 0 0 53px ${colors.grayLight};
    margin: 53px 0 0 0;
  }
`

export const StyledDialog = styled(Dialog)`
  position: relative;
  box-sizing: border-box;
  float: right;
  margin: 70px 0 0 auto;
  max-width: 490px;
  width: 100%;

  ${StyledForm} {
    border-radius: inherit;
  }

  ${StyledTags} {
    margin-bottom: 0;
  }

  ${StyledFilterInner} {
    > ${ButtonBase} {
      display: none;
    }
  }

  @media (max-width: ${breakpoints.sm}) {
    margin-top: 100px;

    ${StyledDialogContent} {
      float: none;
      width: 100%;
      border-radius: 3rem 3rem 0 0;
      padding: 32px 15px;
      min-width: initial;
      max-width: initial;
      transform: translateY(0) translateY(100%);
    }

    &[data-enter] {
      ${StyledDialogContent} {
        transform: translateY(0);
      }
    }
  }
`

export const StyledModalBackdrop = styled(DialogBackdrop)`
  position: fixed;
  left: 0;
  top: 0;
  bottom: 0;
  right: 0;
  z-index: 1000;
  height: 100%;
  width: 100%;
  padding: 28px 0;
  background: ${colors.transparentBlack};
  transition: all 0.6s ${transitionTimingFunction};
  opacity: 0;
  visibility: hidden;
  overflow: auto;
  overflow-y: auto;
  overflow-x: hidden;
  -webkit-overflow-scrolling: touch;
  animation-fill-mode: none;

  &[data-leave] {
    opacity: 0;
    visibility: hidden;
  }

  &[data-enter] {
    opacity: 1;
    visibility: visible;
  }

  &[data-variant="full"] {
    padding: 0;
    max-width: 100%;
    width: 100%;

    ${StyledDialog} {
      margin: 0;
      max-width: 100%;
      width: 100%;
    }

    ${StyledDialogContent} {
      border-radius: 0;
      box-shadow: none;
      margin: 0;
      padding: 32px 30px;
      min-height: 100vh;
      display: flex;
      flex-direction: column;
      max-width: 100%;
      width: 100%;
      transition: all 0.8s ${transitionTimingFunction};
    }

    .${cssButtonModalClose} {
      top: 20px;
      right: 20px;
      left: auto;

      &:hover,
      &:active {
        .${cssIcon} {
          fill: ${colors.brand.purple};
        }
      }
    }

    ${TotalControls} {
      display: inline-grid;
      bottom: 0;
    }

    @media (max-width: ${breakpoints.md}) {
      ${StyledDialogContent} {
        padding-left: 15px;
        padding-right: 15px;
      }
    }
  }

  &[data-variant="rounded-50"] {
    ${StyledDialogContent} {
      transition: all 0.6s ${transitionTimingFunction};
    }
    ${StyledDialog} {
      max-width: 50%;
    }
  }

  &[data-variant="rounded-70"] {
    ${StyledDialogContent} {
      transition: all 0.7s ${transitionTimingFunction};
      max-width: 100%;
    }
    ${StyledDialog} {
      max-width: 70%;
    }
  }

  &[data-variant="rounded-100"] {
    ${StyledDialogContent} {
      transition: all 0.8s ${transitionTimingFunction};
    }
    ${StyledDialog} {
      max-width: 90%;
    }
  }

  &[data-enter] {
    ${StyledDialogContent} {
      transform: translateX(0);
      opacity: 1;
      visibility: visible;
    }
    ${StyledOverlay} {
      opacity: 1;
      visibility: visible;
    }
  }

  @media (max-width: ${breakpoints.xl}) {
    &[data-variant="rounded-50"] {
      ${StyledDialog} {
        max-width: 80%;
      }
    }
  }

  @media (max-width: ${breakpoints.lg}) {
    &[data-variant="rounded-50"] {
      ${StyledDialog} {
        max-width: 100%;
      }
    }
    &[data-variant="rounded-70"] {
      ${StyledDialog} {
        max-width: 100%;
      }
    }
  }

  @media (max-width: ${breakpoints.sm}) {
    padding-bottom: 0;

    ${StyledDialog} {
      max-width: 100%;
    }

    &[data-variant] {
      ${StyledDialog}, ${StyledDialogContent} {
        width: 100%;
        max-width: 100%;
      }

      ${StyledDialogContent} {
        min-height: 70vh;
      }
    }

    &[data-variant="full"] {
      ${StyledDialog} {
        margin-top: 0;
      }

      ${StyledDialogContent} {
        min-height: 100vh;
      }
    }

    &:not([data-variant="full"]).${cssIsSticky} ${Header} {
      position: sticky;
      background: ${colors.white};
      width: auto;
      left: 0;
      right: 0;
      z-index: 5;
      display: flex;
      flex-direction: row;
      justify-content: flex-end;
      align-items: center;
      top: -28px;
      margin-right: -15px;
      margin-left: -15px;
      padding-left: 15px;
      height: 60px;
      border-bottom: 1px solid ${colors.gray};

      ${Heading} {
        flex: 1;
        margin: 0;

        > * {
          margin: 0;
          text-align: left;
          display: -webkit-box;
          -webkit-line-clamp: 2;
          -webkit-box-orient: vertical;
          overflow: hidden;
          line-clamp: 2;
          -webkit-line-clamp: 2;
          -webkit-box-orient: vertical;
          max-height: 46px;
        }
      }

      .${cssButtonModalClose}${ButtonBase} {
        position: relative;
        top: auto;
        right: auto;
        left: auto;
      }
    }
  }
`
