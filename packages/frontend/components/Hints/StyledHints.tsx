import { styled } from "@linaria/react"
import {
  BoxShadowPopover,
  colors,
  transitionTimingFunction,
} from "../../styles/utils/vars"
import { StyledList, StyledListItem } from "../List/StyledList"
import { getCustomizeScroll } from "../../styles/utils/Utils"
import { TypographyBase } from "../Typography/Typography"

export const StyledHints = styled.div`
  ${getCustomizeScroll()};
  position: absolute;
  left: 0;
  z-index: 10;
  background: ${colors.white};
  width: 100%;
  max-height: 300px;
  overflow-x: hidden;
  overflow-y: auto;
  top: calc(100% - 10px);
  border: 1px solid ${colors.gray};
  border-radius: 8px;
  padding: 10px 8px;
  background: ${colors.white};
  transition: opacity 250ms ${transitionTimingFunction},
    transform 250ms ${transitionTimingFunction};
  transform-origin: top center;
  box-shadow: ${BoxShadowPopover};

  ${StyledList} {
    ${StyledListItem} {
      padding: 4px 8px;
      width: 100%;
      cursor: pointer;
      border-radius: 4px;

      &:hover,
      &:active {
        background: ${colors.grayLight};
      }
    }
  }

  > ${TypographyBase} {
    margin-bottom: 0;
  }
`
export const StyledHintsRelative = styled(StyledHints)`
  position: relative;
  box-shadow: none;
  border: none;
  margin: 0;
  padding-left: 0;
  padding-right: 0;
  max-height: 380px;
`
