import { styled } from "@linaria/react"
import { breakpoints, colors } from "../../styles/utils/vars"
import {
  Paragraph12,
  Paragraph14,
  TypographyBase,
} from "../Typography/Typography"
import { cssIsActive } from "../../styles/utils/Utils"

export const StyledImportantList = styled.div`
  width: 100%;
  column-count: 3;
  column-gap: 32px;
  margin-bottom: 20px;

  > * {
    margin-bottom: 4px;
  }

  @media (max-width: ${breakpoints.md}) {
    column-count: 2;
  }

  @media (max-width: 320px) {
    column-count: 1;
  }
`

export const StyledLocationItem = styled(Paragraph14)`
  cursor: pointer;
  &:hover,
  &:active {
    color: ${colors.brand.purple};
  }

  &.${cssIsActive} {
    color: ${colors.brand.purple};
  }
`

export const StyledLocationHint = styled.div`
  width: 100%;
  display: flex;
  flex-direction: column;

  ${Paragraph14}, ${TypographyBase} {
    margin: 0;
  }

  ${Paragraph12} {
    color: ${colors.grayDark};
  }
`
