import { FC, useCallback, useContext, useEffect, useState } from "react"
import { Field } from "../Field/Field"
import { LOCATIONS_IMPORTANT } from "../../utils/constants"
import {
  StyledImportantList,
  StyledLocationHint,
  StyledLocationItem,
} from "./Styled"
import { cx } from "@linaria/core"
import { cssIsActive } from "../../styles/utils/Utils"
import { ModalContext } from "../Modals/Modal"
import { useMutation } from "react-query"
import { fetchSuggestAddresses } from "../../api/dadataAPI"
import { useDebounce } from "../../hooks/debounce"
import { LocationType } from "../../types/types"
import { StyledHintsRelative } from "../Hints/StyledHints"
import { cssListVertical, StyledList, StyledListItem } from "../List/StyledList"
import { Typography } from "../Typography/Typography"
import { Button } from "../Button"
import { getFormatResponseLocation } from "../../utils/helpers"

export const LocationList: FC<{
  currentLocation?: LocationType
  onSelectLocation?: (location: LocationType | null) => void
}> = ({ onSelectLocation, currentLocation }) => {
  const modalContext = useContext(ModalContext)
  const [inputCity, setInputCity] = useState<string | undefined>()
  const debouncedCity = useDebounce<string>(inputCity || "")
  const [hints, setHints] = useState<LocationType[] | null>(null)

  const { mutate: suggestAddressesMutate, reset: resetMutate } = useMutation(
    fetchSuggestAddresses,
    {
      onSuccess: (data) => {
        if (!!data && !!data.suggestions) {
          setHints(
            data.suggestions
              .map((s) => {
                return getFormatResponseLocation(s?.data || null)
              })
              .filter((item) => !!item.city),
          )
        } else {
          setHints([])
        }
      },
      onError: (error) => {
        console.log("error address ", error)
      },
    },
  )

  const reset = useCallback(() => {
    setHints(null)
    resetMutate()
    setInputCity(undefined)
  }, [resetMutate])

  const onSelectHandle = useCallback(
    (location: LocationType | null) => {
      if (onSelectLocation) {
        onSelectLocation(location)
      }
      modalContext?.hide()
    },
    [modalContext, onSelectLocation],
  )

  useEffect(() => {
    if (!!debouncedCity && debouncedCity.length > 0) {
      suggestAddressesMutate({
        address: debouncedCity,
        fromBound: {
          value: "city",
        },
        toBound: {
          value: "city",
        },
      })
    }
  }, [debouncedCity, suggestAddressesMutate])

  return (
    <>
      <Field
        type={"text"}
        value={
          inputCity !== undefined ? inputCity : currentLocation?.city || ""
        }
        placeholder={"Найдите свой город"}
        withAnimatingLabel
        iconField={"NearMe"}
        onChange={(e) => {
          setInputCity(e.currentTarget.value)
        }}
      />
      {hints === null && (
        <>
          <StyledImportantList>
            {LOCATIONS_IMPORTANT.map((l, index) => (
              <StyledLocationItem
                key={index}
                className={cx(l.city === currentLocation?.city && cssIsActive)}
                onClick={() => {
                  setInputCity(undefined)
                  onSelectHandle(l)
                }}
              >
                {l.city}
              </StyledLocationItem>
            ))}
          </StyledImportantList>
        </>
      )}
      {hints !== null && (
        <>
          <Button variant={"translucent"} size={"small"} onClick={reset}>
            Сбросить
          </Button>
          {hints.length > 0 ? (
            <>
              <StyledHintsRelative>
                <StyledList className={cssListVertical}>
                  {hints.map((location, i) => (
                    <StyledListItem
                      key={i}
                      onClick={() => {
                        setHints(null)
                        setInputCity(location.city || "")
                        onSelectHandle(location)
                      }}
                    >
                      <StyledLocationHint>
                        <Typography>{location.city}</Typography>
                        <Typography variant={"p12"}>
                          {location.region} {location.region_type}
                        </Typography>
                      </StyledLocationHint>
                    </StyledListItem>
                  ))}
                </StyledList>
              </StyledHintsRelative>
            </>
          ) : (
            <>
              <Typography variant={"p14"}>Результатов не найдено</Typography>
            </>
          )}
        </>
      )}
    </>
  )
}
