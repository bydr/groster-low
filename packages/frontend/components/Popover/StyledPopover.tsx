import { styled } from "@linaria/react"
import {
  BoxShadowPopover,
  breakpoints,
  colors,
  sizeSVG,
  transitionTimingFunction,
} from "../../styles/utils/vars"
import { Popover, PopoverArrow, PopoverDisclosure } from "reakit/Popover"
import { getTypographyBase, TypographyBase } from "../Typography/Typography"
import { cssIcon, getSizeSVG } from "../Icon"
import { ButtonBase, cssButtonClean } from "../Button/StyledButton"
import { StyledRangeContainer } from "../Range/StyledRange"

export const StyledPopoverArrow = styled(PopoverArrow)`
  fill: ${colors.white};
`

export const StyledPopover = styled(Popover)``

export const StyledPopoverInner = styled.div`
  width: 100%;
  display: flex;
  flex-direction: column;
  align-items: flex-start;

  > ${TypographyBase} {
    &:last-child {
      margin-bottom: 0;
    }
  }
`

export const StyledPopoverDisclosure = styled(PopoverDisclosure)`
  ${getTypographyBase("p14")};
  appearance: none;
  display: inline-flex;
  align-items: center;
  justify-content: space-between;
  padding: 2px 12px 2px 14px;
  background: ${colors.grayLight};
  border: none;
  border-radius: 2px;
  cursor: pointer;
  margin-bottom: 0;

  &${ButtonBase} {
    &:not(.${cssButtonClean}) {
      padding: 0.3em 1em;
      border: 2px solid transparent;
      color: inherit;
    }
  }
`

export const PopoverContainer = styled.div`
  display: inline-flex;
  margin: 0 2px 2px 0;

  > ${ButtonBase} {
    width: 100%;
  }

  ${StyledPopoverInner} {
    border: 0;
    background: none;
    padding: 0;
    width: 100%;
  }

  ${StyledPopoverInner} {
    display: flex;
    border: 1px solid ${colors.gray};
    border-radius: 8px;
    padding: 16px 14px;
    background: ${colors.white};
    transition: opacity 250ms ${transitionTimingFunction},
      transform 250ms ${transitionTimingFunction};
    opacity: 0;
    transform-origin: top center;
    transform: translate3d(0, -20px, 0);
    box-shadow: ${BoxShadowPopover};
    width: 100%;
  }

  &[data-size="small"] {
    ${StyledPopoverInner} {
      padding: 5px 10px;
    }
  }

  &:not([data-styling-icon="false"]) {
    ${StyledPopoverDisclosure} {
      .${cssIcon} {
        ${getSizeSVG(sizeSVG.smaller)};
        fill: ${colors.brand.purple};
        margin-left: 4px;
        transition: all 0.2s ${transitionTimingFunction};
      }

      &[aria-expanded="true"] {
        .${cssIcon} {
          transform: rotate(180deg);
        }
      }
    }
  }

  [data-enter] {
    ${StyledPopoverInner} {
      opacity: 1;
      transform: translate3d(0, 0, 0);
    }
  }

  [role="dialog"] {
    z-index: 10;
  }

  &[data-hidden="true"] {
    ${StyledPopoverDisclosure} {
      position: absolute;
      left: 0;
      top: 0;
      right: 0;
      bottom: 0;
      opacity: 0;
      width: 100%;
      height: 100%;
      z-index: -1;
      user-select: none;
      padding: 0;
      margin: 0;
      pointer-events: none;
    }
  }

  ${StyledRangeContainer} {
    max-width: 320px;
  }

  @media (max-width: ${breakpoints.lg}) {
    ${StyledRangeContainer} {
      max-width: 100%;
    }
  }
`
