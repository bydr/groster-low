import { BaseHTMLAttributes, forwardRef, ReactElement } from "react"
import type { CheckboxPropsType } from "./CustomCheckbox"
import { Checkbox } from "reakit/Checkbox"
import { styled } from "@linaria/react"
import {
  boxCheck,
  boxRadio,
  colors,
  transitionTimingFunction,
} from "../../styles/utils/vars"
import {
  ErrorMessageField,
  getTypographyBase,
  TypographyBase,
} from "../Typography/Typography"
import { StyledLinkBase } from "../Link/StyledLink"
import { css } from "@linaria/core"

export const CheckboxBox = styled.span`
  margin-right: 12px;
  margin-top: 3px;
`
export const CheckboxContainer = styled.span`
  width: 100%;
  display: inline-flex;
  align-items: flex-start;
`
export const CheckboxBase = styled.label`
  ${getTypographyBase("p14")};
  cursor: pointer;
  display: inline-flex;
  flex-direction: column;
  align-items: flex-start;
  padding-right: 12px;

  [type="checkbox"] {
    visibility: hidden;
    opacity: 0;
    position: absolute;
  }

  ${CheckboxBox} {
    transition: all 0.2s ${transitionTimingFunction};

    &:after {
      content: "";
      position: absolute;
      transition: all 0.3s ${transitionTimingFunction};
    }
  }

  ${TypographyBase} {
    margin: 0;
  }

  ${StyledLinkBase} {
    margin: 0 0.3em;
    display: inline;
  }

  &[data-iserror="true"] {
    ${CheckboxContainer} {
      ${CheckboxBox} {
        border-color: ${colors.red};
      }
    }
  }
`

const CheckboxCheck = styled(CheckboxBase)`
  ${CheckboxBox} {
    position: relative;
    width: ${boxCheck.container.w};
    min-width: ${boxCheck.container.w};
    height: ${boxCheck.container.h};
    min-height: ${boxCheck.container.h};
    border: 1px solid ${colors.gray};
    border-radius: 2px;

    &:after {
      width: ${boxCheck.target.w};
      min-width: ${boxCheck.target.w};
      height: ${boxCheck.target.h};
      min-height: ${boxCheck.target.h};
      background: transparent;
      border-radius: 2px;
      top: 50%;
      left: 50%;
      transform: translate(-50%, -50%);
      opacity: 0;
    }
  }

  [type="checkbox"] {
    &:checked {
      ~ ${CheckboxBox} {
        &:after {
          background: ${colors.brand.yellow};
          opacity: 1;
        }
      }
    }
  }

  &:hover {
    ${CheckboxBox} {
      border-color: ${colors.black};

      &:after {
        background: ${colors.grayLight};
        opacity: 1;
      }
    }
  }
`
const CheckboxRadio = styled(CheckboxBase)`
  ${CheckboxBox} {
    width: ${boxRadio.container.w};
    min-width: ${boxRadio.container.w};
    height: ${boxRadio.container.h};
    min-height: ${boxRadio.container.h};
    position: relative;
    border-radius: 50px;
    display: flex;
    background: ${colors.grayLight};

    &:after {
      width: ${boxRadio.target.w};
      min-width: ${boxRadio.target.w};
      height: ${boxRadio.target.h};
      min-height: ${boxRadio.target.h};
      background: ${colors.brand.yellow};
      border-radius: 50px;
      top: 2px;
      left: 2px;
    }
  }

  [type="checkbox"] {
    &:checked {
      ~ ${CheckboxBox} {
        background: ${colors.grayDark};

        &:after {
          transform: translateX(100%);
        }
      }
    }
  }
`

export const cssCheckBoxOverlay = css`
  &,
  &${CheckboxCheck} {
    ${CheckboxBox} {
      border-color: ${colors.white};
    }

    [type="checkbox"] {
      &:checked {
        ~ ${CheckboxBox} {
          &:after {
            background: ${colors.white};
          }
        }
      }
    }

    &:hover {
      ${CheckboxBox} {
        border-color: ${colors.black};

        &:after {
          background: transparent;
        }
      }
    }
  }
`

interface CheckboxWrapperType extends BaseHTMLAttributes<HTMLInputElement> {
  ComponentCheckbox: typeof CheckboxCheck | typeof CheckboxRadio
  message: string | ReactElement
  errorMessage?: string
}

const CheckboxWrapper = forwardRef<HTMLInputElement, CheckboxWrapperType>(
  ({ ComponentCheckbox, message, className, errorMessage, ...props }, ref) => {
    return (
      <ComponentCheckbox className={className} data-iserror={!!errorMessage}>
        <CheckboxContainer>
          <Checkbox {...props} ref={ref} />
          <CheckboxBox />
          {message}
        </CheckboxContainer>
        {errorMessage && <ErrorMessageField>{errorMessage}</ErrorMessageField>}
      </ComponentCheckbox>
    )
  },
)
CheckboxWrapper.displayName = "CheckboxWrapper"

const StyledCheckbox = forwardRef<HTMLInputElement, CheckboxPropsType>(
  ({ variant = "check", ...props }, ref) => {
    switch (variant) {
      case "check": {
        return (
          <CheckboxWrapper
            ComponentCheckbox={CheckboxCheck}
            {...props}
            ref={ref}
          />
        )
      }
      case "radio": {
        return (
          <CheckboxWrapper
            ComponentCheckbox={CheckboxRadio}
            {...props}
            ref={ref}
          />
        )
      }
      default:
        return <></>
    }
  },
)
StyledCheckbox.displayName = "StyledCheckbox"

export default StyledCheckbox
