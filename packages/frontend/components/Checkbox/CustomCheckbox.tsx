import type { BaseHTMLAttributes, ReactElement } from "react"
import { forwardRef, useEffect } from "react"
import StyledCheckbox from "./StyledCheckbox"
import { useCheckboxState } from "reakit"

export type CheckboxPropsType = {
  variant: "radio" | "check"
  name: string
  message: string | ReactElement
  initialState?: boolean
  stateCheckbox?: boolean
  errorMessage?: string
  indeterminate?: boolean
}

export const CustomCheckbox = forwardRef<
  HTMLInputElement,
  CheckboxPropsType & BaseHTMLAttributes<HTMLInputElement>
>(
  (
    {
      indeterminate,
      variant,
      name,
      message,
      initialState = false,
      stateCheckbox,
      ...props
    },
    ref,
  ) => {
    const checkbox = useCheckboxState({ state: initialState })

    useEffect(() => {
      if (stateCheckbox !== undefined) {
        checkbox.setState(stateCheckbox)
      }
    }, [stateCheckbox, checkbox])

    return (
      <StyledCheckbox
        variant={variant}
        message={message}
        name={name}
        {...props}
        {...checkbox}
        data-indeterminate={indeterminate}
        ref={ref}
      />
    )
  },
)
CustomCheckbox.displayName = "CustomCheckbox"
