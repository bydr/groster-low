import { FC, useEffect } from "react"
import { CatalogProducts } from "../Products/Catalog/CatalogProducts"
import { VIEW_PRODUCTS_GRID } from "../../types/types"
import { Typography } from "../Typography/Typography"
import { useAppDispatch, useAppSelector } from "../../hooks/redux"
import { productsAPI } from "../../api/productsAPI"
import { accountSlice } from "../../store/reducers/accountSlice"
import { StyledFavorites } from "./Styled"

export const Favorites: FC = () => {
  const favorites = useAppSelector((state) => state.profile.favorites)
  const dispatch = useAppDispatch()
  const { setFavoritesProducts } = accountSlice.actions
  const { data } = productsAPI.useProductsFetch(
    favorites.keys !== null ? favorites.keys.join(",") : null,
  )

  useEffect(() => {
    if (data !== undefined) {
      dispatch(setFavoritesProducts(data || []))
    }
  }, [data, dispatch, setFavoritesProducts])

  return (
    <>
      <>
        <StyledFavorites>
          {favorites.products.length > 0 ? (
            <CatalogProducts
              view={VIEW_PRODUCTS_GRID}
              products={favorites.products}
              withBanner={false}
            />
          ) : (
            <Typography variant={"h5"}>
              Здесь будут товары, которые вам понравились
            </Typography>
          )}
        </StyledFavorites>
      </>
    </>
  )
}
