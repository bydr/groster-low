import { styled } from "@linaria/react"
import { Paragraph12 } from "../Typography/Typography"
import { colors } from "../../styles/utils/vars"
import {
  PopoverContainer,
  StyledPopoverDisclosure,
  StyledPopoverInner,
} from "../Popover/StyledPopover"
import { StyledList, StyledListItem } from "../List/StyledList"
import { ButtonBase } from "../Button/StyledButton"

export const Title = styled(Paragraph12)`
  margin-bottom: 24px;
  color: ${colors.grayDark};
`

export const StyledNav = styled.nav`
  ${StyledPopoverDisclosure} {
    background-color: transparent;
    padding: 0;
  }

  ${StyledList} {
    ${StyledListItem} {
      color: ${colors.grayDark};

      &:hover,
      &:active {
        color: ${colors.black};
      }
    }
  }

  ${PopoverContainer} {
    margin-bottom: 0;

    ${StyledPopoverDisclosure}, ${StyledPopoverDisclosure}${ButtonBase} {
      padding: 0;
      font-weight: normal;
      font-size: inherit;
      line-height: inherit;
      color: inherit;
    }

    ${StyledPopoverInner} {
      min-width: 220px;
    }

    ${StyledList} {
      ${StyledListItem} {
        margin-right: 0;
        width: 100%;

        &:last-child {
          margin-bottom: 0;
        }
      }
    }
  }
`
