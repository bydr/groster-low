import { ElementType, FC, memo } from "react"
import { cssListVertical, StyledList, StyledListItem } from "../List/StyledList"
import { Link, LinkVariants } from "../Link"
import { NavItemType } from "../../types/types"
import { Icon } from "../Icon"
import { StyledNav } from "./StyledNavigation"
import { cssIsActive } from "../../styles/utils/Utils"
import { Popover } from "../Popover/Popover"
import { Button } from "../Button"

interface NavItemPropsType extends NavItemType {
  as?: ElementType
  isStylingIconDisclosure?: boolean
}

export const NavigationItem: FC<
  NavItemPropsType & { variant?: LinkVariants; activePath?: string }
> = memo(
  ({
    title,
    path,
    subItems = [],
    variant,
    activePath,
    as = "li",
    isScroll,
    isLink,
    isStylingIconDisclosure = true,
  }) => {
    return !subItems.length ? (
      <StyledListItem
        as={as}
        className={
          activePath === path
            ? cssIsActive
            : activePath
                ?.split("/")
                .filter((p, i) => i < 3)
                .join("/")
                .replace("//", "/") === path
            ? cssIsActive
            : ""
        }
      >
        <Link variant={variant} href={path} scroll={isScroll} title={title}>
          {title}
        </Link>
      </StyledListItem>
    ) : (
      <StyledListItem as={as}>
        <Popover
          placement={"bottom-start"}
          withHover
          isStylingIconDisclosure={isStylingIconDisclosure}
          offset={[0, 0]}
          aria-label={title || "popoverNav"}
          disclosure={
            <Button>
              {!!isLink ? (
                <>
                  <Link
                    variant={variant}
                    href={path}
                    scroll={isScroll}
                    title={title}
                  >
                    {title}
                  </Link>
                </>
              ) : (
                <>{title}</>
              )}
              <Icon NameIcon={"AngleBottom"} />
            </Button>
          }
        >
          <StyledNav>
            <StyledList className={cssListVertical}>
              {subItems.map((si, i) => {
                return (
                  <NavigationItem
                    key={i}
                    title={si.title}
                    path={si.path}
                    isScroll={isScroll}
                  />
                )
              })}
            </StyledList>
          </StyledNav>
        </Popover>
      </StyledListItem>
    )
  },
)

NavigationItem.displayName = "NavigationItem"
