import type { FC } from "react"
import { StyledNav, Title } from "./StyledNavigation"
import { NavType } from "../../types/types"
import { cssListVertical, StyledList } from "../List/StyledList"
import { NavigationItem } from "./NavigationItem"
import { LinkVariants } from "../Link"
import { cx } from "@linaria/core"

interface NavigationPropsType extends NavType {
  orientation?: "horizontal" | "vertical" | undefined
}
const Navigation: FC<
  Omit<NavigationPropsType, "title"> & {
    variant?: LinkVariants
    title?: string
    activePath?: string
  }
> = ({
  title,
  items,
  variant = "gray-dark-to-black",
  activePath,
  orientation = "horizontal",
}) => {
  const navItems = items.map((item, index) => (
    <NavigationItem
      variant={variant}
      key={index}
      {...item}
      activePath={activePath}
    />
  ))
  return (
    <StyledNav>
      {!!title && <Title>{title}</Title>}
      <StyledList className={cx(orientation === "vertical" && cssListVertical)}>
        {navItems}
      </StyledList>
    </StyledNav>
  )
}

export default Navigation
