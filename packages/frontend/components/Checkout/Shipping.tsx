import { FC, ReactNode } from "react"
import { PanelHeading } from "../../styles/utils/StyledPanel"
import { FormGroup, FormPanel } from "../Forms/StyledForms"
import { Control, Controller } from "react-hook-form"
import { CustomRadioGroup } from "../Radio/CustomRadioGroup"
import { Typography } from "../Typography/Typography"
import { RadioGroupItemsType, ShippingsType } from "../../types/types"
import { CheckoutFormFieldsType } from "./Checkout"

export type ShippingPropsType = {
  elementsShippingMethods: Record<string, { content: ReactNode }> | null
  isFetchingShippingMethods: boolean
  itemsShippings: (Omit<RadioGroupItemsType, "value"> & {
    value: ShippingsType
  })[]
  control: Control<CheckoutFormFieldsType>
}

export const Shipping: FC<ShippingPropsType> = ({
  elementsShippingMethods,
  isFetchingShippingMethods,
  itemsShippings,
  control,
}) => {
  return (
    <>
      <FormPanel>
        <PanelHeading>Доставка</PanelHeading>
        <FormGroup>
          {itemsShippings.length > 0 ? (
            <>
              <Controller
                name={"shipping_method"}
                control={control}
                rules={{
                  required: {
                    value: true,
                    message: "Выберите метод доставки",
                  },
                }}
                render={({ field, fieldState }) => {
                  return (
                    <>
                      <CustomRadioGroup
                        variant={"rounds"}
                        indexCurrent={itemsShippings.findIndex(
                          ({ value }) => value === field.value,
                        )}
                        items={itemsShippings}
                        itemsElements={
                          elementsShippingMethods !== null
                            ? elementsShippingMethods
                            : undefined
                        }
                        ariaLabel={"shippingMethods"}
                        onChange={field.onChange}
                        isFetching={isFetchingShippingMethods}
                        errorMessage={fieldState.error?.message}
                        ref={field.ref}
                      />
                    </>
                  )
                }}
              />
            </>
          ) : (
            <>
              <Typography variant={"p14"}>
                Методов доставки не найдено
              </Typography>
            </>
          )}
        </FormGroup>
      </FormPanel>
    </>
  )
}
