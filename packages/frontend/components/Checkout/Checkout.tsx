import { FC, useEffect, useState } from "react"
import {
  ContentContainer,
  SidebarContainer,
} from "../../styles/utils/StyledGrid"
import { StickyContainer } from "../../styles/utils/Utils"
import Summary from "../Cart/Summary/Summary"
import { Promocode } from "../Cart/Promocode/Promocode"
import { SingleError, Typography } from "../Typography/Typography"
import { Button } from "../Button"
import { useAuth } from "../../hooks/auth"
import { CheckoutForm } from "./CheckoutForm"
import { useCart } from "../../hooks/cart"
import { DefaultValues } from "react-hook-form"
import { RequestOrderSave } from "../../../contracts/contracts"
import { Replacements } from "./Replacements/Replacements"
import { CustomCheckbox } from "../Checkbox/CustomCheckbox"
import { StyledFieldWrapper } from "../Field/StyledField"
import { BaseLoader } from "../Loaders/BaseLoader/BaseLoader"
import { cx } from "@linaria/core"
import { cssButtonClean } from "../Button/StyledButton"
import { cssInlineLink } from "../Forms/PrivacyAgreement/Styled"
import { AgreeContent } from "../Agree/AgreeContent"
import dynamic, { DynamicOptions } from "next/dynamic"
import type { ModalDefaultPropsType } from "../Modals/Modal"

const DynamicModal = dynamic(
  (() =>
    import("..//Modals/Modal").then(
      (mod) => mod.Modal,
    )) as DynamicOptions<ModalDefaultPropsType>,
  {
    ssr: false,
  },
)

export type CheckoutFormFieldsType = Omit<
  RequestOrderSave,
  "shipping_date" | "replacement"
> & {
  bussiness_area_custom?: string
  payment_method: string
  shipping_address_pickup: string
  shipping_address_courier: string
  shipping_date?: Date
  replacement: number | null
  dataPrivacyAgreement: boolean
}

const SummaryCheckout: FC<{
  errorSavingMsg: string | null
  isNotCall: boolean
  setIsNotCall: (val: boolean) => void
  dataPrivacyAgreement: boolean
  setDataPrivacyAgreement: (val: boolean) => void
  totalCost: number
  setIsTriggerSubmit: (val: boolean) => void
  errorValidateMsg: null | string
  dataPrivacyAgreementErrorMsg?: string
}> = ({
  errorSavingMsg,
  isNotCall,
  setIsNotCall,
  totalCost,
  setIsTriggerSubmit,
  errorValidateMsg,
  setDataPrivacyAgreement,
  dataPrivacyAgreement,
  dataPrivacyAgreementErrorMsg,
}) => {
  const [isShow, setIsShow] = useState<boolean>(false)

  return (
    <>
      <Summary>
        <>
          <StyledFieldWrapper>
            <CustomCheckbox
              variant={"check"}
              message={
                <Typography variant={"p12"}>
                  Не звонить, заказ подтверждаю
                </Typography>
              }
              name={"isCallConfirm"}
              stateCheckbox={isNotCall}
              onChange={(e) => {
                setIsNotCall(e.currentTarget.checked)
              }}
            />
          </StyledFieldWrapper>
          <StyledFieldWrapper>
            <CustomCheckbox
              name={"dataPrivacyAgreement"}
              stateCheckbox={dataPrivacyAgreement}
              onChange={(e) => {
                setDataPrivacyAgreement(e.currentTarget.checked)
              }}
              variant={"check"}
              message={
                <Typography variant={"p12"}>
                  <Typography variant={"span"}>Согласен c</Typography>
                  <DynamicModal
                    isShowModal={isShow}
                    closeMode={"destroy"}
                    variant={"rounded-100"}
                    disclosure={
                      <Button
                        variant={"link"}
                        as={"a"}
                        type={"button"}
                        className={cx(cssButtonClean, cssInlineLink)}
                        onClick={() => {
                          setIsShow(true)
                        }}
                      >
                        {" "}
                        Правилами обработки информации
                      </Button>
                    }
                  >
                    <AgreeContent />
                    <Button
                      type={"button"}
                      variant={"filled"}
                      size={"large"}
                      onClick={(e) => {
                        e.preventDefault()
                        setIsShow(false)
                        setDataPrivacyAgreement(true)
                      }}
                    >
                      Согласен
                    </Button>
                  </DynamicModal>
                </Typography>
              }
              errorMessage={dataPrivacyAgreementErrorMsg}
            />
          </StyledFieldWrapper>

          <Button
            variant={"filled"}
            size={"large"}
            fullWidth
            disabled={totalCost <= 0}
            onClick={() => {
              if (totalCost > 0) {
                setIsTriggerSubmit(true)
              }
            }}
          >
            Оформить заказ
          </Button>
          {errorSavingMsg !== null && errorSavingMsg.length > 0 && (
            <SingleError>{errorSavingMsg}</SingleError>
          )}
          {errorValidateMsg !== null && errorValidateMsg.length > 0 && (
            <SingleError>{errorValidateMsg}</SingleError>
          )}
        </>
      </Summary>
    </>
  )
}

export const Checkout: FC = () => {
  const {
    isAuth,
    isInit,
    showModalAuth,
    user,
    lastOrder,
    customer,
    isFetching,
    isLoadingLastOrder,
  } = useAuth()
  const { totalCost, token, specification, productsFetching } = useCart()
  const [defaultValues, setDefaultValues] =
    useState<DefaultValues<RequestOrderSave> | null>(null)
  const [isTriggerSubmit, setIsTriggerSubmit] = useState<boolean>(false)
  const [currentReplacement, setCurrentReplacement] = useState<number | null>(
    null,
  )
  const [errorValidateMsg, setErrorValidateMsg] = useState<string | null>(null)
  const [errorSavingMsg, setErrorSavingMsg] = useState<string | null>(null)
  const [isNotCall, setIsNotCall] = useState<boolean>(false)
  const [dataPrivacyAgreement, setDataPrivacyAgreement] =
    useState<boolean>(false)
  const [dataPrivacyAgreementErrorMsg, setDataPrivacyAgreementErrorMsg] =
    useState<string | undefined>(undefined)
  const [isSaving, setIsSaving] = useState<boolean>(false)

  useEffect(() => {
    if (isLoadingLastOrder || !isInit) {
      return
    }
    if (lastOrder === null) {
      setDefaultValues({
        email: user?.email || "",
        phone: user?.phone || "",
        fio: user?.fio || "",
      })
    } else {
      const {
        email,
        fio,
        region,
        phone,
        payer,
        payment_method,
        shipping_method,
        shipping_address,
        replacement,
        bussiness_area,
      } = lastOrder
      setDefaultValues({
        email: email || user?.email || undefined,
        fio: fio || user?.fio || undefined,
        phone: phone || user?.phone || undefined,
        region: region || undefined,
        payment_method: payment_method || undefined,
        payer: payer || undefined,
        shipping_method: shipping_method || undefined,
        shipping_address: shipping_address || undefined,
        replacement: replacement || undefined,
        bussiness_area: bussiness_area || undefined,
      })
      if (replacement !== null && replacement !== undefined) {
        setCurrentReplacement(replacement)
      }
    }
  }, [
    isInit,
    isLoadingLastOrder,
    lastOrder,
    user?.email,
    user?.fio,
    user?.phone,
  ])

  return (
    <>
      {(isSaving || specification === null || productsFetching.length > 0) && (
        <BaseLoader isFixed />
      )}
      {isInit && specification !== null && (
        <>
          {Object.keys(specification || {}).length > 0 || isSaving ? (
            <>
              {!isAuth ? (
                <>
                  <ContentContainer
                    style={{
                      minHeight: "35vh",
                    }}
                  >
                    <Typography variant={"p14"}>
                      <Button
                        variant={"link"}
                        onClick={(e) => {
                          e.preventDefault()
                          showModalAuth()
                        }}
                      >
                        Войдите
                      </Button>{" "}
                      по телефону или по почте, чтобы продолжить
                    </Typography>
                  </ContentContainer>
                </>
              ) : (
                <>
                  <ContentContainer>
                    {defaultValues !== null && customer !== null && (
                      <>
                        <CheckoutForm
                          tokenCart={token}
                          defaultValues={defaultValues}
                          customerData={customer}
                          isTriggerSubmit={isTriggerSubmit}
                          setIsTriggerSubmit={setIsTriggerSubmit}
                          isFetching={isFetching}
                          currentReplacement={currentReplacement}
                          setErrorValidateMsg={(value: string | null) => {
                            setErrorValidateMsg(value)
                          }}
                          setErrorSavingMsg={(value: string | null) => {
                            setErrorSavingMsg(value)
                          }}
                          isNotCall={isNotCall}
                          dataPrivacyAgreement={dataPrivacyAgreement}
                          setIsSaving={(value: boolean) => {
                            setIsSaving(value)
                          }}
                          setDataPrivacyAgreementErrorMsg={
                            setDataPrivacyAgreementErrorMsg
                          }
                          setCurrentReplacement={setCurrentReplacement}
                        />
                      </>
                    )}
                  </ContentContainer>
                </>
              )}
              <SidebarContainer>
                {isAuth && (
                  <>
                    <StickyContainer>
                      <Promocode />
                      <SummaryCheckout
                        errorSavingMsg={errorSavingMsg}
                        isNotCall={isNotCall}
                        setIsNotCall={setIsNotCall}
                        dataPrivacyAgreement={dataPrivacyAgreement}
                        setDataPrivacyAgreement={setDataPrivacyAgreement}
                        totalCost={totalCost}
                        setIsTriggerSubmit={setIsTriggerSubmit}
                        errorValidateMsg={errorValidateMsg}
                        dataPrivacyAgreementErrorMsg={
                          dataPrivacyAgreementErrorMsg
                        }
                      />
                      <Replacements
                        onChange={(value) => {
                          setCurrentReplacement(!value ? null : +value)
                        }}
                        initialReplacement={
                          currentReplacement === null
                            ? undefined
                            : currentReplacement
                        }
                      />
                    </StickyContainer>
                  </>
                )}
              </SidebarContainer>
            </>
          ) : (
            <>
              <ContentContainer
                style={{
                  minHeight: "35vh",
                }}
              >
                <Typography variant={"p14"}>Ваша корзина пуста</Typography>
              </ContentContainer>
            </>
          )}
        </>
      )}
    </>
  )
}
