import { FC } from "react"
import {
  Content,
  Description,
  Main,
  Name,
  Side,
  StyledShippingMethod,
} from "./StyledShippingMethod"
import { FrontDeliveryMethod } from "../../../../contracts/src/api/shippingMethods"
import { Typography } from "../../Typography/Typography"
import { Icon } from "../../Icon"

export const ShippingMethodTemplate: FC<
  FrontDeliveryMethod & {
    isActive?: boolean
  }
> = ({ name, cost, is_fast, description, isActive, children }) => {
  return (
    <>
      <StyledShippingMethod>
        <Main>
          {name && (
            <>
              <Name>
                {is_fast && <Icon NameIcon={"Bolt"} />} {name}
              </Name>
            </>
          )}
          {description && (
            <>
              <Description>{description}</Description>
            </>
          )}
        </Main>
        <Side>
          {cost && (
            <>
              <Typography>{cost === "0" ? "бесплатно" : cost}</Typography>
            </>
          )}
        </Side>
        {isActive && children !== undefined && (
          <>
            <Content>{children}</Content>
          </>
        )}
      </StyledShippingMethod>
    </>
  )
}
