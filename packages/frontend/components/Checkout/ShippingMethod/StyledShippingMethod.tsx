import { styled } from "@linaria/react"
import {
  getTypographyBase,
  Paragraph12,
  Paragraph14,
  TypographyBase,
} from "../../Typography/Typography"
import { colors } from "../../../styles/utils/vars"
import { cssIcon } from "../../Icon"
import { css } from "@linaria/core"

export const StyledShippingMethod = styled.div`
  ${getTypographyBase("p14")};
  flex: 1;
  flex-direction: row;
  width: 100%;
  justify-content: space-between;
  display: grid;
  grid-template-columns: 8fr 4fr;
  gap: 14px;
  margin: 0;
`
export const Side = styled.div`
  color: ${colors.black};
  text-align: right;

  ${TypographyBase} {
    margin-bottom: 0;
  }
`
export const Main = styled.div`
  flex: 1;
`

export const Name = styled(Paragraph14)`
  font-weight: 600;
  display: inline-flex;
  align-items: flex-start;
  flex: 1;
  margin: 0;

  .${cssIcon} {
    fill: ${colors.brand.purple};
    margin-top: 4px;
    margin-right: 4px;
  }
`
export const Description = styled(Paragraph12)`
  color: ${colors.grayDark};
  margin: 0;
`
export const Content = styled.div`
  grid-column: 1 / -1;
  &:empty {
    display: none;
  }
`

export const cssShippingCourier = css`
  > *:last-child {
    flex: 0.3;
  }
`
