import {
  RowContentSidebar,
  SidebarContainer,
} from "../../styles/utils/StyledGrid"
import { styled } from "@linaria/react"
import { breakpoints } from "../../styles/utils/vars"

export const CheckoutRow = styled(RowContentSidebar)`
  @media (max-width: ${breakpoints.lg}) {
    grid-template-areas:
      "title"
      "content"
      "sidebar";

    ${SidebarContainer} {
      grid-row: initial;
    }
  }
`
