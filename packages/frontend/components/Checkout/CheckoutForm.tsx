import { FormGroup, FormPanel, StyledForm } from "components/Forms/StyledForms"
import {
  FC,
  memo,
  ReactNode,
  useCallback,
  useEffect,
  useMemo,
  useRef,
  useState,
} from "react"
import type { DefaultValues } from "react-hook-form"
import { Controller, useForm, useFormState } from "react-hook-form"
import {
  ApiError,
  Category,
  RequestOrderSave,
  ResponseCustomerDataList,
} from "../../../contracts/contracts"
import { Field } from "../Field/Field"
import { PanelHeading } from "../../styles/utils/StyledPanel"
import { PhoneField } from "../Field/PhoneField"
import { EMAIL_PATTERN } from "../../validations/email"
import { Select, SelectItemsType } from "../Select/Select"
import { Button } from "../Button"
import { BaseLoader } from "../Loaders/BaseLoader/BaseLoader"
import {
  LocationExtendsType,
  LocationType,
  RadioGroupItemsType,
  ShippingsType,
} from "../../types/types"
import { FullDataForm } from "../Account/Payers/Create/FullDataForm"
import { Typography } from "../Typography/Typography"
import { FrontDeliveryMethod } from "../../../contracts/src/api/shippingMethods"
import { CustomRadioGroup } from "../Radio/CustomRadioGroup"
import { ShippingMethodTemplate } from "./ShippingMethod/ShippingMethodTemplate"
import { useShops } from "../../hooks/shops"
import { cssShippingCourier } from "./ShippingMethod/StyledShippingMethod"
import { AddressesForm } from "../Account/Addresses/AddressesForm"
import { CheckoutFormFieldsType } from "./Checkout"
import { useMutation } from "react-query"
import { fetchSaveOrder, fetchShippingMethods } from "../../api/checkoutAPI"
import { DatepickerField } from "../Field/Datepicker/DatepickerField"
import { useRouter } from "next/router"
import { EMPTY_DATA_PLACEHOLDER, ROUTES } from "../../utils/constants"
import { useCart } from "../../hooks/cart"
import { getClientUser, useAuth } from "../../hooks/auth"
import { usePromocode } from "../../hooks/promocode"
import {
  dateToString,
  getCheckoutFromStorage,
  getLocationFormat,
  getLocationRegion,
  isCurrentYear,
  setCheckoutToStorage,
  setLeadHitStockId,
} from "../../utils/helpers"
import { useApp } from "../../hooks/app"
import { ListMap } from "../Shops/ListMap"
import { useAddresses } from "../../hooks/addresses"
import { usePayers } from "../../hooks/payers"
import { LocationList } from "../LocationList"
import { useShippings } from "../../hooks/shippings"
import { AutoComplete } from "../Field/AutoComplete/AutoComplete"
import NumberFormat from "react-number-format"
import {
  FORMAT_PHONE,
  getPhoneWithCode,
  getPhoneWithOutCode,
  RULES_PHONE_VALIDATE,
} from "../../validations/phone"
import { CreatePayer } from "../Account/Payers/Create"
import dynamic, { DynamicOptions } from "next/dynamic"
import { ShippingPropsType } from "./Shipping"
import { ModalDefaultPropsType } from "../Modals/Modal"
import { useAppSelector } from "../../hooks/redux"

const DynamicShipping = dynamic(
  (() =>
    import("./Shipping").then(
      (mod) => mod.Shipping,
    )) as DynamicOptions<ShippingPropsType>,
  {
    ssr: false,
  },
)

const Modal = dynamic(
  (() =>
    import("../Modals/Modal").then(
      (mod) => mod.Modal,
    )) as DynamicOptions<ModalDefaultPropsType>,
  {
    ssr: false,
  },
)

const BUSINESS_AREA_VALUE_DEFAULT = "Другая сфера"

const COMPARE_FIELDS: Record<
  string,
  "shipping_address_pickup" | "shipping_address_courier" | "shipping_address"
> = {
  courier: "shipping_address_courier",
  pickup: "shipping_address_pickup",
}

const LOCAL_DATA = getCheckoutFromStorage()

const getDefaultValues = (
  defaultValues: DefaultValues<RequestOrderSave> | null,
): CheckoutFormFieldsType => {
  const localData = LOCAL_DATA

  const shippingAddress = localData?.shipping_address || ""
  let shipping_address_courier = ""
  let shipping_address_pickup = ""
  if (shippingAddress.length === 0) {
    if ((defaultValues?.shipping_method || "").length > 0) {
      switch (defaultValues?.shipping_method as ShippingsType) {
        case "courier":
        case "company":
        case "express": {
          shipping_address_courier =
            (localData?.shipping_address_courier || "").length === 0
              ? defaultValues?.shipping_address || ""
              : localData?.shipping_address_courier || ""
          break
        }
        case "pickup": {
          shipping_address_pickup =
            (localData?.shipping_address_pickup || "").length === 0
              ? defaultValues?.shipping_address || ""
              : localData?.shipping_address_pickup || ""
          break
        }
      }
    }
  }

  return {
    fio: localData?.fio || defaultValues?.fio || "",
    bussiness_area:
      !!localData?.bussiness_area && localData?.bussiness_area.length > 0
        ? localData?.bussiness_area
        : !!defaultValues?.bussiness_area &&
          defaultValues?.bussiness_area.length > 0
        ? defaultValues?.bussiness_area
        : BUSINESS_AREA_VALUE_DEFAULT,
    comment: localData?.comment || defaultValues?.comment || "",
    email: localData?.email || defaultValues?.email || "",
    payer: localData?.payer || defaultValues?.payer || "",
    phone:
      getPhoneWithOutCode(
        localData?.phone ||
          (!!defaultValues?.phone ? defaultValues?.phone : ""),
      ) || "",
    region: localData?.region || defaultValues?.region || "",
    replacement: localData?.replacement || defaultValues?.replacement || null,
    shipping_date: !!localData?.shipping_date
      ? new Date(localData?.shipping_date)
      : !!defaultValues?.shipping_date
      ? new Date(defaultValues?.shipping_date)
      : undefined,
    shipping_method:
      localData?.shipping_method || defaultValues?.shipping_method || "",
    payment_method:
      localData?.payment_method || defaultValues?.payment_method || "",
    bussiness_area_custom: localData?.bussiness_area_custom || "",
    shipping_address: shippingAddress,
    shipping_address_courier: shipping_address_courier,
    shipping_address_pickup: shipping_address_pickup,
    not_call: false,
    dataPrivacyAgreement: false,
  }
}

const onSelectBusinessAreaHandle = (
  businessAreas: Category[] | null,
  name: string,
) => {
  const foundId = (businessAreas || []).find((ba) => ba.name === name)?.uuid
  if (foundId !== undefined) {
    setLeadHitStockId(foundId)
  }
}

export const CheckoutForm: FC<{
  tokenCart: string | null
  defaultValues: DefaultValues<RequestOrderSave> | null
  isTriggerSubmit?: boolean
  setIsTriggerSubmit?: (isTriggerSubmit: boolean) => void
  customerData?: ResponseCustomerDataList
  isFetching?: boolean
  currentReplacement: number | null
  setErrorValidateMsg: (value: string | null) => void
  setErrorSavingMsg: (value: string | null) => void
  setIsSaving: (value: boolean) => void
  isNotCall: boolean
  dataPrivacyAgreement: boolean
  setDataPrivacyAgreementErrorMsg: (val?: string) => void
  setCurrentReplacement: (id: number | null) => void
}> = memo(
  ({
    defaultValues,
    customerData,
    isTriggerSubmit,
    setIsTriggerSubmit,
    isFetching,
    currentReplacement = null,
    setErrorValidateMsg,
    setErrorSavingMsg,
    isNotCall,
    setIsSaving,
    tokenCart,
    dataPrivacyAgreement,
    setDataPrivacyAgreementErrorMsg,
    setCurrentReplacement,
  }) => {
    const businessAreas = useAppSelector((state) => state.catalog.businessAreas)
    const {
      token,
      updateToken,
      setOrder,
      cartCost,
      totalCost,
      nextShippingDate,
      pickupDate,
    } = useCart()
    const {
      shippingCost,
      refetchShippingCost,
      paymentMethods,
      isFetchingShippingCost,
    } = useShippings()
    const { discount, promocode } = usePromocode({
      cart: token,
    })

    const { updateUser, user } = useAuth()
    const [itemsAreas, setItemsAreas] = useState<SelectItemsType[]>([])
    const [itemsEmails, setItemsEmails] = useState<SelectItemsType[]>([])
    const [itemsPhones, setItemsPhones] = useState<SelectItemsType[]>([])
    const [itemsPayers, setItemsPayers] = useState<SelectItemsType[]>([])
    const [itemsAddresses, setItemsAddresses] = useState<SelectItemsType[]>([])
    const [itemsShopsAddresses, setItemsShopsAddresses] = useState<
      SelectItemsType[]
    >([])
    const [itemsShippings, setItemsShippings] = useState<
      (Omit<RadioGroupItemsType, "value"> & { value: ShippingsType })[]
    >([])

    const [itemsPayments, setItemsPayments] = useState<RadioGroupItemsType[]>(
      [],
    )

    const [checkoutLocation, setCheckoutLocation] =
      useState<LocationExtendsType | null>(null)
    const isInitLocation = useRef<boolean>(false)

    const { location, settings } = useApp()

    const refSubmit = useRef<HTMLButtonElement>(null)

    const defaultValuesMemoized = useMemo(() => {
      return getDefaultValues(defaultValues)
    }, [defaultValues])

    const {
      handleSubmit,
      control,
      register,
      setValue,
      watch,
      getValues,
      clearErrors,
    } = useForm<CheckoutFormFieldsType>({
      defaultValues: defaultValuesMemoized,
      mode: "onChange",
    })

    const { errors, isValidating, isSubmitted } = useFormState({
      control,
    })

    const watchAll = watch()
    const watchBusinessArea = watch("bussiness_area")
    const watchPayer = watch("payer")
    const watchShippingMethod: ShippingsType | undefined = watch(
      "shipping_method",
    ) as ShippingsType | undefined
    const watchShippingDate = watch("shipping_date")

    const [isBusinessAreaCustom, setIsBusinessAreaCustom] =
      useState<boolean>(false)
    const [elementsShippingMethods, setElementsShippingMethods] =
      useState<Record<string, { content: ReactNode }> | null>(null)

    const [methodsShippingData, setMethodsShippingData] = useState<
      (Omit<FrontDeliveryMethod, "alias"> & { alias?: ShippingsType })[]
    >([])
    const [emailViewMode, setEmailViewMode] = useState<"input" | "select">(
      "input",
    )
    const [phoneViewMode, setPhoneViewMode] = useState<"input" | "select">(
      "input",
    )
    const router = useRouter()
    const [isShowModal, setIsShowModal] = useState(false)
    const {
      shops,
      isFetching: reqShopsIsFetching,
      requestShops,
    } = useShops({
      selectedShopId: watchAll.shipping_address_pickup || undefined,
    })

    const {
      isFetching: isFetchingAddress,
      addresses,
      refetch: refetchAddresses,
    } = useAddresses()

    const {
      isFetching: isFetchingPayers,
      payers,
      refetch: refetchPayers,
    } = usePayers()

    const {
      mutate: shippingMethodsMutate,
      isLoading: isFetchingShippingMethods,
    } = useMutation(fetchShippingMethods, {
      onSuccess: (response) => {
        if (!!response) {
          setItemsShippings(
            response
              .sort((a, b) => {
                if ((a.weight || 0) > (b.weight || 0)) {
                  return 1
                }
                if ((a.weight || 0) < (b.weight || 0)) {
                  return -1
                }
                return 0
              })
              .map((el: FrontDeliveryMethod) => {
                return {
                  value: (el.alias || "") as ShippingsType,
                  message: el.name || "",
                }
              }),
          )

          setMethodsShippingData(
            response as (Omit<FrontDeliveryMethod, "alias"> & {
              alias?: ShippingsType
            })[],
          )
        } else {
          setItemsShippings([])
          setMethodsShippingData([])
        }
      },
    })

    const getAddressForm = useCallback(() => {
      let shippingAddress = ""
      const shippingMethod = getValues("shipping_method")

      switch (shippingMethod as ShippingsType) {
        case "courier":
        case "company":
        case "express": {
          const foundAddress =
            addresses !== null
              ? addresses?.find(
                  (addr) => addr.uid === getValues("shipping_address_courier"),
                ) || null
              : null
          shippingAddress =
            foundAddress !== null
              ? `${foundAddress.name}, ${foundAddress.address}`
              : ""
          break
        }
        case "pickup": {
          shippingAddress = !!shops
            ? shops.find((s) => s.uuid === getValues("shipping_address_pickup"))
                ?.address || ""
            : ""
          break
        }
      }

      return shippingAddress
    }, [addresses, getValues, shops])

    const { mutate: orderSaveMutate } = useMutation(fetchSaveOrder, {
      onMutate: () => {
        setIsSaving(true)
        setErrorSavingMsg(null)
      },
      onSuccess: (response) => {
        const user = getClientUser()
        if (user !== null) {
          updateUser({
            ...user,
            cart: null,
          })
        }

        updateToken(null)
        setCheckoutToStorage(null)

        const shippingAddress = getAddressForm()
        const shippingDate = getValues("shipping_date")

        setOrder({
          fio: getValues("fio"),
          shippingCost: shippingCost || 0,
          shippingMethod:
            itemsShippings.find((s) => s.value === getValues("shipping_method"))
              ?.message || "",
          phone: getValues("phone") || "",
          discount: discount,
          promocode: promocode,
          totalCost: totalCost,
          productsCost: cartCost,
          id: response?.number || "",
          shippingDate:
            shippingDate !== undefined ? dateToString(shippingDate) : "",
          shippingAddress: shippingAddress,
        })

        void router.push(ROUTES.thank)
      },
      onError: (error: ApiError) => {
        if (error.status === 404) {
          if (user !== null) {
            updateUser({ ...user, cart: null })
          }
          updateToken(null)
        }
        setErrorSavingMsg(error.message)
        setIsSaving(false)
      },
    })

    const methodMultiElement: ReactNode = useMemo(
      () => (
        <>
          <FormGroup className={cssShippingCourier}>
            <Controller
              control={control}
              rules={{
                required: {
                  value:
                    watchShippingMethod === "courier" ||
                    watchShippingMethod === "company" ||
                    watchShippingMethod === "express",
                  message: "Поле обязательно для заполнения",
                },
              }}
              name={"shipping_address_courier"}
              render={({ field, fieldState }) => {
                return (
                  <>
                    <Select
                      ariaLabel={"Адрес"}
                      initialValue={`${field.value}`}
                      items={itemsAddresses}
                      variant={"default"}
                      onSelectValue={(value) => {
                        field.onChange(value)
                      }}
                      isFetching={isFetchingAddress}
                      errorMessage={fieldState.error?.message}
                      isVisibleSelectedHint
                      ref={field.ref}
                      required={
                        watchShippingMethod === "courier" ||
                        watchShippingMethod === "company" ||
                        watchShippingMethod === "express"
                      }
                      buttonAddedItem={
                        <>
                          <Modal
                            closeMode={"destroy"}
                            variant={"rounded-0"}
                            title={"Добавить адрес"}
                            disclosure={
                              <Button
                                variant={"link"}
                                type={"button"}
                                icon={"UserAdd"}
                              >
                                Добавить адрес
                              </Button>
                            }
                          >
                            <AddressesForm
                              onSuccess={(uid) => {
                                refetchAddresses()
                                setValue("shipping_address_courier", uid, {
                                  shouldDirty: true,
                                  shouldTouch: true,
                                })
                              }}
                            />
                          </Modal>
                        </>
                      }
                    />
                  </>
                )
              }}
            />
            <Controller
              control={control}
              name={"shipping_date"}
              rules={{
                required: {
                  value: true,
                  message: "Поле обязательно для заполнения",
                },
              }}
              render={({ field, fieldState }) => {
                return (
                  <>
                    <DatepickerField
                      required={true}
                      value={field.value}
                      onDayChange={(day) => {
                        field.onChange(day)
                      }}
                      placeholder={"Выберите дату"}
                      errorMessage={fieldState.error?.message}
                      ref={field.ref}
                      disabledDays={
                        nextShippingDate !== null
                          ? [
                              {
                                before: nextShippingDate,
                              },
                              ...(settings?.holidays.map((h) => new Date(h)) ||
                                []),
                            ]
                          : undefined
                      }
                    />
                  </>
                )
              }}
            />
          </FormGroup>
        </>
      ),
      [
        control,
        watchShippingMethod,
        itemsAddresses,
        isFetchingAddress,
        refetchAddresses,
        setValue,
        nextShippingDate,
        settings?.holidays,
      ],
    )

    const methodPickupElement: ReactNode = useMemo(
      () => (
        <>
          <FormGroup>
            <Controller
              control={control}
              name={"shipping_address_pickup"}
              rules={{
                required: {
                  value: watchShippingMethod === "pickup",
                  message: "Поле обязательно для заполнения",
                },
              }}
              render={({ field, fieldState }) => {
                return (
                  <>
                    <Select
                      required={watchShippingMethod === "pickup"}
                      ariaLabel={"Выберите магазин"}
                      initialValue={`${field.value}`}
                      items={itemsShopsAddresses}
                      variant={"default"}
                      onSelectValue={(value) => {
                        field.onChange(value)
                      }}
                      isFetching={reqShopsIsFetching}
                      errorMessage={fieldState.error?.message}
                      isVisibleSelectedHint
                    />
                  </>
                )
              }}
            />
          </FormGroup>
          <Typography>
            <Modal
              title={"Пункты самовывоза на карте"}
              variant={"rounded-100"}
              closeMode={"destroy"}
              disclosure={
                <Button
                  variant={"link"}
                  type={"button"}
                  onClick={() => {
                    setIsShowModal(true)
                  }}
                >
                  Показать на карте
                </Button>
              }
              isShowModal={isShowModal}
              onClose={() => {
                setIsShowModal(false)
              }}
            >
              <ListMap
                enableSelectShop={true}
                onSelectShop={(id) => {
                  setValue("shipping_address_pickup", id, {
                    shouldDirty: true,
                    shouldValidate: true,
                  })
                }}
                shopBody={
                  pickupDate !== null && (
                    <>
                      <Typography variant={"p12"}>
                        Можно забрать после:{" "}
                        {dateToString(pickupDate, !isCurrentYear(pickupDate))}
                      </Typography>
                    </>
                  )
                }
              />
            </Modal>
          </Typography>
        </>
      ),
      [
        control,
        reqShopsIsFetching,
        watchShippingMethod,
        isShowModal,
        itemsShopsAddresses,
        pickupDate,
        setValue,
      ],
    )

    const onSubmit = handleSubmit((data) => {
      if (!dataPrivacyAgreement) {
        setDataPrivacyAgreementErrorMsg(
          "Подтвердите согласие на обработку персональных данных",
        )
        return
      } else {
        setDataPrivacyAgreementErrorMsg()
      }

      let shippingAddress = getValues("shipping_address") || ""
      let shippingDate =
        data.shipping_date !== undefined ? data.shipping_date.toISOString() : ""

      if (getValues("shipping_method").length > 0) {
        switch (getValues("shipping_method") as ShippingsType) {
          case "pickup": {
            shippingAddress = getValues("shipping_address_pickup")
            shippingDate = ""
            break
          }
          case "courier":
          case "company":
          case "express": {
            shippingAddress = getValues("shipping_address_courier")
            break
          }
        }
      }

      const formatData: Omit<typeof data, "shipping_date" | "phone"> & {
        shipping_date: string
        phone: string
      } = {
        ...data,
        shipping_address: shippingAddress,
        bussiness_area: isBusinessAreaCustom
          ? data.bussiness_area_custom
          : data.bussiness_area,
        not_call: isNotCall,
        shipping_date: shippingDate,
        phone: !!data.phone ? getPhoneWithCode(data.phone) : "",
      }
      if (token !== null) {
        orderSaveMutate({
          cart: token,
          phone: formatData.phone,
          fio: formatData.fio,
          email: formatData.email,
          bussiness_area: formatData.bussiness_area,
          shipping_method: formatData.shipping_method,
          shipping_date: formatData.shipping_date,
          payer: formatData.payer,
          shipping_address: formatData.shipping_address,
          region: formatData.region,
          replacement: formatData.replacement,
          comment: formatData.comment,
          not_call: formatData.not_call,
          payment_method: formatData.payment_method,
        })
      }
    })

    const refetchShippingCostHandle = useCallback(() => {
      if (watchShippingMethod !== undefined) {
        refetchShippingCost(
          watchShippingMethod,
          methodsShippingData.find((m) => m.alias === watchShippingMethod)
            ?.region || "",
          tokenCart,
          watchShippingDate,
        )
      }
    }, [
      methodsShippingData,
      refetchShippingCost,
      tokenCart,
      watchShippingMethod,
      watchShippingDate,
    ])

    const updateCheckoutLocation = useCallback(
      (location: LocationType | null) => {
        const formattedLocation = getLocationFormat(location)

        setCheckoutLocation(formattedLocation)

        shippingMethodsMutate({
          regions: getLocationRegion(formattedLocation) || "",
        })
        setValue("region", formattedLocation?.city_full || "", {
          shouldDirty: true,
        })

        if (isInitLocation.current) {
          setValue("shipping_date", undefined)
          setValue("shipping_address", "")
          setValue("shipping_method", "")
          setValue("shipping_address_pickup", "")
          setValue("shipping_address_courier", "")
        }

        isInitLocation.current = true
      },
      [setValue, shippingMethodsMutate],
    )

    const initCheckoutLocation = useCallback(() => {
      updateCheckoutLocation(LOCAL_DATA?.location || location || null)
    }, [location, updateCheckoutLocation])

    useEffect(() => {
      if (
        defaultValuesMemoized?.replacement !== null &&
        defaultValuesMemoized?.replacement !== undefined
      ) {
        setCurrentReplacement(defaultValuesMemoized.replacement)
      }
    }, [defaultValuesMemoized.replacement, setCurrentReplacement])

    useEffect(() => {
      if (dataPrivacyAgreement) {
        setDataPrivacyAgreementErrorMsg()
      }
    }, [dataPrivacyAgreement])

    useEffect(() => {
      initCheckoutLocation()
    }, [initCheckoutLocation])

    useEffect(() => {
      if (watchAll !== null) {
        setCheckoutToStorage({
          fio: watchAll.fio,
          email: watchAll.email,
          phone: watchAll.phone,
          payer: watchAll.payer,
          shipping_address: watchAll.shipping_address,
          region: watchAll.region,
          bussiness_area: watchAll.bussiness_area,
          shipping_method: watchAll.shipping_method,
          bussiness_area_custom: watchAll.bussiness_area_custom,
          payment_method: watchAll.payment_method,
          shipping_address_courier: watchAll.shipping_address_courier,
          shipping_address_pickup: watchAll.shipping_address_pickup,
          location: checkoutLocation,
          replacement: watchAll.replacement,
        })
      }
    }, [watchAll, checkoutLocation])

    // обновляем стоимость доставки
    // на основе выбранного пункта доставки
    useEffect(() => {
      refetchShippingCostHandle()
    }, [refetchShippingCostHandle])

    useEffect(() => {
      if (addresses !== null) {
        let defaultItem: string | undefined
        const makedItems = addresses.map((addr) => {
          if (addr.is_default) {
            defaultItem = addr.uid
          }
          return {
            name: addr.name || "",
            value: addr.uid || "",
            layout: (
              <>
                <Typography variant={"span"}>
                  <Typography>{addr.name}</Typography>
                  <Typography variant={"p12"}>{addr.address}</Typography>
                </Typography>
              </>
            ),
          }
        })

        if (!getValues("shipping_address_courier") && !!defaultItem) {
          setValue("shipping_address_courier", defaultItem)
        }

        setItemsAddresses(makedItems)
      } else {
        setItemsAddresses([])
      }
    }, [addresses, getValues, setValue])

    useEffect(() => {
      if (
        (defaultValuesMemoized.shipping_address_courier || "").length === 0 ||
        addresses === null
      ) {
        return
      }

      const uid = addresses.find(
        (addr) =>
          addr.address === defaultValuesMemoized.shipping_address_courier,
      )?.uid

      if (uid !== undefined) {
        setValue("shipping_address_courier", uid)
      }
    }, [addresses, defaultValuesMemoized.shipping_address_courier, setValue])

    //построение элементов списка самовывоза
    useEffect(() => {
      if (shops !== null) {
        setItemsShopsAddresses(
          shops.map((shop) => {
            return {
              name: shop.address || EMPTY_DATA_PLACEHOLDER,
              value: shop.uuid || "",
              layout: (
                <>
                  <Typography variant={"span"}>
                    <Typography>
                      {shop.address || EMPTY_DATA_PLACEHOLDER}
                    </Typography>
                    {pickupDate !== null && (
                      <>
                        <Typography variant={"p12"}>
                          Можно забрать после:{" "}
                          {dateToString(pickupDate, !isCurrentYear(pickupDate))}
                        </Typography>
                      </>
                    )}
                  </Typography>
                </>
              ),
            }
          }),
        )
      } else {
        setItemsShopsAddresses([])
      }
    }, [nextShippingDate, shops, pickupDate])

    //построение элементов списка плательщиков
    useEffect(() => {
      if (payers !== null) {
        setItemsPayers(
          payers.map((item, i) => {
            return {
              name: item.name || "",
              value: item.uid || `${i}`,
            }
          }),
        )
      } else {
        setItemsPayers([])
      }
    }, [payers])

    useEffect(() => {
      refetchPayers()
      refetchAddresses()
    }, [refetchPayers, refetchAddresses])

    useEffect(() => {
      setEmailViewMode(itemsEmails.length <= 1 ? "input" : "select")
    }, [itemsEmails])

    useEffect(() => {
      const localPhone = LOCAL_DATA?.phone
      if (
        itemsPhones.length <= 1 ||
        (!!localPhone && !itemsPhones.find((p) => p.value === localPhone))
      ) {
        setPhoneViewMode("input")
      } else {
        setPhoneViewMode("select")
      }
    }, [itemsPhones])

    useEffect(() => {
      const methods: Record<string, { content: ReactNode }> = {}
      methodsShippingData
        .sort((a, b) => {
          if ((a.weight || 0) > (b.weight || 0)) {
            return 1
          }
          if ((a.weight || 0) < (b.weight || 0)) {
            return -1
          }
          return 0
        })
        .map((method) => {
          methods[method.alias || ""] = {
            content: (
              <ShippingMethodTemplate
                {...method}
                isActive={watchShippingMethod === (method.alias || "")}
              >
                {method.alias === "pickup" && methodPickupElement}
                {method.alias === "courier" && methodMultiElement}
                {(method.alias === "express" || method.alias === "company") &&
                  methodMultiElement}
              </ShippingMethodTemplate>
            ),
          }
        })
      setElementsShippingMethods(methods)
    }, [
      methodMultiElement,
      methodPickupElement,
      methodsShippingData,
      watchShippingMethod,
    ])

    useEffect(() => {
      if (paymentMethods !== null) {
        setItemsPayments(
          [...paymentMethods]
            .sort((a, b) => {
              const valA = a.weight || 0
              const valB = b.weight || 0
              return valA > valB ? -1 : valA < valB ? 1 : 0
            })
            .map((p) => ({
              value: p.uid,
              message: p.name || EMPTY_DATA_PLACEHOLDER,
            })),
        )
      } else {
        setItemsPayments([])
      }
    }, [paymentMethods])

    useEffect(() => {
      if (customerData?.areas !== undefined) {
        setItemsAreas([
          {
            name: BUSINESS_AREA_VALUE_DEFAULT,
            value: BUSINESS_AREA_VALUE_DEFAULT,
          },
          ...customerData?.areas
            .reduce((uniq: string[], item) => {
              return uniq.includes(item) ? uniq : [...uniq, item]
            }, [])
            .map((name) => {
              return {
                name: name,
                value: name,
              }
            }),
        ])
      } else {
        setItemsAreas([])
      }

      if (customerData?.phones !== undefined) {
        setItemsPhones(
          customerData?.phones.map((p) => {
            return {
              name: p,
              value: getPhoneWithOutCode(p) || "",
              layout: (
                <NumberFormat
                  format={FORMAT_PHONE}
                  mask="_"
                  displayType={"text"}
                  value={getPhoneWithOutCode(p) || ""}
                />
              ),
            }
          }),
        )
      }

      if (customerData?.emails !== undefined) {
        setItemsEmails(
          customerData?.emails
            .reduce((uniq: string[], item) => {
              return uniq.includes(item) ? uniq : [...uniq, item]
            }, [])
            .map((e) => {
              return {
                name: e,
                value: e,
              }
            }),
        )
      } else {
        setItemsEmails([])
      }
    }, [customerData])

    useEffect(() => {
      if (isTriggerSubmit) {
        refSubmit?.current?.click()

        if (setIsTriggerSubmit) {
          setIsTriggerSubmit(false)
        }
      }
    }, [isTriggerSubmit, setIsTriggerSubmit])

    useEffect(() => {
      setIsBusinessAreaCustom(watchBusinessArea === BUSINESS_AREA_VALUE_DEFAULT)
    }, [watchBusinessArea])

    useEffect(() => {
      if (watchShippingMethod === "pickup" && shops === null) {
        requestShops()
      }
    }, [requestShops, shops, watchShippingMethod])

    useEffect(() => {
      if (!!watchShippingMethod) {
        const forClearArr = Object.keys(COMPARE_FIELDS)
          .filter((key) => key !== watchShippingMethod)
          .map((k) => COMPARE_FIELDS[k])
        clearErrors(forClearArr)
      }
    }, [watchShippingMethod, errors, clearErrors])

    useEffect(() => {
      if (currentReplacement !== null) {
        setValue("replacement", currentReplacement)
      }
    }, [currentReplacement, setValue])

    useEffect(() => {
      setErrorValidateMsg(
        Object.keys(errors).length > 0
          ? "Исправьте ошибки в форме чтобы продолжить"
          : null,
      )
    }, [errors, isSubmitted, isValidating])

    return (
      <>
        {isFetching ? (
          <BaseLoader />
        ) : (
          <>
            <StyledForm onSubmit={onSubmit}>
              {!isFetching && (
                <>
                  <FormPanel>
                    <PanelHeading>Личные данные</PanelHeading>
                    <Controller
                      name={"fio"}
                      control={control}
                      rules={{
                        required: {
                          value: true,
                          message: "Поле обязательно для заполнения",
                        },
                      }}
                      render={({ field, fieldState }) => (
                        <Field
                          type={"text"}
                          value={field.value}
                          placeholder={"Фамилия, Имя и Отчество"}
                          onChange={field.onChange}
                          errorMessage={fieldState.error?.message}
                          withAnimatingLabel
                          ref={field.ref}
                          required={true}
                        />
                      )}
                    />
                    <FormGroup>
                      {phoneViewMode === "select" ? (
                        <>
                          <Controller
                            rules={RULES_PHONE_VALIDATE}
                            render={({ field, fieldState }) => {
                              return (
                                <Select
                                  ariaLabel={"Телефон"}
                                  initialValue={getPhoneWithOutCode(
                                    field.value,
                                  )}
                                  items={itemsPhones}
                                  variant={"default"}
                                  withButton={true}
                                  iconButton={"Plus"}
                                  onClickButton={() => {
                                    setPhoneViewMode("input")
                                    setValue("phone", "", {
                                      shouldValidate: true,
                                      shouldTouch: true,
                                      shouldDirty: true,
                                    })
                                  }}
                                  isVisibleLayout
                                  onSelectValue={(value) => {
                                    setValue("phone", value, {
                                      shouldDirty: true,
                                      shouldValidate: true,
                                    })
                                  }}
                                  errorMessage={fieldState.error?.message}
                                  ref={field.ref}
                                  required={true}
                                />
                              )
                            }}
                            name={"phone"}
                            control={control}
                          />
                        </>
                      ) : (
                        <>
                          <Controller
                            rules={RULES_PHONE_VALIDATE}
                            render={({ field, fieldState }) => {
                              return (
                                <PhoneField
                                  withButton={itemsPhones.length > 1}
                                  iconButton={"ListBullet"}
                                  onClickButton={() => {
                                    setPhoneViewMode("select")
                                    setValue("phone", "", {
                                      shouldValidate: true,
                                      shouldTouch: true,
                                      shouldDirty: true,
                                    })
                                  }}
                                  value={field.value}
                                  onValueChange={({ value }) => {
                                    field.onChange(value)
                                  }}
                                  errorMessage={fieldState?.error?.message}
                                  required
                                  ref={field.ref}
                                />
                              )
                            }}
                            control={control}
                            name={"phone"}
                          />
                        </>
                      )}

                      {emailViewMode === "select" ? (
                        <Controller
                          control={control}
                          rules={{
                            required: {
                              value: true,
                              message: "Поле обязательно для заполнения",
                            },
                            pattern: {
                              value: EMAIL_PATTERN,
                              message: "Неверный формат email",
                            },
                          }}
                          name={"email"}
                          render={({ field, fieldState }) => {
                            return (
                              <Select
                                ariaLabel={"Ранее использованные email"}
                                initialValue={field.value}
                                items={itemsEmails}
                                variant={"default"}
                                withButton={true}
                                iconButton={"Plus"}
                                onClickButton={() => {
                                  setEmailViewMode("input")
                                  field.onChange("")
                                }}
                                onSelectValue={(value) => {
                                  field.onChange(value)
                                }}
                                required={true}
                                errorMessage={fieldState.error?.message}
                                ref={field.ref}
                              />
                            )
                          }}
                        />
                      ) : (
                        <Controller
                          name={"email"}
                          control={control}
                          rules={{
                            required: {
                              value: true,
                              message: "Поле обязательно для заполнения",
                            },
                            pattern: {
                              value: EMAIL_PATTERN,
                              message: "Неверный формат email",
                            },
                          }}
                          render={({ field, fieldState }) => {
                            return (
                              <Field
                                placeholder={"Email"}
                                withAnimatingLabel
                                type={"email"}
                                withButton={itemsEmails.length > 1}
                                iconButton={"ListBullet"}
                                onClickButton={() => {
                                  setEmailViewMode("select")
                                  field.onChange("")
                                }}
                                onChange={field.onChange}
                                value={field.value}
                                required={true}
                                errorMessage={fieldState.error?.message}
                                ref={field.ref}
                              />
                            )
                          }}
                        />
                      )}
                    </FormGroup>

                    <FormGroup>
                      <Controller
                        name={"region"}
                        control={control}
                        rules={{
                          required: {
                            value: true,
                            message: "Поле обязательно для заполнения",
                          },
                        }}
                        render={({ field, fieldState }) => (
                          <Modal
                            closeMode={"destroy"}
                            title={"Выберите свой город"}
                            variant={"rounded-50"}
                            disclosure={
                              <Field
                                type={"text"}
                                value={field.value}
                                onChange={field.onChange}
                                placeholder={"Населённый пункт"}
                                errorMessage={fieldState.error?.message}
                                withAnimatingLabel
                                readOnly
                                iconField={"NearMe"}
                                style={{
                                  cursor: "pointer",
                                }}
                                ref={field.ref}
                              />
                            }
                          >
                            <LocationList
                              currentLocation={checkoutLocation || undefined}
                              onSelectLocation={(location) => {
                                updateCheckoutLocation(location)
                              }}
                            />
                          </Modal>
                        )}
                      />
                    </FormGroup>
                    <FormGroup>
                      {itemsAreas.length > 0 ? (
                        <>
                          <Controller
                            render={({ field }) => {
                              return (
                                <Select
                                  ariaLabel={"Сфера бизнеса"}
                                  initialValue={field.value}
                                  items={itemsAreas}
                                  variant={"default"}
                                  onSelectValue={(value) => {
                                    field.onChange(value)
                                    onSelectBusinessAreaHandle(
                                      businessAreas,
                                      value,
                                    )
                                  }}
                                  ref={field.ref}
                                />
                              )
                            }}
                            control={control}
                            name={"bussiness_area"}
                          />
                        </>
                      ) : (
                        <>
                          <Field
                            placeholder={"Ваша сфера бизнеса"}
                            withAnimatingLabel
                            errorMessage={errors?.bussiness_area?.message}
                            {...register("bussiness_area", {
                              required: {
                                value: true,
                                message: "Поле обязательно для заполнения",
                              },
                            })}
                            required={true}
                          />
                        </>
                      )}
                    </FormGroup>
                    {isBusinessAreaCustom && (
                      <>
                        <FormGroup>
                          <Controller
                            rules={{
                              required: {
                                value: true,
                                message: "Поле обязательно для заполнения",
                              },
                              validate: (value) => {
                                return (value || "").length <= 0
                                  ? "Заполните поле"
                                  : true
                              },
                            }}
                            render={({ field, fieldState }) => {
                              return (
                                <AutoComplete
                                  ariaLabel={"Ваша сфера бизнеса"}
                                  initialValue={field.value}
                                  items={itemsAreas.filter(
                                    (a) =>
                                      a.value !== BUSINESS_AREA_VALUE_DEFAULT,
                                  )}
                                  onSelectValue={(value) => {
                                    field.onChange(value)
                                    onSelectBusinessAreaHandle(
                                      businessAreas,
                                      value,
                                    )
                                  }}
                                  errorMessage={fieldState.error?.message}
                                  ref={field.ref}
                                  required={true}
                                />
                              )
                            }}
                            control={control}
                            name={"bussiness_area_custom"}
                          />
                        </FormGroup>
                      </>
                    )}
                  </FormPanel>
                </>
              )}

              <FormPanel>
                <PanelHeading>
                  <Typography variant={"span"}>Плательщик</Typography>{" "}
                  {!!watchPayer && (
                    <>
                      <Modal
                        closeMode={"destroy"}
                        variant={"rounded-50"}
                        title={"Данные плательщика"}
                        disclosure={<Button variant={"box"} icon={"Edit"} />}
                      >
                        <FullDataForm
                          onSuccess={(uid) => {
                            refetchPayers()
                            setValue("payer", uid, {
                              shouldDirty: true,
                            })
                          }}
                          payer={
                            payers !== null
                              ? payers.find((p) => p.uid === watchPayer)
                              : undefined
                          }
                        />
                      </Modal>
                    </>
                  )}
                </PanelHeading>
                <FormGroup>
                  <Controller
                    control={control}
                    name={"payer"}
                    render={({ field }) => {
                      return (
                        <>
                          <Select
                            ariaLabel={"Выберите плательщика"}
                            initialValue={`${field.value}`}
                            items={itemsPayers}
                            variant={"default"}
                            isFetching={isFetchingPayers}
                            ref={field.ref}
                            buttonAddedItem={
                              <>
                                <Modal
                                  closeMode={"destroy"}
                                  variant={"rounded-50"}
                                  title={"Добавить плательщика"}
                                  disclosure={
                                    <Button
                                      variant={"link"}
                                      type={"button"}
                                      icon={"UserAdd"}
                                    >
                                      Добавить плательщика
                                    </Button>
                                  }
                                >
                                  <CreatePayer
                                    onSuccess={(uid) => {
                                      refetchPayers()
                                      setValue("payer", uid, {
                                        shouldDirty: true,
                                      })
                                    }}
                                  />
                                </Modal>
                              </>
                            }
                            onSelectValue={(value) => {
                              field.onChange(value)
                            }}
                          />
                        </>
                      )
                    }}
                  />
                </FormGroup>
              </FormPanel>

              <DynamicShipping
                elementsShippingMethods={elementsShippingMethods}
                control={control}
                isFetchingShippingMethods={isFetchingShippingMethods}
                itemsShippings={itemsShippings}
              />

              <FormPanel>
                <PanelHeading>Оплата</PanelHeading>
                <FormGroup>
                  {paymentMethods !== null ? (
                    <>
                      {paymentMethods.length > 0 ? (
                        <Controller
                          name={"payment_method"}
                          rules={{
                            required: {
                              value: true,
                              message: "Выберите метод оплаты",
                            },
                          }}
                          control={control}
                          render={({ field, fieldState }) => {
                            return (
                              <CustomRadioGroup
                                indexCurrent={itemsPayments.findIndex(
                                  ({ value }) => value === field.value,
                                )}
                                variant={"rounds"}
                                items={itemsPayments}
                                ariaLabel={"paymentMethod"}
                                onChange={field.onChange}
                                isFetching={isFetchingShippingCost}
                                ref={field.ref}
                                errorMessage={fieldState.error?.message}
                              />
                            )
                          }}
                        />
                      ) : (
                        <>
                          <Typography variant={"p14"}>
                            Методов оплаты не найдено
                          </Typography>
                        </>
                      )}
                    </>
                  ) : (
                    <>
                      <Typography variant={"p14"}>
                        Выберите метод доставки, чтобы мы могли подобрать для
                        Вас подходящие методы оплаты
                      </Typography>
                    </>
                  )}
                </FormGroup>
              </FormPanel>

              <FormPanel>
                <FormGroup>
                  <Controller
                    name={"comment"}
                    control={control}
                    render={({ field, fieldState }) => (
                      <Field
                        variant={"textarea"}
                        type={"text"}
                        value={field.value}
                        placeholder={"Комментарий к заказу (необязательно)"}
                        onChange={field.onChange}
                        errorMessage={fieldState.error?.message}
                        withAnimatingLabel
                        ref={field.ref}
                      />
                    )}
                  />
                </FormGroup>
              </FormPanel>

              <Button
                style={{ display: "none" }}
                type={"submit"}
                ref={refSubmit}
              />
            </StyledForm>
          </>
        )}
      </>
    )
  },
)

CheckoutForm.displayName = "CheckoutForm"
