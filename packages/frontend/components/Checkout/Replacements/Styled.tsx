import { styled } from "@linaria/react"
import { Span, TypographyBase } from "../../Typography/Typography"

export const StyledReplacement = styled(Span)`
  margin: 0;

  ${TypographyBase} {
    margin-bottom: 0;
  }
`
