import { FC, ReactNode, useEffect, useState } from "react"
import { Panel, PanelHeading } from "../../../styles/utils/StyledPanel"
import { CustomRadioGroup } from "../../Radio/CustomRadioGroup"
import { ReplacementListItem } from "../../../types/types"
import { useMutation } from "react-query"
import { fetchReplacements } from "../../../api/checkoutAPI"
import { Typography } from "../../Typography/Typography"
import { colors } from "../../../styles/utils/vars"
import { StyledReplacement } from "./Styled"

export const Replacements: FC<{
  onChange?: (value: string | number | undefined) => void
  initialReplacement?: number
}> = ({ onChange, initialReplacement }) => {
  const [replacementsData, setReplacementsData] = useState<
    ReplacementListItem[]
  >([])
  const [elementsReplacements, setElementsReplacements] = useState<Record<
    string,
    { content: ReactNode }
  > | null>(null)

  const { mutate: replacementsMutate, isLoading: isFetchingReplacements } =
    useMutation(fetchReplacements, {
      onSuccess: (response) => {
        if (!!response) {
          setReplacementsData(response)
          const item: Record<string, { content: ReactNode }> = {}
          response.map((r) => {
            item[r.id] = {
              content: (
                <>
                  <StyledReplacement>
                    <Typography>{r.title}</Typography>
                    <Typography variant={"p12"}>{r.description}</Typography>
                  </StyledReplacement>
                </>
              ),
            }
          })
          setElementsReplacements(item)
        }
      },
    })

  useEffect(() => {
    replacementsMutate()
  }, [replacementsMutate])

  return (
    <>
      {replacementsData.length > 0 && (
        <>
          <Panel>
            <PanelHeading>Способы замены товаров</PanelHeading>
            <Typography variant={"p12"} color={colors.grayDark}>
              В определенных случаях товар может закончиться или не
              соответствовать критериям качества. В этом случае сборщик вашего
              заказа будет действовать согласно этим настройкам.
            </Typography>
            <CustomRadioGroup
              variant={"rounds"}
              indexCurrent={
                initialReplacement !== undefined
                  ? replacementsData.findIndex(
                      ({ id }) => id === initialReplacement,
                    )
                  : undefined
              }
              items={replacementsData.map((item) => {
                return {
                  value: item.id,
                  message: item.title,
                }
              })}
              itemsElements={
                elementsReplacements !== null ? elementsReplacements : undefined
              }
              ariaLabel={"replacements"}
              onChange={(value) => {
                if (onChange) {
                  onChange(value)
                }
              }}
              isFetching={isFetchingReplacements}
            />
          </Panel>
        </>
      )}
    </>
  )
}
