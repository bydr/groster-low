import { FC, SVGProps } from "react"
import { colors, sizeSVG } from "../../styles/utils/vars"
import { css, CSSProperties } from "@linaria/core"
import dynamic, { DynamicOptions } from "next/dynamic"
import type { IconWithStylesPropsType } from "./Icon"

const IconInner = dynamic(
  (() => import("./Icon").then((m) => m.IconInner)) as DynamicOptions<
    IconWithStylesPropsType & SVGProps<SVGSVGElement>
  >,
  {
    ssr: false,
  },
)

export type IconNameType =
  | "Copy"
  | "AngleRight"
  | "AngleBottom"
  | "Check"
  | "Rotate"
  | "ArrowDown"
  | "ArrowUp"
  | "Plus"
  | "Minus"
  | "ViewBoxes"
  | "ViewSchedule"
  | "Layers"
  | "Tag"
  | "StatsNot"
  | "StatsMany"
  | "StatsFew"
  | "Car"
  | "Star"
  | "StarOutline"
  | "ArrowRight"
  | "ArrowLeft"
  | "X"
  | "Info"
  | "Search"
  | "ShoppingCart"
  | "Lock"
  | "PayApple"
  | "PayGoogle"
  | "PayMastercard"
  | "PayMaestro"
  | "PayVisa"
  | "PayMir"
  | "Phone"
  | "Chat"
  | "Email"
  | "Location"
  | "Instagram"
  | "Youtube"
  | "Facebook"
  | "Twitter"
  | "Telegram"
  | "Pinterest"
  | "Menu"
  | "Filter"
  | "Vk"
  | "Google"
  | "User"
  | "ArrowCircleRight"
  | "Share"
  | "Print"
  | "Delete"
  | "Transaction"
  | "Undo"
  | "CircleLoading"
  | "AngleLeft"
  | "Union"
  | "UserAdd"
  | "Edit"
  | "Bolt"
  | "ListBullet"
  | "Verified"
  | "Flag"
  | "Extension"
  | "Ratio"
  | "ZoomIn"
  | "NoImage"
  | "PlusCircle"
  | "Wallet"
  | "CalendarEdit"
  | "FileRemove"
  | "NearMe"
  | "Remove"
  | "Book"
  | "PlusSquare"
  | "Trophy"
  | "Heart"
  | "WhatsApp"
  | "Viber"
  | "File"
  | "Download"
  | "InBrowser"
  | "Refresh"
  | "Warning"
  | "RefreshTime"
  | "Link"

export type sizeSVGNamesType = keyof typeof sizeSVG

export interface IconPropsType {
  NameIcon: IconNameType
  fill?: string
  size?: sizeSVGNamesType
}

export type GetSizeSVGType = (
  size: typeof sizeSVG[keyof typeof sizeSVG],
) => string | number | CSSProperties

export const getSizeSVG: GetSizeSVGType = (size) => {
  return {
    width: size,
    height: size,
    minWidth: size,
    minHeight: size,
  }
}

export const cssIcon = css`
  ${getSizeSVG("20px")};

  &.smaller {
    ${getSizeSVG(sizeSVG.smaller)};
  }
  &.small {
    ${getSizeSVG(sizeSVG.small)};
  }
  &.default {
    ${getSizeSVG(sizeSVG.default)};
  }
  &.large {
    ${getSizeSVG(sizeSVG.large)};
  }
  &.largeL {
    ${getSizeSVG(sizeSVG.largeL)};
  }
  &.largeM {
    ${getSizeSVG(sizeSVG.largeM)};
  }
  &.largeXL {
    ${getSizeSVG(sizeSVG.largeXL)};
  }
  &.mediumM {
    ${getSizeSVG(sizeSVG.mediumM)};
  }
  &.medium {
    ${getSizeSVG(sizeSVG.medium)};
  }
  &.fill {
    ${getSizeSVG(sizeSVG.fill)};
  }
`

export const cssIconShoppingCart = css`
  fill: ${colors.brand.yellow};
`

export const Icon: FC<IconPropsType & SVGProps<SVGSVGElement>> = ({
  NameIcon,
  fill,
  size = "default",
  className,
  ...props
}) => {
  const classNameCalc = `${cssIcon} ${className ? className : ""} ${
    size ? `${size}` : ""
  }`
  const styles: CSSProperties | undefined = fill ? { fill: fill } : undefined

  return (
    <IconInner
      NameIcon={NameIcon}
      className={classNameCalc}
      style={styles}
      {...props}
    />
  )
}
