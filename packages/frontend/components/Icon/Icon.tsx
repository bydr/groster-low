import { FC, SVGProps } from "react"
import Copy from "./svg/copy.svg"
import AngleRight from "./svg/angleRight.svg"
import AngleBottom from "./svg/angleBottom.svg"
import Check from "./svg/check.svg"
import Rotate from "./svg/rotate.svg"
import ArrowDown from "./svg/arrowDown.svg"
import ArrowUp from "./svg/arrowUp.svg"
import Plus from "./svg/plus.svg"
import Minus from "./svg/minus.svg"
import ViewBoxes from "./svg/viewBoxes.svg"
import ViewSchedule from "./svg/viewSchedule.svg"
import Layers from "./svg/layers.svg"
import Tag from "./svg/tag.svg"
import StatsFew from "./svg/statsFew.svg"
import StatsNot from "./svg/statsNot.svg"
import StatsMany from "./svg/statsMany.svg"
import Car from "./svg/car.svg"
import Star from "./svg/star.svg"
import StarOutline from "./svg/starOutline.svg"
import ArrowRight from "./svg/arrowRight.svg"
import ArrowLeft from "./svg/arrowLeft.svg"
import X from "./svg/x.svg"
import Info from "./svg/info.svg"
import Search from "./svg/search.svg"
import ShoppingCart from "./svg/shoppingCart.svg"
import Lock from "./svg/lock.svg"
import PayApple from "./svg/payApple.svg"
import PayGoogle from "./svg/payGoogle.svg"
import PayMastercard from "./svg/payMastercard.svg"
import PayMaestro from "./svg/payMaestro.svg"
import PayVisa from "./svg/payVisa.svg"
import PayMir from "./svg/payMir.svg"
import Phone from "./svg/phone.svg"
import Chat from "./svg/chat.svg"
import Email from "./svg/email.svg"
import Location from "./svg/location.svg"
import Instagram from "./svg/inst.svg"
import Youtube from "./svg/youtube.svg"
import Facebook from "./svg/fb.svg"
import Twitter from "./svg/twitter.svg"
import Telegram from "./svg/telegram.svg"
import Pinterest from "./svg/pinterest.svg"
import Menu from "./svg/menu.svg"
import Filter from "./svg/filter.svg"
import Vk from "./svg/vk.svg"
import Google from "./svg/google.svg"
import User from "./svg/user.svg"
import ArrowCircleRight from "./svg/arrowCircleRight.svg"
import Share from "./svg/share.svg"
import Print from "./svg/print.svg"
import Delete from "./svg/delete.svg"
import Transaction from "./svg/transaction.svg"
import Undo from "./svg/undo.svg"
import CircleLoading from "./svg/circleLoading.svg"
import AngleLeft from "./svg/angleLeft.svg"
import Union from "./svg/union.svg"
import UserAdd from "./svg/userAdd.svg"
import Edit from "./svg/edit.svg"
import Bolt from "./svg/bolt.svg"
import ListBullet from "./svg/listBullet.svg"
import Verified from "./svg/verified.svg"
import Flag from "./svg/flag.svg"
import Extension from "./svg/extension.svg"
import Ratio from "./svg/ratio.svg"
import ZoomIn from "./svg/zoomIn.svg"
import NoImage from "./svg/noimage.svg"
import PlusCircle from "./svg/plusCircle.svg"
import Wallet from "./svg/wallet.svg"
import FileRemove from "./svg/fileRemove.svg"
import CalendarEdit from "./svg/calendarEdit.svg"
import NearMe from "./svg/nearMe.svg"
import Remove from "./svg/remove.svg"
import Book from "./svg/book.svg"
import PlusSquare from "./svg/plusSquare.svg"
import Heart from "./svg/heart.svg"
import Trophy from "./svg/trophy.svg"
import WhatsApp from "./svg/whatsApp.svg"
import Viber from "./svg/viber.svg"
import File from "./svg/file.svg"
import Download from "./svg/download.svg"
import InBrowser from "./svg/inBrowser.svg"
import Refresh from "./svg/refresh.svg"
import Warning from "./svg/warning.svg"
import RefreshTime from "./svg/refreshTime.svg"
import Link from "./svg/link.svg"
import { IconNameType } from "./index"

export type IconWithStylesPropsType = {
  NameIcon: IconNameType
}
export const IconInner: FC<IconWithStylesPropsType & SVGProps<SVGSVGElement>> =
  ({ NameIcon, ...props }) => {
    switch (NameIcon) {
      case "Copy": {
        return <Copy {...props} />
      }
      case "AngleRight": {
        return <AngleRight {...props} />
      }
      case "AngleBottom": {
        return <AngleBottom {...props} />
      }
      case "Check": {
        return <Check {...props} />
      }
      case "Rotate": {
        return <Rotate {...props} />
      }
      case "ArrowDown": {
        return <ArrowDown {...props} />
      }
      case "ArrowUp": {
        return <ArrowUp {...props} />
      }
      case "Plus": {
        return <Plus {...props} />
      }
      case "Minus": {
        return <Minus {...props} />
      }
      case "ViewBoxes": {
        return <ViewBoxes {...props} />
      }
      case "ViewSchedule": {
        return <ViewSchedule {...props} />
      }
      case "Layers": {
        return <Layers {...props} />
      }
      case "Tag": {
        return <Tag {...props} />
      }
      case "StatsFew": {
        return <StatsFew {...props} />
      }
      case "StatsNot": {
        return <StatsNot {...props} />
      }
      case "StatsMany": {
        return <StatsMany {...props} />
      }
      case "Car": {
        return <Car {...props} />
      }
      case "Star": {
        return <Star {...props} />
      }
      case "StarOutline": {
        return <StarOutline {...props} />
      }
      case "ArrowRight": {
        return <ArrowRight {...props} />
      }
      case "ArrowLeft": {
        return <ArrowLeft {...props} />
      }
      case "X": {
        return <X {...props} />
      }
      case "Info": {
        return <Info {...props} />
      }
      case "Search": {
        return <Search {...props} />
      }
      case "ShoppingCart": {
        return <ShoppingCart {...props} />
      }
      case "Lock": {
        return <Lock {...props} />
      }
      case "PayApple": {
        return <PayApple {...props} />
      }
      case "PayGoogle": {
        return <PayGoogle {...props} />
      }
      case "PayMastercard": {
        return <PayMastercard {...props} />
      }
      case "PayMaestro": {
        return <PayMaestro {...props} />
      }
      case "PayVisa": {
        return <PayVisa {...props} />
      }
      case "PayMir": {
        return <PayMir {...props} />
      }
      case "Phone": {
        return <Phone {...props} />
      }
      case "Chat": {
        return <Chat {...props} />
      }
      case "Email": {
        return <Email {...props} />
      }
      case "Location": {
        return <Location {...props} />
      }
      case "Instagram": {
        return <Instagram {...props} />
      }
      case "Youtube": {
        return <Youtube {...props} />
      }
      case "Facebook": {
        return <Facebook {...props} />
      }
      case "Twitter": {
        return <Twitter {...props} />
      }
      case "Telegram": {
        return <Telegram {...props} />
      }
      case "Pinterest": {
        return <Pinterest {...props} />
      }
      case "Menu": {
        return <Menu {...props} />
      }
      case "Filter": {
        return <Filter {...props} />
      }
      case "Vk": {
        return <Vk {...props} />
      }
      case "Google": {
        return <Google {...props} />
      }
      case "User": {
        return <User {...props} />
      }
      case "ArrowCircleRight": {
        return <ArrowCircleRight {...props} />
      }
      case "Share": {
        return <Share {...props} />
      }
      case "Print": {
        return <Print {...props} />
      }
      case "Delete": {
        return <Delete {...props} />
      }
      case "Transaction": {
        return <Transaction {...props} />
      }
      case "Undo": {
        return <Undo {...props} />
      }
      case "CircleLoading": {
        return <CircleLoading {...props} />
      }
      case "AngleLeft": {
        return <AngleLeft {...props} />
      }
      case "Union": {
        return <Union {...props} />
      }
      case "UserAdd": {
        return <UserAdd {...props} />
      }
      case "Edit": {
        return <Edit {...props} />
      }
      case "Bolt": {
        return <Bolt {...props} />
      }
      case "ListBullet": {
        return <ListBullet {...props} />
      }
      case "Verified": {
        return <Verified {...props} />
      }
      case "Flag": {
        return <Flag {...props} />
      }
      case "Extension": {
        return <Extension {...props} />
      }
      case "Ratio": {
        return <Ratio {...props} />
      }
      case "ZoomIn": {
        return <ZoomIn {...props} />
      }
      case "NoImage": {
        return <NoImage {...props} />
      }
      case "PlusCircle": {
        return <PlusCircle {...props} />
      }
      case "Wallet": {
        return <Wallet {...props} />
      }
      case "FileRemove": {
        return <FileRemove {...props} />
      }
      case "CalendarEdit": {
        return <CalendarEdit {...props} />
      }
      case "NearMe": {
        return <NearMe {...props} />
      }
      case "Remove": {
        return <Remove {...props} />
      }
      case "Book": {
        return <Book {...props} />
      }
      case "PlusSquare": {
        return <PlusSquare {...props} />
      }
      case "Heart": {
        return <Heart {...props} />
      }
      case "Trophy": {
        return <Trophy {...props} />
      }
      case "WhatsApp": {
        return <WhatsApp {...props} />
      }
      case "Viber": {
        return <Viber {...props} />
      }
      case "File": {
        return <File {...props} />
      }
      case "Download": {
        return <Download {...props} />
      }
      case "InBrowser": {
        return <InBrowser {...props} />
      }
      case "Refresh": {
        return <Refresh {...props} />
      }
      case "Warning": {
        return <Warning {...props} />
      }
      case "RefreshTime": {
        return <RefreshTime {...props} />
      }
      case "Link": {
        return <Link {...props} />
      }
      default:
        return <></>
    }
  }
