import { styled } from "@linaria/react"
import { ButtonBase } from "../Button/StyledButton"
import {
  StyledFieldInput,
  StyledFieldWrapper,
  StyledInput,
} from "../Field/StyledField"
import { cssIcon } from "../Icon"
import { colors } from "../../styles/utils/vars"
import { getTypographyBase } from "../Typography/Typography"
import { CheckboxBase } from "../Checkbox/StyledCheckbox"

export const StyledSubscription = styled.div`
  position: relative;
  margin-bottom: 32px;
  width: 100%;

  ${StyledFieldWrapper} {
    border-top-right-radius: 50px;
    border-bottom-right-radius: 50px;

    ${StyledFieldInput} {
      ${StyledInput} {
        border: none;
      }

      ${ButtonBase} {
        margin: 4px 4px 4px 0;
        border-radius: 50px;

        > .${cssIcon} {
          fill: ${colors.white};
        }

        &:hover,
        &:active {
          .${cssIcon} {
            fill: ${colors.brand.yellow};
          }
        }
      }
    }
  }

  ${CheckboxBase} {
    ${getTypographyBase("p12")};
  }
`
