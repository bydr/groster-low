import type { FC } from "react"
import { useState } from "react"
import { Field } from "../Field/Field"
import { StyledSubscription } from "./StyledSubscription"
import { SingleError } from "../Typography/Typography"
import { FormGroup, StyledForm } from "components/Forms/StyledForms"
import { useForm } from "react-hook-form"
import { useMutation } from "react-query"
import { fetchSubscribe } from "../../api/feedbackAPI"
import { SuccessOverlay } from "../Forms/SuccessOverlay/SuccessOverlay"
import { TIMEOUT_SUCCESS } from "../../utils/constants"
import { ApiError } from "../../../contracts/contracts"
import { PrivacyAgreement } from "../Forms/PrivacyAgreement"

type SubscriptionFormFieldsType = {
  email: string
  dataPrivacyAgreement: boolean
}

export const Subscription: FC = () => {
  const [error, setError] = useState<string | null>(null)

  const {
    handleSubmit,
    register,
    formState: { errors },
    reset,
  } = useForm<SubscriptionFormFieldsType>({
    defaultValues: {
      email: "",
    },
    mode: "onChange",
  })

  const {
    mutate: subscribeMutate,
    reset: resetMutation,
    isSuccess,
    isLoading,
  } = useMutation(fetchSubscribe, {
    onMutate: () => {
      setError(null)
    },
    onSuccess: () => {
      console.log("success")
      reset()
      setTimeout(() => {
        resetMutation()
      }, TIMEOUT_SUCCESS)
      setError(null)
    },
    onError: (err: ApiError) => {
      setError(err.message)
    },
  })

  const onSubmit = handleSubmit((data) => {
    subscribeMutate({
      email: data.email,
    })
  })

  return (
    <StyledSubscription>
      <SuccessOverlay
        isSuccess={isSuccess}
        message={"Подписка успешно оформлена"}
      />
      <StyledForm onSubmit={onSubmit}>
        <FormGroup>
          <Field
            placeholder={"Ваш e-mail"}
            type={"email"}
            withAnimatingLabel={false}
            withButton={true}
            iconButton={"ArrowRight"}
            textButton={"Подписаться"}
            iconPositionButton={"right"}
            variantButton={"filled"}
            isFetching={isLoading}
            errorMessage={errors?.email?.message}
            disabledButton={Object.keys(errors).length > 0}
            {...register("email", {
              required: {
                value: true,
                message: "Заполните это поле",
              },
            })}
          />
        </FormGroup>
        <PrivacyAgreement
          {...register("dataPrivacyAgreement", {
            required: {
              value: true,
              message: "Нам необходимо ваше согласие",
            },
          })}
          errorMessage={errors?.dataPrivacyAgreement?.message}
          isCheckboxOverlay
        />
      </StyledForm>

      {error !== null && <SingleError>{error}</SingleError>}
    </StyledSubscription>
  )
}
