import { styled } from "@linaria/react"
import { css } from "@linaria/core"
import { Radio, RadioGroup } from "reakit"
import {
  ErrorMessageField,
  getTypographyBase,
  Span,
} from "../Typography/Typography"
import { colors } from "../../styles/utils/vars"
import { StyledFieldWrapper } from "../Field/StyledField"

export const cssIsDisabled = css`
  pointer-events: none !important;
`
export const cssRadioRounds = css``
export const cssRadioTabs = css``
export const cssRadioButtons = css``

export const cssMockInput = css`
  opacity: 0 !important;
  user-select: none !important;
  position: absolute !important;
  z-index: -1 !important;
`

export const StyledRadioBox = styled.div`
  width: 24px;
  height: 24px;
  min-height: 24px;
  min-width: 24px;
  border: 1px solid ${colors.gray};
  border-radius: 50%;
  transition: all 0.15s ease-in-out;
  display: inline-flex;
  align-items: center;
  justify-content: center;
  text-align: center;
  margin: 4px 14px 0 0;

  &:before {
    content: "";
    width: 8px;
    height: 8px;
    min-height: 8px;
    min-width: 8px;
    border-radius: inherit;
    background: transparent;
    transition: all 0.1s ease-in-out;
    opacity: 0;
    visibility: hidden;
    transform: scale(0);
  }
`

export const StyledRadioMessage = styled(Span)`
  margin-bottom: 0;
`

export const RadioLabel = styled.label`
  ${getTypographyBase("p12")};
  cursor: pointer;
  padding: 8px 10px;
  position: relative;
  flex: 1;
  width: 100%;
  display: flex;
  margin-bottom: 0;

  .${cssRadioRounds} & {
    ${getTypographyBase("p14")};
    padding-left: 0;
    padding-right: 0;
    font-weight: 600;

    &:hover {
      ${StyledRadioBox} {
        border-color: ${colors.black};

        &:before {
          background: ${colors.grayLight};
          opacity: 1;
        }
      }
    }
  }

  .${cssRadioButtons} & {
    ${getTypographyBase("p14")};
    text-align: center;
    padding: 2px 12px;
    white-space: nowrap;
  }

  .${cssRadioTabs} & {
    text-align: center;
  }

  &.${cssIsDisabled} {
    opacity: 0.3 !important;
  }
`
export const StyledRadioInner = styled.span`
  font-size: inherit;
  line-height: inherit;
  color: ${colors.grayDark};
  flex: 1;
  margin-bottom: 0;

  > * {
    position: relative;
  }

  .${cssRadioTabs} &:before {
    content: "";
    top: 0;
    left: 0;
    width: 100%;
    background: ${colors.gray};
    height: 2px;
    position: absolute;
  }

  .${cssRadioButtons} &:before {
    content: "";
    top: 0;
    bottom: 0;
    left: 0;
    right: 0;
    background: transparent;
    position: absolute;
    z-index: 0;
    border-radius: 50px;
  }

  .${cssRadioButtons} & > * {
    z-index: 1;
  }

  &:hover,
  &:active {
    color: ${colors.black};
  }
`
export const StyledRadio = styled(Radio)`
  opacity: 0;
  visibility: hidden;
  position: absolute;

  &:checked {
    & ~ ${StyledRadioInner} {
      color: ${colors.black};

      &:before {
        background: ${colors.brand.yellow};
      }
    }
  }

  &:checked,
  &:hover {
    & ~ ${StyledRadioBox} {
      border-color: ${colors.black};

      &:before {
        opacity: 1;
        visibility: visible;
        transform: scale(1);
      }
    }
  }

  &:hover {
    & ~ ${StyledRadioBox} {
      &:before {
        background: ${colors.grayLight};
      }
    }
  }

  &[type="radio"] {
    &:checked {
      & ~ ${StyledRadioBox} {
        &:before {
          background: ${colors.brand.yellow};
        }
      }
    }
  }
`
export const StyledRadioGroup = styled(RadioGroup)`
  display: inline-flex;
  width: 100%;

  &.${cssRadioRounds} {
    display: flex;
    flex-direction: column;
  }
`

export const cssRadioGroup = css`
  background: inherit;
  width: 100%;
`

export const StyledRadioGroupWrap = styled(StyledFieldWrapper)`
  width: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;

  ${ErrorMessageField} {
    width: 100%;
  }

  &[data-iserror="true"] {
    ${RadioLabel} {
      ${StyledRadioBox} {
        border-color: ${colors.red};
      }
    }
  }
`
