import { styled } from "@linaria/react"
import { Tab as ReakitTab, TabProps } from "reakit/Tab"
import { getTypographyBase, TypographyBase } from "../Typography/Typography"
import { colors } from "../../styles/utils/vars"
import { FC, useContext } from "react"
import { TabsContext } from "./Context"

export const StyledTab = styled(ReakitTab)`
  ${getTypographyBase("p12")};
  appearance: none;
  background: transparent;
  border: none;
  position: relative;
  padding: 8px 10px;
  color: ${colors.grayDark};
  cursor: pointer;

  &:before {
    content: "";
    top: 0;
    left: 0;
    width: 100%;
    background: ${colors.gray};
    height: 2px;
    position: absolute;
  }

  &[aria-selected="true"] {
    color: ${colors.black};

    &:before {
      background: ${colors.brand.yellow};
    }
  }

  [data-variant="button"] > & {
    ${getTypographyBase("p14")};
    display: flex;
    align-items: center;
    background: transparent;
    color: ${colors.grayDark};
    padding: 2px 12px;
    border-radius: 50px;

    &:before {
      content: none;
      display: none;
    }

    &[aria-selected="true"] {
      color: ${colors.black};
      background: ${colors.brand.yellow};
    }

    &:hover,
    &:active {
      color: ${colors.black};
    }
  }

  ${TypographyBase} {
    margin-bottom: 0;
  }
`

export const Tab: FC<Partial<TabProps>> = (props) => {
  const tab = useContext(TabsContext)
  return <StyledTab {...tab} {...props} />
}
