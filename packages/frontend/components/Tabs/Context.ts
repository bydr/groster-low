import { createContext } from "react";
import { TabStateReturn } from "reakit/src/Tab/TabState";

export const TabsContext = createContext<TabStateReturn | null>(null);
