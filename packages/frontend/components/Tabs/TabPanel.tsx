import { TabPanel as ReakitTabPanel, TabPanelOptions } from "reakit/Tab"
import { FC, useContext } from "react";
import { TabsContext } from "./Context";

export const TabPanel: FC<Partial<TabPanelOptions>> = (props) => {
  const tab = useContext(TabsContext);
  return <ReakitTabPanel {...tab} {...props} />;
}
