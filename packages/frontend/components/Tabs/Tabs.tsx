import { TabsContext } from "./Context"
import { useTabState } from "reakit/Tab"
import type { FC } from "react"
import { TabInitialState } from "reakit/src/Tab/TabState"

export const Tabs: FC<TabInitialState> = ({ children, ...initialState }) => {
  const tab = useTabState(initialState)
  return <TabsContext.Provider value={tab}>{children}</TabsContext.Provider>
}
