import { FC, useContext } from "react"
import { styled } from "@linaria/react"
import { TabList as ReakitTabList, TabListProps } from "reakit/Tab"
import { TabsContext } from "./Context"

export const StyledTabList = styled(ReakitTabList)`
  width: 100%;
  position: relative;
  margin-bottom: 20px;
  display: flex;
`
type TabListPropsType = {
  variant?: "inline" | "button"
}

export const TabList: FC<Partial<TabListProps> & TabListPropsType> = ({
  variant = "inline",
  ...props
}) => {
  const tab = useContext(TabsContext)
  return <StyledTabList data-variant={variant} {...tab} {...props} />
}
