import { FC, useEffect, useState } from "react"
import {
  cssButtonIsActive,
  cssButtonPaginator,
  cssIsAdditionalLoad,
  PaginatorLoad,
  PaginatorPages,
  StyledPaginator,
} from "./StyledPaginator"
import { Button } from "../Button"
import { cx } from "@linaria/core"

type PaginatorPropsType = {
  totalCount: number
  itemsPerPage: number
  currentPage: number
  onPageChanged: (page: number) => void
  partsPerPage?: number
  onPageAdditional?: (page: number) => void
}
export const Paginator: FC<PaginatorPropsType> = ({
  totalCount,
  itemsPerPage,
  currentPage,
  onPageChanged,
  partsPerPage = 5,
  onPageAdditional,
}) => {
  const currentPartNumber =
    currentPage > partsPerPage ? Math.floor(currentPage / partsPerPage) : 1
  const pagesCount: number = Math.ceil(totalCount / itemsPerPage)
  const pages: number[] = []
  for (let i = 1; i <= pagesCount; i++) {
    pages.push(i)
  }

  const partCount: number = Math.ceil(pagesCount / partsPerPage) // 43 / 10 = 4,3 (5 partsCount)
  const [partNumber, setPartNumber] = useState(currentPartNumber) // 1

  useEffect(() => {
    setPartNumber(
      currentPage > partsPerPage
        ? Math.floor(currentPage / partsPerPage) + 1
        : 1,
    )
  }, [currentPage, partsPerPage])

  const leftPart: number = (partNumber - 1) * partsPerPage + 1 // 1
  const rightPart: number = partNumber * partsPerPage // 1 * 10 = 10

  return (
    <>
      {pagesCount > 1 && (
        <StyledPaginator
          className={cx(onPageAdditional !== undefined && cssIsAdditionalLoad)}
        >
          <PaginatorPages>
            {partNumber > 1 && (
              <Button
                variant={"small"}
                isHiddenBg
                className={cssButtonPaginator}
                icon={"ArrowLeft"}
                onClick={() => {
                  setPartNumber(partNumber - 1)
                }}
              >
                Назад
              </Button>
            )}
            {pages
              .filter((p) => p >= leftPart && p <= rightPart) //create new array pages in part
              .map((p) => (
                <Button
                  key={p}
                  variant={"small"}
                  isHiddenBg
                  className={cx(
                    cssButtonPaginator,
                    currentPage === p && cssButtonIsActive,
                  )}
                  onClick={() => {
                    onPageChanged(p)
                  }}
                >
                  {p}
                </Button>
              ))}
            {partCount > partNumber && (
              <Button
                variant={"small"}
                isHiddenBg
                className={cssButtonPaginator}
                icon={"ArrowRight"}
                iconPosition={"right"}
                onClick={() => {
                  setPartNumber(partNumber + 1)
                }}
              >
                Вперёд
              </Button>
            )}
          </PaginatorPages>
          {pagesCount > currentPage && onPageAdditional && (
            <PaginatorLoad>
              <Button
                variant={"outline"}
                className={cssButtonPaginator}
                onClick={() => {
                  if (onPageAdditional) {
                    onPageAdditional(currentPage + 1)
                  }
                }}
              >
                Показать еще
              </Button>
            </PaginatorLoad>
          )}
        </StyledPaginator>
      )}
    </>
  )
}
