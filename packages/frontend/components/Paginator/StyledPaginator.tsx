import { styled } from "@linaria/react"
import { css } from "@linaria/core"
import { breakpoints, colors, sizeSVG } from "../../styles/utils/vars"
import { cssIcon, getSizeSVG } from "../Icon"
import { getTypographyBase } from "../Typography/Typography"
import { ButtonBase } from "../Button/StyledButton"

export const cssIsAdditionalLoad = css``

export const cssButtonPaginator = css`
  ${getTypographyBase("p12")};
`
export const cssButtonIsActive = css``
export const PaginatorPages = styled.div`
  width: 100%;
  display: flex;
  align-items: center;
  flex: 1;
  flex-wrap: wrap;
`
export const PaginatorLoad = styled.div`
  width: 100%;
  flex: 1;
  display: flex;
  justify-content: flex-end;
`

export const StyledPaginator = styled.div`
  position: relative;
  display: flex;
  width: 100%;
  align-items: center;
  margin-top: 30px;
  z-index: 1;

  .${cssButtonPaginator}${ButtonBase} {
    ${getTypographyBase("p12")};
    font-weight: 600;
    min-height: 32px;
    min-width: 32px;

    .${cssIcon} {
      ${getSizeSVG(sizeSVG.medium)};
    }
  }

  ${PaginatorPages} {
    .${cssButtonPaginator}${ButtonBase} {
      margin-right: 8px;

      &.${cssButtonIsActive} {
        background: ${colors.grayLight};
        color: ${colors.black};
        user-select: none;
        pointer-events: none;
      }

      &:hover,
      &:active {
        background: transparent;
        color: ${colors.black};
      }
    }
  }

  &.${cssIsAdditionalLoad} {
    @media (max-width: ${breakpoints.sm}) {
      ${PaginatorLoad} {
        justify-content: center;

        ${ButtonBase} {
          width: 100%;
        }
      }
    }
  }

  @media (max-width: ${breakpoints.sm}) {
    padding: 0;
    flex-direction: column;
    justify-content: center;

    ${PaginatorPages} {
      margin-bottom: 20px;
      justify-content: center;
    }

    ${PaginatorLoad} {
      justify-content: center;

      ${ButtonBase} {
        width: 100%;
      }
    }
  }
`
