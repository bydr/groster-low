import { styled } from "@linaria/react"
import { StyledSelect } from "../../Select/StyledSelect"

export const StyledAutoComplete = styled(StyledSelect)`
  [role="combobox"] {
    user-select: initial;
    color: initial;
    padding-top: 20px;
  }
`
