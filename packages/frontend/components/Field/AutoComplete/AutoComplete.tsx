import { ForwardedRef, forwardRef, useEffect, useMemo, useState } from "react"
import { SelectItemsType } from "../../Select/Select"
import { Placeholder, StyledFieldWrapper } from "../StyledField"
import {
  StyledSelectInputDiv,
  StyledSelectInputWrapper,
} from "../../Select/StyledSelect"
import { ErrorMessageField, Typography } from "../../Typography/Typography"
import {
  unstable_Combobox as Combobox,
  unstable_ComboboxOption as ComboboxOption,
  unstable_ComboboxPopover as ComboboxPopover,
  unstable_useComboboxState as useComboboxState,
} from "reakit/Combobox"
import { StyledAutoComplete } from "./StyledAutoComplete"
import { FieldVariantsPropsType } from "../../../types/types"
import { Icon } from "../../Icon"

interface AutoCompletePropsType {
  items: SelectItemsType[]
  ariaLabel?: string
  onSelectValue?: (value: string) => void
}

export const AutoComplete = forwardRef<
  ForwardedRef<HTMLInputElement>,
  AutoCompletePropsType & {
    initialValue?: string
  } & Pick<FieldVariantsPropsType, "errorMessage" | "required">
>(
  (
    {
      items,
      initialValue,
      ariaLabel,
      onSelectValue,
      errorMessage,
      required = false,
    },
    ref,
  ) => {
    const itemsMemo = useMemo(() => items, [items])
    const [itemSelected, setItemSelected] = useState<
      SelectItemsType | undefined
    >()

    const combobox = useComboboxState({
      list: true,
      inline: true,
      // autoSelect: true,
      gutter: 2,
      values: (itemsMemo || []).map((item) => item.value),
      unstable_offset: [0, -2],
      placement: "bottom-start",
    })

    useEffect(() => {
      combobox.setValues(itemsMemo.map((item) => item.value))
    }, [itemsMemo, combobox.setValues])

    useEffect(() => {
      if (!!initialValue && initialValue.length > 0) {
        setItemSelected(itemsMemo.find((i) => i.value === initialValue))
        combobox.setInputValue(initialValue)
      }
    }, [initialValue, itemsMemo, combobox.setInputValue])

    useEffect(() => {
      if (itemSelected?.value !== undefined) {
        if (onSelectValue) {
          onSelectValue(itemSelected.value)
        }
      }
    }, [itemSelected?.value])

    return (
      <>
        <StyledFieldWrapper
          data-iserror={!!errorMessage}
          data-required={required}
        >
          <StyledAutoComplete>
            <StyledSelectInputWrapper>
              <StyledSelectInputDiv
                data-is-empty={(combobox?.inputValue || "").length <= 0}
              >
                <Combobox
                  {...combobox}
                  aria-label={ariaLabel || "auto complete"}
                  onClick={(e) => {
                    if (combobox.visible) {
                      e.preventDefault()
                      combobox.setVisible(false)
                    }
                  }}
                  onChange={(e) => {
                    setItemSelected({
                      value: e.target.value,
                      name: e.target.value,
                    })
                  }}
                  ref={ref as ForwardedRef<HTMLInputElement>}
                />
                {ariaLabel && <Placeholder>{ariaLabel}</Placeholder>}
                <Icon NameIcon={"AngleBottom"} />
              </StyledSelectInputDiv>
            </StyledSelectInputWrapper>
            <ComboboxPopover
              {...combobox}
              aria-label={ariaLabel || "aria label"}
            >
              {combobox.matches.map((value) => {
                const item = items.find((item) => item.value === value)
                return (
                  <ComboboxOption
                    {...combobox}
                    key={value}
                    value={value}
                    onClick={() => {
                      setItemSelected(
                        itemsMemo.find((item) => item.value === value),
                      )
                    }}
                  >
                    <Typography variant={"span"}>
                      {item?.layout !== undefined ? item?.layout : item?.name}
                    </Typography>
                  </ComboboxOption>
                )
              })}
            </ComboboxPopover>
          </StyledAutoComplete>
          {errorMessage && (
            <ErrorMessageField>{errorMessage}</ErrorMessageField>
          )}
        </StyledFieldWrapper>
      </>
    )
  },
)

AutoComplete.displayName = "AutoComplete"
