import { BaseHTMLAttributes, forwardRef, MouseEvent } from "react"

import { Button } from "../Button"
import { ErrorMessageField, Typography } from "../Typography/Typography"
import {
  cssButtonFieldClear,
  Placeholder,
  StyledFieldInput,
  StyledFieldWrapper,
  StyledHint,
  StyledInput,
} from "./StyledField"
import { FieldVariantsPropsType } from "../../types/types"
import { Icon } from "../Icon"
import { SelectedHint, SelectedHintBody } from "../Select/StyledSelect"

export const InputField = forwardRef<
  HTMLInputElement,
  FieldVariantsPropsType &
    Pick<BaseHTMLAttributes<HTMLInputElement>, "className">
>(
  (
    {
      name,
      errorMessage,
      withAnimatingLabel = true,
      placeholder,
      iconButton,
      withButton,
      textButton,
      iconPositionButton = "right",
      variantButton = "box",
      isFetching,
      onClickButton,
      disabledButton,
      iconField,
      hint,
      classNameButton,
      className,
      withClean = false,
      cleanCb,
      required = false,
      isVisibleSelectedHint = false,
      selectedValue,
      ...props
    },
    ref,
  ) => {
    return (
      <StyledFieldWrapper
        data-isanimating-label={withAnimatingLabel}
        data-iswith-button={withButton}
        data-iserror={!!errorMessage}
        data-required={required}
      >
        <StyledFieldInput>
          <StyledInput
            name={name}
            ref={ref}
            placeholder={withAnimatingLabel ? " " : placeholder}
            className={className}
            {...props}
          />

          {withAnimatingLabel && (
            <Placeholder htmlFor={props.id}>{placeholder}</Placeholder>
          )}
          {withClean && (
            <>
              <Button
                variant={"box"}
                icon={"X"}
                type={"button"}
                size={"medium"}
                className={cssButtonFieldClear}
                onClick={(e) => {
                  e.preventDefault()
                  if (cleanCb) {
                    cleanCb()
                  }
                }}
              />
            </>
          )}
          {withButton && (
            <Button
              variant={variantButton}
              icon={iconButton}
              iconPosition={iconPositionButton}
              isFetching={isFetching}
              disabled={disabledButton}
              className={classNameButton}
              onClick={(e: MouseEvent<HTMLButtonElement>) => {
                if (onClickButton) {
                  onClickButton(e)
                }
              }}
            >
              {textButton}
            </Button>
          )}
          {iconField !== undefined && (
            <>
              <Icon NameIcon={iconField} />
            </>
          )}
        </StyledFieldInput>

        {hint && <StyledHint>{hint}</StyledHint>}

        {!!errorMessage && (
          <ErrorMessageField>{errorMessage}</ErrorMessageField>
        )}

        {isVisibleSelectedHint && selectedValue !== undefined && (
          <>
            <SelectedHint>
              <Typography variant={"p12"}>
                <b>Выбран: </b>
              </Typography>
              <SelectedHintBody>{selectedValue}</SelectedHintBody>
            </SelectedHint>
          </>
        )}
      </StyledFieldWrapper>
    )
  },
)
InputField.displayName = "InputField"
