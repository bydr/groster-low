import { ForwardedRef, forwardRef } from "react"
import { FieldProps } from "../../types/types"
import { InputField } from "./InputField"
import { TextareaField } from "./TextareaField"

export const Field = forwardRef<
  HTMLInputElement | HTMLTextAreaElement,
  FieldProps
>(({ variant = "input", ...props }, ref) => {
  switch (variant) {
    case "input":
      return (
        <InputField
          variant={variant}
          {...props}
          ref={ref as ForwardedRef<HTMLInputElement>}
        />
      )
    case "textarea":
      return (
        <TextareaField
          {...props}
          ref={ref as ForwardedRef<HTMLTextAreaElement>}
        />
      )
    default:
      return <></>
  }
})

Field.displayName = "Field"
