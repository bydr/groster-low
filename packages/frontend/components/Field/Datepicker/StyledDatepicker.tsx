import { styled } from "@linaria/react"
import { getTypographyBase } from "../../Typography/Typography"
import { colors } from "../../../styles/utils/vars"
import { css } from "@linaria/core"
import { ButtonSliderArrow } from "../../Button/StyledButton"
import { cssIcon, getSizeSVG } from "../../Icon"
import { StyledFieldInput, StyledFieldWrapper } from "../StyledField"

export const cssNavbar = css`
  display: flex;
  width: 100%;
  justify-content: space-between;
  padding: 1em;
  margin: 0;
`

export const StyledFieldDatepicker = styled.div`
  ${getTypographyBase("p14")};

  ${StyledFieldInput} {
    user-select: none;
    cursor: pointer;
  }

  .DayPicker {
    border-radius: 2px;
    box-shadow: none;
    border: 1px solid ${colors.gray};

    & .${cssNavbar} ${ButtonSliderArrow} {
      position: relative;
      padding: 0;
      height: 32px;
      width: 32px;
      right: auto;
      left: auto;
      margin: 0;

      .${cssIcon} {
        ${getSizeSVG("14px")};
        fill: ${colors.brand.purple};
      }

      &:hover {
        .${cssIcon} {
          fill: ${colors.brand.yellow};
        }
      }
    }

    &,
    & * {
      font-size: inherit;
      font-family: inherit;
    }

    & .DayPicker-Weekday {
      ${getTypographyBase("p12")};
    }

    &:not(.DayPicker--interactionDisabled) .DayPicker-Day {
      &:not(.DayPicker-Day--disabled):not(.DayPicker-Day--selected):not(.DayPicker-Day--outside) {
        &:hover {
          background: ${colors.brand.yellowTransparent15};
        }
      }
    }

    .DayPicker-Day--selected:not(.DayPicker-Day--disabled):not(.DayPicker-Day--outside) {
      position: relative;
      background-color: ${colors.brand.yellow};
      color: ${colors.black};
      font-weight: bold;
    }

    .DayPicker-Day--selected:not(.DayPicker-Day--disabled):not(.DayPicker-Day--outside):hover {
      background-color: ${colors.brand.yellow};
    }

    .DayPicker-Day--selected {
      pointer-events: none !important;
      cursor: default !important;
    }
  }

  .DayPicker-Day--today {
    &,
    &.DayPicker-Day--disabled {
      color: ${colors.brand.yellow};
      font-weight: bold;
      background: ${colors.brand.purple};
    }
  }

  .DayPicker-Day--outside {
    color: ${colors.grayDark};
    cursor: default;
  }

  .DayPicker-Day--disabled {
    color: ${colors.brand.orange};
    opacity: 0.5;
    cursor: default;
    /* background-color: #eff1f1; */
  }

  .DayPicker-Day {
    padding: 0 0.7em;
    height: 35px;
    width: 35px;
  }

  &.DayPicker-wrapper {
    box-shadow: none;
    padding: 0;
  }

  .DayPickerInput-Overlay {
    box-shadow: none;
  }

  .DayPicker-Month {
    margin: 0;
  }

  .DayPicker-Months {
    padding: 1em;
  }
`

export const StyledDatepickerWrapper = styled(StyledFieldWrapper)`
  margin-bottom: 0;
`
