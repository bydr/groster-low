import {
  ComponentClass,
  ForwardedRef,
  forwardRef,
  ReactElement,
  SFC,
  useEffect,
  useState,
} from "react"
import DayPickerInput from "react-day-picker/DayPickerInput"
import { DayModifiers, DayPickerInputProps, Modifier } from "react-day-picker"
import "react-day-picker/lib/style.css"
import { Field } from "../Field"
import { Button } from "../../Button"
import { NavbarElementProps } from "react-day-picker/types/Props"
import { cx } from "@linaria/core"
import {
  cssNavbar,
  StyledDatepickerWrapper,
  StyledFieldDatepicker,
} from "./StyledDatepicker"
import { FieldVariantsPropsType } from "../../../types/types"
import { MONTHS, WEEKDAYS_LONG, WEEKDAYS_SHORT } from "../../../utils/constants"
import { dateToString } from "../../../utils/helpers"

const FIRST_DAY_OF_WEEK = {
  ru: 1,
}
// Translate aria-labels
const LABELS = {
  ru: { nextMonth: "следующий месяц", previousMonth: "предыдущий месяц" },
}

const Navbar:
  | ReactElement<Partial<NavbarElementProps>>
  | ComponentClass<NavbarElementProps>
  | SFC<NavbarElementProps> = ({
  onPreviousClick,
  onNextClick,
  className,
  locale,
  month,
}) => {
  return (
    <div className={cx(cssNavbar, className)}>
      <Button
        type={"button"}
        variant={"arrow"}
        icon={"AngleLeft"}
        onClick={() => onPreviousClick()}
      />
      {MONTHS[locale][month.getMonth()]}
      <Button
        type={"button"}
        variant={"arrow"}
        icon={"AngleRight"}
        onClick={() => onNextClick()}
      />
    </div>
  )
}

export const DatepickerField = forwardRef<
  ForwardedRef<HTMLInputElement>,
  Omit<DayPickerInputProps, "value onDayChange"> & {
    value?: Date
    onDayChange: (day: Date) => void
  } & Pick<FieldVariantsPropsType, "errorMessage" | "required"> & {
      disabledDays?: Modifier[]
    }
>(
  (
    { onDayChange, errorMessage, disabledDays, required = false, ...props },
    ref,
  ) => {
    const [selectedDay, setSelectedDay] = useState<Date>()
    const [formatDay, setFormatDay] = useState<string>()

    const handleDayChange: (
      selectedDay: Date,
      modifiers: DayModifiers,
      dayPickerInput: DayPickerInput,
    ) => void = (selectedDay) => {
      setSelectedDay(selectedDay)
    }

    useEffect(() => {
      if (selectedDay !== undefined) {
        setFormatDay(dateToString(selectedDay))
        if (onDayChange) {
          onDayChange(selectedDay)
        }
      }
    }, [selectedDay])

    return (
      <>
        <StyledDatepickerWrapper data-required={required}>
          <StyledFieldDatepicker>
            <DayPickerInput
              {...props}
              dayPickerProps={{
                selectedDays: selectedDay,
                disabledDays: [
                  {
                    daysOfWeek: [0],
                  },
                  {
                    before: new Date(),
                  },
                  ...(disabledDays || []),
                ],
                locale: "ru",
                months: MONTHS["ru"],
                weekdaysLong: WEEKDAYS_LONG["ru"],
                weekdaysShort: WEEKDAYS_SHORT["ru"],
                firstDayOfWeek: FIRST_DAY_OF_WEEK["ru"],
                labels: LABELS["ru"],
                navbarElement: Navbar,
                captionElement: <></>,
              }}
              onDayChange={(day, DayModifiers, dayPickerInput) => {
                handleDayChange(day, DayModifiers, dayPickerInput)
              }}
              component={Field}
              value={formatDay}
              inputProps={{
                readOnly: true,
                errorMessage: errorMessage,
                ref: ref,
              }}
            />
          </StyledFieldDatepicker>
        </StyledDatepickerWrapper>
      </>
    )
  },
)

DatepickerField.displayName = "DatepickerField"
