import { styled } from "@linaria/react"
import {
  borderRadiusControl,
  colors,
  transitionTimingFunction,
} from "../../styles/utils/vars"
import { getTypographyBase, Paragraph12 } from "../Typography/Typography"
import { ButtonBase, ButtonBox } from "../Button/StyledButton"
import { Input } from "reakit"
import { cssIcon } from "../Icon"
import { StyledSelectInputWrapper } from "../Select/StyledSelect"
import { css } from "@linaria/core"

export const Placeholder = styled.label`
  ${getTypographyBase("p14")};
  position: absolute;
  left: 18px;
  top: 14px;
  color: ${colors.grayDark};
  transition: all 0.2s ${transitionTimingFunction};
  cursor: text;
  user-select: none;
  pointer-events: none;
`
export const StyledHint = styled(Paragraph12)`
  color: ${colors.grayDark};
`
export const StyledTextarea = styled.textarea`
  resize: none;
  min-height: 60px;
  -webkit-appearance: none;
  appearance: none;
`
export const StyledInput = styled(Input)`
  -webkit-appearance: none;
  appearance: none;
`

export const StyledFieldInput = styled.div`
  position: relative;
  display: flex;
  background: ${colors.white};
  border-radius: inherit;

  ${ButtonBase} {
    margin: 0;
  }

  > .${cssIcon} {
    position: absolute;
    right: 16px;
    bottom: 12px;
    fill: ${colors.brand.purple};
    pointer-events: none;
  }
`
export const StyledFieldTextarea = styled(StyledFieldInput)``

export const StyledFieldWrapper = styled.div`
  position: relative;
  border-radius: 4px;
  margin: 0 0 20px 0;

  ${StyledInput}, ${StyledTextarea}, ${StyledSelectInputWrapper} [role="combobox"] {
    & ~ ${ButtonBase} {
      &:not(${ButtonBox}) {
        &:before {
          display: none;
          content: none;
        }
      }
    }
  }

  ${StyledInput}, ${StyledTextarea}, ${StyledSelectInputWrapper} [role="combobox"] {
    ${getTypographyBase("p14")};
    line-height: 24px;
    background: ${colors.white};
    border: 1px solid ${colors.gray};
    border-radius: ${borderRadiusControl};
    padding: 10px 16px 6px 18px;
    min-height: 56px;
    width: 100%;
    margin-bottom: 0;

    &::placeholder {
      color: ${colors.gray};
    }

    & ~ ${ButtonBase} {
      border-radius: 0 4px 4px 0;
      border: 1px solid ${colors.gray};
      border-left: none;
      height: auto;

      &:hover,
      &:active {
        border-color: ${colors.gray};
      }

      &:not(${ButtonBox}) {
        position: relative;
        border-color: transparent;

        &:before {
          content: "";
          top: -5px;
          bottom: -5px;
          left: 0;
          right: -5px;
          background: transparent;
          border: 1px solid ${colors.gray};
          border-radius: 0 50px 50px 0;
          border-left: none;
          position: absolute;
        }
      }
    }

    &[disabled] {
      background: ${colors.grayLight};
    }

    &:invalid,
    &:user-invalid,
    &:required:invalid {
      border-color: ${colors.red};

      & ~ ${ButtonBase} {
        border-color: ${colors.red};
      }
    }

    &:focus {
      border-color: ${colors.brand.yellow};
      & ~ ${ButtonBase} {
        border-color: ${colors.brand.yellow};

        &:not(${ButtonBox}) {
          border-color: transparent;

          &:before {
            border-color: ${colors.brand.yellow};
          }
        }
      }
    }
  }

  ${StyledTextarea} {
    min-height: 60px;
  }

  &[data-isanimating-label="true"] {
    ${StyledInput}, ${StyledTextarea} {
      padding-top: 24px;

      &:focus,
      &:not(:placeholder-shown) {
        ~ {
          ${Placeholder} {
            ${getTypographyBase("p10")};
            top: 10px;
            transform: none;
            line-height: 14px;
          }
        }
      }
    }

    > ${StyledFieldTextarea}:before {
      content: "";
      position: absolute;
      left: 1px;
      top: 1px;
      right: 18px;
      height: 24px;
      background: inherit;
      border-radius: inherit;
      border-bottom-right-radius: 0;
      border-bottom-left-radius: 0;
      pointer-events: none;
      cursor: inherit;
    }
  }

  &[data-isanimating-label="false"] {
    ${StyledInput}, ${StyledTextarea} {
      padding: 10px 18px;
    }
  }

  &[data-iswith-button="true"] {
    ${StyledInput}, ${StyledTextarea}, ${StyledSelectInputWrapper} [role="combobox"] {
      border-top-right-radius: 0;
      border-bottom-right-radius: 0;
      border-right: none;
    }

    ${ButtonBase} {
      margin: 0;
      &:active {
        transform: none;

        .${cssIcon} {
          transform: scale(0.98);
        }
      }
    }
  }

  &[data-iserror="true"] {
    ${StyledInput}, ${StyledTextarea}, ${StyledSelectInputWrapper} [role="combobox"] {
      border-color: ${colors.red};

      & ~ ${ButtonBase} {
        border-color: ${colors.red};

        &:not(${ButtonBox}) {
          border-color: ${colors.red};
        }
      }
    }
  }

  &[data-required="true"] {
    &:after {
      content: "*";
      color: ${colors.red};
      position: absolute;
      z-index: 1;
      left: 5px;
      top: 5px;
      font-size: 22px;
      line-height: 100%;
    }
  }
`

export const cssButtonFieldClear = css`
  &,
  &${ButtonBase}, ${StyledFieldWrapper} &,
  ${StyledFieldWrapper} &${ButtonBase} {
    border-radius: 0;
    padding: 0 8px 0 0;
    border-right: 0;

    .${cssIcon} {
      fill: ${colors.grayDarkLight};
      background: ${colors.grayLight};
      border-radius: 50px;
      padding: 2px;
    }

    &:hover {
      .${cssIcon} {
        fill: ${colors.grayDark};
      }
    }
  }
`
