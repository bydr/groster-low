import { FC } from "react"
import { Field } from "./Field"
import { PATTERN_PASSWORD } from "../../validations/password"
import { FieldProps } from "../../types/types"

export const PasswordField: FC<
  FieldProps & {
    register: any
    isConfirm?: boolean
    comparisonVal?: string
  }
> = ({
  placeholder,
  errorMessage,
  register,
  name = "password",
  comparisonVal,
  isConfirm = false,
}) => {
  return (
    <>
      <Field
        placeholder={placeholder}
        type={"password"}
        withAnimatingLabel
        errorMessage={errorMessage}
        {...register(name, {
          required: {
            value: true,
            message: "Поле обязательно для заполнения",
          },
          pattern: {
            value: PATTERN_PASSWORD.value,
            message: PATTERN_PASSWORD.message,
          },
          validate: isConfirm
            ? (value: string) =>
                comparisonVal === value || "Пароли не совпадают"
            : undefined,
        })}
      />
    </>
  )
}
