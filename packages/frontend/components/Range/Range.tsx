import type { FC } from "react"
import { memo, useCallback, useEffect, useMemo, useState } from "react"
import "rc-slider/assets/index.css"
import { PriceRangeItemType } from "../../store/reducers/catalogSlice"
import {
  StyledRange,
  StyledRangeContainer,
  StyledRangeInput,
  StyledRangeWrapper,
} from "./StyledRange"
import { Field } from "../Field/Field"
import { Typography } from "../Typography/Typography"
import { colors } from "../../styles/utils/vars"
import { priceToFormat } from "../../utils/helpers"
import { CURRENCY } from "../../utils/constants"

export const Range: FC<{
  onAfterChange?: (value?: number[]) => void
  defaultValue: PriceRangeItemType
  value?: PriceRangeItemType
  isFormatPrice?: boolean
}> = memo(
  ({
    defaultValue: defaultValueOut,
    value: valueOut,
    onAfterChange,
    isFormatPrice = true,
  }) => {
    const valMax = useMemo(() => valueOut?.max, [valueOut?.max])
    const valMin = useMemo(() => valueOut?.min, [valueOut?.min])
    const defMax = useMemo(() => defaultValueOut.max, [defaultValueOut.max])
    const defMin = useMemo(() => defaultValueOut.min, [defaultValueOut.min])

    const [localValue, setLocalValue] = useState<PriceRangeItemType>({
      min: defMin || 0,
      max: defMax || 0,
    })

    const onAfterChangeHandle = useCallback(() => {
      if (
        (localValue.min === defMin && localValue.max === defMax) ||
        (valMax === localValue.max && valMin === localValue.min)
      ) {
        return
      }

      console.log("obChange val ", localValue.min, "  ", localValue.max)
      if (onAfterChange !== undefined) {
        onAfterChange([localValue.min, localValue.max])
      }
    }, [
      defMax,
      defMin,
      localValue.max,
      localValue.min,
      onAfterChange,
      valMax,
      valMin,
    ])

    useEffect(() => {
      if (valMax !== undefined && valMin !== undefined) {
        setLocalValue({
          min: valMin,
          max: valMax,
        })
      } else {
        setLocalValue({
          min: defMin,
          max: defMax,
        })
      }
    }, [defMax, defMin, setLocalValue, valMin, valMax])

    return (
      <>
        <StyledRangeContainer>
          <StyledRangeWrapper>
            <Typography variant={"p10"}>
              {isFormatPrice ? priceToFormat(defMin) : defMin} {CURRENCY}
            </Typography>
            <StyledRange
              allowCross={false}
              value={[localValue.min, localValue.max]}
              min={+defMin}
              max={+defMax}
              onChange={(val) => {
                setLocalValue({
                  min: +(val[0] || 0),
                  max: +(val[1] || 0),
                })
              }}
              onAfterChange={onAfterChangeHandle}
            />
            <Typography variant={"p10"}>
              {isFormatPrice ? priceToFormat(defMax) : defMax} {CURRENCY}
            </Typography>
          </StyledRangeWrapper>

          <StyledRangeInput>
            <Typography variant={"p12"} color={colors.grayDark}>
              от
            </Typography>
            <Field
              name={"priceMin"}
              value={
                isFormatPrice ? priceToFormat(localValue.min) : localValue.min
              }
              withAnimatingLabel={false}
              onChange={(e) => {
                setLocalValue({
                  min: isFormatPrice
                    ? +(e.target.value || 0) * 100
                    : +(e.target.value || 0),
                  max: localValue.max,
                })
              }}
              onBlur={onAfterChangeHandle}
            />
          </StyledRangeInput>
          <StyledRangeInput>
            <Typography variant={"p12"} color={colors.grayDark}>
              до
            </Typography>
            <Field
              name={"priceMax"}
              value={
                isFormatPrice ? priceToFormat(localValue.max) : localValue.max
              }
              withAnimatingLabel={false}
              onChange={(e) => {
                setLocalValue({
                  min: localValue.min,
                  max: isFormatPrice
                    ? +(e.target.value || 0) * 100
                    : +(e.target.value || 0),
                })
              }}
              onBlur={onAfterChangeHandle}
            />
          </StyledRangeInput>
        </StyledRangeContainer>
      </>
    )
  },
)

Range.displayName = "Range"
