import { styled } from "@linaria/react"
import { StyledFieldWrapper, StyledInput } from "../Field/StyledField"
import { TypographyBase } from "../Typography/Typography"
import { colors } from "../../styles/utils/vars"
import { Range } from "rc-slider"

export const StyledRange = styled(Range)`
  .rc-slider-track,
  .rc-slider-handle {
    transition: all 0.1s ease-in-out;
  }
  .rc-slider-track {
    background: ${colors.brand.purple};
  }
  .rc-slider-handle {
    width: 18px;
    height: 18px;
    margin-top: -7px;
    border-color: ${colors.brand.purple};
    &:active {
      box-shadow: 0 0 0 6px ${colors.brand.purpleTransparent15};
    }
  }
  .rc-slider-handle-dragging.rc-slider-handle-dragging.rc-slider-handle-dragging {
    border-color: ${colors.brand.purple};
    box-shadow: 0 0 0 6px ${colors.brand.purpleTransparent15};
  }
  .rc-slider-rail {
    background: ${colors.grayLight};
  }
`
export const StyledRangeInput = styled.div`
  grid-row: 2;
  margin: 0;
  position: relative;

  &:first-child {
    grid-column: 1/1;
  }
  &:last-child {
    grid-column: 2/2;
  }

  ${StyledFieldWrapper} {
    margin: 0;
    ${StyledInput} {
      padding-left: 30px;
      min-height: 40px;
    }
  }

  > ${TypographyBase} {
    position: absolute;
    left: 10px;
    top: 50%;
    transform: translateY(-50%);
    margin-top: 2px;
    z-index: 1;
  }
`
export const StyledRangeContainer = styled.div`
  display: grid;
  grid-template-columns: 1fr 1fr;
  gap: 20px 10px;
`

export const StyledRangeWrapper = styled.div`
  grid-row: 1;
  grid-column: 1 / -1;
  display: flex;
  align-items: center;
  width: 100%;
  justify-content: space-between;

  > ${TypographyBase} {
    color: ${colors.grayDark};
  }

  ${StyledRange} {
    flex: 1;
    margin: 0 15px;
  }
`
