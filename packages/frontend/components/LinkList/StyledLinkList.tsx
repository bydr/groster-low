import { styled } from "@linaria/react"
import { StyledList, StyledListItem } from "../List/StyledList"
import { colors } from "../../styles/utils/vars"
import { css } from "@linaria/core"
import { cssIcon } from "../Icon"
import { StyledLinkBase } from "../Link/StyledLink"

export const StyledLinkList = styled(StyledList)`
  flex-direction: column;
  align-items: flex-start;
  margin-bottom: 20px;
`

export const StyledLinkListItem = styled(StyledListItem)`
  ${StyledLinkBase} {
    align-items: flex-start;

    > .${cssIcon} {
      margin-top: 2px;
    }
  }

  &:last-child(2) {
    margin-bottom: 24px;
  }
`
export const cssCLSocials = css`
  &${StyledList} {
    margin-top: 10px;
    flex-direction: row;
    align-items: center;

    ${StyledListItem} {
      margin: 11px;

      &:first-child {
        margin-left: 0;
      }

      .${cssIcon} {
        margin: 0;
      }
    }
  }

  ${StyledLinkBase} {
    &:hover,
    &:active {
      .${cssIcon} {
        fill: ${colors.brand.purple};
      }
    }
  }
`

export const cssListItemPurple = css`
  ${StyledListItem} {
    color: ${colors.brand.purple};

    &:hover,
    &:active {
      color: ${colors.brand.purpleDarken};
    }
  }
`
