import { DetailedHTMLProps, FC, HTMLAttributes } from "react"
import { Icon } from "../Icon"
import { LinkItemType } from "../../types/types"
import { Link } from "../Link"
import { StyledLinkList, StyledLinkListItem } from "./StyledLinkList"

const LinkListItem: FC<LinkItemType> = ({
  icon,
  path,
  title,
  target,
  fillIcon,
}) => {
  return (
    <StyledLinkListItem>
      <Link href={path} target={target}>
        <Icon NameIcon={icon} fill={fillIcon} />
        {title}
      </Link>
    </StyledLinkListItem>
  )
}

type LinkListPropsType = {
  items: LinkItemType[]
}

const LinkList: FC<
  LinkListPropsType &
    DetailedHTMLProps<HTMLAttributes<HTMLUListElement>, HTMLUListElement>
> = ({ items, ...props }) => {
  const contactElements = items.map((c, index) => (
    <LinkListItem
      key={index}
      icon={c.icon}
      path={c.path}
      title={c.title}
      fillIcon={c.fillIcon}
      target={c.target}
    />
  ))
  return <StyledLinkList {...props}>{contactElements}</StyledLinkList>
}

export default LinkList
