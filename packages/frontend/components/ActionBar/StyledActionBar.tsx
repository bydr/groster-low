import { styled } from "@linaria/react"
import { colors } from "../../styles/utils/vars"
import { getTypographyBase } from "../Typography/Typography"
import { VariantActionBarType } from "./ActionBar"
import { cssIcon } from "../Icon"
import { ButtonBox } from "../Button/StyledButton"

export const ActionBarContainer = styled.div`
  width: 100%;
  margin-bottom: 6px;
`

export const Description = styled.div`
  ${getTypographyBase("span")};
  margin: 0;
`

export const StyledActionBar = styled.article<{
  variant?: VariantActionBarType
}>`
  ${getTypographyBase("p12")};
  color: ${colors.grayDark};
  background-color: ${(props) => {
    switch (props.variant) {
      case "blue": {
        return colors.brand.blueTransparent1
      }
      case "red": {
        return colors.brand.orangeTransparent1
      }
      default: {
        return colors.brand.blueTransparent1
      }
    }
  }};
  display: inline-flex;
  align-items: center;
  border-radius: 6px;
  padding: 4px 12px;
  margin-right: 4px;
  margin-bottom: 4px;

  > * {
    ${getTypographyBase("p12")};
    margin-bottom: 0;
    display: inline-flex;
    align-items: center;
  }

  ${ButtonBox} {
    width: 30px;
    height: 30px;
    margin-left: 10px;
  }

  .${cssIcon} {
    margin-right: 8px;
    fill: ${colors.brand.purple};
  }
`
