import type { FC, ReactNode } from "react"
import { Description, StyledActionBar } from "./StyledActionBar"
import { Icon, IconNameType } from "../Icon"

export type VariantActionBarType = "blue" | "red"

export const ActionBar: FC<{
  icon?: IconNameType
  description: string | ReactNode
  variant?: VariantActionBarType
}> = ({ icon, description, variant = "blue" }) => {
  return (
    <>
      <StyledActionBar variant={variant}>
        {icon && <Icon NameIcon={icon} size={"medium"} />}
        {!!description && (
          <>
            <Description>{description}</Description>
          </>
        )}
      </StyledActionBar>
    </>
  )
}
