import type { FC } from "react"
import Head from "next/head"
import { TITLE_SITE_RU } from "../../utils/constants"

const TITLE_DEFAULT = `${TITLE_SITE_RU} - товары для бизнеса оптом по низким ценам`
const DESCRIPTION_DEFAULT = `Товары для бизнеса в Волгограде в интернет-магазине «Гростер». ✔ Широкий ассортимент. ✔ Доставка по РФ. ✔ Индивидуальные предложения для клиентов. ☎  +7 (8442) 20-36-35`

export const Meta: FC<{
  title?: string
  description?: string
  ogTags?: {
    description?: string
    title?: string
    url?: string
    image?: string
  }
}> = ({ title, description, ogTags }) => {
  return (
    <>
      <Head>
        <meta httpEquiv="X-UA-Compatible" content="IE=edge" />
        <meta
          name="viewport"
          content="width=device-width, initial-scale=1, maximum-scale=1"
        />
        <title>{title || TITLE_DEFAULT}</title>
        <meta property="title" content={title || TITLE_DEFAULT} />
        <meta
          property="description"
          content={description || DESCRIPTION_DEFAULT}
        />
        <meta property="og:type" content={"website"} />
        <meta property="og:site_name" content={TITLE_SITE_RU} />
        <meta
          property="og:description"
          content={ogTags?.description || description || DESCRIPTION_DEFAULT}
        />
        <meta
          property="og:title"
          content={ogTags?.title || title || TITLE_DEFAULT}
        />
        <meta property="og:url" content={ogTags?.url} />
        <meta property="og:image" content={ogTags?.image} />
      </Head>
    </>
  )
}
