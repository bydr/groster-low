import { styled } from "@linaria/react"
import { getTypographyBase } from "../Typography/Typography"

export const StyledEntityImage = styled.div`
  ${getTypographyBase("p12")};
  display: block;
  position: relative;
  justify-content: center;
  margin: 0 auto;
  width: auto;
  align-items: center;
  overflow: hidden;
  text-align: center;
  max-width: 100%;

  img {
    object-fit: contain;
    object-position: center top;
  }

  svg {
    margin: 0;
  }

  * {
    font-size: inherit;
    line-height: inherit;
  }
`
