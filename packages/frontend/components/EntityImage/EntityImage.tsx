import type { FC } from "react"
import Image, { ImageProps } from "next/image"
import { Icon } from "../Icon"
import { StyledEntityImage } from "./Styled"
import { useState } from "react"
import { BaseLoader } from "../Loaders/BaseLoader/BaseLoader"

type ImagePropsType = Omit<ImageProps, "src" | "draggable"> & {
  imagePath?: string
  imageAlt?: string
  withPreloader?: boolean
  isStatic?: boolean
}
export const EntityImage: FC<ImagePropsType> = ({
  imagePath,
  imageAlt = "",
  priority,
  width,
  height,
  withPreloader,
  isStatic = false,
  placeholder,
  ...props
}) => {
  const [isLoading, setIsLoading] = useState(!!withPreloader)

  const src = isStatic
    ? imagePath || ""
    : imagePath?.includes("http") || ""
    ? imagePath || ""
    : `https://${imagePath}`

  return (
    <StyledEntityImage>
      {imagePath !== undefined && imagePath.length > 0 ? (
        <>
          {isLoading && <BaseLoader />}
          <Image
            src={src}
            {...props}
            draggable={false}
            alt={imageAlt}
            priority={priority}
            width={width}
            height={height}
            onLoadingComplete={() => {
              setIsLoading(false)
            }}
            onError={() => {
              setIsLoading(false)
            }}
          />
        </>
      ) : (
        <div
          style={{
            width: "100%",
            height: "100%",
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
          }}
        >
          <Icon NameIcon={"NoImage"} size={"largeL"} />
        </div>
      )}
    </StyledEntityImage>
  )
}
