import { FC, useMemo } from "react"
import { OrderThankType } from "../../../types/types"
import { Content, Name, StyledOrderThank, Value } from "../StyledOrders"
import { Typography } from "../../Typography/Typography"
import {
  cssListVertical,
  StyledList,
  StyledListItem,
} from "../../List/StyledList"
import { CURRENCY } from "../../../utils/constants"
import Price from "../../Products/parts/Price/Price"

export const OrderThank: FC<{ order: OrderThankType | null }> = ({ order }) => {
  const recipient = useMemo(
    () => [order?.fio, order?.phone].filter((i) => i !== undefined).join(", "),
    [order?.fio, order?.phone],
  )
  const shipping = useMemo(
    () =>
      [order?.shippingMethod, order?.shippingAddress]
        .filter((i) => i !== undefined)
        .join(", "),
    [order?.shippingAddress, order?.shippingMethod],
  )

  return (
    <>
      {order !== null && (
        <>
          <StyledOrderThank>
            <Typography>
              Ваш заказ {!!order.id ? `№ ${order.id}` : ``} обрабатывается.
            </Typography>
            <StyledList className={cssListVertical}>
              {recipient.length > 0 && (
                <>
                  <StyledListItem>
                    <Name>Получатель</Name>
                    <Content>
                      <Value>
                        <Typography variant={"h5"}>{recipient}</Typography>
                      </Value>
                    </Content>
                  </StyledListItem>
                </>
              )}

              {!!order.email && (
                <>
                  <StyledListItem>
                    <Name>Почта</Name>
                    <Content>{order.email}</Content>
                  </StyledListItem>
                </>
              )}

              {!!order.contact && (
                <>
                  <StyledListItem>
                    <Name>Контактные данные: </Name>
                    <Content>{order.contact}</Content>
                  </StyledListItem>
                </>
              )}

              {shipping.length > 0 && (
                <>
                  <StyledListItem>
                    <Name>Доставка</Name>
                    <Content>
                      <Value>
                        <Typography>{shipping}</Typography>
                        {order.shippingDate && (
                          <Typography>{order.shippingDate}</Typography>
                        )}
                      </Value>
                    </Content>
                  </StyledListItem>
                </>
              )}

              <StyledListItem>
                <Name>Сумма заказа</Name>
                <Content>
                  <Value>
                    <Typography>
                      <Price
                        variant={"span"}
                        price={order.productsCost || 0}
                        currency={CURRENCY}
                        isClean
                      />
                    </Typography>
                  </Value>
                  {!!order.promocode && (
                    <Value aria-disabled>
                      <Price
                        variant={"span"}
                        price={order.discount || 0}
                        currency={CURRENCY}
                        isClean
                        isSale
                      />
                    </Value>
                  )}
                  {order.shippingCost !== undefined && (
                    <>
                      <Value aria-disabled>
                        <Typography>
                          <Price
                            variant={"span"}
                            price={order.shippingCost || 0}
                            currency={CURRENCY}
                            isClean
                            prefix={"+"}
                            messageImportant={"бесплатно"}
                          />
                          <Typography variant={"span"}>, доставка</Typography>
                        </Typography>
                      </Value>
                    </>
                  )}
                </Content>
              </StyledListItem>

              <StyledListItem>
                <Name>
                  <Typography variant={"default"}>Итого</Typography>
                </Name>
                <Content>
                  <Value>
                    <Typography variant={"default"}>
                      <Price
                        variant={"span"}
                        price={order.totalCost || 0}
                        currency={CURRENCY}
                        isClean
                      />
                    </Typography>
                  </Value>
                </Content>
              </StyledListItem>
            </StyledList>
          </StyledOrderThank>
        </>
      )}
    </>
  )
}
