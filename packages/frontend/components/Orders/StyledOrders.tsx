import { styled } from "@linaria/react"
import { getTypographyBase } from "../Typography/Typography"
import { StyledList, StyledListItem } from "../List/StyledList"
import { breakpoints, colors } from "../../styles/utils/vars"
import { ButtonBase } from "../Button/StyledButton"

export const Name = styled.div`
  ${getTypographyBase("p14")};
  grid-area: name;

  @media (max-width: ${breakpoints.sm}) {
    color: ${colors.grayDark};
  }
`
export const Content = styled.div`
  ${getTypographyBase("p14")};
  grid-area: content;
`
export const Value = styled.div`
  ${getTypographyBase("p14")};

  &[aria-disabled] {
    color: ${colors.grayDark};
  }

  > * {
    color: inherit;
    margin-bottom: 0;
  }
`

export const StyledOrderThank = styled.section`
  ${getTypographyBase("p14")};

  ${StyledList} {
    margin: 0 0 14px 0;
  }

  ${StyledListItem} {
    width: 100%;
    display: grid;
    grid-template-areas: "name content";
    grid-template-columns: 4fr 8fr;
    grid-column-gap: 14px;
  }

  @media (max-width: ${breakpoints.sm}) {
    ${StyledListItem} {
      grid-template-areas: "name" "content";
      grid-template-columns: 1fr;
    }
  }
`

export const StyledThankYouPage = styled.div`
  padding-bottom: 48px;

  @media (max-width: ${breakpoints.sm}) {
    ${ButtonBase} {
      width: 100%;
    }
  }
`
