import { FC, ReactNode } from "react"
import { useDisclosureState } from "reakit"
import {
  StyledDisclosure,
  StyledDisclosureContent,
  StyledToggle,
  StyledToggleContent,
} from "./StyledToggle"
import { Typography } from "../Typography/Typography"
import { Icon } from "../Icon"

type ToggleType = {
  title: string
  content: string | ReactNode
}

export const Toggle: FC<ToggleType> = ({ title, content }) => {
  const disclosure = useDisclosureState({
    animated: 800,
  })
  return (
    <>
      <StyledToggle>
        <StyledDisclosure {...disclosure}>
          <Typography variant={"span"}>{title}</Typography>
          <Icon NameIcon={"AngleBottom"} />
        </StyledDisclosure>
        <StyledDisclosureContent {...disclosure}>
          <StyledToggleContent
            dangerouslySetInnerHTML={{
              __html: `${content || ""}`,
            }}
          />
        </StyledDisclosureContent>
      </StyledToggle>
    </>
  )
}
