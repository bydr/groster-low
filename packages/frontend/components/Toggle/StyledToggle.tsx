import { styled } from "@linaria/react"
import { Disclosure, DisclosureContent } from "reakit"
import { getTypographyBase } from "../Typography/Typography"
import { colors, transitionTimingFunction } from "../../styles/utils/vars"
import { cssIcon } from "../Icon"

export const StyledToggleContent = styled.div`
  width: 100%;
`
export const StyledDisclosure = styled(Disclosure)`
  ${getTypographyBase("p14")};
  padding: 24px 0;
  gap: 0 16px;
  display: flex;
  width: 100%;
  align-items: center;
  justify-content: space-between;
  position: relative;
  z-index: 2;
  background: ${colors.white};
  appearance: none;
  outline: none;
  box-shadow: none;
  border: none;
  cursor: pointer;
  border-top: 1px solid ${colors.gray};
  text-align: left;

  > * {
    margin: 0;
  }

  .${cssIcon} {
    fill: ${colors.brand.purple};
    transition: all 0.2s ease-in-out;
  }

  &[aria-expanded="true"] {
    border-top-color: transparent;

    .${cssIcon} {
      transform: rotate(180deg);
    }
  }
`
export const StyledDisclosureContent = styled(DisclosureContent)`
  ${getTypographyBase("p14")};
  transition: opacity 250ms ${transitionTimingFunction},
    transform 250ms ${transitionTimingFunction};
  opacity: 0;
  transform: translate3d(0, -100%, 0);
  padding-bottom: 24px;
  will-change: transform;

  &[data-enter] {
    opacity: 1;
    transform: translate3d(0, 0, 0);
  }
  &[data-leave] {
  }
`
export const StyledToggle = styled.article`
  width: 100%;
  position: relative;
  overflow: hidden;

  &:first-child {
    ${StyledDisclosure} {
      border-top-color: transparent;
    }
  }

  ${StyledDisclosureContent} {
    transition: opacity 0.5s ${transitionTimingFunction},
      transform 0.5s ${transitionTimingFunction};
  }
`
