import type { FC } from "react"
import { Typography } from "../Typography/Typography"
import { colors } from "../../styles/utils/vars"
import { Icon, IconNameType } from "../Icon"
import type { AvailableStatusType } from "../../types/types"
import { StyledAvailable, StyledAvailableInner } from "./StyledAvailable"
import { useApp } from "../../hooks/app"

type AvailablePropsType = {
  status?: AvailableStatusType | null
  isHideDeliveryIndicator?: boolean
}

const Layout: FC<{
  title: string
  iconColor: string
  iconName: IconNameType
  isHideDeliveryIndicator: boolean
}> = ({ iconColor, isHideDeliveryIndicator, title, iconName }) => {
  const { location } = useApp()

  return (
    <>
      <Icon NameIcon={iconName} />
      <Typography variant={"p12"}>{title}</Typography>
      {location?.city === "Волгоград" && (
        <>
          {!isHideDeliveryIndicator && (
            <Icon NameIcon={"Car"} fill={iconColor} />
          )}
        </>
      )}
    </>
  )
}

const AvailableItem: FC<
  Omit<AvailablePropsType, "status"> & { status: AvailableStatusType }
> = ({ status, isHideDeliveryIndicator = false }) => {
  switch (status) {
    case "many": {
      return (
        <>
          <Layout
            title={"Много"}
            iconColor={colors.brand.green}
            iconName={"StatsMany"}
            isHideDeliveryIndicator={isHideDeliveryIndicator}
          />
        </>
      )
    }
    case "few": {
      return (
        <>
          <Layout
            title={"Мало"}
            iconColor={colors.brand.yellow}
            iconName={"StatsFew"}
            isHideDeliveryIndicator={isHideDeliveryIndicator}
          />
        </>
      )
    }
    case "not": {
      return (
        <>
          <Layout
            title={"Нет в наличии"}
            iconColor={colors.red}
            iconName={"StatsNot"}
            isHideDeliveryIndicator={isHideDeliveryIndicator}
          />
        </>
      )
    }
  }
}

const Available: FC<AvailablePropsType> = ({ status, ...props }) => {
  return (
    <>
      {!!status && (
        <>
          <StyledAvailable>
            <StyledAvailableInner>
              <AvailableItem {...props} status={status} />
            </StyledAvailableInner>
          </StyledAvailable>
        </>
      )}
    </>
  )
}

export default Available
