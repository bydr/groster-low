import { styled } from "@linaria/react"
import { colors, sizeSVG } from "../../styles/utils/vars"
import { cssIcon, getSizeSVG } from "../Icon"
import { TypographyBase } from "../Typography/Typography"
import { StyledTableContainer } from "../Products/Detail/StyledDetail"

export const StyledAvailableInner = styled.div`
  width: 100%;
  display: flex;
  align-items: center;
  justify-content: center;
`

export const StyledAvailable = styled.div`
  color: ${colors.grayDark};
  display: inline-flex;
  align-items: center;
  justify-content: center;
  width: 100%;

  ${StyledTableContainer} & {
    justify-content: flex-start;
  }

  ${StyledAvailableInner} {
    justify-content: inherit;

    ${TypographyBase} {
      margin: 0 10px 0 6px;
      line-height: 115%;
    }

    .${cssIcon} {
      ${getSizeSVG(sizeSVG.medium)}
    }
  }
`
