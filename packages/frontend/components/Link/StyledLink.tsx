import { styled } from "@linaria/react"
import { colors } from "../../styles/utils/vars"

export const StyledLinkBase = styled.a`
  appearance: none;
  text-decoration: none;
  display: inline-block;
  color: ${colors.brand.purple};

  &:hover,
  &:active {
    color: ${colors.brand.purpleDarken};
  }
`

export const StyledLinkBlackToPurple = styled(StyledLinkBase)`
  color: ${colors.black};
  &:hover,
  &:active {
    color: ${colors.brand.purple};
  }
`

export const StyledLinkGrayDarkToBlack = styled(StyledLinkBase)`
  color: ${colors.grayDark};
  &:hover,
  &:active {
    color: ${colors.black};
  }
`
export type LinkVariants = "black-to-purple" | "gray-dark-to-black"

export const createLink = (variant?: LinkVariants): typeof StyledLinkBase => {
  switch (variant) {
    case "black-to-purple":
      return StyledLinkBlackToPurple
    case "gray-dark-to-black":
      return StyledLinkGrayDarkToBlack
    default:
      return StyledLinkBase
  }
}
