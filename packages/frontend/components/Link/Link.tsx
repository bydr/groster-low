import type { BaseHTMLAttributes, FC } from "react"
import NextLink, { LinkProps } from "next/link"
import { LinkVariants, createLink } from "./StyledLink"

export const Link: FC<
  Omit<LinkProps, "passHref"> & {
    variant?: LinkVariants
  } & BaseHTMLAttributes<HTMLAnchorElement>
> = ({ href, children, variant, className, ...rest }) => {
  const LinkComponent = createLink(variant)

  return (
    <NextLink href={href} {...rest} className={className} passHref>
      <LinkComponent
        title={rest.title}
        target={rest.target}
        className={className}
      >
        {children}
      </LinkComponent>
    </NextLink>
  )
}

export type { LinkVariants }
