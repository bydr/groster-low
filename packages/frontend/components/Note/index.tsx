import { FC } from "react"
import { StyledMain, StyledNote, StyledSide } from "./StyledNote"

export const Note: FC<{ sideChildren?: JSX.Element }> = ({
  children,
  sideChildren,
}) => {
  return (
    <>
      <StyledNote>
        <StyledMain>{children}</StyledMain>
        {sideChildren && (
          <>
            <StyledSide>{sideChildren}</StyledSide>
          </>
        )}
      </StyledNote>
    </>
  )
}
