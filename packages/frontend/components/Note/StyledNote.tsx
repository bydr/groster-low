import { styled } from "@linaria/react"
import { breakpoints } from "../../styles/utils/vars"
import { StyledNotice } from "./Notice/StyledNotice"

export const StyledMain = styled.div`
  grid-area: main;
`
export const StyledSide = styled.div`
  grid-area: side;
`

export const StyledNote = styled.div`
  width: 100%;
  display: grid;
  grid-template-columns: 8fr 4fr;
  grid-template-areas: "main side";
  gap: 20px 110px;
  margin-bottom: 40px;

  ${StyledMain} > *:last-child {
    margin-bottom: 0;
  }

  ${StyledNotice} + ${StyledNotice} {
    margin-top: 40px;
  }

  @media (max-width: ${breakpoints.xl}) {
    gap: 20px 60px;
  }

  @media (max-width: ${breakpoints.md}) {
    grid-template-areas: "main main" "side side";
    margin-bottom: 20px;
  }
`
