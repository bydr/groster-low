import { styled } from "@linaria/react"
import { getTypographyBase, Heading3 } from "../../Typography/Typography"
import { colors } from "../../../styles/utils/vars"

export const Title = styled(Heading3)`
  color: ${colors.black};
`

export const Line = styled.div`
  position: relative;
  width: 64px;
  height: 2px;
  min-height: 2px;
  background: ${colors.brand.yellow};
  margin-bottom: 16px;
  padding: 0;
`

export const StyledNotice = styled.article`
  ${getTypographyBase("p12")};
  width: 100%;
  position: relative;
  color: ${colors.grayDark};
  margin: 0;

  > *:not(${Title}):not(${Line}) {
    ${getTypographyBase("span")};
  }
`
