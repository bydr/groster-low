import { FC } from "react"
import { Line, StyledNotice, Title } from "./StyledNotice"

type NoticeType = {
  title?: string
}

export const Notice: FC<NoticeType> = ({ title, children }) => {
  return (
    <>
      <StyledNotice>
        <Line />
        {title && <Title>{title}</Title>}
        {children}
      </StyledNotice>
    </>
  )
}
