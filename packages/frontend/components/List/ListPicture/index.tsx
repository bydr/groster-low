import { BaseHTMLAttributes, FC } from "react"
import { StyledItemDescription, StyledItemTitle } from "../StyledList"
import { Icon, IconNameType } from "../../Icon"
import {
  StyledContent,
  StyledListPicture,
  StyledPicture,
  StyledPictureListItem,
} from "./Styled"

export type ListPictureItemType = {
  icon?: IconNameType
  title: string
  description: string
}

export const ListPicture: FC<
  {
    items: ListPictureItemType[]
  } & BaseHTMLAttributes<HTMLUListElement>
> = ({ items, ...props }) => {
  return (
    <>
      <StyledListPicture {...props}>
        {items.map((item, index) => (
          <StyledPictureListItem key={index}>
            {item.icon !== undefined && (
              <StyledPicture>
                <Icon NameIcon={item.icon} size={"largeM"} />
              </StyledPicture>
            )}
            <StyledContent>
              <StyledItemTitle>{item.title}</StyledItemTitle>
              <StyledItemDescription>{item.description}</StyledItemDescription>
            </StyledContent>
          </StyledPictureListItem>
        ))}
      </StyledListPicture>
    </>
  )
}
