import { styled } from "@linaria/react"
import { breakpoints, colors } from "../../../styles/utils/vars"
import { cssIcon } from "../../Icon"
import { StyledList, StyledListItem } from "../StyledList"

export const StyledPicture = styled.div`
  min-width: 64px;
  min-height: 64px;
  width: 64px;
  height: 64px;
  border-radius: 50px;
  background: ${colors.grayLight};
  display: flex;
  align-items: center;
  justify-content: center;

  .${cssIcon} {
    fill: ${colors.brand.yellow};
  }
`
export const StyledContent = styled.div`
  margin-top: 16px;
`

export const StyledPictureListItem = styled(StyledListItem)`
  gap: 16px;
  flex: 1;
`
export const StyledListPicture = styled(StyledList)`
  gap: 2px 24px;
  width: 100%;

  @media (max-width: ${breakpoints.xl}) {
    flex-direction: column;
    align-items: flex-start;
    justify-content: flex-start;
  }
`
