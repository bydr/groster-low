import { styled } from "@linaria/react"
import { getTypographyBase, Paragraph14 } from "../Typography/Typography"
import {
  breakpoints,
  colors,
  TypographyVariantsType,
} from "../../styles/utils/vars"
import { cssIcon } from "../Icon"
import { StyledPopoverDisclosure } from "../Popover/StyledPopover"
import { StyledLinkBase } from "../Link/StyledLink"
import { css } from "@linaria/core"

// export const Counter = styled(Span)`
//   ${getTypographyBase("p12")};
//   display: inline-block;
//   color: ${colors.grayDark};
//   margin: 0 0 0 4px;
// `
export type StyledListPropsType = {
  isDefault?: boolean
  variantTypography?: TypographyVariantsType
}

export const StyledListItem = styled.li`
  color: ${colors.black};
  margin-bottom: 0;
  display: flex;

  ${StyledPopoverDisclosure}, ${StyledLinkBase} {
    font-size: inherit;
    line-height: inherit;
    color: inherit;
  }

  ${StyledLinkBase} {
    display: flex;
    width: 100%;
    align-items: center;

    .${cssIcon} {
      margin-right: 8px;
    }

    > p {
      width: 100%;
      margin-bottom: 0;
    }
  }

  > * {
    font-size: inherit;
    line-height: inherit;
    font-weight: inherit;
    color: inherit;
  }
`

export const cssListVertical = css``

export const StyledList = styled.ul<StyledListPropsType>`
  ${getTypographyBase("p14")};
  padding: ${(props) => (props.isDefault ? "0 0 0 16px" : "0")};
  margin: 0;
  list-style: ${(props) => (props.isDefault ? "initial" : "none")};
  display: flex;
  flex-direction: row;
  align-items: center;

  &.${cssListVertical} {
    flex-direction: column;
    align-items: flex-start;

    ${StyledListItem} {
      margin-bottom: 8px;
    }
  }

  ${StyledListItem} {
    font-size: inherit;
    line-height: inherit;
    font-weight: inherit;
    display: ${(props) => (props.isDefault ? "list-item" : "flex")};
  }

  ${StyledListItem} & {
    display: flex;
    flex-direction: column;
    align-items: flex-start;
  }

  @media (max-width: ${breakpoints.md}) {
    flex-direction: column;
    align-items: flex-start;
    justify-content: flex-start;
  }
`

export const StyledItemTitle = styled.p`
  ${getTypographyBase("h3")};
`

export const StyledItemDescription = styled(Paragraph14)``
