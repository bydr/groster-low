import { FC, useState } from "react"
import { TypographyVariantsType } from "../../../styles/utils/vars"
import { Typography } from "../../Typography/Typography"
import { StyledBannerContainer } from "./StyledBannerHeader"
import { Button } from "../../Button"
import dynamic, { DynamicOptions } from "next/dynamic"
import { ModalDefaultPropsType } from "../../Modals/Modal"
import { useRouter } from "next/router"
import { FormConfigExtendsType } from "../../../types/types"

const DynamicModal = dynamic(
  (() =>
    import("../../Modals/Modal").then(
      (mod) => mod.Modal,
    )) as DynamicOptions<ModalDefaultPropsType>,
  {
    ssr: false,
  },
)

const SendCustomMailForm = dynamic(
  (() =>
    import("../../Forms/SendCustomMail").then(
      (mod) => mod.SendCustomMail,
    )) as DynamicOptions<FormConfigExtendsType>,
  {
    ssr: false,
  },
)

type BannerHeaderPropsType = {
  text: string
  variantTypography?: TypographyVariantsType
  href: string
}

export const BannerHeader: FC<BannerHeaderPropsType> = ({
  variantTypography = "h5",
  text,
  href,
}) => {
  const router = useRouter()
  const [isShowModal, setIsShowModal] = useState(false)

  return (
    <>
      <StyledBannerContainer>
        <Button onClick={() => setIsShowModal(true)} />
        <Typography variant={variantTypography}>{text}</Typography>
      </StyledBannerContainer>
      <DynamicModal
        title={"Прежде чем перейти"}
        variant={"rounded-0"}
        closeMode={"destroy"}
        isShowModal={isShowModal}
        onClose={() => {
          setIsShowModal(false)
        }}
      >
        <SendCustomMailForm
          successMessage={
            "Благодарим! Сейчас Вы будете направлены на старую версию сайта!"
          }
          cbOnSubmit={() => {
            void router.push(href)
          }}
        />
      </DynamicModal>
    </>
  )
}
