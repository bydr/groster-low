import { styled } from "@linaria/react"
import { colors } from "../../../styles/utils/vars"
import { TypographyBase } from "../../Typography/Typography"
import { StyledLinkBase } from "../../Link/StyledLink"
import { ButtonBase } from "../../Button/StyledButton"

export const StyledBannerContainer = styled.section`
  position: relative;
  height: 40px;
  max-height: 40px;
  background-color: ${colors.brand.yellow};
  color: ${colors.brand.purple};
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;

  ${TypographyBase}, > * {
    margin: 0;
    padding: 0;
    color: inherit;
  }

  ${StyledLinkBase}, ${ButtonBase} {
    position: absolute;
    left: 0;
    top: 0;
    z-index: 1;
    width: 100%;
    height: 100%;
    background-color: transparent;
    text-align: center;
  }
`
