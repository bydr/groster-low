import { FC } from "react"
import { Container } from "../../../styles/utils/StyledGrid"
import { Banner } from "../Banner"
import { RequestBanner } from "../../../../contracts/contracts"
import { StyledBannerSection } from "../Banner/Styled"

export const SingleUnderCatalog: FC<{ banner?: RequestBanner }> = ({
  banner,
}) => {
  return (
    <>
      {!!banner && banner.desktop && (
        <>
          <StyledBannerSection>
            <Container>
              <Banner
                image={{
                  desktop: {
                    src: banner.desktop,
                    width: 1464,
                    height: 208,
                    layout: "responsive",
                  },
                  tablet: {
                    src: banner.tablet,
                    width: 704,
                    height: 208,
                    layout: "responsive",
                  },
                  mobile: {
                    src: banner.mobile,
                    width: 345,
                    height: 208,
                    layout: "responsive",
                  },
                }}
                url={banner.url}
              />
            </Container>
          </StyledBannerSection>
        </>
      )}
    </>
  )
}
