import { StyledBanner } from "../../Banner/Styled"
import { css } from "@linaria/core"

export const cssBannerSlide = css`
  &,
  &${StyledBanner} {
    border-radius: 0;

    img {
      border-radius: 0;
    }
  }
`
