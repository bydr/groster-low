import { styled } from "@linaria/react"
import { colors } from "../../../styles/utils/vars"
import { cssBullet, cssBulletActive } from "../../Swiper/StyledSwiper"

export const StyledSliderInner = styled.div`
  width: 100%;
  background: ${colors.grayLighter};
`
export const StyledSlider = styled.section`
  width: 100%;
  position: relative;
  display: flex;
  justify-content: center;
  overflow: hidden;

  .swiper-pagination-bullets,
  .swiper-pagination-bullets.swiper-pagination-horizontal {
    bottom: 10px;
    left: 50%;
    transform: translateX(-50%);
    background: rgba(255, 255, 255, 0.3);
    padding: 0 10px;
    border-radius: 50px;
    min-height: initial;
    box-sizing: border-box;
    display: flex;
    align-items: center;
    height: 20px;
    width: auto;
  }

  .${cssBullet} {
    background: ${colors.white};
    width: 6px;
    height: 6px;
    min-height: 6px;
    min-width: 6px;
    margin-right: 12px;
    transition: all 0.5s ease-in-out;
    cursor: pointer;

    &:last-child {
      margin-right: 0;
    }

    &.${cssBulletActive} {
      width: 16px;
      min-width: 16px;
      background: ${colors.brand.purple};
    }
  }
`
