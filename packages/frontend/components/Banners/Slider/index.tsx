import { BaseHTMLAttributes, FC } from "react"
import { StyledSlider, StyledSliderInner } from "./Styled"
import { BannerSlide } from "./Slide"
import { RequestBanner } from "../../../../contracts/contracts"
import { SwiperWrapper } from "../../Swiper/SwiperWrapper"
import { SwiperSlide } from "swiper/react"
import { Autoplay, Pagination, EffectCreative, Keyboard } from "swiper"
import { cssBullet, cssBulletActive } from "../../Swiper/StyledSwiper"

export const SliderBanners: FC<
  {
    banners?: RequestBanner[]
  } & BaseHTMLAttributes<HTMLDivElement>
> = ({ banners, ...props }) => {
  return (
    <>
      {!!banners && banners.length > 0 && (
        <>
          <StyledSlider {...props}>
            <StyledSliderInner>
              <SwiperWrapper
                loop={true}
                modules={[EffectCreative, Autoplay, Pagination, Keyboard]}
                pagination={{
                  bulletClass: cssBullet,
                  bulletActiveClass: cssBulletActive,
                  clickable: false,
                }}
                speed={900}
                keyboard={true}
                autoplay={{
                  delay: 3000,
                  pauseOnMouseEnter: true,
                  disableOnInteraction: false,
                }}
                effect={"creative"}
                creativeEffect={{
                  prev: {
                    translate: ["-20%", 0, -1],
                  },
                  next: {
                    translate: ["100%", 0, 0],
                  },
                }}
              >
                {[...banners]
                  .reverse()
                  .sort((a, b) => {
                    const valA = a.weight || 0
                    const valB = b.weight || 0
                    return valA < valB ? -1 : valA > valB ? 1 : 0
                  })
                  .map((b, index) => (
                    <SwiperSlide key={index}>
                      <BannerSlide banner={b} priority />
                    </SwiperSlide>
                  ))}
              </SwiperWrapper>
            </StyledSliderInner>
          </StyledSlider>
        </>
      )}
    </>
  )
}
