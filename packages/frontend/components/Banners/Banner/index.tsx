import { BaseHTMLAttributes, createElement, FC, useMemo, useState } from "react"
import { BannerType } from "../../../types/types"
import {
  cssDesktop,
  cssMobile,
  cssTablet,
  cssWithHover,
  StyledBanner,
  StyledImage,
  StyledInner,
} from "./Styled"
import Image from "next/image"
import { getImagePath } from "../../../utils/helpers"
import { cx } from "@linaria/core"
import dynamic, { DynamicOptions } from "next/dynamic"
import { Button } from "../../Button"
import { Link } from "../../Link"
import { ModalDefaultPropsType } from "../../Modals/Modal"

const NotFindNeedForm = dynamic(
  (() =>
    import("../../Forms/NotFindNeed").then(
      (mod) => mod.NotFindNeedForm,
    )) as DynamicOptions,
  {
    ssr: false,
  },
)

const DynamicModal = dynamic(
  (() =>
    import("../../Modals/Modal").then(
      (mod) => mod.Modal,
    )) as DynamicOptions<ModalDefaultPropsType>,
  {
    ssr: false,
  },
)

type ModalConfigType = Record<
  string,
  Pick<ModalDefaultPropsType, "variant" | "title"> & {
    Component: JSX.Element
  }
>

const MODAL_CONFIG: ModalConfigType = {
  notfindneed: {
    title: "Не нашли нужного?",
    variant: "rounded-0",
    Component: <NotFindNeedForm />,
  },
}

export const DEFAULT_PATH = "/"

export const Banner: FC<BannerType & BaseHTMLAttributes<HTMLDivElement>> = ({
  url,
  withHover = true,
  className,
  image,
  quality = 100,
  priority,
  unoptimized,
  ...props
}) => {
  const isModal = useMemo(() => (url || DEFAULT_PATH)[0] === "#", [url])
  const path = useMemo(
    () =>
      isModal
        ? (url || DEFAULT_PATH).slice(1).toLowerCase()
        : url || DEFAULT_PATH,
    [isModal, url],
  )
  const [isShowModal, setIsShowModal] = useState(false)

  return (
    <>
      <StyledBanner
        {...props}
        className={cx(className, withHover && cssWithHover)}
      >
        <StyledInner>
          {isModal ? (
            <>
              <Button
                variant={"link"}
                onClick={() => {
                  setIsShowModal(true)
                }}
              />
            </>
          ) : (
            <>
              <Link
                href={isModal ? "" : url || "/"}
                draggable={false}
                scroll
                target={"_blank"}
              />
            </>
          )}

          <StyledImage className={cssDesktop}>
            <Image
              alt={""}
              src={getImagePath(image.desktop.src.toString())}
              objectPosition={image.desktop.objectPosition || "center"}
              objectFit={image.desktop.objectFit || "contain"}
              layout={image.desktop.layout}
              width={image.desktop.width}
              height={image.desktop.height}
              priority={priority}
              quality={quality}
              unoptimized={unoptimized}
            />
          </StyledImage>
          <StyledImage className={cssTablet}>
            <Image
              alt={""}
              src={getImagePath(
                (image.tablet?.src || image.desktop.src).toString(),
              )}
              objectPosition={image.tablet ? "center" : "right"}
              objectFit={image.tablet ? "contain" : "cover"}
              width={image.tablet?.width}
              height={image.tablet?.height}
              priority={priority}
              quality={quality}
              layout={image.tablet?.layout}
              unoptimized={unoptimized}
            />
          </StyledImage>
          <StyledImage className={cssMobile}>
            <Image
              alt={""}
              src={getImagePath(
                (image.mobile?.src || image.desktop.src).toString(),
              )}
              objectPosition={image.mobile ? "center" : "right"}
              objectFit={image.mobile ? "contain" : "cover"}
              width={image.mobile?.width}
              height={image.mobile?.height}
              priority={priority}
              layout={image.mobile?.layout}
              quality={quality}
              unoptimized={unoptimized}
            />
          </StyledImage>
        </StyledInner>

        {isModal && MODAL_CONFIG[path] !== undefined && (
          <DynamicModal
            title={MODAL_CONFIG[path].title}
            variant={MODAL_CONFIG[path].variant}
            closeMode={"destroy"}
            isShowModal={isShowModal}
            onClose={() => {
              setIsShowModal(false)
            }}
          >
            {createElement(() => MODAL_CONFIG[path].Component)}
          </DynamicModal>
        )}
      </StyledBanner>
    </>
  )
}
