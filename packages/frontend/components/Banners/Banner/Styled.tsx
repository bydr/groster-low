import { css } from "@linaria/core"
import { styled } from "@linaria/react"
import { breakpoints, colors } from "../../../styles/utils/vars"
import { StyledLinkBase } from "../../Link/StyledLink"
import { Section } from "../../../styles/utils/Utils"
import { ButtonBase } from "../../Button/StyledButton"

export const cssDesktop = css``
export const cssMobile = css``
export const cssTablet = css``

export const cssWithHover = css``

export const StyledImage = styled.div``

export const StyledBanner = styled.div`
  width: 100%;
  position: relative;
  display: flex;
  overflow: hidden;
  border-radius: 12px;
  transition: all 0.2s ease-in-out;

  img {
    border-radius: 12px;
  }

  &.${cssWithHover} {
    &:before {
      content: "";
      position: absolute;
      left: 0;
      top: 0;
      width: 100%;
      height: 100%;
      opacity: 0;
      visibility: hidden;
      background: ${colors.brand.purpleDarken};
      transition: all 0.2s ease-in-out;
      z-index: 2;
    }

    &:hover,
    &:active {
      &:before {
        opacity: 0.3;
        visibility: visible;
      }
    }

    &:active {
      transform: scale(0.98);
    }
  }
`
export const StyledInner = styled.div`
  position: relative;
  display: block;
  width: 100%;
  text-align: center;

  ${StyledImage} {
    width: 100%;
    &.${cssDesktop} {
      display: block;
    }
    &.${cssTablet} {
      display: none;
    }
    &.${cssMobile} {
      display: none;
    }

    @media (max-width: ${breakpoints.md}) {
      &.${cssDesktop} {
        display: none;
      }
      &.${cssTablet} {
        display: block;
      }
      &.${cssMobile} {
      }
    }

    @media (max-width: ${breakpoints.xs}) {
      &.${cssDesktop} {
      }
      &.${cssTablet} {
        display: none;
      }
      &.${cssMobile} {
        display: block;
      }
    }
  }

  > ${StyledLinkBase}, > ${ButtonBase} {
    position: absolute;
    z-index: 3;
    width: 100%;
    height: 100%;
    top: 0;
    left: 0;
    background: transparent;
    user-select: none;
    -webkit-user-drag: none;
  }
`

export const StyledBannerSection = styled(Section)`
  width: 100%;
`
