import { styled } from "@linaria/react"
import { cssIcon } from "../../Icon"
import { colors } from "../../../styles/utils/vars"

export const StyledSpin = styled.div`
  z-index: 1;
  display: flex;
  align-items: center;
  justify-content: center;
  text-align: center;
  border-radius: inherit;

  .${cssIcon} {
    fill: ${colors.brand.purple};
    animation-name: spin;
    animation-duration: 1s;
    animation-iteration-count: infinite;
    animation-timing-function: linear;
    will-change: transform, animation;
  }

  @keyframes spin {
    from {
      transform: rotate(0deg);
    }
    to {
      transform: rotate(360deg);
    }
  }
`
