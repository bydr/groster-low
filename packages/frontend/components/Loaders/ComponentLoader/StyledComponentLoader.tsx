import { StyledLoaderContainer } from "../BaseLoader/StyledBaseLoader"
import { styled } from "@linaria/react"
import { cssIcon } from "../../Icon"
import { colors } from "../../../styles/utils/vars"
import { StyledSpin } from "../Spin/StyledSpin"

export const StyledComponentLoaderContainer = styled(StyledLoaderContainer)`
  ${StyledSpin} {
    .${cssIcon} {
      fill: ${colors.brand.purple}
      width: 100%;
      height: 100%;
      max-width: 50px;
      max-height: 50px;
      min-width: 50px;
      min-height: 50px;
    }
  }
`
