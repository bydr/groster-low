import type { FC } from "react"
import { StyledLoaderOverlay } from "../BaseLoader/StyledBaseLoader"
import { StyledComponentLoaderContainer } from "./StyledComponentLoader"
import { Spin } from "../Spin"

export const ComponentLoader: FC = () => {
  return (
    <>
      <StyledComponentLoaderContainer>
        <Spin />
        <StyledLoaderOverlay />
      </StyledComponentLoaderContainer>
    </>
  )
}
