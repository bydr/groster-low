import { styled } from "@linaria/react"
import { colors } from "../../../styles/utils/vars"
import { StyledSpin } from "../Spin/StyledSpin"
import { cssIcon, getSizeSVG } from "../../Icon"
import { css } from "@linaria/core"

export const cssIsFixed = css``

export const StyledLoaderOverlay = styled.div`
  position: absolute;
  background: ${colors.white};
  opacity: 0.8;
  width: 100%;
  height: 100%;
  left: 0;
  top: 0;
  border-radius: inherit;
`

export const StyledLoaderContent = styled.div`
  position: relative;
  color: ${colors.brand.purple};
  z-index: 2;

  ${StyledSpin} {
    .${cssIcon} {
      ${getSizeSVG("60px")};
    }
  }
`

export const StyledLoaderContainer = styled.div`
  position: absolute;
  z-index: 8;
  width: 100%;
  height: 100%;
  left: 0;
  top: 0;
  display: flex;
  align-items: center;
  justify-content: center;
  border-radius: inherit;

  ${StyledSpin} {
    .${cssIcon} {
      ${getSizeSVG("100px")};
    }
  }

  &.${cssIsFixed} {
    position: fixed;
    z-index: 10;
  }
`
