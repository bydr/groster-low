const withLinaria = require("next-linaria")
const withBundleAnalyzer = require("@next/bundle-analyzer")({
  enabled: process.env.ANALYZE === "true",
})
// const env = process.env.NODE_ENV

const securityHeaders = [
  {
    key: "X-DNS-Prefetch-Control",
    value: "on",
  },
  {
    key: "Strict-Transport-Security",
    value: "max-age=63072000; includeSubDomains; preload",
  },
  {
    key: "X-XSS-Protection",
    value: "1; mode=block",
  },
  {
    key: "X-Frame-Options",
    value: "SAMEORIGIN",
  },
  {
    key: "Permissions-Policy",
    value: "camera=(), microphone=(), geolocation=(), interest-cohort=()",
  },
  {
    key: "X-Content-Type-Options",
    value: "nosniff",
  },
  {
    key: "Referrer-Policy",
    value: "origin-when-cross-origin",
  },
]

// if (env === "production") {
//   securityHeaders.push({
//     key: "Content-Security-Policy",
//     value: "default-src 'self'",
//   })
// }

/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: true,
  productionBrowserSourceMaps: true,
  compress: true,
  async headers() {
    return [
      {
        // Apply these headers to all routes in your application.
        source: "/(.*)",
        headers: securityHeaders,
      },
    ]
  },
  experimental: { esmExternals: true },
  typescript: {
    /** @todo remove after reakit typing fix */
    ignoreBuildErrors: true,
  },
  poweredByHeader: true,
  images: {
    domains: ["static.groster.me", "static.grosternew.vertical-web.ru"],
    minimumCacheTTL: 31536000,
  },
}

module.exports = withBundleAnalyzer(withLinaria(nextConfig))
