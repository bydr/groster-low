import { PatternType } from "../types/types"

export const MIN_LENGTH_PASSWORD = 8

export const PATTERN_PASSWORD: PatternType = {
  value: new RegExp(
    `(?=.*[a-zA-Z])[0-9a-zA-Z!@#$%^&*]{${MIN_LENGTH_PASSWORD},}`,
  ),
  message: `Минимум ${MIN_LENGTH_PASSWORD} символов, минимум одна буква и одна цифра`,
}
