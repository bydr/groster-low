import { ValidateRuleType } from "../types/types"

export const MINLENGTH_OGRNIP: ValidateRuleType = {
  value: 13,
  message: "Минимум 13 знаков для юр. лиц",
}

export const MAXLENGTH_OGRNIP: ValidateRuleType = {
  value: 15,
  message: "Максимум 15 знаков для физ. лиц",
}

export const MAXLENGTH_INN: ValidateRuleType = {
  value: 12,
  message: "Максимум 12 знаков",
}

export const MINLENGTH_INN: ValidateRuleType = {
  value: 10,
  message: "Минимум 10 знаков",
}

export const getPureValue = (phone: string): string => {
  return phone.replace(/\D/g, "")
}

export const normalizeValue = (v: string, limit: number): string => {
  let normalize = ""
  for (let i = 0; i < limit; i++) {
    normalize += `${v[i] || "_"}`
  }

  return normalize
}

export const nextSelectionPosition = (purePhone: string): number => {
  return purePhone.length
}
