import { getClientUser, getServerUser, setUser, User } from "../hooks/auth"
import { fetchRefreshToken } from "../api/authAPI"
import { setTokenStorage } from "../hooks/cart"
import { ApiError } from "../../contracts/contracts"
import { IncomingMessage, ServerResponse } from "http"
import { NextApiRequestCookies } from "next/dist/server/api-utils"
import jwtDecode from "jwt-decode"
import { BASE_URL, BASE_VERSION_URL } from "../utils/constants"

type RequestOptionsType = RequestInit

export type FetcherBasePropsType = {
  server?: boolean
  req?: IncomingMessage & { cookies: NextApiRequestCookies }
  res?: ServerResponse
}

const normalizeUrl = (url: string) =>
  url.includes("http") ? url : BASE_VERSION_URL + url

export const getAbsolutePath = (isServer?: boolean): string =>
  isServer ? `${BASE_URL}${BASE_VERSION_URL}` : ""

const getUser = ({
  req,
}: {
  req?: IncomingMessage & { cookies: NextApiRequestCookies }
}) => {
  let user
  if (req !== undefined) {
    user = getServerUser({ req })
  } else {
    user = getClientUser()
  }

  return user
}

const updateToken = async ({
  res,
  accessToken,
  refreshToken,
  user,
}: {
  res?: ServerResponse
  accessToken: string
  refreshToken: string
  user: User
}) => {
  const tokenDecode:
    | {
        user?: string
        type?: string
        exp?: number
      }
    | undefined = jwtDecode(accessToken)
  if (!!tokenDecode?.exp) {
    if (Date.now() + 1000000 >= tokenDecode.exp * 1000) {
      const newToken = await fetchRefreshToken({
        refresh_token: refreshToken,
      }).catch(() => {
        setUser(null, res)
        setTokenStorage(null, res)

        if (res !== undefined) {
          res.setHeader("location", `/`)
          res.statusCode = 302
        } else {
          window?.location.replace("/")
        }
      })

      if (newToken) {
        setUser(
          {
            cart: newToken.info?.cart || null,
            accessToken: newToken.access_token || null,
            refreshToken: newToken.refresh_token || null,
            email: user.email,
            phone: user.phone,
            isAdmin: user.isAdmin,
            fio: user.fio,
          },
          res,
        )
        setTokenStorage(newToken.info?.cart || null, res)
      }
    }
  }
}

const getHeadersWithAuth = <T>(
  requestOptions: T,
  isAuth?: boolean,
  req?: IncomingMessage & { cookies: NextApiRequestCookies },
  res?: ServerResponse,
): T => {
  if (!isAuth) {
    return requestOptions
  }

  const user = getUser({
    req: req,
  })

  if (
    user === null ||
    user.accessToken === null ||
    user.refreshToken === null
  ) {
    return requestOptions
  }

  if (user.accessToken) {
    void updateToken({
      res: res,
      user: user,
      refreshToken: user.refreshToken,
      accessToken: user.accessToken,
    })
    requestOptions["headers"]["Authorization"] = user.accessToken
  }

  return requestOptions
}

export const get = <R>(
  url: string,
  isAuth?: boolean,
  req?: IncomingMessage & { cookies: NextApiRequestCookies },
  res?: ServerResponse,
): Promise<R> => {
  const requestOptions: RequestOptionsType = {
    method: "GET",
    headers: {},
  }
  getHeadersWithAuth(requestOptions, isAuth, req, res)

  return fetch(normalizeUrl(url), requestOptions).then((response) =>
    handleResponse(response, undefined, req, res),
  )
}

export const post = <R, Q>(
  url: string,
  body: Q,
  isAuth?: boolean,
  key?: string,
  req?: IncomingMessage & { cookies: NextApiRequestCookies },
  res?: ServerResponse,
): Promise<R> => {
  const requestOptions: RequestOptionsType = {
    method: "POST",
    headers: { "Content-Type": "application/json" },
    body: JSON.stringify(body),
  }
  getHeadersWithAuth(requestOptions, isAuth, req, res)

  return fetch(normalizeUrl(url), requestOptions).then((response) =>
    handleResponse(response, key, req, res),
  )
}

export const put = <R, Q>(
  url: string,
  body: Q,
  isAuth?: boolean,
): Promise<R> => {
  const requestOptions: RequestOptionsType = {
    method: "PUT",
    headers: { "Content-Type": "application/json" },
    body: JSON.stringify(body),
  }
  getHeadersWithAuth(requestOptions, isAuth)

  return fetch(normalizeUrl(url), requestOptions).then((response) =>
    handleResponse(response),
  )
}

export const deleteFetch = <R, Q>(
  url: string,
  body: Q,
  isAuth?: boolean,
  key?: string,
): Promise<R> => {
  const requestOptions: RequestOptionsType = {
    mode: "cors",
    method: "DELETE",
    headers: { "Content-Type": "application/json" },
    body: JSON.stringify(body),
  }
  getHeadersWithAuth(requestOptions, isAuth)

  return fetch(normalizeUrl(url), requestOptions).then((response) =>
    handleResponse(response, key),
  )
}

const handleResponse = async <T>(
  response: Response,
  key?: string,
  req?: IncomingMessage & { cookies: NextApiRequestCookies },
  res?: ServerResponse,
): Promise<T> => {
  const status = response.status

  if (status === 204) {
    return null as unknown as T
  }

  if (status === 401) {
    const user = getUser({
      req: req,
    })

    if (user !== null) {
      void updateToken({
        res: res,
        user: user,
        refreshToken: user.refreshToken || "",
        accessToken: user.accessToken || "",
      })
    }
  }

  if (response.status === 418) {
    throw {
      status: 418,
      message: response.url,
    } as ApiError
  }

  if (!response.ok) {
    const res = await response.json()
    throw {
      ...res,
      status: response.status,
      message: res?.message || "Произошла ошибка",
    } as ApiError
  }

  return (await response.json()) as T
}
