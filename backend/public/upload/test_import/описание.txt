﻿ProductsPrices.xml - Товары и их описание
	ProductInformation - ифнформация о товарах
		BusinessAreas - сферы бизнеса
			BusinessArea - описание сферы бизнеса IDBusinessArea - id сферы бизнеса NameBusinessArea - наименование сферы бизнеса ActiveBusinessArea - активность сферы бизнеса
		ProductSections - разделы товаров
			ProductSection - раздел товара IDProductSection - id раздела товаров NameProductSection - наименование раздела товаров ActiveProductSection - признак активности раздела товаров ParentIDProductSection - id родительского раздела товаров
		Products - товары	
			Product - товар ActiveProduct - активность товара IDProduct - id товара NameProduct - наименование товара MainPicture - id основной картинки VerbalDescription - словесное описание товара UnitName - наименование основной единицы измерения ArticleProduct - артикул товара CodeProduct - код товара  Weight - вес товара в килограммах LengthSize - длина товара в сантиметрах WidthSize - ширина товара в сантиметрах HeightSize - высота товара в сантиметрах Size - объем товара в кубических метрах Bestseller - признак Хит CollectionProductUsed - у номенклатуры используются характеристики CollectionSeparateProduct - характеристики товара делят товар на сайте как разные товары Kit - номенклатура является набором (комплектом) PackageName1 - наименование первой тары PackageName2 - наименование второй тары PackageName3 - наименование третьей тары LotName1 - количество товара в первой таре LotName2 - количество товара во второй таре LotName3 - количество товара в третьей таре SellOnlyUnit - продажа только в разрезе единиц измерения
				ProductPictures - картинки товара
					ProductPicture -  картинка товара ActiveProductPicture - активность картинки товара IDProductPicture - id картинки ImageExtensionProductPicture - формат файла картинки ImageFileSizeProductPicture - размер файла FilePathProductPicture - наименование файла NameFileDirectoryProductPicture - путь файла	
				ProductProperties - свойства товара
					ProductProperty - свойство товара IDProductProperty - id свойства NameProductProperty - имя свойства ValueProductProperty - значение свойства	
				StructureKitProduct - описание состава набора (комплекта)
					ValueStructureKitProduct - товар который является частью набора IDKitProduct - id товара который является частью набора
				AnalogsProduct - описание аналогов товара
					AnalogProduct - товар который является аналогом IDAnalogProduct - id товара который является аналогом
				CompanionsProduct - описание сопутствующих товаров
					CompanionProduct - товар который является сопутсвтующим IDCompanionProduct - id товара который является сопутсвтующим
				CollectionsProducts - описание характеристик товара
					CollectionProduct - характеристика товара ActiveCollectionProduct - признак активности характеристики Name - наименование характеристики ArticleCollectionProduct - артикул харатеристики CodeCollectionProduct - код характеристики IDCollectionProduct - id характеристики MainPictureCollectionProduct - id основной картинки характеристики
						CollectionProductPropertes - описание свойств характеристики
							CollectionProductProperty - описание свойства характеристики IDCollectionProductProperty - id свойства характеристики NameCollectionProductProperty - наименование свойства характеристики ValueCollectionProductProperty - значение свойства характеристики		 
						CollectionProductPictures - описание картинок характеристик
							CollectionProductPicture - описание картинки характеристики IDPictureCollectionProduct - id картинки характеристики ActivePictureCollectionProduct - активность картинки характеристики ImageExtensionPictureCollectionProduct - формат файла картинки ImageFileSizePictureCollectionProduct - размер файла FilePathPictureCollectionProduct - наименование файла NameFileDirectoryPictureCollectionProduct - путь файла	
				BusinessAreasProduct - сферы бизнеса товара
					BusinessAreaProduct - сфера бизнеса товара IDBusinessAreaProduct - id сферы бизнеса
				ProductEntrySections - разделы в которые входит продукт
					ProductEntrySection - раздел в который входит продукт ProductSectionID - id раздела
				ProductUnits - единицы измерения товара 
					ProductUnit - описание единицы измерения товара IDProductUnit - id едницы измерения товара ActiveProductUnit - признак активности единицы измерения NameProductUnit - наименование единицы измерения RateProductUnit - количество базовых еидницы в единице изменения товава StandardUnitNameProductUnit - наименование используемой базовой единицы измерения товара WidthSizeProductUnit - ширина единицы измерения товара в сантиметрах HeightSizeProductUnit - высота единицы измерения товара в сантиметрах LengthSizeProductUnit - длина единицы измерения товара в сантиметрах SizeProductUnit - объем единицы измерения товара в кубических метрах WeightProductUnit - вес единицы измерения товара в килогаммах
ProductsPrices.xml - цены товаров и характеристик
	ProductsPrices - цены товаров и характеристик
		PriceTypes - строки видов цен IDMainPriceType - id основного вида цен
			PriceType - строка вида цен IDPriceType - id вида цен NamePriceType - наименование вида цен			
		ProductsPrice - строки товаров и их цен
			Product	- товар IDProduct - id товара
				Prices - строки цены			
					Price - строка цены PriceSet - цена установлена PriceHasCollection - признак того что эта строка является ценой характеристики IDCollectionProduct - id характеристики IDProductsPriceType - id вида цены Date - дата цены Value - значение цены 			
ProductsStock.xml - остатки товаров на складах
	ProductsStock - остатки товаров на складах DateReceiptStock - дата получения остатков 
		Stores - склады товаров
			Store - описание склада ID - id склада Name - наименование склада Active - активность склада ActualAddressValue - фактический адрес в формате JSON ActualAddressRepresentation - представление фактического адреса			
		Products - товары
			Product - товар IDProduct - id товара PriceHasCollection - признак того что эта строка является строкой товара с характеристикой
				Stock - строка остатка StoreID - id склада остатка Value - значение остатка на складе MinimumLevelAvailability - минимально допустимое количество на складе MaximumLevelAvailability - максимальное допустимое количество на складе

		





