<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220121055631 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SEQUENCE shipping_limit_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE shipping_method_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE shipping_limit (id INT NOT NULL, method_id INT NOT NULL, type SMALLINT NOT NULL, order_min_cost INT DEFAULT NULL, shipping_cost INT DEFAULT NULL, region VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_910935DA19883967 ON shipping_limit (method_id)');
        $this->addSql('CREATE TABLE shipping_method (id INT NOT NULL, name VARCHAR(255) NOT NULL, description TEXT DEFAULT NULL, weight INT NOT NULL, cost VARCHAR(255) NOT NULL, use_address BOOLEAN NOT NULL, is_fast BOOLEAN NOT NULL, alias VARCHAR(255) NOT NULL, is_active BOOLEAN DEFAULT \'true\' NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX unique_alias ON shipping_method (alias)');
        $this->addSql('ALTER TABLE shipping_limit ADD CONSTRAINT FK_910935DA19883967 FOREIGN KEY (method_id) REFERENCES shipping_method (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE "order" ADD shipping_method_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE "order" ADD address TEXT DEFAULT NULL');
        $this->addSql('ALTER TABLE "order" ADD shipping_date TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL');
        $this->addSql('ALTER TABLE "order" ADD shipping_cost INT DEFAULT 0 NOT NULL');
        $this->addSql('ALTER TABLE "order" ADD CONSTRAINT FK_F52993985F7D6850 FOREIGN KEY (shipping_method_id) REFERENCES shipping_method (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_F52993985F7D6850 ON "order" (shipping_method_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE "order" DROP CONSTRAINT FK_F52993985F7D6850');
        $this->addSql('ALTER TABLE shipping_limit DROP CONSTRAINT FK_910935DA19883967');
        $this->addSql('DROP SEQUENCE shipping_limit_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE shipping_method_id_seq CASCADE');
        $this->addSql('DROP TABLE shipping_limit');
        $this->addSql('DROP TABLE shipping_method');
        $this->addSql('DROP INDEX IDX_F52993985F7D6850');
        $this->addSql('ALTER TABLE "order" DROP shipping_method_id');
        $this->addSql('ALTER TABLE "order" DROP address');
        $this->addSql('ALTER TABLE "order" DROP shipping_date');
        $this->addSql('ALTER TABLE "order" DROP shipping_cost');
    }
}
