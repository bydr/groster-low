<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20211006044752 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SEQUENCE product_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE product_image_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE product (id INT NOT NULL, uuid VARCHAR(50) NOT NULL, 
                            create_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, 
                            update_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, 
                            is_active BOOLEAN NOT NULL, name VARCHAR(255) NOT NULL, 
                            description TEXT DEFAULT NULL, price INT DEFAULT NULL, 
                            code VARCHAR(15) NOT NULL, unit VARCHAR(10) NOT NULL, 
                            is_sell_unit BOOLEAN NOT NULL, total_qty INT DEFAULT 0,
                            fast_qty INT DEFAULT 0, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE product_image (id INT NOT NULL, product_id INT NOT NULL, path VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_64617F034584665A ON product_image (product_id)');
        $this->addSql('ALTER TABLE product_image ADD CONSTRAINT FK_64617F034584665A FOREIGN KEY (product_id) REFERENCES product (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE product ADD collection_parent_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE product ADD CONSTRAINT FK_D34A04AD54527A12 FOREIGN KEY (collection_parent_id) REFERENCES product (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_D34A04AD54527A12 ON product (collection_parent_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE product DROP CONSTRAINT FK_D34A04AD54527A12');
        $this->addSql('DROP INDEX IDX_D34A04AD54527A12');
        $this->addSql('ALTER TABLE product DROP collection_parent_id');
        $this->addSql('ALTER TABLE product_image DROP CONSTRAINT FK_64617F034584665A');
        $this->addSql('DROP SEQUENCE product_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE product_image_id_seq CASCADE');
        $this->addSql('DROP TABLE product');
        $this->addSql('DROP TABLE product_image');
    }
}
