<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220921052332 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE auth_user_product (auth_user_id INT NOT NULL, product_id INT NOT NULL, PRIMARY KEY(auth_user_id, product_id))');
        $this->addSql('CREATE INDEX IDX_755F13A3E94AF366 ON auth_user_product (auth_user_id)');
        $this->addSql('CREATE INDEX IDX_755F13A34584665A ON auth_user_product (product_id)');
        $this->addSql('ALTER TABLE auth_user_product ADD CONSTRAINT FK_755F13A3E94AF366 FOREIGN KEY (auth_user_id) REFERENCES auth_user (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE auth_user_product ADD CONSTRAINT FK_755F13A34584665A FOREIGN KEY (product_id) REFERENCES product (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE auth_user_product');
    }
}
