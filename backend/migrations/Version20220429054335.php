<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220429054335 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SEQUENCE payment_method_id_seq INCREMENT BY 1 MINVALUE 1 START 5');
        $this->addSql('CREATE TABLE payment_method (id INT NOT NULL, uid VARCHAR(33) NOT NULL, name VARCHAR(255) NOT NULL, weight INT NOT NULL, is_active BOOLEAN NOT NULL, PRIMARY KEY(id))');
        $this->addSql('ALTER TABLE "order" ADD payment_method_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE "order" ADD CONSTRAINT FK_F52993985AA1164F FOREIGN KEY (payment_method_id) REFERENCES payment_method (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_F52993985AA1164F ON "order" (payment_method_id)');

        $this->addSql("INSERT INTO payment_method (id, uid, name, weight, is_active) VALUES
                                                                  (1, 'cash_courier', 'Наличными курьеру', 1, true),
                                                                  (2, 'card_courier', 'Картой курьеру', 2, true),
                                                                  (3, 'card_online', 'Картой онлайн', 3, true),
                                                                  (4, 'bank_transfer', 'Банковский перевод', 4, true);
                                                              ");
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE "order" DROP CONSTRAINT FK_F52993985AA1164F');
        $this->addSql('DROP SEQUENCE payment_method_id_seq CASCADE');
        $this->addSql('DROP TABLE payment_method');
        $this->addSql('DROP INDEX IDX_F52993985AA1164F');
        $this->addSql('ALTER TABLE "order" DROP payment_method_id');
    }
}
