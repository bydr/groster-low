<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220104111941 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE "order" ADD fio VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE "order" ADD email VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE "order" ADD phone VARCHAR(15) DEFAULT NULL');
        $this->addSql('ALTER TABLE "order" ADD bussiness_area VARCHAR(255) DEFAULT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE "order" DROP fio');
        $this->addSql('ALTER TABLE "order" DROP email');
        $this->addSql('ALTER TABLE "order" DROP phone');
        $this->addSql('ALTER TABLE "order" DROP bussiness_area');
    }
}
