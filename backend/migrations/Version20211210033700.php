<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20211210033700 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE product ADD weight VARCHAR(10) DEFAULT NULL');
        $this->addSql('ALTER TABLE product ADD length VARCHAR(10) DEFAULT NULL');
        $this->addSql('ALTER TABLE product ADD width VARCHAR(10) DEFAULT NULL');
        $this->addSql('ALTER TABLE product ADD height VARCHAR(10) DEFAULT NULL');
        $this->addSql('ALTER TABLE product ADD size VARCHAR(10) DEFAULT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE product DROP weight');
        $this->addSql('ALTER TABLE product DROP length');
        $this->addSql('ALTER TABLE product DROP width');
        $this->addSql('ALTER TABLE product DROP height');
        $this->addSql('ALTER TABLE product DROP size');
    }
}
