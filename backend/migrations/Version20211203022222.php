<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20211203022222 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SEQUENCE favorite_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE favorite (id INT NOT NULL, product_id INT NOT NULL, shop_user_id INT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_68C58ED94584665A ON favorite (product_id)');
        $this->addSql('CREATE INDEX IDX_68C58ED9A45D93BF ON favorite (shop_user_id)');
        $this->addSql('ALTER TABLE favorite ADD CONSTRAINT FK_68C58ED94584665A FOREIGN KEY (product_id) REFERENCES product (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE favorite ADD CONSTRAINT FK_68C58ED9A45D93BF FOREIGN KEY (shop_user_id) REFERENCES auth_user (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER INDEX idx_c7a2ec1b6c8a81a9 RENAME TO IDX_C7A2EC1B4584665A');
        $this->addSql('ALTER TABLE order_item ADD sample SMALLINT DEFAULT 0 NOT NULL');
        $this->addSql('ALTER TABLE product ALTER total_qty SET NOT NULL');
        $this->addSql('ALTER TABLE product ALTER fast_qty SET NOT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP SEQUENCE favorite_id_seq CASCADE');
        $this->addSql('DROP TABLE favorite');
        $this->addSql('ALTER TABLE product ALTER total_qty DROP NOT NULL');
        $this->addSql('ALTER TABLE product ALTER fast_qty DROP NOT NULL');
        $this->addSql('ALTER TABLE order_item DROP sample');
        $this->addSql('ALTER INDEX idx_c7a2ec1b4584665a RENAME TO idx_c7a2ec1b6c8a81a9');
    }
}
