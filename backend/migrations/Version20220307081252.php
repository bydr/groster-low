<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220307081252 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE product ADD alias VARCHAR(255) NOT NULL');
        $this->addSql('CREATE UNIQUE INDEX unique_product_uuid ON product (uuid)');
        $this->addSql('CREATE UNIQUE INDEX unique_product_alias ON product (alias)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP INDEX unique_product_uuid');
        $this->addSql('DROP INDEX unique_product_alias');
        $this->addSql('ALTER TABLE product DROP alias');
    }
}
