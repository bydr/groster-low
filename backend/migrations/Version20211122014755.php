<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20211122014755 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SEQUENCE rest_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE store_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE rest (id INT NOT NULL, product_id INT NOT NULL, store_id INT NOT NULL, value INT NOT NULL, update_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_FD1421D04584665A ON rest (product_id)');
        $this->addSql('CREATE INDEX IDX_FD1421D0B092A811 ON rest (store_id)');
        $this->addSql('CREATE TABLE store (id INT NOT NULL, uuid VARCHAR(50) NOT NULL, name VARCHAR(255) NOT NULL, is_active BOOLEAN NOT NULL, is_main BOOLEAN NOT NULL DEFAULT FALSE, PRIMARY KEY(id))');
        $this->addSql('ALTER TABLE rest ADD CONSTRAINT FK_FD1421D04584665A FOREIGN KEY (product_id) REFERENCES product (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE rest ADD CONSTRAINT FK_FD1421D0B092A811 FOREIGN KEY (store_id) REFERENCES store (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
//        $this->addSql('ALTER INDEX idx_c7a2ec1b6c8a81a9 RENAME TO IDX_C7A2EC1B4584665A');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE rest DROP CONSTRAINT FK_FD1421D0B092A811');
        $this->addSql('ALTER TABLE rest DROP CONSTRAINT IDX_FD1421D04584665A');
        $this->addSql('DROP SEQUENCE rest_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE store_id_seq CASCADE');
        $this->addSql('DROP TABLE rest');
        $this->addSql('DROP TABLE store');
//        $this->addSql('ALTER INDEX idx_c7a2ec1b4584665a RENAME TO idx_c7a2ec1b6c8a81a9');
    }
}
