<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20211006052552 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SEQUENCE param_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE value_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE param (id INT NOT NULL, uuid VARCHAR(50) NOT NULL, name VARCHAR(255) NOT NULL, weight SMALLINT NOT NULL DEFAULT 0, is_show BOOLEAN NOT NULL DEFAULT TRUE, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE value (id INT NOT NULL, param_id INT NOT NULL, uuid VARCHAR(50) NOT NULL, name VARCHAR(255) NOT NULL, image VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_1D7758345647C863 ON value (param_id)');
        $this->addSql('ALTER TABLE value ADD CONSTRAINT FK_1D7758345647C863 FOREIGN KEY (param_id) REFERENCES param (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE value DROP CONSTRAINT FK_1D7758345647C863');
        $this->addSql('DROP SEQUENCE param_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE value_id_seq CASCADE');
        $this->addSql('DROP TABLE param');
        $this->addSql('DROP TABLE value');
    }
}
