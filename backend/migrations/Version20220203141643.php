<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220203141643 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SEQUENCE faq_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE faq_type_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE faq (id INT NOT NULL, type_id INT NOT NULL, question TEXT NOT NULL, answer TEXT NOT NULL, weight SMALLINT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_E8FF75CCC54C8C93 ON faq (type_id)');
        $this->addSql('CREATE TABLE faq_type (id INT NOT NULL, name VARCHAR(255) NOT NULL, weight SMALLINT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('ALTER TABLE faq ADD CONSTRAINT FK_E8FF75CCC54C8C93 FOREIGN KEY (type_id) REFERENCES faq_type (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE faq DROP CONSTRAINT FK_E8FF75CCC54C8C93');
        $this->addSql('DROP SEQUENCE faq_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE faq_type_id_seq CASCADE');
        $this->addSql('DROP TABLE faq');
        $this->addSql('DROP TABLE faq_type');
    }
}
