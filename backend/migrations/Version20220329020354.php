<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220329020354 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE waiting_product ADD customer_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE waiting_product ADD CONSTRAINT FK_EBE398999395C3F3 FOREIGN KEY (customer_id) REFERENCES auth_user (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_EBE398999395C3F3 ON waiting_product (customer_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE waiting_product DROP CONSTRAINT FK_EBE398999395C3F3');
        $this->addSql('DROP INDEX IDX_EBE398999395C3F3');
        $this->addSql('ALTER TABLE waiting_product DROP customer_id');
    }
}
