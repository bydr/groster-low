<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20221003062429 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SEQUENCE product_value_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE product_value (id INT NOT NULL, product_id INT NOT NULL, value_id INT NOT NULL, is_filter BOOLEAN NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_BC2B23684584665A ON product_value (product_id)');
        $this->addSql('CREATE INDEX IDX_BC2B2368F920BBA2 ON product_value (value_id)');
        $this->addSql('ALTER TABLE product_value ADD CONSTRAINT FK_BC2B23684584665A FOREIGN KEY (product_id) REFERENCES product (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE product_value ADD CONSTRAINT FK_BC2B2368F920BBA2 FOREIGN KEY (value_id) REFERENCES value (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('DROP TABLE value_product');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP SEQUENCE product_value_id_seq CASCADE');
        $this->addSql('CREATE TABLE value_product (value_id INT NOT NULL, product_id INT NOT NULL, PRIMARY KEY(value_id, product_id))');
        $this->addSql('CREATE INDEX idx_38e871cd4584665a ON value_product (product_id)');
        $this->addSql('CREATE INDEX idx_38e871cdf920bba2 ON value_product (value_id)');
        $this->addSql('ALTER TABLE value_product ADD CONSTRAINT fk_38e871cdf920bba2 FOREIGN KEY (value_id) REFERENCES value (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE value_product ADD CONSTRAINT fk_38e871cd4584665a FOREIGN KEY (product_id) REFERENCES product (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('DROP TABLE product_value');
    }
}
