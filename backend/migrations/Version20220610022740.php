<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220610022740 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP SEQUENCE IF EXISTS shop_id_seq CASCADE');
        $this->addSql('CREATE OR REPLACE FUNCTION update_product_total() RETURNS TRIGGER AS
                            $$
                            DECLARE
                                total int;
                                fast int;
                            BEGIN
                                total = COALESCE((SELECT SUM(value)
                                         FROM rest
                                         WHERE product_id = NEW.product_id), 0);
                                fast = COALESCE((SELECT value
                                        FROM rest
                                                 INNER JOIN store s on s.id = rest.store_id
                                        WHERE s.is_main = true AND rest.product_id = NEW.product_id), 0);
                                UPDATE product SET total_qty = total,
                                                   fast_qty = fast
                                                WHERE id = NEW.product_id;
                                RETURN NEW;
                            END;
                            $$ language plpgsql');
        $this->addSql('CREATE TRIGGER total_update
    AFTER UPDATE ON rest FOR EACH ROW EXECUTE PROCEDURE update_product_total()');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TRIGGER IF EXISTS total_update ON rest CASCADE');
        $this->addSql('DROP FUNCTION update_product_total()');
        $this->addSql('CREATE SEQUENCE shop_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
    }
}
