<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20211117083123 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE category ADD is_active BOOLEAN NOT NULL');
        $this->addSql('ALTER TABLE category ADD type INT NOT NULL');
        $this->addSql('ALTER TABLE category ADD alias VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE category DROP code');
        $this->addSql('CREATE UNIQUE INDEX unique_type_alias ON category (parent_id, alias)');
        $this->addSql('ALTER INDEX uuid_unique RENAME TO unique_uuid');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP INDEX unique_type_alias');
        $this->addSql('ALTER TABLE category ADD code VARCHAR(50) DEFAULT NULL');
        $this->addSql('ALTER TABLE category DROP is_active');
        $this->addSql('ALTER TABLE category DROP type');
        $this->addSql('ALTER TABLE category DROP alias');
        $this->addSql('ALTER INDEX unique_uuid RENAME TO uuid_unique');
    }
}
