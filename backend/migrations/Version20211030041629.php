<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20211030041629 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP INDEX uniq_a3b536fde7927c74');
        $this->addSql('ALTER TABLE auth_user ADD phone VARCHAR(20) DEFAULT NULL');
        $this->addSql('ALTER TABLE auth_user ALTER email DROP NOT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE auth_user DROP phone');
        $this->addSql('ALTER TABLE auth_user ALTER email SET NOT NULL');
        $this->addSql('CREATE UNIQUE INDEX uniq_a3b536fde7927c74 ON auth_user (email)');
    }
}
