<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220610061331 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE OR REPLACE FUNCTION update_order_total() RETURNS TRIGGER AS
                            $$
                            DECLARE
                                total_cost int;
                                discount int;
                            BEGIN
                                total_cost = COALESCE((SELECT SUM(order_item.total)
                                         FROM order_item
                                         WHERE order_id = NEW.order_id), 0);
                                discount = COALESCE((SELECT SUM(order_item.discount)
                                        FROM order_item
                                        WHERE order_id = NEW.order_id), 0);
                                UPDATE "order" SET total = total_cost,
                                                 discount_value = discount
                                                WHERE id = NEW.order_id;
                                RETURN NEW;
                            END;
                            $$ language plpgsql');
        $this->addSql('CREATE TRIGGER order_total_update
    AFTER INSERT OR UPDATE OR DELETE ON order_item FOR EACH ROW EXECUTE PROCEDURE update_order_total()');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TRIGGER IF EXISTS order_total_update ON rest CASCADE');
        $this->addSql('DROP FUNCTION update_order_total()');
    }
}
