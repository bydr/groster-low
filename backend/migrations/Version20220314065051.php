<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220314065051 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE store ADD address VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE store ADD schedule VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE store ADD description TEXT DEFAULT NULL');
        $this->addSql('ALTER TABLE store ADD phone VARCHAR(50) DEFAULT NULL');
        $this->addSql('ALTER TABLE store ADD latitude VARCHAR(25) DEFAULT NULL');
        $this->addSql('ALTER TABLE store ADD longitude VARCHAR(25) DEFAULT NULL');
        $this->addSql('ALTER TABLE store ADD image VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE store ADD email VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE store ADD update_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE store DROP address');
        $this->addSql('ALTER TABLE store DROP schedule');
        $this->addSql('ALTER TABLE store DROP description');
        $this->addSql('ALTER TABLE store DROP phone');
        $this->addSql('ALTER TABLE store DROP latitude');
        $this->addSql('ALTER TABLE store DROP longitude');
        $this->addSql('ALTER TABLE store DROP image');
        $this->addSql('ALTER TABLE store DROP email');
        $this->addSql('ALTER TABLE store DROP update_at');
    }
}
