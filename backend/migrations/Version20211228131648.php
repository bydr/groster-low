<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20211228131648 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SEQUENCE discount_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE discount (id INT NOT NULL, name VARCHAR(255) NOT NULL, code VARCHAR(20) NOT NULL, type SMALLINT NOT NULL, value DOUBLE PRECISION NOT NULL, reusable BOOLEAN NOT NULL, start TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, finish TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE discount_category (discount_id INT NOT NULL, category_id INT NOT NULL, PRIMARY KEY(discount_id, category_id))');
        $this->addSql('CREATE INDEX IDX_6C4A315E4C7C611F ON discount_category (discount_id)');
        $this->addSql('CREATE INDEX IDX_6C4A315E12469DE2 ON discount_category (category_id)');
        $this->addSql('ALTER TABLE discount_category ADD CONSTRAINT FK_6C4A315E4C7C611F FOREIGN KEY (discount_id) REFERENCES discount (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE discount_category ADD CONSTRAINT FK_6C4A315E12469DE2 FOREIGN KEY (category_id) REFERENCES category (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE "order" ADD discount_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE "order" ADD discount_value INT DEFAULT NULL');
        $this->addSql('ALTER TABLE "order" ADD CONSTRAINT FK_F52993984C7C611F FOREIGN KEY (discount_id) REFERENCES discount (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_F52993984C7C611F ON "order" (discount_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE discount_category DROP CONSTRAINT FK_6C4A315E4C7C611F');
        $this->addSql('ALTER TABLE "order" DROP CONSTRAINT FK_F52993984C7C611F');
        $this->addSql('DROP SEQUENCE discount_id_seq CASCADE');
        $this->addSql('DROP TABLE discount');
        $this->addSql('DROP TABLE discount_category');
        $this->addSql('DROP INDEX IDX_F52993984C7C611F');
        $this->addSql('ALTER TABLE "order" DROP discount_id');
        $this->addSql('ALTER TABLE "order" DROP discount_value');
    }
}
