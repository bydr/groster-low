<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220921052148 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE user_product_auth_user DROP CONSTRAINT fk_350a3b5be2e3a0b6');
        $this->addSql('ALTER TABLE user_product_product DROP CONSTRAINT fk_104f7392e2e3a0b6');
        $this->addSql('DROP SEQUENCE user_product_id_seq CASCADE');
        $this->addSql('DROP TABLE user_product');
        $this->addSql('DROP TABLE user_product_auth_user');
        $this->addSql('DROP TABLE user_product_product');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SEQUENCE user_product_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE user_product (id INT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE user_product_auth_user (user_product_id INT NOT NULL, auth_user_id INT NOT NULL, PRIMARY KEY(user_product_id, auth_user_id))');
        $this->addSql('CREATE INDEX idx_350a3b5be94af366 ON user_product_auth_user (auth_user_id)');
        $this->addSql('CREATE INDEX idx_350a3b5be2e3a0b6 ON user_product_auth_user (user_product_id)');
        $this->addSql('CREATE TABLE user_product_product (user_product_id INT NOT NULL, product_id INT NOT NULL, PRIMARY KEY(user_product_id, product_id))');
        $this->addSql('CREATE INDEX idx_104f73924584665a ON user_product_product (product_id)');
        $this->addSql('CREATE INDEX idx_104f7392e2e3a0b6 ON user_product_product (user_product_id)');
        $this->addSql('ALTER TABLE user_product_auth_user ADD CONSTRAINT fk_350a3b5be2e3a0b6 FOREIGN KEY (user_product_id) REFERENCES user_product (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE user_product_auth_user ADD CONSTRAINT fk_350a3b5be94af366 FOREIGN KEY (auth_user_id) REFERENCES auth_user (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE user_product_product ADD CONSTRAINT fk_104f7392e2e3a0b6 FOREIGN KEY (user_product_id) REFERENCES user_product (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE user_product_product ADD CONSTRAINT fk_104f73924584665a FOREIGN KEY (product_id) REFERENCES product (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
    }
}
