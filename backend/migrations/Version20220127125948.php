<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220127125948 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SEQUENCE payer_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE replacement_method_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE shipping_address_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE payer (id INT NOT NULL, customer_id INT NOT NULL, name VARCHAR(255) DEFAULT NULL, ogrnip VARCHAR(15) DEFAULT NULL, inn VARCHAR(12) DEFAULT NULL, account VARCHAR(50) DEFAULT NULL, bik VARCHAR(50) DEFAULT NULL, kpp VARCHAR(50) DEFAULT NULL, contact VARCHAR(255) DEFAULT NULL, contact_phone VARCHAR(20) DEFAULT NULL, uid VARCHAR(28) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_41CB5B999395C3F3 ON payer (customer_id)');
        $this->addSql('CREATE UNIQUE INDEX payer_unique_uid ON payer (uid)');
        $this->addSql('CREATE TABLE replacement_method (id INT NOT NULL, title VARCHAR(255) NOT NULL, description TEXT NOT NULL, weight SMALLINT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE shipping_address (id INT NOT NULL, customer_id INT NOT NULL, name VARCHAR(255) NOT NULL, address TEXT NOT NULL, uid VARCHAR(30) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_EB0669459395C3F3 ON shipping_address (customer_id)');
        $this->addSql('CREATE UNIQUE INDEX shipping_address_unique_uid ON shipping_address (uid)');
        $this->addSql('ALTER TABLE payer ADD CONSTRAINT FK_41CB5B999395C3F3 FOREIGN KEY (customer_id) REFERENCES auth_user (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE shipping_address ADD CONSTRAINT FK_EB0669459395C3F3 FOREIGN KEY (customer_id) REFERENCES auth_user (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE "order" ADD replacement_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE "order" ADD payer_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE "order" ADD region VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE "order" RENAME COLUMN address TO shipping_address');
        $this->addSql('ALTER TABLE "order" ADD CONSTRAINT FK_F52993989D25CF90 FOREIGN KEY (replacement_id) REFERENCES replacement_method (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE "order" ADD CONSTRAINT FK_F5299398C17AD9A9 FOREIGN KEY (payer_id) REFERENCES payer (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_F52993989D25CF90 ON "order" (replacement_id)');
        $this->addSql('CREATE INDEX IDX_F5299398C17AD9A9 ON "order" (payer_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE "order" DROP CONSTRAINT FK_F5299398C17AD9A9');
        $this->addSql('ALTER TABLE "order" DROP CONSTRAINT FK_F52993989D25CF90');
        $this->addSql('DROP SEQUENCE payer_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE replacement_method_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE shipping_address_id_seq CASCADE');
        $this->addSql('DROP TABLE payer');
        $this->addSql('DROP TABLE replacement_method');
        $this->addSql('DROP TABLE shipping_address');
        $this->addSql('DROP INDEX IDX_F52993989D25CF90');
        $this->addSql('DROP INDEX IDX_F5299398C17AD9A9');
        $this->addSql('ALTER TABLE "order" DROP replacement_id');
        $this->addSql('ALTER TABLE "order" DROP payer_id');
        $this->addSql('ALTER TABLE "order" DROP region');
        $this->addSql('ALTER TABLE "order" RENAME COLUMN shipping_address TO address');
    }
}
