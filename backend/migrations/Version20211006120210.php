<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20211006120210 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE value_product (value_id INT NOT NULL, product_id INT NOT NULL, PRIMARY KEY(value_id, product_id))');
        $this->addSql('CREATE INDEX IDX_38E871CDF920BBA2 ON value_product (value_id)');
        $this->addSql('CREATE INDEX IDX_38E871CD4584665A ON value_product (product_id)');
        $this->addSql('ALTER TABLE value_product ADD CONSTRAINT FK_38E871CDF920BBA2 FOREIGN KEY (value_id) REFERENCES value (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE value_product ADD CONSTRAINT FK_38E871CD4584665A FOREIGN KEY (product_id) REFERENCES product (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE value_product');
    }
}
