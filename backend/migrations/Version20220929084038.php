<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220929084038 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SEQUENCE product_companion_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE product_companion (id INT NOT NULL, parent_id INT DEFAULT NULL, companion_id INT NOT NULL, weight INT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_750D98E4727ACA70 ON product_companion (parent_id)');
        $this->addSql('CREATE INDEX IDX_750D98E48227E3FD ON product_companion (companion_id)');
        $this->addSql('ALTER TABLE product_companion ADD CONSTRAINT FK_750D98E4727ACA70 FOREIGN KEY (parent_id) REFERENCES product (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE product_companion ADD CONSTRAINT FK_750D98E48227E3FD FOREIGN KEY (companion_id) REFERENCES product (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('DROP TABLE companion');
        $this->addSql('ALTER TABLE product_analogs ALTER weight DROP DEFAULT');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP SEQUENCE product_companion_id_seq CASCADE');
        $this->addSql('CREATE TABLE companion (product_source INT NOT NULL, product_target INT NOT NULL, PRIMARY KEY(product_source, product_target))');
        $this->addSql('CREATE INDEX idx_1bad2e6924136e58 ON companion (product_target)');
        $this->addSql('CREATE INDEX idx_1bad2e693df63ed7 ON companion (product_source)');
        $this->addSql('ALTER TABLE companion ADD CONSTRAINT fk_1bad2e693df63ed7 FOREIGN KEY (product_source) REFERENCES product (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE companion ADD CONSTRAINT fk_1bad2e6924136e58 FOREIGN KEY (product_target) REFERENCES product (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('DROP TABLE product_companion');
        $this->addSql('ALTER TABLE product_analogs ALTER weight SET DEFAULT 0');
    }
}
