<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220201122227 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Создание таблицы waiting_product для сообщения о поступлении товара';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SEQUENCE waiting_product_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE waiting_product (id INT NOT NULL, product_id INT NOT NULL, contact VARCHAR(255) NOT NULL, notificated_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_EBE398994584665A ON waiting_product (product_id)');
        $this->addSql('CREATE UNIQUE INDEX unique_waiting_product ON waiting_product (product_id, contact, notificated_at)');
        $this->addSql('ALTER TABLE waiting_product ADD CONSTRAINT FK_EBE398994584665A FOREIGN KEY (product_id) REFERENCES product (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP SEQUENCE waiting_product_id_seq CASCADE');
        $this->addSql('DROP TABLE waiting_product');
    }
}
