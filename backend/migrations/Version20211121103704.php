<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20211121103704 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SEQUENCE container_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE container (id INT NOT NULL, product_id INT NOT NULL, name VARCHAR(255) NOT NULL, is_active BOOLEAN NOT NULL, rate INT NOT NULL, uuid VARCHAR(50) NOT NULL, standard_name VARCHAR(255) DEFAULT NULL, width VARCHAR(10) DEFAULT NULL, height VARCHAR(10) DEFAULT NULL, length VARCHAR(10) DEFAULT NULL, size VARCHAR(10) DEFAULT NULL, weight VARCHAR(10) DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_C7A2EC1B6C8A81A9 ON container (product_id)');
        $this->addSql('CREATE TABLE analog (product_source INT NOT NULL, product_target INT NOT NULL, PRIMARY KEY(product_source, product_target))');
        $this->addSql('CREATE INDEX IDX_A78C95C13DF63ED7 ON analog (product_source)');
        $this->addSql('CREATE INDEX IDX_A78C95C124136E58 ON analog (product_target)');
        $this->addSql('CREATE TABLE kit (product_source INT NOT NULL, product_target INT NOT NULL, PRIMARY KEY(product_source, product_target))');
        $this->addSql('CREATE INDEX IDX_589498183DF63ED7 ON kit (product_source)');
        $this->addSql('CREATE INDEX IDX_5894981824136E58 ON kit (product_target)');
        $this->addSql('CREATE TABLE companion (product_source INT NOT NULL, product_target INT NOT NULL, PRIMARY KEY(product_source, product_target))');
        $this->addSql('CREATE INDEX IDX_1BAD2E693DF63ED7 ON companion (product_source)');
        $this->addSql('CREATE INDEX IDX_1BAD2E6924136E58 ON companion (product_target)');
        $this->addSql('ALTER TABLE container ADD CONSTRAINT FK_C7A2EC1B6C8A81A9 FOREIGN KEY (product_id) REFERENCES product (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE analog ADD CONSTRAINT FK_A78C95C13DF63ED7 FOREIGN KEY (product_source) REFERENCES product (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE analog ADD CONSTRAINT FK_A78C95C124136E58 FOREIGN KEY (product_target) REFERENCES product (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE kit ADD CONSTRAINT FK_589498183DF63ED7 FOREIGN KEY (product_source) REFERENCES product (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE kit ADD CONSTRAINT FK_5894981824136E58 FOREIGN KEY (product_target) REFERENCES product (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE companion ADD CONSTRAINT FK_1BAD2E693DF63ED7 FOREIGN KEY (product_source) REFERENCES product (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE companion ADD CONSTRAINT FK_1BAD2E6924136E58 FOREIGN KEY (product_target) REFERENCES product (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP SEQUENCE container_id_seq CASCADE');
        $this->addSql('DROP TABLE container');
        $this->addSql('DROP TABLE analog');
        $this->addSql('DROP TABLE kit');
        $this->addSql('DROP TABLE companion');
    }
}
