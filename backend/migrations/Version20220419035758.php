<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220419035758 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP INDEX idx_2d0e4b1612469de2');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_2D0E4B1612469DE2 ON category_image (category_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP INDEX UNIQ_2D0E4B1612469DE2');
        $this->addSql('CREATE INDEX idx_2d0e4b1612469de2 ON category_image (category_id)');
    }
}
