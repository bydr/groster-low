<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220308040656 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SEQUENCE price_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE product_price_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE price (id INT NOT NULL, uuid VARCHAR(40) NOT NULL, name VARCHAR(255) NOT NULL, is_default BOOLEAN DEFAULT \'false\' NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX unique_price_uuid ON price (uuid)');
        $this->addSql('CREATE TABLE product_price (id INT NOT NULL, price_id INT NOT NULL, product_id INT NOT NULL, value INT NOT NULL, update_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_6B945985D614C7E7 ON product_price (price_id)');
        $this->addSql('CREATE INDEX IDX_6B9459854584665A ON product_price (product_id)');
        $this->addSql('ALTER TABLE product_price ADD CONSTRAINT FK_6B945985D614C7E7 FOREIGN KEY (price_id) REFERENCES price (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE product_price ADD CONSTRAINT FK_6B9459854584665A FOREIGN KEY (product_id) REFERENCES product (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE auth_user ADD price_type_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE auth_user ADD CONSTRAINT FK_A3B536FDAE6A44CF FOREIGN KEY (price_type_id) REFERENCES price (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_A3B536FDAE6A44CF ON auth_user (price_type_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE auth_user DROP CONSTRAINT FK_A3B536FDAE6A44CF');
        $this->addSql('ALTER TABLE product_price DROP CONSTRAINT FK_6B945985D614C7E7');
        $this->addSql('DROP SEQUENCE price_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE product_price_id_seq CASCADE');
        $this->addSql('DROP TABLE price');
        $this->addSql('DROP TABLE product_price');
        $this->addSql('DROP INDEX IDX_A3B536FDAE6A44CF');
        $this->addSql('ALTER TABLE auth_user DROP price_type_id');
    }
}
