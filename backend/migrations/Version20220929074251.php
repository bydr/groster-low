<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220929074251 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SEQUENCE product_analogs_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE product_analogs (id INT NOT NULL, product_source_id INT NOT NULL, product_target_id INT NOT NULL, weight INT NOT NULL DEFAULT 0, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_2613AA08FB9CAAEC ON product_analogs (product_source_id)');
        $this->addSql('CREATE INDEX IDX_2613AA087B2EBDEB ON product_analogs (product_target_id)');
        $this->addSql('ALTER TABLE product_analogs ADD CONSTRAINT FK_2613AA08FB9CAAEC FOREIGN KEY (product_source_id) REFERENCES product (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE product_analogs ADD CONSTRAINT FK_2613AA087B2EBDEB FOREIGN KEY (product_target_id) REFERENCES product (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('DROP TABLE analog');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP SEQUENCE product_analogs_id_seq CASCADE');
        $this->addSql('CREATE TABLE analog (product_source INT NOT NULL, product_target INT NOT NULL, PRIMARY KEY(product_source, product_target))');
        $this->addSql('CREATE INDEX idx_a78c95c13df63ed7 ON analog (product_source)');
        $this->addSql('CREATE INDEX idx_a78c95c124136e58 ON analog (product_target)');
        $this->addSql('ALTER TABLE analog ADD CONSTRAINT fk_a78c95c13df63ed7 FOREIGN KEY (product_source) REFERENCES product (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE analog ADD CONSTRAINT fk_a78c95c124136e58 FOREIGN KEY (product_target) REFERENCES product (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('DROP TABLE product_analogs');
    }
}
