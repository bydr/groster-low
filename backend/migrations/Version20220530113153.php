<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220530113153 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE auth_user ALTER fio TYPE VARCHAR(250)');
        $this->addSql('ALTER TABLE product DROP is_kit_part');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE auth_user ALTER fio TYPE VARCHAR(40)');
        $this->addSql('ALTER TABLE product ADD is_kit_part BOOLEAN DEFAULT \'false\' NOT NULL');
    }
}
