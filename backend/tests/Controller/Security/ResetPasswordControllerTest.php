<?php

namespace App\Tests\Controller\Security;

use App\ApiError;
use App\Controller\ApiController;
use App\Helpers\Params;
use App\Repository\AuthUserRepository;
use App\Service\Cache;
use App\Tests\Controller\CustomWebTestCase;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * Class ResetPasswordControllerTest
 */
class ResetPasswordControllerTest extends CustomWebTestCase
{
    public function testResetPassword()
    {
        $testCases = [
            [
                "name" => 'Isset email',
                'contact' => "qwe1@test.com",
                'statusCode' => 200,
            ],
            [
                "name" => 'Undefined email',
                'contact' => "test@test.com",
                'statusCode' => ApiController::STATUS_BAD_REQUEST,
                'errorCode' => ApiError::ERROR_UNKNOWN_EMAIL
            ],
        ];

        $client = static::createClient();

        foreach ($testCases as $tc) {
            $client->jsonRequest('POST', '/api/v1/auth/reset_password', ["email" => $tc['contact']]);
            $this->assertResponseStatusCodeSame($tc['statusCode'], sprintf("%s: Error status code", $tc['name']));

            $resp = $this->getResp2Arr($client->getResponse());
            if($tc['statusCode'] == 200) {
                self::assertArrayHasKey(Params::LIFETIME, $resp);
                self::assertEquals(Cache::CODE_LIFETIME, $resp[Params::LIFETIME], sprintf("%s: Error lifetime field", $tc['name']));
            }else{
                self::assertArrayHasKey(Params::CODE, $resp);
                $this->assertEquals(ApiError::ERROR_UNKNOWN_EMAIL, $resp[Params::CODE]);
            }
        }
    }

    public function testUpdatePassword()
    {
        $testCases = [
            [
                "name" => 'Success',
                'email' => "qwe1@test.com",
                'password' => "123456",
                'code' => '1111',
                'statusCode' => 200
            ],
            [
                "name" => 'Wrong email',
                'email' => "test@test.com",
                'password' => "123456",
                'code' => '1111',
                'statusCode' => 400,
                'errorCode' => ApiError::ERROR_UNKNOWN_EMAIL,
            ],
        ];

        $client = static::createClient();
        $userRepository = static::getContainer()->get(AuthUserRepository::class);

        foreach ($testCases as $tc) {
            $client->jsonRequest('POST', '/api/v1/auth/update_password', ["email" => $tc['email'], "password" => $tc['password'], "code" => $tc['code']]);
            $this->assertResponseStatusCodeSame($tc['statusCode'], sprintf("%s: Error status code", $tc['name']));

            $resp = $this->getResp2Arr($client->getResponse());
            if($tc['statusCode'] == 200) {
                $testUser = $userRepository->findOneByEmail($tc['email']);
                self::assertTrue($testUser->IsLogin(), "Email {$tc['email']} is logged");
                $this->assertArrayHasKey(Params::ACCESS_TOKEN, $resp);
                $this->assertArrayHasKey(Params::REFRESH_TOKEN, $resp);
                self::assertEquals("success", $resp[Params::MESSAGE], "Fail success message");
            }else{
                $this->assertArrayHasKey(Params::CODE, $resp);
                $this->assertEquals($tc['errorCode'], $resp[Params::CODE]);
            }
        }
    }
}

