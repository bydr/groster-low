<?php

namespace App\Tests\Controller\Security;

use App\ApiError;
use App\Controller\ApiController;
use App\Helpers\Params;
use App\Helpers\ValidateHelper;
use App\Repository\AuthUserRepository;
use App\Service\Cache;
use App\Tests\Controller\CustomWebTestCase;

/**
 * Class LoginControllerTest
 */
class LoginControllerTest extends CustomWebTestCase
{
    /**
     * @group login
     * @group loginByEmail
     */
    public function testLoginByEmail(): void
    {
        $client = static::createClient();

        $testEmail = "qwe1@test.com";
        $userRepository = static::getContainer()->get(AuthUserRepository::class);
        $testUser = $userRepository->findOneByEmail($testEmail);
        self::assertFalse($testUser->IsLogin(), "Email $testEmail is logged");

        $client->jsonRequest('POST', '/api/v1/auth/login_email', ["email" => $testEmail, "password" => "password1"]);
        $this->assertResponseIsSuccessful();
        $testUser = $userRepository->findOneByEmail($testEmail);
        self::assertTrue($testUser->IsLogin(), "Email $testEmail does not logged");
        $userRepository->setLogin($testUser, false);
        $resp = $this->getResp2Arr($client->getResponse(), false);
        $this->matchObjectVars([Params::MESSAGE, Params::ACCESS_TOKEN, Params::REFRESH_TOKEN, Params::INFO], $resp);
        $this->matchObjectVars([Params::CART, Params::FIO, Params::IS_ADMIN], $resp->info);
        $this->assertFalse($resp->info->is_admin);

        $resp = $this->getResp2Arr($client->getResponse());
        $this->assertArrayHasKey(Params::ACCESS_TOKEN, $resp);
        $this->assertArrayHasKey(Params::REFRESH_TOKEN, $resp);
        $this->assertEquals("success", $resp[Params::MESSAGE], "Fail success message");

        // Invalid email
        $client->jsonRequest('POST', '/api/v1/auth/login_email', ["email" => 'wrong@example.com', "password" => "password1"]);
        $this->assertResponseStatusCodeSame(ApiController::STATUS_BAD_REQUEST);
        $resp = $this->getResp2Arr($client->getResponse());
        $this->assertArrayHasKey(Params::CODE, $resp);
        $this->assertEquals(ApiError::ERROR_INVALID_EMAIL_OR_PASS, $resp[Params::CODE]);

        // Invalid pass
        $client->jsonRequest('POST', '/api/v1/auth/login_email', ["email" => $testEmail, "password" => "wrongPass"]);
        $this->assertResponseStatusCodeSame(ApiController::STATUS_BAD_REQUEST);
        $resp = $this->getResp2Arr($client->getResponse());
        $this->assertArrayHasKey(Params::CODE, $resp);
        $this->assertEquals(ApiError::ERROR_INVALID_EMAIL_OR_PASS, $resp[Params::CODE]);

    }

    /**
     * @group login
     * @group loginByPhone
     */
    public function testLoginByPhone(): void
    {
        $client = static::createClient();

        $testPhone = ValidateHelper::getValidPhone('+7(988) 999-99-91');
        $userRepository = static::getContainer()->get(AuthUserRepository::class);
        $testUser = $userRepository->findOneByPhone($testPhone);
        self::assertFalse($testUser->IsLogin(), "Phone $testPhone is logged");

        $client->jsonRequest('POST', '/api/v1/auth/login_phone', ["phone" => $testPhone, "code" => "1111"]);
        $this->assertResponseIsSuccessful();
        $testUser = $userRepository->findOneByPhone($testPhone);
        self::assertTrue($testUser->IsLogin(), "Phone $testPhone is not logged");
        $userRepository->setLogin($testUser, false);

        $resp = $this->getResp2Arr($client->getResponse(), false);
        self::assertEquals("success", $resp->message);
        $this->matchObjectVars([Params::MESSAGE, Params::ACCESS_TOKEN, Params::REFRESH_TOKEN, Params::INFO], $resp);
        $this->matchObjectVars([Params::CART, Params::FIO, Params::IS_ADMIN], $resp->info);
        $this->assertFalse($resp->info->is_admin);

        // Invalid phone
        $client->jsonRequest('POST', '/api/v1/auth/login_phone', ["phone" => "+498745632177", "code" => "1111"]);
        $this->assertResponseStatusCodeSame(ApiController::STATUS_BAD_REQUEST);
        $resp = $this->getResp2Arr($client->getResponse());
        $this->assertArrayHasKey(Params::CODE, $resp);
        $this->assertEquals(ApiError::ERROR_INVALID_PHONE, $resp[Params::CODE]);

        // Invalid pass
        $client->jsonRequest('POST', '/api/v1/auth/login_phone', ["phone" => $testPhone, "code" => "0000"]);
        $this->assertResponseStatusCodeSame(ApiController::STATUS_BAD_REQUEST);
        $resp = $this->getResp2Arr($client->getResponse());
        $this->assertArrayHasKey(Params::CODE, $resp);
        $this->assertEquals(ApiError::ERROR_INVALID_CODE, $resp[Params::CODE]);
    }

    /**
     * @group login
     * @group sendCode
     */
    public function testSendCode()
    {
        $testCases = [
            [
                "name" => 'Send code for email',
                'contact' => "qwe1@test.com",
                'statusCode' => 200
            ],
            [
                "name" => 'Send code for phone',
                'contact' => "+7(988) 999-99-99",
                'statusCode' => 200
            ],
            [
                "name" => 'Send code for invalid data',
                'contact' => "+7(99)999-99",
                'statusCode' => 400,
                'errorCode' => ApiError::ERROR_BAD_REQUEST,
            ],
        ];

        $client = static::createClient();

        foreach ($testCases as $tc) {
            $client->jsonRequest('POST', '/api/v1/auth/send_code', ["contact" => $tc['contact']]);
            $this->assertResponseStatusCodeSame($tc['statusCode'], sprintf("%s: Error status code", $tc['name']));

            $resp = $this->getResp2Arr($client->getResponse());
            if($tc['statusCode'] == 200) {
                self::assertEquals(Cache::CODE_LIFETIME, $resp[Params::LIFETIME], sprintf("%s: Error lifetime field", $tc['name']));
            }else{
                $this->assertArrayHasKey(Params::CODE, $resp);
                $this->assertEquals($tc['errorCode'], $resp[Params::CODE]);
            }
        }
    }
}

