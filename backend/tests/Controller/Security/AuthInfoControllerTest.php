<?php

namespace App\Tests\Controller\Security;

use App\Helpers\Params;
use App\Repository\AuthUserRepository;
use App\Tests\Controller\CustomWebTestCase;

/**
 * Class AuthInfoControllerTest
 */
class AuthInfoControllerTest extends CustomWebTestCase
{
    public function testInfo(): void
    {
        $testEmail = 'qwe1@test.com';
        $client = static::createClient();
        $userRepository = static::getContainer()->get(AuthUserRepository::class);
        $testUser = $userRepository->findOneByEmail($testEmail);
        self::assertFalse($testUser->IsLogin(), "Email $testEmail is logged");

        $client->jsonRequest('POST', '/api/v1/auth/login_email', ["email" => $testEmail, "password" => "password1"]);
        $this->assertResponseIsSuccessful();

        $resp = $this->getResp2Arr($client->getResponse());
        $this->assertArrayHasKey(Params::ACCESS_TOKEN, $resp);
        $this->assertArrayHasKey(Params::REFRESH_TOKEN, $resp);
        self::assertEquals("success", $resp[Params::MESSAGE], "Fail success message");

        $client->jsonRequest('GET', '/api/v1/auth/info', [], ['HTTP_AUTHORIZATION' => $resp[Params::ACCESS_TOKEN]]);
        $this->assertResponseIsSuccessful();

        $resp = $this->getResp2Arr($client->getResponse());
        self::assertArrayHasKey(Params::FIO, $resp);
        self::assertArrayHasKey(Params::PHONE, $resp);
        self::assertArrayHasKey(Params::EMAIL, $resp);
        self::assertEquals($resp[Params::EMAIL], $testEmail, "Response email does not equal $testEmail");

        $testUser = $userRepository->findOneByEmail($testEmail);
        $userRepository->setLogin($testUser, false);

    }

}
