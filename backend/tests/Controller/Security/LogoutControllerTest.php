<?php

namespace App\Tests\Controller\Security;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * Class LogoutControllerTest
 */
class LogoutControllerTest extends WebTestCase
{
    public function testLogout(): void
    {
        $client = static::createClient();

        $client->request('GET', '/api/v1/auth/logout');
        $this->assertResponseIsSuccessful();

        $client->request('POST', '/api/v1/auth/logout');
        $this->assertResponseIsSuccessful();
    }

}
