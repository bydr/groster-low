<?php

namespace App\Tests\Controller\Security;

use App\ApiError;
use App\Helpers\Params;
use App\Helpers\ValidateHelper;
use App\Repository\AuthUserRepository;
use App\Tests\Controller\CustomWebTestCase;

/**
 * Class RegisterControllerTest
 */
class RegisterControllerTest extends CustomWebTestCase
{
    /**
     * @group register
     * @group registerByEmail
     */
    public function testRegisterEmail(): void
    {
        $client = static::createClient();
        $testEmail = 'test@test.com';
        $userRepository = static::getContainer()->get(AuthUserRepository::class);
        $testUser = $userRepository->findOneByEmail($testEmail);
        self::assertNull($testUser, "Email $testEmail has been already registered");

        $client->jsonRequest('POST', '/api/v1/auth/register_email', ["email" => $testEmail, "password" => "password", "code" => '1111']);
        $this->assertResponseIsSuccessful();
        $testUser = $userRepository->findOneByEmail($testEmail);
        self::assertNotNull($testUser, "Email $testEmail does not exist");

        $resp = $this->getResp2Arr($client->getResponse());
        self::assertArrayHasKey(Params::ACCESS_TOKEN, $resp, "Token does not exist");
        self::assertNotEmpty($resp[Params::ACCESS_TOKEN], "Token is empty");
        $userRepository->remove($testUser);
    }

    /**
     * @group register
     * @group emailAlreadyExists
     */
    public function testRegisterEmailAlreadyExists(): void
    {
        $client = static::createClient();

        $testEmail = 'qwe1@test.com';
        $userRepository = static::getContainer()->get(AuthUserRepository::class);
        $testUser = $userRepository->findOneByEmail($testEmail);
        self::assertNotNull($testUser, "Email $testEmail does not exist");

        $client->jsonRequest('POST', '/api/v1/auth/register_email', ["email" => $testEmail, "password" => "password", "code" => '1111']);
        self::assertResponseStatusCodeSame(400);

        $resp = $this->getResp2Arr($client->getResponse());
        self::assertEquals(ApiError::ERROR_DUPLICATE_EMAIL, $resp[Params::CODE], 'Fail error code');
    }

    /**
     * @group register
     * @group InvalidCode
     */
    public function testRegisterEmailInvalidCode(): void
    {
        $client = static::createClient();

        $testEmail = 'test1@test.com';
        $userRepository = static::getContainer()->get(AuthUserRepository::class);
        $client->jsonRequest('POST', '/api/v1/auth/register_email', ["email" => $testEmail, "password" => "password", "code" => '0000']);
        $this->assertResponseStatusCodeSame(400);
        $testUser = $userRepository->findOneByEmail($testEmail);
        self::assertNull($testUser, "Email $testEmail exists");

        $resp = $this->getResp2Arr($client->getResponse());
        self::assertEquals(ApiError::ERROR_INVALID_CODE, $resp[Params::CODE], 'Fail error code');
    }

    /**
     * @group register
     * @group registerByPhone
     */
    public function testRegisterPhone(): void
    {
        $client = static::createClient();

        $testPhone = ValidateHelper::getValidPhone('+7(988) 999-99-89');
        $userRepository = static::getContainer()->get(AuthUserRepository::class);
        $testUser = $userRepository->findOneByPhone($testPhone);
        self::assertNull($testUser, "Phone $testPhone already exists");

        $client->jsonRequest('POST', '/api/v1/auth/register_phone', ["phone" => $testPhone, "code" => '1111']);
        $this->assertResponseIsSuccessful();
        $testUser = $userRepository->findOneByPhone($testPhone);
        self::assertNotNull($testUser, "Phone $testPhone does not exist");

        $resp = $this->getResp2Arr($client->getResponse());
        self::assertArrayHasKey(Params::ACCESS_TOKEN, $resp, 'Token does not exist');
        self::assertNotEmpty($resp[Params::ACCESS_TOKEN], 'Empty token');
        $userRepository->remove($testUser);
    }

    /**
     * @group register
     * @group phoneAlreadyExists
     */
    public function testRegisterPhoneAlreadyExists(): void
    {
        $client = static::createClient();

        $testPhone = ValidateHelper::getValidPhone('+7(988) 999-99-91');
        $userRepository = static::getContainer()->get(AuthUserRepository::class);
        $testUser = $userRepository->findOneByPhone($testPhone);
        self::assertNotNull($testUser, "Phone $testPhone does not exist");

        $client->jsonRequest('POST', '/api/v1/auth/register_phone', ["phone" => $testPhone, "code" => '1111']);
        self::assertResponseStatusCodeSame(400);

        $resp = $this->getResp2Arr($client->getResponse());
        self::assertEquals(ApiError::ERROR_DUPLICATE_PHONE, $resp[Params::CODE], 'Fail error code');
    }

    /**
     * @group register
     * @group invalidCode
     */
    public function testRegisterPhoneInvalidCode(): void
    {
        $client = static::createClient();

        $testPhone = ValidateHelper::getValidPhone('+7(988) 999-99-88');
        $userRepository = static::getContainer()->get(AuthUserRepository::class);
        $client->jsonRequest('POST', '/api/v1/auth/register_phone', ["phone" => $testPhone, "code" => '0000']);
        $this->assertResponseStatusCodeSame(400);
        $testUser = $userRepository->findOneByPhone($testPhone);
        self::assertNull($testUser);

        $resp = $this->getResp2Arr($client->getResponse());
        self::assertEquals(ApiError::ERROR_INVALID_CODE, $resp[Params::CODE], 'Fail error code');
    }

}
