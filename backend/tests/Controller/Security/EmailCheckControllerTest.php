<?php

namespace App\Tests\Controller\Security;

use App\ApiError;
use App\Helpers\Params;
use App\Repository\AuthUserRepository;
use App\Tests\Controller\CustomWebTestCase;

/**
 * Class EmailCheckControllerTest
 */
class EmailCheckControllerTest extends CustomWebTestCase
{
    public function testExistEmailCheck(): void
    {
        $client = static::createClient();

        $testEmail = 'qwe1@test.com';
        $userRepository = static::getContainer()->get(AuthUserRepository::class);
        $testUser = $userRepository->findOneByEmail($testEmail);
        $this->assertNotNull($testUser, "Email $testEmail does not exist");

        $client->jsonRequest('GET', '/api/v1/auth/email_check?email=' . $testEmail);
        $this->assertResponseIsSuccessful();

        $resp = $this->getResp2Arr($client->getResponse());
        self::assertEquals(false, $resp[Params::IS_FREE], "Invalid response data");
    }
    public function testNewEmailCheck(): void
    {
        $client = static::createClient();

        $testEmail = 'qweq@test.com';
        $userRepository = static::getContainer()->get(AuthUserRepository::class);
        $testUser = $userRepository->findOneByEmail($testEmail);
        $this->assertNull($testUser, "Email $testEmail exists");

        $client->jsonRequest('GET', '/api/v1/auth/email_check?email=' . $testEmail);
        $this->assertResponseIsSuccessful();

        $resp = $this->getResp2Arr($client->getResponse());
        self::assertEquals(true, $resp[Params::IS_FREE], "Invalid response data");
    }

    public function testEmailCheckInvalid(): void
    {
        $client = static::createClient();

        $testEmail = 'qweq@test.com';
        $userRepository = static::getContainer()->get(AuthUserRepository::class);
        $testUser = $userRepository->findOneByEmail($testEmail);
        $this->assertNull($testUser, "Email $testEmail exists");

        $client->jsonRequest('GET', '/api/v1/auth/email_check');
        $this->assertResponseStatusCodeSame(400);

        $resp = $this->getResp2Arr($client->getResponse());
        $this->assertArrayHasKey(Params::CODE, $resp, 'Not returned code');
        $this->assertEquals(ApiError::ERROR_INVALID_DATA, $resp[Params::CODE]);
    }

}
