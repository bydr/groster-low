<?php

namespace App\Tests\Controller\Security;

use App\Helpers\Params;
use App\Repository\AuthUserRepository;
use App\Tests\Controller\CustomWebTestCase;

/**
 * Class RefreshControllerTest
 */
class RefreshControllerTest extends CustomWebTestCase
{
    /**
     * @group refresh
     */
    public function testRefresh(): void
    {
        $client = static::createClient();

        $testEmail = 'qwe1@test.com';
        $userRepository = static::getContainer()->get(AuthUserRepository::class);
        $testUser = $userRepository->findOneByEmail($testEmail);
        self::assertFalse($testUser->IsLogin());
        $client->jsonRequest('POST', '/api/v1/auth/login_email', ["email" => $testEmail, "password" => "password1"]);
        $this->assertResponseIsSuccessful("Error response is successful in login");
        self::assertTrue($testUser->IsLogin(), "Email $testEmail is not logged");

        $resp = $this->getResp2Arr($client->getResponse());
        self::assertEquals("success", $resp[Params::MESSAGE], "Fail success message");

        $client->jsonRequest('POST', '/api/v1/auth/refresh', [Params::REFRESH_TOKEN => $resp[Params::REFRESH_TOKEN]]);
        $this->assertResponseIsSuccessful("Error response is successful in refresh");

        $resp = $this->getResp2Arr($client->getResponse());
        self::assertEquals("success", $resp[Params::MESSAGE], "Fail success message");

        $testUser = $userRepository->findOneByEmail($testEmail);
        $userRepository->setLogin($testUser, false);
    }

}
