<?php

namespace App\Tests\Controller\Admin;

use App\Helpers\Params;
use App\Tests\Controller\CustomWebTestCase;

/**
 * Class CategoryControllerTest
 */
class CategoryControllerTest extends CustomWebTestCase
{
    /**
     * @group admin
     * @group category
     * @group find
     */
    public function testFindOne(): void
    {
        $expectedCategory = [
            Params::ID => 184,
            Params::UUID => "903c65f6-55e6-11e9-9e9a-ac1f6b855a52",
            Params::NAME => "Аксессуары для бумажных стаканов",
            Params::HELP_PAGE => null,
            Params::PARENT => 35,
            Params::TAGS => [],
            Params::NAME_1C => 'Аксессуары для бумажных стаканов',
            Params::ALIAS => 'aksessuary-dlya-bumajnyh-stakanov',
            Params::IMAGE => '',
            Params::META_TITLE => 'Купить акксессуары для бумажных стаканов',
            Params::META_DESCRIPTION => 'Аксессуары для бумажных стаканов оптом',
            Params::SELECTION_FILE => '',
            Params::WEIGHT => 4,
        ];

        $client = static::createClient();
        $client->jsonRequest('GET', '/api/v1/admin/category/' . $expectedCategory["id"], [], [self::HEADER_AUTH => $this->getAdminToken($client)]);
        $category = $this->getResp2Arr($client->getResponse(), false);

        $this->assertResponseIsSuccessful();
        $this->assertResponseStatusCodeSame(200);
        $this->assertEquals((object)$expectedCategory, $category, "Fail equal categories");
        $this->matchObjectVars(array_keys($expectedCategory), $category);
    }

    /**
     * @group admin
     * @group category
     * @group update
     */
    public function testUpdateOne(): void
    {
        $expectedCategory = [
            Params::ID => 184,
            Params::UUID => "903c65f6-55e6-11e9-9e9a-ac1f6b855a52",
            Params::NAME => "Аксессуары для бумажных стаканова",
            Params::HELP_PAGE => '<p>Help page category 1</p>',
            Params::PARENT => 35,
            Params::TAGS => [3,4],
            Params::NAME_1C => 'Аксессуары для бумажных стаканов',
            Params::ALIAS => 'aksessuary-dlya-bumajnyh-stakanov',
            Params::IMAGE => '',
            Params::META_TITLE => 'Купить оптом акксессуары для бумажных стаканов',
            Params::META_DESCRIPTION => 'Аксессуары для бумажных стаканов оптом дёшево',
            Params::SELECTION_FILE => '',
            Params::WEIGHT => 4,
        ];

        $client = static::createClient();
        $client->jsonRequest('PUT', '/api/v1/admin/category/' . $expectedCategory["id"], [
            Params::NAME => $expectedCategory['name'],
            Params::ALIAS => $expectedCategory['alias'],
            Params::TAGS => $expectedCategory['tags'],
            Params::HELP_PAGE => $expectedCategory['help_page'],
            Params::META_TITLE => $expectedCategory[Params::META_TITLE],
            Params::META_DESCRIPTION => $expectedCategory[Params::META_DESCRIPTION],
            Params::WEIGHT => $expectedCategory[Params::WEIGHT],
            ], [self::HEADER_AUTH => $this->getAdminToken($client)]);

        $this->assertResponseIsSuccessful();
        $this->assertResponseStatusCodeSame(200);

        $client->jsonRequest('GET', '/api/v1/admin/category/' . $expectedCategory["id"], [], [self::HEADER_AUTH => $this->getAdminToken($client)]);
        $category = $this->getResp2Arr($client->getResponse(), false);

        $this->assertResponseIsSuccessful();
        $this->assertResponseStatusCodeSame(200);
        $this->assertEquals((object)$expectedCategory, $category);
    }
}
