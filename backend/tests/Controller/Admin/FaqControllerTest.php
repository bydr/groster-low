<?php

namespace App\Tests\Controller\Admin;

use App\Helpers\Params;
use App\Repository\FaqRepository;
use App\Repository\FaqTypeRepository;
use App\Tests\Controller\CustomWebTestCase;

/**
 * Тесты работы с блоком вопрос-ответ
 */
class FaqControllerTest extends CustomWebTestCase
{
    /**
     * @group admin
     * @group faq
     * @group allBlocks
     */
    public function testAllBlocks(): void
    {
        $expectedBlock = [
            Params::ID => 2,
            Params::NAME => "Общие вопросы",
            Params::WEIGHT => 0,
        ];

        $client = static::createClient();
        $client->jsonRequest('GET', '/api/v1/admin/faq/blocks', [], ['HTTP_AUTHORIZATION' => $this->getAdminToken($client)]);
        $resp = $this->getResp2Arr($client->getResponse(), false);

        $this->assertResponseIsSuccessful();
        $this->assertCount(2, $resp);
        $this->assertEquals((object)$expectedBlock, $resp[0], "Fail equal faq block");
        $this->matchObjectVars(array_keys($expectedBlock), $resp[0]);
    }

    /**
     * @group admin
     * @group faq
     * @group editBlock
     */
    public function testEditBlock(): void
    {
        $client = static::createClient();
        $adminToken = $this->getAdminToken($client);
        /** @var FaqTypeRepository $blockRepo */
        $newBlock = [
            Params::NAME => "Новый блок вопросов",
            Params::WEIGHT => 2
        ];

        $client->jsonRequest('POST', '/api/v1/admin/faq/block/add', $newBlock, [self::HEADER_AUTH => $adminToken]);

        $this->assertResponseIsSuccessful();

        $block = static::getContainer()->get(FaqTypeRepository::class)->findBy([], ['id' => 'DESC'])[0];
        $this->assertNotNull($block);
        $this->assertEquals($newBlock[Params::NAME], $block->getName());
        $this->assertEquals($newBlock[Params::WEIGHT], $block->getWeight());

        $newBlock[Params::NAME] = 'Измененный блок';
        $client->jsonRequest('PUT', '/api/v1/admin/faq/block/' . $block->getId(), $newBlock, [self::HEADER_AUTH => $adminToken]);

        $this->assertResponseIsSuccessful();

        $block = static::getContainer()->get(FaqTypeRepository::class)->find($block->getId());
        $this->assertNotNull($block);
        $this->assertEquals($newBlock[Params::NAME], $block->getName());

        $client->jsonRequest('DELETE', '/api/v1/admin/faq/block/' . $block->getId(), [], [self::HEADER_AUTH => $adminToken]);

        $this->assertResponseIsSuccessful();

        $block = static::getContainer()->get(FaqTypeRepository::class)->find($block->getId());
        $this->assertNull($block);
    }

    /**
     * @group admin
     * @group faq
     * @group allQuestions
     */
    public function testAllQuestions(): void
    {
        $expectedQuestion = [
            Params::ID => 1,
            Params::TITLE => "Возможен ли самовывоз заказанного товара?",
            Params::ANSWER => 'Да, Вы можете самостоятельно забрать заказанные товары в наших магазинах.',
            Params::WEIGHT => 0,
            Params::BLOCK => 1,
        ];

        $client = static::createClient();
        $client->jsonRequest('GET', '/api/v1/admin/faq/questions', [], ['HTTP_AUTHORIZATION' => $this->getAdminToken($client)]);
        $resp = $this->getResp2Arr($client->getResponse());

        $this->assertResponseIsSuccessful();
        $this->assertCount(3, $resp);
        $this->matchObjectVars(array_keys($expectedQuestion), (object)$resp[2]);
        $this->assertEquals($expectedQuestion[Params::ID], $resp[2][Params::ID]);
        $this->assertEquals($expectedQuestion[Params::TITLE], $resp[2][Params::TITLE]);
        $this->assertEquals($expectedQuestion[Params::ANSWER], $resp[2][Params::ANSWER]);
        $this->assertEquals($expectedQuestion[Params::WEIGHT], $resp[2][Params::WEIGHT]);
        $this->assertEquals($expectedQuestion[Params::BLOCK], $resp[2][Params::BLOCK]);
    }

    /**
     * @group admin
     * @group faq
     * @group editQuestion
     */
    public function testEditQuestion(): void
    {
        $client = static::createClient();
        $adminToken = $this->getAdminToken($client);
        /** @var FaqTypeRepository $blockRepo */
        $newQuestion = [
            Params::TITLE => "Новый вопрос",
            Params::ANSWER => 'Новый ответ',
            Params::WEIGHT => 3,
            Params::BLOCK => 2,
        ];

        $client->jsonRequest('POST', '/api/v1/admin/faq/question/add', $newQuestion, [self::HEADER_AUTH => $adminToken]);

        $this->assertResponseIsSuccessful();

        $faq = static::getContainer()->get(FaqRepository::class)->findBy([], ['id' => 'DESC'])[0];
        $this->assertNotNull($faq);
        $this->assertEquals($newQuestion[Params::TITLE], $faq->getQuestion());
        $this->assertEquals($newQuestion[Params::ANSWER], $faq->getAnswer());
        $this->assertEquals($newQuestion[Params::WEIGHT], $faq->getWeight());
        $this->assertEquals($newQuestion[Params::BLOCK], $faq->getType()->getId());

        $newQuestion[Params::TITLE] = 'Измененный вопрос';
        $client->jsonRequest('PUT', '/api/v1/admin/faq/question/' . $faq->getId(), $newQuestion, [self::HEADER_AUTH => $adminToken]);

        $this->assertResponseIsSuccessful();

        $faq = static::getContainer()->get(FaqRepository::class)->find($faq->getId());
        $this->assertNotNull($faq);
        $this->assertEquals($newQuestion[Params::TITLE], $faq->getQuestion());

        $client->jsonRequest('DELETE', '/api/v1/admin/faq/question/' . $faq->getId(), [], [self::HEADER_AUTH => $adminToken]);

        $this->assertResponseIsSuccessful();

        $faq = static::getContainer()->get(FaqRepository::class)->find($faq->getId());
        $this->assertNull($faq);
    }
}
