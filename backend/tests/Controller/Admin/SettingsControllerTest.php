<?php

namespace App\Tests\Controller\Admin;

use App\Controller\ApiController;
use App\Entity\Settings;
use App\Helpers\Params;
use App\Repository\SettingsRepository;
use App\Tests\Controller\CustomWebTestCase;

/**
 * Личный кабинет
 */
class SettingsControllerTest extends CustomWebTestCase
{
    /**
     * @group settings
     * @group getSettings
     */
    public function testGetSettings()
    {
        $client = static::createClient();
        $token = $this->getAdminToken($client);

        $expected = [
            Params::MIN_MANY_QUANTITY => 100,
            Params::HOLIDAYS => [],
            Params::DELIVERY_SHIFT => 1,
            Params::DELIVERY_FAST_TIME => '11:00',
            Params::VIBER => null,
            Params::WHATSAPP => null,
            Params::TELEGRAM => null,
        ];

        // Получение настроек
        $client->jsonRequest("GET", "/api/v1/admin/settings", [], [self::HEADER_AUTH => $token]);
        $this->assertResponseIsSuccessful();

        $resp = $this->getResp2Arr($client->getResponse());
        $this->matchObjectVars(array_keys($expected), (object)$resp);
        $this->assertEquals($expected[Params::MIN_MANY_QUANTITY], $resp[Params::MIN_MANY_QUANTITY]);
        $this->assertEquals($expected[Params::HOLIDAYS], $resp[Params::HOLIDAYS]);
        $this->assertEquals($expected[Params::DELIVERY_SHIFT], $resp[Params::DELIVERY_SHIFT]);
        $this->assertEquals($expected[Params::DELIVERY_FAST_TIME], $resp[Params::DELIVERY_FAST_TIME]);

        // Невалидный токен
        $client->jsonRequest("GET", "/api/v1/admin/settings", [], [self::HEADER_AUTH =>  'invalidToken']);
        $this->assertResponseStatusCodeSame(ApiController::STATUS_UNAUTHORIZATION);
    }

    /**
     * @group settings
     * @group updateSettings
     */
    public function testUpdateSettings()
    {
        $client = static::createClient();
        $token = $this->getAdminToken($client);

        $expected = [
            Params::MIN_MANY_QUANTITY => 300,
            Params::HOLIDAYS => ['2022-01-01T11:20:33+0300'],
            Params::DELIVERY_SHIFT => 2,
            Params::DELIVERY_FAST_TIME => '10:00',
            Params::VIBER => '+79889878',
            Params::WHATSAPP => '+79889876',
            Params::TELEGRAM => '+79889875',
        ];

        // Получение настроек
        $client->jsonRequest("PUT", "/api/v1/admin/settings", $expected, [self::HEADER_AUTH => $token]);
        $this->assertResponseIsSuccessful();
        /** @var Settings $settings */
        $settings = static::getContainer()->get(SettingsRepository::class)->find(1);
        $this->assertNotNull($settings);
        $this->assertEquals($expected[Params::MIN_MANY_QUANTITY], $settings->getMinManyQuantity());
        $this->assertEquals($expected[Params::HOLIDAYS], $settings->getHolidays());
        $this->assertEquals($expected[Params::DELIVERY_SHIFT], $settings->getDeliveryShift());
        $this->assertEquals($expected[Params::DELIVERY_FAST_TIME], $settings->getDeliveryFastTime());
        $this->assertEquals($expected[Params::VIBER], $settings->getViber());
        $this->assertEquals($expected[Params::WHATSAPP], $settings->getWhatsapp());
        $this->assertEquals($expected[Params::TELEGRAM], $settings->getTelegram());

        // Невалидный токен
        $client->jsonRequest("PUT", "/api/v1/admin/settings", $expected, [self::HEADER_AUTH =>  'invalidToken']);
        $this->assertResponseStatusCodeSame(ApiController::STATUS_UNAUTHORIZATION);
    }
}