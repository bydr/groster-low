<?php

namespace App\Tests\Controller\Admin;

use App\Entity\Discount;
use App\Helpers\Params;
use App\Repository\CategoryRepository;
use App\Repository\DiscountRepository;
use App\Tests\Controller\CustomWebTestCase;
use DateTime;
use DateTimeInterface;

/**
 * Управление скидками
 */
class PromocodeControllerTest extends CustomWebTestCase
{
    /**
     * @group admin
     * @group discount
     */
    public function testAll()
    {
        $client = static::createClient();
        $discounts = static::getContainer()->get(DiscountRepository::class)->findAll();

        $client->jsonRequest('GET', '/api/v1/admin/promocodes', [], ['HTTP_AUTHORIZATION' => $this->getAdminToken($client)]);
        $this->assertResponseIsSuccessful();

        $resp = $this->getResp2Arr($client->getResponse());
        $this->assertCount(count($discounts), $resp);
    }

    /**
     * @group admin
     * @group discount
     */
    public function testOne()
    {
        $client = static::createClient();
        $discounts = static::getContainer()->get(DiscountRepository::class)->findAll();
        /** @var Discount $discount */
        $discount = $discounts[0];

        $client->jsonRequest('GET', '/api/v1/admin/promocode/' . $discount->getId(), [], ['HTTP_AUTHORIZATION' => $this->getAdminToken($client)]);
        $this->assertResponseIsSuccessful();

        $resp = $this->getResp2Arr($client->getResponse());
        $this->assertEquals($discount->getName(), $resp[Params::NAME]);
        $this->assertEquals($discount->getCode(), $resp[Params::CODE]);
        $this->assertEquals($discount->getType(), $resp[Params::TYPE]);
        $this->assertEquals($discount->getValue(), $resp[Params::VALUE]);
        $this->assertEquals($discount->getReusable(), $resp[Params::REUSABLE]);
        $this->assertGreaterThan(0, count($resp[Params::CATEGORIES]));
        $this->assertEquals($discount->getCategoriesUuid(), $resp[Params::CATEGORIES]);
    }

    /**
     * @group admin
     * @group discount
     */
    public function testSaveOne(): void
    {
        $client = static::createClient();
        $category = static::getContainer()->get(CategoryRepository::class)->find(199);

        $newPromo = [
            "name" => "new test promo",
            "code" => "newtestpromo",
            "type" => Discount::TYPE_RATE,
            "value" => 10,
            "reusable" => true,
            "start" => (new DateTime())->format(DateTimeInterface::ISO8601),
            "finish" => (new DateTime())->modify("+1 hour")->format(DateTimeInterface::ISO8601),
            "categories" => $category->getUuid(),
        ];


        $client->jsonRequest('POST', '/api/v1/admin/promocode/add', $newPromo, ['HTTP_AUTHORIZATION' => $this->getAdminToken($client)]);
        $result = $this->getResp2Arr($client->getResponse());

        $this->assertResponseIsSuccessful();
        $this->assertResponseStatusCodeSame(200);
        $this->assertGreaterThan(0, $result['id'], "Error get promocode");
        $this->assertEquals($newPromo['name'], $result['name'], "Fail equal promocode name");
        $this->assertEquals($newPromo['value'], $result['value'], "Fail equal promocode value");
    }

    /**
     * @group admin
     * @group discount
     */
    public function testUpdateOne(): void
    {
        $client = static::createClient();
        $category = static::getContainer()->get(CategoryRepository::class)->find(199);

        $updatePromo = [
            "name" => "update test promo",
            "code" => "newtestpromo",
            "type" => Discount::TYPE_RATE,
            "value" => 15,
            "reusable" => true,
            "start" => (new DateTime())->format(DateTimeInterface::ISO8601),
            "finish" => (new DateTime())->modify("+1 hour")->format(DateTimeInterface::ISO8601),
            "categories" => $category->getUuid(),
        ];

        $client->jsonRequest('PUT', '/api/v1/admin/promocode/1', $updatePromo, ['HTTP_AUTHORIZATION' => $this->getAdminToken($client)]);

        $this->assertResponseIsSuccessful();
        $result = $this->getResp2Arr($client->getResponse());

        $this->assertEquals($updatePromo['name'], $result['name']);
        $this->assertEquals($updatePromo['value'], $result['value']);
    }
}
