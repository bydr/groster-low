<?php

namespace App\Tests\Controller\Admin;

use App\ApiError;
use App\Controller\ApiController;
use App\Entity\Banner;
use App\Helpers\Params;
use App\Repository\BannerRepository;
use App\Tests\Controller\CustomWebTestCase;

class BannerControllerTest extends CustomWebTestCase
{
    const IMAGE_BASE64 = 'iVBORw0KGgoAAAANSUhEUgAAAeEAAANFCAIAAADDOtOIAAAACXBIWXMAAA7EAAAOxAGVKw4bAAAMkUlEQVR4Xu3dsc1iOQBG0fdWrwQySnALI40mIKIbJDJaIaAiAipZrTTSn6ANnCGRwg3OieyvgBs48frr958FgKR/XgcAMjQaoEujAbo0GqBLowG6NBqgS6MBujQaoEujAbo0GqBLowG6NBqga/v737+vGwAN28/Pz+sGQIO3DoAujQbo0miALo0G6NJogC6NBujSaIAujQbo0miALo0G6NJogC6NBujSaIAujQbo0miALo0G6NJogC6NBujSaIAujQbo0miALo0G6NJogC6NBujSaIAujQbo0miALo0G6NJogC6NBujSaIAujQbo0miALo0G6NJogC6NBujSaIAujQbo0miALo0G6NJogC6NBujSaIAujQbo0miALo0G6NJogC6NBujSaIAujQbo0miALo0G6NJogC6NBujSaIAujQbo0miALo0G6NJogC6NBujSaIAujQbo0miALo0G6NJogC6NBujSaIAujQbo0miALo0G6NJogC6NBujSaIAujQbo0miALo0G6NJogC6NBujSaIAujQbo0miALo0G6NJogC6NBujSaIAujQbo0miALo0G6NJogC6NBujSaIAujQbo0miALo0G6NJogC6NBujSaIAujQbo0miALo0G6NJogC6NBujSaIAujQbo0miALo0G6NJogC6NBujSaIAujQbo0miALo0G6NJogC6NBujSaIAujQbo0miALo0G6NJogC6NBujSaIAujQbo0miALo0G6NJogC6NBujSaIAujQbo0miALo0G6NJogC6NBujSaIAujQbo0miALo0G6NJogC6NBujSaIAujQbo0miALo0G6NJogC6NBujSaIAujQbo0miALo0G6NJogC6NBujSaIAujQbo0miALo0G6NJogC6NBujSaIAujQbo0miALo0G6NJogC6NBujSaIAujQbo2pb98XI+7Obteb+ebo95tn9mB3hvHWO8bgA0eOsA6NJogC6NBujSaIAujQbo0miALo0G6NJogC6NBujSaIAujQbo0miALo0G6NJogC6NBujSaIAujQbo0miALo0G6NJogC7/gn97B3jPv+AAXd46ALo0GqBLowG6NBqgS6MBujQaoEujAbo0GqBLowG6NBqgS6MBujQaoEujAbo0GqBLowG6NBqgS6MBujQaoEujAbo0GqBLowG6NBqgS6MBujQaoEujAbo0GqBLowG6NBqgS6MBujQaoEujAbo0GqBLowG6NBqgS6MBujQaoEujAbo0GqBLowG6NBqgS6MBujQaoGtb9sfL+bCbt+f9ero95tn+mR3gvXWM8boB0OCtA6BLowG6NBqgS6MBujQaoEujAbo0GqBLowG6NBqgS6MBujQaoEujAbo0GqBLowG6NBqgS6MBujQaoEujAbo0GqBLowG6NBqgS6MBujQaoEujAbo0GqBLowG6NBqgS6MBujQaoEujAbo0GqBLowG6NBqgS6MBujQaoEujAbo0GqBLowG6NBqgS6MBujQaoEujAbq2ZX+8nA+7eXver6fbY57tn9kB3lvHGK8bAA3eOgC6NBqgS6MBujQaoEujAbo0GqBLowG6NBqgS6MBujQaoEujAbo0GqBLowG6NBqgS6MBujQaoEujAbo0GqBLowG6NBqgS6MBujQaoEujAbo0GqBLowG6NBqgS6MBujQaoEujAbo0GqBLowG6NBqgS6MBujQaoEujAbo0GqBLowG6NBqgS6MBujQaoEujAbo0GqBrW/bHy/mwm7fn/Xq6PebZ/pkd4L11jPG6AdDgrQOgS6MBujQaoEujAbo0GqBLowG6NBqgS6MBujQaoEujAbo0GqBLowG6NBqgS6MBujQaoEujAbo0GqBLowG6NBqgS6MBuvwL/u0d4D3/ggN0eesA6NJogC6NBujSaIAujQbo0miALo0G6NJogC6NBujSaIAujQbo0miALo0G6NJogC6NBujSaIAujQbo0miALo0G6NJogC6NBujSaIAujQbo0miALo0G6NJogC6NBujSaIAujQbo0miALo0G6NJogC6NBujSaIAujQbo0miALo0G6NJogC6NBujSaIAujQbo0miArm3ZHy/nw27envfr6faYZ/tndoD31jHG6wZAg7cOgC6NBujSaIAujQbo0miALo0G6NJogC6NBujSaIAujQbo0miALo0G6NJogC6NBujSaIAujQbo0miALo0G6NJogC6NBujyL/i3d4D3/AsO0OWtA6BLowG6NBqgS6MBujQaoEujAbo0GqBLowG6NBqgS6MBujQaoEujAbo0GqBLowG6NBqgS6MBujQaoEujAbo0GqBLowG6NBqgS6MBujQaoEujAbo0GqBLowG6NBqgS6MBujQaoEujAbo0GqBLowG6NBqgS6MBujQaoEujAbo0GqBLowG6NBqgS6MBujQaoEujAbo0GqBLowG6NBqgS6MBujQaoEujAbo0GqBLowG6NBqgS6MBujQaoEujAbo0GqBLowG6NBqgS6MBujQaoEujAbo0GqBLowG6NBqgS6MBujQaoEujAbo0GqBLowG6NBqgS6MBujQaoEujAbo0GqBLowG6NBqgS6MBujQaoEujAbo0GqBLowG6NBqgS6MBujQaoEujAbo0GqBLowG6NBqgS6MBujQaoEujAbo0GqBLowG6NBqgS6MBujQaoEujAbo0GqBLowG6NBqgS6MBujQaoEujAbo0GqBLowG6NBqgS6MBujQaoEujAbo0GqBLowG6NBqgS6MBujQaoEujAbo0GqBLowG6NBqgS6MBujQaoEujAbo0GqBLowG6NBqgS6MBujQaoEujAbo0GqBLowG6NBqgS6MBujQaoEujAbo0GqBLowG6NBqgS6MBujQaoEujAbo0GqBLowG6NBqgS6MBujQaoEujAbo0GqBLowG6NBqgS6MBujQaoEujAbo0GqBLowG6NBqgS6MBujQaoEujAbo0GqBLowG6NBqgS6MBujQaoEujAbo0GqBLowG6NBqgS6MBujQaoEujAbo0GqBLowG6NBqgS6MBujQaoEujAbo0GqBLowG6NBqgS6MBujQaoEujAbo0GqBLowG6NBqgS6MBujQaoEujAbo0GqBLowG6NBqgS6MBujQaoEujAbo0GqBLowG6NBqgS6MBujQaoEujAbo0GqBLowG6NBqgS6MBujQaoEujAbo0GqBLowG6NBqgS6MBujQaoEujAbo0GqBLowG6NBqgS6MBujQaoEujAbo0GqBLowG6tmV/vJwPu3l73q+n22Oe7Xa73f7tfR1jzDMANd46ALo0GqBLowG6NBqgS6MBujQaoEujAbo0GqBLowG6NBqgS6MBujQaoEujAbo0GqBLowG6NBqgS6MBujQaoEujAbo0GqDLv+B2u93e3f0LDtDlrQOgS6MBujQaoEujAbo0GqBLowG6NBqgS6MBujQaoEujAbo0GqBLowG6NBqgS6MBujQaoEujAbo0GqBLowG6NBqgS6MBujQaoEujAbo0GqBLowG6NBqgS6MBujQaoEujAbo0GqBLowG6NBqgS6MBujQaoEujAbo0GqBLowG6NBqgS6MBujQaoEujAbo0GqBLowG6tmV/vJwPu3l73q+n22Oe7Xa73f7tfR1jzDMANd46ALo0GqBLowG6NBqgS6MBujQaoEujAbo0GqBLowG6NBqgS6MBujQaoEujAbo0GqBLowG6NBqgS6MBujQaoEujAbo0GqBLowG6NBqgS6MBujQaoEujAbo0GqBLowG6NBqgS6MBujQaoEujAbo0GqBLowG6NBqgS6MBujQaoEujAbo0GqBLowG6NBqgS6MBujQaoGtb9sfL+bCbt+f9ero95tlut9vt397XMcY8A1DjrQOgS6MBujQaoEujAbo0GqBLowG6NBqgS6MBujQaoEujAbo0GqBLowG6NBqgS6MBujQaoEujAbo0GqBLowG6NBqgS6MBujQaoEujAbo0GqBLowG6NBqgS6MBujQaoEujAbo0GqBLowG6NBqgS6MBujQaoEujAbo0GqBLowG6NBqgS6MBujQaoEujAbo0GqBLowG6tmV/vJwPu3l73q+n22Oe7Xa73f7tfR1jzDMANd46ALo0GqBLowG6NBqgS6MBujQaoEujAbo0GqBLowG6NBqgS6MBujQaoEujAbo0GqBLowG6NBqgS6MBujQaoEujAbo0GqDLv+B2u93e3f0LDtDlrQOgS6MBujQaoEujAbo0GqBLowG6NBqgS6MBujQaoEujAbo0GqBLowG6NBqgS6MBujQaoEujAbo0GqBLowG6/gcMj4c0c0NI8AAAAABJRU5ErkJggg==';

    /**
     * @group admin
     * @group banner
     */
    public function testAddValid()
    {
        $client = static::createClient();

        $banner = [
            "desktop" => self::IMAGE_BASE64,
            "tablet" => self::IMAGE_BASE64,
            "mobile" => self::IMAGE_BASE64,
            "type" => 1,
            "url" => "http://temp.trd",
            "weight" => 1
        ];

        $client->jsonRequest("POST", "/api/v1/admin/pictures", $banner, ['HTTP_AUTHORIZATION' => $this->getAdminToken($client)]);
        $this->assertResponseIsSuccessful();

        $client->jsonRequest("GET", "/api/v1/admin/pictures", [], ['HTTP_AUTHORIZATION' => $this->getAdminToken($client)]);
        $this->assertResponseIsSuccessful();

        $resp = $this->getResp2Arr($client->getResponse());
        $this->assertCount(2, $resp, "Error to equal count banners");
        $banner = $resp[1];

        $publicDir = sprintf("%s/public", static::getContainer()->getParameter('kernel.project_dir'));

        $this->assertTrue($this->issetImage($publicDir, $banner[Params::DESKTOP]));
        $this->assertTrue($this->issetImage($publicDir, $banner[Params::TABLET]));
        $this->assertTrue($this->issetImage($publicDir, $banner[Params::MOBILE]));
    }

    /**
     * @group admin
     * @group banner
     */
    public function testAddInvalid()
    {
        $client = static::createClient();

        // Invalid type
        $banner = [
            Params::DESKTOP => self::IMAGE_BASE64,
            Params::TABLET => self::IMAGE_BASE64,
            Params::MOBILE => self::IMAGE_BASE64,
            Params::TYPE => 9999,
            Params::URL => "http://temp.trd",
            Params::WEIGHT => 1
        ];

        $client->jsonRequest("POST", "/api/v1/admin/pictures", $banner, ['HTTP_AUTHORIZATION' => $this->getAdminToken($client)]);
        $this->assertResponseStatusCodeSame(ApiController::STATUS_BAD_REQUEST);

        $resp = $this->getResp2Arr($client->getResponse());
        $this->assertArrayHasKey(Params::CODE, $resp);
        $this->assertEquals(ApiError::ERROR_INVALID_DATA, $resp[Params::CODE]);

        // Not set url
        $banner = [
            Params::DESKTOP => self::IMAGE_BASE64,
            Params::TABLET => self::IMAGE_BASE64,
            Params::MOBILE => self::IMAGE_BASE64,
            Params::TYPE => 1,
            Params::WEIGHT => 1
        ];

        $client->jsonRequest("POST", "/api/v1/admin/pictures", $banner, ['HTTP_AUTHORIZATION' => $this->getAdminToken($client)]);
        $this->assertResponseStatusCodeSame(ApiController::STATUS_BAD_REQUEST);

        $resp = $this->getResp2Arr($client->getResponse());
        $this->assertArrayHasKey(Params::CODE, $resp);
        $this->assertEquals(ApiError::ERROR_INVALID_DATA, $resp[Params::CODE]);
    }

    /**
     * @group admin
     * @group banner
     */
    public function testUpdate()
    {
        $client = static::createClient();

        /** @var Banner $banner */
        $banner = static::getContainer()->get(BannerRepository::class)->find(1);
        $this->assertNotNull($banner);
        $this->assertEmpty($banner->getUpdatedAt());

        $data = [
            Params::DESKTOP => self::IMAGE_BASE64,
            Params::TYPE => $banner->getType(),
            Params::WEIGHT => $banner->getWeight(),
            Params::URL => $banner->getUrl(),
        ];

        $client->jsonRequest("PUT", "/api/v1/admin/pictures/" . $banner->getId(), $data, ['HTTP_AUTHORIZATION' => $this->getAdminToken($client)]);
        $this->assertResponseIsSuccessful();

        /** @var Banner $updatedBanner */
        $updatedBanner = static::getContainer()->get(BannerRepository::class)->find(1);
        $this->assertNotEquals($banner->getDesktop(), $updatedBanner->getDesktop());
        $this->assertEquals($data[Params::TYPE], $updatedBanner->getType());
        $this->assertEquals($data[Params::WEIGHT], $updatedBanner->getWeight());
        $this->assertEquals($data[Params::URL], $updatedBanner->getUrl());
        $this->assertNotEmpty($updatedBanner->getUpdatedAt());

        $publicDir = sprintf("%s/public", static::getContainer()->getParameter('kernel.project_dir'));

        $this->assertFalse($this->issetImage($publicDir, $banner->getDesktop()));
        $this->assertTrue($this->issetImage($publicDir, $updatedBanner->getDesktop()));
    }

    /**
     * @group admin
     * @group banner
     */
    public function testDelete()
    {
        $client = static::createClient();

        /** @var Banner $banner */
        $banner = static::getContainer()->get(BannerRepository::class)->find(1);

        $client->jsonRequest("DELETE", "/api/v1/admin/pictures/" . $banner->getId(), [], ['HTTP_AUTHORIZATION' => $this->getAdminToken($client)]);
        $this->assertResponseIsSuccessful();

        $publicDir = sprintf("%s/public", static::getContainer()->getParameter('kernel.project_dir'));

        $this->assertFalse($this->issetImage($publicDir, $banner->getDesktop()));
        $this->assertFalse($this->issetImage($publicDir, $banner->getTablet()));
        $this->assertFalse($this->issetImage($publicDir, $banner->getMobile()));

        $banner = static::getContainer()->get(BannerRepository::class)->find(1);
        $this->assertNull($banner);
    }

    /**
     * Проверка, что файл существует
     * @param $publicDir
     * @param $filename
     * @return void
     */
    private function issetImage($publicDir, $filename): bool
    {
        $this->assertNotEmpty($filename);

        if(!preg_match('~.*/(.*\.\w{3,4})~', $filename, $matches)) {
            return false;
        }

        $filepath = sprintf("%s/%s/%s", $publicDir, Banner::FOLDER, $matches[1]);
        return is_file($filepath);
    }


}