<?php

namespace App\Tests\Controller\Admin;

use App\Entity\PaymentMethod;
use App\Helpers\Params;
use App\Repository\PaymentMethodRepository;
use App\Tests\Controller\CustomWebTestCase;

/**
 * Тесты способов оплаты
 */
class PaymentMethodControllerTest extends CustomWebTestCase
{
    /**
     * @group admin
     * @group paymentMethods
     */
    public function testPaymentMethods()
    {
        $client = static::createClient();
        $token = $this->getAdminToken($client);

        $expectedFields = [Params::UID, Params::NAME, Params::WEIGHT, Params::IS_ACTIVE];

        $client->jsonRequest("GET", "/api/v1/admin/payment-methods", [], [self::HEADER_AUTH => $token]);
        $this->assertResponseIsSuccessful();

        $resp = $this->getResp2Arr($client->getResponse());
        $this->assertCount(4, $resp, "Fail to get methods count");
        $this->matchObjectVars($expectedFields, (object)$resp[0]);
    }

    /**
     * @group admin
     * @group paymentMethods
     */
    public function testPaymentMethodAdd()
    {
        $client = static::createClient();
        $token = $this->getAdminToken($client);

        // Добавление
        $expected = [
            Params::NAME => 'оплата биткоинами',
            Params::WEIGHT => 4,
            Params::IS_ACTIVE => true,
        ];

        $client->jsonRequest("POST", "/api/v1/admin/payment-method/add", $expected, [self::HEADER_AUTH => $token]);
        $this->assertResponseIsSuccessful();

        $resp = $this->getResp2Arr($client->getResponse());
        $this->assertArrayHasKey(Params::UID, $resp);

        $uid = $resp[Params::UID];

        /** @var PaymentMethod $method */
        $method = static::getContainer()->get(PaymentMethodRepository::class)->findOneByUid($uid);
        $this->assertNotNull($method);

        $this->assertEquals($expected[Params::NAME], $method->getName());
        $this->assertEquals($expected[Params::WEIGHT], $method->getWeight());
        $this->assertEquals($expected[Params::IS_ACTIVE], $method->getIsActive());

        // Обновление
        $expected[Params::NAME] = 'обновленный новый способ';
        $client->jsonRequest("PUT", "/api/v1/admin/payment-method/{$uid}", $expected, [self::HEADER_AUTH => $token]);
        $this->assertResponseIsSuccessful();

        $resp = $this->getResp2Arr($client->getResponse());
        $this->assertArrayHasKey(Params::UID, $resp);

        /** @var PaymentMethod $method */
        $method = static::getContainer()->get(PaymentMethodRepository::class)->findOneByUid($uid);
        $this->assertNotNull($method);

        $this->assertEquals($expected[Params::NAME], $method->getName());
        $this->assertEquals($expected[Params::WEIGHT], $method->getWeight());
        $this->assertEquals($expected[Params::IS_ACTIVE], $method->getIsActive());
    }
}