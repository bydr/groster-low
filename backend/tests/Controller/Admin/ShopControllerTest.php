<?php

namespace App\Tests\Controller\Admin;

use App\ApiError;
use App\Entity\Store;
use App\Helpers\Params;
use App\Repository\StoreRepository;
use App\Tests\Controller\CustomWebTestCase;
use Faker\Factory;

/**
 * Class ShopControllerTest
 */
class ShopControllerTest extends CustomWebTestCase
{
    const IMAGE_BASE64 = 'iVBORw0KGgoAAAANSUhEUgAAAeEAAANFCAIAAADDOtOIAAAACXBIWXMAAA7EAAAOxAGVKw4bAAAMkUlEQVR4Xu3dsc1iOQBG0fdWrwQySnALI40mIKIbJDJaIaAiAipZrTTSn6ANnCGRwg3OieyvgBs48frr958FgKR/XgcAMjQaoEujAbo0GqBLowG6NBqgS6MBujQaoEujAbo0GqBLowG6NBqga/v737+vGwAN28/Pz+sGQIO3DoAujQbo0miALo0G6NJogC6NBujSaIAujQbo0miALo0G6NJogC6NBujSaIAujQbo0miALo0G6NJogC6NBujSaIAujQbo0miALo0G6NJogC6NBujSaIAujQbo0miALo0G6NJogC6NBujSaIAujQbo0miALo0G6NJogC6NBujSaIAujQbo0miALo0G6NJogC6NBujSaIAujQbo0miALo0G6NJogC6NBujSaIAujQbo0miALo0G6NJogC6NBujSaIAujQbo0miALo0G6NJogC6NBujSaIAujQbo0miALo0G6NJogC6NBujSaIAujQbo0miALo0G6NJogC6NBujSaIAujQbo0miALo0G6NJogC6NBujSaIAujQbo0miALo0G6NJogC6NBujSaIAujQbo0miALo0G6NJogC6NBujSaIAujQbo0miALo0G6NJogC6NBujSaIAujQbo0miALo0G6NJogC6NBujSaIAujQbo0miALo0G6NJogC6NBujSaIAujQbo0miALo0G6NJogC6NBujSaIAujQbo0miALo0G6NJogC6NBujSaIAujQbo0miALo0G6NJogC6NBujSaIAujQbo0miALo0G6NJogC6NBujSaIAujQbo0miALo0G6NJogC6NBujSaIAujQbo0miALo0G6NJogC6NBujSaIAujQbo0miALo0G6NJogC6NBujSaIAujQbo0miALo0G6NJogC6NBujSaIAujQbo0miALo0G6NJogC6NBujSaIAujQbo2pb98XI+7Obteb+ebo95tn9mB3hvHWO8bgA0eOsA6NJogC6NBujSaIAujQbo0miALo0G6NJogC6NBujSaIAujQbo0miALo0G6NJogC6NBujSaIAujQbo0miALo0G6NJogC7/gn97B3jPv+AAXd46ALo0GqBLowG6NBqgS6MBujQaoEujAbo0GqBLowG6NBqgS6MBujQaoEujAbo0GqBLowG6NBqgS6MBujQaoEujAbo0GqBLowG6NBqgS6MBujQaoEujAbo0GqBLowG6NBqgS6MBujQaoEujAbo0GqBLowG6NBqgS6MBujQaoEujAbo0GqBLowG6NBqgS6MBujQaoGtb9sfL+bCbt+f9ero95tn+mR3gvXWM8boB0OCtA6BLowG6NBqgS6MBujQaoEujAbo0GqBLowG6NBqgS6MBujQaoEujAbo0GqBLowG6NBqgS6MBujQaoEujAbo0GqBLowG6NBqgS6MBujQaoEujAbo0GqBLowG6NBqgS6MBujQaoEujAbo0GqBLowG6NBqgS6MBujQaoEujAbo0GqBLowG6NBqgS6MBujQaoEujAbq2ZX+8nA+7eXver6fbY57tn9kB3lvHGK8bAA3eOgC6NBqgS6MBujQaoEujAbo0GqBLowG6NBqgS6MBujQaoEujAbo0GqBLowG6NBqgS6MBujQaoEujAbo0GqBLowG6NBqgS6MBujQaoEujAbo0GqBLowG6NBqgS6MBujQaoEujAbo0GqBLowG6NBqgS6MBujQaoEujAbo0GqBLowG6NBqgS6MBujQaoEujAbo0GqBrW/bHy/mwm7fn/Xq6PebZ/pkd4L11jPG6AdDgrQOgS6MBujQaoEujAbo0GqBLowG6NBqgS6MBujQaoEujAbo0GqBLowG6NBqgS6MBujQaoEujAbo0GqBLowG6NBqgS6MBuvwL/u0d4D3/ggN0eesA6NJogC6NBujSaIAujQbo0miALo0G6NJogC6NBujSaIAujQbo0miALo0G6NJogC6NBujSaIAujQbo0miALo0G6NJogC6NBujSaIAujQbo0miALo0G6NJogC6NBujSaIAujQbo0miALo0G6NJogC6NBujSaIAujQbo0miALo0G6NJogC6NBujSaIAujQbo0miArm3ZHy/nw27envfr6faYZ/tndoD31jHG6wZAg7cOgC6NBujSaIAujQbo0miALo0G6NJogC6NBujSaIAujQbo0miALo0G6NJogC6NBujSaIAujQbo0miALo0G6NJogC6NBujyL/i3d4D3/AsO0OWtA6BLowG6NBqgS6MBujQaoEujAbo0GqBLowG6NBqgS6MBujQaoEujAbo0GqBLowG6NBqgS6MBujQaoEujAbo0GqBLowG6NBqgS6MBujQaoEujAbo0GqBLowG6NBqgS6MBujQaoEujAbo0GqBLowG6NBqgS6MBujQaoEujAbo0GqBLowG6NBqgS6MBujQaoEujAbo0GqBLowG6NBqgS6MBujQaoEujAbo0GqBLowG6NBqgS6MBujQaoEujAbo0GqBLowG6NBqgS6MBujQaoEujAbo0GqBLowG6NBqgS6MBujQaoEujAbo0GqBLowG6NBqgS6MBujQaoEujAbo0GqBLowG6NBqgS6MBujQaoEujAbo0GqBLowG6NBqgS6MBujQaoEujAbo0GqBLowG6NBqgS6MBujQaoEujAbo0GqBLowG6NBqgS6MBujQaoEujAbo0GqBLowG6NBqgS6MBujQaoEujAbo0GqBLowG6NBqgS6MBujQaoEujAbo0GqBLowG6NBqgS6MBujQaoEujAbo0GqBLowG6NBqgS6MBujQaoEujAbo0GqBLowG6NBqgS6MBujQaoEujAbo0GqBLowG6NBqgS6MBujQaoEujAbo0GqBLowG6NBqgS6MBujQaoEujAbo0GqBLowG6NBqgS6MBujQaoEujAbo0GqBLowG6NBqgS6MBujQaoEujAbo0GqBLowG6NBqgS6MBujQaoEujAbo0GqBLowG6NBqgS6MBujQaoEujAbo0GqBLowG6NBqgS6MBujQaoEujAbo0GqBLowG6NBqgS6MBujQaoEujAbo0GqBLowG6NBqgS6MBujQaoEujAbo0GqBLowG6NBqgS6MBujQaoEujAbo0GqBLowG6NBqgS6MBujQaoEujAbo0GqBLowG6NBqgS6MBujQaoEujAbo0GqBLowG6NBqgS6MBujQaoEujAbo0GqBLowG6NBqgS6MBujQaoEujAbo0GqBLowG6NBqgS6MBujQaoEujAbo0GqBLowG6tmV/vJwPu3l73q+n22Oe7Xa73f7tfR1jzDMANd46ALo0GqBLowG6NBqgS6MBujQaoEujAbo0GqBLowG6NBqgS6MBujQaoEujAbo0GqBLowG6NBqgS6MBujQaoEujAbo0GqDLv+B2u93e3f0LDtDlrQOgS6MBujQaoEujAbo0GqBLowG6NBqgS6MBujQaoEujAbo0GqBLowG6NBqgS6MBujQaoEujAbo0GqBLowG6NBqgS6MBujQaoEujAbo0GqBLowG6NBqgS6MBujQaoEujAbo0GqBLowG6NBqgS6MBujQaoEujAbo0GqBLowG6NBqgS6MBujQaoEujAbo0GqBLowG6tmV/vJwPu3l73q+n22Oe7Xa73f7tfR1jzDMANd46ALo0GqBLowG6NBqgS6MBujQaoEujAbo0GqBLowG6NBqgS6MBujQaoEujAbo0GqBLowG6NBqgS6MBujQaoEujAbo0GqBLowG6NBqgS6MBujQaoEujAbo0GqBLowG6NBqgS6MBujQaoEujAbo0GqBLowG6NBqgS6MBujQaoEujAbo0GqBLowG6NBqgS6MBujQaoGtb9sfL+bCbt+f9ero95tlut9vt397XMcY8A1DjrQOgS6MBujQaoEujAbo0GqBLowG6NBqgS6MBujQaoEujAbo0GqBLowG6NBqgS6MBujQaoEujAbo0GqBLowG6NBqgS6MBujQaoEujAbo0GqBLowG6NBqgS6MBujQaoEujAbo0GqBLowG6NBqgS6MBujQaoEujAbo0GqBLowG6NBqgS6MBujQaoEujAbo0GqBLowG6tmV/vJwPu3l73q+n22Oe7Xa73f7tfR1jzDMANd46ALo0GqBLowG6NBqgS6MBujQaoEujAbo0GqBLowG6NBqgS6MBujQaoEujAbo0GqBLowG6NBqgS6MBujQaoEujAbo0GqDLv+B2u93e3f0LDtDlrQOgS6MBujQaoEujAbo0GqBLowG6NBqgS6MBujQaoEujAbo0GqBLowG6NBqgS6MBujQaoEujAbo0GqBLowG6/gcMj4c0c0NI8AAAAABJRU5ErkJggg==';

    /**
     * @group admin
     * @group shop
     */
    public function testAllShops()
    {
        $client = static::createClient();

        $activeShops = static::getContainer()->get(StoreRepository::class)->findAll();

        $client->jsonRequest("GET", "/api/v1/admin/shops", [], ['HTTP_AUTHORIZATION' => $this->getAdminToken($client)]);
        $this->assertResponseIsSuccessful();

        $shops = $this->getResp2Arr($client->getResponse());
        $this->assertCount(count($activeShops), $shops, "Error to equal count shops");

        /** @var Store $shop */
        $shop = $activeShops[0];
        $expectedShop = [
            Params::NAME => $shop->getName(),
            Params::UUID => $shop->getUuid(),
            Params::ADDRESS => $shop->getAddress(),
            Params::SCHEDULE => $shop->getSchedule(),
            Params::PHONE => $shop->getPhone(),
            Params::COORDINATES => [
                Params::LATITUDE => $shop->getLatitude(),
                Params::LONGITUDE => $shop->getLongitude(),
            ],
            Params::IS_ACTIVE => true,
            Params::DESCRIPTION => '',
            Params::EMAIL => '',
            Params::IMAGE => ''
        ];
        $this->matchObjectVars(array_keys($expectedShop), (object)$shops[0]);
        $this->assertEquals($expectedShop, $shops[0]);
    }

    /**
     * @group admin
     * @group shop
     */
    public function testGetShopValid()
    {
        $client = static::createClient();

        $activeShops = static::getContainer()->get(StoreRepository::class)->findAll();
        /** @var Store $shop */
        $shop = $activeShops[0];
        $expectedShop = [
            Params::NAME => $shop->getName(),
            Params::UUID => $shop->getUuid(),
            Params::ADDRESS => $shop->getAddress(),
            Params::SCHEDULE => $shop->getSchedule(),
            Params::PHONE => $shop->getPhone(),
            Params::COORDINATES => [
                Params::LATITUDE => $shop->getLatitude(),
                Params::LONGITUDE => $shop->getLongitude(),
            ],
            Params::IS_ACTIVE => true,
            Params::DESCRIPTION => '',
            Params::EMAIL => '',
            Params::IMAGE => ''
        ];

        $client->jsonRequest("GET", "/api/v1/admin/shops/" . $shop->getUuid(), [], [
            'HTTP_AUTHORIZATION' => $this->getAdminToken($client)
        ]);
        $this->assertResponseIsSuccessful();

        $actualShop = $this->getResp2Arr($client->getResponse());

        $this->matchObjectVars(array_keys($expectedShop), (object)$actualShop);
        $this->assertEquals((object)$expectedShop, (object)$actualShop);
    }

    /**
     * @group admin
     * @group shop
     */
    public function testGetShopInvalidUid()
    {
        $client = static::createClient();

        $client->jsonRequest("GET", "/api/v1/admin/shops/invaliduid", [], [
            'HTTP_AUTHORIZATION' => $this->getAdminToken($client)
        ]);
        $this->assertResponseStatusCodeSame(400);

        $result = $this->getResp2Arr($client->getResponse());
        $this->assertArrayHasKey("code", $result, "Error to get error code");
        $this->assertEquals(ApiError::ERROR_BAD_REQUEST, $result['code'], "Error to get error code status");
    }

    /**
     * @group admin
     * @group shop
     */
    public function testGetShopWrongMethod()
    {
        $client = static::createClient();

        $client->jsonRequest("POST", "/api/v1/admin/shops", [], [
            'HTTP_AUTHORIZATION' => $this->getAdminToken($client)
        ]);
        $this->assertResponseStatusCodeSame(405);
    }

    /**
     * @group admin
     * @group shop
     */
    public function testUpdateShop()
    {
        $client = static::createClient();
        $faker = Factory::create();
        $shop = static::getContainer()->get(StoreRepository::class)->findOneBy([], ['id' => 'DESC']);

        $updateShop = [
            Params::ADDRESS => 'Новый адрес',
            Params::SCHEDULE => 'Пн-Сб 10:00-20:00',
            Params::PHONE => '+79880000001',
            Params::COORDINATES => [
                Params::LATITUDE => $faker->latitude,
                Params::LONGITUDE => $faker->longitude,
            ],
            Params::EMAIL => 'updateshop@groster.ru',
            Params::DESCRIPTION => $faker->text(),
            Params::IS_ACTIVE => false,
        ];

        $client->jsonRequest("PUT", "/api/v1/admin/shops/" . $shop->getUuid(), $updateShop, ['HTTP_AUTHORIZATION' => $this->getAdminToken($client)]);
        $this->assertResponseIsSuccessful();

        $shop = $this->getResp2Arr($client->getResponse());
        $updateShop[Params::UUID] = $shop[Params::UUID];
        $updateShop[Params::IMAGE] = $shop[Params::IMAGE];
        $updateShop[Params::NAME] = 'Основной склад (г. Волгоград, ул Кольцевая 64)';

        $this->matchObjectVars(array_keys($updateShop), (object)$shop);
        $this->assertEquals($updateShop, $shop);
    }
}