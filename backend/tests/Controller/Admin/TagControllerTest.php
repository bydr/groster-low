<?php

namespace App\Tests\Controller\Admin;

use App\Repository\TagRepository;
use App\Tests\Controller\CustomWebTestCase;

/**
 * Class TagControllerTest
 */
class TagControllerTest extends CustomWebTestCase
{
    /**
     * @group admin
     * @group tag
     */
    public function testSaveOne(): void
    {
        $newTag = [
            "name" => "test tag",
            "url" => "/test tag",
            "meta_title" => "title",
            "meta_description" => "description",
            "h1" => "h1",
            "text" => "bottom text"
        ];

        $client = static::createClient();
        $client->jsonRequest('POST', '/api/v1/admin/tag/add', $newTag, ['HTTP_AUTHORIZATION' => $this->getAdminToken($client)]);
        $tag = $this->getResp2Arr($client->getResponse(), false);

        $this->assertResponseIsSuccessful();
        $this->assertResponseStatusCodeSame(200);
        $this->assertGreaterThan(0, $tag->id, "Error get tags");
        $this->assertEquals($newTag['name'], $tag->name, "Fail equal tag name");
        $this->assertEquals($newTag['url'], $tag->url, "Fail equal tag url");
    }

    /**
     * @group admin
     * @group tag
     */
    public function testUpdateOne(): void
    {
        $expectedTag = [
            "name" => "Updated tag",
            "url" => "http://updated.tag",
            "meta_title" => "Mr.",
            "meta_description" => "I'll never go THERE again!' said Alice indignantly. 'Let me alone!' 'Serpent, I say again!' repeated the Pigeon, raising its voice to a farmer, you know, this sort of knot, and then the different.",
            "h1" => "Prof.",
            "text" => "I think I can say.' This was not otherwise than."
        ];

        $client = static::createClient();
        $client->jsonRequest('PUT', '/api/v1/admin/tags/1', $expectedTag, ['HTTP_AUTHORIZATION' => $this->getAdminToken($client)]);

        $this->assertResponseIsSuccessful();
        $this->assertResponseStatusCodeSame(200);

        $client->jsonRequest('GET', '/api/v1/catalog/tags/1');
        $tag = $this->getResp2Arr($client->getResponse(), false);

        $this->assertResponseIsSuccessful();
        $this->assertResponseStatusCodeSame(200);
        $this->assertEquals((object)$expectedTag, $tag);
    }

    /**
     * @group admin
     * @group tag
     */
    public function testDeleteOne(): void
    {
        $client = static::createClient();
        $client->jsonRequest('DELETE', '/api/v1/admin/tags/1', [], ['HTTP_AUTHORIZATION' => $this->getAdminToken($client)]);

        $this->assertResponseIsSuccessful();
        $this->assertResponseStatusCodeSame(200);

        $tag = static::getContainer()->get(TagRepository::class)->find(1);
        $this->assertNull($tag);
    }
}
