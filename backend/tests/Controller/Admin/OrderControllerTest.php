<?php

namespace App\Tests\Controller\Admin;

use App\Repository\ReplacementMethodRepository;
use App\Tests\Controller\CustomWebTestCase;

/**
 * Заказы
 */
class OrderControllerTest extends CustomWebTestCase
{
    /**
     * @group admin
     * @group order
     * @group replacementAdd
     */
    public function testReplacementAdd()
    {
        $expected = [
            'title' => 'Позвоните мне тест',
            'description' => 'Ваш личный выбор тест',
            'weight' => 10
        ];

        $client = static::createClient();
        $client->jsonRequest("POST", '/api/v1/admin/replacement/add', [
            "title" => $expected['title'],
            'description' => $expected['description'],
            'weight' => $expected['weight']
        ], [self::HEADER_AUTH => $this->getAdminToken($client)]);

        $this->assertResponseIsSuccessful();
        $resp = $this->getResp2Arr($client->getResponse());
        $this->assertArrayHasKey("id", $resp);

        /** @var ReplacementMethodRepository $repo */
        $repo = static::getContainer()->get(ReplacementMethodRepository::class);
        $method = $repo->find($resp['id']);
        $this->assertNotNull($method);

        $this->assertEquals($expected['title'], $method->getTitle());
        $this->assertEquals($expected['description'], $method->getDescription());
        $this->assertEquals($expected['weight'], $method->getWeight());
    }

    /**
     * @group admin
     * @group order
     * @group replacementUpdate
     */
    public function testReplacementUpdate()
    {
        $client = static::createClient();

        $id = 1;

        $expected = [
            'title' => 'Позвоните мне тест',
            'description' => 'Ваш личный выбор тест',
            'weight' => 10
        ];


        $client->jsonRequest("PUT", '/api/v1/admin/replacement/' . $id, [
            "title" => $expected['title'],
            'description' => $expected['description'],
            'weight' => $expected['weight']
        ], [self::HEADER_AUTH => $this->getAdminToken($client)]);

        $this->assertResponseIsSuccessful();
        $resp = $this->getResp2Arr($client->getResponse());
        $this->assertArrayHasKey("id", $resp);

        /** @var ReplacementMethodRepository $repo */
        $repo = static::getContainer()->get(ReplacementMethodRepository::class);
        $method = $repo->find($resp['id']);
        $this->assertNotNull($method);
        $this->assertEquals($expected['title'], $method->getTitle());
        $this->assertEquals($expected['description'], $method->getDescription());
        $this->assertEquals($expected['weight'], $method->getWeight());
    }
}