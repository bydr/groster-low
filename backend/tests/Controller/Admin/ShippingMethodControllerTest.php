<?php

namespace App\Tests\Controller\Admin;

use App\Entity\ShippingLimit;
use App\Entity\ShippingMethod;
use App\Helpers\Params;
use App\Repository\ShippingMethodRepository;
use App\Tests\Controller\CustomWebTestCase;

/**
 * Тесты способов доставки
 */
class ShippingMethodControllerTest extends CustomWebTestCase
{
    /**
     * @group admin
     * @group shippingMethods
     */
    public function testShippingMethods()
    {
        $client = static::createClient();
        $token = $this->getAdminToken($client);

        $expectedFields = [Params::NAME, Params::WEIGHT, Params::ALIAS, Params::IS_ACTIVE, Params::COST];

        $client->jsonRequest("GET", "/api/v1/admin/shipping-methods", [], [self::HEADER_AUTH => $token]);
        $this->assertResponseIsSuccessful();

        $resp = $this->getResp2Arr($client->getResponse());
        $this->assertCount(5, $resp, "Fail to get methods count");
        $this->matchObjectVars($expectedFields, (object)$resp[0]);
    }

    /**
     * @group admin
     * @group shippingMethods
     */
    public function testShippingMethodAdd()
    {
        $client = static::createClient();
        $token = $this->getAdminToken($client);

        // Добавление
        $expected = [
            Params::NAME => 'новый способ',
            Params::DESCRIPTION => 'новый способ доставки',
            Params::WEIGHT => 5,
            Params::COST => 'от 320',
            Params::USE_ADDRESS => false,
            Params::IS_FAST => false,
            Params::ALIAS => 'new method'
        ];

        $client->jsonRequest("POST", "/api/v1/admin/shipping-method/add", $expected, [self::HEADER_AUTH => $token]);
        $this->assertResponseIsSuccessful();

        $resp = $this->getResp2Arr($client->getResponse());
        $this->assertArrayHasKey(Params::ALIAS, $resp);

        $alias = $resp[Params::ALIAS];

        /** @var ShippingMethod $method */
        $method = static::getContainer()->get(ShippingMethodRepository::class)->findOneByAlias($alias);
        $this->assertNotNull($method);

        $this->assertEquals($expected[Params::NAME], $method->getName());
        $this->assertEquals($expected[Params::DESCRIPTION], $method->getDescription());
        $this->assertEquals($expected[Params::WEIGHT], $method->getWeight());
        $this->assertEquals($expected[Params::COST], $method->getCost());
        $this->assertEquals($expected[Params::USE_ADDRESS], $method->getUseAddress());
        $this->assertEquals($expected[Params::IS_FAST], $method->getIsFast());

        // Получение данных метода
        $client->jsonRequest("GET", "/api/v1/admin/shipping-method/{$alias}", [], [self::HEADER_AUTH => $token]);
        $this->assertResponseIsSuccessful();

        $resp = $this->getResp2Arr($client->getResponse(), false);
        $this->matchObjectVars(array_merge(array_keys($expected), [Params::ALIAS]), $resp);

        // Обновление
        $expected[Params::NAME] = 'обновленный новый способ';
        $client->jsonRequest("PUT", "/api/v1/admin/shipping-method/{$alias}", $expected, [self::HEADER_AUTH => $token]);
        $this->assertResponseIsSuccessful();

        $resp = $this->getResp2Arr($client->getResponse());
        $this->assertArrayHasKey(Params::ALIAS, $resp);

        /** @var ShippingMethod $method */
        $method = static::getContainer()->get(ShippingMethodRepository::class)->findOneByAlias($alias);
        $this->assertNotNull($method);

        $this->assertEquals($expected[Params::NAME], $method->getName());
        $this->assertEquals($expected[Params::DESCRIPTION], $method->getDescription());
        $this->assertEquals($expected[Params::WEIGHT], $method->getWeight());
        $this->assertEquals($expected[Params::COST], $method->getCost());
        $this->assertEquals($expected[Params::USE_ADDRESS], $method->getUseAddress());
        $this->assertEquals($expected[Params::IS_FAST], $method->getIsFast());
    }

    /**
     * @group admin
     * @group limits
     */
    public function shippingMethodLimit()
    {
        $client = static::createClient();
        $token = $this->getAdminToken($client);

        /** @var ShippingMethod $method */
        $method = static::getContainer()->get(ShippingMethodRepository::class)->find(1);
        $this->assertNotNull($method);

        // Получение ограничений метода
        $client->jsonRequest("GET", "/api/v1/admin/shipping-method/{$method->getAlias()}/limit", [], [self::HEADER_AUTH => $token]);
        $this->assertResponseIsSuccessful();

        $resp = $this->getResp2Arr($client->getResponse());
        $this->assertEquals($method->getLimits()[0]->getShippingCost(), $resp[0][Params::SHIPPING_COST]);

        // Добавление ограничения для метода
        $expected = [
            Params::TYPE => ShippingLimit::TYPE_COST,
            Params::ORDER_MIN_COST => 15000,
            Params::ORDER_MAX_COST => 25000,
            Params::SHIPPING_COST => 1000,
        ];

        $client->jsonRequest("POST", "/api/v1/admin/shipping-method/{$method->getAlias()}/limit", $expected, [self::HEADER_AUTH => $token]);
        $this->assertResponseIsSuccessful();

        $resp = $this->getResp2Arr($client->getResponse());
        $this->assertArrayHasKey(Params::ID, $resp);
        $this->assertCount(2, $method->getLimits());

        $limitId = $resp[Params::ID];

        // Изменение ограничения для метода
        $expected[Params::ORDER_MIN_COST] = 16000;

        $client->jsonRequest("PUT", "/api/v1/admin/shipping-method/{$method->getAlias()}/limit/{$limitId}", $expected, [self::HEADER_AUTH => $token]);
        $this->assertResponseIsSuccessful();

        $limit = null;
        foreach ($method->getLimits() as $item) {
            if($item->getId() == $limitId) {
                $limit = $item;
            }
        }

        $this->assertEquals($expected[Params::ORDER_MIN_COST], $limit->getOrderMinCost());

        // Удаление ограничения для метода
        $client->jsonRequest("DELETE", "/api/v1/admin/shipping-method/{$method->getAlias()}/limit/{$limitId}", [], [self::HEADER_AUTH => $token]);
        $this->assertResponseIsSuccessful();

        $this->assertCount(1, $method->getLimits());
    }
}