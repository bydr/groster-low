<?php

namespace App\Tests\Controller\Front;

use App\ApiError;
use App\Entity\Discount;
use App\Entity\Order;
use App\Helpers\Params;
use App\Repository\DiscountRepository;
use App\Repository\OrderRepository;
use App\Repository\ProductRepository;
use App\Tests\Controller\CustomWebTestCase;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;

/**
 * Тесты для CheckoutController
 */
class PromocodeControllerTest extends CustomWebTestCase
{
    /**
     * @group front
     * @group promocode
     */
    public function testValidRateUseAndRemove(): void
    {
        $client = static::createClient();
        $token = $this->getUserToken($client);
        $cart = $this->createCart($client, 1);
        /** @var Discount $rateDiscount */
        $rateDiscount = static::getContainer()->get(DiscountRepository::class)->find(1);
        $this->assertNotNull($rateDiscount, "Error to get rateDiscount");
        $this->assertEquals(Discount::TYPE_RATE, $rateDiscount->getType());
        $this->assertTrue($cart->getItemsTotal() > 0);

        // Добавление промокода
        $client->jsonRequest("PUT", "/api/v1/promocode", ["cart" => $cart->getUid(), "code" => $rateDiscount->getCode()], [self::HEADER_AUTH => $token]);
        $this->assertResponseIsSuccessful();

        $resp = $this->getResp2Arr($client->getResponse());
        $this->assertArrayHasKey("discount", $resp, "Error to return discount field");
        $this->assertArrayHasKey("total_cost", $resp, "Error to return total cost field");

        $this->assertGreaterThan(0, $resp['discount'], 'Error to get discount');
        $this->assertEquals($cart->getItemsTotal() * $rateDiscount->getValue() / 100, $resp['discount']);
        $this->assertEquals($cart->getItemsTotal() - ($cart->getItemsTotal() * $rateDiscount->getValue() / 100), $resp['total_cost']);

        // Удаление промокода
        $client->jsonRequest("DELETE", "/api/v1/promocode", ["cart" => $cart->getUid()], [self::HEADER_AUTH => $token]);
        $this->assertResponseIsSuccessful();

        $resp = $this->getResp2Arr($client->getResponse());
        $this->assertArrayNotHasKey("discount", $resp, "Error to not return discount field");
        $this->assertArrayHasKey("total_cost", $resp, "Error to return total cost field");

        $this->assertEquals($cart->getItemsTotal(), $resp['total_cost']);
    }

    /**
     * @group front
     * @group promocode
     */
    public function testValidMoneyUseAndRemove(): void
    {
        $client = static::createClient();
        $token = $this->getUserToken($client);
        $cart = $this->createCart($client, 1);
        /** @var Discount $moneyDiscount */
        $moneyDiscount = static::getContainer()->get(DiscountRepository::class)->find(2);
        $this->assertNotNull($moneyDiscount, "Error to get moneyDiscount");
        $this->assertEquals(Discount::TYPE_MONEY, $moneyDiscount->getType());

        // Добавление промокода
        $client->jsonRequest("PUT", "/api/v1/promocode", ["cart" => $cart->getUid(), "code" => $moneyDiscount->getCode()], [self::HEADER_AUTH => $token]);
        $this->assertResponseIsSuccessful();

        $resp = $this->getResp2Arr($client->getResponse());

        $this->assertArrayHasKey("discount", $resp, "Error to return discount field");
        $this->assertArrayHasKey("total_cost", $resp, "Error to return total cost field");

        $this->assertEquals($moneyDiscount->getValue(), $resp['discount']);
        $this->assertEquals($cart->getItemsTotal() - $moneyDiscount->getValue(), $resp['total_cost']);

        // Удаление промокода
        $client->jsonRequest("DELETE", "/api/v1/promocode", ["cart" => $cart->getUid()], [self::HEADER_AUTH => $token]);
        $this->assertResponseIsSuccessful();

        $resp = $this->getResp2Arr($client->getResponse());
        $this->assertArrayNotHasKey("discount", $resp, "Error to not return discount field");
        $this->assertArrayHasKey("total_cost", $resp, "Error to return total cost field");

        $this->assertEquals($cart->getItemsTotal(), $resp['total_cost']);
    }

    /**
     * @group front
     * @group promocode
     */
    public function testValidNotMatchCategories(): void
    {
        $client = static::createClient();
        $token = $this->getUserToken($client);
        $cart = $this->createCart($client, 2);
        /** @var Discount $rateDiscount */
        $rateDiscount = static::getContainer()->get(DiscountRepository::class)->find(2);
        $this->assertNotNull($rateDiscount, "Error to get moneyDiscount");
        $this->assertEquals(Discount::TYPE_MONEY, $rateDiscount->getType());

        // Добавление промокода
        $client->jsonRequest("PUT", "/api/v1/promocode", ["cart" => $cart->getUid(), "code" => $rateDiscount->getCode()], [self::HEADER_AUTH => $token]);
        $this->assertResponseIsSuccessful();

        $resp = $this->getResp2Arr($client->getResponse());
        $this->assertArrayHasKey("discount", $resp, "Error to return discount field");
        $this->assertArrayHasKey("total_cost", $resp, "Error to return total cost field");

        $this->assertEquals(0, $resp['discount']);
        $this->assertEquals($cart->getItemsTotal(), $resp['total_cost']);

        // Удаление промокода
        $client->jsonRequest("DELETE", "/api/v1/promocode", ["cart" => $cart->getUid()], [self::HEADER_AUTH => $token]);
        $this->assertResponseIsSuccessful();

        $resp = $this->getResp2Arr($client->getResponse());
        $this->assertArrayNotHasKey("discount", $resp, "Error to not return discount field");
        $this->assertArrayHasKey("total_cost", $resp, "Error to return total cost field");

        $this->assertEquals($cart->getItemsTotal(), $resp['total_cost']);
    }

    /**
     * @group front
     * @group promocode
     */
    public function testInvalidTwoDiscount(): void
    {
        $client = static::createClient();
        $token = $this->getUserToken($client);
        $cart = $this->createCart($client, 1);
        /** @var Discount $rateDiscount */
        $rateDiscount = static::getContainer()->get(DiscountRepository::class)->find(2);
        $this->assertNotNull($rateDiscount, "Error to get moneyDiscount");
        $this->assertEquals(Discount::TYPE_MONEY, $rateDiscount->getType());

        // Добавление промокода
        $client->jsonRequest("PUT", "/api/v1/promocode", ["cart" => $cart->getUid(), "code" => $rateDiscount->getCode()], [self::HEADER_AUTH => $token]);
        $this->assertResponseIsSuccessful();

        $resp = $this->getResp2Arr($client->getResponse());
        $this->assertArrayHasKey("discount", $resp, "Error to return discount field");
        $this->assertArrayHasKey("total_cost", $resp, "Error to return total cost field");

        $this->assertEquals($rateDiscount->getValue(), $resp['discount']);
        $this->assertEquals($cart->getItemsTotal() - $rateDiscount->getValue(), $resp['total_cost']);

        // Добавление еще одного промокода
        $client->jsonRequest("PUT", "/api/v1/promocode", ["cart" => $cart->getUid(), "code" => $rateDiscount->getCode()], [self::HEADER_AUTH => $token]);
        $this->assertResponseStatusCodeSame(400);
        $resp = $this->getResp2Arr($client->getResponse());

        $this->assertArrayHasKey(Params::CODE, $resp, 'Not returned code');
        $this->assertEquals(ApiError::ERROR_DUPLICATE_PROMOCODE, $resp[Params::CODE]);

        // Удаление промокода
        $client->jsonRequest("DELETE", "/api/v1/promocode", ["cart" => $cart->getUid()], [self::HEADER_AUTH => $token]);
        $this->assertResponseIsSuccessful();

        $resp = $this->getResp2Arr($client->getResponse());
        $this->assertArrayNotHasKey("discount", $resp, "Error to not return discount field");
        $this->assertArrayHasKey("total_cost", $resp, "Error to return total cost field");

        $this->assertEquals($cart->getItemsTotal(), $resp['total_cost']);
    }

    /**
     * @group front
     * @group promocode
     */
    public function testInvalidUnauthorized(): void
    {
        $client = static::createClient();
        $cart = $this->createCart($client, 1);
        /** @var Discount $rateDiscount */
        $rateDiscount = static::getContainer()->get(DiscountRepository::class)->find(2);
        $this->assertNotNull($rateDiscount, "Error to get moneyDiscount");
        $this->assertEquals(Discount::TYPE_MONEY, $rateDiscount->getType());

        // Добавление промокода
        $client->jsonRequest("PUT", "/api/v1/promocode", ["cart" => $cart->getUid(), "code" => $rateDiscount->getCode()]);
        $this->assertResponseStatusCodeSame(401);
        $resp = $this->getResp2Arr($client->getResponse());

        $this->assertArrayHasKey(Params::CODE, $resp, 'Not returned code');
        $this->assertEquals(ApiError::ERROR_UNAUTHORIZATION, $resp[Params::CODE]);
    }

    /**
     * @group front
     * @group promocode
     */
    public function testInvalidFailCart(): void
    {
        $client = static::createClient();
        $token = $this->getUserToken($client);
        /** @var Discount $rateDiscount */
        $rateDiscount = static::getContainer()->get(DiscountRepository::class)->find(2);
        $this->assertNotNull($rateDiscount, "Error to get moneyDiscount");
        $this->assertEquals(Discount::TYPE_MONEY, $rateDiscount->getType());

        // Добавление промокода
        $client->jsonRequest("PUT", "/api/v1/promocode", ["cart" => "fail cart", "code" => $rateDiscount->getCode()], [self::HEADER_AUTH => $token]);
        $this->assertResponseStatusCodeSame(400);
        $resp = $this->getResp2Arr($client->getResponse());

        $this->assertArrayHasKey(Params::CODE, $resp, 'Not returned code');
        $this->assertEquals(ApiError::ERROR_INVALID_CART, $resp[Params::CODE]);
    }

    /**
     * @group front
     * @group promocode
     */
    public function testInvalidFailPromo(): void
    {
        $client = static::createClient();
        $token = $this->getUserToken($client);
        $cart = $this->createCart($client, 1);

        // Добавление промокода
        $client->jsonRequest("PUT", "/api/v1/promocode", ["cart" => $cart->getUid(), "code" => "invalid code"], [self::HEADER_AUTH => $token]);
        $this->assertResponseStatusCodeSame(400);
        $resp = $this->getResp2Arr($client->getResponse());

        $this->assertArrayHasKey(Params::CODE, $resp, 'Not returned code');
        $this->assertEquals(ApiError::ERROR_INVALID_PROMOCODE, $resp[Params::CODE]);
    }

    /**
     * @group front
     * @group promocode
     */
    public function testInvalidUseNotReusablePromo(): void
    {
        $client = static::createClient();
        $token = $this->getUserToken($client);
        $cart = $this->createCart($client, 1);
        /** @var Discount $rateDiscount */
        $rateDiscount = static::getContainer()->get(DiscountRepository::class)->find(3);
        $this->assertNotNull($rateDiscount, "Error to get rateDiscount");
        $this->assertEquals(Discount::TYPE_RATE, $rateDiscount->getType());

        // Добавление промокода
        $client->jsonRequest("PUT", "/api/v1/promocode", ["cart" => $cart->getUid(), "code" => $rateDiscount->getCode()], [self::HEADER_AUTH => $token]);
        $this->assertResponseIsSuccessful();

        $resp = $this->getResp2Arr($client->getResponse());
        $this->assertArrayHasKey("discount", $resp, "Error to return discount field");
        $this->assertArrayHasKey("total_cost", $resp, "Error to return total cost field");

        $this->assertEquals($cart->getItemsTotal() * $rateDiscount->getValue() / 100, $resp['discount']);
        $this->assertEquals($cart->getItemsTotal() - ($cart->getItemsTotal() * $rateDiscount->getValue() / 100), $resp['total_cost']);

        $cart2 = $this->createCart($client, 1);
        // Повторное использование одноразового промокода
        $client->jsonRequest("PUT", "/api/v1/promocode", ["cart" => $cart2->getUid(), "code" => $rateDiscount->getCode()], [self::HEADER_AUTH => $token]);
        $this->assertResponseStatusCodeSame(400);
        $resp = $this->getResp2Arr($client->getResponse());

        $this->assertArrayHasKey(Params::CODE, $resp, 'Not returned code');
        $this->assertEquals(ApiError::ERROR_INVALID_PROMOCODE, $resp[Params::CODE]);
    }

    /**
     * @group front
     * @group promocode
     */
    public function testInvalidExpiredPromo(): void
    {
        $client = static::createClient();
        $token = $this->getUserToken($client);
        $cart = $this->createCart($client, 1);
        /** @var Discount $rateDiscount */
        $rateDiscount = static::getContainer()->get(DiscountRepository::class)->find(4);
        $this->assertNotNull($rateDiscount, "Error to get rateDiscount");
        $this->assertEquals(Discount::TYPE_RATE, $rateDiscount->getType());

        // Добавление промокода
        $client->jsonRequest("PUT", "/api/v1/promocode", ["cart" => $cart->getUid(), "code" => $rateDiscount->getCode()], [self::HEADER_AUTH => $token]);
        $this->assertResponseStatusCodeSame(400);
        $resp = $this->getResp2Arr($client->getResponse());

        $this->assertArrayHasKey(Params::CODE, $resp, 'Not returned code');
        $this->assertEquals(ApiError::ERROR_INVALID_PROMOCODE, $resp[Params::CODE]);
    }

    /**
     * Создание корзины для тестов
     * @param KernelBrowser $client
     * @param int $productId
     * @return Order
     */
    private function createCart(KernelBrowser $client, int $productId): Order
    {
        $token = $this->getUserToken($client);

        $testProduct = static::getContainer()->get(ProductRepository::class)->find($productId);

        $data = [
            'product' => $testProduct->getUuid(),
            'quantity' => 1
        ];

        $client->jsonRequest('POST', '/api/v1/cart/change-qty', $data, [self::HEADER_AUTH => $token]);

        $this->assertResponseIsSuccessful();

        $response = $client->getResponse();
        $content = $response->getContent();
        $respCart = json_decode($content, true);

        return static::getContainer()->get(OrderRepository::class)->findOneByUid($respCart[Params::CART]);
    }
}