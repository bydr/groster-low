<?php

namespace App\Tests\Controller\Front;

use App\Repository\BannerRepository;
use App\Tests\Controller\CustomWebTestCase;

class BannerControllerTest extends CustomWebTestCase
{
    /**
     * @group front
     * @group banner
     */
    public function testAll()
    {
        $client = static::createClient();

        $banners = static::getContainer()->get(BannerRepository::class)->findAll();

        $client->jsonRequest("GET", "/api/v1/pictures");
        $this->assertResponseIsSuccessful();

        $resp = $this->getResp2Arr($client->getResponse());
        $this->assertCount(count($banners), $resp, "Error to equal count banners");
    }
}