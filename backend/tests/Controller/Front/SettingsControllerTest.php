<?php

namespace App\Tests\Controller\Front;

use App\Helpers\Params;
use App\Tests\Controller\CustomWebTestCase;

/**
 * Личный кабинет
 */
class SettingsControllerTest extends CustomWebTestCase
{
    /**
     * @group front
     * @group settings
     * @group getSettings
     */
    public function testGetSettings()
    {
        $client = static::createClient();

        $expected = [
            Params::MIN_MANY_QUANTITY => 100,
            Params::HOLIDAYS => [],
            Params::DELIVERY_SHIFT => 1,
            Params::DELIVERY_FAST_TIME => '11:00',
            Params::VIBER => null,
            Params::WHATSAPP => null,
            Params::TELEGRAM => null,
        ];

        // Получение настроек
        $client->jsonRequest("GET", "/api/v1/settings");
        $this->assertResponseIsSuccessful();

        $resp = $this->getResp2Arr($client->getResponse());
        $this->matchObjectVars(array_keys($expected), (object)$resp);
        $this->assertEquals($expected[Params::MIN_MANY_QUANTITY], $resp[Params::MIN_MANY_QUANTITY]);
        $this->assertEquals($expected[Params::HOLIDAYS], $resp[Params::HOLIDAYS]);
        $this->assertEquals($expected[Params::DELIVERY_SHIFT], $resp[Params::DELIVERY_SHIFT]);
        $this->assertEquals($expected[Params::DELIVERY_FAST_TIME], $resp[Params::DELIVERY_FAST_TIME]);
    }
}