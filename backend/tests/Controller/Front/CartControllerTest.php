<?php

namespace App\Tests\Controller\Front;

use App\ApiError;
use App\Controller\ApiController;
use App\Entity\Order;
use App\Entity\OrderItem;
use App\Entity\Product;
use App\Helpers\Params;
use App\Helpers\ValidateHelper;
use App\Repository\OrderItemRepository;
use App\Repository\OrderRepository;
use App\Repository\ProductRepository;
use App\Tests\Controller\CustomWebTestCase;

/**
 * Class OrderControllerTest
 */
class CartControllerTest extends CustomWebTestCase
{
    const FAKE_UID = 'fakeUid';
    const TEST_PRODUCT = 3;
    const TEST_KIT = 11;
    const TEST_KIT_CHILD = 16;
    const PRODUCT_ZERO_QTY = 12;

    /** @var Product */
    private $testProduct;

    /**
     * @group front
     * @group order
     * @group addProduct
     */
    public function testAddProductAtNewCart()
    {
        $client = static::createClient();
        $this->testProduct = static::getContainer()->get(ProductRepository::class)->find(self::TEST_PRODUCT);

        $data = [
            Params::PRODUCT => $this->testProduct->getUuid(),
            Params::QUANTITY => 1
        ];

        $client->jsonRequest('POST', '/api/v1/cart/change-qty', $data);

        $this->assertResponseIsSuccessful();

        $respCart = $this->getResp2Arr($client->getResponse());

        $this->assertArrayHasKey(Params::CART, $respCart, 'Not returned uid');
        $this->assertNotEmpty($respCart[Params::CART], 'Empty uid');
        $this->assertArrayHasKey(Params::ADDED_QTY, $respCart, 'Not returned added quantity');
        $this->assertEquals(1, $respCart[Params::ADDED_QTY]);
        $this->assertArrayHasKey(Params::DISCOUNT, $respCart);
        $this->assertArrayHasKey(Params::TOTAL_COST, $respCart);
    }

    /**
     * @group front
     * @group order
     * @group addProduct
     */
    public function testAddProductToExistCart()
    {
        $client = static::createClient();
        $this->testProduct = static::getContainer()->get(ProductRepository::class)->find(self::TEST_PRODUCT);

        $data = [
            Params::PRODUCT => $this->testProduct->getUuid(),
            Params::QUANTITY => 1
        ];

        // Создаем корзину
        $client->jsonRequest('POST', '/api/v1/cart/change-qty', $data);

        $this->assertResponseIsSuccessful();

        $respCart = $this->getResp2Arr($client->getResponse());

        $this->assertArrayHasKey(Params::CART, $respCart, 'Not returned uid');
        $this->assertNotEmpty($respCart[Params::CART], 'Empty uid');
        $this->assertArrayHasKey(Params::ADDED_QTY, $respCart, 'Not returned uid');
        $this->assertEquals(1, $respCart[Params::ADDED_QTY]);
        $this->assertArrayHasKey(Params::DISCOUNT, $respCart);
        $this->assertArrayHasKey(Params::TOTAL_COST, $respCart);

        // Добавляем еще 1 товар в корзину
        $data[Params::CART] = $respCart[Params::CART];
        $product2 = static::getContainer()->get(ProductRepository::class)->find(4);
        $data[Params::PRODUCT] = $product2->getUuid();
        $data[Params::QUANTITY] = 10;
        $client->jsonRequest('POST', '/api/v1/cart/change-qty', $data);
        $this->assertArrayHasKey(Params::DISCOUNT, $respCart);
        $this->assertArrayHasKey(Params::TOTAL_COST, $respCart);

        $this->assertResponseIsSuccessful();

        $respCart2 = $this->getResp2Arr($client->getResponse());

        $this->assertArrayHasKey(Params::CART, $respCart2, 'Not returned uid');
        $this->assertNotEmpty($respCart2[Params::CART], 'Empty uid');
        $this->assertArrayHasKey(Params::ADDED_QTY, $respCart2, 'Not returned uid');
        $this->assertEquals(10, $respCart2[Params::ADDED_QTY]);

        // Изменяем количество у первого товара в корзине
        $data[Params::PRODUCT] = $this->testProduct->getUuid();
        $data[Params::QUANTITY] = 7;
        $client->jsonRequest('POST', '/api/v1/cart/change-qty', $data);

        $this->assertResponseIsSuccessful();

        /** @var Order $cart */
        $cart = static::getContainer()->get(OrderRepository::class)->findOneByUid($respCart[Params::CART]);
        foreach ($cart->getItems() as $item) {
            if($item->getProduct() === $this->testProduct) {
                $this->assertEquals(7, $item->getQuantity());
            }elseif($item->getProduct() == $product2) {
                $this->assertEquals(10, $item->getQuantity());
            }
        }
    }

    /**
     * @group front
     * @group order
     * @group addKitProduct
     */
    public function testAddKitToCart()
    {
        $client = static::createClient();
        $kit = static::getContainer()->get(ProductRepository::class)->find(self::TEST_KIT);

        $data = [
            Params::PRODUCT => $kit->getUuid(),
            Params::QUANTITY => 1
        ];

        // Создаем новую корзину и добавляем комплект
        $client->jsonRequest('POST', '/api/v1/cart/change-qty', $data);

        $this->assertResponseIsSuccessful();

        $resp1 = $this->getResp2Arr($client->getResponse());

        $this->assertArrayHasKey(Params::CART, $resp1, 'Not returned uid');
        $this->assertNotEmpty($resp1[Params::CART], 'Empty uid');
        $this->assertArrayHasKey(Params::ADDED_QTY, $resp1, 'Not returned uid');
        $this->assertEquals(1, $resp1[Params::ADDED_QTY]);

        // Добавляем еще 1 товар из комплекта в корзину
        $data[Params::CART] = $resp1[Params::CART];
        /** @var Product $child */
        $child = static::getContainer()->get(ProductRepository::class)->find(self::TEST_KIT_CHILD);
        $data[Params::PRODUCT] = $child->getUuid();
        $data[Params::QUANTITY] = 10;
        $client->jsonRequest('POST', '/api/v1/cart/change-qty', $data);

        $this->assertResponseIsSuccessful();

        $resp2 = $this->getResp2Arr($client->getResponse());

        $this->assertArrayHasKey(Params::CART, $resp2, 'Not returned uid');
        $this->assertNotEmpty($resp2[Params::CART], 'Empty uid');
        $this->assertArrayHasKey(Params::ADDED_QTY, $resp2, 'Not returned uid');
        $this->assertEquals(10, $resp2[Params::ADDED_QTY]);

        // Добавляем комплекта больше, чем доступно
        $data[Params::PRODUCT] = $kit->getUuid();
        $data[Params::QUANTITY] = $child->getTotalQty();
        $client->jsonRequest('POST', '/api/v1/cart/change-qty', $data);

        $this->assertResponseIsSuccessful();
        $resp3 = $this->getResp2Arr($client->getResponse());

        $this->assertArrayHasKey(Params::CART, $resp3, 'Not returned uid');
        $this->assertNotEmpty($resp2[Params::CART], 'Empty uid');
        $this->assertArrayHasKey(Params::ADDED_QTY, $resp3, 'Not returned uid');
        $this->assertEquals($data[Params::QUANTITY] - 10, $resp3[Params::ADDED_QTY]);
    }

    /**
     * @group front
     * @group order
     * @group addProduct
     */
    public function testAddProductToExistCartWithUser()
    {
        $client = static::createClient();
        $this->testProduct = static::getContainer()->get(ProductRepository::class)->find(self::TEST_PRODUCT);

        $testEmail = 'qwe1@test.com';
        $cartRepo = static::getContainer()->get(OrderRepository::class);

        $client->jsonRequest('POST', '/api/v1/auth/login_email', ["email" => $testEmail, "password" => "password1"]);
        $this->assertResponseIsSuccessful();

        $responseData = $this->getResp2Arr($client->getResponse());

        $data = [
            Params::PRODUCT => $this->testProduct->getUuid(),
            Params::QUANTITY => 5
        ];

        $client->jsonRequest('POST', '/api/v1/cart/change-qty', $data, ['HTTP_AUTHORIZATION' => $responseData[Params::ACCESS_TOKEN]]);

        $this->assertResponseIsSuccessful();

        $respCart = $this->getResp2Arr($client->getResponse());

        $this->assertArrayHasKey(Params::CART, $respCart, 'Not returned uid');
        $this->assertNotEmpty($respCart[Params::CART], 'Empty uid');
        $this->assertArrayHasKey(Params::ADDED_QTY, $respCart, 'Not returned uid');
        $this->assertEquals($data[Params::QUANTITY], $respCart[Params::ADDED_QTY]);

        /** @var Order $cart */
        $cart = $cartRepo->findOneByUid($respCart[Params::CART]);
        $this->assertNotNull($cart);
        $this->assertEquals($testEmail, $cart->getCustomer()->getEmail());
        $this->assertEquals($data[Params::QUANTITY], $cart->getItems()[0]->getQuantity());

        $data2[Params::PRODUCT] = $data[Params::PRODUCT];
        $data2[Params::CART] = $cart->getUid();
        $data2[Params::QUANTITY] = 10;
        $client->jsonRequest('POST', '/api/v1/cart/change-qty', $data2, ['HTTP_AUTHORIZATION' => $responseData[Params::ACCESS_TOKEN]]);

        $this->assertResponseIsSuccessful();

        $respCart2 = $this->getResp2Arr($client->getResponse());

        $this->assertArrayHasKey(Params::CART, $respCart2, 'Not returned uid');
        $this->assertNotEmpty($respCart2[Params::CART], 'Empty uid');
        $this->assertArrayHasKey(Params::ADDED_QTY, $respCart2, 'Not returned uid');
        $this->assertEquals($data2[Params::QUANTITY], $respCart2[Params::ADDED_QTY]);
    }

    /**
     * @group front
     * @group order
     * @group addProduct
     */
    public function testAddProductToExistCartWithWrongUser()
    {
        $client = static::createClient();
        $this->testProduct = static::getContainer()->get(ProductRepository::class)->find(self::TEST_PRODUCT);

        $testEmail = 'qwe1@test.com';
        $cartRepo = static::getContainer()->get(OrderRepository::class);

        $client->jsonRequest('POST', '/api/v1/auth/login_email', ["email" => $testEmail, "password" => "password1"]);
        $this->assertResponseIsSuccessful();

        $responseData = $this->getResp2Arr($client->getResponse());

        $data = [
            Params::PRODUCT => $this->testProduct->getUuid(),
            Params::QUANTITY => 1
        ];

        $client->jsonRequest('POST', '/api/v1/cart/change-qty', $data, ['HTTP_AUTHORIZATION']);

        $this->assertResponseIsSuccessful();

        $respCart = $this->getResp2Arr($client->getResponse());

        $this->assertArrayHasKey(Params::CART, $respCart, 'Not returned uid');
        $this->assertNotEmpty($respCart[Params::CART], 'Empty uid');
        $this->assertArrayHasKey(Params::ADDED_QTY, $respCart, 'Not returned uid');
        $this->assertEquals(1, $respCart[Params::ADDED_QTY]);

        /** @var Order $cart */
        $cart = $cartRepo->findOneByUid($respCart[Params::CART]);
        $this->assertNotNull($cart);
        $this->assertNull($cart->getCustomer());
        $this->assertEquals(1, $cart->getItems()[0]->getQuantity());

        $data[Params::CART] = $respCart[Params::CART];
        $data[Params::QUANTITY] = 10;
        $client->jsonRequest('POST', '/api/v1/cart/change-qty', $data, ['HTTP_AUTHORIZATION' => $responseData[Params::ACCESS_TOKEN]]);

        $this->assertResponseStatusCodeSame(400);
        $response = $client->getResponse();
        $this->assertTrue($response->headers->contains('Content-Type', 'application/json'), 'Invalid header Content-Type');
        $respCart = $this->getResp2Arr($client->getResponse());
        $this->assertArrayHasKey(Params::CODE, $respCart, 'Not returned code');
        $this->assertEquals(ApiError::ERROR_CART, $respCart[Params::CODE]);
    }

    /**
     * @group front
     * @group order
     * @group addProduct
     */
    public function testNegativeQuantity()
    {
        $client = static::createClient();
        $this->testProduct = static::getContainer()->get(ProductRepository::class)->find(self::TEST_PRODUCT);

        $data = [
            Params::PRODUCT => $this->testProduct->getUuid(),
            Params::QUANTITY => -1
        ];

        $client->jsonRequest('POST', '/api/v1/cart/change-qty', $data);

        $this->assertResponseStatusCodeSame(400, "Wrong expected status code");
        $respCart = $this->getResp2Arr($client->getResponse());

        $this->assertArrayHasKey(Params::CODE, $respCart, 'Not returned code');
        $this->assertEquals(ApiError::ERROR_INVALID_DATA, $respCart[Params::CODE]);
    }

    /**
     * @group front
     * @group order
     * @group addProduct
     */
    public function testZeroQuantity()
    {
        $client = static::createClient();
        $this->testProduct = static::getContainer()->get(ProductRepository::class)->find(self::TEST_PRODUCT);

        $data = [
            Params::PRODUCT => $this->testProduct->getUuid(),
            Params::QUANTITY => 0
        ];

        $client->jsonRequest('POST', '/api/v1/cart/change-qty', $data);

        $this->assertResponseStatusCodeSame(400, "Wrong expected status code");
        $respCart = $this->getResp2Arr($client->getResponse());

        $this->assertArrayHasKey(Params::CODE, $respCart, 'Not returned code');
        $this->assertEquals(ApiError::ERROR_INVALID_DATA, $respCart[Params::CODE]);
    }

    /**
     * @group front
     * @group order
     * @group wrongCart
     */
    public function testWrongCartUid()
    {
        $client = static::createClient();
        $this->testProduct = static::getContainer()->get(ProductRepository::class)->find(self::TEST_PRODUCT);

        $data = [
            Params::PRODUCT => $this->testProduct->getUuid(),
            Params::QUANTITY => 1,
            Params::CART => self::FAKE_UID
        ];

        $client->jsonRequest('POST', '/api/v1/cart/change-qty', $data);

        $this->assertResponseStatusCodeSame(400, "Wrong expected status code");
        $respCart = $this->getResp2Arr($client->getResponse());

        $this->assertArrayHasKey(Params::CODE, $respCart, 'Not returned code');
        $this->assertEquals(ApiError::ERROR_CART, $respCart[Params::CODE]);
    }

    /**
     * @group front
     * @group order
     * @group addProduct
     */
    public function testAddQtyMoreThenAvailable()
    {
        $client = static::createClient();
        $this->testProduct = static::getContainer()->get(ProductRepository::class)->find(self::TEST_PRODUCT);

        $data = [
            Params::PRODUCT => $this->testProduct->getUuid(),
            Params::QUANTITY => $this->testProduct->getTotalQty() + 1
        ];

        $client->jsonRequest('POST', '/api/v1/cart/change-qty', $data);

        $this->assertResponseIsSuccessful();

        $response = $client->getResponse();
        $respCart = $this->getResp2Arr($client->getResponse());

        $this->assertArrayHasKey(Params::CART, $respCart, 'Not returned uid');
        $this->assertNotEmpty($respCart[Params::CART], 'Empty uid');
        $this->assertArrayHasKey(Params::ADDED_QTY, $respCart, 'Not returned uid');
        $this->assertEquals($this->testProduct->getTotalQty(), $respCart[Params::ADDED_QTY]);
    }

    /**
     * @group front
     * @group order
     * @group addProduct
     */
    public function testAddProductWithZeroTotal()
    {
        $client = static::createClient();
        $testZeroQtyProduct = static::getContainer()->get(ProductRepository::class)->find(self::PRODUCT_ZERO_QTY);


        $data = [
            Params::PRODUCT => $testZeroQtyProduct->getUuid(),
            Params::QUANTITY => 1
        ];

        $client->jsonRequest('POST', '/api/v1/cart/change-qty', $data);

        $this->assertResponseStatusCodeSame(400, "Wrong status code");

        $response = $client->getResponse();
        $respCart = $this->getResp2Arr($client->getResponse());

        $this->assertArrayHasKey(Params::CODE, $respCart, 'Not returned code');
        $this->assertEquals(ApiError::ERROR_EMPTY_PRODUCT, $respCart[Params::CODE]);
    }

    /**
     * @group front
     * @group order
     * @group bindUser
     */
    public function testBindUser()
    {
        $client = static::createClient();

        /** @var OrderRepository $orderRepository */
        $orderRepository = static::getContainer()->get(OrderRepository::class);
        $testCartUid = $this->getCartUid($client);

        $testPhone = ValidateHelper::getValidPhone('+7(988) 999-99-91');
        $client->jsonRequest('POST', '/api/v1/auth/login_phone', [Params::PHONE => $testPhone, Params::CODE => "1111"]);
        $this->assertResponseIsSuccessful();

        $responseData = $this->getResp2Arr($client->getResponse());

        $client->jsonRequest("PUT", "/api/v1/cart/{$testCartUid}/bind-user", [], ['HTTP_AUTHORIZATION' => $responseData[Params::ACCESS_TOKEN]]);
        $this->assertResponseIsSuccessful();

        $cart = $orderRepository->findOneByUid($testCartUid);
        $this->assertNotNull($cart->getCustomer(), "Error to get cart customer");
        $this->assertEquals($cart->getCustomer()->getPhone(), $testPhone, "Error to match cart customer");
    }

    /**
     * @group front
     * @group order
     * @group bindUser
     */
    public function testBindUserWithoutUser()
    {
        $client = static::createClient();

        /** @var OrderRepository $orderRepository */
        $orderRepository = static::getContainer()->get(OrderRepository::class);
        $testCartUid = $this->getCartUid($client);

        /** @var Order $testCart */
        $testCart = $orderRepository->findOneByUid($testCartUid);

        $client->jsonRequest("PUT", "/api/v1/cart/{$testCart->getUid()}/bind-user");
        $this->assertResponseStatusCodeSame(400);

        $cart = $orderRepository->findOneByUid($testCartUid);
        $this->assertNull($cart->getCustomer(), "Error to get null for customer");
    }

    /**
     * @group front
     * @group order
     * @group removeProduct
     */
    public function testRemoveProduct()
    {
        $client = static::createClient();

        /** @var OrderRepository $orderRepository */
        $orderRepository = static::getContainer()->get(OrderRepository::class);
        $testCartUid = $this->getCartUid($client, [11, 15]);
        $product = static::getContainer()->get(ProductRepository::class)->find(15);

        /** @var Order $testCart */
        $testCart = $orderRepository->findOneByUid($testCartUid);

        $client->jsonRequest('DELETE', '/api/v1/cart/delete-product', [
            Params::PRODUCT => $product->getUuid(),
            Params::CART => $testCart->getUid()
        ]);

        $this->assertResponseIsSuccessful();
        $resp = $this->getResp2Arr($client->getResponse());
        $this->assertArrayHasKey(Params::DISCOUNT, $resp);
        $this->assertArrayHasKey(Params::TOTAL_COST, $resp);
        $this->assertEquals(1228, $resp[Params::TOTAL_COST]);
    }

    /**
     * @group front
     * @group order
     * @group removeProduct
     */
    public function testRemoveKit()
    {
        $client = static::createClient();

        /** @var OrderRepository $orderRepository */
        $orderRepository = static::getContainer()->get(OrderRepository::class);
        $testCartUid = $this->getCartUid($client, [11, 15]);
        $kit = static::getContainer()->get(ProductRepository::class)->find(11);

        /** @var Order $testCart */
        $testCart = $orderRepository->findOneByUid($testCartUid);

        $client->jsonRequest('DELETE', '/api/v1/cart/delete-product', [
            Params::PRODUCT => $kit->getUuid(),
            Params::CART => $testCart->getUid()
        ]);

        $this->assertResponseIsSuccessful();
        $resp = $this->getResp2Arr($client->getResponse());
        $this->assertArrayHasKey(Params::DISCOUNT, $resp);
        $this->assertArrayHasKey(Params::TOTAL_COST, $resp);
        $this->assertEquals(663, $resp[Params::TOTAL_COST]);
    }

    /**
     * @group front
     * @group order
     * @group removeProduct
     */
    public function testRemoveProductFailCart()
    {
        $client = static::createClient();

        /** @var OrderRepository $orderRepository */
        $orderRepository = static::getContainer()->get(OrderRepository::class);

        $testCartUid = $this->getCartUid($client);

        /** @var Order $testCart */
        $testCart = $orderRepository->findOneByUid($testCartUid);
        $client->jsonRequest('DELETE', '/api/v1/cart/delete-product', [
            Params::PRODUCT => $testCart->getItems()[0]->getProduct()->getUuid(),
            Params::CART => self::FAKE_UID
        ]);

        $this->assertResponseStatusCodeSame(400, "Wrong expected status code");
        $respCart = $this->getResp2Arr($client->getResponse());

        $this->assertArrayHasKey(Params::CODE, $respCart, 'Not returned code');
        $this->assertEquals(ApiError::ERROR_INVALID_JSON, $respCart[Params::CODE]);
    }

    /**
     * @group front
     * @group order
     * @group removeProduct
     */
    public function testRemoveFailProduct()
    {
        $client = static::createClient();

        /** @var OrderRepository $orderRepository */
        $orderRepository = static::getContainer()->get(OrderRepository::class);
        $testCartUid = $this->getCartUid($client);

        /** @var Order $testCart */
        $testCart = $orderRepository->findOneByUid($testCartUid);

        $client->jsonRequest('DELETE', '/api/v1/cart/delete-product', [
            Params::PRODUCT => self::FAKE_UID,
            Params::CART => $testCart->getUid()
        ]);

        $this->assertResponseStatusCodeSame(400, "Wrong expected status code");
        $respCart = $this->getResp2Arr($client->getResponse());

        $this->assertArrayHasKey(Params::CODE, $respCart, 'Not returned code');
        $this->assertEquals(ApiError::ERROR_INVALID_JSON, $respCart[Params::CODE]);
    }

    /**
     * @group front
     * @group order
     * @group cart
     */
    public function testGetCartFull()
    {
        $client = static::createClient();

        ApiController::setCurrentUser(null);
        /** @var OrderRepository $orderRepository */
        $orderRepository = static::getContainer()->get(OrderRepository::class);
        $testCartUid = $this->getCartUid($client);

        $client->jsonRequest("GET", "/api/v1/cart/" . $testCartUid);

        $this->assertResponseIsSuccessful();

        $respCart = $this->getResp2Arr($client->getResponse());

        /** @var Order $testCart */
        $testCart = $orderRepository->findOneByUid($testCartUid);

        $this->assertArrayHasKey(Params::SAMPLE, $respCart[Params::PRODUCTS][0], 'Not returned sample');
        $this->assertEquals(0, $respCart[Params::PRODUCTS][0][Params::SAMPLE], 'Not valid has sample');

        $this->assertArrayHasKey(Params::TOTAL_COST, $respCart, 'Not returned total cost');
        $this->assertEquals($testCart->getTotal(), $respCart[Params::TOTAL_COST], 'Not equal total cost');
        $this->assertSameSize($testCart->getItems(), $respCart[Params::PRODUCTS], 'Not equal products count');

    }

    /**
     * @group front
     * @group order
     * @group cart
     */
    public function testCartClear()
    {
        $client = static::createClient();
        $userToken = $this->getUserToken($client, "cart@test.com");

        ApiController::setCurrentUser(null);
        /** @var OrderRepository $orderRepository */
        $orderRepository = static::getContainer()->get(OrderRepository::class);
        $testCart = $orderRepository->find(1);
        $this->assertNotNull($testCart);
        $this->assertCount(5, $testCart->getItems());

        $client->jsonRequest("PUT", "/api/v1/cart/remove/" . $testCart->getUid(), [], [self::HEADER_AUTH => 'Bearer ' . $userToken]);
        $this->assertResponseIsSuccessful();

        $testCart = $orderRepository->find(1);
        $this->assertNotNull($testCart);
        $this->assertCount(0, $testCart->getItems());
    }

    /**
     * @group front
     * @group order
     * @group cart
     */
    public function testCartRemove()
    {
        $client = static::createClient();
        $userToken = $this->getUserToken($client, "cart@test.com");

        ApiController::setCurrentUser(null);
        /** @var OrderRepository $orderRepository */
        $orderRepository = static::getContainer()->get(OrderRepository::class);
        $testCart = $orderRepository->find(1);
        $this->assertNotNull($testCart);
        $this->assertCount(5, $testCart->getItems());

        $client->jsonRequest("DELETE", "/api/v1/cart/remove/" . $testCart->getUid(), [], [self::HEADER_AUTH => 'Bearer ' . $userToken]);
        $this->assertResponseIsSuccessful();

        $this->assertNull($orderRepository->find(1));

        $items = static::getContainer()->get(OrderItemRepository::class)->findBy(["order" => $testCart]);
        $this->assertCount(0, $items);
    }

    /**
     * @group front
     * @group order
     * @group sample
     */
    public function testAddSampleWithProduct()
    {
        $client = static::createClient();
        $this->testProduct = static::getContainer()->get(ProductRepository::class)->find(self::TEST_PRODUCT);

        $data = [
            Params::PRODUCT => $this->testProduct->getUuid(),
            Params::QUANTITY => 1
        ];

        $client->jsonRequest('POST', '/api/v1/cart/change-qty', $data);

        $this->assertResponseIsSuccessful();

        $respCart = $this->getResp2Arr($client->getResponse());

        $this->assertArrayHasKey(Params::CART, $respCart, 'Not returned uid');
        $this->assertNotEmpty($respCart[Params::CART], 'Empty uid');
        $this->assertArrayHasKey(Params::ADDED_QTY, $respCart, 'Not returned uid');
        $this->assertEquals(1, $respCart[Params::ADDED_QTY]);

        /** @var Order $cart */
        $cart = static::getContainer()->get(OrderRepository::class)->findOneByUid($respCart[Params::CART]);
        $this->assertNotEmpty($cart, 'Cart does not exist');

        $client->jsonRequest('POST', '/api/v1/cart/sample', [Params::PRODUCT => $this->testProduct->getUuid(), Params::CART => $respCart[Params::CART], Params::QUANTITY => 1]);
        $this->assertResponseIsSuccessful();
        $res = $this->getResp2Arr($client->getResponse());
        $this->assertEquals(1, $res[Params::QUANTITY]);

    }

    /**
     * @group front
     * @group order
     * @group sample
     */
    public function testAddSampleWithoutProduct()
    {
        $client = static::createClient();
        $this->testProduct = static::getContainer()->get(ProductRepository::class)->find(self::TEST_PRODUCT);

        $data = [
            Params::PRODUCT => $this->testProduct->getUuid(),
            Params::QUANTITY => 1
        ];

        $client->jsonRequest('POST', '/api/v1/cart/change-qty', $data);

        $this->assertResponseIsSuccessful();

        $respCart = $this->getResp2Arr($client->getResponse());

        /** @var Order $cart */
        $cart = static::getContainer()->get(OrderRepository::class)->findOneByUid($respCart[Params::CART]);
        $this->assertNotEmpty($cart, 'Cart does not exist');

        /** @var Product $newProduct */
        $newProduct = static::getContainer()->get(ProductRepository::class)->find(self::TEST_PRODUCT + 1);
        $this->assertNotNull($newProduct, "Does not exist new product");

        $client->jsonRequest('POST', '/api/v1/cart/sample', [
            Params::PRODUCT => $newProduct->getUuid(), Params::CART => $cart->getUid(), Params::QUANTITY => 1
        ]);
        $this->assertResponseIsSuccessful();
        $res = $this->getResp2Arr($client->getResponse());
        /** @var Order $cart */
        $cart = static::getContainer()->get(OrderRepository::class)->findOneByUid($res[Params::CART]);
        $this->assertEquals(1, $res[Params::QUANTITY]);
        $this->assertCount(2, $cart->getItems());

        // Удаление образца
        $client->jsonRequest('POST', '/api/v1/cart/sample', [
            Params::PRODUCT => $newProduct->getUuid(), Params::CART => $cart->getUid(), Params::QUANTITY => 0
        ]);
        $this->assertResponseIsSuccessful();
        $res = $this->getResp2Arr($client->getResponse());
        /** @var Order $cart */
        $cart = static::getContainer()->get(OrderRepository::class)->findOneByUid($res[Params::CART]);
        $this->assertEquals(0, $res[Params::QUANTITY]);
        $this->assertCount(1, $cart->getItems());
    }

    /**
     * @group front
     * @group order
     * @group sample
     */
    public function testAddSampleWithBigQty()
    {
        $client = static::createClient();
        $this->testProduct = static::getContainer()->get(ProductRepository::class)->find(self::TEST_PRODUCT);

        $data = [
            Params::PRODUCT => $this->testProduct->getUuid(),
            Params::QUANTITY => 1
        ];

        $client->jsonRequest('POST', '/api/v1/cart/change-qty', $data);

        $this->assertResponseIsSuccessful();

        $respCart = $this->getResp2Arr($client->getResponse());

        /** @var Order $cart */
        $cart = static::getContainer()->get(OrderRepository::class)->findOneByUid($respCart[Params::CART]);
        $this->assertNotEmpty($cart, 'Cart does not exist');

        $newProduct = static::getContainer()->get(ProductRepository::class)->find(self::TEST_PRODUCT + 1);
        $this->assertNotEmpty($newProduct, "Does not exist new product");

        $client->jsonRequest('POST', '/api/v1/cart/sample', [Params::PRODUCT => $this->testProduct->getUuid(), Params::CART => $respCart[Params::CART], Params::QUANTITY => 999]);
        $this->assertResponseIsSuccessful();
        $res = $this->getResp2Arr($client->getResponse());
        $this->assertEquals(OrderItem::MAX_SAMPLE_QTY, $res[Params::QUANTITY]);
    }

    /**
     * @group front
     * @group order
     * @group sample
     */
    public function testAddSampleWithNegativeQty()
    {
        $client = static::createClient();
        $this->testProduct = static::getContainer()->get(ProductRepository::class)->find(self::TEST_PRODUCT);

        $data = [
            Params::PRODUCT => $this->testProduct->getUuid(),
            Params::QUANTITY => 1
        ];

        $client->jsonRequest('POST', '/api/v1/cart/change-qty', $data);

        $this->assertResponseIsSuccessful();

        $respCart = $this->getResp2Arr($client->getResponse());

        /** @var Order $cart */
        $cart = static::getContainer()->get(OrderRepository::class)->findOneByUid($respCart[Params::CART]);
        $this->assertNotEmpty($cart, 'Cart does not exist');

        $newProduct = static::getContainer()->get(ProductRepository::class)->find(self::TEST_PRODUCT + 1);
        $this->assertNotEmpty($newProduct, "Does not exist new product");

        $client->jsonRequest('POST', '/api/v1/cart/sample', [Params::PRODUCT => $this->testProduct->getUuid(), Params::CART => $respCart[Params::CART], Params::QUANTITY => -1]);
        $this->assertResponseIsSuccessful();
        $res = $this->getResp2Arr($client->getResponse());
        $this->assertEquals(0, $res[Params::QUANTITY]);
    }

    /**
     * @group front
     * @group cart
     * @group freeShipping
     */
    public function testFreeShipping()
    {
        $client = static::createClient();

        // Есть бесплатная доставка
        $client->jsonRequest("GET", "/api/v1/free-shipping?" . Params::REGION . "=г Волгоград", [], []);
        $this->assertResponseIsSuccessful();

        $resp = $this->getResp2Arr($client->getResponse());
        $this->assertEquals(100000, $resp[Params::ORDER_MIN_COST]);

        // Есть бесплатная доставка
        $client->jsonRequest("GET", "/api/v1/free-shipping?" . Params::REGION . '=г Москва', [], []);
        $this->assertResponseStatusCodeSame(ApiController::STATUS_EMPTY);
    }

    /**
     * @group front
     * @group cart
     * @group fastOrder
     */
    public function testFastOrderWithEmail()
    {
        $client = static::createClient();
        $this->testProduct = static::getContainer()->get(ProductRepository::class)->find(self::TEST_PRODUCT);

        $data = [
            Params::PRODUCT => $this->testProduct->getUuid(),
            Params::QUANTITY => 1
        ];

        $client->jsonRequest('POST', '/api/v1/cart/change-qty', $data);
        $this->assertResponseIsSuccessful();

        $respCart = $this->getResp2Arr($client->getResponse());
        $cartUid = $respCart[Params::CART];

        // Невалидный email
        $client->jsonRequest('PUT', "/api/v1/fast-order/{$cartUid}", [Params::CONTACT => 'invalid email']);
        $this->assertResponseStatusCodeSame(ApiController::STATUS_BAD_REQUEST);

        $respCart = $this->getResp2Arr($client->getResponse());
        $this->assertArrayHasKey(Params::CODE, $respCart);
        $this->assertEquals(ApiError::ERROR_INVALID_DATA, $respCart[Params::CODE]);

        // Валидный email
        $client->jsonRequest('PUT', "/api/v1/fast-order/{$cartUid}", [Params::CONTACT => 'test@example.com']);
        $this->assertResponseIsSuccessful();

        /** @var Order $cart */
        $cart = static::getContainer()->get(OrderRepository::class)->findOneByUid($cartUid);
        $this->assertNotEmpty($cart, 'Cart does not exist');

        $this->assertTrue($cart->getIsFast());
    }

    /**
     * @group front
     * @group cart
     * @group fastOrder
     */
    public function testFastOrderWithPhone()
    {
        $client = static::createClient();
        $this->testProduct = static::getContainer()->get(ProductRepository::class)->find(self::TEST_PRODUCT);

        $data = [
            Params::PRODUCT => $this->testProduct->getUuid(),
            Params::QUANTITY => 1
        ];

        $client->jsonRequest('POST', '/api/v1/cart/change-qty', $data);
        $this->assertResponseIsSuccessful();

        $respCart = $this->getResp2Arr($client->getResponse());
        $cartUid = $respCart[Params::CART];

        // Невалидный email
        $client->jsonRequest('PUT', "/api/v1/fast-order/{$cartUid}", [Params::CONTACT => '+380509876541']);
        $this->assertResponseStatusCodeSame(ApiController::STATUS_BAD_REQUEST);

        $respCart = $this->getResp2Arr($client->getResponse());
        $this->assertArrayHasKey(Params::CODE, $respCart);
        $this->assertEquals(ApiError::ERROR_INVALID_DATA, $respCart[Params::CODE]);

        // Валидный email
        $client->jsonRequest('PUT', "/api/v1/fast-order/{$cartUid}", [Params::CONTACT => '+79890000000']);
        $this->assertResponseIsSuccessful();

        /** @var Order $cart */
        $cart = static::getContainer()->get(OrderRepository::class)->findOneByUid($cartUid);
        $this->assertNotEmpty($cart, 'Cart does not exist');

        $this->assertTrue($cart->getIsFast());
    }
}
