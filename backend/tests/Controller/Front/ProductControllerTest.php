<?php

namespace App\Tests\Controller\Front;

use App\ApiError;
use App\Controller\ApiController;
use App\DataFixtures\SecurityFixtures;
use App\Entity\Order;
use App\Entity\Product;
use App\Entity\WaitingProduct;
use App\Helpers\Params;
use App\Repository\ProductRepository;
use App\Repository\WaitingProductRepository;
use App\Tests\Controller\CustomWebTestCase;
use DateTime;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;

/**
 * Class ProductControllerTest
 */
class ProductControllerTest extends CustomWebTestCase
{
    /**
     * @group front
     * @group product
     * @group product-list
     */
    public function testProductListValid(): void
    {
        $expected = [
            Params::UUID => "25c32eac-bdec-11e7-9b53-38607704c280",
            Params::NAME => 'Контейнер 1000 мл прямоугол прозрач Д191*129 (306 шт)+крышка АЛЬЯНС',
            Params::ALIAS => 'konteyner-1000-ml-pryamougol-prozrach-d191-129-306-sht-kryshka-alyans',
            Params::IMAGES => [
                "/images/shop/d211aafc-e151-11ea-6b81-ac1f6b855a52.png",
            ],
            Params::CODE => '00000002692',
            Params::PRICE => 2400,
            Params::TOTAL_QTY => 1412,
            Params::FAST_QTY => 884,
            Params::UNIT => "шт",
            Params::CONTAINERS => (object) [
                Params::ONLY_CONTAINERS => false,
                Params::UNITS => [
                    (object) [
                        "name" => "кор",
                        "value" => 306
                    ]
                ]
            ],
            Params::CATEGORIES => [
                'd9a176ee-1e9c-11e2-ad82-742f68689961',
            ],
            Params::KIT => [],
            Params::KIT_PARENTS => [],
            Params::PROPERTIES => [],
            Params::PARENT => null,
            Params::ANALOGS => (object)[
                Params::FAST => [],
                Params::OTHER => []
            ],
            Params::COMPANIONS => [],
            Params::STORES => [
                (object)[
                    Params::NAME => '400127, Волгоградская обл, Волгоград г, Кольцевая ул, дом № 64',
                    Params::QUANTITY => 884,
                    Params::IS_MAIN => true,
                    Params::UUID => '5f268ab5-c232-11e7-82fe-448a5b2839e6',
                ],
                (object)[
                    Params::NAME => '400048, Волгоградская обл, Волгоград г, Авиаторов ш, дом 18',
                    Params::QUANTITY => 222,
                    Params::IS_MAIN => false,
                    Params::UUID => 'c6559408-1695-11ea-2093-ac1f6b855a52',
                ],
            ],
            Params::IS_BESTSELLER => false,
            Params::IS_NEW => true,
            Params::KEYWORDS => null,
            Params::ALLOW_SAMPLE => false,
        ];

        $client = static::createClient();
        $client->jsonRequest('GET', '/api/v1/product/list?uuid=' . $expected[Params::UUID]);
        $products = $this->getResp2Arr($client->getResponse(), false);

        $this->assertResponseIsSuccessful();
        $this->assertResponseStatusCodeSame(200);
        $this->assertCount(1, $products, "Products count does not equal 1");
        $this->assertEquals((object)$expected, $products[0], "Fail get product");
        $this->assertCount(count($expected[Params::IMAGES]), $products[0]->images);
        $this->assertEquals($expected[Params::IMAGES][0], $products[0]->images[0]);
        $this->matchObjectVars(array_keys($expected), $products[0]);
    }

    /**
     * @group front
     * @group product
     * @group product-list
     */
    public function testProductListInvalid(): void
    {
        $client = static::createClient();
        $client->jsonRequest('GET', '/api/v1/product/list');
        $this->assertResponseStatusCodeSame(400);

        $client->jsonRequest('GET', '/api/v1/product/list?uuid=00000000-0000-0000-0000-000000000000');
        $this->assertResponseStatusCodeSame(204);
    }

    /**
     * @group front
     * @group product
     * @group product-detail
     */
    public function testProductDetailValid(): void
    {
        $client = static::createClient();

        $productUuid = '5ff8776a-a4b2-11ea-d193-ac1f6b855a52';
        /** @var Product $product */
        $product = static::getContainer()->get(ProductRepository::class)->findOneByUuid($productUuid);

        $expectedProduct = [
            Params::UUID => $productUuid,
            Params::NAME => 'Контейнер ПР-МС-350 (РР) СУПНИЦА ЧЕРНАЯ (540шт кор) БЕЗ КРЫШКИ',
            Params::ALIAS => 'konteyner-pr-ms-350-rr-supnica-chernaya-540sht-kor-bez-kryshki',
            Params::CODE => '01-00000484',
            Params::IMAGES => [
                '/images/shop/6b200608-a4b2-11ea-d193-ac1f6b855a52.jpg'
            ],
            Params::PRICE => 565,
            Params::TOTAL_QTY => 6770,
            Params::FAST_QTY => 5500,
            Params::UNIT => 'шт',
            Params::CONTAINERS => [
                Params::ONLY_CONTAINERS => false,
                Params::UNITS => [
                    [
                        Params::NAME => 'кор',
                        Params::VALUE => 540
                    ],
                ],
            ],
            Params::CATEGORIES => [
                'b0a5a1b2-4f9d-11e9-9e9a-ac1f6b855a52',
            ],
            Params::DESCRIPTION => '',
            Params::ANALOGS => $product->getProductAnalogs(),
            Params::COMPANIONS => $product->getProductCompanions(),
            Params::KIT => [],
            Params::PARAMS => $product->getProductParams(),
            Params::PROPERTIES => [],
            Params::STORES => [
                [
                    Params::NAME => '400127, Волгоградская обл, Волгоград г, Кольцевая ул, дом № 64',
                    Params::QUANTITY => 5500,
                    Params::IS_MAIN => true,
                    Params::UUID => '5f268ab5-c232-11e7-82fe-448a5b2839e6',
                ],
                [
                    Params::NAME => '400048, Волгоградская обл, Волгоград г, Авиаторов ш, дом 18',
                    Params::QUANTITY => 730,
                    Params::IS_MAIN => false,
                    Params::UUID => 'c6559408-1695-11ea-2093-ac1f6b855a52',
                ],
            ],
            Params::PARENT => null,
            Params::VARIATIONS => [],
            Params::IS_BESTSELLER => true,
            Params::IS_NEW => false,
            Params::META_TITLE => null,
            Params::META_DESCRIPTION => null,
            Params::KIT_PARENTS => [
                'konteyner-pr-ms-350-rr-supnica-chernaya-540-komplekt'
            ],
            Params::KEYWORDS => null,
            Params::ALLOW_SAMPLE => false,
        ];

        $client->jsonRequest("GET", "/api/v1/product/" . $expectedProduct['uuid']);
        $this->assertResponseIsSuccessful();

        $actualProduct = $this->getResp2Arr($client->getResponse());

        $this->assertEquals($expectedProduct, $actualProduct, 'Error to match product');
        $this->assertEquals("ff1d7054-9776-11eb-2481-ac1f6b855a52", $actualProduct[Params::ANALOGS][Params::FAST][3][Params::UUID]);
        $this->assertEquals("ff1d7054-9776-11eb-2481-ac1f6b855a52", $actualProduct[Params::COMPANIONS][1][Params::UUID]);
    }

    /**
     * @group front
     * @group product
     * @group waiting-product
     */
    public function testWaitingProduct()
    {
        $client = static::createClient();
        /** @var WaitingProductRepository $waitingRepo */
        $waitingRepo = static::getContainer()->get(WaitingProductRepository::class);

        $emptyProduct = static::getContainer()->get(ProductRepository::class)->find(12);
        $fullProduct = static::getContainer()->get(ProductRepository::class)->find(1);

        $validEmail = "test@example.com";

        // Невалидный контакт
        $client->jsonRequest("POST", "/api/v1/product/waiting/" . $emptyProduct->getUuid(), [
            Params::EMAIL => "invalid email"
        ]);
        $this->assertResponseStatusCodeSame(ApiController::STATUS_BAD_REQUEST);
        $resp = $this->getResp2Arr($client->getResponse());
        $this->assertArrayHasKey(Params::CODE, $resp);
        $this->assertEquals(ApiError::ERROR_INVALID_EMAIL, $resp[Params::CODE]);

        // Уведомление для товара, в котором нет количества
        $client->jsonRequest("POST", "/api/v1/product/waiting/" . $emptyProduct->getUuid(), [
            Params::EMAIL => $validEmail
        ]);
        $this->assertResponseIsSuccessful();

        $waitings = $waitingRepo->findBy(['product' => $emptyProduct, 'contact' => $validEmail, 'notificatedAt' => null]);
        $this->assertCount(1, $waitings);

        // Повторное добавление записи для товара, в котором нет количества
        $client->jsonRequest("POST", "/api/v1/product/waiting/" . $emptyProduct->getUuid(), [
            Params::EMAIL => $validEmail
        ]);
        $this->assertResponseIsSuccessful();
        $waitings = $waitingRepo->findBy(['product' => $emptyProduct, 'contact' => $validEmail, 'notificatedAt' => null]);
        $this->assertCount(1, $waitings);

        //Невалидный uuid товара
        $client->jsonRequest("POST", "/api/v1/product/waiting/invaliduuid", [
            Params::EMAIL => $validEmail
        ]);
        $this->assertResponseStatusCodeSame(ApiController::STATUS_NOT_FOUND);

        // Добавление нового уведомления после срабатывания старого
        $waitings[0]->setNotificatedAt(new DateTime());
        $waitingRepo->setNotificationAt($waitings[0]);
        $waitings = $waitingRepo->findBy(['product' => $emptyProduct, 'contact' => $validEmail, 'notificatedAt' => null]);
        $this->assertCount(0, $waitings);

        $client->jsonRequest("POST", "/api/v1/product/waiting/" . $emptyProduct->getUuid(), [
            Params::EMAIL => $validEmail
        ]);
        $this->assertResponseIsSuccessful();
        $waitings = $waitingRepo->findBy(['product' => $emptyProduct, 'contact' => $validEmail, 'notificatedAt' => null]);
        $this->assertCount(1, $waitings);

        // Добавление уведомления для товара, у которого есть количество
        $client->jsonRequest("POST", "/api/v1/product/waiting/" . $fullProduct->getUuid(), [
            Params::EMAIL => $validEmail
        ]);
        $this->assertResponseStatusCodeSame(ApiController::STATUS_NOT_FOUND);

        // Отправка уведомления сработала при обновлении товара при парсинге
        $waiting = $waitingRepo->find(1);
        $this->assertEquals(SecurityFixtures::CART_EMAIL, $waiting->getContact());
        $this->assertNotNull($waiting->getNotificatedAt());
    }

    /**
     * @group front
     * @group product
     * @group fields
     */
    public function testProductFields()
    {
        $client = static::createClient();

        $fields = ["name", "uuid", "price","unit", "alias", "categories", "images", "containers", "kit", "properties", "stores"];
        $productUuid = '5e5c7a48-bdbb-11e9-cc89-ac1f6b855a52';

        // Проверка выбранных полей в запросе списка
        $client->jsonRequest("GET", sprintf("/api/v1/product/list?uuid=%s&fields=%s", $productUuid, implode(",", $fields)));
        $this->assertResponseIsSuccessful();

        $products = $this->getResp2Arr($client->getResponse());
        $this->matchObjectVars($fields, (object)$products[0]);
        $this->assertCount(count($fields), $products[0]);

        // Проверка выбранных полей в запросе индивидуального товара
        $client->jsonRequest("GET", sprintf("/api/v1/product/%s?fields=%s", $productUuid, implode(",", $fields)));
        $this->assertResponseIsSuccessful();

        $product = $this->getResp2Arr($client->getResponse());
        $this->matchObjectVars($fields, (object)$product);
        $this->assertCount(count($fields), $product);
    }
}
