<?php

namespace App\Tests\Controller\Front;

use App\DataFixtures\CartFixtures;
use App\Entity\Order;
use App\Entity\PaymentMethod;
use App\Entity\ShippingAddress;
use App\Entity\ShippingMethod;
use App\Helpers\Params;
use App\Repository\OrderRepository;
use App\Repository\PayerRepository;
use App\Repository\PaymentMethodRepository;
use App\Repository\ShippingAddressRepository;
use App\Repository\ShippingMethodRepository;
use App\Repository\StoreRepository;
use App\Tests\Controller\CustomWebTestCase;

/**
 * Тесты для контроллера оформления заказа
 */
class CheckoutControllerTest extends CustomWebTestCase
{
    /**
     * @group front
     * @group checkout
     * @group lastOrder
     */
    public function testLastOrder()
    {
        $client = static::createClient();
        $userToken = $this->getUserToken($client, "cart@test.com");

        /** @var Order $cart */
        $cart = static::getContainer()->get(OrderRepository::class)->find(1);
        /** @var Order $order */
        $order = static::getContainer()->get(OrderRepository::class)->find(2);
        $this->assertNotNull($cart, "Error to exists cart");
        $this->assertNotNull($order, "Error to exists order");
        $this->assertTrue(Order::STATE_CART == $cart->getState(), "Error to state cart");
        $this->assertTrue(Order::STATE_NEW == $order->getState(), "Error to state order");

        $client->jsonRequest("GET", "/api/v1/checkout/last-order", [], [self::HEADER_AUTH => 'Bearer ' . $userToken]);
        $this->assertResponseIsSuccessful();

        $resp = $this->getResp2Arr($client->getResponse());
        $this->assertArrayHasKey("fio", $resp);
        $this->assertArrayHasKey("email", $resp);
        $this->assertArrayHasKey("phone", $resp);
        $this->assertArrayHasKey("bussiness_area", $resp);
    }

    /**
     * @group front
     * @group checkout
     * @group customerData
     */
    public function testCustomerData()
    {
        $client = static::createClient();
        $userToken = $this->getUserToken($client, "cart@test.com");

        $client->jsonRequest("GET", "/api/v1/checkout/customer-data", [], [self::HEADER_AUTH => 'Bearer ' . $userToken]);
        $this->assertResponseIsSuccessful();

        $resp = $this->getResp2Arr($client->getResponse());
        $this->assertArrayHasKey(Params::AREAS, $resp);
        $this->assertContains(CartFixtures::CUSTOM_BUSSINESS_AREA, $resp[Params::AREAS]);

        $this->assertArrayHasKey(Params::EMAILS, $resp);
        $this->assertContains("cart@test.com", $resp[Params::EMAILS]);

        $this->assertArrayHasKey(Params::PHONES, $resp);
        $this->assertNotEmpty($resp[Params::PHONES]);

    }

    /**
     * @group front
     * @group checkout
     * @group shippingMethods
     */
    public function testShippingMethods()
    {
        $client = static::createClient();
        $userToken = $this->getUserToken($client, "cart@test.com");
        $expectedFields = [Params::NAME, Params::DESCRIPTION, Params::WEIGHT, Params::COST,
            Params::USE_ADDRESS, Params::IS_FAST, Params::ALIAS, Params::REGION];

        $client->jsonRequest("GET", "/api/v1/checkout/shipping-methods?regions=г Волгоград", [], [self::HEADER_AUTH => 'Bearer ' . $userToken]);
        $this->assertResponseIsSuccessful();

        $resp = $this->getResp2Arr($client->getResponse());
        $this->assertCount(3, $resp, "Fail to get methods count");
        $this->matchObjectVars($expectedFields, (object)$resp[0]);
    }

    /**
     * @group front
     * @group checkout
     * @group paymentMethods
     */
    public function testPaymentMethods()
    {
        $client = static::createClient();
        $userToken = $this->getUserToken($client, "cart@test.com");
        $expectedFields = [Params::NAME, Params::WEIGHT, Params::UID];

        $client->jsonRequest("GET", "/api/v1/checkout/payment-methods", [], [self::HEADER_AUTH => 'Bearer ' . $userToken]);
        $this->assertResponseIsSuccessful();

        $resp = $this->getResp2Arr($client->getResponse());
        $this->assertCount(4, $resp, "Fail to get methods count");
        $this->matchObjectVars($expectedFields, (object)$resp[0]);
    }


    /**
     * @group front
     * @group checkout
     * @group replacementMethods
     */
    public function testReplacementMethods()
    {
        $expectedFields = ['id', 'title', 'description', 'weight'];
        $client = static::createClient();
        $userToken = $this->getUserToken($client, "cart@test.com");

        $client->jsonRequest("GET", "/api/v1/checkout/replacements", [], [self::HEADER_AUTH => 'Bearer ' . $userToken]);
        $this->assertResponseIsSuccessful();

        $resp = $this->getResp2Arr($client->getResponse());
        $this->assertCount(4, $resp, "Fail to get methods count");
        $this->assertEquals($expectedFields, array_keys($resp[0]));
    }

    /**
     * @group front
     * @group checkout
     * @group shippingCost
     */
    public function testShippingCost()
    {
        $client = static::createClient();
        $userToken = $this->getUserToken($client, "cart@test.com");
        $region = 'г Волгоград';

        /** @var Order $cart */
        $cart = static::getContainer()->get(OrderRepository::class)->find(1);

        // Платная доставка
        $client->jsonRequest("POST", "/api/v1/checkout/shipping-cost", [
            Params::CART => $cart->getUid(), Params::SHIPPING_METHOD => ShippingMethod::METHOD_COMPANY,
            Params::REGION => $region, Params::SHIPPING_DATE => (new \DateTime())->format("Y-m-d")
        ], [self::HEADER_AUTH => 'Bearer ' . $userToken]);
        $this->assertResponseIsSuccessful();

        $resp = $this->getResp2Arr($client->getResponse());
        $this->assertArrayHasKey(Params::SHIPPING_COST, $resp);
        $this->assertEquals(0, $resp[Params::SHIPPING_COST]);
        $this->assertCount(2, $resp[Params::PAYMENT_METHODS]);

        // Бесплатная доставка
        $data = [
            Params::PRODUCT => '25c32eac-bdec-11e7-9b53-38607704c280',
            Params::QUANTITY => 100,
            Params::CART => $cart->getUid()
        ];

        $client->jsonRequest('POST', '/api/v1/cart/change-qty', $data, [self::HEADER_AUTH => 'Bearer ' . $userToken]);
        $this->assertResponseIsSuccessful();

        // Возвращает все методы оплаты
        $client->jsonRequest("POST", "/api/v1/checkout/shipping-cost", [
            Params::CART => $cart->getUid(), Params::SHIPPING_METHOD => ShippingMethod::METHOD_COURIER, Params::REGION => $region
        ], [self::HEADER_AUTH => 'Bearer ' . $userToken]);
        $this->assertResponseIsSuccessful();

        $resp = $this->getResp2Arr($client->getResponse());
        $this->assertArrayHasKey(Params::SHIPPING_COST, $resp);
        $this->assertEquals(0, $resp[Params::SHIPPING_COST]);
        $this->assertCount(4, $resp[Params::PAYMENT_METHODS]);

        // Метод доставки "Транспортная компания" возвращает только оплата онлайн или банковский перевод
        $client->jsonRequest("POST", "/api/v1/checkout/shipping-cost", [
            Params::CART => $cart->getUid(), Params::SHIPPING_METHOD => ShippingMethod::METHOD_COMPANY, Params::REGION => 'г Москва'
        ], [self::HEADER_AUTH => 'Bearer ' . $userToken]);
        $this->assertResponseIsSuccessful();

        $resp = $this->getResp2Arr($client->getResponse());
        $this->assertArrayHasKey(Params::SHIPPING_COST, $resp);
        $this->assertEquals(0, $resp[Params::SHIPPING_COST]);
        $this->assertCount(2, $resp[Params::PAYMENT_METHODS]);

        $methods = [];
        foreach ($resp[Params::PAYMENT_METHODS] as $method) {
            $methods[] = $method[Params::UID];
        }

        $this->assertFalse(in_array(PaymentMethod::UID_CARD_COURIER, $methods));
        $this->assertFalse(in_array(PaymentMethod::UID_CASH, $methods));
    }

    /**
     * @group front
     * @group checkout
     * @group saveOrder
     */
    public function testSaveOrderWithCourier()
    {
        $client = static::createClient();
        $userToken = $this->getUserToken($client, "cart@test.com");
        $region = 'г Волгоград';

        $payer = self::getContainer()->get(PayerRepository::class)->find(1);
        $address = self::getContainer()->get(ShippingAddressRepository::class)->find(1);
        $shippingMethod = self::getContainer()->get(ShippingMethodRepository::class)->findOneBy(['alias' => ShippingMethod::METHOD_COURIER]);
        $paymentMethod = self::getContainer()->get(PaymentMethodRepository::class)->find(1);

        $data = [
            Params::FIO => 'Иванов И.И.',
            Params::EMAIL => 'test@gmail.com',
            Params::PHONE => '+79899632541',
            Params::BUSSINESS_AREA => 'Магазины',
            Params::SHIPPING_METHOD => $shippingMethod->getAlias(),
            Params::SHIPPING_DATE => '2022-01-01T11:20:33+0300',
            Params::PAYER => $payer->getUid(),
            Params::SHIPPING_ADDRESS => $address->getUid(),
            Params::REGION => $region,
            Params::REPLACEMENT => 1,
            Params::COMMENT => 'комментарий',
            Params::PAYMENT_METHOD => $paymentMethod->getUid(),
        ];

        /** @var Order $cart */
        $cart = static::getContainer()->get(OrderRepository::class)->find(1);

        $client->jsonRequest("PUT", "/api/v1/checkout/order-save/" . $cart->getUid(), $data,
            [self::HEADER_AUTH => 'Bearer ' . $userToken]);

        $this->assertResponseIsSuccessful();

        $resp = $this->getResp2Arr($client->getResponse());
        /** @var Order $cart */
        $cart = static::getContainer()->get(OrderRepository::class)->find(1);
        $this->assertEquals(Order::STATE_NEW, $cart->getState());
        $this->assertNotEmpty($cart->getCheckoutCompleteAt());
        $this->assertArrayHasKey(Params::NUMBER, $resp);
        $this->assertEquals($cart->getId(), $resp[Params::NUMBER]);
    }

    /**
     * @group front
     * @group checkout
     * @group saveOrder
     */
    public function testSaveOrderWithPickup()
    {
        $client = static::createClient();
        $userToken = $this->getUserToken($client, "cart@test.com");
        $region = 'г Волгоград';

        $address = self::getContainer()->get(StoreRepository::class)->find(1);
        $shippingMethod = self::getContainer()->get(ShippingMethodRepository::class)->find(2);

        $data = [
            Params::FIO => 'Иванов И.И.',
            Params::EMAIL => 'test@gmail.com',
            Params::PHONE => '+79899632541',
            Params::BUSSINESS_AREA => 'Магазины',
            Params::SHIPPING_METHOD => $shippingMethod->getAlias(),
            Params::SHIPPING_ADDRESS => $address->getUuid(),
            Params::REGION => $region,
            Params::REPLACEMENT => 1,
            Params::COMMENT => 'комментарий',
        ];

        /** @var Order $cart */
        $cart = static::getContainer()->get(OrderRepository::class)->find(1);

        $client->jsonRequest("PUT", "/api/v1/checkout/order-save/" . $cart->getUid(), $data,
            [self::HEADER_AUTH => 'Bearer ' . $userToken]);

        $this->assertResponseIsSuccessful();

        $resp = $this->getResp2Arr($client->getResponse());
        /** @var Order $cart */
        $cart = static::getContainer()->get(OrderRepository::class)->find(1);
        $this->assertEquals(Order::STATE_NEW, $cart->getState());
        $this->assertNotEmpty($cart->getCheckoutCompleteAt());
        $this->assertArrayHasKey(Params::NUMBER, $resp);
        $this->assertEquals($cart->getId(), $resp[Params::NUMBER]);
    }
}