<?php

namespace App\Tests\Controller\Front;

use App\DataFixtures\FakeData;
use App\Entity\Category;
use App\Helpers\Params;
use App\Repository\CategoryRepository;
use App\Repository\ValueRepository;
use App\Tests\Controller\CustomWebTestCase;

/**
 * Class CatalogControllerTest
 */
class CatalogControllerTest extends CustomWebTestCase
{
    /**
     * @group front
     * @group catalog
     * @group categories
     */
    public function testCategories(): void
    {
        $expectedBusinessArea = [
            Params::ID => 8,
            Params::UUID => "3afcc968-dba7-11e9-fa83-ac1f6b855a52",
            Params::NAME => "Бани",
            Params::ALIAS => "bani",
            Params::PARENT => 0,
            Params::TAGS => [],
            Params::PRODUCT_QTY => 0,
            Params::IMAGE => '',
            Params::META_TITLE => null,
            Params::META_DESCRIPTION => null,
            Params::SELECTION_FILE => '',
            Params::WEIGHT => 0,
        ];

        $expectedCategory = [
            Params::ID => 37,
            Params::UUID => "37832fef-1dd8-11e2-bf75-742f68689961",
            Params::NAME => "Одноразовая Посуда / Приборы",
            Params::ALIAS => "odnorazovaya-posuda-pribory",
            Params::PARENT => 0,
            Params::TAGS => [],
            Params::PRODUCT_QTY => 9,
            Params::IMAGE => '',
            Params::META_TITLE => 'Купить товары Одноразовая Посуда / Приборы',
            Params::META_DESCRIPTION => 'Товары Одноразовая Посуда / Приборы оптом',
            Params::SELECTION_FILE => '',
            Params::WEIGHT => 2,
        ];

        $client = static::createClient();
        $client->jsonRequest('GET', '/api/v1/catalog/categories');
        $result = $this->getResp2Arr($client->getResponse(), false);
        $businessAreas = $result->bussinessAreas;

        $this->assertResponseIsSuccessful();
        $this->assertResponseStatusCodeSame(200);
        $this->assertCount(15, $businessAreas, "Categories count does not equal 15");
        $this->assertEquals((object)$expectedBusinessArea, $businessAreas[7], "Fail get category");
        $this->matchObjectVars(array_keys($expectedBusinessArea), $businessAreas[7]);

        $categories = $result->categories;
        $this->assertResponseIsSuccessful();
        $this->assertResponseStatusCodeSame(200);
        $this->assertCount(193, $categories, "Categories count does not equal 193");
        $this->assertEquals((object)$expectedCategory, $categories[17], "Fail get category");
        $this->matchObjectVars(array_keys($expectedCategory), $categories[17]);
    }

    /**
     * @group front
     * @group catalog
     * @group categories
     */
    public function testCategoriesByArea(): void
    {
        $expected = [
            "uuid" => "6d399b00-55e6-11e9-9e9a-ac1f6b855a52",
            'product_qty' => 1,
            'children' => []
        ];

        $client = static::createClient();

        /** @var Category $businessArea */
        $businessArea = static::getContainer()->get(CategoryRepository::class)->find(6);
        $client->jsonRequest('GET', '/api/v1/catalog/bussiness-area/' . $businessArea->getUuid());
        $categories = $this->getResp2Arr($client->getResponse(), false);

        $this->assertResponseIsSuccessful();
        $this->assertResponseStatusCodeSame(200);
        $this->assertCount(4, $categories, "Categories count does not equal");
        $this->matchObjectVars(array_keys($expected), $categories[0]);
    }

    /**
     * @group front
     * @group catalog
     * @group tags
     */
    public function testTags(): void
    {
        $expectedFields = ["id", "name", "url"];

        $client = static::createClient();
        $client->jsonRequest('GET', '/api/v1/catalog/tags');
        $tags = $this->getResp2Arr($client->getResponse(), false);

        $this->assertResponseIsSuccessful();
        $this->assertResponseStatusCodeSame(200);
        $this->assertCount(FakeData::FAKE_QTY, $tags, "Tags count does not equal " . FakeData::FAKE_QTY);
        $this->matchObjectVars($expectedFields, $tags[0]);
    }

    /**
     * @group front
     * @group catalog
     * @group tags
     */
    public function testOneTag(): void
    {
        $expectedFields = ["name", "url", "meta_title", "meta_description", "h1", "text"];

        $client = static::createClient();
        $client->jsonRequest('GET', '/api/v1/catalog/tags/1');
        $tag = $this->getResp2Arr($client->getResponse(), false);

        $this->assertResponseIsSuccessful();
        $this->assertResponseStatusCodeSame(200);
        $this->assertNotEmpty($tag, "Fail get tag with id 1");
        $this->matchObjectVars($expectedFields, $tag);
    }

    /**
     * @group front
     * @group catalog
     * @group params
     */
    public function testParams(): void
    {
        $expectedProductQty = [
            [2, 0],
            [10],
            [2],
            [1],
            [1],
            [1],
            [1],
            [1],
            [0],
            [1]
        ];

        $client = static::createClient();
        $client->jsonRequest('GET', '/api/v1/catalog/params');
        $params = $this->getResp2Arr($client->getResponse(), false);

        $this->assertResponseIsSuccessful();
        $this->assertCount(10, $params, "Fail params count");
        for ($i = 0; $i < count($expectedProductQty); $i++){
            for ($j = 0; $j < count($expectedProductQty[$i]); $j++){
                $expectedVal = $expectedProductQty[$i][$j];
                $this->assertEquals($expectedVal, $params[$i]->values[$j]->product_qty, $i);
            }
        }

        $expectedParamFields = ["uuid", "name", "order", "values"];
        $expectedValueFields = ["uuid", "name", "image", "product_qty"];

        $this->matchObjectVars($expectedParamFields, $params[0]);
        $this->matchObjectVars($expectedValueFields, $params[0]->values[0]);

    }

    /**
     * @group front
     * @group catalog
     * @group filter
     */
    public function testFilterProducts(): void
    {
        $client = static::createClient();

        /** Uuid первого свойства, т.к. uuid для свойств формируются динамически */
        $testParam1 = static::getContainer()->get(ValueRepository::class)->find(7)->getUuid();
        $testParam2 = static::getContainer()->get(ValueRepository::class)->find(8)->getUuid();

        $testCases = [
            [
                'name' => 'Empty max per page',
                'params' => [],
                'expectedStatus' => 200,
                'expectedProductCount' => 16,
            ],
            [
                'name' => 'Two params with category uuid',
                'params' => [
                    Params::PARAMS => sprintf('%s,%s', $testParam1, $testParam2),
                    Params::CATEGORIES => '903c65f6-55e6-11e9-9e9a-ac1f6b855a52',
                ],
                'expectedStatus' => 204,
            ],
            [
                'name' => 'Two params with category alias',
                'params' => [
                    Params::PARAMS => sprintf('%s,%s', $testParam1, $testParam2),
                    Params::CATEGORIES => 'aksessuary-dlya-bumajnyh-stakanov',
                ],
                'expectedStatus' => 204,
            ],
            [
                'name' => 'Set max per page',
                'params' => [Params::PER_PAGE => 10],
                'expectedStatus' => 200,
                'expectedProductCount' => 10,
            ],
            [
                'name' => 'Sort by price asc',
                'params' => [],
                'expectedStatus' => 200,
                'expectedProductCount' => 16,
                'expectedProductFields' => [Params::UUID, Params::ALIAS, Params::NAME, Params::PRICE, Params::IMAGES,
                    Params::UNIT, Params::TOTAL_QTY, Params::FAST_QTY, Params::PROPERTIES, Params::CONTAINERS,
                    Params::STORES, Params::IS_BESTSELLER, Params::KIT, Params::PARAMS, Params::KEYWORDS, Params::BUSINESS_AREAS],
            ],
            [
                'name' => 'Select page 2',
                'params' => [
                    Params::PAGE => 2,
                    Params::PER_PAGE => 10
                ],
                'expectedStatus' => 200,
                'expectedProductCount' => 6,
            ],
            [
                'name' => 'Just categories',
                'params' => [
                    Params::CATEGORIES => "903c65f6-55e6-11e9-9e9a-ac1f6b855a52",
                    Params::IS_ENABLED => 1,
                ],
                'expectedStatus' => 200,
                'expectedProductCount' => 4
            ],
            [
                'name' => 'Just params',
                'params' => [
                    Params::PARAMS => $testParam1,
                ],
                'expectedStatus' => 200,
                'expectedProductCount' => 1
            ],
            [
                'name' => 'Just price',
                'params' => [
                    Params::PRICE => "210-600"
                ],
                'expectedStatus' => 200,
                'expectedProductCount' => 5
            ],
            [
                'name' => 'Has products',
                'params' => [
                    Params::CATEGORIES => "903c65f6-55e6-11e9-9e9a-ac1f6b855a52",
//                    Params::PARAMS => $testParam1,
                    Params::PRICE => "50-200"
                ],
                'expectedStatus' => 200,
                'expectedProductCount' => 4
            ],
            [
                'name' => 'No products',
                'params' => [
                    Params::CATEGORIES => "64bb00d1-e257-11e7-be89-6c3be5236808,779ed608-e258-11e7-b790-448a5b2839e6",
                    Params::PARAMS => "9b0db09f-8fb0-3f36-9fae-efd0933779e9,08bba9c5-4242-3171-ac68-3e74af062eaf",
                    Params::PRICE => "700-900"
                ],
                'expectedStatus' => 204,
                'expectedProductCount' => 0
            ],
            [
                'name' => 'Fast delivery',
                'params' => [
                    Params::IS_FAST => true
                ],
                'expectedStatus' => 200,
                'expectedProductCount' => 13
            ],
            [
                'name' => 'Bestsellers',
                'params' => [
                    Params::BESTSELLER => true
                ],
                'expectedStatus' => 200,
                'expectedProductCount' => 3
            ],
            [
                'name' => 'News',
                'params' => [
                    Params::NEW => true
                ],
                'expectedStatus' => 200,
                'expectedProductCount' => 8
            ],
        ];

        foreach ($testCases as $tc) {
            $params = [];
            foreach ($tc['params'] as $key => $value) {
                $params[] = sprintf("%s=%s", $key, $value);
            }

            $client->request('GET', '/api/v1/catalog?' . implode("&", $params));

            $this->assertResponseIsSuccessful($tc['name']);
            $this->assertResponseStatusCodeSame($tc['expectedStatus'], sprintf("%s: fail expected code", $tc['name']));

            if(204 == $tc['expectedStatus']) {
                continue;
            }

            $result = $this->getResp2Arr($client->getResponse(), false);
            if(200 == $tc['expectedStatus']) {
                $this->matchObjectVars(['total', 'products', 'params', 'priceRange'], $result);
            }
            if($tc['expectedProductCount']) {
                $this->assertCount($tc['expectedProductCount'], $result->products, sprintf("%s: does not equal expected count", $tc['name']));
            }

            if(isset($tc['expectedProductFields'])) {
                $this->matchObjectVars($tc['expectedProductFields'], $result->products[0], $tc['name']);
            }
        }
    }

    /**
     * @group front
     * @group catalog
     * @group sort-type
     */
    public function testSortType(): void
    {
        $expectedFields = ["alias", "name"];

        $client = static::createClient();
        $client->jsonRequest('GET', '/api/v1/catalog/sort-type');
        $tags = $this->getResp2Arr($client->getResponse(), false);

        $this->assertResponseIsSuccessful();
        $this->assertResponseStatusCodeSame(200);
        $this->matchObjectVars($expectedFields, $tags[0]);
    }

    /**
     * @group front
     * @group catalog
     * @group sort-type
     */
    public function testSort()
    {
        $client = static::createClient();

        $testCases = [
            [
                'name' => 'by name',
                'type' => Params::SORT_BY_NAME,
                'expectedUuid' => '25c32eac-bdec-11e7-9b53-38607704c280'
            ],
            [
                'name' => 'by popular',
                'type' => Params::SORT_BY_POPULAR,
                'expectedUuid' => '5ff8776a-a4b2-11ea-d193-ac1f6b855a52'
            ],
            [
                'name' => 'by price asc',
                'type' => Params::SORT_BY_PRICE_ASC,
                'expectedUuid' => '0dad5f90-980e-11e9-df8a-ac1f6b855a52'
            ],
            [
                'name' => 'by price desc',
                'type' => Params::SORT_BY_PRICE_DESC,
                'expectedUuid' => '8417862e-8be1-11eb-d996-ac1f6b855a52'
            ],
            [
                'name' => 'by default',
                'type' => 'failed_type',
                'expectedUuid' => '25c32eac-bdec-11e7-9b53-38607704c280'
            ],
        ];

        foreach ($testCases as $tc) {
            $client->jsonRequest('GET', '/api/v1/catalog?sortby=' . $tc['type']);

            $this->assertResponseIsSuccessful();
            $content = $this->getResp2Arr($client->getResponse());
            $products = $content['products'];

            $this->assertEquals($tc['expectedUuid'], $products[0]['uuid'], 'Error to unexpected product in ' . $tc['name']);
        }


    }

    /**
     * @group front
     * @group category
     * @group help-page
     */
    public function testGetHelpPage(): void
    {
        $testCases = [
            [
                'name' => 'Valid help page',
                'category' => 1,
                'statusCode' => 200,
            ],
            [
                'name' => 'Invalid help page category',
                'category' => 0,
                'statusCode' => 404,
            ],
        ];

        $client = static::createClient();
        foreach ($testCases as $tc) {
            $client->jsonRequest('GET', '/api/v1/catalog/category/help-page/' . $tc['category']);
            $this->assertResponseStatusCodeSame($tc['statusCode'], sprintf("%s: fail expected code", $tc['name']));
        }
    }

    /**
     * @group front
     * @group catalog
     * @group hits
     */
    public function testHits(): void
    {
        $client = static::createClient();
        $expectedProductFields = [Params::UUID, Params::ALIAS, Params::NAME, Params::PRICE, Params::IMAGES,
            Params::UNIT, Params::TOTAL_QTY, Params::FAST_QTY, Params::PROPERTIES, Params::CONTAINERS,
            Params::STORES, Params::IS_BESTSELLER, Params::KIT, Params::PARAMS];

        $client->jsonRequest("GET", "/api/v1/hits");
        $this->assertResponseIsSuccessful();

        $resp = $this->getResp2Arr($client->getResponse());
        $this->assertCount(3, $resp);
        $this->matchObjectVars($expectedProductFields, (object)$resp[0]);
    }
}
