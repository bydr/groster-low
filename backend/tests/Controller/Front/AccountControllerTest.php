<?php

namespace App\Tests\Controller\Front;

use App\ApiError;
use App\Controller\ApiController;
use App\Entity\Order;
use App\Entity\Payer;
use App\Entity\Security\AuthUser;
use App\Helpers\Params;
use App\Repository\AuthUserRepository;
use App\Repository\OrderRepository;
use App\Repository\PayerRepository;
use App\Repository\ProductRepository;
use App\Repository\ShippingAddressRepository;
use App\Repository\WaitingProductRepository;
use App\Tests\Controller\CustomWebTestCase;
use phpDocumentor\Reflection\Types\Self_;

/**
 * Личный кабинет
 */
class AccountControllerTest extends CustomWebTestCase
{
    /**
     * @group front
     * @group account
     * @group address
     */
    public function testAddress()
    {
        $client = static::createClient();
        $token = $this->getUserToken($client);

        $expected = [
            Params::NAME => 'Новый тестовый адрес',
            Params::ADDRESS => 'г. Тест ул. Тестовых партизан, д. 1',
            Params::UID => '',
            Params::IS_DEFAULT => false,
        ];

        // Добавление адреса
        $client->jsonRequest("POST", "/api/v1/account/address/add", $expected, [self::HEADER_AUTH => $token]);
        $this->assertResponseIsSuccessful();

        $resp = $this->getResp2Arr($client->getResponse());
        $this->assertArrayHasKey(Params::UID, $resp);
        $this->assertNotEmpty($resp[Params::UID]);

        $address = static::getContainer()->get(ShippingAddressRepository::class)->findOneByUid($resp[Params::UID]);
        $this->assertNotNull($address);
        $this->assertFalse($address->getIsDefault());

        // Изменение адреса
        $expected["name"] = "Измененный адрес";
        $client->jsonRequest("PUT", "/api/v1/account/address/" . $resp[Params::UID], $expected, [self::HEADER_AUTH => $token]);

        $resp = $this->getResp2Arr($client->getResponse());
        $this->assertArrayHasKey(Params::UID, $resp);
        $this->assertNotEmpty($resp[Params::UID]);

        $uid = $resp[Params::UID];

        /// Получение данных адреса
        $client->jsonRequest("GET", "/api/v1/account/address/" . $resp[Params::UID], [], [self::HEADER_AUTH => $token]);
        $resp = $this->getResp2Arr($client->getResponse());
        $expected[Params::UID] = $uid;
        $this->assertEquals(array_keys($expected), array_keys($resp));

        // Неверный uid
        $client->jsonRequest("GET", "/api/v1/account/address/invalidUid", [], [self::HEADER_AUTH => $token]);
        $this->assertResponseStatusCodeSame(ApiController::STATUS_NOT_FOUND);

        // Невалидный токен
        $client->jsonRequest("GET", "/api/v1/account/address/" . $resp[Params::UID], [], [self::HEADER_AUTH => 'invalid token']);
        $this->assertResponseStatusCodeSame(ApiController::STATUS_UNAUTHORIZATION);
    }

    /**
     * @group front
     * @group account
     * @group address
     */
    public function testAddressSetDefault()
    {
        $client = static::createClient();
        $token = $this->getUserToken($client);

        $data = [
            Params::NAME => 'новый тест адрес',
            Params::ADDRESS => 'какой-то адрес',
            Params::IS_DEFAULT => true
        ];

        // Добавление адреса
        $client->jsonRequest("POST", "/api/v1/account/address/add", $data, [self::HEADER_AUTH => $token]);
        $this->assertResponseIsSuccessful();

        $resp = $this->getResp2Arr($client->getResponse());
        $this->assertArrayHasKey(Params::UID, $resp);
        $uid1 = $resp[Params::UID];
        $this->assertNotEmpty($uid1);
        $address = static::getContainer()->get(ShippingAddressRepository::class)->findOneByUid($uid1);
        $this->assertNotNull($address);
        $this->assertTrue($address->getIsDefault());

        // Установка нового адреса по умолчанию
        $data[Params::NAME] = 'новый тест адрес 2';
        $client->jsonRequest("POST", "/api/v1/account/address/add", $data, [self::HEADER_AUTH => $token]);
        $this->assertResponseIsSuccessful();

        $resp = $this->getResp2Arr($client->getResponse());
        $this->assertArrayHasKey(Params::UID, $resp);
        $uid2 = $resp[Params::UID];
        $this->assertNotEmpty($uid2);
        $address2 = static::getContainer()->get(ShippingAddressRepository::class)->findOneByUid($uid2);
        $this->assertNotNull($address2);
        $this->assertTrue($address2->getIsDefault());

        $address = static::getContainer()->get(ShippingAddressRepository::class)->findOneByUid($uid1);
        $this->assertNotNull($address);
        $this->assertFalse($address->getIsDefault());

        // Установка старого адреса по умолчанию
        $client->jsonRequest("PUT", "/api/v1/account/address/{$uid1}/default", [], [self::HEADER_AUTH => $token]);
        $this->assertResponseIsSuccessful();

        $address = static::getContainer()->get(ShippingAddressRepository::class)->findOneByUid($uid1);
        $this->assertNotNull($address);
        $this->assertTrue($address->getIsDefault());

        $address2 = static::getContainer()->get(ShippingAddressRepository::class)->findOneByUid($uid2);
        $this->assertNotNull($address2);
        $this->assertFalse($address2->getIsDefault());

        // Неверный uid
        $client->jsonRequest("PUT", "/api/v1/account/address/invalidUid/default", [], [self::HEADER_AUTH => $token]);
        $this->assertResponseStatusCodeSame(ApiController::STATUS_NOT_FOUND);

        // Невалидный токен
        $client->jsonRequest("PUT", "/api/v1/account/address/{$uid1}/default", [], [self::HEADER_AUTH => 'invalid token']);
        $this->assertResponseStatusCodeSame(ApiController::STATUS_UNAUTHORIZATION);
    }

    /**
     * @group front
     * @group account
     * @group payer
     */
    public function testPayer()
    {
        $client = static::createClient();
        $token = $this->getUserToken($client);

        $expected = [
            Params::NAME => 'ИП Яровина',
            Params::OGRNIP => '012345678901234',
            Params::INN => '012345678901',
            Params::ACCOUNT => '0123456789',
            Params::BIK => '0123456789',
            Params::KPP => '0123456789',
            Params::CONTACT => 'Иванов И.И.',
            Params::CONTACT_PHONE => '+79899632587',
        ];

        // Добавление
        $client->jsonRequest("POST", "/api/v1/account/payer/add", $expected, [self::HEADER_AUTH => $token]);
        $this->assertResponseIsSuccessful();

        $resp = $this->getResp2Arr($client->getResponse());
        $this->assertArrayHasKey(Params::UID, $resp);
        $this->assertNotEmpty($resp[Params::UID]);
        $expected[Params::UID] = $resp[Params::UID];
        $expected[Params::TITLE] = sprintf("%s, ОГРН %d", $expected[Params::NAME], $expected[Params::OGRNIP]);

        $this->assertNotNull(static::getContainer()->get(PayerRepository::class)->findOneByUid($resp[Params::UID]));

        // Изменение
        $expected["contact"] = "Зайцев С.С.";
        $client->jsonRequest("PUT", "/api/v1/account/payer/" . $resp[Params::UID], $expected, [self::HEADER_AUTH => $token]);
        $this->assertResponseIsSuccessful();

        $resp = $this->getResp2Arr($client->getResponse());
        $this->assertArrayHasKey(Params::UID, $resp);
        $this->assertNotEmpty($resp[Params::UID]);

        $uid = $resp[Params::UID];

        // Получение данных
        $client->jsonRequest("GET", "/api/v1/account/payer/" . $uid, [], [self::HEADER_AUTH => $token]);
        $this->assertResponseIsSuccessful();
        $resp = $this->getResp2Arr($client->getResponse());
        $this->assertEquals(array_keys($expected), array_keys($resp));

        // Неверный uid
        $client->jsonRequest("GET", "/api/v1/account/payer/invalidUid", [], [self::HEADER_AUTH => $token]);
        $this->assertResponseStatusCodeSame(ApiController::STATUS_NOT_FOUND);

        // Не валидное изменение
        $expected["contact_phone"] = "+380955643210";
        $client->jsonRequest("PUT", "/api/v1/account/payer/" . $uid, $expected, [self::HEADER_AUTH => $token]);
        $this->assertResponseStatusCodeSame(ApiController::STATUS_BAD_REQUEST);

        $resp = $this->getResp2Arr($client->getResponse());
        $this->assertArrayHasKey(Params::CODE, $resp);
        $this->assertEquals(ApiError::ERROR_INVALID_PHONE, $resp[Params::CODE]);

        // Невалидный токен
        $client->jsonRequest("GET", "/api/v1/account/address/" . $uid, $expected, [self::HEADER_AUTH => 'invalid token']);
        $this->assertResponseStatusCodeSame(ApiController::STATUS_UNAUTHORIZATION);
    }

    /**
     * @group front
     * @group account
     * @group updatePassword
     */
    public function testUpdatePassword()
    {
        $client = static::createClient();
        $token = $this->getUserToken($client);

        $data = [
            Params::OLD_PASSWORD => 'password1',
            Params::NEW_PASSWORD => 'password2',
        ];
        $client->jsonRequest("PUT", "/api/v1/account/update-password", $data, [self::HEADER_AUTH => $token]);
        $this->assertResponseIsSuccessful();

        $client->jsonRequest("POST", "/api/v1/auth/login_email", [
            Params::EMAIL => 'qwe1@test.com',
            Params::PASSWORD => $data[Params::NEW_PASSWORD]
        ]);
        $this->assertResponseIsSuccessful();

        // Невалидный токен
        $client->jsonRequest("PUT", "/api/v1/account/update-password", $data, [self::HEADER_AUTH => 'invalid token']);
        $this->assertResponseStatusCodeSame(ApiController::STATUS_UNAUTHORIZATION);

        // Неверный старый пароль
        $client->jsonRequest("PUT", "/api/v1/account/update-password", [
            Params::OLD_PASSWORD => 'wrong pass',
            Params::NEW_PASSWORD => 'newpass2'
        ], [self::HEADER_AUTH => $token]);
        $this->assertResponseStatusCodeSame(ApiController::STATUS_BAD_REQUEST);
        $resp = $this->getResp2Arr($client->getResponse());
        $this->assertArrayHasKey(Params::CODE, $resp);
        $this->assertEquals(ApiError::INVALID_PASSWORD, $resp[Params::CODE]);

        // Возвращение старого пароля
        $client->jsonRequest("PUT", "/api/v1/account/update-password", [
            Params::OLD_PASSWORD => $data[Params::NEW_PASSWORD],
            Params::NEW_PASSWORD => $data[Params::OLD_PASSWORD]
        ], [self::HEADER_AUTH => $token]);
        $this->assertResponseIsSuccessful();
    }

    /**
     * @group front
     * @group account
     * @group orderListOrders
     */
    public function testOrderListOrders()
    {
        $client = static::createClient();
        $token = $this->getUserToken($client, "cart@test.com");

        $fields = [
            Params::UID,
            Params::TOTAL_COST,
            Params::STATE,
            Params::CREATE_AT,
            Params::NUMBER,
            Params::SUBSCRIBE_INTERVAL
        ];
        // Статус заказ новый
        $client->jsonRequest("GET", "/api/v1/account/order-list/orders?state=" . Order::STATE_NEW, [], [self::HEADER_AUTH => $token]);
        $this->assertResponseIsSuccessful();

        $resp = $this->getResp2Arr($client->getResponse());
        $this->assertCount(1, $resp[Params::ORDERS]);
        $this->assertEquals(Order::STATE_NEW, $resp[Params::ORDERS][0][Params::STATE]);
        $this->matchObjectVars($fields, (object)$resp[Params::ORDERS][0]);

        // Статус заказ выполнен
        $client->jsonRequest("GET", "/api/v1/account/order-list/orders?state=" . Order::STATE_FINISHED, [], [self::HEADER_AUTH => $token]);
        $this->assertResponseIsSuccessful();

        $resp = $this->getResp2Arr($client->getResponse());
        $this->assertCount(1, $resp[Params::ORDERS]);
        $this->assertEquals(Order::STATE_FINISHED, $resp[Params::ORDERS][0][Params::STATE]);
        $this->matchObjectVars($fields, (object)$resp[Params::ORDERS][0]);

        // Статус заказ отменен
        $payer = static::getContainer()->get(PayerRepository::class)->find(1);
        $this->assertNotNull($payer);

        $client->jsonRequest("GET",
            sprintf("%s?state=%d&year=%d&payer=%s", "/api/v1/account/order-list/orders", Order::STATE_CANCELED, 2022, $payer->getUid()),
            [],
            [self::HEADER_AUTH => $token]);
        $this->assertResponseIsSuccessful();

        $resp = $this->getResp2Arr($client->getResponse());
        $this->assertCount(1, $resp[Params::ORDERS]);
        $this->assertEquals(Order::STATE_CANCELED, $resp[Params::ORDERS][0][Params::STATE]);
        $this->matchObjectVars($fields, (object)$resp[Params::ORDERS][0]);

        // Статус заказ отменен с фильтрацией по году
        $client->jsonRequest("GET",
            sprintf("%s?%s=%d&%s=%d", "/api/v1/account/order-list/orders", Params::STATE, Order::STATE_CANCELED, Params::YEAR, 2020),
            [],
            [self::HEADER_AUTH => $token]);
        $this->assertResponseStatusCodeSame(ApiController::STATUS_EMPTY);

        // Неверный статус заказа
        $client->jsonRequest("GET", "/api/v1/account/order-list/orders?state=100", [], [self::HEADER_AUTH => $token]);
        $this->assertResponseStatusCodeSame(ApiController::STATUS_BAD_REQUEST);

        $resp = $this->getResp2Arr($client->getResponse());
        $this->assertArrayHasKey(Params::CODE, $resp);
        $this->assertEquals(ApiError::ERROR_INVALID_DATA, $resp[Params::CODE]);
    }

    /**
     * @group front
     * @group account
     * @group orderListProducts
     */
    public function testOrderListProducts()
    {
        $client = static::createClient();
        $token = $this->getUserToken($client, "cart@test.com");

        // Статус заказ новый
        $client->jsonRequest("GET", "/api/v1/account/order-list/products?state=" . Order::STATE_NEW, [], [self::HEADER_AUTH => $token]);
        $this->assertResponseIsSuccessful();

        $resp = $this->getResp2Arr($client->getResponse());
        $this->assertCount(2, $resp[Params::PRODUCTS]);

        // Статус заказ выполнен
        $client->jsonRequest("GET", "/api/v1/account/order-list/products?state=" . Order::STATE_FINISHED, [], [self::HEADER_AUTH => $token]);
        $this->assertResponseIsSuccessful();

        $resp = $this->getResp2Arr($client->getResponse());
        $this->assertCount(2, $resp[Params::PRODUCTS]);

        // Статус заказ отменен
        $payer = static::getContainer()->get(PayerRepository::class)->find(1);
        $this->assertNotNull($payer);

        $client->jsonRequest("GET",
            sprintf("%s?state=%d&year=%d&payer=%s", "/api/v1/account/order-list/products", Order::STATE_CANCELED, 2022, $payer->getUid()),
            [],
            [self::HEADER_AUTH => $token]);
        $this->assertResponseIsSuccessful();

        $resp = $this->getResp2Arr($client->getResponse());
        $this->assertCount(2, $resp[Params::PRODUCTS]);

        // Статус заказ отменен с фильтрацией по году
        $client->jsonRequest("GET",
            sprintf("%s?%s=%d&%s=%d", "/api/v1/account/order-list/products", Params::STATE, Order::STATE_CANCELED, Params::YEAR, 2020),
            [],
            [self::HEADER_AUTH => $token]);
        $this->assertResponseStatusCodeSame(ApiController::STATUS_EMPTY);

        // Сортировка по категории
        $payer = static::getContainer()->get(PayerRepository::class)->find(1);
        $this->assertNotNull($payer);

        $client->jsonRequest("GET",
            sprintf("%s?category=%s", "/api/v1/account/order-list/products", '903c65f6-55e6-11e9-9e9a-ac1f6b855a52'),
            [],
            [self::HEADER_AUTH => $token]);
        $this->assertResponseIsSuccessful();

        $resp = $this->getResp2Arr($client->getResponse());
        $this->assertCount(2, $resp[Params::PRODUCTS]);
        $product = static::getContainer()->get(ProductRepository::class)->find(4);
        $this->assertEquals($product->getUuid(), $resp[Params::PRODUCTS][0]);

        // Неверный статус заказа
        $client->jsonRequest("GET", "/api/v1/account/order-list/products?state=100", [], [self::HEADER_AUTH => $token]);
        $this->assertResponseStatusCodeSame(ApiController::STATUS_BAD_REQUEST);

        $resp = $this->getResp2Arr($client->getResponse());
        $this->assertArrayHasKey(Params::CODE, $resp);
        $this->assertEquals(ApiError::ERROR_INVALID_DATA, $resp[Params::CODE]);
    }

    /**
     * @group front
     * @group account
     * @group orderDetail
     */
    public function testOrderDetail()
    {
        $client = static::createClient();
        $token = $this->getUserToken($client, "cart@test.com");

        /** @var Order $order */
        $order = static::getContainer()->get(OrderRepository::class)->find(2);
        $this->assertNotNull($order);

        $client->jsonRequest("GET", "/api/v1/account/orders/" . $order->getUid(), [], [self::HEADER_AUTH => $token]);
        $this->assertResponseIsSuccessful();

        $fields = [
            Params::CREATE_AT, Params::FIO, Params::EMAIL, Params::PHONE, Params::BUSSINESS_AREA,
            Params::SHIPPING_METHOD, Params::SHIPPING_DATE, Params::PAYMENT_METHOD, Params::PAYER, Params::SHIPPING_ADDRESS,
            Params::DISCOUNT, Params::SHIPPING_COST, Params::TOTAL_COST, Params::PRODUCTS, Params::NUMBER,
            Params::SUBSCRIBE_INTERVAL, Params::STATE
        ];

        $productFields = [Params::UUID, Params::QUANTITY, Params::SAMPLE, Params::TOTAL_COST, Params::PARENT];

        $resp = $this->getResp2Arr($client->getResponse());
        $this->matchObjectVars($fields, (object)$resp);
        $this->matchObjectVars($productFields, (object)$resp[Params::PRODUCTS][0]);

        //Нет искомого заказа
        $client->jsonRequest("GET", "/api/v1/account/orders/invalidUid", [], [self::HEADER_AUTH => $token]);
        $this->assertResponseStatusCodeSame(ApiController::STATUS_NOT_FOUND);
    }

    /**
     * @group front
     * @group account
     * @group waitingProducts
     */
    public function testWaitingProducts()
    {
        $client = static::createClient();
        $token = $this->getUserToken($client, "cart@test.com");

        $product = static::getContainer()->get(ProductRepository::class)->findOneByUuid('25c32eac-bdec-11e7-9b53-38607704c280');
        $this->assertNotNull($product);

        // Получение списка товаров "В ожидании"
        $client->jsonRequest("GET", "/api/v1/account/waiting-products", [], [self::HEADER_AUTH => $token]);
        $this->assertResponseIsSuccessful();

        $resp = $this->getResp2Arr($client->getResponse());
        $this->assertCount(1, $resp);
        $this->assertEquals($product->getUuid(), $resp[0]);

        // Невалидный токен для получения списков товаров
        $client->jsonRequest("GET", "/api/v1/account/waiting-products", [], [self::HEADER_AUTH => 'invalid token']);
        $this->assertResponseStatusCodeSame(ApiController::STATUS_UNAUTHORIZATION);

        // Удаление товара
        $client->jsonRequest("DELETE", "/api/v1/account/waiting-products", [Params::PRODUCT => $product->getUuid()], [self::HEADER_AUTH => $token]);
        $this->assertResponseIsSuccessful();

        $customer = static::getContainer()->get(AuthUserRepository::class)->findOneByEmail("cart@test.com");
        $this->assertNotNull($customer);
        $this->assertCount(0, static::getContainer()->get(WaitingProductRepository::class)->findBy(['product' => $product, 'customer' => $customer]));

        // Повторное удаление товара
        $client->jsonRequest("DELETE", "/api/v1/account/waiting-products", [Params::PRODUCT => $product->getUuid()], [self::HEADER_AUTH => $token]);
        $this->assertResponseIsSuccessful();

        // Невалидный токен для удаления товара
        $client->jsonRequest("DELETE", "/api/v1/account/waiting-products", [Params::PRODUCT => $product->getUuid()], [self::HEADER_AUTH => 'invalid token']);
        $this->assertResponseStatusCodeSame(ApiController::STATUS_UNAUTHORIZATION);
    }

    /**
     * @group front
     * @group account
     * @group subscribeOrder
     */
    public function testSubscribeOrder()
    {
        $client = static::createClient();
        $token = $this->getUserToken($client, "cart@test.com");

        /** @var Order $order2 */
        $order2 = static::getContainer()->get(OrderRepository::class)->find(2);

        // Подписка на заказ
        $client->jsonRequest("PUT", "/api/v1/account/orders/{$order2->getUid()}/subscribe",
            [Params::INTERVAL => Order::SUBSCRIBE_DAILY], [self::HEADER_AUTH => $token]);
        $this->assertResponseIsSuccessful();

        /** @var Order $order2 */
        $order2 = static::getContainer()->get(OrderRepository::class)->find(2);
        $this->assertNotNull($order2);
        $this->assertEquals(Order::SUBSCRIBE_DAILY, $order2->getSubscribeInterval());

        // Невалидный токен
        $client->jsonRequest("PUT", "/api/v1/account/orders/{$order2->getUid()}/subscribe", [], [self::HEADER_AUTH => 'invalid token']);
        $this->assertResponseStatusCodeSame(ApiController::STATUS_UNAUTHORIZATION);

        // Подписка на корзину
        /** @var Order $order1 */
        $order1 = static::getContainer()->get(OrderRepository::class)->find(1);
        $client->jsonRequest("PUT", "/api/v1/account/orders/{$order1->getUid()}/subscribe",
            [Params::INTERVAL => Order::SUBSCRIBE_DAILY], [self::HEADER_AUTH => $token]);
        $this->assertResponseStatusCodeSame(ApiController::STATUS_NOT_FOUND);

        $resp = $this->getResp2Arr($client->getResponse());
        $this->assertArrayHasKey(Params::CODE, $resp);
        $this->assertEquals(ApiError::ERROR_NOT_FOUND, $resp[Params::CODE]);

        // Отписка от заказа
        $client->jsonRequest("PUT", "/api/v1/account/orders/{$order2->getUid()}/unsubscribe",
            [], [self::HEADER_AUTH => $token]);
        $this->assertResponseIsSuccessful();

        /** @var Order $order2 */
        $order2 = static::getContainer()->get(OrderRepository::class)->find(2);
        $this->assertNotNull($order2);
        $this->assertNull($order2->getSubscribeInterval());
    }

    /**
     * @group front
     * @group account
     * @group updateFio
     */
    public function testUpdateFio()
    {
        $client = static::createClient();
        $token = $this->getUserToken($client, "cart@test.com");
        $newFio = "Сергеев Сергей Сергеевич";

        /** @var AuthUser $user */
        $user = static::getContainer()->get(AuthUserRepository::class)->findOneByEmail("cart@test.com");
        $this->assertNotNull($user);
        $this->assertEmpty($user->getFio());

        $client->jsonRequest("PUT", "/api/v1/account/update-fio", [Params::FIO => $newFio], [self::HEADER_AUTH => $token]);
        $this->assertResponseIsSuccessful();

        $user = static::getContainer()->get(AuthUserRepository::class)->findOneByEmail("cart@test.com");
        $this->assertNotNull($user);
        $this->assertEquals($newFio, $user->getFio());

        // Невалидный токен
        $client->jsonRequest("PUT", "/api/v1/account/update-fio", [Params::FIO => $newFio], [self::HEADER_AUTH => 'invalid token']);
        $this->assertResponseStatusCodeSame(ApiController::STATUS_UNAUTHORIZATION);
    }

    /**
     * @group front
     * @group account
     * @group recommendations
     */
    public function testRecommendationProducts()
    {
        $client = static::createClient();
        $token = $this->getUserToken($client, "cart@test.com");

        $product = static::getContainer()->get(ProductRepository::class)->findOneByUuid('25c32eac-bdec-11e7-9b53-38607704c280');
        $this->assertNotNull($product);

        // Получение списка товаров "В ожидании"
        $client->jsonRequest("GET", "/api/v1/account/recommendations", [], [self::HEADER_AUTH => $token]);
        $this->assertResponseIsSuccessful();

        $resp = $this->getResp2Arr($client->getResponse());
        $this->assertCount(1, $resp);
        $this->assertEquals($product->getUuid(), $resp[0]);

        // Невалидный токен для получения списков товаров
        $client->jsonRequest("GET", "/api/v1/account/recommendations", [], [self::HEADER_AUTH => 'invalid token']);
        $this->assertResponseStatusCodeSame(ApiController::STATUS_UNAUTHORIZATION);
    }
}