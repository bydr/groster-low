<?php

namespace App\Tests\Controller\Front;

use App\ApiError;
use App\Entity\Store;
use App\Repository\StoreRepository;
use App\Tests\Controller\CustomWebTestCase;

/**
 * Class ShopControllerTest
 */
class ShopControllerTest extends CustomWebTestCase
{
    private $adminToken = '';

    /**
     * @group front
     * @group shop
     */
    public function testAllShops()
    {
        $client = static::createClient();

        $activeShops = static::getContainer()->get(StoreRepository::class)->findAll('isActive = true');

        $client->jsonRequest("GET", "/api/v1/shops");
        $this->assertResponseIsSuccessful();

        $response = $client->getResponse();
        $this->assertTrue($response->headers->contains('Content-Type', 'application/json'), 'Invalid header Content-Type');
        $shops = $this->getResp2Arr($client->getResponse());
        $this->assertCount(count($activeShops), $shops, "Error to equal count shops");

        /** @var Store $shop */
        $shop = $activeShops[0];
        $expectedShop = [
            'uuid' => $shop->getUuid(),
            'address' => $shop->getAddress(),
            'schedule' => $shop->getSchedule(),
            'phone' => $shop->getPhone(),
            'coordinates' => [
                'latitude' => $shop->getLatitude(),
                'longitude' => $shop->getLongitude(),
            ]
        ];
        $this->matchObjectVars(array_keys($expectedShop), (object)$shops[0]);
        $this->assertEquals($expectedShop, $shops[0]);
    }

    /**
     * @group front
     * @group shop
     */
    public function testGetShopValid()
    {
        $client = static::createClient();

        $activeShops = static::getContainer()->get(StoreRepository::class)->findAll('isActive = true');
        /** @var Store $shop */
        $shop = $activeShops[0];
        $expectedShop = [
            'uuid' => $shop->getUuid(),
            'address' => $shop->getAddress(),
            'schedule' => $shop->getSchedule(),
            'phone' => $shop->getPhone(),
            'coordinates' => [
                'latitude' => $shop->getLatitude(),
                'longitude' => $shop->getLongitude(),
            ],
            'image' => $shop->getImage(),
            'email' => $shop->getEmail(),
            'description' => $shop->getDescription()
        ];

        $client->jsonRequest("GET", "/api/v1/shops/" . $shop->getUuid());
        $this->assertResponseIsSuccessful();

        $response = $client->getResponse();
        $this->assertTrue($response->headers->contains('Content-Type', 'application/json'), 'Invalid header Content-Type');
        $actualShop = $this->getResp2Arr($client->getResponse());

        $this->matchObjectVars(array_keys($expectedShop), (object)$actualShop);
        $this->assertEquals($expectedShop, $actualShop);
    }

    /**
     * @group front
     * @group shop
     */
    public function testGetShopInvalidUid()
    {
        $client = static::createClient();

        $client->jsonRequest("GET", "/api/v1/shops/invaliduid");
        $this->assertResponseStatusCodeSame(404);

        $result = $this->getResp2Arr($client->getResponse());

        $this->assertArrayHasKey("code", $result, "Error to get error code");
        $this->assertEquals(ApiError::ERROR_NOT_FOUND, $result['code'], "Error to get error code status");
    }

    /**
     * @group front
     * @group shop
     */
    public function testGetShopWrongMethod()
    {
        $client = static::createClient();

        $client->jsonRequest("POST", "/api/v1/shops");
        $this->assertResponseStatusCodeSame(405);
    }
}