<?php

namespace App\Tests\Controller\Front;

use App\ApiError;
use App\Controller\ApiController;
use App\Helpers\Params;
use App\Tests\Controller\CustomWebTestCase;

/**
 * Тесты для контроллера обратной связи
 */
class FeedbackControllerTest extends CustomWebTestCase
{
    /**
     * @group front
     * @group feedback
     */
    public function testFeedback()
    {
        $client = static::createClient();
        $data = [
            "name" => "Александр",
            "phone" => "+79889632541",
            "email" => "test@example.com",
            "message" => "У меня важный вопрос"
        ];

        $client->jsonRequest("POST", "/api/v1/feedback", $data);
        $this->assertResponseIsSuccessful();

        // Отправка без имени
        $data1 = $data;
        $data1['name'] = "";
        $client->jsonRequest("POST", "/api/v1/feedback", $data1);
        $this->assertResponseStatusCodeSame(ApiController::STATUS_BAD_REQUEST);

        $resp = $this->getResp2Arr($client->getResponse());
        $this->assertArrayHasKey(Params::CODE, $resp);
        $this->assertEquals(ApiError::ERROR_INVALID_JSON, $resp[Params::CODE]);

        // Отправка невалидного телефона
        $data2 = $data;
        $data2['phone'] = '987654123';
        $client->jsonRequest("POST", "/api/v1/feedback", $data2);
        $this->assertResponseStatusCodeSame(ApiController::STATUS_BAD_REQUEST);

        $resp = $this->getResp2Arr($client->getResponse());
        $this->assertArrayHasKey(Params::CODE, $resp);
        $this->assertEquals(ApiError::ERROR_INVALID_PHONE, $resp[Params::CODE]);

        // Отправка невалидного email
        $data3 = $data;
        $data3['email'] = 'invalid email';
        $client->jsonRequest("POST", "/api/v1/feedback", $data3);
        $this->assertResponseStatusCodeSame(ApiController::STATUS_BAD_REQUEST);

        $resp = $this->getResp2Arr($client->getResponse());
        $this->assertArrayHasKey(Params::CODE, $resp);
        $this->assertEquals(ApiError::ERROR_INVALID_EMAIL, $resp[Params::CODE]);

        // Отправка без сообщения
        $data['message'] = '';
        $client->jsonRequest("POST", "/api/v1/feedback", $data);
        $this->assertResponseStatusCodeSame(ApiController::STATUS_BAD_REQUEST);

        $resp = $this->getResp2Arr($client->getResponse());
        $this->assertArrayHasKey(Params::CODE, $resp);
        $this->assertEquals(ApiError::ERROR_INVALID_JSON, $resp[Params::CODE]);
    }

    /**
     * @group front
     * @group feedback
     */
    public function testRecall()
    {
        $client = static::createClient();
        $data = [
            "name" => "Александр",
            "phone" => "+79889632541",
        ];

        $client->jsonRequest("POST", "/api/v1/recall", $data);
        $this->assertResponseIsSuccessful();

        // Отправка без имени
        $data1 = $data;
        $data1['name'] = "";
        $client->jsonRequest("POST", "/api/v1/recall", $data1);
        $this->assertResponseStatusCodeSame(ApiController::STATUS_BAD_REQUEST);

        $resp = $this->getResp2Arr($client->getResponse());
        $this->assertArrayHasKey(Params::CODE, $resp);
        $this->assertEquals(ApiError::ERROR_INVALID_JSON, $resp[Params::CODE]);

        // Отправка невалидного телефона
        $data2 = $data;
        $data2['phone'] = '987654123';
        $client->jsonRequest("POST", "/api/v1/recall", $data2);
        $this->assertResponseStatusCodeSame(ApiController::STATUS_BAD_REQUEST);

        $resp = $this->getResp2Arr($client->getResponse());
        $this->assertArrayHasKey(Params::CODE, $resp);
        $this->assertEquals(ApiError::ERROR_INVALID_PHONE, $resp[Params::CODE]);
    }

    /**
     * @group front
     * @group feedback
     */
    public function testCustomEmail()
    {
        $client = static::createClient();
        $data = [
            "message" => "Нравится дизайн старого сайта, а новый непонятный",
        ];

        $client->jsonRequest("POST", "/api/v1/send-custom-email", $data);
        $this->assertResponseIsSuccessful();

        // Отправка без сообщения
        $data['message'] = "";
        $client->jsonRequest("POST", "/api/v1/send-custom-email", $data);
        $this->assertResponseStatusCodeSame(ApiController::STATUS_BAD_REQUEST);

        $resp = $this->getResp2Arr($client->getResponse());
        $this->assertArrayHasKey(Params::CODE, $resp);
        $this->assertEquals(ApiError::ERROR_INVALID_JSON, $resp[Params::CODE]);
    }


    /**
     * @group front
     * @group feedback
     * @group faq
     */
    public function testFaq()
    {
        $client = static::createClient();

        $client->jsonRequest("GET", "/api/v1/faq");
        $this->assertResponseIsSuccessful();

        $resp = $this->getResp2Arr($client->getResponse());

        $this->assertCount(1, $resp);
        $block = $resp[0];
        $this->assertArrayHasKey("name", $block);
        $this->assertArrayHasKey("questions", $block);

        $questions = $block['questions'];
        $this->assertCount(3, $questions);
        $this->matchObjectVars(['title', 'answer'], (object)$questions[0]);
    }

    /**
     * @group front
     * @group feedback
     * @group question
     */
    public function testQuestion()
    {
        $client = static::createClient();
        $data = [
            "name" => "Александр",
            "phone" => "+79889632541",
            "email" => "test@example.com",
            "message" => "У меня важный вопрос"
        ];

        $client->jsonRequest("POST", "/api/v1/question", $data);
        $this->assertResponseIsSuccessful();

        // Отправка без имени
        $data1 = $data;
        $data1['name'] = "";
        $client->jsonRequest("POST", "/api/v1/question", $data1);
        $this->assertResponseStatusCodeSame(ApiController::STATUS_BAD_REQUEST);

        $resp = $this->getResp2Arr($client->getResponse());
        $this->assertArrayHasKey(Params::CODE, $resp);
        $this->assertEquals(ApiError::ERROR_INVALID_JSON, $resp[Params::CODE]);

        // Отправка невалидного телефона
        $data2 = $data;
        $data2['phone'] = '987654123';
        $client->jsonRequest("POST", "/api/v1/question", $data2);
        $this->assertResponseStatusCodeSame(ApiController::STATUS_BAD_REQUEST);

        $resp = $this->getResp2Arr($client->getResponse());
        $this->assertArrayHasKey(Params::CODE, $resp);
        $this->assertEquals(ApiError::ERROR_INVALID_PHONE, $resp[Params::CODE]);

        // Отправка невалидного email
        $data3 = $data;
        $data3['email'] = 'invalid email';
        $client->jsonRequest("POST", "/api/v1/question", $data3);
        $this->assertResponseStatusCodeSame(ApiController::STATUS_BAD_REQUEST);

        $resp = $this->getResp2Arr($client->getResponse());
        $this->assertArrayHasKey(Params::CODE, $resp);
        $this->assertEquals(ApiError::ERROR_INVALID_EMAIL, $resp[Params::CODE]);

        // Отправка без сообщения
        $data['message'] = '';
        $client->jsonRequest("POST", "/api/v1/question", $data);
        $this->assertResponseStatusCodeSame(ApiController::STATUS_BAD_REQUEST);

        $resp = $this->getResp2Arr($client->getResponse());
        $this->assertArrayHasKey(Params::CODE, $resp);
        $this->assertEquals(ApiError::ERROR_INVALID_JSON, $resp[Params::CODE]);
    }

    /**
     * @group front
     * @group feedback
     * @group subscribe
     */
    public function testSubscribe()
    {
        $client = static::createClient();

        $data = [
            Params::EMAIL => 'test@example.com',
        ];

        // Валидный email
        $client->jsonRequest("POST", "/api/v1/subscribe", $data);
        $this->assertResponseIsSuccessful();

        // Невалидный email
        $data[Params::EMAIL] = 'invalid email';
        $client->jsonRequest("POST", "/api/v1/subscribe", $data);
        $this->assertResponseStatusCodeSame(ApiController::STATUS_BAD_REQUEST);

        $resp = $this->getResp2Arr($client->getResponse());
        $this->assertArrayHasKey(Params::CODE, $resp);
        $this->assertEquals(ApiError::ERROR_INVALID_EMAIL, $resp[Params::CODE]);
    }

    /**
     * @group front
     * @group feedback
     * @group unsubscribe
     */
    public function testUnsubscribe()
    {
        $client = static::createClient();

        $data = [
            Params::EMAIL => 'test@example.com',
        ];

        // Валидный email
        $client->jsonRequest("POST", "/api/v1/unsubscribe", $data);
        $this->assertResponseIsSuccessful();

        // Невалидный email
        $data[Params::EMAIL] = 'invalid email';
        $client->jsonRequest("POST", "/api/v1/unsubscribe", $data);
        $this->assertResponseStatusCodeSame(ApiController::STATUS_BAD_REQUEST);

        $resp = $this->getResp2Arr($client->getResponse());
        $this->assertArrayHasKey(Params::CODE, $resp);
        $this->assertEquals(ApiError::ERROR_INVALID_EMAIL, $resp[Params::CODE]);
    }

    /**
     * @group front
     * @group feedback
     */
    public function testFoundCheaper()
    {
        $client = static::createClient();
        $data = [
            "phone" => "+79889632541",
            "link" => 'https://grosternew.ru',
            "message" => 'Нашел товар 1 дешевле на указанном сайте. Сделайте скидку'
        ];

        $client->jsonRequest("POST", "/api/v1/found-cheaper", $data);
        $this->assertResponseIsSuccessful();

        // Отправка невалидного телефона
        $data2 = $data;
        $data2['phone'] = '987654123';
        $client->jsonRequest("POST", "/api/v1/found-cheaper", $data2);
        $this->assertResponseStatusCodeSame(ApiController::STATUS_BAD_REQUEST);

        $resp = $this->getResp2Arr($client->getResponse());
        $this->assertArrayHasKey(Params::CODE, $resp);
        $this->assertEquals(ApiError::ERROR_INVALID_PHONE, $resp[Params::CODE]);
    }

    /**
     * @group front
     * @group feedback
     */
    public function testNotFoundNeeded()
    {
        $client = static::createClient();
        $data = [
            "phone" => "+79889632541",
            "message" => 'Не нашел товар 1 на вашем сайте'
        ];

        $client->jsonRequest("POST", "/api/v1/not-found-needed", $data);
        $this->assertResponseIsSuccessful();

        // Отправка невалидного телефона
        $data2 = $data;
        $data2['phone'] = '987654123';
        $client->jsonRequest("POST", "/api/v1/not-found-needed", $data2);
        $this->assertResponseStatusCodeSame(ApiController::STATUS_BAD_REQUEST);

        $resp = $this->getResp2Arr($client->getResponse());
        $this->assertArrayHasKey(Params::CODE, $resp);
        $this->assertEquals(ApiError::ERROR_INVALID_PHONE, $resp[Params::CODE]);
    }
}