<?php

namespace App\Tests\Controller\Front;

use App\ApiError;
use App\Controller\ApiController;
use App\Entity\Product;
use App\Helpers\Params;
use App\Repository\FavoriteRepository;
use App\Repository\ProductRepository;
use App\Tests\Controller\CustomWebTestCase;

/**
 * Class FavoriteControllerTest
 */
class FavoriteControllerTest extends CustomWebTestCase
{
    private const FAIL_TOKEN = 'fail token';
    private const FAIL_UUID = 'fail uuid';

    /**
     * @group front
     * @group favorite
     * @group add
     */
    public function testAddProductsSuccess(): void
    {
        $client = static::createClient();

        $testUser = $this->getTestUser();
        $token = $this->getUserToken($client, $testUser->getEmail());
        $product = $this->getProduct();

        $client->jsonRequest("POST", "/api/v1/favorites/add", [Params::PRODUCTS => $product->getUuid()], [self::HEADER_AUTH => $token]);
        $this->assertResponseStatusCodeSame(ApiController::STATUS_SUCCESS_SAVE, "Error to get status code");
        $favorite = static::getContainer()->get(FavoriteRepository::class)->findOneBy(['shopUser' => $testUser, 'product' => $product]);
        $this->assertNotEmpty($favorite);
    }

    /**
     * @group front
     * @group favorite
     * @group add
     */
    public function testAddProductsAccessDenied()
    {
        $client = static::createClient();

        $product = $this->getProduct();

        $client->jsonRequest("POST", "/api/v1/favorites/add", [Params::PRODUCTS => $product->getUuid()], [self::HEADER_AUTH => self::FAIL_TOKEN]);
        $this->assertResponseStatusCodeSame(ApiController::STATUS_UNAUTHORIZATION, 'Fail status code');
    }

    /**
     * @group front
     * @group favorite
     * @group add
     */
    public function testAddProductsInvalidUuid()
    {
        $client = static::createClient();

        $testUser = $this->getTestUser();
        $token = $this->getUserToken($client, $testUser->getEmail());

        $client->jsonRequest("POST", "/api/v1/favorites/add", [Params::PRODUCTS => self::FAIL_UUID], [self::HEADER_AUTH => $token]);
        $this->assertResponseStatusCodeSame(ApiController::STATUS_BAD_REQUEST, 'Fail status code');
        $resp = $this->getResp2Arr($client->getResponse());

        $this->assertArrayHasKey(Params::CODE, $resp);
        $this->assertEquals(ApiError::ERROR_INVALID_PRODUCT, $resp[Params::CODE]);
    }

    /**
     * @group front
     * @group favorite
     * @group delete
     */
    public function testDeleteProductSuccess(): void
    {
        $client = static::createClient();

        $testUser = $this->getTestUser();
        $token = $this->getUserToken($client, $testUser->getEmail());
        $product = $this->getProduct();

        $client->jsonRequest("POST", "/api/v1/favorites/add", [Params::PRODUCTS => $product->getUuid()], [self::HEADER_AUTH => $token]);
        $this->assertResponseStatusCodeSame(ApiController::STATUS_SUCCESS_SAVE, "Error to get status code");

        $client->jsonRequest("DELETE", "/api/v1/favorites/delete", [Params::PRODUCTS => $product->getUuid()], [self::HEADER_AUTH => $token]);
        $this->assertResponseStatusCodeSame(ApiController::STATUS_OK, "Error to get status code");
        $favorite = static::getContainer()->get(FavoriteRepository::class)->findOneBy(['shopUser' => $testUser, 'product' => $product]);
        $this->assertNotEmpty($favorite);
    }

    /**
     * @group front
     * @group favorite
     * @group delete
     */
    public function testDeleteProductAccessDenied()
    {
        $client = static::createClient();

        $token = $this->getUserToken($client);
        $product = $this->getProduct();

        $client->jsonRequest("POST", "/api/v1/favorites/add", [Params::PRODUCTS => $product->getUuid()], [self::HEADER_AUTH => $token]);
        $this->assertResponseStatusCodeSame(ApiController::STATUS_SUCCESS_SAVE, "Error to get status code");

        $client->jsonRequest("DELETE", "/api/v1/favorites/delete", [Params::PRODUCTS => $product->getUuid()], [self::HEADER_AUTH => self::FAIL_TOKEN]);
        $this->assertResponseStatusCodeSame(ApiController::STATUS_UNAUTHORIZATION, 'Fail status code');
    }

    /**
     * @group front
     * @group favorite
     * @group delete
     */
    public function testDeleteProductInvalidUuid()
    {
        $client = static::createClient();

        $token = $this->getUserToken($client);
        $product = $this->getProduct();

        $client->jsonRequest("POST", "/api/v1/favorites/add", [Params::PRODUCTS => $product->getUuid()], [self::HEADER_AUTH => $token]);
        $this->assertResponseStatusCodeSame(ApiController::STATUS_SUCCESS_SAVE, "Error to get status code");

        $client->jsonRequest("DELETE", "/api/v1/favorites/delete", [Params::PRODUCTS => self::FAIL_UUID], [self::HEADER_AUTH => $token]);
        $this->assertResponseIsSuccessful();
    }

    /**
     * @group front
     * @group favorite
     * @group get
     */
    public function testGetAllProductSuccess(): void
    {
        $client = static::createClient();

        $token = $this->getUserToken($client);
        $product = $this->getProduct();

        $client->jsonRequest("POST", "/api/v1/favorites/add", [Params::PRODUCTS => $product->getUuid()], [self::HEADER_AUTH => $token]);
        $this->assertResponseStatusCodeSame(ApiController::STATUS_SUCCESS_SAVE, "Error to get status code");

        $client->jsonRequest("GET", "/api/v1/favorites", [], ['HTTP_AUTHORIZATION' => $token]);
        $this->assertResponseIsSuccessful("Error to get status code");
        $resp = $this->getResp2Arr($client->getResponse());

        $this->assertArrayHasKey(Params::PRODUCTS, $resp);
        $this->assertEquals($product->getUuid(), $resp[Params::PRODUCTS][0]);
    }

    /**
     * @group front
     * @group favorite
     * @group get
     */
    public function testGetAllProductEmpty(): void
    {
        $client = static::createClient();

        $token = $this->getUserToken($client);

        $client->jsonRequest("GET", "/api/v1/favorites", [], [self::HEADER_AUTH => $token]);
        $this->assertResponseStatusCodeSame(ApiController::STATUS_EMPTY, 'Fail status code');
    }

    /**
     * @group front
     * @group favorite
     * @group get
     */
    public function testGetAllProductAccessDenied(): void
    {
        $client = static::createClient();

        $client->jsonRequest("GET", "/api/v1/favorites", [], [self::HEADER_AUTH => self::FAIL_TOKEN]);
        $this->assertResponseStatusCodeSame(ApiController::STATUS_UNAUTHORIZATION, 'Fail status code');
    }

    /**
     * Возвращает товар
     * @return Product
     */
    private function getProduct(): Product
    {
        $productRepository = static::getContainer()->get(ProductRepository::class);
        $product = $productRepository->find(1);
        $this->assertNotEmpty($product->getUuid(), "Empty uuid for product");

        return $product;
    }

}