<?php

namespace App\Tests\Controller;

use App\DataFixtures\SecurityFixtures;
use App\Entity\Security\AuthUser;
use App\Helpers\Params;
use App\Kernel;
use App\Repository\AuthUserRepository;
use App\Repository\ProductRepository;
use Exception;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class CustomWebTestCase
 */
class CustomWebTestCase extends WebTestCase
{
    const HEADER_AUTH = "HTTP_AUTHORIZATION";

    protected $logger;
    private $adminToken = '';
    private $customerToken = '';


    /**
     * Логгер для тестов
     * @throws Exception
     */
    private function getLogger(): Logger
    {
        if(empty($this->logger)) {
            $logger = new Logger('poster');
            $logDir = dirname(__DIR__, 2) . '/var/log/tests/';
            if(!is_dir($logDir)) {
                mkdir($logDir, 0777, true);
            }
            $logger->pushHandler(new StreamHandler($logDir . DIRECTORY_SEPARATOR . 'all.log', Logger::INFO));

            $this->logger = $logger;
        }

        return $this->logger;

    }

    /**
     * Проверка, что api возвращает те же поля, что и указаны в контрактах
     * @param array $expected
     * @param object $actual
     */
    protected function matchObjectVars(array $expected, object $actual, string $name = '')
    {
        foreach ($expected as $field) {
            $this->assertObjectHasAttribute($field, $actual, sprintf("%s: Object does not have field %s", $name, $field));
        }
    }

    /**
     * @return string
     */
    protected static function getKernelClass(): string
    {
        return Kernel::class;
    }

    /**
     * Получение токена админа
     * @param KernelBrowser $client
     * @return string
     */
    protected function getAdminToken(KernelBrowser $client): string
    {
        if(empty($this->adminToken)) {
            $client->jsonRequest("POST", "/api/v1/auth/login_email", [
                'email' => SecurityFixtures::ADMIN_EMAIL,
                'password' => SecurityFixtures::ADMIN_PASSWORD
            ]);

            $this->assertResponseIsSuccessful();
            $resp = $this->getResp2Arr($client->getResponse());
            $token = $resp[Params::ACCESS_TOKEN];
            $this->assertNotEmpty($token, 'Error to empty token');

            $this->adminToken = $token;
        }

        return $this->adminToken;
    }

    /**
     * Возвращает токен авторизации покупателя
     * @param KernelBrowser $client
     * @param string $testEmail
     * @return string
     */
    protected function getUserToken(KernelBrowser $client, string $testEmail = ""): string
    {
        if(empty($this->customerToken)){
            if (empty($testEmail)) {
                $testUser = $this->getTestUser();
                $testEmail = $testUser->getEmail();
            }
            $client->jsonRequest('POST', '/api/v1/auth/login_email', ["email" => $testEmail, "password" => "password1"]);
            $this->assertResponseIsSuccessful();
            $resp = $this->getResp2Arr($client->getResponse());
            $this->assertArrayHasKey(Params::ACCESS_TOKEN, $resp);
            $this->assertArrayHasKey(Params::REFRESH_TOKEN, $resp);

            $this->customerToken = $resp[Params::ACCESS_TOKEN];
        }


        return $this->customerToken;
    }

    /**
     * Получение действующего пользователя
     * @return AuthUser
     */
    protected function getTestUser(): AuthUser
    {
        $testEmail = "qwe1@test.com";
        $userRepository = static::getContainer()->get(AuthUserRepository::class);
        $testUser = $userRepository->findOneByEmail($testEmail);
        self::assertFalse($testUser->IsLogin(), "Email $testEmail is logged");

        return $testUser;
    }

    public function getCartUid($client, $productIds = [])
    {
        $products[] = static::getContainer()->get(ProductRepository::class)->find(3);
        if(!empty($productIds)) {
            $products = array_merge($products, static::getContainer()->get(ProductRepository::class)->findBy(['id' => $productIds]));
        }

        $cartUid = null;
        foreach ($products as $product) {
            $data = [
                Params::PRODUCT => $product->getUuid(),
                Params::QUANTITY => 1
            ];

            if(!empty($cartUid)) {
                $data[Params::CART] = $cartUid;
            }

            $client->jsonRequest('POST', '/api/v1/cart/change-qty', $data);

            $this->assertResponseIsSuccessful();
            $respCart = $this->getResp2Arr($client->getResponse());
            $this->assertArrayHasKey(Params::CART, $respCart);
            $this->assertNotEmpty($respCart[Params::CART], "Error to get empty for cart");
            $cartUid = $respCart[Params::CART];
        }

        return $cartUid;
    }

    /**
     * Декодированный ответ сервера
     * @param Response $response
     * @param bool $isArray
     * @return array|object
     */
    protected function getResp2Arr(Response $response, bool $isArray = true)
    {
        $this->assertTrue($response->headers->contains('Content-Type', 'application/json'), 'Invalid header Content-Type');
        $content = $response->getContent();
        $this->assertJson($content, 'Content is not json');

        return json_decode($content, $isArray);
    }
}