<?php

namespace App\Tests\Controller\Server1C;

use App\ApiError;
use App\Controller\ApiController;
use App\Entity\Discount;
use App\Entity\Order;
use App\Entity\OrderItem;
use App\Entity\Product;
use App\Helpers\Params;
use App\Repository\CategoryRepository;
use App\Repository\DiscountRepository;
use App\Repository\OrderRepository;
use App\Repository\ProductRepository;
use App\Tests\Controller\CustomWebTestCase;
use DateTime;
use DateTimeInterface;

class ServerControllerTest extends CustomWebTestCase
{
    /**
     * @group api
     * @group product
     * @group updateRest
     */
    public function testUpdateRest()
    {
        $client = static::createClient();
        $apiKey = static::getContainer()->getParameter('api_key_1c');
        $product1Id = 1;
        $product2Id = 2;

        /** @var Product $product1 */
        $product1 = static::getContainer()->get(ProductRepository::class)->find($product1Id);
        $this->assertNotNull($product1);

        /** @var Product $product2 */
        $product2 = static::getContainer()->get(ProductRepository::class)->find($product2Id);
        $this->assertNotNull($product2);

        $data[Params::PRODUCTS] = [
            [
                Params::ID => $product1->getUuid(),
                Params::IS_DELETE => true,
                Params::RESTS => [
                    [
                        Params::STORE => $product1->getStores()[0],
                        Params::VALUE => 20
                    ]
                ]
            ],
            [
                Params::ID => $product2->getUuid(),
                Params::IS_DELETE => false,
                Params::RESTS => [
                    [
                        Params::STORE => $product2->getStores()[0],
                        Params::VALUE => 30
                    ]
                ]
            ],
        ];

        $client->jsonRequest("PUT", "/api/v1/product/rest-update?api_key=" . $apiKey, $data);
        $this->assertResponseIsSuccessful();

        /** @var Product $product1 */
        $product1 = static::getContainer()->get(ProductRepository::class)->find($product1Id);
        $this->assertNotNull($product1);
        $this->assertFalse($product1->getIsActive());

        foreach ($product1->getRests() as $rest) {
            if($rest->getStore()->getUuid() != $data[Params::PRODUCTS][0][Params::RESTS][0][Params::STORE]) {
                continue;
            }

            $this->assertEquals($data[Params::PRODUCTS][0][Params::RESTS][0][Params::VALUE], $rest->getValue());
        }

        /** @var Product $product2 */
        $product2 = static::getContainer()->get(ProductRepository::class)->find($product2Id);
        $this->assertNotNull($product2);
        $this->assertTrue($product2->getIsActive());

        foreach ($product2->getRests() as $rest) {
            if($rest->getStore()->getUuid() != $data[Params::PRODUCTS][1][Params::RESTS][0][Params::STORE]) {
                continue;
            }

            $this->assertEquals($data[Params::PRODUCTS][1][Params::RESTS][0][Params::VALUE], $rest->getValue());
        }

        //Неверный api_key
        $client->jsonRequest("PUT", "/api/v1/product/rest-update?api_key=invalidKey", $data);
        self::assertResponseStatusCodeSame(ApiController::STATUS_UNAUTHORIZATION);

        //Отрицательное значение для остатка
        $data[Params::PRODUCTS][0][Params::RESTS][0][Params::VALUE] = -5;
        $client->jsonRequest("PUT", "/api/v1/product/rest-update?api_key=" . $apiKey, $data);
        $this->assertResponseIsSuccessful();

        /** @var Product $product1 */
        $product1 = static::getContainer()->get(ProductRepository::class)->find($product1Id);
        $this->assertNotNull($product1);
        $this->assertFalse($product1->getIsActive());

        foreach ($product1->getRests() as $rest) {
            if($rest->getStore()->getUuid() != $data[Params::PRODUCTS][0][Params::RESTS][0][Params::STORE]) {
                continue;
            }

            $this->assertEquals(0, $rest->getValue());
        }

        //не указан идентификатор товара
        $data[Params::PRODUCTS][0][Params::ID] = '';
        $client->jsonRequest("PUT", "/api/v1/product/rest-update?api_key=" . $apiKey, $data);
        $this->assertResponseStatusCodeSame(ApiController::STATUS_BAD_REQUEST);

        $resp = $this->getResp2Arr($client->getResponse());
        $this->assertArrayHasKey(Params::CODE, $resp);
    }

    /**
     * @group api
     * @group order
     * @group change-status
     */
    public function testChangeStatusToProcessing()
    {
        $client = static::createClient();
        $apiKey = static::getContainer()->getParameter('api_key_1c');

        $order = static::getContainer()->get(OrderRepository::class)->findOneBy(['id' => 2]);
        $this->assertNotNull($order);
        $this->assertEquals(Order::STATE_NEW, $order->getState());

        //Неверный uid
        $client->jsonRequest("PUT", sprintf("/api/v1/order/change-status/%s?%s=%s&state=%d",
            "invalidUid",
            Params::API_KEY,
            $apiKey,
            Order::STATE_FINISHED));
        self::assertResponseStatusCodeSame(ApiController::STATUS_NOT_FOUND);
        $resp = $this->getResp2Arr($client->getResponse());
        self::assertArrayHasKey(Params::CODE, $resp);
        self::assertEquals(ApiError::ERROR_NOT_FOUND, $resp[Params::CODE]);


        // В обработке
        $url = sprintf("/api/v1/order/change-status/%s?api_key=%s&state=%d",
            $order->getUid(), $apiKey, Order::STATE_PROCESSING);
        $client->jsonRequest("PUT", $url);
        $this->assertResponseIsSuccessful();
        $order = static::getContainer()->get(OrderRepository::class)->findOneBy(['id' => 2]);
        $this->assertNotNull($order);
        $this->assertEquals(Order::STATE_PROCESSING, $order->getState());

        // Выполнен
        $url = sprintf("/api/v1/order/change-status/%s?api_key=%s&state=%d",
            $order->getUid(), $apiKey, Order::STATE_FINISHED);
        $client->jsonRequest("PUT", $url);
        $this->assertResponseIsSuccessful();
        $order = static::getContainer()->get(OrderRepository::class)->findOneBy(['id' => 2]);
        $this->assertNotNull($order);
        $this->assertEquals(Order::STATE_FINISHED, $order->getState());

        // Невалидный ключ доступа
        $url = sprintf("/api/v1/order/change-status/%s?api_key=%s&state=%d",
            $order->getUid(), 'invalid_api_key', Order::STATE_PROCESSING);
        $client->jsonRequest("PUT", $url);
        $this->assertResponseStatusCodeSame(ApiController::STATUS_UNAUTHORIZATION);
        $order = static::getContainer()->get(OrderRepository::class)->findOneBy(['id' => 2]);
        $this->assertNotNull($order);
        $this->assertEquals(Order::STATE_FINISHED, $order->getState());
    }

    /**
     * @group api
     * @group order
     * @group change-status
     */
    public function testChangeStatus()
    {
        $client = static::createClient();
        $apiKey = static::getContainer()->getParameter('api_key_1c');

        $order = static::getContainer()->get(OrderRepository::class)->findOneBy(['id' => 2]);
        $this->assertNotNull($order);
        $this->assertEquals(Order::STATE_NEW, $order->getState());

        // Неверный статус заказа
        $url = sprintf("/api/v1/order/change-status/%s?api_key=%s&state=%d",
            $order->getUid(), $apiKey, 0);
        $client->jsonRequest("PUT", $url);
        $this->assertResponseStatusCodeSame(ApiController::STATUS_BAD_REQUEST);
        $resp = $this->getResp2Arr($client->getResponse());
        $this->assertArrayHasKey(Params::CODE, $resp);
        $this->assertEquals(ApiError::ERROR_INVALID_ORDER_STATE, $resp[Params::CODE]);

        // Выполнен
        $url = sprintf("/api/v1/order/change-status/%s?api_key=%s&state=%d",
            $order->getUid(), $apiKey, Order::STATE_FINISHED);
        $client->jsonRequest("PUT", $url);
        $this->assertResponseIsSuccessful();
        $order = static::getContainer()->get(OrderRepository::class)->findOneBy(['id' => 2]);
        $this->assertNotNull($order);
        $this->assertEquals(Order::STATE_FINISHED, $order->getState());

        // Отменен после выполнен
        $url = sprintf("/api/v1/order/change-status/%s?api_key=%s&state=%d",
            $order->getUid(), $apiKey, Order::STATE_CANCELED);
        $client->jsonRequest("PUT", $url);
        $this->assertResponseStatusCodeSame(ApiController::STATUS_BAD_REQUEST);
        $resp = $this->getResp2Arr($client->getResponse());
        $this->assertArrayHasKey(Params::CODE, $resp);
        $this->assertEquals(ApiError::ERROR_INVALID_ORDER_STATE, $resp[Params::CODE]);
        $order = static::getContainer()->get(OrderRepository::class)->findOneBy(['id' => 2]);
        $this->assertNotNull($order);
        $this->assertEquals(Order::STATE_FINISHED, $order->getState());

    }

    /**
     * @group api
     * @group order
     * @group change-items
     */
    public function testChangeItems()
    {
        $client = static::createClient();
        $apiKey = static::getContainer()->getParameter('api_key_1c');

        /** @var Order $order */
        $order = static::getContainer()->get(OrderRepository::class)->findOneBy(['id' => 2]);
        $this->assertNotNull($order);
        $this->assertEquals(Order::STATE_NEW, $order->getState());

        // Переносим в статус "В обработке"
        $url = sprintf("/api/v1/order/change-status/%s?api_key=%s&state=%d",
            $order->getUid(), $apiKey, Order::STATE_PROCESSING);
        $client->jsonRequest("PUT", $url);
        $this->assertResponseIsSuccessful();
        /** @var Order $order */
        $order = static::getContainer()->get(OrderRepository::class)->findOneBy(['id' => 2]);
        $this->assertNotNull($order);
        $this->assertEquals(Order::STATE_PROCESSING, $order->getState());

        $updates = [];
        $total = 0;
        /** @var OrderItem $item */
        foreach ($order->getItems() as $item) {
            $qty = $item->getQuantity() + $item->getSample() + 5;
            $price = $item->getPrice();
            $updates['Products'][$item->getProduct()->getId()]['IDProduct'] = $item->getProduct()->getUuid();
            $updates['Products'][$item->getProduct()->getId()]['IDKitProduct'] = $item->getParentUuid();
            $updates['Products'][$item->getProduct()->getId()]['CountProduct'] = $qty;
            $updates['Products'][$item->getProduct()->getId()]['PriceProduct'] = $price;
            $updates['Products'][$item->getProduct()->getId()]['SumDiscount'] = 0;
            $updates['Products'][$item->getProduct()->getId()]['ThisProductSample'] = $item->getSample() > 0;
            $total += $price * $qty;
        }

        $url = sprintf("/api/v1/order/change-products/%s?api_key=%s", $order->getUid(), $apiKey);
        $client->jsonRequest("PUT", $url, $updates);
        $this->assertResponseIsSuccessful();
        /** @var Order $order */
        $order = static::getContainer()->get(OrderRepository::class)->findOneBy(['id' => 2]);
        $this->assertNotNull($order);
        $this->assertEquals(Order::STATE_PROCESSING, $order->getState());
        $this->assertEquals($total, $order->getItemsTotal());
    }

    /**
     * @group api
     * @group discount
     * @group get
     */
    public function testGetPromo()
    {
        $client = static::createClient();
        $apiKey = static::getContainer()->getParameter('api_key_1c');
        $discounts = static::getContainer()->get(DiscountRepository::class)->findAll();
        /** @var Discount $discount */
        $discount = $discounts[0];

        $url = sprintf("/api/v1/server/promocode/%s?api_key=%s",
            $discount->getCode(), $apiKey);
        $client->jsonRequest('GET', $url);
        $this->assertResponseIsSuccessful();

        $resp = $this->getResp2Arr($client->getResponse());
        $this->assertEquals($discount->getName(), $resp[Params::NAME]);
        $this->assertEquals($discount->getCode(), $resp[Params::CODE]);
        $this->assertEquals($discount->getType(), $resp[Params::TYPE]);
        $this->assertEquals($discount->getValue(), $resp[Params::VALUE]);
        $this->assertEquals($discount->getReusable(), $resp[Params::REUSABLE]);
        $this->assertEquals($discount->getStart()->format(DateTimeInterface::ISO8601), $resp[Params::START]);
        $this->assertEquals($discount->getFinish()->format(DateTimeInterface::ISO8601), $resp[Params::FINISH]);
        $this->assertGreaterThan(0, count($resp[Params::CATEGORIES]));
        $this->assertEquals($discount->getCategoriesUuid(), $resp[Params::CATEGORIES]);
    }

    /**
     * @group api
     * @group discount
     * @group create
     */
    public function testCreatePromo()
    {
        $client = static::createClient();
        $apiKey = static::getContainer()->getParameter('api_key_1c');

        $url = sprintf("/api/v1/server/promocode/add?api_key=%s", $apiKey);
        $category = static::getContainer()->get(CategoryRepository::class)->find(199);

        $newPromo = [
            "name" => "new test promo",
            "code" => "newtestpromo",
            "type" => Discount::TYPE_RATE,
            "value" => 10,
            "reusable" => true,
            "start" => (new DateTime())->format(DateTimeInterface::ISO8601),
            "finish" => (new DateTime())->modify("+1 hour")->format(DateTimeInterface::ISO8601),
            "categories" => $category->getUuid(),
        ];


        $client->jsonRequest('POST', $url, $newPromo);
        $this->assertResponseIsSuccessful();
        $result = $this->getResp2Arr($client->getResponse());

        $this->assertResponseStatusCodeSame(200);
    }

    /**
     * @group api
     * @group discount
     * @group update
     */
    public function testUpdatePromo()
    {
        $client = static::createClient();
        $apiKey = static::getContainer()->getParameter('api_key_1c');

        $url = sprintf("/api/v1/server/promocode/expired?api_key=%s", $apiKey);
        $category = static::getContainer()->get(CategoryRepository::class)->find(199);

        $updatePromo = [
            "name" => "update test promo",
            "code" => "newtestpromo",
            "type" => Discount::TYPE_RATE,
            "value" => 15,
            "reusable" => true,
            "start" => (new DateTime())->format(DateTimeInterface::ISO8601),
            "finish" => (new DateTime())->modify("+1 hour")->format(DateTimeInterface::ISO8601),
            "categories" => $category->getUuid(),
        ];


        $client->jsonRequest('PUT', $url, $updatePromo);
        $this->assertResponseIsSuccessful();
        $promo = static::getContainer()->get(DiscountRepository::class)->findOneByCode($updatePromo['code']);
        $this->assertNotNull($promo);

        $this->assertEquals($updatePromo['name'], $promo->getName());
        $this->assertEquals($updatePromo['value'], $promo->getValue());
    }
}