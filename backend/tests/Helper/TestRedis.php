<?php

namespace App\Tests\Helper;

/**
 * Класс-заглушка для тестов
 */
class TestRedis
{
    public function set($key, $value, $lifetime)
    {
        return true;
    }

    public function get($key)
    {
        return 1111;
    }
}