<?php

namespace App\Tests\Unit;

use App\Helpers\ValidateHelper;
use PHPUnit\Framework\TestCase;

/**
 * Тест валидаций
 */
class ValidateHelperTest extends TestCase
{
    public function testValidateEmail()
    {
        $testCases = [
            ['name' => 'Valid', 'isValid' => true, 'email' => 'ingling@mail.ru',],
            ['name' => 'с точкой внутри', 'isValid' => true, 'email' => 'ing.ling@mail.ru',],
            ['name' => 'с % внутри: ing%ling@mail.ru', 'isValid' => true, 'email' => 'ing%ling@mail.ru',],
            ['name' => 'Invalid', 'isValid' => false, 'email' => '0231254145',],
            ['name' => 'Внутри несколько @ подряд', 'isValid' => false, 'email' => 'ing.ling@@@@mail.ru',],
            ['name' => 'Внутри пробел', 'isValid' => false, 'email' => 'ingling @mail.ru',],
            ['name' => 'Кириллица внутри', 'isValid' => false, 'email' => 'рукака@mail.ru',],
            ['name' => '2 почты через запятую', 'isValid' => false, 'email' => 'ingling@mail.ru,second@mail.ru',]
        ];

        foreach ($testCases as $tc) {
            if($tc['isValid']) {
                $this->assertTrue(ValidateHelper::validateEmail($tc['email']), "Failed to validate {$tc['email']}");
            }else{
                $this->assertFalse(ValidateHelper::validateEmail($tc['email']), "Failed to validate {$tc['email']}");
            }
        }
    }

    public function testValidatePhone()
    {
        $testCases = [
            ['name' => 'Телефон российский: "+73652669293"', 'isValid' => true, 'phone' => '+73652669293',],
            ['name' => 'Телефон турецкий: "+902122903070"', 'isValid' => false, 'phone' => '+902122903070',],
        ];

        foreach ($testCases as $tc) {
            if($tc['isValid']) {
                $this->assertNotNull(ValidateHelper::getValidPhone($tc['phone']), "Failed to validate {$tc['phone']}");
            }else{
                $this->assertNull(ValidateHelper::getValidPhone($tc['phone']), "Failed to validate {$tc['phone']}");
            }
        }
    }
}