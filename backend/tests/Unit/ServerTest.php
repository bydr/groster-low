<?php

namespace App\Tests\Unit;

use App\Entity\Discount;
use App\Entity\Order;
use App\Entity\OrderItem;
use App\Entity\Param;
use App\Entity\PaymentMethod;
use App\Entity\Product;
use App\Entity\Security\AuthUser;
use App\Helpers\Params;
use App\Helpers\SerializerHelper;
use App\Helpers\ValidateHelper;
use App\Service\Server\Order as ServerOrder;
use App\Service\Server\User as ServerUser;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Faker\Generator;
use Http\Message\Formatter;
use PHPUnit\Framework\TestCase;

/**
 * Тест валидации заказа для 1С
 */
class ServerTest extends TestCase
{
    /**
     * @group server
     */
    public function testOrder()
    {
        $customer = $this->getCustomer();
        $item = $this->getOrderItem();
        $paymentMethod = new PaymentMethod();
        $paymentMethod->setUid(PaymentMethod::UID_BANK);
        $discount = $this->getDiscount();

        $order = new Order();
        $order->setId(0)->setCustomer($customer)->setState(Order::STATE_NEW)->setCheckoutCompleteAt(new \DateTime())
            ->addItem($item)->setPaymentMethod($paymentMethod)->setBussinessArea("тестовая сфера бизнеса")
            ->setDiscount($discount)
        ;

        $em = $this->createMock(EntityManager::class);
        $serverOrder = new ServerOrder($em, $order);

        $res = SerializerHelper::serialize($serverOrder, [SerializerHelper::GROUP_REMOTE_ORDER]);
        $decodeRes = json_decode($res, true);

        // Общие данные заказа
        $this->assertEquals($order->getId(), $decodeRes['NumberOrder']);
        $this->assertEquals($order->getUid(), $decodeRes['IDOrder']);
        $this->assertEquals($order->getCreateAt()->format('c'), $decodeRes['OrderDate']);
        if($order->getShippingDate()) {
            $this->assertEquals($order->getShippingDate()->format('c'), $decodeRes['OrderShipmentDate']);
        }
        $this->assertEquals($order->getCheckoutCompleteAt()->format(DATE_ATOM), $decodeRes['OrderDate']);

        $this->assertEquals($order->getFio(), $decodeRes['NameContactOrder']);
        $this->assertEquals($order->getPhone(), $decodeRes['ViewTelContactOrder']);
        $this->assertEquals($order->getEmail(), $decodeRes['ViewEmailContactOrder']);
        $this->assertEquals($order->getComment(), $decodeRes['OrderComment']);
        $this->assertEquals($order->getShippingMethodName(), $decodeRes['TypeDelivery']);
        $this->assertEquals($order->getDiscount()->getCode(), $decodeRes['Promocodes'][0]);
        $this->assertEquals($order->getBussinessArea(), $decodeRes['BusinessAreasOrder'][0]['NameBusinessArea']);

        if($order->getShippingCost()) {
            $this->assertFalse($decodeRes['FreeShipping']);
        } else {
            $this->assertTrue($decodeRes['FreeShipping']);
        }
        $this->assertEquals($order->getShippingCost(), $decodeRes['ShippingPrice']);

        // Данные клиента
        $this->assertEquals($order->getCustomer()->getUid(), $decodeRes['IDBuyer']);
        $this->assertEquals($order->getCustomer()->getPhone(), $decodeRes['ViewTelContactBuyer']);
        $this->assertEquals($order->getCustomer()->getEmail(), $decodeRes['ViewEmailContactBuyer']);

        if($payer = $order->getPayer()) {
            $this->assertEquals($payer->getId(), $decodeRes['IDPayer']);
            $this->assertEquals($payer->getContactPhone(), $decodeRes['TelViewPayer']);
            $this->assertEquals($payer->getName(), $decodeRes['JuristicNamePayer']);
            $this->assertEquals($payer->getInn(), $decodeRes['TaxpayerIdentificationNumberPayer']);
            $this->assertEquals($payer->getKpp(), $decodeRes['RegistrationCodePayer']);
            $this->assertEquals($payer->getOgrnip(), $decodeRes['StateRegistrationNumberPayer']);
            $this->assertEquals($payer->getContact(), $decodeRes['FullNamePayer']);
        }

        if($order->getShippingAddress()) {
            $this->assertEquals($order->getShippingAddress(), $decodeRes['AdressStoreBuyerView']);
        }

        if($replacement = $order->getReplacement()) {
            $this->assertEquals($replacement->getId() - 1, $decodeRes['ProductReplacementMethod']);
        }

        // 2 товара - сам товар и образцы
        $this->assertCount(2, $decodeRes['Products']);
        $this->assertNull($decodeRes['Products'][0]['IDPriceType']);

        $this->assertTrue($decodeRes['NeedWaybills']);
        $this->assertNull($decodeRes['BringAcquiringTerminal']);
    }

    public function testUser()
    {
        $customer = $this->getCustomer();
        $serverUser = new ServerUser($customer);

        $res = SerializerHelper::serialize($serverUser, [SerializerHelper::GROUP_REMOTE_USER]);
        $decodeRes = json_decode($res, true);

        $this->assertEquals($customer->getUid(), $decodeRes[Params::ID]);
        $this->assertArrayHasKey('contacts', $decodeRes);
        $this->assertArrayHasKey(Params::PHONE, $decodeRes['contacts']);
        $this->assertCount(1, $decodeRes['contacts'][Params::PHONE]);
        $this->assertArrayHasKey(Params::EMAIL, $decodeRes['contacts']);
        $this->assertCount(1, $decodeRes['contacts'][Params::EMAIL]);
    }

    /**
     * @group server
     */
    public function testValidatePhone()
    {
        $testCases = [
            ['name' => 'Телефон российский: "+73652669293"', 'isValid' => true, 'phone' => '+73652669293',],
            ['name' => 'Телефон турецкий: "+902122903070"', 'isValid' => false, 'phone' => '+902122903070',],
        ];

        foreach ($testCases as $tc) {
            if($tc['isValid']) {
                $this->assertNotNull(ValidateHelper::getValidPhone($tc['phone']), "Failed to validate {$tc['phone']}");
            }else{
                $this->assertNull(ValidateHelper::getValidPhone($tc['phone']), "Failed to validate {$tc['phone']}");
            }
        }
    }

    /**
     * Тестовый покупатель
     * @return AuthUser
     */
    private function getCustomer(): AuthUser
    {
        $customer = new AuthUser();
        return $customer->setEmail('test@email.ru')->setPhone('79999999999')
            ->setFio('fake user');
    }

    private function getOrderItem(): OrderItem
    {
        $product = $this->getProduct();
        $item = new OrderItem();
        return $item->setQuantity(5)->setSample(2)->setProduct($product)
        ->setPrice($product->getPrice())->setTotal(5 * $product->getPrice())
            ;
    }

    private function getProduct(): Product
    {
        $product = new Product();
        return $product->setPrice(50000)->setName('Test product')
            ->setIsActive(true)->setUuid('uuid');
    }

    private function getDiscount()
    {
        $discount = new Discount();
        $discount->setValue(5)->setName("скидка")->setCode("discc");

        return $discount;
    }
}