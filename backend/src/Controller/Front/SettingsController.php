<?php

namespace App\Controller\Front;

use App\ApiError;
use App\Controller\ApiController;
use App\Handler\Front\SettingsHandler;
use App\Helpers\SerializerHelper;
use App\Interfaces\Front\SettingsInterface;
use Exception;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Общие настройки
 */
class SettingsController extends ApiController
{
    /** @var SettingsInterface */
    private $handler;

    public function __construct(SettingsHandler $handler)
    {
        parent::__construct();
        $this->handler = $handler;
    }

    /**
     * Запрос-заглушка для проверки, что апи доступен
     * @return Response
     * @Route("/api/v1/mock", methods={"GET", "POST"})
     */
    public function mock(): Response
    {
        return $this->createSuccessResponse('');
    }

    /**
     * Обновить данные по категории
     * @Route("/api/v1/settings", methods={"GET"})
     */
    public function getSettings(): Response
    {
        try {
            $settings = $this->handler->getSettings();

            if(empty($settings)){
                return $this->createBadRequestResponse(ApiError::ERROR_NOT_FOUND, self::STATUS_NOT_FOUND);
            }

            $jsonResult = SerializerHelper::serialize($settings, [SerializerHelper::GROUP_SETTINGS]);
            return $this->createSuccessResponse($jsonResult);
        } catch (Exception $fallthrough) {
            return $this->createErrorResponse(new HttpException(self::STATUS_EXCEPTION, "", $fallthrough));
        }
    }
}