<?php

namespace App\Controller\Front;

use App\ApiError;
use App\Controller\ApiController;
use App\Handler\Front\FeedbackHandler;
use App\Helpers\SerializerHelper;
use App\Interfaces\Front\FeedbackInterface;
use Exception;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Обратная связь
 */
class FeedbackController extends ApiController
{
    /** @var FeedbackInterface */
    private $handler;

    public function __construct(FeedbackHandler $handler)
    {
        parent::__construct();

        $this->handler = $handler;
    }

    /**
     * Обратная связь
     * @Route("/api/v1/feedback", methods={"POST"})
     */
    public function feedback(Request $request): JsonResponse
    {
        return $this->feedbackQuestion($request);
    }

    /**
     * Задать вопрос
     * @Route("/api/v1/question", methods={"POST"})
     */
    public function question(Request $request): JsonResponse
    {
        return $this->feedbackQuestion($request);
    }

    private function feedbackQuestion(Request $request): JsonResponse
    {
        try {
            $json = $this->getJsonRequest($request);
            if(is_null($json)) {
                return $this->createBadRequestResponse(ApiError::ERROR_INVALID_JSON);
            }

            $error = $this->handler->question($json);
            if(!is_null($error)) {
                return $this->createBadRequestResponse($error);
            }

            return $this->createSuccessResponse("");
        } catch (Exception $fallthrough) {
            return $this->createErrorResponse(new HttpException(self::STATUS_EXCEPTION, "", $fallthrough));
        }
    }

    /**
     * Заказать звонок
     * @Route("/api/v1/recall", methods={"POST"})
     */
    public function recall(Request $request): JsonResponse
    {
        try {
            $json = $this->getJsonRequest($request);
            if(is_null($json)) {
                return $this->createBadRequestResponse(ApiError::ERROR_INVALID_JSON);
            }

            $error = $this->handler->recall($json);
            if(!is_null($error)) {
                return $this->createBadRequestResponse($error);
            }

            return $this->createSuccessResponse("");
        } catch (Exception $fallthrough) {
            return $this->createErrorResponse(new HttpException(self::STATUS_EXCEPTION, "", $fallthrough));
        }
    }

    /**
     * @param Request $request
     * @return JsonResponse
     * @Route("/api/v1/send-custom-email", methods={"POST"})
     */
    public function customEmail(Request $request): JsonResponse
    {
        try {
            $json = $this->getJsonRequest($request);
            if(is_null($json)) {
                return $this->createBadRequestResponse(ApiError::ERROR_INVALID_JSON);
            }

            $error = $this->handler->customEmail($json);
            if(!is_null($error)) {
                return $this->createBadRequestResponse($error);
            }

            return $this->createSuccessResponse("");
        } catch (Exception $fallthrough) {
            return $this->createErrorResponse(new HttpException(self::STATUS_EXCEPTION, "", $fallthrough));
        }
    }


    /**
     * Нашли дешевле
     * @Route("/api/v1/found-cheaper", methods={"POST"})
     */
    public function foundCheaper(Request $request): JsonResponse
    {
        try {
            $json = $this->getJsonRequest($request);
            if(is_null($json)) {
                return $this->createBadRequestResponse(ApiError::ERROR_INVALID_JSON);
            }

            $error = $this->handler->foundCheaper($json);
            if(!is_null($error)) {
                return $this->createBadRequestResponse($error);
            }

            return $this->createSuccessResponse("");
        } catch (Exception $fallthrough) {
            return $this->createErrorResponse(new HttpException(self::STATUS_EXCEPTION, "", $fallthrough));
        }
    }

    /**
     * Не нашли нужного
     * @Route("/api/v1/not-found-needed", methods={"POST"})
     */
    public function notFoundNeeded(Request $request): JsonResponse
    {
        try {
            $json = $this->getJsonRequest($request);
            if(is_null($json)) {
                return $this->createBadRequestResponse(ApiError::ERROR_INVALID_JSON);
            }

            $error = $this->handler->notFoundNeeded($json);
            if(!is_null($error)) {
                return $this->createBadRequestResponse($error);
            }

            return $this->createSuccessResponse("");
        } catch (Exception $fallthrough) {
            return $this->createErrorResponse(new HttpException(self::STATUS_EXCEPTION, "", $fallthrough));
        }
    }

    /**
     * Подписка на рассылку
     * @Route("/api/v1/subscribe", methods={"POST"})
     */
    public function subscribe(Request $request): JsonResponse
    {
        try {
            $json = $this->getJsonRequest($request);
            if(is_null($json)) {
                return $this->createBadRequestResponse(ApiError::ERROR_INVALID_JSON);
            }

            $error = $this->handler->subscribe($json);
            if(!is_null($error)) {
                return $this->createBadRequestResponse($error);
            }

            return $this->createSuccessResponse("");
        } catch (Exception $fallthrough) {
            return $this->createErrorResponse(new HttpException(self::STATUS_EXCEPTION, "", $fallthrough));
        }
    }

    /**
     * Отписаться от рассылки
     * @Route("/api/v1/unsubscribe", methods={"POST"})
     */
    public function unsubscribe(Request $request): JsonResponse
    {
        try {
            $json = $this->getJsonRequest($request);
            if(is_null($json)) {
                return $this->createBadRequestResponse(ApiError::ERROR_INVALID_JSON);
            }

            $error = $this->handler->unsubscribe($json);
            if(!is_null($error)) {
                return $this->createBadRequestResponse($error);
            }

            return $this->createSuccessResponse("");
        } catch (Exception $fallthrough) {
            return $this->createErrorResponse(new HttpException(self::STATUS_EXCEPTION, "", $fallthrough));
        }
    }


    /**
     * Список вопрос-ответ
     * @return JsonResponse
     *
     * @Route("/api/v1/faq", methods={"GET"})
     */
    public function faq(): JsonResponse
    {
        try {
            $questions = $this->handler->faq();

            if(empty($questions)) {
                return $this->createEmptyResponse();
            }

            return $this->createSuccessResponse($questions);
        } catch (Exception $fallthrough) {
            return $this->createErrorResponse(new HttpException(self::STATUS_EXCEPTION, "", $fallthrough));
        }
    }

    /**
     * заявка в отдел коммерции
     * @Route("/api/v1/commercial-department", methods={"POST"})
     */
    public function commercialDepartment(Request $request): JsonResponse
    {
        try {
            $json = $this->getJsonRequest($request);
            if(is_null($json)) {
                return $this->createBadRequestResponse(ApiError::ERROR_INVALID_JSON);
            }

            $error = $this->handler->commercialDepartment($json);
            if(!is_null($error)) {
                return $this->createBadRequestResponse($error);
            }

            return $this->createSuccessResponse("");
        } catch (Exception $fallthrough) {
            return $this->createErrorResponse(new HttpException(self::STATUS_EXCEPTION, "", $fallthrough));
        }
    }
}