<?php

namespace App\Controller\Front;

use App\ApiError;
use App\Controller\ApiController;
use App\Controller\Security\BaseSecurityController;
use App\Entity\Order;
use App\Handler\Front\OrderHandler;
use App\Handler\Front\ProductHandler;
use App\Helpers\Params;
use App\Helpers\SerializerHelper;
use App\Repository\AuthUserRepository;
use App\RequestJson;
use Exception;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Эндпоинты для управления товарами
 */
class ProductController extends ApiController
{
    /** @var ProductHandler */
    private $handler;

    public function __construct(ProductHandler $handler)
    {
        parent::__construct();
        $this->handler = $handler;
    }

    /**
     * @Route("/api/v1/product/list", methods={"GET"})
     */
    public function getList(Request $request): JsonResponse
    {
        try {
            $uuids = $request->get(Params::UUID);
            if(empty($uuids)) {
                return $this->createBadRequestResponse(ApiError::ERROR_INVALID_JSON);
            }

            $products = $this->handler->getByUuids(new RequestJson([Params::UUID => $uuids]));

            if(empty($products)) {
                return $this->createSuccessResponse("", self::STATUS_EMPTY);
            }

            $fields = $request->get(Params::FIELDS);
            if(!empty($fields)) {
                $jsonResult = SerializerHelper::serialize($products, [SerializerHelper::GROUP_PRODUCT_LIST], explode(",", $fields));
            } else {
                $jsonResult = SerializerHelper::serialize($products, [SerializerHelper::GROUP_PRODUCT_LIST]);
            }

            return $this->createSuccessResponse($jsonResult);
        }catch (Exception $fallthrough) {
            return $this->createErrorResponse(new HttpException(ApiController::STATUS_EXCEPTION, "", $fallthrough));
        }
    }

    /**
     * @param string $uuid
     * @return JsonResponse
     * @Route("/api/v1/product/{uuid}", methods={"GET"})
     */
    public function getProduct(Request $request, string $uuid): JsonResponse
    {
        try {
            if(empty($uuid)) {
                return $this->createBadRequestResponse();
            }

            $product = $this->handler->getProduct($uuid);

            if(empty($product)) {
                return $this->createBadRequestResponse(ApiError::ERROR_NOT_FOUND);
            }

            $fields = $request->get(Params::FIELDS);
            if(!empty($fields)) {
                $json = SerializerHelper::serialize($product, [SerializerHelper::GROUP_PRODUCT], explode(",", $fields));
            } else {
                $json = SerializerHelper::serialize($product, [SerializerHelper::GROUP_PRODUCT]);
            }

            return $this->createSuccessResponse($json);
        }catch (Exception $fallthrough) {
            return $this->createErrorResponse(new HttpException(ApiController::STATUS_EXCEPTION, "", $fallthrough));
        }
    }

    /**
     * Сообщить о поступлении товара
     *
     * @param Request $request
     * @param string $uuid
     * @return JsonResponse
     * @Route("/api/v1/product/waiting/{uuid}", methods={"POST"})
     */
    public function waiting(Request $request, string $uuid): JsonResponse
    {
        try {
            if(empty($uuid)) {
                return $this->createBadRequestResponse();
            }

            $json = $this->getJsonRequest($request);
            if(is_null($json)) {
                return $this->createBadRequestResponse(ApiError::ERROR_INVALID_JSON);
            }

            $error = $this->handler->waiting($uuid, $json);

            if(!is_null($error)) {
                return $this->createBadRequestResponse($error);
            }

            return $this->createSuccessResponse("");
        }catch (Exception $fallthrough) {
            return $this->createErrorResponse(new HttpException(ApiController::STATUS_EXCEPTION, "", $fallthrough));
        }
    }

    /**
     * Обновление остатков из 1C
     * @param Request $request
     * @return JsonResponse
     * @Route("/api/v1/product/rest-update", methods={"PUT"})
     */
    public function updateRest(Request $request): JsonResponse
    {
        try {
            $json = $this->getJsonRequest($request);
            if (is_null($json)) {
                return $this->createBadRequestResponse(ApiError::ERROR_INVALID_JSON);
            }

            if($request->get(Params::API_KEY) != $this->getParameter('api_key_1c')) {
                return $this->createUnauthorizationResponse();
            }

            $msg = $this->handler->updateRest($json);

            if(!empty($msg)) {
                return $this->createBadRequestResponse(ApiError::ERROR_INVALID_DATA, ApiController::STATUS_BAD_REQUEST, $msg);
            }

            return $this->createSuccessResponse('');
        } catch (Exception $fallthrough) {
            return $this->createErrorResponse(new HttpException( self::STATUS_EXCEPTION, "", $fallthrough));
        }
    }
}