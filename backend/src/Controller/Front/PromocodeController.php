<?php

namespace App\Controller\Front;

use App\ApiError;
use App\Controller\ApiController;
use App\Handler\Front\PromocodeHandler;
use App\Helpers\SerializerHelper;
use App\Interfaces\Front\PromocodeInterface;
use Exception;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Оформление заказа
 */
class PromocodeController extends ApiController
{
    /** @var PromocodeInterface */
    public $handler;

    public function __construct(PromocodeHandler $handler)
    {
        parent::__construct();
        $this->handler = $handler;
    }

    /**
     * Использование промокода
     * @param Request $request
     * @return JsonResponse
     *
     * @Route("/api/v1/promocode", methods={"PUT"})
     */
    public function promocodeUse(Request $request): JsonResponse
    {
        try {
            if (empty(ApiController::getCurrentUser())) {
                return $this->createUnauthorizationResponse();
            }

            $jsonReq = $this->getJsonRequest($request);
            if(empty($jsonReq)) {
                return $this->createBadRequestResponse(ApiError::ERROR_INVALID_JSON);
            }

            $errorCode = null;
            $cart = $this->handler->use($jsonReq, $errorCode);
            if(!is_null($errorCode)) {
                return $this->createBadRequestResponse($errorCode);
            }

            if(is_null($cart)) {
                return $this->createBadRequestResponse(ApiError::ERROR_INVALID_CART);
            }

            return $this->json([
                "discount" => $cart->getItemsDiscount(),
                "total_cost" => $cart->getItemsTotal(),
            ]);
        }catch (Exception $exception) {
            return $this->createErrorResponse(new HttpException(ApiController::STATUS_EXCEPTION, "", $exception));
        }
    }

    /**
     * @param Request $request
     * @return JsonResponse
     *
     * @Route("/api/v1/promocode", methods={"DELETE"})
     */
    public function promocodeDelete(Request $request): JsonResponse
    {
        try {
            if (empty(ApiController::getCurrentUser())) {
                return $this->createUnauthorizationResponse();
            }

            $jsonReq = $this->getJsonRequest($request);
            if(empty($jsonReq)) {
                return $this->createBadRequestResponse(ApiError::ERROR_INVALID_JSON);
            }

            $errorCode = null;
            $cart = $this->handler->delete($jsonReq, $errorCode);
            if(!is_null($errorCode)) {
                return $this->createBadRequestResponse($errorCode);
            }

            $jsonResult = SerializerHelper::serialize($cart, [SerializerHelper::GROUP_PROMO_REMOVE]);
            return $this->createSuccessResponse($jsonResult);
        }catch (Exception $exception) {
            return $this->createErrorResponse(new HttpException(ApiController::STATUS_EXCEPTION, "", $exception));
        }
    }
}