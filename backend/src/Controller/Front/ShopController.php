<?php

namespace App\Controller\Front;

use App\ApiError;
use App\Controller\ApiController;
use App\Handler\Front\ShopHandler;
use App\Helpers\SerializerHelper;
use App\Interfaces\Front\ShopInterface;
use Exception;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Routing\Annotation\Route;

/**
 * class ShopController
 * Эндпоинты для магазинов
 */
class ShopController extends ApiController
{
    /** @var ShopInterface */
    private $handler;

    public function __construct(ShopHandler $handler)
    {
        parent::__construct();
        $this->handler = $handler;
    }

    /**
     * @return JsonResponse
     *
     * @Route("/api/v1/shops", methods={"GET"})
     */
    public function getAllActive(): JsonResponse
    {
        try {
            $shops = $this->handler->getAllActive();

            if(empty($shops)) {
                return $this->createSuccessResponse("", parent::STATUS_EMPTY);
            }

            $jsonResult = SerializerHelper::serialize($shops, [SerializerHelper::GROUP_SHOP_LIST]);
            return $this->createSuccessResponse($jsonResult);
        }catch (Exception $fallthrough) {
            return $this->createErrorResponse(new HttpException(parent::STATUS_EXCEPTION, "", $fallthrough));
        }
    }

    /**
     * @param string $uuid
     * @return JsonResponse
     *
     * @Route("/api/v1/shops/{uuid}", methods={"GET"})
     */
    public function getOne(string $uuid): JsonResponse
    {
        try {
            if(empty($uuid)) {
                return $this->createBadRequestResponse();
            }

            $shop = $this->handler->getOne($uuid);

            if(empty($shop)) {
                return $this->createBadRequestResponse(ApiError::ERROR_NOT_FOUND, parent::STATUS_NOT_FOUND);
            }

            $jsonResult = SerializerHelper::serialize($shop, [SerializerHelper::GROUP_SHOP]);
            return $this->createSuccessResponse($jsonResult);
        }catch (Exception $fallthrough) {
            return $this->createErrorResponse(new HttpException(parent::STATUS_EXCEPTION, "", $fallthrough));
        }
    }
}