<?php

namespace App\Controller\Front;

use App\ApiError;
use App\Controller\ApiController;
use App\Controller\Security\BaseSecurityController;
use App\Entity\Order;
use App\Handler\Front\OrderHandler;
use App\Helpers\Params;
use App\Helpers\SerializerHelper;
use App\Interfaces\Front\OrderInterface;
use App\Repository\AuthUserRepository;
use App\RequestJson;
use Exception;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class OrderController
 */
class OrderController extends BaseSecurityController
{
    /** @var OrderHandler */
    private $handler;

    public function __construct(AuthUserRepository $repository, OrderHandler $handler)
    {
        parent::__construct($repository);
        $this->handler = $handler;
    }

    /**
     * @Route("/api/v1/cart/{uid}", methods={"GET"})
     */
    public function getCart(string $uid): JsonResponse
    {
        try {
            if(empty($uid)) {
                return $this->createBadRequestResponse(ApiError::ERROR_CART);
            }

            $cart = $this->handler->getCart($uid);

            if(is_null($cart)) {
                return $this->createBadRequestResponse(ApiError::ERROR_NOT_FOUND, ApiController::STATUS_NOT_FOUND);
            }

            if(count($cart->getItems()) == 0) {
                return $this->createSuccessResponse("", ApiController::STATUS_EMPTY);
            }

            $jsonResult = SerializerHelper::serialize($cart, [SerializerHelper::GROUP_CART]);
            return $this->createSuccessResponse($jsonResult);
        }catch (Exception $fallthrough) {
            return $this->createErrorResponse(new HttpException(self::STATUS_EXCEPTION, "", $fallthrough));
        }
    }

    /**
     * @param Request $request
     * @return JsonResponse
     *
     * @Route("/api/v1/cart/change-qty", methods={"POST"})
     */
    public function changeQty(Request $request): JsonResponse
    {
        try {
            $json = $this->getJsonRequest($request);
            if(is_null($json)) {
                return $this->createBadRequestResponse(ApiError::ERROR_INVALID_JSON);
            }

            $errorCode = null;

            $resp = $this->handler->changeProductQty($json, $errorCode);

            if(!is_null($errorCode)) {
                return $this->createBadRequestResponse($errorCode);
            }

            return $this->json($resp);
        } catch (Exception $fallthrough) {
            return $this->createErrorResponse(new HttpException(self::STATUS_EXCEPTION, "", $fallthrough));
        }
    }

    /**
     * @param string $uid
     * @return JsonResponse
     *
     * @Route("/api/v1/cart/{uid}/bind-user", methods={"PUT"})
     */
    public function bindUser(string $uid): JsonResponse
    {
        try {
            if(empty($uid)) {
                return $this->createBadRequestResponse(ApiError::ERROR_INVALID_JSON);
            }

            if(!$this->handler->bindUser($uid)) {
                return $this->createBadRequestResponse(ApiError::ERROR_INVALID_DATA);
            }

            return $this->createSuccessResponse("");
        } catch (Exception $fallthrough) {
            return $this->createErrorResponse(new HttpException(self::STATUS_EXCEPTION, "", $fallthrough));
        }
    }

    /**
     * @param Request $request
     * @return JsonResponse
     *
     * @Route("/api/v1/cart/delete-product", methods={"DELETE"})
     */
    public function deleteProduct(Request $request): JsonResponse
    {
        try {
            $json = $this->getJsonRequest($request);
            if(is_null($json)) {
                return $this->createBadRequestResponse(ApiError::ERROR_INVALID_JSON);
            }

            $errorCode = null;

           $cart = $this->handler->deleteProduct($json, $errorCode);

            if(!is_null($errorCode)) {
                return $this->createBadRequestResponse($errorCode);
            }

            $json = SerializerHelper::serialize($cart, [SerializerHelper::GROUP_CART_DELETE_PRODUCT]);
            return $this->createSuccessResponse($json);
        } catch (Exception $fallthrough) {
            return $this->createErrorResponse(new HttpException(self::STATUS_EXCEPTION, "", $fallthrough));
        }
    }

    /**
     * Очистка корзины от товаров
     * @param string $uid
     * @return JsonResponse
     *
     * @Route("/api/v1/cart/remove/{uid}", methods={"PUT"})
     */
    public function clear(string $uid): JsonResponse
    {
        try {
            if(empty($uid)) {
                return $this->createBadRequestResponse(ApiError::ERROR_INVALID_JSON);
            }

            if(!$this->handler->clear($uid)) {
                return $this->createBadRequestResponse(ApiError::ERROR_CART);
            }

            return $this->createSuccessResponse("");
        } catch (Exception $fallthrough) {
            return $this->createErrorResponse(new HttpException( self::STATUS_EXCEPTION, "", $fallthrough));
        }
    }

    /**
     * @param string $uid
     * @return JsonResponse
     *
     * @Route("/api/v1/cart/remove/{uid}", methods={"DELETE"})
     */
    public function remove(string $uid): JsonResponse
    {
        try {
            if(empty($uid)) {
                return $this->createBadRequestResponse(ApiError::ERROR_INVALID_JSON);
            }

            if(!$this->handler->remove($uid)) {
                return $this->createBadRequestResponse(ApiError::ERROR_CART);
            }

            return $this->createSuccessResponse("");
        } catch (Exception $fallthrough) {
            return $this->createErrorResponse(new HttpException( self::STATUS_EXCEPTION, "", $fallthrough));
        }
    }

    /**
     * @param Request $request
     * @return JsonResponse
     *
     * @Route("/api/v1/cart/sample", methods={"POST"})
     */
    public function sample(Request $request): JsonResponse
    {
        try {
            $json = $this->getJsonRequest($request);
            if(is_null($json)) {
                return $this->createBadRequestResponse(ApiError::ERROR_INVALID_JSON);
            }

            $res = $this->handler->sample($json);

            if(empty($res)) {
                return $this->createBadRequestResponse();
            }

            return $this->json($res);
        } catch (Exception $fallthrough) {
            return $this->createErrorResponse(new HttpException( self::STATUS_EXCEPTION, "", $fallthrough));
        }
    }

    /**
     * Сумма минимального заказа для бесплатной доставки
     *
     * @param Request $request
     * @return JsonResponse
     *
     * @Route("/api/v1/free-shipping", methods={"GET"})
     */
    public function freeShipping(Request $request): JsonResponse
    {
        try {
            $region = $request->get(Params::REGION);
            if(empty($region)) {
                return $this->createBadRequestResponse(ApiError::ERROR_INVALID_JSON);
            }

            $res = $this->handler->freeShipping($region);

            if(empty($res)) {
                return $this->createEmptyResponse();
            }

            return $this->json([Params::ORDER_MIN_COST => $res]);
        } catch (Exception $fallthrough) {
            return $this->createErrorResponse(new HttpException( self::STATUS_EXCEPTION, "", $fallthrough));
        }
    }

    /**
     * Заказ указывается как "Быстрый заказ"
     *
     * @param Request $request
     * @param string $uid
     * @return JsonResponse
     *
     * @Route("/api/v1/fast-order/{uid}", methods={"PUT"})
     */
    public function fastOrder(Request $request, string $uid): JsonResponse
    {
        try {
            $json = $this->getJsonRequest($request);
            if (is_null($json)) {
                return $this->createBadRequestResponse(ApiError::ERROR_INVALID_JSON);
            }

            $error = null;

            $orderNumber = $this->handler->fastOrder($json, $uid, $error);
            if (!is_null($error)) {
                return $this->createBadRequestResponse($error);
            }

            return $this->json([Params::NUMBER => $orderNumber]);
        } catch (Exception $fallthrough) {
            return $this->createErrorResponse(new HttpException( self::STATUS_EXCEPTION, "", $fallthrough));
        }
    }

    /**
     * Изменение из 1C статуса заказа
     * @param Request $request
     * @param string $uid
     * @return JsonResponse
     * @Route("/api/v1/order/change-status/{uid}", methods={"PUT"})
     */
    public function changeStatus(Request $request, string $uid): JsonResponse
    {
        try {
            $json = new RequestJson($request->query->all());

            if($json->getString(Params::API_KEY) != $this->getParameter('api_key_1c')) {
                return $this->createUnauthorizationResponse();
            }

            $error = $this->handler->changeStatus($json, $uid);

            if(!is_null($error)) {
                return $this->createBadRequestResponse($error);
            }

            return $this->createSuccessResponse('');
        } catch (Exception $fallthrough) {
            return $this->createErrorResponse(new HttpException( self::STATUS_EXCEPTION, "", $fallthrough));
        }
    }

    /**
     * Изменение из 1C состава заказа
     * @param Request $request
     * @param string $uid
     * @return JsonResponse
     * @Route("/api/v1/order/change-products/{uid}", methods={"PUT"})
     */
    public function changeProducts(Request $request, string $uid): JsonResponse
    {
        try {
            if($request->get(Params::API_KEY) != $this->getParameter('api_key_1c')) {
                return $this->createUnauthorizationResponse();
            }

            $json = $this->getJsonRequest($request);
            if(is_null($json)) {
                return $this->createBadRequestResponse(ApiError::ERROR_INVALID_JSON);
            }

            $error = $this->handler->changeComposition($json, $uid);

            if(!empty($error)) {
                if(is_string($error)) {
                    return $this->createBadRequestResponse($error);
                }

                $msg = implode(";", $error);
                return $this->createBadRequestResponse(ApiError::ERROR_BAD_REQUEST, ApiController::STATUS_BAD_REQUEST, $msg);
            }

            return $this->createSuccessResponse('');
        } catch (Exception $fallthrough) {
            return $this->createErrorResponse(new HttpException( self::STATUS_EXCEPTION, "", $fallthrough));
        }
    }
}