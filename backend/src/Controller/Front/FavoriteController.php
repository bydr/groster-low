<?php

namespace App\Controller\Front;

use App\ApiError;
use App\Controller\ApiController;
use App\Controller\Security\BaseSecurityController;
use App\Entity\Category;
use App\Handler\Front\CatalogHandler;
use App\Handler\Front\FavoriteHanlder;
use App\Helpers\Params;
use App\Helpers\SerializerHelper;
use App\Interfaces\Front\CatalogInterface;
use App\Interfaces\Front\FavoriteInterface;
use App\RequestJson;
use App\Service\Cache;
use Exception;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Избранное
 */
class FavoriteController extends ApiController
{
    /** @var FavoriteInterface */
    private $handler;

    public function __construct(FavoriteHanlder $handler)
    {
        parent::__construct();
        $this->handler = $handler;
    }

    /**
     * Список всех товаров в избранном
     * @Route("/api/v1/favorites", methods={"GET"})
     */
    public function findAll(): JsonResponse
    {
        try {
            $products = $this->handler->getAllProducts();

            if(empty($products)) {
                return $this->createEmptyResponse();
            }

            return $this->json([Params::PRODUCTS => $products]);
        } catch (Exception $fallthrough) {
            return $this->createErrorResponse(new HttpException(self::STATUS_EXCEPTION, "", $fallthrough));
        }
    }

    /**
     * Список всех товаров в избранном
     * @Route("/api/v1/favorites/add", methods={"POST"})
     */
    public function addProduct(Request $request): JsonResponse
    {
        try {
            $json = $this->getJsonRequest($request);
            if(is_null($json)) {
                return $this->createBadRequestResponse(ApiError::ERROR_INVALID_JSON);
            }

            $error = $this->handler->addProducts($json);

            if(!empty($error)) {
                return $this->createBadRequestResponse($error);
            }

            return $this->createSuccessResponse('', ApiController::STATUS_SUCCESS_SAVE);
        } catch (Exception $fallthrough) {
            return $this->createErrorResponse(new HttpException(self::STATUS_EXCEPTION, "", $fallthrough));
        }
    }

    /**
     * Список всех товаров в избранном
     * @Route("/api/v1/favorites/delete", methods={"DELETE"})
     */
    public function deleteProduct(Request $request): JsonResponse
    {
        try {
            $json = $this->getJsonRequest($request);
            if(is_null($json)) {
                return $this->createBadRequestResponse(ApiError::ERROR_INVALID_JSON);
            }

            $error = '';
            $this->handler->deleteProduct($json, $error);

            if(!empty($error)) {
                return $this->createBadRequestResponse($error);
            }

            return $this->createSuccessResponse('');
        } catch (Exception $fallthrough) {
            return $this->createErrorResponse(new HttpException( self::STATUS_EXCEPTION, "", $fallthrough));
        }
    }
}
