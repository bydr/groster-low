<?php

namespace App\Controller\Front;

use App\ApiError;
use App\Controller\ApiController;
use App\Entity\PaymentMethod;
use App\Handler\Front\CheckoutHandler;
use App\Handler\Front\PromocodeHandler;
use App\Helpers\Params;
use App\Helpers\SerializerHelper;
use App\Interfaces\Front\CheckoutInterface;
use App\Interfaces\Front\PromocodeInterface;
use App\RequestJson;
use Exception;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Оформление заказа
 */
class CheckoutController extends ApiController
{
    /** @var CheckoutInterface */
    public $handler;

    public function __construct(CheckoutHandler $handler)
    {
        parent::__construct();
        $this->handler = $handler;
    }

    /**
     * Использование промокода
     * @return JsonResponse
     *
     * @Route("/api/v1/checkout/last-order", methods={"GET"})
     */
    public function lastOrder(): JsonResponse
    {
        try {
            if (empty(ApiController::getCurrentUser())) {
                return $this->createUnauthorizationResponse();
            }

            $lastOrder = $this->handler->lastOrder();
            if(is_null($lastOrder)) {
                return $this->createSuccessResponse("", ApiController::STATUS_EMPTY);
            }


            $jsonResult = SerializerHelper::serialize($lastOrder, [SerializerHelper::GROUP_LAST_ORDER]);
            return $this->createSuccessResponse($jsonResult);
        }catch (Exception $exception) {
            return $this->createErrorResponse(new HttpException(ApiController::STATUS_EXCEPTION, "", $exception));
        }
    }

    /**
     * Список пользовательских данных из прошлых заказов
     *
     * @return JsonResponse
     * @Route("/api/v1/checkout/customer-data", methods={"GET"})
     */
    public function customerData(): JsonResponse
    {
        try {
            if (empty(ApiController::getCurrentUser())) {
                return $this->createUnauthorizationResponse();
            }

            return $this->json($this->handler->customerData());
        }catch (Exception $exception) {
            return $this->createErrorResponse(new HttpException(ApiController::STATUS_EXCEPTION, "", $exception));
        }
    }

    /**
     * Список доступных методов доставки
     *
     * @param Request $request
     * @return JsonResponse
     * @Route("/api/v1/checkout/shipping-methods", methods={"GET"})
     */
    public function shippingMethods(Request $request): JsonResponse
    {
        try {
            $json = new RequestJson($request->query->all());

            $methods = $this->handler->shippingMethods($json);
            if(empty($methods)) {
                return $this->createSuccessResponse('', ApiController::STATUS_EMPTY);
            }

            $respJson = SerializerHelper::serialize($methods, [SerializerHelper::GROUP_SHIPPING_METHOD_LIST]);
            return $this->createSuccessResponse($respJson);
        }catch (Exception $exception) {
            return $this->createErrorResponse(new HttpException(ApiController::STATUS_EXCEPTION, "", $exception));
        }
    }

    /**
     * Список доступных методов оплаты
     *
     * @param Request $request
     * @return JsonResponse
     * @Route("/api/v1/checkout/payment-methods", methods={"GET"})
     */
    public function paymentMethods(Request $request): JsonResponse
    {
        try {
            if (empty(ApiController::getCurrentUser())) {
                return $this->createUnauthorizationResponse();
            }

            $methods = $this->handler->paymentMethods();
            if(empty($methods)) {
                return $this->createSuccessResponse('', ApiController::STATUS_EMPTY);
            }

            $respJson = SerializerHelper::serialize($methods, [SerializerHelper::GROUP_PAYMENT_METHOD_LIST]);
            return $this->createSuccessResponse($respJson);
        }catch (Exception $exception) {
            return $this->createErrorResponse(new HttpException(ApiController::STATUS_EXCEPTION, "", $exception));
        }
    }

    /**
     * Список доступных методов доставки
     *
     * @return JsonResponse
     * @Route("/api/v1/checkout/replacements", methods={"GET"})
     */
    public function replacements(): JsonResponse
    {
        try {
            if (empty(ApiController::getCurrentUser())) {
                return $this->createUnauthorizationResponse();
            }

            $methods = $this->handler->getReplacements();
            if(empty($methods)) {
                return $this->createEmptyResponse();
            }

            $resJson = SerializerHelper::serialize($methods, [SerializerHelper::GROUP_REPLACEMENT_METHODS]);
            return $this->createSuccessResponse($resJson);
        }catch (Exception $exception) {
            return $this->createErrorResponse(new HttpException(ApiController::STATUS_EXCEPTION, "", $exception));
        }
    }

    /**
     * @param Request $request
     * @return JsonResponse
     * @Route("/api/v1/checkout/shipping-cost", methods={"POST"})
     */
    public function shippingCost(Request $request): JsonResponse
    {
        try {
            if (empty(ApiController::getCurrentUser())) {
                return $this->createUnauthorizationResponse();
            }

            $json = $this->getJsonRequest($request);
            if(is_null($json)) {
                return $this->createBadRequestResponse(ApiError::ERROR_INVALID_JSON);
            }

            $error = null;
            list($shippingCost, $paymentMethods) = $this->handler->shippingCost($json, $error);

            if(!is_null($error)) {
                return $this->createBadRequestResponse($error);
            }

            $payMethods = [];
            /** @var PaymentMethod $paymentMethod */
            foreach ($paymentMethods as $paymentMethod) {
                $payMethods[] = [
                    Params::UID => $paymentMethod->getUid(),
                    Params::NAME => $paymentMethod->getName(),
                    Params::WEIGHT => $paymentMethod->getWeight(),
                ];
            }
            return $this->json([
                Params::SHIPPING_COST => $shippingCost,
                Params::PAYMENT_METHODS => $payMethods,
            ]);
        }catch (Exception $exception) {
            return $this->createErrorResponse(new HttpException(ApiController::STATUS_EXCEPTION, "", $exception));
        }
    }

    /**
     * @param Request $request
     * @param string $uid
     * @return JsonResponse
     *
     * @Route("/api/v1/checkout/order-save/{uid}", methods={"PUT"})
     */
    public function saveOrder(Request $request, string $uid): JsonResponse
    {
        try {
            if (empty(ApiController::getCurrentUser())) {
                return $this->createUnauthorizationResponse();
            }

            $json = $this->getJsonRequest($request);
            if(is_null($json)) {
                return $this->createBadRequestResponse(ApiError::ERROR_INVALID_JSON);
            }

            $error = null;
            $orderNumber = $this->handler->orderSave($json, $uid, $error);

            if(!is_null($error)) {
                return $this->createBadRequestResponse($error);
            }

            return $this->json([Params::NUMBER => $orderNumber]);
        }catch (Exception $exception) {
            return $this->createErrorResponse(new HttpException(ApiController::STATUS_EXCEPTION, "", $exception));
        }
    }
}