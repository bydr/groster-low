<?php

namespace App\Controller\Front;

use App\Controller\ApiController;
use App\Handler\Front\BannerHandler;
use App\Interfaces\Front\BannerInterface;
use Exception;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Баннеры
 */
class BannerController extends ApiController
{
    /** @var BannerInterface */
    private $handler;

    public function __construct(BannerHandler $handler)
    {
        parent::__construct();
        $this->handler = $handler;
    }

    /**
     * Список баннеров
     * @Route("/api/v1/pictures", methods={"GET"})
     */
    public function banners(): JsonResponse
    {
        try {
            $banners = $this->handler->getAll();

            if(empty($banners)) {
                return $this->createEmptyResponse();
            }

            return $this->createSuccessResponse($banners);
        } catch (Exception $fallthrough) {
            return $this->createErrorResponse(new HttpException(ApiController::STATUS_EXCEPTION, "", $fallthrough));
        }
    }
}
