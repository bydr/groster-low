<?php

namespace App\Controller\Front;

use App\ApiError;
use App\Controller\ApiController;
use App\Entity\Order;
use App\Handler\Front\AccountHandler;
use App\Helpers\OrderHelper;
use App\Helpers\Params;
use App\Helpers\SerializerHelper;
use App\Interfaces\Front\AccountInterface;
use App\RequestJson;
use App\Service\RetailCRM;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;
use function Clue\StreamFilter\remove;

/**
 * Личный кабинет
 */
class AccountController extends ApiController
{
    /** @var AccountInterface */
    private $handler;

    public function __construct(AccountHandler $handler)
    {
        parent::__construct();
        $this->handler = $handler;
    }

    /**
     * Список плательщиков
     * @Route("/api/v1/account/payers", methods={"GET"})
     */
    public function payers(): JsonResponse
    {
        try {
            if (empty(ApiController::getCurrentUser())) {
                return $this->createUnauthorizationResponse();
            }

            $payers = $this->handler->allPayers();

            $jsonResult = SerializerHelper::serialize($payers, [SerializerHelper::GROUP_PAYER]);
            return $this->createSuccessResponse($jsonResult);
        } catch (Exception $fallthrough) {
            return $this->createErrorResponse(new HttpException(ApiController::STATUS_EXCEPTION, "", $fallthrough));
        }
    }

    /**
     * Данные плательщика
     * @param string $uid
     * @return JsonResponse
     * @Route("/api/v1/account/payer/{uid}", methods={"GET"})
     */
    public function payerGet(string $uid): JsonResponse
    {
        try {
            if (empty(ApiController::getCurrentUser())) {
                return $this->createUnauthorizationResponse();
            }

            $payer = $this->handler->payerGet($uid);

            if(empty($payer)) {
                return $this->createBadRequestResponse(ApiError::ERROR_NOT_FOUND, ApiController::STATUS_NOT_FOUND);
            }

            $jsonResult = SerializerHelper::serialize($payer, [SerializerHelper::GROUP_PAYER]);
            return $this->createSuccessResponse($jsonResult);
        } catch (Exception $fallthrough) {
            return $this->createErrorResponse(new HttpException(ApiController::STATUS_EXCEPTION, "", $fallthrough));
        }
    }

    /**
     * Добавление плательщика
     * @Route("/api/v1/account/payer/add", methods={"POST"})
     */
    public function payerAdd(Request $request): JsonResponse
    {
        try {
            if (empty(ApiController::getCurrentUser())) {
                return $this->createUnauthorizationResponse();
            }

            $jReq = $this->getJsonRequest($request);
            if(is_null($jReq)) {
                return $this->createBadRequestResponse(ApiError::ERROR_INVALID_JSON);
            }

            $error = null;
            $uid = $this->handler->payerUpdate($jReq, 0, $error);

            if(!is_null($error)) {
                return $this->createBadRequestResponse($error);
            }

            return $this->json(["uid" => $uid]);
        } catch (Exception $fallthrough) {
            return $this->createErrorResponse(new HttpException(ApiController::STATUS_EXCEPTION, "", $fallthrough));
        }
    }

    /**
     * Обновление плательщика
     * @Route("/api/v1/account/payer/{uid}", methods={"PUT"})
     */
    public function payerUpdate(Request $request, string $uid): JsonResponse
    {
        try {
            if (empty(ApiController::getCurrentUser())) {
                return $this->createUnauthorizationResponse();
            }

            $jReq = $this->getJsonRequest($request);
            if(is_null($jReq)) {
                return $this->createBadRequestResponse(ApiError::ERROR_INVALID_JSON);
            }

            $error = null;
            $res = $this->handler->payerUpdate($jReq, $uid, $error);

            if(!is_null($error)) {
                return $this->createBadRequestResponse($error);
            }

            return $this->json(["uid" => $res]);
        } catch (Exception $fallthrough) {
            return $this->createErrorResponse(new HttpException(ApiController::STATUS_EXCEPTION, "", $fallthrough));
        }
    }

    /**
     * Список адресов
     * @Route("/api/v1/account/addresses", methods={"GET"})
     */
    public function addresses(): JsonResponse
    {
        try {
            if (empty(ApiController::getCurrentUser())) {
                return $this->createUnauthorizationResponse();
            }

            $payers = $this->handler->allAddresses();

            $jsonResult = SerializerHelper::serialize($payers, [SerializerHelper::GROUP_SHIPPING_ADDRESS]);
            return $this->createSuccessResponse($jsonResult);
        } catch (Exception $fallthrough) {
            return $this->createErrorResponse(new HttpException(ApiController::STATUS_EXCEPTION, "", $fallthrough));
        }
    }

    /**
     * Данные адреса
     * @param string $uid
     * @return JsonResponse
     * @Route("/api/v1/account/address/{uid}", methods={"GET"})
     */
    public function addressGet(string $uid): JsonResponse
    {
        try {
            if (empty(ApiController::getCurrentUser())) {
                return $this->createUnauthorizationResponse();
            }

            $address = $this->handler->addressGet($uid);

            if(empty($address)) {
                return $this->createBadRequestResponse(ApiError::ERROR_NOT_FOUND, ApiController::STATUS_NOT_FOUND);
            }

            $jsonResult = SerializerHelper::serialize($address, [SerializerHelper::GROUP_SHIPPING_ADDRESS]);
            return $this->createSuccessResponse($jsonResult);
        } catch (Exception $fallthrough) {
            return $this->createErrorResponse(new HttpException(ApiController::STATUS_EXCEPTION, "", $fallthrough));
        }
    }

    /**
     * Добавление адреса
     * @Route("/api/v1/account/address/add", methods={"POST"})
     */
    public function addressAdd(Request $request): JsonResponse
    {
        try {
            if (empty(ApiController::getCurrentUser())) {
                return $this->createUnauthorizationResponse();
            }

            $jReq = $this->getJsonRequest($request);
            if(is_null($jReq)) {
                return $this->createBadRequestResponse(ApiError::ERROR_INVALID_JSON);
            }

            $uid = $this->handler->addressUpdate($jReq, null);

            if(empty($uid)) {
                return $this->createBadRequestResponse(ApiError::ERROR_INVALID_JSON);
            }

            return $this->json(["uid" => $uid]);
        } catch (Exception $fallthrough) {
            return $this->createErrorResponse(new HttpException(ApiController::STATUS_EXCEPTION, "", $fallthrough));
        }
    }

    /**
     * Обновление адреса
     * @Route("/api/v1/account/address/{uid}", methods={"PUT"})
     */
    public function addressUpdate(Request $request, string $uid): JsonResponse
    {
        try {
            if (empty(ApiController::getCurrentUser())) {
                return $this->createUnauthorizationResponse();
            }

            $jReq = $this->getJsonRequest($request);
            if(is_null($jReq)) {
                return $this->createBadRequestResponse(ApiError::ERROR_INVALID_JSON);
            }

            $res = $this->handler->addressUpdate($jReq, $uid);

            if(empty($res)) {
                return $this->createBadRequestResponse(ApiError::ERROR_NOT_FOUND, ApiController::STATUS_NOT_FOUND);
            }

            return $this->json(["uid" => $res]);
        } catch (Exception $fallthrough) {
            return $this->createErrorResponse(new HttpException(ApiController::STATUS_EXCEPTION, "", $fallthrough));
        }
    }

    /**
     * Обновление адреса
     * @Route("/api/v1/account/address/{uid}/default", methods={"PUT"})
     */
    public function addressSetDefault(string $uid): JsonResponse
    {
        try {
            if (empty(ApiController::getCurrentUser())) {
                return $this->createUnauthorizationResponse();
            }

            $error = $this->handler->addressSetDefault($uid);

            if(!empty($error)) {
                return $this->createBadRequestResponse($error);
            }

            return $this->createSuccessResponse('');
        } catch (Exception $fallthrough) {
            return $this->createErrorResponse(new HttpException(ApiController::STATUS_EXCEPTION, "", $fallthrough));
        }
    }

    /**
     * Обновление пароля в ЛК
     * @param Request $request
     * @param UserPasswordHasherInterface $hasher
     * @return JsonResponse
     *
     * @Route("/api/v1/account/update-password", methods={"PUT"})
     */
    public function updatePassword(Request $request, UserPasswordHasherInterface $hasher): JsonResponse
    {
        try {
            if (empty(ApiController::getCurrentUser())) {
                return $this->createUnauthorizationResponse();
            }

            $json = $this->getJsonRequest($request);
            if(is_null($json)) {
                return $this->createBadRequestResponse(ApiError::ERROR_INVALID_JSON);
            }

            if($error = $this->handler->updatePassword($json, $hasher)) {
                return $this->createBadRequestResponse($error);
            }

            return $this->createSuccessResponse('');
        } catch (Exception $fallthrough) {
            return $this->createErrorResponse(new HttpException(ApiController::STATUS_EXCEPTION, "", $fallthrough));
        }
    }

    /**
     * Изменение ФИО пользователя
     * @param Request $request
     * @return JsonResponse
     *
     * @Route("/api/v1/account/update-fio", methods={"PUT"})
     */
    public function updateFio(Request $request): JsonResponse
    {
        try {
            if (empty(ApiController::getCurrentUser())) {
                return $this->createUnauthorizationResponse();
            }

            $json = $this->getJsonRequest($request);
            if(is_null($json)) {
                return $this->createBadRequestResponse(ApiError::ERROR_INVALID_JSON);
            }

            $this->handler->updateFio($json);

            return $this->createSuccessResponse('');
        } catch (Exception $fallthrough) {
            return $this->createErrorResponse(new HttpException(ApiController::STATUS_EXCEPTION, "", $fallthrough));
        }
    }

    /**
     * @param Request $request
     * @return JsonResponse
     * @Route("/api/v1/account/order-list/orders", methods={"GET"})
     */
    public function orderListOrders(Request $request): JsonResponse
    {
        try {
            if (empty(ApiController::getCurrentUser())) {
                return $this->createUnauthorizationResponse();
            }

            $json = new RequestJson($request->query->all());

            $error = null;
            $result = $this->handler->orderListOrders($json, $error);
            if(!is_null($error)) {
                return $this->createBadRequestResponse($error);
            }

            if(empty($result[Params::TOTAL])) {
                return $this->createEmptyResponse();
            }


            $resp = SerializerHelper::serialize($result, [SerializerHelper::GROUP_ACCOUNT_ORDERS]);
            return $this->createSuccessResponse($resp);
        } catch (Exception $fallthrough) {
            return $this->createErrorResponse(new HttpException(ApiController::STATUS_EXCEPTION, "", $fallthrough));
        }
    }

    /**
     * @param Request $request
     * @return JsonResponse
     * @Route("/api/v1/account/order-list/products", methods={"GET"})
     */
    public function orderListProducts(Request $request): JsonResponse
    {
        try {
            if (empty(ApiController::getCurrentUser())) {
                return $this->createUnauthorizationResponse();
            }

            $json = new RequestJson($request->query->all());

            $error = null;
            $result = $this->handler->orderListProducts($json, $error);
            if(!is_null($error)) {
                return $this->createBadRequestResponse($error);
            }

            if(empty($result[Params::TOTAL])) {
                return $this->createEmptyResponse();
            }


            return $this->json($result);
        } catch (Exception $fallthrough) {
            return $this->createErrorResponse(new HttpException(ApiController::STATUS_EXCEPTION, "", $fallthrough));
        }
    }

    /**
     * @param string $uid
     * @return JsonResponse
     * @Route("/api/v1/account/orders/{uid}", methods={"GET"})
     */
    public function orderDetail(string $uid): JsonResponse
    {
        try {
            if (empty(ApiController::getCurrentUser())) {
                return $this->createUnauthorizationResponse();
            }

            $order= $this->handler->orderDetail($uid);

            if(empty($order)) {
                return $this->createBadRequestResponse(ApiError::ERROR_NOT_FOUND);
            }

            $resp = SerializerHelper::serialize($order, [SerializerHelper::GROUP_ACCOUNT_ORDER_DETAIL]);
            return $this->createSuccessResponse($resp);
        } catch (Exception $fallthrough) {
            return $this->createErrorResponse(new HttpException(ApiController::STATUS_EXCEPTION, "", $fallthrough));
        }
    }

    /**
     * Cancel order
     * @param string $uid
     * @return JsonResponse
     * @Route("/api/v1/account/orders/{uid}", methods={"DELETE"})
     */
    public function orderCancel(string $uid): JsonResponse
    {
        try {
            if (empty(ApiController::getCurrentUser())) {
                return $this->createUnauthorizationResponse();
            }

            $error = $this->handler->orderCancel($uid);

            if(!empty($error)) {
                return $this->createBadRequestResponse($error);
            }

            return $this->createSuccessResponse('');
        } catch (Exception $fallthrough) {
            return $this->createErrorResponse(new HttpException(ApiController::STATUS_EXCEPTION, "", $fallthrough));
        }
    }

    /**
     * @return JsonResponse
     * @Route("/api/v1/account/waiting-products", methods={"GET"})
     */
    public function waitingProductsGet(): JsonResponse
    {
        try {
            if (empty(ApiController::getCurrentUser())) {
                return $this->createUnauthorizationResponse();
            }

            $products = ApiController::getCurrentUser()->getWaiting();
            return $this->json($products);
        } catch (Exception $fallthrough) {
            return $this->createErrorResponse(new HttpException(ApiController::STATUS_EXCEPTION, "", $fallthrough));
        }
    }

    /**
     * @param Request $request
     * @return JsonResponse
     * @Route("/api/v1/account/waiting-products", methods={"DELETE"})
     */
    public function waitingProductDelete(Request $request): JsonResponse
    {
        try {
            if (empty(ApiController::getCurrentUser())) {
                return $this->createUnauthorizationResponse();
            }

            $json = $this->getJsonRequest($request);
            if(is_null($json)) {
                return $this->createBadRequestResponse(ApiError::ERROR_INVALID_JSON);
            }

            $error = $this->handler->waitingProductDelete($json);
            if(!is_null($error)) {
                return $this->createBadRequestResponse($error);
            }

            return $this->createSuccessResponse('');
        } catch (Exception $fallthrough) {
            return $this->createErrorResponse(new HttpException(ApiController::STATUS_EXCEPTION, "", $fallthrough));
        }
    }

    /**
     * Подписка на заказ
     * @param Request $request
     * @param string $uid
     * @return JsonResponse
     * @Route("/api/v1/account/orders/{uid}/subscribe", methods={"PUT"})
     */
    public function subscribe(Request $request, string $uid): JsonResponse
    {
        try {
            if (empty(ApiController::getCurrentUser())) {
                return $this->createUnauthorizationResponse();
            }

            $json = $this->getJsonRequest($request);
            if(is_null($json)) {
                return $this->createBadRequestResponse(ApiError::ERROR_INVALID_JSON);
            }

            $error = $this->handler->subscribeOrder($json, $uid);
            if(!is_null($error)) {
                return $this->createBadRequestResponse($error);
            }

            return $this->createSuccessResponse("");
        } catch (Exception $fallthrough) {
            return $this->createErrorResponse(new HttpException(ApiController::STATUS_EXCEPTION, "", $fallthrough));
        }
    }

    /**
     * Отмена подписки на заказ
     * @param string $uid
     * @return JsonResponse
     * @Route("/api/v1/account/orders/{uid}/unsubscribe", methods={"PUT"})
     */
    public function unsubscribe(string $uid): JsonResponse
    {
        try {
            if (empty(ApiController::getCurrentUser())) {
                return $this->createUnauthorizationResponse();
            }

            $error = $this->handler->unsubscribeOrder($uid);
            if(!is_null($error)) {
                return $this->createBadRequestResponse($error);
            }

            return $this->createSuccessResponse("");
        } catch (Exception $fallthrough) {
            return $this->createErrorResponse(new HttpException(ApiController::STATUS_EXCEPTION, "", $fallthrough));
        }
    }

    /**
     * Получение файлов подписанного договора плательщика
     * @param string $uid
     * @return JsonResponse
     * @Route("/api/v1/account/order/{uid}/attached-documents-contractpayer", methods={"GET"})
     */
    public function attachedDocuments(string $uid)
    {
        try {
            if (empty(ApiController::getCurrentUser())) {
                return $this->createUnauthorizationResponse();
            }

            $err = null;
            $link = $this->handler->getAttachedContract($uid, $err);
            if(!empty($err)) {
                return $this->createBadRequestResponse($err);
            }

            if (empty($link)) {
                return $this->createEmptyResponse();
            }

            return $this->json([Params::LINK => $link]);

        } catch (Exception $fallthrough) {
            return $this->createErrorResponse(new HttpException(ApiController::STATUS_EXCEPTION, "", $fallthrough));
        }
    }

    /**
     * @param string $uid
     * @return JsonResponse
     * @Route("/api/v1/account/order/{uid}/generate-document-invoice-payment", methods={"GET"})
     */
    public function invoicePayment(string $uid): JsonResponse
    {
        try {
            if (empty(ApiController::getCurrentUser())) {
                return $this->createUnauthorizationResponse();
            }

            $err = null;
            $link = $this->handler->getInvoicePayment($uid, $err);
            if(!empty($err)) {
                return $this->createBadRequestResponse($err);
            }

            if (empty($link)) {
                return $this->createEmptyResponse();
            }

            return $this->json([Params::LINK => $link]);

        } catch (Exception $fallthrough) {
            return $this->createErrorResponse(new HttpException(ApiController::STATUS_EXCEPTION, "", $fallthrough));
        }
    }

    /**
     * Файлы договора плательщика
     * @param string $uid
     * @return JsonResponse
     * @Route("/api/v1/account/order/{uid}/generate-document-contractpayer", methods={"GET"})
     */
    public function generateDocuments(string $uid): JsonResponse
    {
        try {
            if (empty(ApiController::getCurrentUser())) {
                return $this->createUnauthorizationResponse();
            }

            $err = null;
            $link = $this->handler->getGenerateDocuments($uid, $err);
            if(!empty($err)) {
                return $this->createBadRequestResponse($err);
            }

            if (empty($link)) {
                return $this->createEmptyResponse();
            }

            return $this->json([Params::LINK => $link]);

        } catch (Exception $fallthrough) {
            return $this->createErrorResponse(new HttpException(ApiController::STATUS_EXCEPTION, "", $fallthrough));
        }
    }

    /**
     * Получение файлов ТОРГ-12 по переданному Id заказа, в случае если товары по заказу уже были отгружены
     * @param string $uid
     * @return JsonResponse
     * @Route("/api/v1/account/order/{uid}/generate-document-packing-list", methods={"GET"})
     */
    public function packingDocuments(string $uid): JsonResponse
    {
        try {
            if (empty(ApiController::getCurrentUser())) {
                return $this->createUnauthorizationResponse();
            }

            $err = null;
            $link = $this->handler->getPackingDocuments($uid, $err);
            if(!empty($err)) {
                return $this->createBadRequestResponse($err);
            }

            if (empty($link)) {
                return $this->createEmptyResponse();
            }

            return $this->json([Params::LINK => $link]);

        } catch (Exception $fallthrough) {
            return $this->createErrorResponse(new HttpException(ApiController::STATUS_EXCEPTION, "", $fallthrough));
        }
    }

    /**
     * @return JsonResponse
     * @Route("/api/v1/account/recommendations", methods={"GET"})
     */
    public function recommendations(): JsonResponse
    {
        try {
            if (empty(ApiController::getCurrentUser())) {
                return $this->createUnauthorizationResponse();
            }

            $products = ApiController::getCurrentUser()->getRecommendationProducts();
            return $this->json($products);
        } catch (Exception $fallthrough) {
            return $this->createErrorResponse(new HttpException(ApiController::STATUS_EXCEPTION, "", $fallthrough));
        }
    }
}
