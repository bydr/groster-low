<?php

namespace App\Controller\Front;

use App\ApiError;
use App\Controller\ApiController;
use App\Entity\Category;
use App\Handler\Front\CatalogHandler;
use App\Helpers\Params;
use App\Helpers\SerializerHelper;
use App\Interfaces\Front\CatalogInterface;
use App\RequestJson;
use App\Service\Cache;
use Exception;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class CatalogController
 */
class CatalogController extends ApiController
{
    /** @var CatalogInterface */
    private $handler;

    public function __construct(CatalogHandler $catalogHandler)
    {
        parent::__construct();
        $this->handler = $catalogHandler;
    }

    /**
     * Список всех категорий
     * @Route("/api/v1/catalog/categories", methods={"GET"})
     */
    public function findAllCategories(): Response
    {
        try {
            $jsonResult = $this->handler->findAllCategories();

            return $this->createSuccessResponse($jsonResult);
        } catch (Exception $fallthrough) {
            return $this->createErrorResponse(new HttpException(ApiController::STATUS_EXCEPTION, "", $fallthrough));
        }
    }

    /**
     * Список всех категорий для сферы
     * @Route("/api/v1/catalog/bussiness-area/{uuid}", methods={"GET"})
     */
    public function findAllCategoriesByArea(string $uuid)
    {
        try {
            $jsonResult = $this->handler->findAllCategoriesByArea($uuid);

            return $this->createSuccessResponse($jsonResult);
        } catch (Exception $fallthrough) {
            return $this->createErrorResponse(new HttpException(ApiController::STATUS_EXCEPTION, "", $fallthrough));
        }
    }

    /**
     * Получение страницы "Помощь в выборе" для категории
     * @Route("/api/v1/catalog/category/help-page/{id}", methods={"GET"})
     */
    public function findHelpPageByCategory($id): Response
    {
        try {
            $errorCode = null;
            $result = $this->handler->findHelpPage($id, $errorCode);

            if(!is_null($errorCode)) {
                return $this->createBadRequestResponse($errorCode, ApiController::STATUS_NOT_FOUND);
            }

            $jsonResult = SerializerHelper::serialize($result, [SerializerHelper::GROUP_HELP_PAGE]);
            return $this->createSuccessResponse($jsonResult);

        } catch (Exception $fallthrough) {
            return $this->createErrorResponse(new HttpException(ApiController::STATUS_EXCEPTION, "", $fallthrough));
        }
    }

    /**
     * Список всех тегов
     * @Route("/api/v1/catalog/tags", methods={"GET"})
     */
    public function findAllTags(): Response
    {
        try {
            $result = $this->handler->findAllTags();

            $jsonResult = SerializerHelper::serialize($result, [SerializerHelper::GROUP_TAGS]);
            return $this->createSuccessResponse($jsonResult);

        } catch (Exception $fallthrough) {
            return $this->createErrorResponse(new HttpException(ApiController::STATUS_EXCEPTION, "", $fallthrough));
        }
    }

    /**
     * Данные по тегу
     * @Route("/api/v1/catalog/tags/{id}", methods={"GET"})
     */
    public function findOneTag(int $id): Response
    {
        try {
            $errorCode = null;
            $result = $this->handler->findOneTag($id, $errorCode);

            if(!is_null($errorCode)) {
                return $this->createBadRequestResponse($errorCode, ApiController::STATUS_NOT_FOUND);
            }

            $jsonResult = SerializerHelper::serialize($result, [SerializerHelper::GROUP_TAG]);
            return $this->createSuccessResponse($jsonResult);

        } catch (Exception $fallthrough) {
            return $this->createErrorResponse(new HttpException(ApiController::STATUS_EXCEPTION, "", $fallthrough));
        }
    }

    /**
     * Список всех параметров
     * @Route("/api/v1/catalog/params", methods={"GET"})
     */
    public function findAllParams(): Response
    {
        try {
            $jsonResult = $this->handler->findAllParams();

            return $this->createSuccessResponse($jsonResult);
        } catch (Exception $fallthrough) {
            return $this->createErrorResponse(new HttpException(ApiController::STATUS_EXCEPTION, "", $fallthrough));
        }
    }

    /**
     * Список типов сортировки
     * @Route("/api/v1/catalog/sort-type", methods={"GET"})
     */
    public function getSortType(): Response
    {
        try {
            $types = $this->handler->getSortTypes();

            return $this->json($types);

        } catch (Exception $fallthrough) {
            return $this->createErrorResponse(new HttpException(ApiController::STATUS_EXCEPTION, "", $fallthrough));
        }
    }

    /**
     * Фильтрация товаров в каталоге
     * @Route("/api/v1/catalog", methods={"GET"})
     */
    public function filterProducts(Request $request): Response
    {
        ini_set('memory_limit', -1);

        try {
            $data = $request->query->all();
            $result = $this->handler->filterProducts($data);


            $jsonResponse = SerializerHelper::serialize($result, [SerializerHelper::GROUP_CATALOG]);

            if(empty($result['products'])) {
                return $this->createSuccessResponse($jsonResponse, self::STATUS_EMPTY);
            }

            return $this->createSuccessResponse($jsonResponse);

        } catch (Exception $fallthrough) {
            return $this->createErrorResponse(new HttpException(ApiController::STATUS_EXCEPTION, "", $fallthrough));
        }
    }

    /**
     * @return Response
     * @Route("/api/v1/hits", methods={"GET"})
     */
    public function getHits(): Response
    {
        try {
            $result = $this->handler->getHits();

            if(empty($result)) {
                return $this->createSuccessResponse($result, self::STATUS_EMPTY);
            }

            return $this->createSuccessResponse($result);

        } catch (Exception $fallthrough) {
            return $this->createErrorResponse(new HttpException(ApiController::STATUS_EXCEPTION, "", $fallthrough));
        }
    }

    /**
     * Запрос прайслиста из 1С
     *
     * @param Request $request
     * @return Response
     * @Route("/api/v1/catalog/pricelist", methods={"GET"})
     */
    public function getPricelist(Request $request): Response
    {
        try {
            $json = new RequestJson($request->query->all());

            $err = null;
            $link = $this->handler->pricelist($json, $err);

            if(!empty($err)) {
                return $this->createBadRequestResponse($err);
            }

            return $this->json([Params::LINK => $link]);
        } catch (Exception $fallthrough) {
            return $this->createErrorResponse(new HttpException(ApiController::STATUS_EXCEPTION, "", $fallthrough));
        }
    }
}
