<?php

namespace App\Controller\Admin;

use App\ApiError;
use App\Controller\ApiController;
use App\Handler\Admin\ShopHandler;
use App\Helpers\SerializerHelper;
use App\Interfaces\Admin\ShopInterface;
use Exception;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Routing\Annotation\Route;

/**
 * class ShopController
 * Эндпоинты для магазинов
 */
class ShopController extends AdminController
{
    /** @var ShopInterface */
    private $handler;

    public function __construct(ShopHandler $handler)
    {
        parent::__construct();
        $this->handler = $handler;
    }

    /**
     * @return JsonResponse
     *
     * @Route("/api/v1/admin/shops", methods={"GET"})
     */
    public function getAllActive(): JsonResponse
    {
        try {
            $shops = $this->handler->getAllActive();

            $jsonResult = SerializerHelper::serialize($shops, [SerializerHelper::GROUP_ADMIN_SHOP_LIST]);
            return $this->createSuccessResponse($jsonResult);
        }catch (Exception $fallthrough) {
            return $this->createErrorResponse(new HttpException(ApiController::STATUS_EXCEPTION, "", $fallthrough));
        }
    }

    /**
     * @param string $uuid
     * @return JsonResponse
     *
     * @Route("/api/v1/admin/shops/{uuid}", methods={"GET"})
     */
    public function getOne(string $uuid): JsonResponse
    {
        try {
            if(empty($uuid)) {
                return $this->createBadRequestResponse();
            }

            $shop = $this->handler->getOne($uuid);

            if(empty($shop)) {
                return $this->createBadRequestResponse();
            }

            $jsonResult = SerializerHelper::serialize($shop, [SerializerHelper::GROUP_ADMIN_SHOP]);
            return $this->createSuccessResponse($jsonResult);
        }catch (Exception $fallthrough) {
            return $this->createErrorResponse(new HttpException(ApiController::STATUS_EXCEPTION, "", $fallthrough));
        }
    }

    /**
     * Обновление данных магазина
     * @param string $uuid
     * @param Request $request
     * @return JsonResponse
     *
     * @Route("/api/v1/admin/shops/{uuid}", methods={"PUT"})
     */
    public function update(string $uuid, Request $request): JsonResponse
    {
        try {
            $json = $this->getJsonRequest($request);
            if(is_null($json) || empty($uuid)) {
                return $this->createBadRequestResponse(ApiError::ERROR_INVALID_JSON);
            }

            $shop = $this->handler->update($uuid, $json);
            if(is_null($shop)) {
                return $this->createBadRequestResponse();
            }

            $jsonResponse = SerializerHelper::serialize($shop, [SerializerHelper::GROUP_ADMIN_SHOP]);
            return $this->createSuccessResponse($jsonResponse);
        }catch (Exception $fallthrough) {
            return $this->createErrorResponse(new HttpException(ApiController::STATUS_EXCEPTION, "", $fallthrough));
        }
    }
}