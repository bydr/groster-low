<?php

namespace App\Controller\Admin;

use App\ApiError;
use App\Handler\Admin\CategoryHandler;
use App\Handler\Admin\FaqHandler;
use App\Helpers\SerializerHelper;
use App\Interfaces\Admin\CategoryInterface;
use App\Interfaces\Admin\FaqInterface;
use Exception;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Работа с блоками вопрос-ответ
 */
class FaqController extends AdminController
{
    /** @var FaqInterface */
    private $handler;

    public function __construct(FaqHandler $handler)
    {
        parent::__construct();
        $this->handler = $handler;
    }

    /**
     * @return JsonResponse
     *
     * @Route("/api/v1/admin/faq/blocks", methods={"GET"})
     */
    public function allBlocks(): JsonResponse
    {
        try {
            $result = $this->handler->allBlocks();

            if(empty($result)){
                return $this->createEmptyResponse();
            }

            $jsonResult = SerializerHelper::serialize($result, [SerializerHelper::GROUP_ADMIN_FAQ_BLOCK]);
            return $this->createSuccessResponse($jsonResult);

        } catch (Exception $fallthrough) {
            return $this->createErrorResponse(new HttpException(self::STATUS_EXCEPTION, "", $fallthrough));
        }
    }

    /**
     * @param Request $request
     * @return JsonResponse
     *
     * @Route("/api/v1/admin/faq/block/add", methods={"POST"})
     */
    public function addBlock(Request $request): JsonResponse
    {
        try {
            $json = $this->getJsonRequest($request);
            if(is_null($json)) {
                return $this->createBadRequestResponse();
            }

            $error = $this->handler->updateBlock($json, 0);

            if(!is_null($error)){
                return $this->createBadRequestResponse($error);
            }

            return $this->createSuccessResponse("");

        } catch (Exception $fallthrough) {
            return $this->createErrorResponse(new HttpException(self::STATUS_EXCEPTION, "", $fallthrough));
        }
    }

    /**
     * @param Request $request
     * @param int $id
     * @return JsonResponse
     *
     * @Route("/api/v1/admin/faq/block/{id}", methods={"PUT"})
     */
    public function updateBlock(Request $request, int $id): JsonResponse
    {
        try {
            $json = $this->getJsonRequest($request);
            if(is_null($json)) {
                return $this->createBadRequestResponse();
            }

            $error = $this->handler->updateBlock($json, $id);

            if(!is_null($error)){
                return $this->createBadRequestResponse($error);
            }

            return $this->createSuccessResponse("");

        } catch (Exception $fallthrough) {
            return $this->createErrorResponse(new HttpException(self::STATUS_EXCEPTION, "", $fallthrough));
        }
    }

    /**
     * @param int $id
     * @return JsonResponse
     *
     * @Route("/api/v1/admin/faq/block/{id}", methods={"DELETE"})
     */
    public function deleteBlock(int $id): JsonResponse
    {
        try {
            $this->handler->deleteBlock($id);

            return $this->createSuccessResponse("");

        } catch (Exception $fallthrough) {
            return $this->createErrorResponse(new HttpException(self::STATUS_EXCEPTION, "", $fallthrough));
        }
    }

    /**
     * @return JsonResponse
     *
     * @Route("/api/v1/admin/faq/questions", methods={"GET"})
     */
    public function allQuestions(): JsonResponse
    {
        try {
            $result = $this->handler->allQuestions();

            if(empty($result)){
                return $this->createEmptyResponse();
            }

            $jsonResult = SerializerHelper::serialize($result, [SerializerHelper::GROUP_ADMIN_FAQ]);
            return $this->createSuccessResponse($jsonResult);

        } catch (Exception $fallthrough) {
            return $this->createErrorResponse(new HttpException(self::STATUS_EXCEPTION, "", $fallthrough));
        }
    }

    /**
     * @param Request $request
     * @return JsonResponse
     *
     * @Route("/api/v1/admin/faq/question/add", methods={"POST"})
     */
    public function addQuestion(Request $request): JsonResponse
    {
        try {
            $json = $this->getJsonRequest($request);
            if(is_null($json)) {
                return $this->createBadRequestResponse();
            }

            $error = $this->handler->updateQuestion($json, 0);

            if(!is_null($error)){
                return $this->createBadRequestResponse($error);
            }

            return $this->createSuccessResponse("");

        } catch (Exception $fallthrough) {
            return $this->createErrorResponse(new HttpException(self::STATUS_EXCEPTION, "", $fallthrough));
        }
    }

    /**
     * @param Request $request
     * @param int $id
     * @return JsonResponse
     *
     * @Route("/api/v1/admin/faq/question/{id}", methods={"PUT"})
     */
    public function updateQuestion(Request $request, int $id): JsonResponse
    {
        try {
            $json = $this->getJsonRequest($request);
            if(is_null($json)) {
                return $this->createBadRequestResponse();
            }

            $error = $this->handler->updateQuestion($json, $id);

            if(!is_null($error)){
                return $this->createBadRequestResponse($error);
            }

            return $this->createSuccessResponse("");

        } catch (Exception $fallthrough) {
            return $this->createErrorResponse(new HttpException(self::STATUS_EXCEPTION, "", $fallthrough));
        }
    }

    /**
     * @param int $id
     * @return JsonResponse
     *
     * @Route("/api/v1/admin/faq/question/{id}", methods={"DELETE"})
     */
    public function deleteQuestion(int $id): JsonResponse
    {
        try {
            $this->handler->deleteQuestion($id);

            return $this->createSuccessResponse("");

        } catch (Exception $fallthrough) {
            return $this->createErrorResponse(new HttpException(self::STATUS_EXCEPTION, "", $fallthrough));
        }
    }

}