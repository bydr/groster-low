<?php

namespace App\Controller\Admin;

use App\ApiError;
use App\Controller\ApiController;
use App\Handler\Admin\CategoryHandler;
use App\Helpers\SerializerHelper;
use App\Interfaces\Admin\CategoryInterface;
use Exception;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Routing\Annotation\Route;

class CategoryController extends AdminController
{
    /** @var CategoryInterface */
    private $handler;

    public function __construct(CategoryHandler $handler)
    {
        parent::__construct();
        $this->handler = $handler;
    }

    /**
     * Обновить данные по категории
     * @Route("/api/v1/admin/category/{id}", methods={"GET"})
     */
    public function findOne(int $id): Response
    {
        try {
            $result = $this->handler->findOne($id);

            if(empty($result)){
                return $this->createBadRequestResponse(ApiError::ERROR_NOT_FOUND, self::STATUS_NOT_FOUND);
            }

            $jsonResult = SerializerHelper::serialize($result, [SerializerHelper::GROUP_ADMIN_CATEGORY]);
            return $this->createSuccessResponse($jsonResult);

        } catch (Exception $fallthrough) {
            return $this->createErrorResponse(new HttpException(self::STATUS_EXCEPTION, "", $fallthrough));
        }
    }

    /**
     * Обновить данные по категории
     * @Route("/api/v1/admin/category/{id}", methods={"PUT"})
     */
    public function updateOne(Request $request, int $id): Response
    {
        try {
            $jsonRequest = $this->getJsonRequest($request);
            if(is_null($jsonRequest)) {
                return $this->createBadRequestResponse(ApiError::ERROR_INVALID_JSON);
            }
            $errorCode = null;
            $this->handler->updateOne($jsonRequest, $id, $errorCode);

            if(!is_null($errorCode)){
                return $this->createBadRequestResponse($errorCode);
            }

            return $this->createSuccessResponse("");
        } catch (Exception $fallthrough) {
            return $this->createErrorResponse(new HttpException(self::STATUS_EXCEPTION, "", $fallthrough));

        }
    }
}