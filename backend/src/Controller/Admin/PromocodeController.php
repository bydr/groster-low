<?php

namespace App\Controller\Admin;

use App\ApiError;
use App\Controller\ApiController;
use App\Entity\Tag;
use App\Handler\Admin\PromocodeHandler;
use App\Handler\Admin\TagHandler;
use App\Helpers\Params;
use App\Helpers\SerializerHelper;
use App\Interfaces\Admin\CategoryInterface;
use App\Interfaces\Admin\PromocodeInterface;
use App\RequestJson;
use Exception;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Эндпоинты для promocode.yml
 */
class PromocodeController extends AdminController
{
    /** @var PromocodeInterface */
    private $handler;

    public function __construct(PromocodeHandler $handler)
    {
        parent::__construct();
        $this->handler = $handler;
    }

    /**
     * Список скидочных купонов
     * @Route("/api/v1/admin/promocodes", methods={"GET"})
     */
    public function findAll(): Response
    {
        try {
            $promos = $this->handler->list();
            if(empty($promos)){
                return $this->createSuccessResponse(self::STATUS_EMPTY);
            }

            $jsonResponse = SerializerHelper::serialize($promos, [SerializerHelper::GROUP_ADMIN_PROMO_LIST]);
            return $this->createSuccessResponse($jsonResponse);
        } catch (Exception $fallthrough) {
            return $this->createErrorResponse(new HttpException(self::STATUS_EXCEPTION, "", $fallthrough));

        }
    }

    /**
     * Добавить промокод
     * @Route("/api/v1/admin/promocode/add", methods={"POST"})
     */
    public function create(Request $request): Response
    {
        try {
            $jsonRequest = $this->getJsonRequest($request);
            if(is_null($jsonRequest)) {
                return $this->createBadRequestResponse(ApiError::ERROR_INVALID_JSON);
            }

            $promocode = $this->handler->create($jsonRequest);
            if(is_null($promocode)){
                return $this->createBadRequestResponse(ApiError::ERROR_INVALID_DATA);
            }

            $jsonResponse = SerializerHelper::serialize($promocode, [SerializerHelper::GROUP_ADMIN_PROMO]);
            return $this->createSuccessResponse($jsonResponse);
        } catch (Exception $fallthrough) {
            return $this->createErrorResponse(new HttpException(self::STATUS_EXCEPTION, "", $fallthrough));
        }
    }

    /**
     * Получить данные по промокоду
     * @Route("/api/v1/admin/promocode/{id}", methods={"GET"})
     */
    public function getOne(int $id): Response
    {
        try {
            $promocode = $this->handler->getOne($id);
            if(is_null($promocode)){
                return $this->createBadRequestResponse(ApiError::ERROR_INVALID_PROMOCODE, self::STATUS_NOT_FOUND);
            }

            $jsonResponse = SerializerHelper::serialize($promocode, [SerializerHelper::GROUP_ADMIN_PROMO]);
            return $this->createSuccessResponse($jsonResponse);
        } catch (Exception $fallthrough) {
            return $this->createErrorResponse(new HttpException(self::STATUS_EXCEPTION, "", $fallthrough));
        }
    }

    /**
     * Добавить промокод
     * @Route("/api/v1/admin/promocode/{id}", methods={"PUT"})
     */
    public function updateOne(Request $request, int $id): Response
    {
        try {
            $jsonRequest = $this->getJsonRequest($request);
            if(is_null($jsonRequest)) {
                return $this->createBadRequestResponse(ApiError::ERROR_INVALID_JSON);
            }

            $error = null;
            $promocode = $this->handler->updateOne($id, $jsonRequest, $error);
            if(!is_null($error)){
                return $this->createBadRequestResponse($error);
            }

            $jsonResponse = SerializerHelper::serialize($promocode, [SerializerHelper::GROUP_ADMIN_PROMO]);
            return $this->createSuccessResponse($jsonResponse);
        } catch (Exception $fallthrough) {
            return $this->createErrorResponse(new HttpException(self::STATUS_EXCEPTION, "", $fallthrough));
        }
    }
}