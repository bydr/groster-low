<?php

namespace App\Controller\Admin;

use App\ApiError;
use App\Controller\ApiController;
use App\Entity\Tag;
use App\Handler\Admin\TagHandler;
use App\Helpers\SerializerHelper;
use App\Interfaces\Admin\CategoryInterface;
use Exception;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Routing\Annotation\Route;

class TagController extends AdminController
{
    /** @var CategoryInterface */
    private $handler;

    public function __construct(TagHandler $handler)
    {
        parent::__construct();
        $this->handler = $handler;
    }

    /**
     * Добавить тег
     * @Route("/api/v1/admin/tag/add", methods={"POST"})
     */
    public function addOne(Request $request): JsonResponse
    {
        try {
            $jsonRequest = $this->getJsonRequest($request);
            if(is_null($jsonRequest)) {
                return $this->createBadRequestResponse(ApiError::ERROR_INVALID_JSON);
            }

            $tag = $this->handler->updateOne($jsonRequest, 0);
            if(is_null($tag)){
                return $this->createBadRequestResponse(ApiError::ERROR_NOT_FOUND);
            }

            $jsonResponse = SerializerHelper::serialize($tag, [SerializerHelper::GROUP_TAGS]);
            return $this->createSuccessResponse($jsonResponse);

        } catch (Exception $fallthrough) {
            return $this->createErrorResponse(new HttpException(self::STATUS_EXCEPTION, "", $fallthrough));

        }
    }

    /**
     * Обновить данные по тегу
     * @Route("/api/v1/admin/tags/{id}", methods={"PUT"})
     */
    public function updateOne(Request $request, int $id): Response
    {
        try {
            $jsonRequest = $this->getJsonRequest($request);
            if(is_null($jsonRequest)) {
                return $this->createBadRequestResponse(ApiError::ERROR_INVALID_JSON);
            }

            $tag = $this->handler->updateOne($jsonRequest, $id);
            if(is_null($tag)){
                return $this->createBadRequestResponse(ApiError::ERROR_NOT_FOUND);
            }

            return $this->createSuccessResponse("");
        } catch (Exception $fallthrough) {
            return $this->createErrorResponse(new HttpException(self::STATUS_EXCEPTION, "", $fallthrough));
        }
    }

    /**
     * Удалить тег
     * @Route("/api/v1/admin/tags/{id}", methods={"DELETE"})
     */
    public function deleteOne(int $id): Response
    {
        try {
            $this->handler->deleteOne($id);

            return $this->createSuccessResponse("");

        } catch (Exception $fallthrough) {
            return $this->createErrorResponse(new HttpException(self::STATUS_EXCEPTION, "", $fallthrough));
        }
    }

}