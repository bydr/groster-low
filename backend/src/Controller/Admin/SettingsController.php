<?php

namespace App\Controller\Admin;

use App\ApiError;
use App\Handler\Admin\SettingsHandler;
use App\Helpers\SerializerHelper;
use App\Interfaces\Admin\SettingsInterface;
use Exception;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Общие настройки
 */
class SettingsController extends AdminController
{
    /** @var SettingsInterface */
    private $handler;

    public function __construct(SettingsHandler $handler)
    {
        parent::__construct();
        $this->handler = $handler;
    }

    /**
     * Обновить данные по категории
     * @Route("/api/v1/admin/settings", methods={"GET"})
     */
    public function getSettings(): Response
    {
        try {
            $settings = $this->handler->getSettings();

            if(empty($settings)){
                return $this->createBadRequestResponse(ApiError::ERROR_NOT_FOUND, self::STATUS_NOT_FOUND);
            }

            $jsonResult = SerializerHelper::serialize($settings, [SerializerHelper::GROUP_SETTINGS]);
            return $this->createSuccessResponse($jsonResult);
        } catch (Exception $fallthrough) {
            return $this->createErrorResponse(new HttpException(self::STATUS_EXCEPTION, "", $fallthrough));
        }
    }

    /**
     * Обновить данные по категории
     * @Route("/api/v1/admin/settings", methods={"PUT"})
     */
    public function update(Request $request): Response
    {
        try {
            $jsonRequest = $this->getJsonRequest($request);
            if(is_null($jsonRequest)) {
                return $this->createBadRequestResponse(ApiError::ERROR_INVALID_JSON);
            }

            $error = $this->handler->update($jsonRequest);

            if(!is_null($error)){
                return $this->createBadRequestResponse($error);
            }

            return $this->createSuccessResponse("");
        } catch (Exception $fallthrough) {
            return $this->createErrorResponse(new HttpException(self::STATUS_EXCEPTION, "", $fallthrough));

        }
    }
}