<?php

namespace App\Controller\Admin;

use App\ApiError;
use App\Controller\ApiController;
use App\Handler\Admin\ShippingMethodHandler;
use App\Helpers\Params;
use App\Helpers\SerializerHelper;
use App\Interfaces\Admin\ShippingMethodInterface;
use Exception;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Способы доставки
 */
class ShippingMethodController extends AdminController
{
    /** @var ShippingMethodInterface */
    private $handler;

    public function __construct(ShippingMethodHandler $handler)
    {
        parent::__construct();
        $this->handler = $handler;
    }

    /**
     * Список доступных методов доставки
     *
     * @return JsonResponse
     * @Route("/api/v1/admin/shipping-methods", methods={"GET"})
     */
    public function getAll(): JsonResponse
    {
        try {
            if (empty(ApiController::getCurrentUser())) {
                return $this->createUnauthorizationResponse();
            }

            $methods = $this->handler->getAll();
            if(empty($methods)) {
                return $this->createEmptyResponse();
            }

            $jsonResp = SerializerHelper::serialize($methods, [SerializerHelper::GROUP_ADMIN_SHIPPING_METHOD_LIST]);
            return $this->createSuccessResponse($jsonResp);
        }catch (Exception $exception) {
            return $this->createErrorResponse(new HttpException(ApiController::STATUS_EXCEPTION, "", $exception));
        }
    }

    /**
     * Добавление метода доставки
     *
     * @param Request $request
     * @return JsonResponse
     * @Route("/api/v1/admin/shipping-method/add", methods={"POST"})
     */
    public function methodAdd(Request $request): JsonResponse
    {
        try {
            if (empty(ApiController::getCurrentUser())) {
                return $this->createUnauthorizationResponse();
            }

            $json = $this->getJsonRequest($request);
            if (is_null($json)) {
                return $this->createBadRequestResponse(ApiError::ERROR_INVALID_JSON);
            }

            $error = null;
            $alias = $this->handler->methodUpdate($json, null, $error);

            if(!is_null($error)) {
                return $this->createBadRequestResponse($error);
            }

            if(empty($alias)) {
                return $this->createBadRequestResponse(ApiError::ERROR_INVALID_JSON);
            }

            return $this->json([Params::ALIAS => $alias]);
        }catch (Exception $exception) {
            return $this->createErrorResponse(new HttpException(ApiController::STATUS_EXCEPTION, "", $exception));
        }
    }

    /**
     * Изменение данных метода доставки
     *
     * @param Request $request
     * @param string $alias
     * @return JsonResponse
     * @Route("/api/v1/admin/shipping-method/{alias}", methods={"PUT"})
     */
    public function methodUpdate(Request $request, string $alias): JsonResponse
    {
        try {
            if (empty(ApiController::getCurrentUser())) {
                return $this->createUnauthorizationResponse();
            }

            $json = $this->getJsonRequest($request);
            if (is_null($json)) {
                return $this->createBadRequestResponse(ApiError::ERROR_INVALID_JSON);
            }

            $error = null;
            $res = $this->handler->methodUpdate($json, $alias, $error);

            if(!is_null($error)) {
                return $this->createBadRequestResponse($error);
            }

            if(empty($res)) {
                return $this->createBadRequestResponse(ApiError::ERROR_INVALID_JSON);
            }

            return $this->json([Params::ALIAS => $res]);
        }catch (Exception $exception) {
            return $this->createErrorResponse(new HttpException(ApiController::STATUS_EXCEPTION, "", $exception));
        }
    }

    /**
     * Получение данных метода доставки
     *
     * @param string $alias
     * @return JsonResponse
     * @Route("/api/v1/admin/shipping-method/{alias}", methods={"GET"})
     */
    public function methodGet(string $alias): JsonResponse
    {
        try {
            if (empty(ApiController::getCurrentUser())) {
                return $this->createUnauthorizationResponse();
            }

            $method = $this->handler->methodGet($alias);

            if(empty($method)) {
                return $this->createBadRequestResponse(ApiError::ERROR_NOT_FOUND);
            }

            $respJson = SerializerHelper::serialize($method, [SerializerHelper::GROUP_ADMIN_SHIPPING_METHOD]);
            return $this->createSuccessResponse($respJson);
        }catch (Exception $exception) {
            return $this->createErrorResponse(new HttpException(ApiController::STATUS_EXCEPTION, "", $exception));
        }
    }

    /**
     * Список ограничений для способа доставки
     *
     * @param string $alias
     * @return JsonResponse
     * @Route("/api/v1/admin/shipping-method/{alias}/limit", methods={"GET"})
     */
    public function methodLimitsAll(string $alias): JsonResponse
    {
        try {
            if (empty(ApiController::getCurrentUser())) {
                return $this->createUnauthorizationResponse();
            }

            $limits = $this->handler->methodLimitsAll($alias);

            if(empty($limits)) {
                return $this->createEmptyResponse();
            }

            $respJson = SerializerHelper::serialize($limits, [SerializerHelper::GROUP_ADMIN_SHIPPING_METHOD_LIMIT_LIST]);
            return $this->createSuccessResponse($respJson);
        }catch (Exception $exception) {
            return $this->createErrorResponse(new HttpException(ApiController::STATUS_EXCEPTION, "", $exception));
        }
    }

    /**
     * Добавление ограничения для способа доставки
     *
     * @param Request $request
     * @param string $alias
     * @return JsonResponse
     *
     * @Route("/api/v1/admin/shipping-method/{alias}/limit", methods={"POST"})
     */
    public function methodLimitAdd(Request $request, string $alias): JsonResponse
    {
        try {
            if (empty(ApiController::getCurrentUser())) {
                return $this->createUnauthorizationResponse();
            }

            $json = $this->getJsonRequest($request);
            if (is_null($json)) {
                return $this->createBadRequestResponse(ApiError::ERROR_INVALID_JSON);
            }

            $error = null;
            $res = $this->handler->methodLimitUpdate($json, $alias, 0, $error);

            if(!is_null($error)) {
                return $this->createBadRequestResponse($error);
            }

            if(empty($res)) {
                return $this->createBadRequestResponse(ApiError::ERROR_INVALID_JSON);
            }

            return $this->json([Params::ID => $res]);
        }catch (Exception $exception) {
            return $this->createErrorResponse(new HttpException(ApiController::STATUS_EXCEPTION, "", $exception));
        }
    }

    /**
     * Обновление ограничения для способа доставки
     *
     * @param Request $request
     * @param string $alias
     * @param int $id
     * @return JsonResponse
     *
     * @Route("/api/v1/admin/shipping-method/{alias}/limit/{id}", methods={"PUT"})
     */
    public function methodLimitUpdate(Request $request, string $alias, int $id): JsonResponse
    {
        try {
            if (empty(ApiController::getCurrentUser())) {
                return $this->createUnauthorizationResponse();
            }

            $json = $this->getJsonRequest($request);
            if (is_null($json)) {
                return $this->createBadRequestResponse(ApiError::ERROR_INVALID_JSON);
            }

            $error = null;
            $res = $this->handler->methodLimitUpdate($json, $alias, $id, $error);

            if(!is_null($error)) {
                return $this->createBadRequestResponse($error);
            }

            if(empty($res)) {
                return $this->createBadRequestResponse(ApiError::ERROR_INVALID_JSON);
            }

            return $this->json([Params::ID => $res]);
        }catch (Exception $exception) {
            return $this->createErrorResponse(new HttpException(ApiController::STATUS_EXCEPTION, "", $exception));
        }
    }

    /**
     * Удаление ограничения для способа доставки
     *
     * @param string $alias
     * @param int $id
     * @return JsonResponse
     *
     * @Route("/api/v1/admin/shipping-method/{alias}/limit/{id}", methods={"DELETE"})
     */
    public function methodLimitDelete(string $alias, int $id): JsonResponse
    {
        try {
            if (empty(ApiController::getCurrentUser())) {
                return $this->createUnauthorizationResponse();
            }

            if(!$this->handler->methodLimitDelete($alias, $id)) {
                return $this->createBadRequestResponse(ApiError::ERROR_INVALID_DATA);
            }

            return $this->createSuccessResponse("");
        }catch (Exception $exception) {
            return $this->createErrorResponse(new HttpException(ApiController::STATUS_EXCEPTION, "", $exception));
        }
    }
}