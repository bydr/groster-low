<?php

namespace App\Controller\Admin;

use App\ApiError;
use App\Handler\Admin\OrderHandler;
use App\Helpers\SerializerHelper;
use App\Interfaces\Admin\OrderInterface;
use Exception;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Заказы
 */
class OrderController extends AdminController
{
    /** @var OrderInterface */
    private $handler;

    public function __construct(OrderHandler $handler)
    {
        parent::__construct();
        $this->handler = $handler;
    }

    /**
     * Обновить данные по категории
     * @Route("/api/v1/admin/replacement/add", methods={"POST"})
     */
    public function replacementAdd(Request $request): JsonResponse
    {
        try {
            $jsonRequest = $this->getJsonRequest($request);
            if(is_null($jsonRequest)) {
                return $this->createBadRequestResponse(ApiError::ERROR_INVALID_JSON);
            }

            $id = $this->handler->replacementUpdate($jsonRequest, 0);
            if(0 == $id){
                return $this->createBadRequestResponse(ApiError::ERROR_NOT_FOUND, self::STATUS_NOT_FOUND);
            }

            return $this->json(['id' => $id]);
        } catch (Exception $fallthrough) {
            return $this->createErrorResponse(new HttpException(self::STATUS_EXCEPTION, "", $fallthrough));
        }
    }

    /**
     * Обновить данные по категории
     * @Route("/api/v1/admin/replacement/{id}", methods={"PUT"})
     */
    public function replacementUpdate(Request $request, int $id): Response
    {
        try {
            $jsonRequest = $this->getJsonRequest($request);
            if(is_null($jsonRequest) || $id < 1) {
                return $this->createBadRequestResponse(ApiError::ERROR_INVALID_JSON);
            }

            if(!$this->handler->replacementUpdate($jsonRequest, $id)){
                return $this->createBadRequestResponse(ApiError::ERROR_NOT_FOUND, self::STATUS_NOT_FOUND);
            }

            return $this->json(['id' => $id]);
        } catch (Exception $fallthrough) {
            return $this->createErrorResponse(new HttpException(self::STATUS_EXCEPTION, "", $fallthrough));

        }
    }
}