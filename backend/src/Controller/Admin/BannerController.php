<?php

namespace App\Controller\Admin;

use App\ApiError;
use App\Controller\ApiController;
use App\Handler\Admin\BannerHandler;
use App\Helpers\SerializerHelper;
use App\Interfaces\Admin\BannerInterface;
use Exception;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Баннеры
 */
class BannerController extends AdminController
{
    /** @var BannerInterface */
    private $handler;

    public function __construct(BannerHandler $handler)
    {
        parent::__construct();
        $this->handler = $handler;
    }

    /**
     * Список баннеров
     * @Route("/api/v1/admin/pictures", methods={"GET"})
     */
    public function banners(): JsonResponse
    {
        try {
            $banners = $this->handler->getAll();

            $json = SerializerHelper::serialize($banners, [SerializerHelper::GROUP_ADMIN_BANNERS]);
            return $this->createSuccessResponse($json);
        } catch (Exception $fallthrough) {
            return $this->createErrorResponse(new HttpException(ApiController::STATUS_EXCEPTION, "", $fallthrough));
        }
    }

    /**
     * Добавление баннера
     * @param Request $request
     * @return JsonResponse
     * @Route("/api/v1/admin/pictures", methods={"POST"})
     */
    public function add(Request $request): JsonResponse
    {
        try {
            $json = $this->getJsonRequest($request);
            if(is_null($json)) {
                return $this->createBadRequestResponse(ApiError::ERROR_INVALID_JSON);
            }

            $error = $this->handler->add($json);
            if(!is_null($error)) {
                return $this->createBadRequestResponse($error);
            }

            return $this->createSuccessResponse("");
        } catch (Exception $fallthrough) {
            return $this->createErrorResponse(new HttpException(ApiController::STATUS_EXCEPTION, "", $fallthrough));
        }
    }

    /**
     * Обновление баннера
     * @param Request $request
     * @param int $id
     * @return JsonResponse
     * @Route("/api/v1/admin/pictures/{id}", methods={"PUT"})
     */
    public function update(Request $request, int $id): JsonResponse
    {
        try {
            $json = $this->getJsonRequest($request);
            if(is_null($json)) {
                return $this->createBadRequestResponse(ApiError::ERROR_INVALID_JSON);
            }

            $error = $this->handler->update($json, $id);
            if(!is_null($error)) {
                return $this->createBadRequestResponse($error);
            }

            return $this->createSuccessResponse("");
        } catch (Exception $fallthrough) {
            return $this->createErrorResponse(new HttpException(ApiController::STATUS_EXCEPTION, "", $fallthrough));
        }
    }

    /**
     * Удаление баннера
     * @param int $id
     * @return JsonResponse
     * @Route("/api/v1/admin/pictures/{id}", methods={"DELETE"})
     */
    public function delete(int $id): JsonResponse
    {
        try {
            $error = $this->handler->delete($id);
            if(!is_null($error)) {
                return $this->createBadRequestResponse($error);
            }

            return $this->createSuccessResponse("");
        } catch (Exception $fallthrough) {
            return $this->createErrorResponse(new HttpException(ApiController::STATUS_EXCEPTION, "", $fallthrough));
        }
    }
}
