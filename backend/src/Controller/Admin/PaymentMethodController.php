<?php

namespace App\Controller\Admin;

use App\ApiError;
use App\Controller\ApiController;
use App\Handler\Admin\PaymentMethodHandler;
use App\Helpers\Params;
use App\Helpers\SerializerHelper;
use App\Interfaces\Admin\PaymentMethodInterface;
use Exception;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Способы оплаты
 */
class PaymentMethodController extends AdminController
{
    /** @var PaymentMethodInterface */
    private $handler;

    public function __construct(PaymentMethodHandler $handler)
    {
        parent::__construct();
        $this->handler = $handler;
    }

    /**
     * Список доступных методов доставки
     *
     * @return JsonResponse
     * @Route("/api/v1/admin/payment-methods", methods={"GET"})
     */
    public function getAll(): JsonResponse
    {
        try {
            if (empty(ApiController::getCurrentUser())) {
                return $this->createUnauthorizationResponse();
            }

            $methods = $this->handler->getAll();
            if(empty($methods)) {
                return $this->createEmptyResponse();
            }

            $jsonResp = SerializerHelper::serialize($methods, [SerializerHelper::GROUP_ADMIN_PAYMENT_METHOD_LIST]);
            return $this->createSuccessResponse($jsonResp);
        }catch (Exception $exception) {
            return $this->createErrorResponse(new HttpException(ApiController::STATUS_EXCEPTION, "", $exception));
        }
    }

    /**
     * Добавление метода доставки
     *
     * @param Request $request
     * @return JsonResponse
     * @Route("/api/v1/admin/payment-method/add", methods={"POST"})
     */
    public function methodAdd(Request $request): JsonResponse
    {
        try {
            if (empty(ApiController::getCurrentUser())) {
                return $this->createUnauthorizationResponse();
            }

            $json = $this->getJsonRequest($request);
            if (is_null($json)) {
                return $this->createBadRequestResponse(ApiError::ERROR_INVALID_JSON);
            }

            $error = null;
            $alias = $this->handler->methodUpdate($json, null, $error);

            if(!is_null($error)) {
                return $this->createBadRequestResponse($error);
            }

            if(empty($alias)) {
                return $this->createBadRequestResponse(ApiError::ERROR_INVALID_JSON);
            }

            return $this->json([Params::UID => $alias]);
        }catch (Exception $exception) {
            return $this->createErrorResponse(new HttpException(ApiController::STATUS_EXCEPTION, "", $exception));
        }
    }

    /**
     * Изменение данных метода доставки
     *
     * @param Request $request
     * @param string $uid
     * @return JsonResponse
     * @Route("/api/v1/admin/payment-method/{uid}", methods={"PUT"})
     */
    public function methodUpdate(Request $request, string $uid): JsonResponse
    {
        try {
            if (empty(ApiController::getCurrentUser())) {
                return $this->createUnauthorizationResponse();
            }

            $json = $this->getJsonRequest($request);
            if (is_null($json)) {
                return $this->createBadRequestResponse(ApiError::ERROR_INVALID_JSON);
            }

            $error = null;
            $res = $this->handler->methodUpdate($json, $uid, $error);

            if(!is_null($error)) {
                return $this->createBadRequestResponse($error);
            }

            if(empty($res)) {
                return $this->createBadRequestResponse(ApiError::ERROR_INVALID_JSON);
            }

            return $this->json([Params::UID => $res]);
        }catch (Exception $exception) {
            return $this->createErrorResponse(new HttpException(ApiController::STATUS_EXCEPTION, "", $exception));
        }
    }
}