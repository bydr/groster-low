<?php

namespace App\Controller\Security;

use App\Controller\Security\BaseSecurityController;
use App\Entity\Security\AuthUser;
use App\Repository\AuthUserRepository;
use Firebase\JWT\JWT;
use phpDocumentor\Reflection\Types\This;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;

class LogoutController extends BaseSecurityController
{
    /**
     * @Route("/api/v1/auth/logout", name="logout"), methods={"GET", "POST"}
     */
    public function endpoint(Request $request): JsonResponse
    {
        if ($this->checkAuth($request)) {
            $this->logout($request);
        }
        return $this->json([]);
    }
}
