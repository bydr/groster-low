<?php

namespace App\Controller\Security;

use App\Entity\Param;
use App\Helpers\Params;
use App\Helpers\SerializerHelper;
use Exception;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Routing\Annotation\Route;

class AuthInfoController extends BaseSecurityController
{
    /**
     * @Route("/api/v1/auth/info", name="info_auth_data", methods={"GET"})
     */
    public function endpoint(Request $request): Response
    {
        try {
            if (!$this->checkAuth($request)) {
                return $this->createUnauthorizationResponse();
            }
            $user = $this->get('security.token_storage')->getToken()->getUser();

            if(empty($user)) {
                return $this->createUnauthorizationResponse();
            }

            $resp = SerializerHelper::serialize($user, [SerializerHelper::GROUP_AUTH_INFO]);
            return $this->createSuccessResponse($resp);
        } catch (Exception $fallthrough) {
            return $this->createErrorResponse(new HttpException(self::STATUS_EXCEPTION, "", $fallthrough));
        }
    }

    /**
     * @Route("/api/v1/auth/ip", methods={"GET"})
     */
    public function ip(): Response
    {
        try {
            return $this->json([
                Params::IP => $_SERVER['REMOTE_ADDR']
            ]);
        } catch (Exception $fallthrough) {
            return $this->createErrorResponse(new HttpException(self::STATUS_EXCEPTION, "", $fallthrough));
        }
    }
}
