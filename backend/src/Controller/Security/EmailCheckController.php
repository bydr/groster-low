<?php

namespace App\Controller\Security;

use App\ApiError;
use App\Controller\Security\BaseSecurityController;
use App\Handler\Front\AuthHandler;
use App\Helpers\Params;
use App\Interfaces\Front\AuthInterface;
use App\Repository\AuthUserRepository;
use App\Service\Cache;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Проверка Email
 */
class EmailCheckController extends BaseSecurityController
{
    /** @var AuthInterface */
    private $handler;

    public function __construct(AuthUserRepository $repository, AuthHandler $userHandler)
    {
        parent::__construct($repository);
        $this->handler = $userHandler;
    }

    /**
     * @Route("/api/v1/auth/email_check", name="email_check", methods={"GET"})
     */
    public function endpoint(Request $request): Response
    {
        try {
            $email = $request->get('email') ?? '';

            if(empty($email)) {
                return $this->createBadRequestResponse(ApiError::ERROR_INVALID_DATA);
            }

            $error = null;
            $issetEmail = $this->handler->emailCheck($email, $error);

            if(!is_null($error)) {
                return $this->createBadRequestResponse($error);
            }

            return $this->json([
                'is_free' => !$issetEmail,
            ]);
        } catch (Exception $fallthrough) {
            return $this->createErrorResponse(new HttpException( self::STATUS_EXCEPTION, "", $fallthrough));
        }
    }
}
