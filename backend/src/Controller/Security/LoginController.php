<?php

namespace App\Controller\Security;

use App\ApiError;
use App\Controller\ApiController;
use App\Handler\Front\AuthHandler;
use App\Helpers\Params;
use App\Interfaces\Front\AuthInterface;
use App\Repository\AuthUserRepository;
use App\Service\Cache;
use App\Service\Server\Server;
use Exception;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;

class LoginController extends BaseSecurityController
{
    /** @var AuthInterface */
    private $handler;

    public function __construct(AuthUserRepository $repository, AuthHandler $userHandler)
    {
        parent::__construct($repository);
        $this->handler = $userHandler;
    }
    /**
     * @Route("/api/v1/auth/login_email", name="login-email", methods={"POST"})
     */
    public function loginEmail(Request $request, UserPasswordHasherInterface $hasher, Server $server): JsonResponse
    {
        try {
            if ($this->checkAuth($request)) {
                return $this->createBadRequestResponse(ApiError::ERROR_REMOVE_AUTH_HEADER);
            }

            $json = $this->getJsonRequest($request);
            if ($json == null) {
                return $this->createBadRequestResponse(ApiError::ERROR_INVALID_JSON);
            }

            $errorCode = null;

            $user = $this->handler->loginEmail($json, $hasher, $errorCode);

            if(!is_null($errorCode)) {
                return $this->createBadRequestResponse($errorCode);
            }

            $server->updateUserProduct($user);

            return $this->authResponse($user);
        } catch (Exception $fallthrough) {
            return $this->createErrorResponse(new HttpException( self::STATUS_EXCEPTION, "", $fallthrough));
        }
    }

    /**
     * @Route("/api/v1/auth/login_phone", name="login-phone", methods={"POST"})
     */
    public function loginPhone(Request $request, Server $server): JsonResponse
    {
        try {
            if ($this->checkAuth($request)) {
                return $this->createBadRequestResponse(ApiError::ERROR_REMOVE_AUTH_HEADER);
            }

            $json = $this->getJsonRequest($request);
            if ($json == null) {
                return $this->createBadRequestResponse(ApiError::ERROR_INVALID_JSON);
            }

            $errorCode = null;

            $user = $this->handler->loginPhone($json, $errorCode);

            if(!is_null($errorCode)) {
                return $this->createBadRequestResponse($errorCode);
            }

            $server->updateUserProduct($user);
            return $this->authResponse($user);
        } catch (Exception $fallthrough) {
            return $this->createErrorResponse(new HttpException( self::STATUS_EXCEPTION, "", $fallthrough));
        }
    }

    /**
     * Отправка кода проверки.
     * Отправляется на телефон всегда.
     * Отправляется на почту только, если это новый пользователь
     * @Route("/api/v1/auth/send_code", name="send_code", methods={"POST"})
     */
    public function sendCode(Request $request): Response
    {
        try {
            $json = $this->getJsonRequest($request);
            if (is_null($json)) {
                return $this->createBadRequestResponse();
            }
            if(empty($json->getString(Params::CONTACT))) {
                return $this->createBadRequestResponse(ApiError::ERROR_INVALID_JSON);
            }

            $isNewUser = false;
            if(!$this->handler->sendCode($json,  $isNewUser)) {
                return $this->createBadRequestResponse();
            }

            return $this->json([
                Params::IS_FREE => $isNewUser,
                Params::LIFETIME => Cache::CODE_LIFETIME,
            ]);
        } catch (Exception $fallthrough) {
            return $this->createErrorResponse(new HttpException( self::STATUS_EXCEPTION, "", $fallthrough));
        }
    }
}
