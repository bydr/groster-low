<?php

namespace App\Controller\Security;

use App\ApiError;
use App\Handler\Front\AuthHandler;
use App\Interfaces\Front\AuthInterface;
use App\Repository\AuthUserRepository;
use Exception;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;

class RegisterController extends BaseSecurityController
{
    /** @var AuthInterface */
    private $handler;

    public function __construct(AuthUserRepository $repository, AuthHandler $userHandler)
    {
        parent::__construct($repository);
        $this->handler = $userHandler;
    }

    /**
     * @Route("/api/v1/auth/register_email", name="register-email"), methods={"POST"})
     */
    public function registerEmail(Request $request, UserPasswordHasherInterface $hasher): JsonResponse
    {
        try {
            $errorCode = null;
            $json = $this->getJsonRequest($request);
            if ($json == null) {
                return $this->createBadRequestResponse(ApiError::ERROR_INVALID_JSON);
            }

            $user = $this->handler->emailRegistration($json, $hasher, $errorCode);

            if(!is_null($errorCode)) {
                return $this->createBadRequestResponse($errorCode);
            }

            return $this->authResponse($user);
        } catch (Exception $fallthrough) {
            return $this->createErrorResponse(new HttpException(self::STATUS_EXCEPTION, "", $fallthrough));
        }
    }

    /**
     * @Route("/api/v1/auth/register_phone", name="register-phone"), methods={"POST"})
     */
    public function registerPhone(Request $request, UserPasswordHasherInterface $hasher): JsonResponse
    {
        try {
            $errorCode = null;
            $json = $this->getJsonRequest($request);
            if ($json == null) {
                return $this->createBadRequestResponse(ApiError::ERROR_INVALID_JSON);
            }

            $user = $this->handler->phoneRegistration($json, $hasher, $errorCode);

            if(!is_null($errorCode)) {
                return $this->createBadRequestResponse($errorCode);
            }

            return $this->authResponse($user);
        } catch (Exception $fallthrough) {
            return $this->createErrorResponse(new HttpException(self::STATUS_EXCEPTION, "", $fallthrough));
        }
    }
}
