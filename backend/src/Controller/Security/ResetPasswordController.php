<?php

namespace App\Controller\Security;

use App\ApiError;
use App\Handler\Front\AuthHandler;
use App\Interfaces\Front\AuthInterface;
use App\Repository\AuthUserRepository;
use App\Service\Cache;
use Exception;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;

class ResetPasswordController extends BaseSecurityController
{
    /** @var AuthInterface */
    private $handler;

    public function __construct(AuthUserRepository $repository, AuthHandler $userHandler)
    {
        parent::__construct($repository);
        $this->handler = $userHandler;
    }
    /**
     * @Route("/api/v1/auth/reset_password", methods={"POST"})
     */
    public function resetPassword(Request $request): JsonResponse
    {
        try {
            if ($this->checkAuth($request)) {
                return $this->createBadRequestResponse(ApiError::ERROR_REMOVE_AUTH_HEADER);
            }

            $json = $this->getJsonRequest($request);
            if ($json == null) {
                return $this->createBadRequestResponse(ApiError::ERROR_INVALID_JSON);
            }

            $errorCode = null;
            if(!$this->handler->resetPassword($json, $errorCode)){
                return $this->createBadRequestResponse(ApiError::ERROR_UNHANDLED);
            }

            if(!is_null($errorCode)) {
                return $this->createBadRequestResponse($errorCode);
            }

            return $this->json([
                'lifetime' => Cache::CODE_LIFETIME,
            ]);
        } catch (Exception $fallthrough) {
            return $this->createErrorResponse(new HttpException( self::STATUS_EXCEPTION, "", $fallthrough));
        }
    }

    /**
     * @Route("/api/v1/auth/update_password", methods={"POST"})
     */
    public function updatePassword(Request $request, UserPasswordHasherInterface $hasher): Response
    {
        try {
            $json = $this->getJsonRequest($request);
            if ($json == null) {
                return $this->createBadRequestResponse();
            }

            $errorCode = null;
            $user = $this->handler->updatePassword($json, $hasher, $errorCode);

            if(!is_null($errorCode)) {
                return $this->createBadRequestResponse($errorCode);
            }

            if(!$user) {
                return $this->createBadRequestResponse(ApiError::ERROR_INVALID_CODE);
            }

            return $this->authResponse($user);
        } catch (Exception $fallthrough) {
            return $this->createErrorResponse(new HttpException( self::STATUS_EXCEPTION, "", $fallthrough));
        }
    }
}
