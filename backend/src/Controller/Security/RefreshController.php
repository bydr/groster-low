<?php

namespace App\Controller\Security;

use App\ApiError;
use App\Handler\Front\AuthHandler;
use App\Helpers\Params;
use App\Interfaces\Front\AuthInterface;
use App\Repository\AuthUserRepository;
use Exception;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Exception\AuthenticationException;

/**
 * Обновление токена доступа
 */
class RefreshController extends BaseSecurityController
{
    /**
     * @Route("/api/v1/auth/refresh", name="refresh"), methods={"POST"})
     */
    public function endpoint(Request $request): JsonResponse
    {
        try {
            $refresh_token = $this->getJsonRequest($request)->getString(Params::REFRESH_TOKEN);
            $credentials = str_replace('Bearer ', '', $refresh_token);
            if(empty($refresh_token)) {
                return $this->createBadRequestResponse(ApiError::ERROR_INVALID_DATA);
            }
            $user = $this->getUserByRefreshToken($credentials);

            if(is_null($user)) {
                return $this->createBadRequestResponse(ApiError::ERROR_INVALID_JSON, self::STATUS_UNAUTHORIZATION);
            }

            return $this->authResponse($user);
        } catch (Exception $fallthrough) {
            return $this->createErrorResponse(new HttpException( self::STATUS_EXCEPTION, "", $fallthrough));
        }
    }
}
