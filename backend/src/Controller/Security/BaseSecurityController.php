<?php

namespace App\Controller\Security;

use App\ApiError;
use App\Controller\ApiController;
use App\Helpers\OrderHelper;
use App\Helpers\Params;
use App\Repository\AuthUserRepository;
use DateTime;
use Exception;
use Firebase\JWT\JWT;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\User\UserInterface;

abstract class BaseSecurityController extends ApiController
{
    protected $repository;

    public function __construct(AuthUserRepository $repository)
    {
        parent::__construct();
        $this->repository = $repository;
    }

    public function logout(Request $request) {
        $credentials = $request->headers->get('Authorization');
        $credentials = str_replace('Bearer ', '', $credentials);
        $user = $this->getUserByAccessToken($credentials);
        $this->repository->setLogin($user, false);
    }

    public function checkAuth(Request $request): bool
    {
        return $request->headers->has('Authorization');
    }

    /**
     * Converts an exception to a serializable array.
     *
     * @param \Exception|null $exception
     *
     * @return array
     */
    private function exceptionToArray(Exception $exception = null): ?array
    {
        if (null === $exception) {
            return null;
        }

        return [
            'message' => $exception->getMessage(),
            'type' => get_class($exception),
        ];
    }

    public function createAccessToken(UserInterface $user): string
    {
        $access_exp = $this->getParameter('jwt_access_exp');
        $payload_access = [
            "user" => $user->getUserIdentifier(),
            "type" => "access",
            "exp" => (new DateTime())->modify("+" . $access_exp . " seconds")->getTimestamp(),
        ];
        return JWT::encode($payload_access, $this->getParameter('jwt_secret'));
    }

    public function createRefreshToken(UserInterface $user): string
    {
        $refresh_exp = $this->getParameter('jwt_refresh_exp');
        $payload_refresh = [
            "user" => $user->getUserIdentifier(),
            "type" => "refresh",
            "exp" => (new DateTime())->modify("+" . $refresh_exp . " seconds")->getTimestamp(),
        ];
        return JWT::encode($payload_refresh, $this->getParameter('jwt_secret'));
    }

    public function getUserByRefreshToken($credentials): ?UserInterface
    {
        try {
            $jwt = (array)JWT::decode(
                $credentials,
                $this->getParameter('jwt_secret'),
                ['HS256']
            );
            if ("refresh" != $jwt['type']) {
                throw new AuthenticationException("type is not refresh");
            }
        }catch (Exception $e) {
            ApiController::getLogger()->error(sprintf("Failed to refresh token: %s", $e->getMessage()));
            return null;
        }

        return $this->repository
            ->findOneBy([
                'uid' => $jwt['user'],
            ]);
    }

    public function getUserByAccessToken($credentials): UserInterface
    {
        $jwt = (array)JWT::decode(
            $credentials,
            $this->getParameter('jwt_secret'),
            ['HS256']
        );
        if ($jwt['type'] != "access") {
            throw new AuthenticationException("type is not access");
        }
        return $this->repository
            ->findOneBy([
                'uid' => $jwt['user'],
            ]);
    }

    public function authResponse(UserInterface $user)
    {
        $jwt_access = $this->createAccessToken($user);
        $jwt_refresh = $this->createRefreshToken($user);

        $cartUid = null;
        if($cart = OrderHelper::getCartByUser($this->getDoctrine(), $user)) {
            $cartUid = $cart->getUid();
        }
        $response = $this->json([
            Params::MESSAGE => 'success',
            Params::ACCESS_TOKEN => sprintf('Bearer %s', $jwt_access),
            Params::REFRESH_TOKEN => $jwt_refresh,
            Params::INFO => [
                Params::CART => $cartUid,
                Params::FIO => $user->getFio(),
                Params::IS_ADMIN => $user->isAdmin(),
            ]
        ], 200);

        $this->repository->setLogin($user);

        return $response;
    }

}