<?php

namespace App\Controller\Server;

use App\ApiError;
use App\Controller\ApiController;
use App\Handler\Admin\PromocodeHandler;
use App\Helpers\Params;
use App\Helpers\SerializerHelper;
use App\Interfaces\Server\ServerInterface;
use App\RequestJson;
use Exception;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Routing\Annotation\Route;

class ServerController extends ApiController
{
    /** @var ServerInterface */
    private $promoHandler;

    public function __construct(PromocodeHandler $promoHandler)
    {
        parent::__construct();
        $this->promoHandler = $promoHandler;

    }

    /**
     * Создание промокода из 1C
     * @param Request $request
     * @return JsonResponse
     * @Route("/api/v1/server/promocode/add", methods={"POST"})
     */
    public function serverCreate(Request $request): JsonResponse
    {
        try {
            $json = $this->getJsonRequest($request);

            if($request->get(Params::API_KEY) != $this->getParameter('api_key_1c')) {
                return $this->createUnauthorizationResponse();
            }

            $error = $this->promoHandler->serverCreate($json);

            if(!is_null($error)) {
                return $this->createBadRequestResponse($error);
            }

            return $this->createSuccessResponse('');
        } catch (Exception $fallthrough) {
            return $this->createErrorResponse(new HttpException( self::STATUS_EXCEPTION, "", $fallthrough));
        }
    }

    /**
     * Получение промокода
     * @param Request $request
     * @param string $code
     * @return JsonResponse
     * @Route("/api/v1/server/promocode/{code}", methods={"GET"})
     */
    public function serverGet(Request $request, string $code): JsonResponse
    {
        try {
            $json = new RequestJson($request->query->all());

            if($json->getString(Params::API_KEY) != $this->getParameter('api_key_1c')) {
                return $this->createUnauthorizationResponse();
            }

            $promo = $this->promoHandler->serverGetOne($code);

            if(!$promo) {
                return $this->createBadRequestResponse(ApiError::ERROR_NOT_FOUND);
            }

            $result = SerializerHelper::serialize($promo, [SerializerHelper::GROUP_PROMO_1C]);
            return $this->createSuccessResponse($result);
        } catch (Exception $fallthrough) {
            return $this->createErrorResponse(new HttpException( self::STATUS_EXCEPTION, "", $fallthrough));
        }
    }

    /**
     * Создание промокода из 1C
     * @param Request $request
     * @param string $uid
     * @return JsonResponse
     * @Route("/api/v1/server/promocode/{code}", methods={"PUT"})
     */
    public function serverUpdate(Request $request, string $code): JsonResponse
    {
        try {
            $json = $this->getJsonRequest($request);

            if($request->get(Params::API_KEY) != $this->getParameter('api_key_1c')) {
                return $this->createUnauthorizationResponse();
            }

            $error = $this->promoHandler->serverUpdateOne($code, $json);

            if(!is_null($error)) {
                return $this->createBadRequestResponse($error);
            }

            return $this->createSuccessResponse('');
        } catch (Exception $fallthrough) {
            return $this->createErrorResponse(new HttpException( self::STATUS_EXCEPTION, "", $fallthrough));
        }
    }
}