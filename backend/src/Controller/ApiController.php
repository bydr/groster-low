<?php
/**
 * Абстрактный класс для всех контроллеров
 *
 * При возвращении успешного ответа используются 2 метода:
 * 1) $this->json - если данные возвращаются в виде массива
 * 2) $this->createSuccessResponse - если данные уже сериализованы
 */

namespace App\Controller;

use App\ApiError;
use App\Entity\Security\AuthUser;
use App\RequestJson;
use App\Service\ValidatorInterface;
use Exception;
use App\Logger;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Security\Core\User\UserInterface;

abstract class ApiController extends AbstractController
{
    const STATUS_OK = 200;
    const STATUS_SUCCESS_SAVE = 201;
    const STATUS_EMPTY = 204;
    const STATUS_BAD_REQUEST = 400;
    const STATUS_UNAUTHORIZATION = 401;
    const STATUS_ACCESS_DENIED = 403;
    const STATUS_NOT_FOUND = 404;
    const STATUS_EXCEPTION = 418;

    protected $validator;

    /** @var Logger */
    private static $logger;

    /** @var AuthUser */
    private static $user;

    public function __construct()
    {
        self::setLogger();
    }

    public function setValidator(ValidatorInterface $validator)
    {
        $this->validator = $validator;
    }

    /**
     * @param string $code
     * @param int $statusCode
     * @param string $msg
     * @return JsonResponse
     */
    public function createBadRequestResponse(string $code = ApiError::ERROR_BAD_REQUEST, int $statusCode = 400, string $msg = ''): JsonResponse
    {
        if(in_array($code, [ApiError::ERROR_NOT_FOUND, ApiError::ERROR_NOT_FOUND_ORDER])) {
            $statusCode = self::STATUS_NOT_FOUND;
        }

        if(empty($msg)) {
            $msg = ApiError::getErrorMessage($code);
        }
        return $this->json([
            "code" => $code,
            "message" => $msg,
        ], $statusCode);
    }

    public function createUnauthorizationResponse(): JsonResponse
    {
        return $this->createBadRequestResponse(ApiError::ERROR_UNAUTHORIZATION, self::STATUS_UNAUTHORIZATION);
    }

    public function createErrorResponse(HttpException $exception): JsonResponse
    {
        $statusCode = $exception->getStatusCode();

        $error = $exception->getPrevious();
        self::$logger->errorWithExc($error);
        return $this->json([
            "code" => ApiError::ERROR_UNHANDLED,
            "message" => ApiError::getErrorMessage(ApiError::ERROR_UNHANDLED),
        ], $statusCode);
    }


    public function createSuccessResponse($jsonResult, $responseCode = 200)
    {
        return new JsonResponse(
            $jsonResult,
            $responseCode,
            [],
            true
        );
    }

    public function createEmptyResponse()
    {
        return new JsonResponse (
            "",
            self::STATUS_EMPTY
        );
    }

    public static function getLogger()
    {
        if(is_null(self::$logger)) {
            self::setLogger();
        }
        return self::$logger;
    }

    private static function setLogger()
    {
        self::$logger = new Logger('api');
    }

    /**
     * Converts an exception to a serializable array.
     *
     * @param \Exception|null $exception
     *
     * @return array
     */
    private function exceptionToArray(\Exception $exception = null)
    {
        if (null === $exception) {
            return null;
        }

        return [
            'message'  => $exception->getMessage(),
            'type'     => get_class($exception),
        ];
    }

    protected function validate($data, $asserts = null)
    {
        $errors = $this->validator->validate($data, $asserts);

        if (count($errors) > 0) {
            $errorsString = (string)$errors;
            return $this->createBadRequestResponse($errorsString);
        }

        return null;
    }

    /**
     * Декодировка тела запроса в массив
     * @param Request $request
     * @return RequestJson|null
     */
    public function getJsonRequest(Request $request): ?RequestJson
    {
        if ($request->headers->get('Content-Type') == 'application/json') {
            $content = json_decode($request->getContent(), true);
            if(!is_array($content)) {
                return null;
            }
            return new RequestJson($content);
        }

        return null;
    }

    /**
     * @return AuthUser|null
     */
    public static function getCurrentUser(): ?AuthUser
    {
        return self::$user;
    }

    /**
     * @param UserInterface|string $user
     */
    public static function setCurrentUser(?AuthUser $user): void
    {
        if(!($user instanceof UserInterface)) {
            $user = null;
        }

        self::$user = $user;
    }
}