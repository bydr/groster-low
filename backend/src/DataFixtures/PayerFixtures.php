<?php

namespace App\DataFixtures;

use App\Entity\Payer;
use App\Entity\Security\AuthUser;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Persistence\ObjectManager;

/**
 * Плательщик
 */
class PayerFixtures extends Fixture implements OrderedFixtureInterface, FixtureGroupInterface
{
    public static function getGroups(): array
    {
        return [FakeData::FIXTURE_GROUP_POST];
    }
    /**
     * @return int
     */
    public function getOrder(): int
    {
        return 4;
    }

    public function load(ObjectManager $manager)
    {
        $customer = $manager->getRepository(AuthUser::class)->findOneBy(['email' => SecurityFixtures::CART_EMAIL]);
        $payer = new Payer();
        $payer->setCustomer($customer);
        $payer->setName("ИП Иванов");
        $payer->setOgrnip("1234567896");

        $manager->persist($payer);
        $manager->flush();
    }
}