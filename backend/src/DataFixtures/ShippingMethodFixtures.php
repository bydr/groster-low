<?php

namespace App\DataFixtures;

use App\Entity\ShippingLimit;
use App\Entity\ShippingMethod;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Persistence\ObjectManager;

/**
 * Способы доставки
 */
class ShippingMethodFixtures extends Fixture implements OrderedFixtureInterface, FixtureGroupInterface
{
    public static function getGroups(): array
    {
        return [FakeData::FIXTURE_GROUP_POST];
    }
    /**
     * @return int
     */
    public function getOrder(): int
    {
        return 8;
    }

    public function load(ObjectManager $manager)
    {
        $data = [
            [
                "name" => 'Срочная доставка в пределах города',
                "weight" => 1,
                "cost" => 'от 1500',
                "useAddress" => true,
                "isFast" => true,
                "alias" => ShippingMethod::METHOD_EXPRESS,
                'limits' => [
                    [
                        'type' => ShippingLimit::TYPE_REGION,
                        'minCost' => 0,
                        'shippingCost' => 150000,
                        'region' => 'г Волгоград',
                    ]
                ],
                'isActive' => true,
            ],
            [
                "name" => 'Самовывоз',
                "weight" => 2,
                "cost" => '0',
                "useAddress" => false,
                "isFast" => false,
                "alias" => ShippingMethod::METHOD_PICKUP,
                'limits' => [
                    [
                        'type' => ShippingLimit::TYPE_REGION,
                        'minCost' => 0,
                        'shippingCost' => 0,
                        'region' => 'г Волгоград',
                    ]
                ],
                'isActive' => true,
            ],
            [
                "name" => 'Курьером',
                "weight" => '3',
                "cost" => 'от 400',
                "useAddress" => true,
                "isFast" => false,
                "alias" => ShippingMethod::METHOD_COURIER,
                'limits' => [
                    [
                        'type' => ShippingLimit::TYPE_REGION,
                        'minCost' => 0,
                        'shippingCost' => 40000,
                        'region' => 'г Волгоград',
                    ],
                    [
                        'type' => ShippingLimit::TYPE_COST,
                        'minCost' => 100000,
                        'shippingCost' => 0,
                        'region' => 'г Волгоград',
                    ],
                ],
                'isActive' => true,
            ],
            [
                "name" => 'Транспортной компанией',
                "weight" => 4,
                "cost" => 0,
                "useAddress" => true,
                "isFast" => false,
                "alias" => ShippingMethod::METHOD_COMPANY,
                'limits' => [
                    [
                        'type' => ShippingLimit::TYPE_REGION,
                        'minCost' => 0,
                        'shippingCost' => 100000,
                        'region' => 'Волгоградская обл',
                    ],
                ],
                'isActive' => true,
            ],
            [
                "name" => 'Отключенный метод',
                "weight" => 5,
                "cost" => 0,
                "useAddress" => true,
                "isFast" => false,
                "alias" => 'deactivated',
                'limits' => [
                ],
                'isActive' => false,
            ],
        ];

        foreach ($data as $item) {
            $method = new ShippingMethod();
            $method->setName($item['name']);
            $method->setWeight($item['weight']);
            $method->setCost($item['cost']);
            $method->setIsFast($item['isFast']);
            $method->setUseAddress($item['useAddress']);
            $method->setAlias($item['alias']);
            $method->setIsActive($item['isActive']);

            $manager->persist($method);

            if(!empty($item['limits'])) {
                foreach ($item['limits'] as $value) {
                    $limit = new ShippingLimit();
                    $limit->setMethod($method);
                    $limit->setType($value['type']);
                    $limit->setOrderMinCost($value['minCost']);
                    $limit->setShippingCost($value['shippingCost']);
                    $limit->setRegion($value['region']);

                    $manager->persist($limit);
                    $method->addLimit($limit);
                }
            }
        }

        $manager->flush();
    }
}