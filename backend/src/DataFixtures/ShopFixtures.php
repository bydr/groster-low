<?php

namespace App\DataFixtures;

use App\Entity\Store;
use App\Helpers\ValidateHelper;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Persistence\ObjectManager;

/**
 * class ShopFixtures
 */
class ShopFixtures extends Fixture implements OrderedFixtureInterface, FixtureGroupInterface
{
    public static function getGroups(): array
    {
        return [FakeData::FIXTURE_GROUP_POST];
    }

    /**
     * @return int
     */
    public function getOrder(): int
    {
        return 9;
    }

    public function load(ObjectManager $manager)
    {
        $shop = $manager->getRepository(Store::class)->findOneByUuid('c6559408-1695-11ea-2093-ac1f6b855a52');
        $shop->setAddress('400048, Волгоградская обл, Волгоград г, Авиаторов ш, дом 18');
        $shop->setDescription('новый уютный склад');
        $shop->setEmail('shop1@groster.ru');
        $shop->setPhone(ValidateHelper::getValidPhone('+79888888888'));
        $shop->setSchedule('Пн-Вс 09:00-18:00');
        $shop->setLatitude('48.758125');
        $shop->setLongitude('44.440313');

        $shop = $manager->getRepository(Store::class)->findOneByUuid('5f268ab5-c232-11e7-82fe-448a5b2839e6');
        $shop->setAddress('400127, Волгоградская обл, Волгоград г, Кольцевая ул, дом № 64');
        $shop->setDescription('новый уютный склад');
        $shop->setEmail('shop1@groster.ru');
        $shop->setPhone(ValidateHelper::getValidPhone('+79888888888'));
        $shop->setSchedule('Пн-Вс 09:00-18:00');
        $shop->setLatitude('48.78801291935449');
        $shop->setLongitude('44.51560959129901');

        $manager->flush();
    }
}