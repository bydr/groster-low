<?php

namespace App\DataFixtures;

use App\Entity\Product;
use App\Entity\Security\AuthUser;
use App\Entity\WaitingProduct;
use App\Helpers\TextHelper;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Persistence\ObjectManager;

/**
 * class WaitingFixtures
 */
class WaitingFixtures extends Fixture implements OrderedFixtureInterface, FixtureGroupInterface
{
    public static function getGroups(): array
    {
        return [FakeData::FIXTURE_GROUP_PRE];
    }

    /**
     * @return int
     */
    public function getOrder(): int
    {
        return 11;
    }

    public function load(ObjectManager $manager)
    {
        /** @var AuthUser $user */
        $user = $manager->getRepository(AuthUser::class)->findOneByEmail(SecurityFixtures::CART_EMAIL);

        $product = $this->createEmptyProduct($manager);
        $waiting = new WaitingProduct();
        $waiting->setProduct($product);
        $waiting->setContact(SecurityFixtures::CART_EMAIL);
        $waiting->setCustomer($user);
        $manager->persist($waiting);
        $manager->flush();
    }

    /**
     * @param ObjectManager $manager
     * @return Product
     */
    private function createEmptyProduct(ObjectManager $manager): Product
    {
        $name = "Контейнер 1000 мл прямоугол прозрач Д191*129 (306 шт)+крышка АЛЬЯНС";
        $product = new Product();
        $product->setName($name);
        $product->setAlias(TextHelper::translitRu2En($name));
        $product->setUuid('25c32eac-bdec-11e7-9b53-38607704c280');
        $product->setPrice(125);
        $product->setCode("code");
        $product->setUnit("unit");
        $product->setIsSellUnit(true);

        $manager->persist($product);

        return $product;
    }
}