<?php

namespace App\DataFixtures;

use App\Entity\Banner;
use App\Helpers\ImageHelper;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Psr\Container\ContainerInterface;

/**
 * class BannerFixtures
 */
class BannerFixtures extends Fixture implements OrderedFixtureInterface, FixtureGroupInterface
{
    public static function getGroups(): array
    {
        return [FakeData::FIXTURE_GROUP_POST];
    }

    /**
     * @return int
     */
    public function getOrder(): int
    {
        return 12;
    }

    public function load(ObjectManager $manager)
    {

        $banner = new Banner();
        $banner->setDesktop("static.groster.trd/images/banners/1.jpg");
        $banner->setTablet("static.groster.trd/images/banners/2.jpg");
        $banner->setMobile("static.groster.trd/images/banners/3.jpg");
        $banner->setType(Banner::TYPE_SLIDER);
        $banner->setUrl("http://test.com");
        $banner->setWeight(1);

        $manager->persist($banner);
        $manager->flush();
    }
}