<?php

namespace App\DataFixtures;

use App\Entity\Product;
use App\Entity\Security\AuthUser;
use App\Entity\WaitingProduct;
use App\Helpers\TextHelper;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Persistence\ObjectManager;

/**
 * class WaitingFixtures
 */
class RecommendationFixtures extends Fixture implements OrderedFixtureInterface, FixtureGroupInterface
{
    public static function getGroups(): array
    {
        return [FakeData::FIXTURE_GROUP_POST];
    }

    /**
     * @return int
     */
    public function getOrder(): int
    {
        return 12;
    }

    public function load(ObjectManager $manager)
    {
        /** @var AuthUser $user */
        $user = $manager->getRepository(AuthUser::class)->findOneByEmail(SecurityFixtures::CART_EMAIL);

        $product = $manager->getRepository(Product::class)->findOneByUuid('25c32eac-bdec-11e7-9b53-38607704c280');
        $user->addRecommendation($product);
        $manager->flush();
    }
}