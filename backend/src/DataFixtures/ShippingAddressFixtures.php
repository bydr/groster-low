<?php

namespace App\DataFixtures;

use App\Entity\Security\AuthUser;
use App\Entity\ShippingAddress;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Persistence\ObjectManager;

/**
 * Адрес доставки
 */
class ShippingAddressFixtures extends Fixture implements OrderedFixtureInterface, FixtureGroupInterface
{
    public static function getGroups(): array
    {
        return [FakeData::FIXTURE_GROUP_POST];
    }

    /**
     * @return int
     */
    public function getOrder(): int
    {
        return 7;
    }

    public function load(ObjectManager $manager)
    {
        $customer = $manager->getRepository(AuthUser::class)->findOneBy(['email' => 'cart@test.com']);
        $address = new ShippingAddress();
        $address->setName("Волгоград");
        $address->setAddress('г Волгоград, ул. Набережная, д.17');
        $address->setCustomer($customer);

        $manager->persist($address);
        $manager->flush();
    }
}