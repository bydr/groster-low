<?php

namespace App\DataFixtures;

use App\Entity\Product;
use App\Entity\Security\AuthUser;
use App\Entity\WaitingProduct;
use App\Helpers\ValidateHelper;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

/**
 * class SecurityFixtures
 */
class SecurityFixtures extends Fixture implements OrderedFixtureInterface, FixtureGroupInterface
{
    const FAKE_QTY = 10;
    const ADMIN_EMAIL = "admin@groster.ru";
    const ADMIN_PASSWORD = 'Lj3$k#@S9e7s';
    const CART_EMAIL = "cart@test.com";

    private $hasher;

    public function __construct(UserPasswordHasherInterface $hasher)
    {
        $this->hasher = $hasher;
    }

    public static function getGroups(): array
    {
        return [FakeData::FIXTURE_GROUP_POST];
    }

    /**
     * @return int
     */
    public function getOrder(): int
    {
        return 1;
    }

    public function load(ObjectManager $manager)
    {
        $this->addUsers($manager);
        $cartUser = $this->addCartUser($manager);
        $this->waitingProduct($manager, $cartUser);

    }

    private function addUsers(ObjectManager $manager)
    {
        for ($i = 1; $i <= self::FAKE_QTY; $i++) {
            $user = new AuthUser();
            $hash = $this->hasher->hashPassword($user, "password" . $i);
            $user->setPassword($hash);
            $user->setIsLogin(false);
            $user->setEmail("qwe" . $i . "@test.com");
            $user->setPhone(ValidateHelper::getValidPhone("7(988) 999-99-9" . ($i - 1)));
            $manager->persist($user);
            $manager->flush();
        }

        $user = new AuthUser();
        $hash = $this->hasher->hashPassword($user, self::ADMIN_PASSWORD);
        $user->setPassword($hash);
        $user->setIsLogin(false);
        $user->setEmail(self::ADMIN_EMAIL);
        $user->setPhone(ValidateHelper::getValidPhone("+7(988) 999-99-00"));
        $user->setRoles([AuthUser::ROLE_ADMIN]);
        $manager->persist($user);
        $manager->flush();
    }

    /**
     * Добавление клиента
     * @param ObjectManager $manager
     * @return AuthUser
     */
    private function addCartUser(ObjectManager $manager): AuthUser
    {
        $user = new AuthUser();
        $hash = $this->hasher->hashPassword($user, "password1");
        $user->setPassword($hash);
        $user->setIsLogin(false);
        $user->setEmail(self::CART_EMAIL);
        $user->setPhone(ValidateHelper::getValidPhone("+7(988) 999-99-99"));
        $manager->persist($user);
        $manager->flush();

        return $user;
    }

    /**
     * Добавление пользователю товаров в ожидании
     * @param ObjectManager $manager
     * @param AuthUser $cartUser
     * @return void
     */
    private function waitingProduct(ObjectManager $manager, AuthUser $cartUser)
    {
        $product = $manager->getRepository(Product::class)->find(1);

        $wp = new WaitingProduct();
        $wp->setCustomer($cartUser);
        $wp->setProduct($product);
        $wp->setContact($cartUser->getEmail());
        $manager->persist($wp);
        $manager->flush();
    }
}
