<?php

namespace App\DataFixtures;

use App\Entity\Category;
use App\Entity\Discount;
use DateTime;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\HttpKernel\KernelInterface;

/**
 * Фикстуры для промокода
 */
class PromoFixtures extends Fixture implements OrderedFixtureInterface, FixtureGroupInterface
{
    /** @var string */
    private $env;

    public function __construct(KernelInterface $kernel)
    {
        $this->env = $kernel->getEnvironment();
    }

    public static function getGroups(): array
    {
        return [FakeData::FIXTURE_GROUP_POST];
    }

    /**
     * @return int
     */
    public function getOrder(): int
    {
        return 5;
    }

    public function load(ObjectManager $manager)
    {
        if($this->env != 'test') {
            return;
        }

        $category = $manager->getRepository(Category::class)->find(132);

        $this->createRateDiscount($manager, $category);
        $this->createMoneyDiscount($manager, $category);
        $this->createNotReusableDiscount($manager, $category);
        $this->createExpiredDiscount($manager, $category);

        $manager->flush();
    }

    /**
     * @param ObjectManager $manager
     * @param $category
     * @return void
     */
    private function createRateDiscount(ObjectManager $manager, $category)
    {
        $promo = new Discount();
        $promo->addCategory($category);
        $promo->setReusable(true);
        $promo->setCode("test promo rate");
        $promo->setStart(new DateTime());
        $promo->setFinish((new DateTime())->modify("+1 hour"));
        $promo->setName("test promo");
        $promo->setType(Discount::TYPE_RATE);
        $promo->setValue(10);
        $manager->persist($promo);
    }

    /**
     * @param ObjectManager $manager
     * @param $category
     * @return void
     */
    private function createMoneyDiscount(ObjectManager $manager, $category)
    {
        $promo = new Discount();
        $promo->addCategory($category);
        $promo->setReusable(true);
        $promo->setCode("test promo money");
        $promo->setStart(new DateTime());
        $promo->setFinish((new DateTime())->modify("+1 hour"));
        $promo->setName("test promo");
        $promo->setType(Discount::TYPE_MONEY);
        $promo->setValue(10 * 100);
        $manager->persist($promo);
    }

    /**
     * @param ObjectManager $manager
     * @param $category
     * @return void
     */
    private function createNotReusableDiscount(ObjectManager $manager, $category)
    {
        $promo = new Discount();
        $promo->addCategory($category);
        $promo->setReusable(false);
        $promo->setCode("not reusable");
        $promo->setStart(new DateTime());
        $promo->setFinish((new DateTime())->modify("+1 hour"));
        $promo->setName("not reusable");

        $promo->setType(Discount::TYPE_RATE);
        $promo->setValue(10);
        $manager->persist($promo);
    }

    /**
     * @param ObjectManager $manager
     * @param $category
     * @return void
     */
    private function createExpiredDiscount(ObjectManager $manager, $category)
    {
        $promo = new Discount();
        $promo->addCategory($category);
        $promo->setReusable(true);
        $promo->setCode("expired");
        $promo->setStart((new DateTime())->modify("-2 hours"));
        $promo->setFinish((new DateTime())->modify("-1 hour"));
        $promo->setName("expired");

        $promo->setType(Discount::TYPE_RATE);
        $promo->setValue(10);
        $manager->persist($promo);
    }
}