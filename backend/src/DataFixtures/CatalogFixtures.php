<?php

namespace App\DataFixtures;

use App\Entity\Category;
use App\Entity\CategoryImage;
use App\Entity\Param;
use App\Entity\Product;
use App\Entity\ProductImage;
use App\Entity\Tag;
use App\Entity\Value;
use App\Helpers\Params;
use App\Helpers\TextHelper;
use App\Repository\ProductRepository;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;
use Faker\Generator;
use Symfony\Component\HttpKernel\KernelInterface;

/**
 * Class CatalogFixtures
 */
class CatalogFixtures extends Fixture implements OrderedFixtureInterface, FixtureGroupInterface
{
    /** @var string */
    private $env;

    public function __construct(KernelInterface $kernel)
    {
        $this->env = $kernel->getEnvironment();
    }

    public static function getGroups(): array
    {
        return [FakeData::FIXTURE_GROUP_POST];
    }


    /**
     * @return int
     */
    public function getOrder(): int
    {
        return 2;
    }

    public function load(ObjectManager $manager)
    {
        if($this->env != 'test') {
            return;
        }

        $this->addCategoriesMeta($manager);

        $this->addTags($manager);

//        $params = $this->addParams($manager, $faker);

//        $this->addProducts($manager, $faker, $categories, $params);
    }

    /**
     * @param ObjectManager $manager
     */
    private function addCategoriesMeta(ObjectManager $manager)
    {
        /** @var Category $posuda */
        $posuda = $manager->getRepository(Category::class)->findOneByUuid('37832fef-1dd8-11e2-bf75-742f68689961');
        $posuda->setMetaTitle('Купить товары Одноразовая Посуда / Приборы');
        $posuda->setMetaDescription('Товары Одноразовая Посуда / Приборы оптом');

        $access = $manager->getRepository(Category::class)->findOneByUuid('903c65f6-55e6-11e9-9e9a-ac1f6b855a52');
        $access->setMetaTitle('Купить акксессуары для бумажных стаканов');
        $access->setMetaDescription('Аксессуары для бумажных стаканов оптом');

        $manager->flush();
    }

    private function addTags(ObjectManager $manager)
    {
        $faker = Factory::create();

        $tagCategory = FakeData::TAG_CATEGORY;
        for ($i = 1; $i <= FakeData::FAKE_QTY; $i++){
            $tag = new Tag();
            $tag->setName('Тег ' . $i)
                ->setUrl($faker->url)
                ->setMetaTitle($faker->realText(20))
                ->setMetaDescription($faker->realText())
                ->setBottomText($faker->realText(50))
                ->setH1($faker->title)
                ;

            /** @var Category $category */
            if(isset($tagCategory[$i])) {
                if ($category = $manager->getRepository(Category::class)->find($tagCategory[$i])) {
                    $category->addTag($tag);
                }
            }
            $manager->persist($tag);
        }

        $manager->flush();
    }

    private function addParams(ObjectManager $manager, Generator $faker)
    {
        $params = [];
        foreach (FakeData::PARAMS as $title => $values) {
            $param = new Param();
            $param->setName($title)
                ->setUuid($faker->uuid)
                ->setWeight(rand(1,2))
                ->setIsShow(true);

            foreach ($values as $key => $datum) {
                $value = new Value();
                $value->setUuid($datum['uuid'])
                    ->setName($datum['name'])
                    ->setImage($faker->imageUrl(64, 64, null, true, $datum['name']))
                    ->setParam($param);

                $param->addValue($value);
                $manager->persist($value);

                $params[$title][$key + 1] = $value;
            }

            $manager->persist($param);
        }

        $manager->flush();

        return $params;
    }

    private function addProducts(ObjectManager $manager, Generator $faker, $categories, $params)
    {
        /** @var ProductRepository $repo */
        $repo = $manager->getRepository(Product::class);
        foreach (FakeData::PRODUCTS as $item) {
            $product = new Product();
            $translit = TextHelper::translitRu2En($item['name']);
            $alias = TextHelper::createAlias($item['uuid'],$translit, $repo);
            $product->setName($item['name'])
                ->setAlias($alias)
                ->setUuid($item['uuid'])
                ->setTotalQty($item['total'])
                ->setPrice($item['price'])
                ;

            $manager->persist($product);

            $images = rand(1, 5);
            for ($i = 0; $i < $images; $i++){
                $image = new ProductImage();
                $image->setPath($faker->imageUrl(640,480,null, true, $product->getName()));
                $image->setProduct($product);
                $manager->persist($image);
            }

            /** @var Category $category */
            $category = $categories[$item['category']];
            if($category) {
                $product->addCategory($category);
                $category->addProduct($product);
            }

            foreach ($item['params'] as $paramTitle => $itemValues) {
                foreach ($itemValues as $itemValue) {
                    $value = $params[$paramTitle][$itemValue];

                    $product->addParam($value);
                }
            }
        }

        $manager->flush();
    }
}
