<?php

namespace App\DataFixtures;

use App\Entity\Faq;
use App\Entity\FaqType;
use App\Entity\Payer;
use App\Entity\Security\AuthUser;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Persistence\ObjectManager;

/**
 * Плательщик
 */
class FaqFixtures extends Fixture implements OrderedFixtureInterface, FixtureGroupInterface
{
    private $faqs = [
        [
            "question" => "Возможен ли самовывоз заказанного товара?",
            "answer" => "Да, Вы можете самостоятельно забрать заказанные товары в наших магазинах.",
        ],
        [
            "question" => "Как осуществляется доставка в регионы?",
            "answer" => "Товары нашего магазина доставляют покупателям любая удобная для вас транспортная компания. Просто оформите заказ и напишите в комментарий транспортную компанию, которой отправить вам груз.",
        ],
        [
            "question" => "Как расчитать стоимость доставки в Магадан?",
            "answer" => "Для точного расчета стоимости доставки вашего заказа обращайтесь к операторам call-центра или задайте вопрос через онлайн-консультанта.",
        ],
    ];

    public static function getGroups(): array
    {
        return [FakeData::FIXTURE_GROUP_POST];
    }
    /**
     * @return int
     */
    public function getOrder(): int
    {
        return 12;
    }

    public function load(ObjectManager $manager)
    {
        $faqType = new FaqType();
        $faqType->setName("Вопросы о доставке");
        $faqType->setWeight(1);
        $manager->persist($faqType);

        foreach ($this->faqs as $i => $item) {
            $faq = new Faq();
            $faq->setType($faqType);
            $faq->setQuestion($item['question']);
            $faq->setAnswer($item['answer']);
            $faq->setWeight($i);
            $manager->persist($faq);
        }

        $faqTypeEmpty = new FaqType();
        $faqTypeEmpty->setName("Общие вопросы");
        $faqTypeEmpty->setWeight(0);
        $manager->persist($faqTypeEmpty);

        $manager->flush();
    }
}