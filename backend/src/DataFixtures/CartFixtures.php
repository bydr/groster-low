<?php

namespace App\DataFixtures;

use App\Entity\Order;
use App\Entity\OrderItem;
use App\Entity\Payer;
use App\Entity\Product;
use App\Entity\ReplacementMethod;
use App\Entity\Security\AuthUser;
use App\Entity\ShippingMethod;
use App\Helpers\ValidateHelper;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

/**
 * Class CartFixtures
 */
class CartFixtures extends Fixture implements OrderedFixtureInterface, FixtureGroupInterface
{
    const CUSTOM_BUSSINESS_AREA = "my bussiness area";

    public static function getGroups(): array
    {
        return [FakeData::FIXTURE_GROUP_POST];
    }

    /**
     * @return int
     */
    public function getOrder(): int
    {
        return 10;
    }

    public function load(ObjectManager $manager)
    {
        $user = $manager->getRepository(AuthUser::class)->findOneByEmail(SecurityFixtures::CART_EMAIL);
        $this->addCart($manager, $user);
        $this->addOrder($manager, $user, Order::STATE_NEW);
        $this->addOrder($manager, $user, Order::STATE_FINISHED);
        $this->addOrder($manager, $user, Order::STATE_CANCELED, 1);
    }



    /**
     * Добавление тестовой корзины
     * @param ObjectManager $manager
     * @param $user
     * @return void
     */
    private function addCart(ObjectManager $manager, $user)
    {
        /** @var Product $product */
        $product1 = $manager->getRepository(Product::class)->find(2);
        $item1 = new OrderItem();
        $item1->setProduct($product1);
        $item1->setPrice($product1->getPrice());
        $item1->setQuantity(1);
        $manager->persist($item1);

        $product2 = $manager->getRepository(Product::class)->find(5);
        $item2 = new OrderItem();
        $item2->setProduct($product2);
        $item2->setQuantity(5);
        $item2->setPrice($product2->getPrice());
        $manager->persist($item2);

        $product11 = $manager->getRepository(Product::class)->find(11);
        $item11 = new OrderItem();
        $item11->setProduct($product11);
        $item11->setQuantity(5);
        $item11->setPrice($product11->getPrice());
        $manager->persist($item11);

        $product15 = $manager->getRepository(Product::class)->find(15);
        $item15 = new OrderItem();
        $item15->setProduct($product15);
        $item15->setQuantity(5);
        $item15->setPrice($product15->getPrice());
        $item15->setParent($product11);
        $manager->persist($item15);

        $product16 = $manager->getRepository(Product::class)->find(16);
        $item16 = new OrderItem();
        $item16->setProduct($product16);
        $item16->setQuantity(5);
        $item16->setPrice($product16->getPrice());
        $item16->setParent($product11);
        $manager->persist($item16);

        $cart = new Order();
        $cart->setCustomer($user);
        $cart->addItem($item1);
        $cart->addItem($item2);
        $cart->addItem($item11);
        $cart->addItem($item15);
        $cart->addItem($item16);
//        $cart->setTotal($item1->getTotal() + $item2->getTotal());
        $cart->setBussinessArea(self::CUSTOM_BUSSINESS_AREA);
        $cart->setCheckoutCompleteAt(new \DateTime());
        $cart->setEmail("order@gmail.com");
        $cart->setFio("test new order");
        $cart->setPhone(ValidateHelper::getValidPhone("+79899632145"));
        $cart->setShippingMethod(null);
        $cart->setReplacement(null);
        $cart->setRegion("Тест");
        $manager->persist($cart);

        $manager->flush();
    }

    /**
     * Добавление тестового заказа
     * @param ObjectManager $manager
     * @param $user
     * @param int $state
     * @return void
     */
    private function addOrder(ObjectManager $manager, $user, int $state, ?int $payerId = null)
    {
        /** @var Product $product */
        $product1 = $manager->getRepository(Product::class)->find(1);
        $item1 = new OrderItem();
        $item1->setProduct($product1);
        $item1->setQuantity(1);
//        $item1->setTotal($product1->getPrice());
        $item1->setPrice($product1->getPrice());
        $item1->setSample(1);
        $manager->persist($item1);

        $product2 = $manager->getRepository(Product::class)->find(4);
        $item2 = new OrderItem();
        $item2->setProduct($product2);
        $item2->setQuantity(1);
//        $item2->setTotal($product2->getPrice());
        $item2->setPrice($product2->getPrice());
        $manager->persist($item2);

        $shippingMethod = $manager->getRepository(ShippingMethod::class)->find(3);
        $replacement = $manager->getRepository(ReplacementMethod::class)->find(1);

        $cart = new Order();
        $cart->setCustomer($user);
        $cart->addItem($item1);
        $cart->addItem($item2);
//        $cart->setTotal($item1->getTotal() + $item2->getTotal());
        $cart->setBussinessArea(self::CUSTOM_BUSSINESS_AREA);
        $cart->setState($state);
        $cart->setCheckoutCompleteAt(new \DateTime());
        $cart->setEmail("order@gmail.com");
        $cart->setFio("test new order");
        $cart->setPhone(ValidateHelper::getValidPhone("+79899632145"));
        $cart->setShippingMethod($shippingMethod);
        $cart->setShippingDate(new \DateTime());
        $cart->setReplacement($replacement);
        $cart->setRegion("Тест");

        if($payerId) {
            $payer = $manager->getRepository(Payer::class)->find($payerId);
            $cart->setPayer($payer);
        }

        $manager->persist($cart);

        $manager->flush();
    }
}
