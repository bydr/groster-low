<?php

namespace App\DataFixtures;

use App\Entity\ReplacementMethod;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Persistence\ObjectManager;

/**
 * Способы замены отсутствующего товара
 */
class ReplacementFixtures extends Fixture implements OrderedFixtureInterface, FixtureGroupInterface
{
    public static function getGroups(): array
    {
        return [FakeData::FIXTURE_GROUP_POST];
    }
    /**
     * @return int
     */
    public function getOrder(): int
    {
        return 6;
    }

    private $data = [
        [
            'title' => 'Позвонить мне',
            'desc' => 'Если не удастся, выбирает сборщик',
        ],
        [
            'title' => 'Позвонить мне',
            'desc' => 'Если не удастся, убрать товар',
        ],
        [
            'title' => 'Не звонить мне',
            'desc' => 'Замену выбирает сборщик',
        ],
        [
            'title' => 'Не звонить мне',
            'desc' => 'Убрать отсутствующий товар',
        ],
    ];
    public function load(ObjectManager $manager)
    {
        foreach ($this->data as $key => $data) {
            $method = new ReplacementMethod();
            $method->setTitle($data['title']);
            $method->setDescription($data['desc']);
            $method->setWeight($key);
            $manager->persist($method);
        }

        $manager->flush();

    }
}