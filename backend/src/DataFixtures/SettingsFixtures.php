<?php

namespace App\DataFixtures;

use App\Entity\Settings;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Persistence\ObjectManager;

/**
 * Class SettingsFixtures
 */
class SettingsFixtures extends Fixture implements OrderedFixtureInterface, FixtureGroupInterface
{
    public static function getGroups(): array
    {
        return [FakeData::FIXTURE_GROUP_POST];
    }
    /**
     * @return int
     */
    public function getOrder(): int
    {
        return 3;
    }

    public function load(ObjectManager $manager)
    {
        $settings = new Settings();
        $settings->setId(Settings::DEFAULT_ID);
        $settings->setDeliveryShift(1);
        $settings->setDeliveryFastTime('11:00');
        $settings->setMinManyQuantity(100);

        $manager->persist($settings);
        $manager->flush();
    }
}