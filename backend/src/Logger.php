<?php

namespace App;

use DateTimeZone;
use Monolog\Handler\StreamHandler;
use Monolog\Logger as BaseLogger;
use Throwable;

/**
 * Class Logger
 */
class Logger extends BaseLogger
{

    public function __construct(string $name, array $handlers = [], array $processors = [], ?DateTimeZone $timezone = null)
    {
        parent::__construct($name, $handlers, $processors, $timezone);
        $logDir = dirname(__DIR__) . '/var/log/api/' . date('Y/m');

        if(!is_dir($logDir)) {
            mkdir($logDir, 0740, true);
        }

        if('prod' == $_ENV['APP_ENV']) {
            $level = parent::INFO;
        } else {
            $level = parent::DEBUG;
        }

        $logFile = $logDir . DIRECTORY_SEPARATOR . date('d') . '.log';
        if (!is_file($logFile)) {
            touch($logFile);
        }

        $this->pushHandler(new StreamHandler($logFile, $level));
    }

    /**
     * Сохранение ошибки в логи
     *
     * @param Throwable|null $error
     * @return void
     */
    public function errorWithExc(?Throwable $error): void
    {
        self::error(sprintf("%s in %s():%s", $error->getMessage(), $error->getFile(), $error->getLine()));
    }
}