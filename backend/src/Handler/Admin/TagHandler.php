<?php

namespace App\Handler\Admin;

use App\Controller\ApiController;
use App\Entity\Category;
use App\Entity\Tag;
use App\Helpers\Params;
use App\Interfaces\Admin\TagInterface;
use App\RequestJson;
use Doctrine\ORM\EntityManagerInterface;

class TagHandler implements TagInterface
{
    /** @var EntityManagerInterface */
    protected $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * Обновление тега
     * @param array{"name": string, "url": string, "h1": string, "meta_title": string, "meta_description": string, "text": string} $request
     * @param int $id
     * @param int $responseCode
     * @param array $responseHeaders
     */
    public function updateOne(RequestJson $json, int $id): ?Tag
    {
        /** @var Tag $tag */
        if(0 == $id) {
            $tag = new Tag();
        }else{
            $tag = $this->em->getRepository(Tag::class)->find($id);
        }

        if(empty($tag)) {
            return null;
        }

        $tag->setName($json->getString(Params::NAME));
        $tag->setUrl($json->getString(Params::URL));
        $tag->setH1($json->getString(Params::H1));
        $tag->setMetaTitle($json->getString(Params::META_TITLE));
        $tag->setMetaDescription($json->getString(Params::META_DESCRIPTION));
        $tag->setBottomText($json->getString(Params::TEXT));

        $this->em->persist($tag);
        $this->em->flush();

        return $tag;
    }

    /**
     * Удаление тега
     * @param int $id
     * @param int $responseCode
     * @param array $responseHeaders
     */
    public function deleteOne(int $id)
    {
        $tag = $this->em->getRepository(Tag::class)->find($id);

        if(empty($tag)) {
            return;
        }

        $this->em->remove($tag);
        $this->em->flush();
    }
}