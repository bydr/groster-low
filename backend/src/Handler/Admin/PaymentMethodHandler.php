<?php

namespace App\Handler\Admin;

use App\ApiError;
use App\Entity\PaymentMethod;
use App\Handler\Handler;
use App\Helpers\Params;
use App\Interfaces\Admin\PaymentMethodInterface;
use App\RequestJson;

/**
 * Способы оплаты
 */
class PaymentMethodHandler extends Handler implements PaymentMethodInterface
{

    public function getAll(): array
    {
        return $this->em->getRepository(PaymentMethod::class)->findBy([],['id' => 'ASC']);
    }

    public function methodUpdate(RequestJson $json, ?string $uid, ?string &$error): string
    {
        if(empty($uid)) {
            $method = new PaymentMethod();
            $this->em->persist($method);
        }else{
            $method = $this->em->getRepository(PaymentMethod::class)->findOneByUid($uid);
        }

        if(is_null($method)) {
            $error = ApiError::ERROR_INVALID_JSON;
            return "";
        }

        $method->setName($json->getString(Params::NAME));
        $method->setWeight($json->getInt(Params::WEIGHT));
        $method->setIsActive($json->getBool(Params::IS_ACTIVE));

        $this->em->flush();

        return $method->getUid();
    }
}