<?php

namespace App\Handler\Admin;

use App\ApiError;
use App\Controller\ApiController;
use App\Entity\Banner;
use App\EventListener\EmailListener;
use App\Handler\Handler;
use App\Helpers\ImageHelper;
use App\Helpers\Params;
use App\Interfaces\Admin\BannerInterface;
use App\RequestJson;
use App\Service\Cache;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use \Exception;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\KernelInterface;

/**
 * Баннеры
 */
class BannerHandler extends Handler implements BannerInterface
{
    private $imageHelper;

    public function __construct(EntityManagerInterface $em,
                                KernelInterface $kernel,
                                ContainerInterface $container,
                                EmailListener $emailListener,
                                Cache $cache,
                                ImageHelper $imageHelper)
    {
        parent::__construct($em, $kernel, $container, $emailListener, $cache);
        $this->imageHelper = $imageHelper;
    }

    public function getAll(): array
    {
        return $this->em->getRepository(Banner::class)->findAll();
    }

    public function add(RequestJson $json): ?string
    {
        $banner = new Banner();
        if (!$this->fillBanner($banner, $json)) {
            return ApiError::ERROR_INVALID_DATA;
        }

        $this->em->persist($banner);
        $this->em->flush();

        return null;
    }

    public function update(RequestJson $json, int $id): ?string
    {
        /** @var Banner $banner */
        $banner = $this->em->getRepository(Banner::class)->find($id);

        if(!$banner) {
            return ApiError::ERROR_NOT_FOUND;
        }

        if (!$this->fillBanner($banner, $json)) {
            return ApiError::ERROR_INVALID_DATA;
        }

        $banner->setUpdatedAt(new DateTime());

        $this->em->flush();

        return null;
    }

    public function delete(int $id): ?string
    {
        /** @var Banner $banner */
        $banner = $this->em->getRepository(Banner::class)->find($id);

        if(!$banner) {
            return ApiError::ERROR_NOT_FOUND;
        }

        $this->geleteImage($banner->getDesktop());
        $this->geleteImage($banner->getTablet());
        $this->geleteImage($banner->getMobile());

        $this->em->remove($banner);
        $this->em->flush();

        return null;
    }

    private function fillBanner(Banner &$banner, RequestJson $json): bool
    {
        $desktop = $json->getString(Params::DESKTOP);
        $tablet = $json->getString(Params::TABLET);
        $mobile = $json->getString(Params::MOBILE);

        if(empty($desktop)) {
            ApiController::getLogger()->error("Not found desktop image");
            return false;
        }

        $url = $json->getString(Params::URL);
        $weight = $json->getInt(Params::WEIGHT);
        $type = $json->getInt(Params::TYPE);

        if(empty($url) || !in_array($type, [Banner::TYPE_SLIDER, Banner::TYPE_HOME, Banner::TYPE_PRODUCT_LIST, Banner::TYPE_CATALOG])) {
            ApiController::getLogger()->error("Not found url or wrong type");
            return false;
        }

        $banner->setUrl($url);
        $banner->setWeight($weight);
        $banner->setType($type);

        if(!empty($desktop)) {
            $img = $this->saveImage($banner->getDesktop(), $desktop);
            if(empty($img)) {
                ApiController::getLogger()->error("Did not create new desktop img");
                return false;
            }
            $banner->setDesktop($img);
        }
        if(!empty($tablet)) {
            $img = $this->saveImage($banner->getTablet(), $tablet);
            if(empty($img)) {
                ApiController::getLogger()->error("Did not create new tablet img");
                return false;
            }
            $banner->setTablet($img);
        }
        if(!empty($mobile)) {
            $img = $this->saveImage($banner->getMobile(), $mobile);
            if(empty($img)) {
                ApiController::getLogger()->error("Did not create new mobile img");
                return false;
            }
            $banner->setMobile($img);
        }

        return true;
    }

    /**
     * Сохранение новых картинок для баннера
     * @param string|null $oldImg
     * @param string $img
     * @return string|null
     * @throws Exception
     */
    private function saveImage(?string $oldImg, string $img): ?string
    {
        $webpath = null;

            if(!empty($img)) {
                try {
                    //TODO когда будет S3, переделать процесс сохранения картинки
                    $folder = sprintf("%s/public/%s", $this->ci->getParameter('kernel.project_dir'), Banner::FOLDER);
                    if($oldImg) {
                        $file = $this->imageHelper->getImageNameFromWebpath($oldImg);
                        if($file) {
                            unlink($folder . DIRECTORY_SEPARATOR . $file);
                        }

                    }

                    $filename = $this->imageHelper->saveBase64Image($img, $folder);
                    if(!empty($filename)) {
                        $webpath = sprintf("%s/%s/%s", $this->ci->getParameter('static_domain'), Banner::FOLDER, $filename);
                    }
                }catch (Exception $e) {
                    ApiController::getLogger()->error(sprintf("Failed to save banner image: %s", $e->getMessage()));
                }
            } else {
                ApiController::getLogger()->error(printf("Does not exist image for save banner"));
            }

        return $webpath;
    }

    private function geleteImage(?string $webpath)
    {
        if(empty($webpath)) {
            return;
        }

        if(!preg_match('~.*/(.*\.\w{3,4})~', $webpath, $matches)) {
            return;
        }

        $filepath = sprintf("%s/public/%s/%s", $this->ci->getParameter('kernel.project_dir'), Banner::FOLDER, $matches[1]);
        if(is_file($filepath)) {
            unlink($filepath);
        }
    }
}