<?php

namespace App\Handler\Admin;

use App\ApiError;
use App\Controller\ApiController;
use App\Entity\Category;
use App\Entity\Tag;
use App\Handler\Handler;
use App\Helpers\CategoryHelper;
use App\Helpers\Params;
use App\Helpers\TextHelper;
use App\Interfaces\Admin\CategoryInterface;
use App\Repository\CategoryRepository;
use App\RequestJson;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Работа с категориями
 */
class CategoryHandler extends Handler implements CategoryInterface
{
    public function findOne(int $id): ?Category
    {
        return $this->em->getRepository(Category::class)->find($id);
    }

    /**
     * @param RequestJson $json
     * @param int $id
     * @param string|null $errorCode
     */
    public function updateOne(RequestJson $json, int $id, ?string &$errorCode): void
    {
        /** @var CategoryRepository $categoryRepo */
        $categoryRepo = $this->em->getRepository(Category::class);

        /** @var Category $category */
        $category = $categoryRepo->find($id);

        if(empty($category)) {
            $errorCode = ApiError::ERROR_NOT_FOUND;
            return;
        }

        $name = $json->getString(Params::NAME);
        $category->setName($name);

        $alias = $json->getString(Params::ALIAS);
        if(empty($alias)) {
            $errorCode = ApiError::ERROR_INVALID_JSON;
            return;
        }

        $alias = TextHelper::createAlias($category->getUuid(), $alias, $categoryRepo);
        $category->setAlias($alias);
        $category->setHelpPage($json->getString(Params::HELP_PAGE));
        $category->setMetaTitle($json->getString(Params::META_TITLE));
        $category->setMetaDescription($json->getString(Params::META_DESCRIPTION));
        $category->removeAllTags();

        if(!empty($json->getArray(Params::TAGS))) {
            $tags = $this->em->getRepository(Tag::class)->findBy(['id' => $json->getArray(Params::TAGS)]);

            foreach ($tags as $tag) {
                $category->addTag($tag);
            }
        }

        if(isset($request['help_page'])) {
            $category->setHelpPage($request['help_page']);
        }

        $this->em->flush();
    }
}