<?php

namespace App\Handler\Admin;

use App\ApiError;
use App\Entity\Faq;
use App\Entity\FaqType;
use App\Handler\Handler;
use App\Helpers\Params;
use App\Interfaces\Admin\FaqInterface;
use App\RequestJson;

/**
 * Блоки вопрос-ответ
 */
class FaqHandler extends Handler implements FaqInterface
{

    public function allBlocks(): array
    {
        return $this->em->getRepository(FaqType::class)->findBy([], ['id' => 'DESC']);
    }

    public function updateBlock(RequestJson $json, int $id): ?string
    {
        if(0 == $id) {
            $block = new FaqType();
            $this->em->persist($block);
        }else{
            $block = $this->em->getRepository(FaqType::class)->find($id);
        }

        if(is_null($block)) {
            return ApiError::ERROR_NOT_FOUND;
        }

        $block->setName($json->getString(Params::NAME));
        $block->setWeight($json->getInt(Params::WEIGHT));

        $this->em->flush();

        return null;
    }

    public function deleteBlock(int $id)
    {
        $block = $this->em->getRepository(FaqType::class)->find($id);

        if(is_null($block)) {
            return;
        }

        $this->em->remove($block);
        $this->em->flush();
    }

    public function allQuestions(): array
    {
        return $this->em->getRepository(Faq::class)->findBy([], ['id' => 'DESC']);
    }

    public function updateQuestion(RequestJson $json, int $id): ?string
    {
        if(0 == $id) {
            $question = new Faq();
            $this->em->persist($question);
        }else{
            $question = $this->em->getRepository(Faq::class)->find($id);
        }

        if(is_null($question)) {
            return ApiError::ERROR_NOT_FOUND;
        }

        /** @var FaqType $block */
        $block = $this->em->getRepository(FaqType::class)->find($json->getInt(Params::BLOCK));

        if(is_null($block)) {
            return ApiError::ERROR_INVALID_BLOCK;
        }

        $question->setAnswer($json->getString(Params::ANSWER));
        $question->setQuestion($json->getString(Params::TITLE));
        $question->setWeight($json->getInt(Params::WEIGHT));
        $question->setType($block);
        $block->addFaq($question);

        $this->em->flush();

        return null;
    }

    public function deleteQuestion(int $id)
    {
        $question = $this->em->getRepository(Faq::class)->find($id);

        if(is_null($question)) {
            return;
        }

        $this->em->remove($question);
        $this->em->flush();
    }
}