<?php

namespace App\Handler\Admin;

use App\Entity\ReplacementMethod;
use App\Handler\Handler;
use App\Helpers\Params;
use App\Interfaces\Admin\OrderInterface;
use App\RequestJson;

/**
 * Заказы
 */
class OrderHandler extends Handler implements OrderInterface
{

    public function replacementUpdate(RequestJson $json, int $id): int
    {
        if ($id > 0) {
            $method = $this->em->getRepository(ReplacementMethod::class)->find($id);
        }else{
            $method = new ReplacementMethod();
            $this->em->persist($method);
        }

        if (is_null($method)) {
            return 0;
        }

        $method->setTitle($json->getString(Params::TITLE));
        $method->setDescription($json->getString(Params::DESCRIPTION));
        $method->setWeight($json->getInt(Params::WEIGHT));

        $this->em->flush();

        return $method->getId();
    }
}