<?php

namespace App\Handler\Admin;

use App\ApiError;
use App\Entity\ShippingLimit;
use App\Entity\ShippingMethod;
use App\Handler\Handler;
use App\Helpers\Params;
use App\Interfaces\Admin\ShippingMethodInterface;
use App\Repository\ShippingMethodRepository;
use App\RequestJson;

/**
 * Способы доставки
 */
class ShippingMethodHandler extends Handler implements ShippingMethodInterface
{

    public function getAll(): array
    {
        return $this->em->getRepository(ShippingMethod::class)->findAll();
    }

    public function methodUpdate(RequestJson $json, ?string $alias, ?string &$error): string
    {
        if(empty($alias)) {
            $method = new ShippingMethod();
            $this->em->persist($method);
        }else{
            $method = $this->em->getRepository(ShippingMethod::class)->findOneByAlias($alias);
        }

        if(is_null($method)) {
            $error = ApiError::ERROR_INVALID_JSON;
            return "";
        }

        $method->setName($json->getString(Params::NAME));
        $method->setDescription($json->getString(Params::DESCRIPTION));
        $method->setWeight($json->getInt(Params::WEIGHT));
        $method->setCost($json->getString(Params::COST));
        $method->setUseAddress($json->getBool(Params::USE_ADDRESS));
        $method->setIsFast($json->getBool(Params::IS_FAST));
        $method->setIsActive($json->getBool(Params::IS_ACTIVE));

        if(empty($method->getAlias())) {
            $alias = $json->getString(Params::ALIAS);

            $existsAlias = $this->em->getRepository(ShippingMethod::class)->findOneByAlias($alias);
            if(!is_null($existsAlias)) {
                $error = ApiError::ERROR_DUPLICATE_ALIAS;
                return "";
            }

            $method->setAlias($alias);
        }

        $this->em->flush();

        return $method->getAlias();
    }

    public function methodGet(string $alias): ?ShippingMethod
    {
        return $this->em->getRepository(ShippingMethod::class)->findOneByAlias($alias);
    }

    public function methodLimitsAll(string $alias): array
    {
        $method = $this->em->getRepository(ShippingMethod::class)->findOneByAlias($alias);

        if(is_null($method)) {
            return [];
        }

        return $method->getLimits()->toArray();
    }

    public function methodLimitUpdate(RequestJson $json, string $alias, int $id, ?string &$error): int
    {
        /** @var ShippingMethod $method */
        $method = $this->em->getRepository(ShippingMethod::class)->findOneByAlias($alias);
        if(is_null($method)) {
            $error = ApiError::ERROR_NOT_FOUND;
            return 0;
        }

        if(0 == $id) {
            $limit = new ShippingLimit();
            $limit->setMethod($method);
            $this->em->persist($limit);
            $method->addLimit($limit);
        }else{
            $limit = $this->em->getRepository(ShippingLimit::class)->find($id);
        }

        if(is_null($limit)) {
            $error = ApiError::ERROR_NOT_FOUND;
            return 0;
        }

        $limit->setShippingCost($json->getInt(Params::SHIPPING_COST));

        switch ($limit->getType()) {
            case ShippingLimit::TYPE_COST:
                $limit->setOrderMinCost($json->getInt(Params::ORDER_MIN_COST));
                $limit->setRegion($json->getString(Params::REGION));
                break;
            case ShippingLimit::TYPE_REGION:
                $limit->setOrderMinCost(0);
                $limit->setRegion($json->getString(Params::REGION));
                break;
            default:
                $error = ApiError::ERROR_INVALID_DATA;
                return 0;
        }

        $this->em->flush();

        return $limit->getId();
    }

    public function methodLimitDelete(string $alias, int $id): bool
    {
        /** @var ShippingMethod $method */
        $method = $this->em->getRepository(ShippingMethod::class)->findOneByAlias($alias);
        if(is_null($method)) {
            return false;
        }

        if(0 == $id) {
            return false;
        }

        foreach ($method->getLimits() as $limit) {
            if($limit->getId() == $id) {
                $this->em->remove($limit);
                $this->em->flush();
                break;
            }
        }

        return true;
    }
}