<?php

namespace App\Handler\Admin;

use App\Controller\ApiController;
use App\Entity\Shop;
use App\Entity\Store;
use App\EventListener\EmailListener;
use App\Handler\Handler;
use App\Helpers\ImageHelper;
use App\Helpers\Params;
use App\Helpers\ValidateHelper;
use App\Interfaces\Admin\ShopInterface;
use App\Repository\ShopRepository;
use App\Repository\StoreRepository;
use App\RequestJson;
use App\Service\Cache;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\KernelInterface;

/**
 * class ShopHandler
 */
class ShopHandler extends Handler implements ShopInterface
{
    private $imageHelper;

    public function __construct(EntityManagerInterface $em,
                                KernelInterface $kernel,
                                ContainerInterface $container,
                                EmailListener $emailListener,
                                Cache $cache,
                                ImageHelper $imageHelper)
    {
        parent::__construct($em, $kernel, $container, $emailListener, $cache);
        $this->imageHelper = $imageHelper;
    }

    public function getAllActive(): array
    {
        return $this->em->getRepository(Store::class)->findAll();
    }

    public function getOne(string $uuid): ?Store
    {
        /** @var StoreRepository $repo */
        $repo = $this->em->getRepository(Store::class);
        return $repo->findOneByUuid($uuid);
    }

    public function update(string $uid, RequestJson $json): ?Store
    {
        /** @var Store $shop */
        $shop = $this->em->getRepository(Store::class)->findOneByUuid($uid);
        if(empty($shop)) {
            return null;
        }

        $shop->setUpdateAt(new DateTime());
        if (!$this->fillShop($shop, $json)) {
            return null;
        }

        $this->em->flush();

        return $shop;
    }

    /**
     * @param Store $shop
     * @param RequestJson $json
     * @return bool
     * @throws Exception
     */
    private function fillShop(Store &$shop, RequestJson $json):bool
    {
        $address = $json->getString(Params::ADDRESS);
        if(empty($address)) {
            ApiController::getLogger()->info(sprintf("Ошибка адреса для магазина: %s", $address));
            return false;
        }
        $shop->setAddress($address);

        $phoneInput = $json->getString(Params::PHONE);
        if(!empty($phoneInput)) {
            if(!$phone = ValidateHelper::getValidPhone($phoneInput)) {
                ApiController::getLogger()->info(sprintf("Ошибка телефона для магазина: %s", $phoneInput));
                return false;
            }
            $shop->setPhone($phone);
        }

        $email = $json->getString(Params::EMAIL);
        if(!empty($email)) {
            if(!ValidateHelper::validateEmail($email)) {
                ApiController::getLogger()->info(sprintf("Ошибка email для магазина: %s", $email));
                return false;
            }
            $shop->setEmail($email);
        }

        $shop->setSchedule($json->getString(Params::SCHEDULE));

        $shop->setIsActive($json->getBool(Params::IS_ACTIVE));

        $shop->setDescription($json->getString(Params::DESCRIPTION));
        $coordinates = $json->getArray(Params::COORDINATES);
        $lat = $coordinates[Params::LATITUDE];
        $lon = $coordinates[Params::LONGITUDE];

        if(empty($lat) || empty($lon)) {
            ApiController::getLogger()->info(sprintf("Ошибка координат для магазина: lat - %s, lon - %s", $lat, $lon));
            return false;
        }

        $shop->setLatitude($lat);
        $shop->setLongitude($lon);

        $img = $json->getString(Params::IMAGE);
        if(!empty($img)) {
            //TODO когда будет S3, переделать процесс сохранения картинки
            $folder = sprintf("%s/public/images/shop", $this->ci->getParameter('kernel.project_dir'));
            if($oldImg = $shop->getImage()) {
                unlink($folder . DIRECTORY_SEPARATOR . $oldImg);
            }

            $filename = $this->imageHelper->saveBase64Image($img, $folder);
            if(!empty($filename)) {
                $webpath = sprintf("%s/%s/%s", $this->ci->getParameter('static_domain'), Store::IMAGE_FOLDER, $filename);

                $shop->setImage($webpath);

            }

        }

        return true;
    }
}