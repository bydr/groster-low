<?php

namespace App\Handler\Admin;

use App\ApiError;
use App\Entity\Faq;
use App\Entity\FaqType;
use App\Entity\Settings;
use App\Handler\Handler;
use App\Helpers\Params;
use App\Interfaces\Admin\FaqInterface;
use App\Interfaces\Admin\SettingsInterface;
use App\RequestJson;

/**
 * Общие настройки
 */
class SettingsHandler extends Handler implements SettingsInterface
{

    public function getSettings(): ?Settings
    {
        return $this->em->getRepository(Settings::class)->find(Settings::DEFAULT_ID);
    }

    public function update(RequestJson $json): ?string
    {
        /** @var Settings $settings */
        $settings = $this->em->getRepository(Settings::class)->find(Settings::DEFAULT_ID);

        if(!$settings) {
            return ApiError::ERROR_NOT_FOUND;
        }

        $settings->setDeliveryFastTime($json->getString(Params::DELIVERY_FAST_TIME));
        $settings->setDeliveryShift($json->getInt(Params::DELIVERY_SHIFT));
        $settings->setMinManyQuantity($json->getInt(Params::MIN_MANY_QUANTITY));
        $settings->setHolidays($json->getArray(Params::HOLIDAYS));
        $settings->setTelegram($json->getString(Params::TELEGRAM));
        $settings->setViber($json->getString(Params::VIBER));
        $settings->setWhatsapp($json->getString(Params::WHATSAPP));

        $this->em->flush();

        return null;
    }
}