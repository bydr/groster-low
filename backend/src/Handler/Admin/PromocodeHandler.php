<?php

namespace App\Handler\Admin;

use App\ApiError;
use App\Controller\ApiController;
use App\Entity\Category;
use App\Entity\Discount;
use App\Handler\Handler;
use App\Helpers\CategoryHelper;
use App\Helpers\Params;
use App\Interfaces\Admin\PromocodeInterface;
use App\Logger;
use App\Repository\CategoryRepository;
use App\RequestJson;
use DateTime;
use Exception;

/**
 * Работа с промокодами
 */
class PromocodeHandler  extends Handler implements PromocodeInterface
{
    public function list(): array
    {
        return $this->em->getRepository(Discount::class)->findAll();
    }

    /**
     * @param RequestJson $json
     * @return Discount|null
     * @throws Exception
     */
    public function create(RequestJson $json): ?Discount
    {
        $promocode = new Discount();
        $this->em->persist($promocode);
        $error = $this->save($promocode, $json);
        if(!is_null($error)) {
            return null;
        }

        return $promocode;
    }

    public function getOne(int $id): ?Discount
    {
        return $this->em->getRepository(Discount::class)->find($id);
    }

    /**
     * @param int $id
     * @param RequestJson $json
     * @param string|null $error
     * @return Discount|null
     * @throws Exception
     */
    public function updateOne(int $id, RequestJson $json, ?string &$error): ?Discount
    {
        $promocode = $this->em->getRepository(Discount::class)->find($id);
        if(!$promocode) {
            $error = ApiError::ERROR_NOT_FOUND;
            return null;
        }

        $error = $this->save($promocode, $json);
        if(!is_null($error)) {
            return null;
        }

        return $promocode;
    }

    /**
     * Обновление данных по промокоду
     *
     * @param Discount $promocode
     * @param RequestJson $json
     * @return bool
     * @throws \Exception
     */
    private function save(Discount $promocode, RequestJson $json): ?string
    {
        $name = $json->getString(Params::NAME);
        $code = $json->getString(Params::CODE);
        $type = $json->getInt(Params::TYPE);
        $value = $json->getString(Params::VALUE);
        $reusable = $json->getBool(Params::REUSABLE);
        $startStr = $json->getString(Params::START);
        $finishStr = $json->getString(Params::FINISH);
        $categoryIds = $json->getArray(Params::CATEGORIES);

        if(empty($code) || empty($startStr) || empty($finishStr) || empty($categoryIds)) {
            return ApiError::ERROR_DOES_NOT_EXIST_REQUIRED_FIELDS;
        }

        if(!in_array($type, [Discount::TYPE_RATE, Discount::TYPE_MONEY])) {
            return ApiError::ERROR_DOES_NOT_EXIST_TYPE;
        }

        if($value <= 0) {
            return ApiError::ERROR_INVALID_PROMO_VALUE;
        }

        $start = new DateTime($startStr);
        $finish = new DateTime($finishStr);

        if($start->getTimestamp() >= $finish->getTimestamp()) {
            return ApiError::ERROR_INVALID_DATE;
        }

        $promocode->setName($name);
        $promocode->setType($type);
        $promocode->setCode($code);
        $promocode->setValue($value);
        $promocode->setReusable($reusable);
        $promocode->setStart($start);
        $promocode->setFinish($finish);

        // Категории из запроса
        $requestCategories = $this->em->getRepository(Category::class)->findBy(['uuid' => $categoryIds]);

        // Все категории из запроса, включая дочерние
        $selectedCategories = CategoryHelper::getChildrenIds($requestCategories);

        foreach ($promocode->getCategories() as $category) {
            if(!in_array($category->getId(), $selectedCategories)) {
                $promocode->removeCategory($category);
                continue;
            }

            $index = array_search($category->getId(), $selectedCategories);
            unset($selectedCategories[$index]);
        }

        $categories = $this->em->getRepository(Category::class)->findBy(['id' => $selectedCategories]);

        /** @var Category $category */
        foreach ($categories as $category) {
            if(!$category || Category::TYPE_PRODUCT_SECTION != $category->getType()) {
                continue;
            }

            $promocode->addCategory($category);
        }

        $this->em->flush($promocode);

        return null;
    }

    public function serverCreate(RequestJson $json): ?string
    {
        $promocode = new Discount();
        $this->em->persist($promocode);

        return $this->save($promocode, $json);
    }

    public function serverUpdateOne(string $code, RequestJson $json): ?string
    {
        if(empty($code)) {
            return ApiError::ERROR_NOT_FOUND;
        }

        $promocode = $this->em->getRepository(Discount::class)->findOneByCode($code);
        if(!$promocode) {
            return ApiError::ERROR_NOT_FOUND;
        }

        return $this->save($promocode, $json);
    }

    public function serverGetOne(string $code): ?Discount
    {
        if(empty($code)) {
            return null;
        }

        return $this->em->getRepository(Discount::class)->findOneByCode($code);
    }
}