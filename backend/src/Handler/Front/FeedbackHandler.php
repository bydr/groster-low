<?php

namespace App\Handler\Front;

use App\ApiError;
use App\Entity\Category;
use App\Entity\CustomRequest;
use App\EventListener\EmailListener;
use App\Handler\Handler;
use App\Helpers\FaqHelper;
use App\Helpers\Params;
use App\Helpers\ValidateHelper;
use App\Interfaces\Front\FeedbackInterface;
use App\RequestJson;
use App\Service\Cache;
use App\Service\RetailCRM;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\KernelInterface;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;

/**
 * Обратная связь
 */
class FeedbackHandler extends Handler implements FeedbackInterface
{
    /** @var RetailCRM */
    private $crm;

    const STATUS_PRODUCER = 1;
    const STATUS_DISTRIBUTOR = 2;

    public function __construct(EntityManagerInterface $em,
                                KernelInterface $kernel,
                                ContainerInterface $container,
                                EmailListener $emailListener,
                                Cache $cache,
                                RetailCRM $crm)
    {
        parent::__construct($em, $kernel, $container, $emailListener, $cache);
        $this->crm = $crm;
    }

    public function question(RequestJson $json): ?string
    {
        $name = $json->getString(Params::NAME);
        $phone = ValidateHelper::getValidPhone($json->getString(Params::PHONE));
        $email = $json->getString(Params::EMAIL);
        $message = $json->getString(Params::MESSAGE);

        if(empty($name) || empty($message)) {
            return ApiError::ERROR_INVALID_JSON;
        }

        if(empty($phone)) {
            return ApiError::ERROR_INVALID_PHONE;
        }

        if(!ValidateHelper::validateEmail($email)) {
            return ApiError::ERROR_INVALID_EMAIL;
        }

        $this->mailer->sendFeedback($name, $phone, $email, $message);

        return null;
    }

    public function faq(): string
    {
        return FaqHelper::getList($this->cache, $this->em);
    }

    public function recall(RequestJson $json): ?string
    {
        $name = $json->getString(Params::NAME);
        $phone = ValidateHelper::getValidPhone($json->getString(Params::PHONE));

        if(empty($name)) {
            return ApiError::ERROR_INVALID_JSON;
        }

        if(empty($phone)) {
            return ApiError::ERROR_INVALID_PHONE;
        }

        if($this->em->getRepository(CustomRequest::class)->isDoubleRecall($phone)) {
            return ApiError::ERROR_DUPLICATE_PHONE;
        }

        $recall = new CustomRequest();
        $recall->setType(CustomRequest::TYPE_RECALL);
        $recall->setName($name);
        $recall->setPhone($phone);
        $this->em->persist($recall);
        $this->em->flush();

        $this->mailer->sendRecall($name, $phone);

        $recall->setCrmId($this->crm->recall($recall));
        $this->em->flush();

        return null;
    }

    public function customEmail(RequestJson $json): ?string
    {
        $message = $json->getString(Params::MESSAGE);

        if(empty($message)) {
            return ApiError::ERROR_INVALID_JSON;
        }

        $this->mailer->sendCustomEmail($message);

        return null;
    }

    public function subscribe(RequestJson $json): ?string
    {
        $email = $json->getString(Params::EMAIL);

        if(!ValidateHelper::validateEmail($email)) {
            return ApiError::ERROR_INVALID_EMAIL;
        }

        if(!$this->crm->subscribe($email)) {
            return ApiError::ERROR_UNHANDLED;
        }

        return null;
    }

    public function unsubscribe(RequestJson $json): ?string
    {
        $email = $json->getString(Params::EMAIL);

        if(!ValidateHelper::validateEmail($email)) {
            return ApiError::ERROR_INVALID_EMAIL;
        }

        if(!$this->crm->unsubscribe($email)) {
            return ApiError::ERROR_UNHANDLED;
        }

        return null;
    }

    public function foundCheaper(RequestJson $json): ?string
    {
        $link = $json->getString(Params::LINK);
        $phone = ValidateHelper::getValidPhone($json->getString(Params::PHONE));
        $message = $json->getString(Params::MESSAGE);

        if(empty($phone)) {
            return ApiError::ERROR_INVALID_PHONE;
        }

        $customRequest = new CustomRequest();
        $customRequest->setType(CustomRequest::TYPE_CHEAPER);
        $customRequest->setPhone($phone);
        $customRequest->setMessage(sprintf("Ссылка: %s\n%s", $link, $message));
        $this->em->persist($customRequest);
        $this->em->flush();

        $this->mailer->sendFoundCheaper($phone, $link, $message);

        $customRequest->setCrmId($this->crm->createTask($customRequest));
        $this->em->flush();

        return null;
    }

    public function notFoundNeeded(RequestJson $json): ?string
    {
        $phone = ValidateHelper::getValidPhone($json->getString(Params::PHONE));
        $message = $json->getString(Params::MESSAGE);

        if(empty($phone)) {
            return ApiError::ERROR_INVALID_PHONE;
        }

        $customRequest = new CustomRequest();
        $customRequest->setType(CustomRequest::TYPE_NOT_FOUND);
        $customRequest->setPhone($phone);
        $customRequest->setMessage($message);
        $this->em->persist($customRequest);
        $this->em->flush();

        $this->mailer->sendNotFoundNeeded($phone, $message);

        $customRequest->setCrmId($this->crm->createTask($customRequest));
        $this->em->flush();

        return null;
    }

    public function commercialDepartment(RequestJson $json): ?string
    {
        $categoryUuid = $json->getString(Params::CATEGORY);
        $brand = $json->getString(Params::BRAND);
        $representation = $json->getString(Params::REPRESENTATION);
        $status = $json->getString(Params::STATUS);
        $fio = $json->getString(Params::FIO);
        $email = $json->getString(Params::EMAIL);
        $phone = $json->getString(Params::PHONE);
        $info = $json->getString(Params::INFO);
        $socialBrand = $json->getString(Params::SOCIAL_BRAND);
        $site = $json->getString(Params::SITE);

        if(empty($fio) || empty($categoryUuid) || empty($brand) || empty($representation) || empty($status)) {
            return ApiError::ERROR_INVALID_JSON;
        }

        /** @var Category $category */
        $category = $this->em->getRepository(Category::class)->findOneBy(['uuid' => $categoryUuid]);
        if(!$category) {
            return ApiError::ERROR_INVALID_CATEGORY;
        }
        $categoryName = $category->getName();

        if(!ValidateHelper::validateEmail($email)) {
            return ApiError::ERROR_INVALID_EMAIL;
        }

        if(empty($phone)) {
            return ApiError::ERROR_INVALID_PHONE;
        }

        $corPhone = ValidateHelper::getValidPhone($phone);
        if(empty($corPhone)) {
            return ApiError::ERROR_INVALID_PHONE;
        }

        $this->mailer->sendCommercialFeedback($categoryName, $brand, $representation, $status, $fio, $phone, $email,
            $info, $socialBrand, $site);

        $this->crm->commercialFeed($categoryName, $brand, $representation, $status, $fio, $phone, $email,
            $info, $socialBrand, $site);

        return null;
    }
}