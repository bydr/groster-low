<?php

namespace App\Handler\Front;

use App\ApiError;
use App\Controller\ApiController;
use App\Entity\Category;
use App\Entity\Order;
use App\Entity\Payer;
use App\Entity\PaymentMethod;
use App\Entity\ReplacementMethod;
use App\Entity\ShippingAddress;
use App\Entity\ShippingLimit;
use App\Entity\ShippingMethod;
use App\Entity\Store;
use App\EventListener\EmailListener;
use App\Handler\Handler;
use App\Helpers\OrderHelper;
use App\Helpers\Params;
use App\Helpers\ValidateHelper;
use App\Interfaces\Front\CheckoutInterface;
use App\Repository\CategoryRepository;
use App\Repository\OrderRepository;
use App\RequestJson;
use App\Service\Cache;
use App\Service\RetailCRM;
use App\Service\TelegramBot;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use RetailCrm\Api\Interfaces\ApiExceptionInterface;
use RetailCrm\Api\Interfaces\ClientExceptionInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\KernelInterface;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;

/**
 * Оформление заказа
 */
class CheckoutHandler extends Handler implements CheckoutInterface
{
    private $crm;
    private $tgbot;

    public function __construct(EntityManagerInterface $em,
                                KernelInterface $kernel,
                                ContainerInterface $container,
                                EmailListener $emailListener,
                                Cache $cache,
                                RetailCRM $crm,
                                TelegramBot $telegramBot)
    {
        parent::__construct($em, $kernel, $container, $emailListener, $cache);
        $this->crm = $crm;
        $this->tgbot = $telegramBot;
    }

    public function lastOrder(): ?Order
    {
        return $this->em->getRepository(Order::class)->findLastUserOrder(ApiController::getCurrentUser());
    }

    public function customerData(): array
    {
        $user = ApiController::getCurrentUser();
        /** @var OrderRepository $orderRepo */
        $orderRepo = $this->em->getRepository(Order::class);

        /** @var CategoryRepository $categoryRepo */
        $categoryRepo = $this->em->getRepository(Category::class);

        return [
            Params::AREAS => OrderHelper::getUserAreaList($this->cache, $categoryRepo, $orderRepo, $user),
            Params::EMAILS => OrderHelper::getUserEmailList($this->cache, $orderRepo, $user),
            Params::PHONES => OrderHelper::getUserPhoneList($this->cache, $orderRepo, $user),
        ];
    }

    public function shippingMethods(RequestJson $json): array
    {
        $regions = $json->getArray(Params::REGIONS);
        if(empty($regions)) {
            return [];
        }

        $allMethods = $this->em->getRepository(ShippingMethod::class)->findAvailableMethods();

        $methods = [];
        /** @var ShippingMethod $method */
        foreach ($allMethods as $method) {
            // Если этот город входит в исключения метода TK, тогда пропускаем
            if($method->getAlias() == ShippingMethod::METHOD_COMPANY &&
                array_intersect($regions, explode(",", $this->ci->getParameter('exclude_regions')))) {
                continue;
            }
            // Если у метода нет ограничений, то показываем
            if(count($method->getLimits()) == 0) {
                $methods[$method->getId()] = $method;
                continue;
            }

            /** @var ShippingLimit $limit */
            foreach ($method->getLimits() as $limit) {
                if(!in_array($limit->getRegion(), $regions)) {
                    continue;
                }

                $methods[$method->getId()] = $method;
            }
        }

        return array_values($methods);
    }

    public function getReplacements(): array
    {
        return $this->em->getRepository(ReplacementMethod::class)->findAll();
    }

    public function shippingCost(RequestJson $json, ?string &$error): array
    {
        /** @var Order $order */
        $order = $this->em->getRepository(Order::class)->findUserCart(ApiController::getCurrentUser(), $json->getString(Params::CART), Order::STATE_CART);
        if(empty($order)) {
            $error = ApiError::ERROR_INVALID_CART;
            return [];
        }

        /** @var ShippingMethod $method */
        $method = $this->em->getRepository(ShippingMethod::class)->findOneByAlias($json->getString(Params::SHIPPING_METHOD));
        if(empty($method)) {
            $error = ApiError::ERROR_INVALID_DATA;
            return [];
        }

        $paymentMethods = OrderHelper::getPaymentMethods($method, $this->em->getRepository(PaymentMethod::class)->findAvailableMethods($method->getAlias()));

        $region = trim($json->getString(Params::REGION));
        if(ShippingMethod::METHOD_COURIER == $method->getAlias()) {
            $cost = $this->isFreeShipping($method, $json->getString(Params::SHIPPING_DATE));

            if(!is_null($cost)) {

                return [$cost, $paymentMethods];
            }
        }

        $cost = OrderHelper::shippingCost($order->getTotal(), $method, $region);

        return [$cost, $paymentMethods];
    }

    /**
     * @param RequestJson $json
     * @param string $uid
     * @param string|null $error
     * @return int
     * @throws \Doctrine\DBAL\Driver\Exception
     * @throws \Doctrine\DBAL\Exception
     * @throws ApiExceptionInterface
     * @throws ClientExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function orderSave(RequestJson $json, string $uid, ?string &$error): int
    {
        /** @var Order $order */
        $order = $this->em->getRepository(Order::class)->findUserCart(ApiController::getCurrentUser(), $uid, Order::STATE_CART);

        if(empty($order)) {
            $error = ApiError::ERROR_NOT_FOUND_ORDER;
            return 0;
        }

        if($payerUid = $json->getString(Params::PAYER)) {
            /** @var Payer $payer */
            $payer = $this->em->getRepository(Payer::class)->findOneByUid($payerUid);

            if(empty($payer) || $payer->getCustomer() != ApiController::getCurrentUser()) {
                $error = ApiError::ERROR_INVALID_PAYER;
                return 0;
            }

            $order->setPayer($payer);
        }

        if(!$region = $json->getString(Params::REGION)) {
            $error = ApiError::ERROR_REGION;
            return 0;
        }

        /** @var ShippingMethod $shippingMethod */
        $shippingMethod = $this->em->getRepository(ShippingMethod::class)->findOneByAlias($json->getString(Params::SHIPPING_METHOD));

        if(empty($shippingMethod)) {
            $error = ApiError::ERROR_INVALID_SHIPPING_METHOD;
            return 0;
        }

        $order->setShippingMethod($shippingMethod);

        if($addressUid = $json->getString(Params::SHIPPING_ADDRESS)) {
            if($shippingMethod->getAlias() == ShippingMethod::METHOD_PICKUP) {
                /** @var Store $shippingAddress */
                $shippingAddress = $this->em->getRepository(Store::class)->findOneByUuid($addressUid);
            } else {
                /** @var ShippingAddress $shippingAddress */
                $shippingAddress = $this->em->getRepository(ShippingAddress::class)
                    ->findOneBy(['uid' => $addressUid, 'customer' => ApiController::getCurrentUser()]);
            }

            if(empty($shippingAddress)) {
                $error = ApiError::ERROR_INVALID_SHIPPING_ADDRESS;
                return 0;
            }

            $order->setShippingAddress($shippingAddress->getAddress());
        }

        $shippingDate = $json->getString(Params::SHIPPING_DATE);
        if($shippingDate) {
            $order->setShippingDate(new DateTime($shippingDate));
        }

        $shippingCost = $this->isFreeShipping($shippingMethod, $shippingDate);
        if(is_null($shippingCost)) {
            $shippingCost = OrderHelper::shippingCost($order->getTotal(), $shippingMethod, $region);
        }

        $order->setShippingCost($shippingCost);

        if($paymentMethodUid = $json->getString(Params::PAYMENT_METHOD)) {
            $paymentMethod = $this->em->getRepository(PaymentMethod::class)->findOneByUid($paymentMethodUid);

            if($paymentMethod) {
                $order->setPaymentMethod($paymentMethod);
            }
        }


        if($replacementId = $json->getInt(Params::REPLACEMENT)) {
            $replacement = $this->em->getRepository(ReplacementMethod::class)->find($replacementId);

            if(!$replacement) {
                $error = ApiError::ERROR_INVALID_REPLACEMENT;
                return 0;
            }

            $order->setReplacement($replacement);
        }


        $email = $json->getString(Params::EMAIL);

        if(!ValidateHelper::validateEmail($email)) {
            $error = ApiError::ERROR_INVALID_EMAIL;
            return 0;
        }

        if (!$phone = ValidateHelper::getValidPhone($json->getString(Params::PHONE))) {
            $error = ApiError::ERROR_INVALID_PHONE;
            return 0;
        }

        $order->setFio($json->getString(Params::FIO));
        $order->setEmail($email);
        $order->setPhone($phone);

        if($bussinessArea = $json->getString(Params::BUSSINESS_AREA)){
            $order->setBussinessArea($bussinessArea);
        }

        if($comment = $json->getString(Params::COMMENT)) {
            $order->setComment($comment);
        }

        $order->setState(Order::STATE_NEW);
        $order->setCheckoutCompleteAt(new DateTime());

        $this->em->flush();

        $order->setCrmId($this->crm->saveOrder($order, $json->getBool(Params::NOT_CALL)));
        $this->em->flush();

        $user = ApiController::getCurrentUser();
        /** @var OrderRepository $orderRepo */
        $orderRepo = $this->em->getRepository(Order::class);

        /** @var CategoryRepository $categoryRepo */
        $categoryRepo = $this->em->getRepository(Category::class);

        OrderHelper::updateAreaList($this->cache, $categoryRepo, $orderRepo, $user);
        OrderHelper::updateEmailList($this->cache, $orderRepo, $user);
        OrderHelper::updatePhoneList($this->cache, $orderRepo, $user);

        try {
            OrderHelper::sendTo1C($this->ci, $this->em, $order);
        } catch (Exception $e) {
            $this->tgbot->sendMessage(sprintf("Ошибка отправки заказа в 1С в файле %s(%d): %s", $e->getFile(), $e->getLine(), $e->getMessage()));
        }

        try {
            $this->mailer->newOrderManagerEmail($order);
            $this->mailer->newOrderClientEmail($order);
        }catch (Exception $e) {
            ApiController::getLogger()->errorWithExc($e);
        }



        return $order->getId();

    }

    /**
     * Проверка возможности бесплатной доставки, если это второй заказ, который будет доставлен вместе с первым
     * @param ShippingMethod $method
     * @param string $date
     * @return int|null
     * @throws Exception
     */
    private function isFreeShipping(ShippingMethod $method, string $date): ?int
    {
        if(ShippingMethod::METHOD_COURIER != $method->getAlias()) {
            return null;
        }

        $shippingDate = new DateTime($date);
        $openOrders = $this->em->getRepository(Order::class)
            ->findBy(['state' => Order::STATE_NEW,
                'customer' => ApiController::getCurrentUser(),
                'shippingDate' => $shippingDate->setTime(0,0,0),
                'shippingMethod' => $method]);
        ApiController::getLogger()->info(count($openOrders));
        if (count($openOrders) > 0) {
            return 0;
        }

        return null;
    }

    public function paymentMethods(): array
    {
        return $this->em->getRepository(PaymentMethod::class)->findAvailableMethods();
    }
}