<?php

namespace App\Handler\Front;

use App\ApiError;
use App\Controller\ApiController;
use App\Entity\Discount;
use App\Entity\Order;
use App\EventListener\EmailListener;
use App\Handler\Handler;
use App\Helpers\OrderHelper;
use App\Helpers\Params;
use App\Helpers\PromocodeHelper;
use App\Interfaces\Front\PromocodeInterface;
use App\RequestJson;
use App\Service\Cache;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\KernelInterface;

/**
 * Оформление заказа
 */
class PromocodeHandler extends Handler implements PromocodeInterface
{
    /** @var PromocodeHelper */
    private $helper;

    public function __construct(EntityManagerInterface $em,
                                KernelInterface $kernel,
                                ContainerInterface $container,
                                EmailListener $emailListener,
                                Cache $cache,
                                PromocodeHelper $helper)
    {
        parent::__construct($em, $kernel, $container, $emailListener, $cache);

        $this->helper = $helper;
    }

    public function use(RequestJson $jsonReq, &$errorCode = null): ?Order
    {
        $code = $jsonReq->getString(Params::CODE);
        $uid = $jsonReq->getString(Params::CART);
        $user = ApiController::getCurrentUser();

        if (empty($code) || empty($uid)) {
            $errorCode = ApiError::ERROR_INVALID_JSON;
            return null;
        }

        /** @var Discount $promo */
        $promo = $this->em->getRepository(Discount::class)->findOneByCode($code);
        if(empty($promo) || !$this->helper->canUse($promo, $user)) {
            $errorCode = ApiError::ERROR_INVALID_PROMOCODE;
            return null;
        }

        /** @var Order $cart */
        $cart = $this->em->getRepository(Order::class)
            ->findUserCart($user, $uid, Order::STATE_CART);
        if(empty($cart)) {
            $errorCode = ApiError::ERROR_INVALID_CART;
            return null;
        }

        // Нельзя использовать 2 промокода вместе
        if($cart->getDiscount()) {
            $errorCode = ApiError::ERROR_DUPLICATE_PROMOCODE;
            return null;
        }

        $cart->setDiscount($promo);
        OrderHelper::updateDiscount($cart);
        $this->em->flush();

        return $cart;
    }

    public function delete(RequestJson $jsonReq, string &$errorCode = null): ?Order
    {
        $uid = $jsonReq->getString(Params::CART);
        if (empty($uid)) {
            $errorCode = ApiError::ERROR_INVALID_JSON;
            return null;
        }

        /** @var Order $cart */
        $cart = $this->em->getRepository(Order::class)
            ->findUserCart(ApiController::getCurrentUser(), $uid, Order::STATE_CART);
        if(empty($cart)) {
            $errorCode = ApiError::ERROR_INVALID_CART;
            return null;
        }

        $cart->setDiscount(null);
        $cart->setDiscountValue(null);
        OrderHelper::updateDiscount($cart);
        $this->em->flush();

        return $cart;
    }
}