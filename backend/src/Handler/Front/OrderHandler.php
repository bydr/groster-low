<?php

namespace App\Handler\Front;

use App\ApiError;
use App\Controller\ApiController;
use App\Entity\Order;
use App\Entity\OrderItem;
use App\Entity\Product;
use App\Entity\ShippingMethod;
use App\EventListener\EmailListener;
use App\Handler\Handler;
use App\Helpers\OrderHelper;
use App\Helpers\Params;
use App\Helpers\ValidateHelper;
use App\Interfaces\Front\OrderInterface;
use App\Repository\OrderRepository;
use App\Repository\ProductRepository;
use App\RequestJson;
use App\Service\Cache;
use App\Service\RetailCRM;
use App\Service\Sms\Sms;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\KernelInterface;
use function PHPUnit\Framework\isNull;

/**
 * Class OrderHandler
 */
class OrderHandler extends Handler implements OrderInterface
{
    /** @var Sms */
    private $sms;

    /** @var RetailCRM  */
    private $crm;

    public function __construct(EntityManagerInterface $em,
                                KernelInterface $kernel,
                                ContainerInterface $container,
                                EmailListener $emailListener,
                                Cache $cache,
                                Sms $sms,
                                RetailCRM $crm)
    {
        parent::__construct($em, $kernel, $container, $emailListener, $cache);
        $this->sms = $sms;
        $this->crm = $crm;
    }

    public function getCart(string $uid): ?Order
    {
        try {
            /** @var OrderRepository $orderRepo */
            $orderRepo = $this->em->getRepository(Order::class);
            return $orderRepo->findUserCart(ApiController::getCurrentUser(), $uid, Order::STATE_CART);
        }catch (Exception $exception) {
            throw new Exception($exception);
        }

    }

    public function changeProductQty(RequestJson $json, ?string &$errorCode): array
    {
        $qty = $json->getInt(Params::QUANTITY);

        if($qty < 1) {
            $errorCode = ApiError::ERROR_INVALID_DATA;
            return [];
        }

        $product = $this->getProduct($json->getString(Params::PRODUCT));
        if(is_null($product)) {
            $errorCode = ApiError::ERROR_INVALID_PRODUCT;
            return [];
        }

        if(0 == $product->getTotalQty()) {
            $errorCode = ApiError::ERROR_EMPTY_PRODUCT;
            return [];
        }

        $cartUid = $json->getString(Params::CART);
        if(empty($cartUid)) {
            $cart = new Order(ApiController::getCurrentUser());
            $this->em->persist($cart);
        }else{
            $cart = $this->getUserCart($cartUid);
        }

        if(is_null($cart)) {
            $errorCode = ApiError::ERROR_CART;
            return [];
        }

        $parent = $this->getProduct($json->getString(Params::PARENT));

        // Если это комплект, то добавляем все составляющие комплекта
        if($product->isKit()) {
            $addedQty = $qty;
            foreach ($product->getChildren() as $child) {
                $addedQty = min($addedQty, $this->changeItemQuantity($cart, $child, $addedQty, $product));
            }
            $this->changeItemQuantity($cart, $product, $addedQty, null);
        }else{
            $addedQty = $this->changeItemQuantity($cart, $product, $qty, $parent);
        }

        OrderHelper::updateDiscount($cart);

        $this->em->flush();

        return [
            Params::CART => $cart->getUid(),
            Params::ADDED_QTY => $addedQty,
            Params::DISCOUNT => $cart->getItemsDiscount(),
            Params::TOTAL_COST => $cart->getItemsTotal()
        ];
    }

    /**
     * @param string $uid
     * @return bool
     * @throws Exception
     */
    public function bindUser(string $uid): bool
    {
        try {
            $user = ApiController::getCurrentUser();
            if(is_null($user)) {
                return false;
            }

            /** @var OrderRepository $orderRepo */
            $orderRepo = $this->em->getRepository(Order::class);
            /** @var Order $cart */
            $cart = $orderRepo->findUserCart(null, $uid, Order::STATE_CART);
            if(is_null($cart) || !$cart->isEditable()) {
                return false;
            }

            $cart->setCustomer($user);
            $this->em->flush();

            return true;
        }catch (Exception $exception) {
            throw new Exception($exception);
        }
    }

    public function deleteProduct(RequestJson $json, ?string &$errorCode): ?Order
    {
        $productUuid = $json->getString(Params::PRODUCT);
        if(empty($productUuid)) {
            $errorCode = ApiError::ERROR_INVALID_JSON;
            return null;
        }

        $product = $this->em->getRepository(Product::class)->findOneByUuid($productUuid);
        if(empty($product)) {
            $errorCode = ApiError::ERROR_INVALID_JSON;
            return null;
        }

        $cart = $this->getUserCart($json->getString(Params::CART));
        if(is_null($cart)) {
            $errorCode = ApiError::ERROR_INVALID_JSON;
            return null;
        }

        $parent = $json->getString(Params::PARENT);

        /** @var OrderItem $item */
        foreach ($cart->getItems() as $item) {
            $itemParent = $item->getParent() ? $item->getParentUuid() : null;

            // Если это составляющая комплекта
            if(!empty($parent)) {
                // если не совпадает товар или родитель
                if($item->getProduct() != $product && $item->getParentUuid() != $parent) {
                    continue;
                }
            }else {
                // если не совпадает товар
                if ($item->getProduct() != $product) {
                    // и товар не является родителем
                    if ($item->getParent() != $product) {
                        continue;
                    }
                } else {
                    // если родитель не пуст
                    if (!is_null($item->getParentUuid())) {
                        continue;
                    }
                }
            }

            if($item->getSample() > 0) {
                $item->setQuantity(0);
            }else{
                $cart->removeItem($item);
            }
        }

        $this->em->flush();

        return $this->getUserCart($cart->getUid());
    }


    public function clear(string $uid): bool
    {
        /** @var OrderRepository $orderRepo */
        $orderRepo = $this->em->getRepository(Order::class);
        $cart = $orderRepo->findUserCart(ApiController::getCurrentUser(), $uid, Order::STATE_CART);
        if(is_null($cart)) {
            return false;
        }

        foreach ($cart->getItems() as $item) {
            $cart->removeItem($item);
        }

        $this->em->flush();

        return true;
    }

    public function remove(string $uid): bool
    {
        /** @var OrderRepository $orderRepo */
        $orderRepo = $this->em->getRepository(Order::class);
        $cart = $orderRepo->findUserCart(ApiController::getCurrentUser(), $uid, Order::STATE_CART);
        if(is_null($cart)) {
            return false;
        }

        $this->em->remove($cart);
        $this->em->flush();
        return true;
    }

    public function sample(RequestJson $json): array
    {
        $product = $this->getProduct($json->getString(Params::PRODUCT));
        if(is_null($product)) {
            return [];
        }

        $cartUid = $json->getString(Params::CART);
        if(empty($cartUid)) {
            $cart = new Order(ApiController::getCurrentUser());
            $this->em->persist($cart);
        }else{
            $cart = $this->getUserCart($cartUid);
        }

        if(is_null($cart)) {
            return [];
        }

        $sampleQty = max($json->getInt(Params::QUANTITY), 0);

        if($sampleQty > OrderItem::MAX_SAMPLE_QTY) {
            $sampleQty = OrderItem::MAX_SAMPLE_QTY;
        }

        $orderItem = null;
        foreach ($cart->getItems() as $item) {
            if($item->getProduct() === $product && is_null($item->getParent())) {
                $orderItem = $item;
                break;
            }
        }

        if(!$orderItem) {
            // Если удаляем образец
            if(0 == $sampleQty) {
                return [
                    Params::CART => $cart->getUid(),
                    Params::QUANTITY => $sampleQty,
                    Params::DISCOUNT => $cart->getItemsDiscount(),
                    Params::TOTAL_COST => $cart->getItemsTotal()
                ];
            }
            $orderItem = new OrderItem();
            $orderItem->setOrder($cart);
            $orderItem->setProduct($product);
            $orderItem->setQuantity(0);
            $this->em->persist($orderItem);
            $cart->addItem($orderItem);
        }

        // Если удаляем образец
        if (0 == $sampleQty && 0 == $orderItem->getQuantity()) {
            $this->em->remove($orderItem);

            $this->em->flush();

            return [
                Params::CART => $cart->getUid(),
                Params::QUANTITY => $sampleQty,
                Params::DISCOUNT => $cart->getItemsDiscount(),
                Params::TOTAL_COST => $cart->getItemsTotal()
            ];
        }

        $orderItem->setSample($sampleQty);
        $orderItem->setPrice($product->getPrice());

        $this->em->flush();

        return [
            Params::CART => $cart->getUid(),
            Params::QUANTITY => $sampleQty,
            Params::DISCOUNT => $cart->getItemsDiscount(),
            Params::TOTAL_COST => $cart->getItemsTotal()
        ];
    }

    /**
     * Изменение количества единиц товара в корзине
     * @param Order $cart
     * @param Product $product
     * @param int $qty
     * @param Product|null $parent - комплект, если product является составляющей комплекта
     * @return int`
     */
    private function changeItemQuantity(Order $cart, Product $product, int $qty, ?Product $parent): int
    {

        $productQty = $product->getTotalQty();    // Доступное количество товара
        $newQty = $qty;
        $orderItem = null;

        /** @var OrderItem $item */
        foreach ($cart->getItems() as $item) {
            if($item->getProduct() === $product) {
                // Пропускаем, если не совпадает parent
                if($item->getParent() !== $parent) {
                    $productQty -= $item->getQuantity();
                    continue;
                }
                $orderItem = $item;
            }
        }

        if(!$orderItem) {
            $orderItem = new OrderItem();
            $orderItem->setProduct($product);
            $orderItem->setParent($parent);
            $orderItem->setOrder($cart);
            $cart->addItem($orderItem);

            $this->em->persist($orderItem);
        }

        if($productQty < $qty) {
            $newQty = $productQty;
        }

        $orderItem->setPrice($product->getPrice());
        $orderItem->setQuantity($newQty);

        return $newQty;
    }

    /**
     * Возвращает корзину текущего пользователя
     *
     * @param string|null $cartUid
     * @return int|mixed|string|null
     */
    private function getUserCart(?string $cartUid): ?Order
    {
        try {
            if(empty($cartUid)) {
                return null;
            }

            /** @var OrderRepository $orderRepo */
            $orderRepo = $this->em->getRepository(Order::class);
            return $orderRepo->findUserCart(ApiController::getCurrentUser(), $cartUid, Order::STATE_CART);
        }catch (Exception $exception) {
            ApiController::getLogger()->errorWithExc($exception);
            return null;
        }
    }

    private function getProduct(string $productUuid): ?Product
    {
        if(empty($productUuid)) {
            return null;
        }

        /** @var ProductRepository $productRepo */
        $productRepo = $this->em->getRepository(Product::class);
        /** @var Product $product */
        return $productRepo->findOneByUuid($productUuid);
    }

    public function freeShipping(string $region): int
    {
        /** @var ShippingMethod $method */
        $method = $this->em->getRepository(ShippingMethod::class)->findOneByAlias(ShippingMethod::METHOD_COURIER);

        if(is_null($method)) {
            return 0;
        }

        $orderMinCost = 0;
        foreach ($method->getLimits() as $limit) {
            if($limit->getRegion() != $region) {
                continue;
            }

            if($limit->getShippingCost() > 0) {
                continue;
            }

            $orderMinCost = $limit->getOrderMinCost();
        }

        return $orderMinCost;
    }

    public function fastOrder(RequestJson $json, string $uid, ?string &$error): int
    {
        $cart = $this->getUserCart($uid);
        if (empty($cart)) {
            $error = ApiError::ERROR_INVALID_CART;
            return 0;
        }

        $key = $json->getString(Params::CONTACT);
        $contactType = $this->contactType($key);
        switch ($contactType){
            case Params::EMAIL:
                $cart->setEmail($key);
                break;
            case Params::PHONE:
                $cart->setPhone(ValidateHelper::getValidPhone($key));
                break;
            default:
                $error = ApiError::ERROR_INVALID_DATA;
                return 0;
        }

        $cart->setIsFast(true);
        $cart->setState(Order::STATE_NEW);
        $cart->setCheckoutCompleteAt(new DateTime());

        $this->em->flush();

        $cart->setCrmId($this->crm->saveOrder($cart, false));
        $this->em->flush();

        OrderHelper::sendTo1C($this->ci, $this->em, $cart);

        try {
            $this->mailer->fastOrderManagerEmail($cart);
            switch ($contactType) {
                case Params::EMAIL:
                    $this->mailer->fastOrderClientEmail($cart);
                    break;
                case Params::PHONE:
                    $msg = sprintf("Ваш заказ #%d принят в обработку", $cart->getId());
                    $this->sms->send($cart->getPhone(), $msg);
                    break;
            }
        }catch (Exception $e) {
            ApiController::getLogger()->errorWithExc($e);
        }

        return $cart->getId();
    }

    /**
     * Возвращает тип контакта
     * @param string $key
     * @return string|null
     */
    private function contactType(string $key): ?string
    {
        if(ValidateHelper::getValidPhone($key)){
            return Params::PHONE;
        }

        if(ValidateHelper::validateEmail($key)){
            return Params::EMAIL;
        }

        return null;
    }

    public function changeStatus(RequestJson $json, string $uid): ?string
    {
        if (empty($uid)) {
            return ApiError::ERROR_INVALID_DATA;
        }

        /** @var Order $order */
        $order = $this->em->getRepository(Order::class)->findOnePrepared($uid);
        if(!$order) {
            return ApiError::ERROR_NOT_FOUND;
        }

        $state = $json->getInt(Params::STATE);
        if(!in_array($state, [Order::STATE_FINISHED, Order::STATE_CANCELED, Order::STATE_PROCESSING, Order::STATE_COURIER])) {
            return ApiError::ERROR_INVALID_ORDER_STATE;
        }

        if(Order::STATE_FINISHED == $order->getState() && Order::STATE_CANCELED == $state) {
            return ApiError::ERROR_INVALID_ORDER_STATE;
        }

        $order->setState($state);
        $order->setUpdateAt(new DateTime());
        $this->em->flush();

        return null;
    }

    /**
     * @param RequestJson $json
     * @param string $uid
     * @return array|string
     */
    public function changeComposition(RequestJson $json, string $uid)
    {
        /** @var Order $order */
        $order = $this->em->getRepository(Order::class)->findOneBy(['uid' => $uid, 'state' => Order::STATE_PROCESSING]);
        if (!$order) {
            return ApiError::ERROR_NOT_FOUND;
        }

        $orderNewItems = [];
        $errors = [];
        /** @var array{IDProduct: string, IDKitProduct: string,
         *      CountProduct: int, PriceProduct: int,
         *      SumDiscount: int, ThisProductSample: bool} $item */
        foreach ($json->getArray('Products') as $item) {
            /** @var Product $product */
            $product = $this->em->getRepository(Product::class)->findOneByUuid($item['IDProduct']);
            if (!$product) {
                $errors[] = sprintf('Не найден товар с IDProduct "%s"', $item['IDProduct']);
                continue;
            }

            $orderItem = $this->getOrderItem($item, $product, $order);
            if(is_string($orderItem)) {
                $errors[] = $orderItem;
                continue;
            }

            $orderNewItems[] = $orderItem;

            $orderItem->setPrice($item['PriceProduct']);
            $orderItem->setDiscount($item['SumDiscount']);

            if($item['ThisProductSample']) {
                $orderItem->setSample($item['CountProduct']);
                $orderItem->setQuantity(0);
            } else {
                $orderItem->setQuantity($item['CountProduct']);
                $orderItem->setSample(0);
            }
        }

        /** @var OrderItem $orderItem */
        foreach ($order->getItems() as $orderItem) {
            if(!in_array($orderItem, $orderNewItems)) {
                $order->removeItem($orderItem);
            }
        }

        $order->setShippingCost($json->getInt('ShippingPrice'));

        $this->em->flush();

        return $errors;
    }

    /**
     * Возвращает элемент заказа или сообщение ошибки
     * @param array{IDProduct: string, IDKitProduct: string,
     *      CountProduct: int, PriceProduct: int,
     *      SumDiscount: int, ThisProductSample: bool} $item
     * @param Product $product
     * @param Order $order
     * @return string|OrderItem
     */
    private function getOrderItem(array $item, Product $product, Order $order)
    {
        $parent = null;
        if($item['IDKitProduct']) {
            $parent = $this->em->getRepository(Product::class)->findOneByUuid($item['IDKitProduct']);

            if(!$parent) {
                return sprintf('Не найден товар с IDKitProduct "%s"', $item['IDKitProduct']);
            }
        }

        /** @var OrderItem $orderItem */
        $orderItem = $this->em->getRepository(OrderItem::class)
            ->findOneBy(['order' => $order, 'product' => $product, 'parent' => $parent]);

        // Если это образец, тогда у orderItem должны быть образцы
        if($orderItem) {
            if ($item['ThisProductSample']) {
                $orderItem = $orderItem->getSample() > 0 ? $orderItem : null;
            } else {
                $orderItem = $orderItem->getSample() == 0 ? $orderItem : null;
            }
        }

        if(!$orderItem) {
            $orderItem = new OrderItem();
            $orderItem->setProduct($product);
            $orderItem->setOrder($order);
            $orderItem->setParent($parent);
            $this->em->persist($orderItem);
        }

        return $orderItem;
    }


}