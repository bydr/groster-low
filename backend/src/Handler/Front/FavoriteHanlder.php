<?php

namespace App\Handler\Front;

use App\ApiError;
use App\Controller\ApiController;
use App\Entity\Category;
use App\Entity\Favorite;
use App\Entity\Param;
use App\Entity\Product;
use App\Entity\Tag;
use App\Handler\Handler;
use App\Helpers\CategoryHelper;
use App\Helpers\Params;
use App\Interfaces\Front\CatalogInterface;
use App\Interfaces\Front\FavoriteInterface;
use App\RequestJson;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Избранное
 */
class FavoriteHanlder extends Handler implements FavoriteInterface
{
    /**
     * Список категорий
     * @return array
     */
    public function getAllProducts(): array
    {
        $favorites = $this->em->getRepository(Favorite::class)->findBy(['shopUser' => ApiController::getCurrentUser()]);

        $products = [];
        /** @var Favorite $favorite */
        foreach ($favorites as $favorite) {
            $products[] = $favorite->getProduct()->getUuid();
        }

        return $products;
    }

    public function addProducts(RequestJson $json): ?string
    {
        $products = $json->getArray(Params::PRODUCTS);
        if(empty($products)) {
            return ApiError::ERROR_INVALID_DATA;
        }

        foreach ($products as $uuid) {
            $product = $this->em->getRepository(Product::class)->findOneByUuid($uuid);
            if(is_null($product)) {
                return ApiError::ERROR_INVALID_PRODUCT;
            }

            if($this->em->getRepository(Favorite::class)->findOneBy(['shopUser' => ApiController::getCurrentUser(), 'product' => $product])){
                continue;
            }

            $favorite = new Favorite();
            $favorite->setShopUser(ApiController::getCurrentUser());
            $favorite->setProduct($product);
            $this->em->persist($favorite);
        }

        $this->em->flush();

        return null;
    }

    public function deleteProduct(RequestJson $json): ?string
    {
        $productUuid = $json->getString(Params::PRODUCT);
        if(empty($productUuid)) {
            return ApiError::ERROR_INVALID_DATA;
        }

        $product = $this->em->getRepository(Product::class)->findOneByUuid($productUuid);
        if(is_null($product)) {
            return ApiError::ERROR_INVALID_DATA;
        }

        $favorites = $this->em->getRepository(Favorite::class)->findOneBy(['shopUser' => ApiController::getCurrentUser(), 'product' => $product]);
        if(empty($favorites)){
            return null;
        }

        $this->em->remove($favorites);
        $this->em->flush();

        return null;
    }
}