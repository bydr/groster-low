<?php

namespace App\Handler\Front;

use App\ApiError;
use App\Controller\ApiController;
use App\Entity\Product;
use App\Entity\Rest;
use App\Entity\Settings;
use App\Entity\WaitingProduct;
use App\EventListener\EmailListener;
use App\Handler\Handler;
use App\Helpers\Params;
use App\Helpers\ValidateHelper;
use App\Interfaces\Front\ProductInterface;
use App\Repository\ProductRepository;
use App\RequestJson;
use App\Service\Cache;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\KernelInterface;

/**
 * Class ProductHandler
 */
class ProductHandler extends Handler implements ProductInterface
{
    /** @var ProductRepository  */
    private $productRepository;

    public function __construct(EntityManagerInterface $em, KernelInterface $kernel, ContainerInterface $container, EmailListener $emailListener, Cache $cache)
    {
        parent::__construct($em, $kernel, $container, $emailListener, $cache);
        $this->productRepository = $em->getRepository(Product::class);
    }

    public function getByUuids(RequestJson $json): array
    {
        $uuids = $json->getArray(Params::UUID);
        if(empty($uuids)) {
            return [];
        }

        return $this->productRepository->findByUuidOrAlias($uuids);
    }

    public function getProduct(string $uuid): ?Product
    {
        $product = $this->productRepository->findOneByUuidOrAlias($uuid);
        if(empty($product)) {
            return null;
        }

        /** @var Settings $settings */
        $settings = $this->em->getRepository(Settings::class)->find(Settings::DEFAULT_ID);
        if(empty($settings)) {
            return null;
        }

        return $product;
    }

    public function waiting(string $uuid, RequestJson $json): ?string
    {
        /** @var Product $product */
        $product = $this->em->getRepository(Product::class)->findOneByUuidOrAlias($uuid);
        if(!$product || $product->getTotalQty() > 0) {
            return ApiError::ERROR_NOT_FOUND;
        }

        $email = $json->getString(Params::EMAIL);
        if(!ValidateHelper::validateEmail($email)) {
            return ApiError::ERROR_INVALID_EMAIL;
        }

        if($this->em->getRepository(WaitingProduct::class)
            ->findOneBy(['product' => $product, 'contact' => $email, 'notificatedAt' => null, 'customer' => ApiController::getCurrentUser()])
        ) {
            return null;
        }

        $waiting = new WaitingProduct();
        $waiting->setProduct($product);
        $waiting->setContact($email);
        $waiting->setCustomer(ApiController::getCurrentUser());
        $this->em->persist($waiting);
        $this->em->flush();

        return null;
    }

    public function updateRest(RequestJson $json): ?string
    {
        $items = $json->getArray(Params::PRODUCTS);
        if(empty($items)) {
            return null;
        }

        $errors = [];
        $ids = [];
        foreach ($items as $item) {
            if(empty($item[Params::ID])) {
                $errors[] = 'не указан идентификатор для товара';
                continue;
            }

            $ids[$item[Params::ID]] = $item;
        }

        $products = $this->em->getRepository(Product::class)->findByUuid(array_keys($ids));

        /** @var Product $product */
        foreach ($products as $product) {
            $item = $ids[$product->getUuid()];
            $product->setIsActive(!$item[Params::IS_DELETE]);

            /** @var Rest $store */
            foreach ($product->getRests() as $store) {
                foreach ($item[Params::RESTS] as $rest) {
                    if ($store->getStore()->getUuid() != $rest[Params::STORE]) {
                        continue;
                    }

                    $value = max(0, (int)$rest[Params::VALUE]);
                    $store->setValue($value);
                    $store->setUpdateAt(new DateTime());
                }
            }

            unset($ids[$item[Params::ID]]);
        }

        if (count($ids)) {
            foreach ($ids as $id => $item) {
                $errors[] = sprintf("не найден товар с идентификатором %s", $id);
            }
        }

        $this->em->flush();

        return implode("; ", $errors);
    }
}