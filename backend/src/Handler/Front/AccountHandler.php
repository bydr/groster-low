<?php

namespace App\Handler\Front;

use App\ApiError;
use App\Controller\ApiController;
use App\Entity\Order;
use App\Entity\OrderItem;
use App\Entity\Payer;
use App\Entity\Product;
use App\Entity\Security\AuthUser;
use App\Entity\ShippingAddress;
use App\Entity\WaitingProduct;
use App\EventListener\EmailListener;
use App\Handler\Handler;
use App\Helpers\OrderHelper;
use App\Helpers\Params;
use App\Helpers\ValidateHelper;
use App\Interfaces\Front\AccountInterface;
use App\Repository\OrderRepository;
use App\RequestJson;
use App\Service\Cache;
use App\Service\RetailCRM;
use App\Service\Server\Server;
use Doctrine\ORM\EntityManagerInterface;
use RetailCrm\Api\ResourceGroup\Api;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\KernelInterface;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

/**
 * Личный кабинет
 */
class AccountHandler extends Handler implements AccountInterface
{
    /** @var RetailCRM  */
    private $crm;

    public function __construct(EntityManagerInterface $em,
                                KernelInterface $kernel,
                                ContainerInterface $container,
                                EmailListener $emailListener,
                                Cache $cache,
                                RetailCRM $crm)
    {
        parent::__construct($em, $kernel, $container, $emailListener, $cache);
        $this->crm = $crm;
    }

    public function allPayers(): array
    {
        return $this->em->getRepository(Payer::class)->findBy(["customer" => ApiController::getCurrentUser()]);
    }

    public function payerUpdate(RequestJson $json, ?string $uid, ?string &$error): ?string
    {
        if(empty($uid)) {
            $payer = new Payer();
            $payer->setCustomer(ApiController::getCurrentUser());

            $this->em->persist($payer);
        }else{
            $payer = $this->em->getRepository(Payer::class)->findOneBy(["uid" => $uid, "customer" => ApiController::getCurrentUser()]);
        }

        if(is_null($payer)) {
            $error = ApiError::ERROR_NOT_FOUND;
            return null;
        }

        $payer->setName($json->getString(Params::NAME));
        $payer->setAccount($json->getString(Params::ACCOUNT));
        $payer->setBik($json->getString(Params::BIK));
        $payer->setInn($json->getString(Params::INN));
        $payer->setOgrnip($json->getString(Params::OGRNIP));
        $payer->setKpp($json->getString(Params::KPP));
        $payer->setContact($json->getString(Params::CONTACT));

        if($phone = $json->getString(Params::CONTACT_PHONE)) {
            if(!$contactPhone = ValidateHelper::getValidPhone($phone)) {
                $error = ApiError::ERROR_INVALID_PHONE;
                return null;
            }

            $payer->setContactPhone($contactPhone);
        }

        $this->em->flush();

        return $payer->getUid();
    }

    public function payerGet(string $uid): ?Payer
    {
        return $this->em->getRepository(Payer::class)->findOneBy(["uid" => $uid, "customer" => ApiController::getCurrentUser()]);
    }

    public function allAddresses(): array
    {
        return $this->em->getRepository(ShippingAddress::class)->findBy(["customer" => ApiController::getCurrentUser()]);
    }

    public function addressUpdate(RequestJson $json, ?string $uid): ?string
    {
        if(empty($uid)) {
            $address = new ShippingAddress();
            $address->setCustomer(ApiController::getCurrentUser());

            $this->em->persist($address);
        }else{
            $address = $this->em->getRepository(ShippingAddress::class)->findOneBy(["uid" => $uid, "customer" => ApiController::getCurrentUser()]);
        }

        if(is_null($address)) {
            return null;
        }

        $address->setName($json->getString(Params::NAME));
        $address->setAddress($json->getString(Params::ADDRESS));

        $this->em->flush();

        if($json->getBool(Params::IS_DEFAULT)) {
            $this->em->getRepository(ShippingAddress::class)->setAsDefault($address->getId());
        }

        return $address->getUid();
    }

    public function addressGet(string $uid): ?ShippingAddress
    {
        return $this->em->getRepository(ShippingAddress::class)->findOneBy(["uid" => $uid, "customer" => ApiController::getCurrentUser()]);
    }

    public function updatePassword(RequestJson $json, UserPasswordHasherInterface $hasher): ?string
    {
        $oldPassword = $json->getString(Params::OLD_PASSWORD);
        $newPassword = $json->getString(Params::NEW_PASSWORD);

        if(empty($newPassword)) {
            return ApiError::ERROR_INVALID_DATA;
        }

        $user = ApiController::getCurrentUser();

        if(!$hasher->isPasswordValid($user, $oldPassword)) {
            return ApiError::INVALID_PASSWORD;
        }

        $hash = $hasher->hashPassword($user, $newPassword);
        $user->setPassword($hash);
        $this->em->flush();

        return null;
    }

    public function orderListOrders(RequestJson $json, ?string &$error): array
    {
        $params = [
            Params::STATE => $json->getInt(Params::STATE),
            Params::PAYER => $json->getArray(Params::PAYER),
            Params::YEAR => $json->getInt(Params::YEAR),
            Params::PAGE => $json->getInt(Params::PAGE, 1),
            Params::PER_PAGE => $json->getInt(Params::PER_PAGE, Params::ORDER_PER_PAGE)
        ];

        if($params[Params::STATE] > 0 && !OrderHelper::isCompletedState($params[Params::STATE])) {
            $error = ApiError::ERROR_INVALID_DATA;
            return [];
        }

        if($params[Params::PER_PAGE] > 100) {
            $params[Params::PER_PAGE] = 100;
        }

        return $this->em->getRepository(Order::class)->userOrders(ApiController::getCurrentUser(), $params);
    }

    public function orderListProducts(RequestJson $json, ?string &$error): array
    {
        $params = [
            Params::STATE => $json->getInt(Params::STATE),
            Params::CATEGORY => $json->getString(Params::CATEGORY),
            Params::PAYER => $json->getArray(Params::PAYER),
            Params::YEAR => $json->getInt(Params::YEAR),
            Params::PAGE => $json->getInt(Params::PAGE, 1),
            Params::PER_PAGE => $json->getInt(Params::PER_PAGE, Params::ORDER_PER_PAGE)
        ];

        if($params[Params::STATE] > 0 && !OrderHelper::isCompletedState($params[Params::STATE])) {
            $error = ApiError::ERROR_INVALID_DATA;
            return [];
        }

        if($params[Params::PER_PAGE] > 100) {
            $params[Params::PER_PAGE] = 100;
        }

        return $this->em->getRepository(OrderItem::class)->userProducts(ApiController::getCurrentUser(), $params);
    }

    public function orderDetail(string $uid): ?Order
    {
        return $this->em->getRepository(Order::class)->findOneBy(["customer" => ApiController::getCurrentUser(), "uid" => $uid]);
    }

    public function addressSetDefault(string $uid): ?string
    {
        $address = $this->em->getRepository(ShippingAddress::class)->findOneByUid($uid);
        if(!$address) {
            return ApiError::ERROR_NOT_FOUND;
        }

        $this->em->getRepository(ShippingAddress::class)->setAsDefault($address->getId());

        return null;
    }

    public function waitingProductDelete(RequestJson $json): ?string
    {
        $product = $this->em->getRepository(Product::class)->findOneByUuid($json->getString(Params::PRODUCT));
        if(empty($product)) {
            return ApiError::ERROR_NOT_FOUND;
        }

        $waitingProducts = $this->em->getRepository(WaitingProduct::class)
            ->findBy(['product' => $product, 'customer' => ApiController::getCurrentUser()]);

        foreach ($waitingProducts as $waiting) {
            $this->em->remove($waiting);
        }

        $this->em->flush();

        return null;
    }

    public function subscribeOrder(RequestJson $json, string $uid): ?string
    {
        /** @var Order $order */
        $order = $this->em->getRepository(Order::class)
            ->findOneBy([
                "uid" => $uid,
                "customer" => ApiController::getCurrentUser(),
            ]);

        if(is_null($order) || $order->getState() < Order::STATE_NEW) {
            return ApiError::ERROR_NOT_FOUND;
        }

        $interval = $json->getInt(Params::INTERVAL);

        if(!in_array($interval, [Order::SUBSCRIBE_DAILY, Order::SUBSCRIBE_WEEKLY, Order::SUBSCRIBE_MONTHLY])) {
            return ApiError::ERROR_INVALID_DATA;
        }

        $order->setSubscribeInterval($interval);
        $this->em->flush();

        return null;
    }

    public function unsubscribeOrder(string $uid): ?string
    {
        /** @var Order $order */
        $order = $this->em->getRepository(Order::class)
            ->findOneBy([
                "uid" => $uid,
                "customer" => ApiController::getCurrentUser(),
            ]);

        if(is_null($order) || $order->getState() < Order::STATE_NEW) {
            return ApiError::ERROR_NOT_FOUND;
        }

        $order->setSubscribeInterval(null);
        $this->em->flush();

        return null;
    }

    public function updateFio(RequestJson $json)
    {
        ApiController::getCurrentUser()->setFio($json->getString(Params::FIO));
        $this->em->flush();
    }

    public function orderCancel(string $uid): ?string
    {
        /** @var Order $order */
        $order = $this->em->getRepository(Order::class)->findUserCart(ApiController::getCurrentUser(), $uid, Order::STATE_NEW);

        if(!$order) {
            return ApiError::ERROR_NOT_FOUND;
        }
        $order->setState(Order::STATE_CANCELED);

        if (!OrderHelper::cancelIn1C($this->ci, $this->em, $order)) {
            return ApiError::ERROR_ORDER_NOT_CANCELED;
        }

        $order->setSentAt1c(new \DateTime());
        $order->setUpdateAt(new \DateTime());
        $this->em->flush();

        $this->crm->cancelOrder($order);

        return null;
    }

    public function getInvoicePayment(string $uid, ?string &$err): ?string
    {
        return $this->getDownloadDocument($uid, Server::METHOD_INVOICE_PAYMENT, Params::DOCUMENT_INVOICE, $err);
    }

    public function getAttachedContract(string $uid, ?string &$err): ?string
    {
        return $this->getDownloadDocument($uid, Server::METHOD_ATTACHED_DOCS, Params::DOCUMENT_ATTACHED_CONTRACT, $err);
    }

    public function getGenerateDocuments(string $uid, ?string &$err): ?string
    {
        return $this->getDownloadDocument($uid, Server::METHOD_ORDER_DOCUMENT, Params::DOCUMENT_CONTRACT, $err);
    }

    public function getPackingDocuments(string $uid, ?string &$err): ?string
    {
        return $this->getDownloadDocument($uid, Server::METHOD_PACKING_DOCS, Params::DOCUMENT_TORG12, $err);
    }

    private function getDownloadDocument(string $uid, string $type, string $prefix, ?string &$err): ?string
    {
        /** @var Order $order */
        $order = $this->em->getRepository(Order::class)->findOneBy(['customer' => ApiController::getCurrentUser(), 'uid' => $uid]);
        if (!$order) {
            $err = ApiError::ERROR_NOT_FOUND;
            return null;
        }

        $filename = sprintf("%s по заказу %d (Гростер)", $prefix, $order->getId());

        return OrderHelper::get1CDocs($this->ci, $this->em, $order->getUid(), $type, $filename);
    }
}