<?php

namespace App\Handler\Front;

use App\ApiError;
use App\Entity\Category;
use App\Entity\Param;
use App\Entity\Product;
use App\Entity\Tag;
use App\Handler\Handler;
use App\Helpers\CategoryHelper;
use App\Helpers\Params;
use App\Helpers\ParamsHelper;
use App\Helpers\ProductHelper;
use App\Interfaces\Front\CatalogInterface;
use App\RequestJson;
use App\Service\Server\Server;

/**
 * Обработчик для каталога
 */
class CatalogHandler extends Handler implements CatalogInterface
{
    const PER_PAGE_MAX = 100;

    /**
     * Список категорий
     * @return string
     */
    public function findAllCategories(): string
    {
        return CategoryHelper::getList($this->cache, $this->em);
    }

    /**
     * Список категорий для сферы
     *
     * @param string $uuid
     * @return string
     */
    public function findAllCategoriesByArea(string $uuid): string
    {
        return CategoryHelper::getListForArea($this->cache, $this->em, $uuid);
    }

    public function findHelpPage(int $id, ?string &$errorCode): ?Category
    {
        /** @var Category $category */
        $category = $this->em->getRepository(Category::class)->find($id);

        if(empty($category)) {
            $errorCode = ApiError::ERROR_NOT_FOUND;
            return null;
        }

        return $category;
    }

    public function findAllTags():array
    {
        $tags = $this->em->getRepository(Tag::class)->findBy([], ["name" => "ASC"]);

        return $tags;
    }

    public function findOneTag(int $id, ?string &$errorCode): ?Tag
    {
        $tag = $this->em->getRepository(Tag::class)->find($id);

        if(empty($tag)) {
            $errorCode = ApiError::ERROR_NOT_FOUND;
            return null;
        }

        return $tag;
    }

    public function findAllParams(): string
    {
        return ParamsHelper::getList($this->cache, $this->em);
    }

    public function filterProducts(array $data): array
    {
        $jsonRequest = new RequestJson($data);

        $params = [
            Params::CATEGORIES => $jsonRequest->getArray(Params::CATEGORIES),
            Params::PARAMS => $jsonRequest->getArray(Params::PARAMS),
            Params::PRICE => $jsonRequest->getArray(Params::PRICE, [], '-'),
            Params::IS_ENABLED => (bool)$jsonRequest->getInt(Params::IS_ENABLED),
            Params::IS_FAST => (bool)$jsonRequest->getInt(Params::IS_FAST),
            Params::PAGE => $jsonRequest->getInt(Params::PAGE, 1),
            Params::BESTSELLER => $jsonRequest->getBool(Params::BESTSELLER),
            Params::NEW => $jsonRequest->getBool(Params::NEW),
            Params::STORE => $jsonRequest->getArray(Params::STORE),
            Params::PER_PAGE => $jsonRequest->getInt(Params::PER_PAGE, 20),
            Params::SORT_BY => $jsonRequest->getString(Params::SORT_BY),
            Params::IS_SEARCH => $jsonRequest->getBool(Params::IS_SEARCH)
        ];

        if(!empty($params[Params::CATEGORIES])) {
            $params[Params::CATEGORIES] = CategoryHelper::getChildrenIds($this->em->getRepository(Category::class)->getCategories($params[Params::CATEGORIES]));
        }

        if(!empty($params[Params::PARAMS])) {
            $params[Params::PARAMS] = ParamsHelper::groupParams($params[Params::PARAMS], $this->cache, $this->em);
        }

        if(!in_array($params[Params::SORT_BY], array_keys(Params::getSortTypes()))) {
            $params[Params::SORT_BY] = Params::SORT_BY_NAME;
        }

        /**
         * Если нужно для поиска, то не ограничиваем количество результатов
         * ВАЖНО: в документации этого не указываю, т.к. только для внутреннего использования
         */
        if(!$params[Params::IS_SEARCH]) {
            $params[Params::PER_PAGE] = min($params[Params::PER_PAGE], self::PER_PAGE_MAX);
        }
        $params[Params::PER_PAGE] = max($params[Params::PER_PAGE], 1);

        return $this->em->getRepository(Product::class)->filter($params);
    }

    /**
     * Список доступных типов сотировки
     * @return array
     */
    public function getSortTypes(): array
    {
        $types = [];
        foreach (Params::getSortTypes() as $alias => $name) {
            $types[] = [
                'alias' => $alias,
                'name' => $name
            ];
        }

        return $types;
    }

    public function getHits(): string
    {
        return ProductHelper::getHitList($this->cache, $this->em);
    }

    public function pricelist(RequestJson $json, ?string &$err): string
    {
        $categories = $json->getArray(Params::CATEGORIES);
        if (empty($categories)) {
            $err = ApiError::ERROR_INVALID_DATA;
            return '';
        }

        $email = $json->getString(Params::EMAIL);
        if (!empty($email)) {
            if(!$this->cache->setPricelistQuery($email, $categories)) {
                $err = ApiError::ERROR_SAVE_PRICELIST_QUERY;
            }
            return '';
        }

        $server = new Server($this->ci, $this->em);
        $filepath = $server->getPricelist($categories);

        if(empty($filepath)) {
            $err = ApiError::ERROR_FILE_DO_NOT_READY;
            return '';
        }

        return $filepath;
    }
}