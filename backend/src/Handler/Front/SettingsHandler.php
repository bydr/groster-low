<?php

namespace App\Handler\Front;

use App\Entity\Settings;
use App\Handler\Handler;
use App\Interfaces\Front\SettingsInterface;
use App\RequestJson;

/**
 * Общие настройки
 */
class SettingsHandler extends Handler implements SettingsInterface
{

    public function getSettings(): ?Settings
    {
        return $this->em->getRepository(Settings::class)->find(Settings::DEFAULT_ID);
    }
}