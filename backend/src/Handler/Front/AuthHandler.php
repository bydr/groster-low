<?php

namespace App\Handler\Front;

use App\ApiError;
use App\Entity\Order;
use App\Entity\Security\AuthUser;
use App\EventListener\EmailListener;
use App\Handler\Handler;
use App\Helpers\Params;
use App\Helpers\ValidateHelper;
use App\Interfaces\Front\AuthInterface;
use App\RequestJson;
use App\Service\Cache;
use App\Service\RetailCRM;
use App\Service\Server\Server;
use App\Service\Sms\Sms;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\KernelInterface;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;

/**
 * Class AuthHandler
 */
class AuthHandler extends Handler implements AuthInterface
{
    private $userRepository;
    private $sms;
    private $crm;

    public function __construct(EntityManagerInterface $em,
                                KernelInterface $kernel,
                                ContainerInterface $container,
                                EmailListener $emailListener,
                                Cache $cache,
                                Sms $sms,
                                RetailCRM $crm
    )
    {
        parent::__construct($em, $kernel, $container, $emailListener, $cache);
        $this->userRepository = $em->getRepository(AuthUser::class);
        $this->sms = $sms;
        $this->crm = $crm;
    }

    public function emailRegistration(RequestJson $json, UserPasswordHasherInterface $hasher, ?string &$errorCode): ?AuthUser
    {
        $email = $json->getString(Params::EMAIL);
        $password = $json->getString(Params::PASSWORD);
        $code = $json->getString(Params::CODE);

        if(!ValidateHelper::validateEmail($email)){
            $errorCode = ApiError::ERROR_INVALID_EMAIL;
            return null;
        }

        /** @var AuthUser $fUser */
        $fUser = $this->userRepository->findOneBy(['email' => $email]);
        if ($fUser != null) {
            $errorCode = ApiError::ERROR_DUPLICATE_EMAIL;
            return null;
        }

        if(!$this->isValidCode($email, $code)) {
            $errorCode = ApiError::ERROR_INVALID_CODE;
            return null;
        }

        $user = new AuthUser();
        $hash = $hasher->hashPassword($user, $password);
        $user->setPassword($hash);
        $user->setEmail($email);
        $user->setIsLogin(false);
        $this->em->persist($user);
        $this->em->flush();

        $user->setCrmId($this->crm->createCustomer($user));
        $this->em->flush();

        $this->mailer->registration($user->getEmail());

        return $user;
    }

    public function phoneRegistration(RequestJson $json, UserPasswordHasherInterface $hasher, ?string &$errorCode): ?AuthUser
    {
        $code = $json->getString(Params::CODE);

        if(!$phone = ValidateHelper::getValidPhone($json->getString(Params::PHONE))){
            $errorCode = ApiError::ERROR_INVALID_PHONE;
            return null;
        }

        /** @var AuthUser $fUser */
        $fUser = $this->userRepository->findOneBy(['phone' => $phone]);
        if ($fUser != null) {
            $errorCode = ApiError::ERROR_DUPLICATE_PHONE;
            return null;
        }

        if(!$this->isValidCode($phone, $code)) {
            $errorCode = ApiError::ERROR_INVALID_CODE;
            return null;
        }

        $user = new AuthUser();
        $user->setPhone($phone);
        $hash = $hasher->hashPassword($user, $phone);
        $user->setPassword($hash);$user->setIsLogin(false);
        $this->em->persist($user);
        $this->em->flush();

        $user->setCrmId($this->crm->createCustomer($user));
        $this->em->flush();

        return $user;
    }

    private function isValidCode($key, $code)
    {
        if($this->env != 'test') {
            $cachedCode = $this->cache->getCode($key);
        }else{
            $cachedCode = 1111;
        }

        return $cachedCode == $code;
    }

    public function emailCheck(string $email, &$error): bool
    {
        if(!ValidateHelper::validateEmail($email)) {
            $error = ApiError::ERROR_INVALID_DATA;
            return false;
        }
        return (bool)$this->userRepository->findOneBy(['email' => $email]);
    }

    /**
     * @param RequestJson $json
     * @param bool $isNewUser
     * @return bool
     * @throws TransportExceptionInterface
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    public function sendCode(RequestJson $json, bool &$isNewUser): bool
    {
        $key = $json->getString(Params::CONTACT);
        $code = $this->createCode();

        if($phone = ValidateHelper::getValidPhone($key)){
            if(!$this->cache->saveCode($phone, $code)) {
                return false;
            }

            if(empty($this->userRepository->findOneByPhone($phone))) {
                $isNewUser = true;
            }

            $msg = sprintf("Код подтверждения %s", $code);

            return $this->sms->send($phone, $msg);
        }

        if(ValidateHelper::validateEmail($key)){
            if(!$this->cache->saveCode($key, $code)) {
                return false;
            }
            if(empty($this->userRepository->findOneByEmail($key))) {
                $isNewUser = true;
                $this->mailer->sendCode($key, $code);
            }
            return true;
        }

        return false;
    }

    public function loginEmail(RequestJson $json, UserPasswordHasherInterface $hasher, ?string &$errorCode): ?AuthUser
    {
        $email = $json->getString(Params::EMAIL);
        $password = $json->getString(Params::PASSWORD);

        if(empty($email) || empty($password)) {
            $errorCode = ApiError::ERROR_INVALID_EMAIL_OR_PASS;
            return null;
        }

        $user = $this->userRepository->findOneBy([
            'email' => $email,
        ]);

        if (!$user || !$hasher->isPasswordValid($user, $password)) {
            $errorCode = ApiError::ERROR_INVALID_EMAIL_OR_PASS;
            return null;
        }

        return $user;
    }

    public function loginPhone(RequestJson $json, ?string &$errorCode): ?AuthUser
    {
        $code = $json->getString(Params::CODE);
        if(!$phone = ValidateHelper::getValidPhone($json->getString(Params::PHONE))) {
            $errorCode = ApiError::ERROR_INVALID_PHONE;
            return null;
        }

        if(empty($phone) || empty($code)) {
            $errorCode = ApiError::ERROR_BAD_REQUEST;
            return null;
        }

        if($code != $this->cache->getCode($phone)) {
            $errorCode = ApiError::ERROR_INVALID_CODE;
            return null;
        }

        $user = $this->userRepository->findOneBy([
            'phone' => $phone,
        ]);

        if (!$user) {
            $errorCode = ApiError::ERROR_INVALID_PHONE;
            return null;
        }

        return $user;
    }

    public function resetPassword(RequestJson $json, ?string &$errorCode): bool
    {
        $email = $json->getString(Params::EMAIL);

        $user = $this->userRepository->findOneByEmail($email);
        if(!$user){
            $errorCode = ApiError::ERROR_UNKNOWN_EMAIL;
            return true;
        }

        $code = $this->createCode();
        if(!$this->cache->saveCode($email, $code)) {
            return false;
        }

        $this->mailer->sendCode($email, $code);

        return true;
    }

    public function updatePassword(RequestJson $json, UserPasswordHasherInterface $hasher, &$errorCode): ?AuthUser
    {
        $email = $json->getString(Params::EMAIL);
        $password = $json->getString(Params::PASSWORD);
        $code = $json->getString(Params::CODE);

        if (empty($email) || empty($code)  || empty($password)) {
            $errorCode = ApiError::ERROR_INVALID_JSON;
            return null;
        }

        if($this->cache->getCode($email) != $code) {
            $errorCode = ApiError::ERROR_INVALID_CODE;
            return null;
        }

        /** @var AuthUser $user */
        $user = $this->userRepository->findOneByEmail($email);
        if(!$user) {
            $errorCode = ApiError::ERROR_UNKNOWN_EMAIL;
            return null;
        }

        $hash = $hasher->hashPassword($user, $password);
        $user->setPassword($hash);
        $user->setIsLogin(false);
        $this->em->persist($user);
        $this->em->flush();

        return $user;
    }

    /**
     * Создание проверочного кода
     * @return string
     */
    private function createCode(): string
    {
        $int = rand(10000, 999999);
        return sprintf("%06d", $int);
    }


}