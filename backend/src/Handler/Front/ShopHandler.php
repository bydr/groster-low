<?php

namespace App\Handler\Front;

use App\Entity\Store;
use App\Handler\Handler;
use App\Interfaces\Front\ShopInterface;
use App\Repository\StoreRepository;

/**
 * class ShopHandler
 */
class ShopHandler extends Handler implements ShopInterface
{
    public function getAllActive(): array
    {
        return $this->em->getRepository(Store::class)->findBy(['isActive' => true]);
    }

    public function getOne(string $uuid): ?Store
    {
        /** @var StoreRepository $repo */
        $repo = $this->em->getRepository(Store::class);
        return $repo->findOneByUuid($uuid);
    }
}