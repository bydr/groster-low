<?php

namespace App\Handler\Front;

use App\Handler\Handler;
use App\Helpers\BannerHelper;
use App\Interfaces\Front\BannerInterface;

/**
 * Баннеры
 */
class BannerHandler extends Handler implements BannerInterface
{
    public function getAll(): string
    {
        return BannerHelper::getAll($this->cache, $this->em);
    }
}