<?php

namespace App\Handler;

use App\EventListener\EmailListener;
use App\Service\Cache;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\KernelInterface;

abstract class Handler
{
    /** @var EntityManagerInterface */
    protected $em;

    /** @var Cache */
    protected $cache;

    /** @var ContainerInterface  */
    protected $ci;

    protected $env;
    protected $mailer;

    public function __construct(EntityManagerInterface $em,
                                KernelInterface $kernel,
                                ContainerInterface $container,
                                EmailListener $emailListener,
                                Cache $cache
    )
    {
        $this->em = $em;
        $this->env = $kernel->getEnvironment();
        $this->ci = $container;
        $this->cache = $cache;
        $this->mailer = $emailListener;
    }
}
