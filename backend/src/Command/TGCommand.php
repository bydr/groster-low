<?php

namespace App\Command;

use App\Service\TelegramBot;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class TGCommand extends ApiCommand
{
    protected static $defaultName = 'app:tg:test';
    private $tgbot;

    public function __construct(string $name = null, TelegramBot $telegramBot)
    {
        parent::__construct($name);
        $this->tgbot = $telegramBot;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->tgbot->sendMessage("Message");
        return self::SUCCESS;
    }
}