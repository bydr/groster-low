<?php

namespace App\Command;

use Doctrine\DBAL\Driver\Exception;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class CheckDuplicatesCommand extends ApiCommand
{
    /** @var EntityManagerInterface */
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;

        parent::__construct();
    }

    protected function configure()
    {
        parent::configure();
        $this->setName('app:check:duplicates')
            ->setDescription('Удаление дубликатов связи товара и свойства. Удаление дубликатов картинок')
        ;

    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|void
     * @throws Exception
     * @throws \Doctrine\DBAL\Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->getLogger()->info('Удаление дубликатов свойств товаров и картинок');
        $conn = $this->em->getConnection();
        $statement = $conn->prepare('
            DELETE FROM product_value WHERE ctid NOT IN
            (SELECT max(ctid) FROM product_value GROUP BY product_id, value_id)');
        $qty = $statement->executeStatement();
        $conn->close();
        $this->getLogger()->info(sprintf("Удалено %s дубликатов свойств", $qty));

        $conn = $this->em->getConnection();$statement = $conn->prepare('DELETE FROM product_image WHERE ctid NOT IN
            (SELECT max(ctid) FROM product_image GROUP BY product_id, path)');
        $qty = $statement->executeStatement();
        $conn->close();
        $this->getLogger()->info(sprintf("Удалено %s дубликатов картинок", $qty));

        return self::SUCCESS;
    }
}