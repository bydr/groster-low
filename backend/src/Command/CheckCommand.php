<?php

namespace App\Command;

use App\Entity\Product;
use App\Entity\ProductImage;
use App\Helpers\ProductHelper;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class CheckCommand проверяет наличие картинок товаров на сервере
 * @when Один раз в день
 */
class CheckCommand extends ApiCommand
{
    /** @var EntityManagerInterface */
    private $em;

    /** @var string */
    private $imageDirPath;

    public function __construct(ContainerInterface $ci, EntityManagerInterface $em)
    {
        $this->em = $em;
        $this->imageDirPath = $ci->getParameter('kernel.project_dir') . '/public' . DIRECTORY_SEPARATOR . "images/shop" . DIRECTORY_SEPARATOR;

        parent::__construct();
    }

    protected function configure()
    {
        parent::configure();
        $this->setName('app:check:images')
            ->setDescription('Проверка наличия картинок товаров на сервере')
        ;

    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        ini_set('memory_limit', '-1');
        $products = $this->em->getRepository(Product::class)->findAll();

        /** @var Product $product */
        foreach ($products as $product) {
            /** @var ProductImage $image */
            foreach ($product->getGallery() as $image) {
                if (empty($image->getPath())) {
                    $this->getLogger()->info(sprintf("Нет ссылки на картинку в товаре %s(%d)", $product->getAlias(), $product->getId() ));
                    $this->em->remove($image);
                    continue;
                }

                $pathArr = explode("/", $image->getPath());
                $filename = array_pop($pathArr);

                if(!is_file($this->imageDirPath . $filename)) {
                    $this->getLogger()->info(sprintf("Нет файла картинки %s для товара %s(%d)", $filename, $product->getAlias(), $product->getId()));
                    $this->em->remove($image);
                }
            }
        }

        $this->em->flush();

        return self::SUCCESS;
    }
}