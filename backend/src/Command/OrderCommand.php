<?php

namespace App\Command;

use App\Entity\Order;
use App\EventListener\EmailListener;
use App\Repository\OrderRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class OrderCommand extends ApiCommand
{
    /** @var ContainerInterface */
    private $ci;

    /** @var EmailListener */
    private $emailListener;


    public function __construct(EntityManagerInterface $em, EmailListener $emailListener)
    {
        $this->em = $em;
        $this->emailListener = $emailListener;
        parent::__construct();
    }

    protected function configure()
    {
        parent::configure();
        $this->setName('app:order:notification')
            ->setDescription('Отправка уведомлений на подписанные заказы')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $orders = $this->em->getRepository(Order::class)->findBy(['subscribe_interval IS NOT NULL']);

        /** @var Order $order */
        foreach ($orders as $order) {
            $isSend = false;
            switch ($order->getSubscribeInterval()) {
                case Order::SUBSCRIBE_DAILY:
                    $isSend = true;
                    break;
                case Order::SUBSCRIBE_WEEKLY:
                    $isSend = in_array(date('d'), [1, 15]);
                    break;
                case Order::SUBSCRIBE_MONTHLY:
                    $isSend = 1 == date('d');
            }

            if($isSend) {
                $this->emailListener->orderNotification($order);
            }
        }

        return 0;
    }
}