<?php

namespace App\Command;

use App\Entity\Order;
use App\EventListener\EmailListener;
use App\Helpers\Params;
use App\Service\Cache;
use App\Service\Server\Server;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class PricelistCommand extends ApiCommand
{
    /** @var ContainerInterface */
    private $ci;

    /** @var EmailListener */
    private $emailListener;

    /** @var Cache */
    private $cache;

    public function __construct(ContainerInterface $ci, EntityManagerInterface $em, EmailListener $emailListener, Cache $cache)
    {
        $this->ci = $ci;
        $this->em = $em;
        $this->emailListener = $emailListener;
        $this->cache = $cache;
        parent::__construct();
    }

    protected function configure()
    {
        parent::configure();
        $this->setName('app:pricelist:send')
            ->setDescription('Отправка прайслистов на почты')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $keys = $this->cache->getPricelistQuiries();
        if (empty($keys)) {
            return self::SUCCESS;
        }

        $server = new Server($this->ci, $this->em);
        foreach ($keys as $key) {
            $data = $this->cache->getPricelistQuiryByKey($key);
            if(empty($data)) {
                continue;
            }

            $filepath = $server->getPricelist($data[Params::CATEGORIES]);
            if(empty($filepath)) {
                continue;
            }

            $this->emailListener->sendPricelist($data[Params::EMAIL], $filepath);
        }

        return self::SUCCESS;
    }
}