<?php

namespace App\Command;

use Monolog\Handler\StreamHandler;
use Monolog\Logger;
use Symfony\Component\Console\Command\Command;

abstract class ApiCommand extends Command
{
    private $logger;

    public function getLogger(): Logger
    {
        if(empty($this->logger)) {
            $this->initLogger();
        }

        return $this->logger;
    }

    private function initLogger()
    {
        $logger = new Logger('api_command');
        $logDir = dirname(dirname(__DIR__)) . '/var/log/api/';
        if(!is_dir($logDir)) {
            mkdir($logDir, 0740, true);
        }
        $logger->pushHandler(new StreamHandler($logDir . 'command.log', Logger::INFO));

        $this->logger = $logger;
    }
}