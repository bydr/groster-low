<?php

namespace App\Command;

use App\Helpers\CategoryHelper;
use App\Helpers\Parsers\Parser;
use App\Service\Cache;
use App\Service\TelegramBot;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\KernelInterface;
use ZipArchive;

class ParserCommand extends ApiCommand
{

    const PRODUCTS = 'Products.xml';
    const STOCK = 'ProductsStock.xml';
    const PRODUCT_PRICES = 'ProductsPrices.xml';
    const BUYERS = 'Buyers.xml';

    /** @var ContainerInterface */
    private $ci;

    private $importDir;

    /** @var Parser */
    private $parser;

    /** @var bool */
    private $isTestEnv;

    /** @var $cache  */
    private $cache;

    private $tgBot;


    public function __construct(ContainerInterface $ci, Parser $parser, KernelInterface $kernel, Cache $cache,
                                TelegramBot $telegramBot)
    {
        $this->ci = $ci;
        $this->importDir = $ci->getParameter('kernel.project_dir') . '/public/upload/import';
        $this->parser = $parser;
        $this->isTestEnv = $kernel->getEnvironment() == 'test';
        $this->cache = $cache;
        $this->tgBot = $telegramBot;
        parent::__construct();
    }

    protected function configure()
    {
        parent::configure();
        $this->setName('app:parse:xml')
            ->setDescription('Parser product files from 1C')
            ->setHelp('Files for parsing should be in public/upload/import')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        ini_set('memory_limit', '-1');

        if($this->isTestEnv) {
            $filesDir = 'test_import';
        }else{
//            $archive = $this->getArchive();
//            // Если нового файла нет - выходим
//            if(!$archive) {
//                $output->writeln("Not found archive for parsing");
//                return 0;
//            }
//            $this->getLogger()->info(sprintf("Парсинг архива %s", $archive));
//
//            $output->writeln("Unzip archive");
//            if(!$this->unzip($archive, $output)) {
//                $this->getLogger()->error("Failed to unzip archive");
//                $output->writeln("Failed to unzip archive");
//                return 0;
//            }

            $filesDir = 'import/webdata';
        }


        $t1 = microtime(true);
        $this->tgBot->sendMessage("Начало парсинга xml");

        $output->writeln([
            'Parse xml',
            '==================',
            ''
        ]);

        $fullPathToDir = sprintf("%s/public/upload/%s/", $this->ci->getParameter('kernel.project_dir'), $filesDir);
        $start = time();
        try {
            if(!is_dir($fullPathToDir)) {
                $output->writeln(sprintf("Нет директории для парсинга %s", $fullPathToDir));
                return 0;
            }
            $productFile = $fullPathToDir . self::PRODUCTS;
            if(is_file($productFile)) {
                $this->parser->products($productFile, $output);
            }

            $start = $this->alarm($start);
            $stockFile = $fullPathToDir . self::STOCK;
            if(is_file($stockFile)) {
                $this->parser->stores($stockFile, $output);
            }

            $start = $this->alarm($start);
            $productPricesFile = $fullPathToDir . self::PRODUCT_PRICES;
            if(is_file($productPricesFile)) {
                $this->parser->productPrices($productPricesFile, $output);
            }

            $start = $this->alarm($start);
            $buyersFile = $fullPathToDir . self::BUYERS;
            if(is_file($buyersFile)) {
                $this->parser->parseBuyers($buyersFile, $output);
            }

        }catch (\Exception $e) {
            $msg = sprintf("Error parse products in %s(%d): %s", $e->getFile(), $e->getLine(), $e->getMessage());
            $this->getLogger()->error($msg);
            $output->writeln("ERROR: " . $msg);
            $this->tgBot->sendMessage($msg);
        }

        $start = $this->alarm($start);
        $this->cache->removeCategoriesList(CategoryHelper::KEY_TOTAL);

        $start = $this->alarm($start);
        $this->cache->removeHits();
        $t2 = microtime(true);
        $mainTime = $t2 - $t1;
        $output->writeln(['----------------------------', 'time execution: ' . $mainTime]);
        $this->tgBot->sendMessage("Парсинг окончен");
        $this->getLogger()->info("Парсинг окончен");
        return 0;
    }

    /**
     * Возвращает файл архива новой выгрузки
     * @return string|null
     */
    private function getArchive(): ?string
    {
        $files = scandir($this->importDir);
        foreach ($files as $file) {
            if(preg_match("~\.zip$~", $file)) {
                return $this->importDir . DIRECTORY_SEPARATOR . $file;
            }
        }

        return null;
    }

    /**
     * Удаление старых файлов выгрузки и распаковка архива
     * @param string $archive
     * @return bool
     */
    private function unzip(string $archive, OutputInterface $output): bool
    {
        $webdata = $this->importDir . DIRECTORY_SEPARATOR . "webdata";
        system("rm -rf ".escapeshellarg($webdata));

        mkdir($webdata, 0777, true);

        $zip = new ZipArchive();
        // Открытие архива
        $output->writeln(sprintf("Открытие архива %s", $archive));
        if(!$zip->open($archive)) {
            $msg = sprintf("Failed to open archive %s", $archive);
            $output->writeln($msg);
            $this->getLogger()->error($msg);
            return false;
        }

        // Извлечение данных из архива
        $output->writeln("Распаковка архива");
        try {
            if(!$zip->extractTo($webdata)) {
                $msg = sprintf("Failed to unzip archive %s", $archive);
                $output->writeln($msg);
                $this->getLogger()->error($msg);
                return false;
            }
        } catch (\Exception $e) {
            $this->getLogger()->error($e->getMessage());
            // Удаление архива
            unlink($archive);
            return false;
        }


        // Удаление архива
        unlink($archive);

        return true;
    }

    /**
     * Уведомление, что парсер еще работает, а не упал
     * @param int $start
     * @return int
     */
    private function alarm(int $start)
    {
        if ((time() - $start) > 600) {
            $start = time();
            $this->tgBot->sendMessage('В процессе работы...');
        }

        return $start;
    }
}