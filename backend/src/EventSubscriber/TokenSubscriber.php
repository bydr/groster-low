<?php

namespace App\EventSubscriber;

use App\Controller\Admin\AdminController;
use App\Controller\ApiController;
use App\Controller\Front\FavoriteController;
use App\Helpers\RequestHelper;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Event\ControllerEvent;
use Symfony\Component\HttpKernel\Event\ResponseEvent;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

/**
 * class TokenSubscriber
 * Middleware для pre-request и post-response
 */
class TokenSubscriber implements EventSubscriberInterface
{
    private $tokenStorage;

    public function __construct(TokenStorageInterface $tokenStorage)
    {
        $this->tokenStorage = $tokenStorage;
    }

    public function onKernelController(ControllerEvent $event)
    {
        $request = $event->getRequest();
        ApiController::getLogger()->debug(sprintf("START %s: %s", $request->getMethod(), $request->getRequestUri()));

        $controller = $event->getController();

        // when a controller class defines multiple action methods, the controller
        // is returned as [$controllerInstance, 'methodName']
        if (is_array($controller)) {
            $controller = $controller[0];
        }

        /** @var ApiController $controller */
        if(!($controller instanceof ApiController)){
            return;
        }

        $token = $event->getRequest()->headers->get('Authorization');

        if (!empty($token)) {
            ApiController::setCurrentUser($this->tokenStorage->getToken()->getUser());
        }else{
            ApiController::setCurrentUser(null);
        }

        if ($controller instanceof FavoriteController) {
            if(empty(ApiController::getCurrentUser())) {
                throw new UnauthorizedHttpException("Bearer");
            }
        }

        // для админки обязательное наличие пользователя и достаточные права у него
        if ($controller instanceof AdminController) {
            $user = ApiController::getCurrentUser();
            if(empty($user) || !$user->isAdmin()) {
                throw new AccessDeniedHttpException('This action needs a valid token!');
            }
        }
    }

    public function onKernelResponse(ResponseEvent $event)
    {
        $request = $event->getRequest();
        ApiController::getLogger()->debug(sprintf("FINISHED %s: %s", $request->getMethod(), $request->getRequestUri()));

        $response = $event->getResponse();

        if ($response instanceof JsonResponse) {
            $response->setEncodingOptions(JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE);
        }
        // Логируем запрос, если он возвращается с кодом ошибки
        if($response->getStatusCode() >= 400) {
            $requestUrl = RequestHelper::getRequestString($event->getRequest());
            $msg = sprintf("[%s] %s with response %s", $event->getRequest()->getMethod(), $requestUrl, $response->getContent());
            ApiController::getLogger()->info($msg);
        }
    }

    public static function getSubscribedEvents(): array
    {
        return [
            KernelEvents::CONTROLLER => 'onKernelController',
            KernelEvents::RESPONSE => 'onKernelResponse',
        ];
    }
}