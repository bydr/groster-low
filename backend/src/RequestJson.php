<?php

namespace App;

/**
 * Тело запроса в JSON
 */
class RequestJson
{
    /** @var array  */
    private $json;

    public function __construct(array $json)
    {
        $this->json = $json;
    }

    public function getBool($key, $default = false): bool
    {
        if(!isset($this->json[$key])) {
            return $default;
        }

        $value = $this->json[$key];
        if(is_bool($value)) {
            return $value;
        }

        return (bool)$value;
    }

    public function getArray($key, $default = [], $sep = ','): array
    {
        if(!isset($this->json[$key])) {
            return $default;
        }

        $value = $this->json[$key];

        if(is_array($value)) {
            return $value;
        }

        if(empty($value)){
            return $default;
        }

        return explode($sep, $value);
    }

    public function getString($key, $default = ""): string
    {
        if(!isset($this->json[$key]) || "null" == $this->json[$key]) {
            return $default;
        }

        return trim($this->json[$key]);
    }

    public function getInt(string $key, $default = 0): int
    {
        if(!isset($this->json[$key])) {
            return $default;
        }

        return (int)$this->json[$key];
    }

}