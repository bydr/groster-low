<?php

namespace App\Interfaces\Server;

use App\Entity\Discount;
use App\RequestJson;

interface ServerInterface
{
    /**
     * Создание промокода из 1С
     * @param RequestJson $json
     * @param $error
     * @return Discount|null
     */
    public function createPromo(RequestJson $json): ?string;

    /**
     * Обновление данных промокода из 1С
     * @param string $code
     * @param RequestJson $json
     * @return Discount|null
     */
    public function updatePromo(string $code, RequestJson $json): ?string;

    /**
     * Получение промокода по id
     * @param string $code
     * @return Discount|null
     */
    public function getPromo(string $code): ?Discount;
}