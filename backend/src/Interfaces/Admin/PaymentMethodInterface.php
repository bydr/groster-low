<?php

namespace App\Interfaces\Admin;

use App\Entity\ShippingMethod;
use App\RequestJson;

/**
 * Способы оплаты
 */
interface PaymentMethodInterface
{
    /**
     * Все способы оплаты
     * @return array
     */
    public function getAll(): array;

    /**
     * Изменение данных способа оплаты
     * @param RequestJson $json
     * @param string|null $uid
     * @param string|null $error
     * @return string
     */
    public function methodUpdate(RequestJson $json, ?string $uid, ?string &$error): string;
}