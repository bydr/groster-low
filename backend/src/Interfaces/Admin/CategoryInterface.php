<?php

namespace App\Interfaces\Admin;

use App\Entity\Category;
use App\RequestJson;

/**
 * Обновление категорий
 */
interface CategoryInterface
{
    public function findOne(int $id): ?Category;
    public function updateOne(RequestJson $json, int $id, ?string &$errorCode): void;
}