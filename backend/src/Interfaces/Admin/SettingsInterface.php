<?php

namespace App\Interfaces\Admin;

use App\Entity\Faq;
use App\Entity\FaqType;
use App\Entity\Settings;
use App\RequestJson;

/**
 * Общие настройки
 */
interface SettingsInterface
{
    /**
     * Список настроек
     * @return Settings|null
     */
    public function getSettings(): ?Settings;

    /**
     * Обновление настроек
     * @param RequestJson $json
     * @return string|null
     */
    public function update(RequestJson $json): ?string;
}