<?php

namespace App\Interfaces\Admin;

use App\RequestJson;

/**
 * Заказы
 */
interface OrderInterface
{
    /**
     * Добавление/обновление способа замены товара
     * @param RequestJson $json
     * @param int $id
     * @return int
     */
    public function replacementUpdate(RequestJson $json, int $id): int;

}