<?php

namespace App\Interfaces\Admin;

use App\Entity\Discount;
use App\RequestJson;

/**
 * Работа с промокодами
 */
interface PromocodeInterface
{
    /**
     * Список существующих промокодов
     * @return array
     */
    public function list(): array;

    /**
     * Создание промокода
     * @param RequestJson $json
     * @return Discount|null
     */
    public function create(RequestJson $json): ?Discount;

    /**
     * Получение промокода по id
     * @param int $id
     * @return Discount|null
     */
    public function getOne(int $id): ?Discount;

    /**
     * Обновление данных промокода
     * @param int $id
     * @param RequestJson $json
     * @param string|null $error
     * @return Discount|null
     */
    public function updateOne(int $id, RequestJson $json, ?string &$error): ?Discount;
}