<?php

namespace App\Interfaces\Admin;

use App\Entity\Tag;
use App\RequestJson;

/**
 * Работа с тегами
 */
interface TagInterface
{
    public function updateOne(RequestJson $json, int $id): ?Tag;
    public function deleteOne(int $id);
}