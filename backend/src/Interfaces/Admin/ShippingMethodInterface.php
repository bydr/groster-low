<?php

namespace App\Interfaces\Admin;

use App\Entity\ShippingMethod;
use App\RequestJson;

/**
 * Способы доставки
 */
interface ShippingMethodInterface
{
    /**
     * Все способы доставки
     * @return array
     */
    public function getAll(): array;

    /**
     * Изменение данных способа доставки
     * @param RequestJson $json
     * @param string|null $alias
     * @param string|null $error
     * @return string
     */
    public function methodUpdate(RequestJson $json, ?string $alias, ?string &$error): string;

    /**
     * Данные способа доставки
     * @param string $alias
     * @return ShippingMethod|null
     */
    public function methodGet(string $alias): ?ShippingMethod;

    /**
     * Список ограничений для способа доставки
     * @param string $alias
     * @return array
     */
    public function methodLimitsAll(string $alias): array;

    /**
     * Обновление ограничения для способа доставки
     * @param RequestJson $json
     * @param string $alias
     * @param int $id
     * @param string|null $error
     * @return int
     */
    public function methodLimitUpdate(RequestJson $json, string $alias, int $id, ?string &$error): int;

    /**
     * Удаление ограничения для способа доставки
     * @param string $alias
     * @param int $id
     * @return bool
     */
    public function methodLimitDelete(string $alias, int $id): bool;
}