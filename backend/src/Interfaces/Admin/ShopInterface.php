<?php

namespace App\Interfaces\Admin;

use App\Entity\Store;
use App\RequestJson;
use Exception;

/**
 * interface ShopInterface
 */
interface ShopInterface
{
    /**
     * Возвращает массив активных магазинов
     *
     * @return Store[]
     */
    public function getAllActive(): array;

    /**
     * Возвращает магазин по uid
     *
     * @param string $uid
     * @return Store|null
     */
    public function getOne(string $uid): ?Store;

    /**
     * Обновляет магазин по uid
     *
     * @param string $uid
     * @param RequestJson $json
     * @return Store|null
     * @throws Exception
     */
    public function update(string $uid, RequestJson $json): ?Store;
}