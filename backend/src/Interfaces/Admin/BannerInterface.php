<?php

namespace App\Interfaces\Admin;

use App\RequestJson;

/**
 * Баннеры
 */
interface BannerInterface
{
    /**
     * Список баннеров
     * @return array
     */
    public function getAll(): array;

    /**
     * Добавление баннера
     * @param RequestJson $json
     * @return string|null
     */
    public function add(RequestJson $json): ?string;

    /**
     * Обновление баннера
     * @param RequestJson $json
     * @param int $id
     * @return string|null
     */
    public function update(RequestJson $json, int $id): ?string;

    /**
     * Удаление баннера
     * @param int $id
     * @return string|null
     */
    public function delete(int $id): ?string;

}