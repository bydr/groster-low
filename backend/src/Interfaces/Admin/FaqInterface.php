<?php

namespace App\Interfaces\Admin;

use App\Entity\Faq;
use App\Entity\FaqType;
use App\RequestJson;

/**
 * Блоки вопрос-ответ
 */
interface FaqInterface
{

    /**
     * Список блоков вопрос-ответ
     * @return FaqType[]
     */
    public function allBlocks(): array;

    /**
     * Сохранение/обновление блока
     * @param RequestJson $json
     * @param int $id
     * @return string|null
     */
    public function updateBlock(RequestJson $json, int $id): ?string;

    /**
     * Удаление блока
     * @param int $id
     */
    public function deleteBlock(int $id);

    /**
     * Список вопрос-ответ
     * @return Faq[]
     */
    public function allQuestions(): array;

    /**
     * Сохранение/редактирование вопрос-ответ
     * @param RequestJson $json
     * @param int $id
     * @return string|null
     */
    public function updateQuestion(RequestJson $json, int $id): ?string;

    /**
     * Удаление вопрос-ответ
     * @param int $id
     */
    public function deleteQuestion(int $id);
}