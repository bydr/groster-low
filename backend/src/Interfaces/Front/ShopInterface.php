<?php

namespace App\Interfaces\Front;

use App\Entity\Store;

/**
 * interface ShopInterface
 */
interface ShopInterface
{
    /**
     * Возвращает массив активных магазинов
     *
     * @return Store[]
     */
    public function getAllActive(): array;

    /**
     * Возвращает магазин по uid
     *
     * @param string $uid
     * @return Store|null
     */
    public function getOne(string $uuid): ?Store;
}