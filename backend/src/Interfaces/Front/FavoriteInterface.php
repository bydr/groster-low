<?php

namespace App\Interfaces\Front;

use App\Entity\Category;
use App\Entity\Tag;
use App\RequestJson;
use Symfony\Component\HttpFoundation\Request;

interface FavoriteInterface
{
    /**
     * Список товаров в избранном
     * @return array
     */
    public function getAllProducts(): array;

    /**
     * Добавление товара в избранное
     * @param RequestJson $json
     * @return string|null
     */
    public function addProducts(RequestJson $json): ?string;

    /**
     * Удаление товара из избранного
     * @param RequestJson $json
     * @return string|null
     */
    public function deleteProduct(RequestJson $json): ?string;

}