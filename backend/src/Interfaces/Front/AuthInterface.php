<?php

namespace App\Interfaces\Front;

use App\Entity\Category;
use App\Entity\Security\AuthUser;
use App\RequestJson;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

/**
 * Interface AuthInterface
 */
interface AuthInterface
{
    /**
     * @param RequestJson $json
     * @param UserPasswordHasherInterface $hasher
     * @param string|null $errorCode
     * @return AuthUser|null
     */
    public function emailRegistration(RequestJson $json, UserPasswordHasherInterface $hasher, ?string &$errorCode): ?AuthUser;

    /**
     * @param RequestJson $json
     * @param UserPasswordHasherInterface $hasher
     * @param string|null $errorCode
     * @return AuthUser|null
     */
    public function phoneRegistration(RequestJson $json, UserPasswordHasherInterface $hasher, ?string &$errorCode): ?AuthUser;

    /**
     * @param string $email
     * @param string|null $errorCode
     * @return bool
     */
    public function emailCheck(string $email, ?string &$errorCode): bool;

    /**
     * @param RequestJson $json
     * @param bool $isNewUser
     * @return bool
     */
    public function sendCode(RequestJson $json, bool &$isNewUser): bool;

    /**
     * @param RequestJson $json
     * @param UserPasswordHasherInterface $hasher
     * @param string|null $errorCode
     * @return AuthUser|null
     */
    public function loginEmail(RequestJson $json, UserPasswordHasherInterface $hasher, ?string &$errorCode): ?AuthUser;

    /**
     * @param RequestJson $json
     * @param string|null $errorCode
     * @return AuthUser|null
     */
    public function loginPhone(RequestJson $json, ?string &$errorCode): ?AuthUser;

    /**
     * @param RequestJson $json
     * @param string|null $errorCode
     * @return bool
     */
    public function resetPassword(RequestJson $json, ?string &$errorCode): bool;

    /**
     * @param RequestJson $json
     * @param UserPasswordHasherInterface $hasher
     * @param string|null $errorCode
     * @return AuthUser|null
     */
    public function updatePassword(RequestJson $json, UserPasswordHasherInterface $hasher, ?string &$errorCode): ?AuthUser;
}