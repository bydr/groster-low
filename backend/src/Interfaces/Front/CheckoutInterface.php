<?php

namespace App\Interfaces\Front;

use App\Entity\Order;
use App\Entity\ReplacementMethod;
use App\RequestJson;

/**
 * Оформление заказа
 */
interface CheckoutInterface
{
    /**
     * Предыдущий заказ пользователя
     * @return Order|null
     */
    public function lastOrder(): ?Order;

    /**
     * Список пользовательских данных из прошлых заказов
     * @return array
     */
    public function customerData(): array;

    /**
     * Список доступных способов доставки
     * @param RequestJson $json
     * @return array
     */
    public function shippingMethods(RequestJson $json): array;

    /**
     * Список доступных способов оплаты
     * @return array
     */
    public function paymentMethods(): array;


    /**
     * Список способов замены товара при его отсутствии
     * @return array
     */
    public function getReplacements(): array;

    /**
     * Стоимость доставки для выбранного заказ
     * @param RequestJson $json
     * @param string|null $error
     * @return array
     */
    public function shippingCost(RequestJson $json, ?string &$error): array;

    /**
     * Сохранение заказа и возвращает номер заказа
     * @param RequestJson $json
     * @param string $uid
     * @param string|null $error
     * @return int
     */
    public function orderSave(RequestJson $json, string $uid, ?string &$error): int;
}