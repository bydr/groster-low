<?php

namespace App\Interfaces\Front;

/**
 * Баннеры
 */
interface BannerInterface
{
    /**
     * Список баннеров
     * @return string
     */
    public function getAll(): string;

}