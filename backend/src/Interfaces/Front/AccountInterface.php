<?php

namespace App\Interfaces\Front;

use App\Entity\Order;
use App\Entity\Payer;
use App\Entity\ShippingAddress;
use App\RequestJson;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

/**
 * Личный кабинет
 */
interface AccountInterface
{
    /**
     * Список плательщиков
     * @return array
     */
    public function allPayers(): array;

    /**
     * Обновление плательщика
     * @param RequestJson $json
     * @param string|null $uid
     * @param string|null $error
     * @return string|null
     */
    public function payerUpdate(RequestJson $json, ?string $uid, ?string &$error): ?string;

    /**
     * Получение данных по плательщику
     * @param string $uid
     * @return Payer|null
     */
    public function payerGet(string $uid): ?Payer;

    /**
     * Список адресов
     * @return array
     */
    public function allAddresses(): array;

    /**
     * Обновление адреса
     * @param RequestJson $json
     * @param string|null $uid
     * @return string|null
     */
    public function addressUpdate(RequestJson $json, ?string $uid): ?string;

    /**
     * Установка адреса по умолчанию
     * @param string|null $uid
     * @return string|null
     */
    public function addressSetDefault(string $uid): ?string;

    /**
     * Получение данных по адресу
     * @param string $uid
     * @return ShippingAddress|null
     */
    public function addressGet(string $uid): ?ShippingAddress;

    /**
     * Обновление пароля
     * @param RequestJson $json
     * @param UserPasswordHasherInterface $hasher
     * @return string|null
     */
    public function updatePassword(RequestJson $json, UserPasswordHasherInterface $hasher): ?string;

    /**
     * Обновление ФИО
     * @param RequestJson $json
     */
    public function updateFio(RequestJson $json);

    /**
     * Список заказов в виде заказов
     * @param RequestJson $json
     * @param string|null $error
     * @return Order[]
     */
    public function orderListOrders(RequestJson $json, ?string &$error): array;

    /**
     * Список заказов в виде товаров
     * @param RequestJson $json
     * @param string|null $error
     * @return Order[]
     */
    public function orderListProducts(RequestJson $json, ?string &$error): array;

    /**
     * Детали заказа
     * @param string $uid
     * @return Order|null
     */
    public function orderDetail(string $uid): ?Order;

    /**
     * Отмена заказа
     * @param string $uid
     * @return Order|null
     */
    public function orderCancel(string $uid): ?string;

    /**
     * Удаление товара из отслеживаемых
     * @param RequestJson $json
     * @return string|null
     */
    public function waitingProductDelete(RequestJson $json): ?string;

    /**
     * Подписка на заказ
     * @param RequestJson $json
     * @param string $uid
     * @return string|null
     */
    public function subscribeOrder(RequestJson $json, string $uid): ?string;

    /**
     * Отмена подписки на заказ
     * @param string $uid
     * @return string|null
     */
    public function unsubscribeOrder(string $uid): ?string;

    /**
     * Получение файлов счета на оплату
     * @param string $uid
     * @param string|null $err
     * @return string|null
     */
    public function getInvoicePayment(string $uid, ?string &$err): ?string;

    /**
     * Получение файлов подписанного договора плательщика
     * @param string $uid
     * @param string|null $err
     * @return string|null
     */
    public function getAttachedContract(string $uid, ?string &$err): ?string;

    /**
     * Файлы договора плательщика
     * @param string $uid
     * @param string|null $err
     * @return string|null
     */
    public function getGenerateDocuments(string $uid, ?string &$err): ?string;

    /**
     * Получение файлов ТОРГ-12 по переданному Id заказа, в случае если товары по заказу уже были отгружены
     * @param string $uid
     * @param string|null $err
     * @return string|null
     */
    public function getPackingDocuments(string $uid, ?string &$err): ?string;
}