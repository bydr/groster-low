<?php

namespace App\Interfaces\Front;

use App\Entity\Order;
use App\RequestJson;
use Exception;

interface OrderInterface
{
    /**
     * Список товаров в корзине
     * @param string $uid
     * @return Order|null
     * @throws Exception
     */
    public function getCart(string $uid): ?Order;

    /**
     * Изменение количества единиц товара в корзине
     * @param RequestJson $json
     * @param string|null $errorCode
     * @return array
     * @throws Exception
     */
    public function changeProductQty(RequestJson $json, ?string &$errorCode): array;

    /**
     * Привязка текущего пользователя к корзине
     *
     * @param string $uid
     * @return bool
     */
    public function bindUser(string $uid): bool;

    /**
     * Удаление товара из корзины
     * @param RequestJson $json
     * @param string|null $errorCode
     * @return Order|null
     * @throws Exception
     */
    public function deleteProduct(RequestJson $json, ?string &$errorCode): ?Order;

    /**
     * Очистка корзины
     * @param string $uid
     * @return bool
     * @throws Exception
     */
    public function clear(string $uid): bool;

    /**
     * Удаление корзины
     * @param string $uid
     * @return bool
     * @throws Exception
     */
    public function remove(string $uid): bool;

    /**
     * Добавление образца в корзину
     * @param RequestJson $json
     * @return array
     */
    public function sample(RequestJson $json): array;

    /**
     * Сумма минимального заказа для бесплатной доставки
     * @param string $region
     * @return int
     */
    public function freeShipping(string $region): int;

    /**
     * Заказ оформляется как "Быстрый заказ"
     * @param RequestJson $json
     * @param string $uid
     * @param string|null $error
     * @return int
     */
    public function fastOrder(RequestJson $json, string $uid, ?string &$error): int;

    /**
     * Изменение из 1C статуса заказа
     * @param RequestJson $json
     * @param string $uid
     * @return string|null
     */
    public function changeStatus(RequestJson $json, string $uid): ?string;

    /**
     * Изменение состава заказа через 1С
     * @param RequestJson $json
     * @param string $uid
     * @return array|string
     */
    public function changeComposition(RequestJson $json, string $uid);
}