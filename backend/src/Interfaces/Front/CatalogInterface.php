<?php

namespace App\Interfaces\Front;

use App\Entity\Category;
use App\Entity\Tag;
use App\RequestJson;
use Symfony\Component\HttpFoundation\Request;

interface CatalogInterface
{
    /**
     * Список категорий
     * @return string
     */
    public function findAllCategories(): string;

    /**
     * Список категорий для сферы
     *
     * @param int $type
     *
     * @return array
     */
    public function findAllCategoriesByArea(string $uuid): string;

    /**
     * Страница "Помощь в выборе"
     * @param int $id
     * @param string|null $errorCode
     * @return Category|null
     */
    public function findHelpPage(int $id, ?string &$errorCode): ?Category;

    /**
     * Список тегов
     *
     * @return array
     */
    public function findAllTags(): array;

    /**
     * Возвращает 1 тег по id
     *
     * @param int $id
     * @param string|null $errorCode
     * @return Tag|null
     */
    public function findOneTag(int $id, ?string &$errorCode): ?Tag;

    /**
     * Список всех параметров
     * @return string
     */
    public function findAllParams(): string;

    /**
     * Фильтрация товаров
     * @param array $data
     * @return array
     */
    public function filterProducts(array $data): array;

    /**
     * Список доступных типов сотировки
     * @return array
     */
    public function getSortTypes(): array;

    /**
     * Фильтрация товаров
     * @return string
     */
    public function getHits(): string;

    /**
     * Запрос прайслиста из 1С
     * @param RequestJson $data
     * @return string
     */
    public function pricelist(RequestJson $data, ?string &$err): string;
}