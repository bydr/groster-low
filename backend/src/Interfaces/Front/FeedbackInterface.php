<?php

namespace App\Interfaces\Front;

use App\RequestJson;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;

/**
 * Обратная связь
 */
interface FeedbackInterface
{
    /**
     * Обработка формы обратной связи
     * @param RequestJson $json
     * @return string|null
     *
     * @throws SyntaxError
     * @throws TransportExceptionInterface
     * @throws RuntimeError
     * @throws LoaderError
     */
    public function question(RequestJson $json): ?string;

    /**
     * Заказать звонок
     * @param RequestJson $json
     * @return string|null
     *
     * @throws SyntaxError
     * @throws TransportExceptionInterface
     * @throws RuntimeError
     * @throws LoaderError
     */
    public function recall(RequestJson $json): ?string;

    /**
     * Отправить письмо менеджеру
     * @param RequestJson $json
     * @return string|null
     *
     * @throws SyntaxError
     * @throws TransportExceptionInterface
     * @throws RuntimeError
     * @throws LoaderError
     */
    public function customEmail(RequestJson $json): ?string;

    /**
     * Нашли дешевле
     * @param RequestJson $json
     * @return string|null
     */
    public function foundCheaper(RequestJson $json): ?string;

    /**
     * Не нашли нужного
     * @param RequestJson $json
     * @return string|null
     */
    public function notFoundNeeded(RequestJson $json): ?string;

    /**
     * Подписка на рассылку
     * @param RequestJson $json
     * @return string|null
     *
     * @throws SyntaxError
     * @throws TransportExceptionInterface
     * @throws RuntimeError
     * @throws LoaderError
     */
    public function subscribe(RequestJson $json): ?string;

    /**
     * Отписаться от рассылки
     * @param RequestJson $json
     * @return string|null
     *
     * @throws SyntaxError
     * @throws TransportExceptionInterface
     * @throws RuntimeError
     * @throws LoaderError
     */
    public function unsubscribe(RequestJson $json): ?string;
    /**
     * Список вопрос-ответ
     * @return string
     */
    public function faq(): string;

    /**
     * Заявка в отдел коммерции
     * @param RequestJson $json
     * @return string|null
     */
    public function commercialDepartment(RequestJson $json): ?string;
}