<?php

namespace App\Interfaces\Front;

use App\Entity\Faq;
use App\Entity\FaqType;
use App\Entity\Settings;
use App\RequestJson;

/**
 * Общие настройки
 */
interface SettingsInterface
{
    /**
     * Список настроек
     * @return Settings|null
     */
    public function getSettings(): ?Settings;
}