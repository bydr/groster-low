<?php

namespace App\Interfaces\Front;

use App\Entity\Product;
use App\RequestJson;

/**
 * Interface ProductInterface
 */
interface ProductInterface
{
    /**
     * Возвращает данные по товарам из списка
     * @param RequestJson $json
     * @return array
     */
    public function getByUuids(RequestJson $json): array;

    /**
     * Возвращает товар по uuid
     * @param string $uuid
     * @return Product|null
     */
    public function getProduct(string $uuid): ?Product;

    /**
     * Сообщить о поступлении товара
     *
     * @param string $uuid
     * @param RequestJson $json
     * @return string|null
     */
    public function waiting(string $uuid, RequestJson $json): ?string;

    /**
     * Обновление остатков у товаров через 1С
     * @param RequestJson $json
     * @return string
     */
    public function updateRest(RequestJson $json): ?string;
}