<?php

namespace App\Interfaces\Front;

use App\Entity\Order;
use App\RequestJson;

/**
 *
 */
interface PromocodeInterface
{
    /**
     * Применение промокода.
     * Возвращает массив с размером скидки и общей суммой заказа с учетом скидки
     * @param RequestJson $jsonReq
     * @param string|null $errorCode
     * @return Order|null
     */
    public function use(RequestJson $jsonReq, string &$errorCode = null): ?Order;

    /**
     * Отвязка промокода от заказа.
     * Возвращает массив с общей суммой заказа
     * @param RequestJson $jsonReq
     * @param string|null $errorCode
     * @return Order|null
     */
    public function delete(RequestJson $jsonReq, string &$errorCode = null): ?Order;
}