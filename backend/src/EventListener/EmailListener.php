<?php


namespace App\EventListener;

use App\Entity\Category;
use App\Entity\Order;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\KernelInterface;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;
use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;
use function PHPUnit\Framework\exactly;

/**
 * Отправка писем
 */
class EmailListener
{
    /** @var MailerInterface  */
    private $mailer;

    /** @var ContainerInterface  */
    private $ci;

    /** @var Environment */
    private $twig;

    /** @var bool */
    private $isTestEnv;

    /** @var string[]  */
    private $managers;

    public function __construct(ContainerInterface $ci, MailerInterface $mailer, Environment $twig, KernelInterface $kernel)
    {
        $this->mailer = $mailer;
        $this->ci = $ci;
        $this->twig = $twig;
        $this->isTestEnv = in_array($kernel->getEnvironment(), ['dev', 'test']);

        $this->managers = explode(",", $this->ci->getParameter('email_manager'));
    }

    /**
     * Отправка кода регистрации
     * @param string $email
     * @param string $code
     * @return void
     * @throws TransportExceptionInterface
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    public function sendCode(string $email, string $code)
    {
        if($this->isTestEnv) {
            return;
        }

        $message = (new Email())
            ->from($this->ci->getParameter('email_support'))
            ->to($email)
            ->subject("Код подтверждения")
            ->html(
                $this->twig->render(
                    'email/send_code.html.twig',
                    ['code' => $code]
                )
            );

        $this->mailer->send($message);
    }

    /**
     * @param Order $order
     * @return void
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     * @throws TransportExceptionInterface
     */
    public function fastOrderManagerEmail(Order $order)
    {
        if($this->isTestEnv) {
            return;
        }

        $body = $this->twig->render(
            'email/fastOrder/manager.html.twig',
            ['order' => $order]
        );

        $this->sendManagerEmail("Новый быстрый заказ", $body);
    }

    /**
     * @param Order $order
     * @return void
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     * @throws TransportExceptionInterface
     */
    public function fastOrderClientEmail(Order $order)
    {
        if($this->isTestEnv) {
            return;
        }

        $clientMessage= (new Email())
            ->from($this->ci->getParameter('email_support'))
            ->to($order->getEmail())
            ->subject("Подтверждение быстрого заказа")
            ->html(
                $this->twig->render(
                    'email/fastOrder/client.html.twig',
                    ['order' => $order]
                )
            );
        $this->mailer->send($clientMessage);
    }

    /**
     * @param Order $order
     * @return void
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     * @throws TransportExceptionInterface
     */
    public function newOrderManagerEmail(Order $order)
    {
        if($this->isTestEnv) {
            return;
        }

        $body = $this->twig->render(
            'email/newOrder/manager.html.twig',
            ['order' => $order]
        );

        $this->sendManagerEmail("Новый заказ", $body);
    }

    /**
     * @param Order $order
     * @return void
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     * @throws TransportExceptionInterface
     */
    public function newOrderClientEmail(Order $order)
    {
        if($this->isTestEnv) {
            return;
        }

        $clientMessage= (new Email())
            ->from($this->ci->getParameter('email_support'))
            ->to($order->getEmail())
            ->subject("Подтверждение нового заказа")
            ->html(
                $this->twig->render(
                    'email/newOrder/client.html.twig',
                    ['order' => $order]
                )
            );
        $this->mailer->send($clientMessage);
    }

    /**
     * Уведомление о поступлении товара
     * @param string $email
     * @param array $waitings
     * @return void
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     * @throws TransportExceptionInterface
     */
    public function sendEmailNotification(string $email, array $waitings)
    {
        if($this->isTestEnv) {
            return;
        }

        $message = (new Email())
            ->from($this->ci->getParameter('email_support'))
            ->to($email)
            ->subject("Уведомление о поступлении товара")
            ->html(
                $this->twig->render(
                    'email/waiting_notification.html.twig',
                    ['waitings' => $waitings]
                )
            );

        $this->mailer->send($message);
    }

    /**
     * Новое сообщение из формы обратной связи
     * @param string $name
     * @param string $phone
     * @param string $email
     * @param string $message
     * @return void
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     * @throws TransportExceptionInterface
     */
    public function sendFeedback(string $name, string $phone, string $email, string $message)
    {
        if($this->isTestEnv) {
            return;
        }

        $body = $this->twig->render(
            'email/feedback.html.twig',
            [
                'name' => $name,
                'phone' => $phone,
                'email' => $email,
                'message' => $message
            ]
        );

        $this->sendManagerEmail("Новый вопрос из формы обратной связи", $body);
    }

    public function sendRecall(string $name, string $phone)
    {
        if($this->isTestEnv) {
            return;
        }

        $body = $this->twig->render(
            'email/recall.html.twig',
            [
                'name' => $name,
                'phone' => $phone,
            ]
        );

        $this->sendManagerEmail("Новый заказ звонка", $body);
    }

    public function sendCustomEmail(string $message)
    {
        if($this->isTestEnv) {
            return;
        }

        $body = $this->twig->render(
            'email/custom_email.html.twig',
            [
                'message' => $message,
            ]
        );

        $this->sendManagerEmail("Новый заказ звонка", $body);
    }

    public function sendFoundCheaper(string $phone, string $link, string $message)
    {
        if($this->isTestEnv) {
            return;
        }

        $body = $this->twig->render(
            'email/found_cheaper.html.twig',
            [
                'phone' => $phone,
                'link' => $link,
                'message' => $message,
            ]
        );

        $this->sendManagerEmail("Новое обращение", $body);

    }

    public function sendNotFoundNeeded(string $phone, string $message)
    {
        if($this->isTestEnv) {
            return;
        }

        $body = $this->twig->render(
            'email/not_found_needed.html.twig',
            [
                'phone' => $phone,
                'message' => $message,
            ]
        );

        $this->sendManagerEmail("Новое обращение", $body);

    }

    private function sendManagerEmail(string $subject, string $body)
    {
        foreach ($this->managers as $managerEmail) {
            $message = (new Email())
                ->from($this->ci->getParameter('email_support'))
                ->to($managerEmail)
                ->subject($subject)
                ->html($body);
            $this->mailer->send($message);
        }
    }

    /**
     * @param Order $order
     * @return void
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     * @throws TransportExceptionInterface
     */
    public function orderNotification(Order $order)
    {
        if($this->isTestEnv) {
            return;
        }

        $subject = "Напоминание о заказе";

        $body = $this->twig->render(
            'email/order_notification.html.twig',
            [
                'order' => $order,
            ]
        );

        $message = (new Email())
            ->from($this->ci->getParameter('email_support'))
            ->to($order->getEmail())
            ->subject($subject)
            ->html($body);

        $this->mailer->send($message);
    }

    public function sendPricelist($email, string $filepath)
    {
        if($this->isTestEnv) {
            return;
        }

        $message = (new Email())
            ->from($this->ci->getParameter('email_support'))
            ->to($email)
            ->subject("Прайслист")
            ->html($this->twig->render(
                'email/pricelist.html.twig',
                [
                    'pricelist' => $filepath,
                ]
            ));

        $this->mailer->send($message);
    }

    public function sendCommercialFeedback(string $category, string $brand, string $representation, string $status,
                                           string $fio, string $phone, string $email, string $info, string $socialBrand,
                                           string $site)
    {
        if($this->isTestEnv) {
            return;
        }

        $body = $this->twig->render(
            'email/commercial_feedback.html.twig',
            [
                'category' => $category,
                'brand' => $brand,
                'representation' => $representation,
                'status' => $status,
                'fio' => $fio,
                'phone' => $phone,
                'email' => $email,
                'info' => $info,
                'socialBrand' => $socialBrand,
                'site' => $site,
            ]
        );

        $this->sendManagerEmail("Новое коммерческое предложение", $body);
    }

    public function registration(string $email)
    {
        if($this->isTestEnv) {
            return;
        }

        $message = (new Email())
            ->from($this->ci->getParameter('email_support'))
            ->to($email)
            ->subject("Регистрация на сайте Гростер")
            ->html($this->twig->render("email/registration.html.twig"));

        $this->mailer->send($message);
    }
}
