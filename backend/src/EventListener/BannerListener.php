<?php

namespace App\EventListener;

use App\Helpers\BannerHelper;
use App\Service\Cache;
use Doctrine\ORM\EntityManagerInterface;

/**
 * Слушатели изменения баннеров
 */
class BannerListener
{
    /** @var Cache  */
    private $cache;

    /** @var EntityManagerInterface  */
    private $em;

    public function __construct(EntityManagerInterface $em, Cache $cache)
    {
        $this->em = $em;
        $this->cache = $cache;
    }
    public function postPersist()
    {
        BannerHelper::update($this->cache, $this->em);
    }

    public function postUpdate()
    {
        BannerHelper::update($this->cache, $this->em);
    }

    public function postRemove()
    {
        BannerHelper::update($this->cache, $this->em);
    }
}