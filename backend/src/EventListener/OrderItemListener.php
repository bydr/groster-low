<?php

namespace App\EventListener;

use App\Entity\Order;
use App\Entity\OrderItem;
use Doctrine\ORM\EntityManagerInterface;

/**
 * Слушатели изменения товара в корзине
 */
class OrderItemListener
{
    /** @var EntityManagerInterface  */
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @param OrderItem $orderItem
     * @return void
     */
    public function postPersist(OrderItem $orderItem)
    {
        $orderItem->updateTotal();
//        $this->updateTotal($orderItem->getOrder());
    }

    /**
     * @param OrderItem $orderItem
     * @return void
     */
    public function postUpdate(OrderItem $orderItem)
    {
        $orderItem->updateTotal();
//        $this->updateTotal($orderItem->getOrder());
    }

    private function updateTotal(Order $order): void
    {
        $total = 0;
        $discount = 0;
        foreach ($order->getItems() as $item) {
            $total += $item->getTotal();
            $discount += $item->getDiscount();
        }

        $order->setTotal($total)
        ->setDiscountValue($discount);

        $this->em->flush();
    }
}
