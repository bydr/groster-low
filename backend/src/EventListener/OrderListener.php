<?php

namespace App\EventListener;

use App\Entity\Category;
use App\Entity\Discount;
use App\Entity\Order;
use App\Helpers\CategoryHelper;
use App\Service\Cache;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\Event\LifecycleEventArgs;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Mailer\MailerInterface;
use Twig\Environment;

/**
 * Слушатели изменения корзины
 */
class OrderListener
{
    /** @var EntityManagerInterface  */
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @param Order $order
     * @return void
     */
    public function postPersist(Order $order)
    {
        if(Order::STATE_CART != $order->getState()) {
            return;
        }

//        $this->updateTotal($order);
    }

    /**
     * @param Order $order
     * @return void
     */
    public function preUpdate(Order $order)
    {
        if(Order::STATE_CART != $order->getState()) {
            return;
        }

//        $order->setUpdateAt(new DateTime());
    }

    /**
     * @param Order $order
     * @return void
     */
    public function postUpdate(Order $order)
    {
        if(Order::STATE_CART != $order->getState()) {
            return;
        }

//        $this->updateTotal($order);
    }

    /**
     * Обновление скидки
     *
     * @param Order $order
     * @return void
     */
    private function updateTotal(Order $order): void
    {
        $promoCategories = [];  // Список id категорий, к которым привязан промокод
        $promoDiscount = 0;     // Скидка по промокоду
        $totalDiscount = 0;     // Общая сумма скидки
        $itemsTotal = 0;        // Общая сумма заказа без учета скидки

        /** @var Discount|null $promo */
        $promo = $order->getDiscount();
        if($promo) {
            $promoCategories = $promo->getCategoryIds();
            $promoDiscount = $promo->getValue();
        }

        foreach ($order->getItems() as $item) {
            $itemsTotal += $item->getTotal();
            foreach ($item->getProduct()->getCategoriesUuid() as $category) {
                if(in_array($category->getId(), $promoCategories)) {
                    $totalDiscount += intval($item->getTotal() * $promoDiscount / 100);
                    break;
                }
            }
        }

        // Если тип скидки в рублях, то общую скидку обновляем
        if($totalDiscount > 0 && Discount::TYPE_MONEY == $promo->getType()) {
            $totalDiscount = $promoDiscount;
        }

        if($totalDiscount > $itemsTotal) {
            $totalDiscount = $itemsTotal;
        }

        if($promo) {
            $order->setDiscountValue($totalDiscount);
            $itemsTotal -= $totalDiscount;
        }

        $order->setTotal($itemsTotal);

        $this->em->flush();
    }
}