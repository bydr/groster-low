<?php

namespace App\EventListener;

use App\Entity\Category;
use App\Helpers\CategoryHelper;
use App\Service\Cache;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\Event\LifecycleEventArgs;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Mailer\MailerInterface;
use Twig\Environment;

/**
 * Слушатели изменения категории
 */
class CategoryListener
{
    /** @var Cache  */
    private $cache;

    /** @var EntityManagerInterface  */
    private $em;

    public function __construct(EntityManagerInterface $em, Cache $cache)
    {
        $this->em = $em;
        $this->cache = $cache;
    }
    public function postPersist()
    {
        CategoryHelper::updateList($this->cache, $this->em);
    }
    public function postUpdate()
    {
        CategoryHelper::updateList($this->cache, $this->em);
    }
}