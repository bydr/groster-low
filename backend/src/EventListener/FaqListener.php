<?php

namespace App\EventListener;

use App\Entity\Category;
use App\Helpers\CategoryHelper;
use App\Helpers\FaqHelper;
use App\Service\Cache;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\Event\LifecycleEventArgs;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Mailer\MailerInterface;
use Twig\Environment;

/**
 * Слушатели изменения вопрос-ответ
 */
class FaqListener
{
    /** @var Cache  */
    private $cache;

    /** @var EntityManagerInterface  */
    private $em;

    public function __construct(EntityManagerInterface $em, Cache $cache)
    {
        $this->em = $em;
        $this->cache = $cache;
    }
    public function postPersist()
    {
        FaqHelper::updateFaq($this->cache, $this->em);
    }

    public function postUpdate()
    {
        FaqHelper::updateFaq($this->cache, $this->em);
    }

    public function postRemove()
    {
        FaqHelper::updateFaq($this->cache, $this->em);
    }
}