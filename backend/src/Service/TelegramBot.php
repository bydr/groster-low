<?php


namespace App\Service;


use Exception;
use GuzzleHttp\Client;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\Response;
/**
 * Сервис для отправки уведомлений в телеграм-бот
 */
class TelegramBot
{
    private $url;
    private $chats;
    /**
     * @var LoggerInterface
     */
    private $logger;

    const PROJECT = 'Гростер';

    /**
     * TelegramBot constructor.
     */
    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
        $botToken = $_ENV["BOT_TOKEN"];
        $chats = $_ENV["BOT_CHATS"];
        $this->url = "https://api.telegram.org/bot{$botToken}/";
        $this->chats = explode(",", $chats);

    }

    public function sendMessage($message)
    {

        foreach ($this->chats as $chat) {
            $params = [
                'chat_id' => $chat,
                'text' => self::PROJECT . "=> " . $message,
            ];
            $url = $this->url . "sendMessage?" . http_build_query($params);
            $client = new Client();

            try {
                $response = $client->request(
                    'GET',
                    $url
                );

            if ($response->getStatusCode() !== Response::HTTP_OK) {
                $this->logger->error("Ошибка отправки сообщения в TelegramBot");
            }
        }catch (Exception $e) {
                $this->logger->error($e->getMessage());
            }
        }

    }

}
