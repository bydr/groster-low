<?php

namespace App\Service;

use App\Controller\ApiController;
use App\Helpers\Params;
use Redis;
use Symfony\Component\Cache\Adapter\RedisAdapter;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\KernelInterface;

/**
 * Class Cache
 * Кеш, основанный на Redis
 */
class Cache
{
    const CODE_LIFETIME = 120;
    const DEFAULT_LIFETIME = 60 * 60 * 24 * 7;
    const KEY_PARAMS_WITH_PRODUCTS = 'paramsProducts';
    const PREFIX_KEY_CODE = 'code';
    const PREFIX_KEY_CATEGORIES = 'categories';
    const PREFIX_KEY_BANNERS = 'banners';
    const FAQ_LIFETIME = 60 * 60 * 24 * 30;
    const PREFIX_KEY_FAQ = 'faq';
    const PREFIX_KEY_PARAMS = 'params';
    const PREFIX_KEY_PRODUCTS = 'products';
    const PREFIX_KEY_HITS = "hits";
    const PREFIX_KEY_PRICELIST = "pricelist";

    const PRODUCTS_LIFETIME = 60 * 60 * 12;
    const PRICELIST_LIFETIME = 60 * 15;
    const PARAMS_LIFETIME = 60 * 15;

    /** @var Redis */
    private static $_cache;
    private $isTest;

    public function __construct(KernelInterface $kernel, ContainerInterface $container)
    {
        $this->isTest = 'test' == $kernel->getEnvironment();
        if (is_null(self::$_cache)) {
            $url = $container->getParameter('redis_url');
            self::$_cache = RedisAdapter::createConnection($url, [
                'class' => Redis::class,
            ]);
        }
    }

    /**
     * Сохраняет код подтверждения авторизации по email или телефону
     *
     * @param string $key - поле для которого сохраняем код (email или телефон)
     * @param string $code - код проверки
     * @return bool
     */
    public function saveCode(string $key, string $code): bool
    {
        if ($this->isTest) {
            return true;
        }

        return self::$_cache->set(self::PREFIX_KEY_CODE . $key, $code, ['EX' => self::CODE_LIFETIME]);
    }

    /**
     * Возвращает код подтверждения авторизации по email или телефону
     * @param string $key
     * @return int
     */
    public function getCode(string $key): int
    {
        if ($this->isTest) {
            return 1111;
        }

        return (int)self::$_cache->get(self::PREFIX_KEY_CODE . $key);
    }

    /**
     * Сохраняем список категорий в кеш на 7 дней
     * @param string $categories
     * @param string $uuid - uuid категории
     * @return bool
     */
    public function saveCategoriesList(string $categories, string $uuid): bool
    {
        return self::$_cache->set(self::PREFIX_KEY_CATEGORIES . ":" . $uuid, $categories, ['ex' => self::DEFAULT_LIFETIME]);
    }

    /**
     * Получить список дочерних категорий из кеша
     * @param string $uuid - uuid категории
     * @return string
     */
    public function getCategoriesList(string $uuid): string
    {
        return self::$_cache->get(self::PREFIX_KEY_CATEGORIES . ":" . $uuid);
    }

    /**
     * Удаляет список категорий по сфере бизнеса
     * @param string $uuid - uuid категории
     * @return void
     */
    public function removeCategoriesList(string $uuid): void
    {
        self::$_cache->del(self::PREFIX_KEY_CATEGORIES . ":" . $uuid);
    }

    /**
     * Возвращает список блоков вопрос-ответ
     * @return string
     */
    public function getFaqList(): string
    {
        return self::$_cache->get(self::PREFIX_KEY_FAQ);
    }

    /**
     * Сохранение списка вопрос-ответ
     * @param string $json
     * @return bool
     */
    public function saveFaqList(string $json): bool
    {
        return self::$_cache->set(self::PREFIX_KEY_FAQ, $json, ['ex' => self::FAQ_LIFETIME]);
    }

    /**
     * Список сфер бизнеса клиента
     * @param int $userId
     * @return false|mixed|string
     */
    public function getCustomerAreaList(int $userId): string
    {
        if ($this->isTest) {
            return "";
        }

        $key = sprintf("customer:%d:areas", $userId);
        return self::$_cache->get($key);
    }

    /**
     * Список сфер бизнеса клиента
     * @param int $userId
     * @param string $areas
     * @return false|mixed|string
     */
    public function saveCustomerAreaList(int $userId, string $areas): bool
    {
        if ($this->isTest) {
            return true;
        }

        $key = sprintf("customer:%d:areas", $userId);
        return self::$_cache->set($key, $areas, ['ex' => self::DEFAULT_LIFETIME]);

    }

    /**
     * Список сфер бизнеса клиента
     * @param int $userId
     * @return false|mixed|string
     */
    public function getCustomerEmailList(int $userId): string
    {
        if ($this->isTest) {
            return "";
        }

        $key = sprintf("customer:%d:emails", $userId);
        return self::$_cache->get($key);
    }

    /**
     * Список сфер бизнеса клиента
     * @param int $userId
     * @param string $emails
     * @return false|mixed|string
     */
    public function saveCustomerEmailList(int $userId, string $emails): bool
    {
        if ($this->isTest) {
            return true;
        }

        $key = sprintf("customer:%d:emails", $userId);
        return self::$_cache->set($key, $emails, ['ex' => self::DEFAULT_LIFETIME]);
    }

    /**
     * Список сфер бизнеса клиента
     * @param int $userId
     * @return false|mixed|string
     */
    public function getCustomerPhoneList(int $userId): string
    {
        if ($this->isTest) {
            return "";
        }

        $key = sprintf("customer:%d:phones", $userId);
        return self::$_cache->get($key);
    }

    /**
     * Список сфер бизнеса клиента
     * @param int $userId
     * @param string $phones
     * @return false|mixed|string
     */
    public function saveCustomerPhoneList(int $userId, string $phones): bool
    {
        if ($this->isTest) {
            return true;
        }

        $key = sprintf("customer:%d:phones", $userId);
        return self::$_cache->set($key, $phones, ['ex' => self::DEFAULT_LIFETIME]);
    }

    /**
     * Возвращает список баннеров
     * @return string
     */
    public function getBanners(): string
    {
        return self::$_cache->get(self::PREFIX_KEY_BANNERS);
    }

    /**
     * Сохраняем список баннеров в кеш на 7 дней
     * @param string banners
     * @return bool
     */
    public function saveBanners(string $banners): bool
    {
        return self::$_cache->set(self::PREFIX_KEY_BANNERS, $banners, ['ex' => self::DEFAULT_LIFETIME]);
    }

    /**
     * Возвращает список параметров
     * @return string
     */
    public function getParams(): string
    {
        if ($this->isTest) {
            return '';
        }

        return self::$_cache->get(self::PREFIX_KEY_PARAMS);
    }

    /**
     * Сохранение сгруппированных параметров
     * @param string $params
     * @return bool
     */
    public function setParams(string $params): bool
    {
        return self::$_cache->set(self::PREFIX_KEY_PARAMS, $params, ['ex' => self::DEFAULT_LIFETIME]);
    }

    /**
     * Удаляет сгруппированные параметры
     * @return void
     * @throws \RedisException
     */
    public function removeParams(): void
    {
        self::$_cache->del(self::PREFIX_KEY_PARAMS);
    }

    /**
     * Получение отфильтрованных товаров каталога
     * @param string $hash
     * @return string
     */
    public function getFilteredProducts(string $hash): string
    {
        $key = sprintf("%s:%s", self::PREFIX_KEY_PRODUCTS, $hash);
        return self::$_cache->get($key);
    }

    /**
     * Сохранение отфильтрованных товаров
     * @param string $hash
     * @param string $data
     * @return bool
     */
    public function setFilteredProducts(string $hash, string $data): bool
    {
        $key = sprintf("%s:%s", self::PREFIX_KEY_PRODUCTS, $hash);
        return self::$_cache->set($key, $data, ['ex' => self::PRODUCTS_LIFETIME]);
    }

    /**
     * Получение хитов
     * @return string
     */
    public function getHits(): string
    {
        return self::$_cache->get(self::PREFIX_KEY_HITS);
    }

    /**
     * Сохранение хитов
     * @param string $data
     * @return bool
     */
    public function saveHits(string $data): bool
    {
        return self::$_cache->set(self::PREFIX_KEY_HITS, $data, ['ex' => self::PRODUCTS_LIFETIME]);
    }

    /**
     * Удаляет список хитов
     * @return void
     */
    public function removeHits(): void
    {
        self::$_cache->del(self::PREFIX_KEY_HITS);
    }

    /**
     * Создает отложенную задачу на отправку прайслиста на почту
     * @param string $email
     * @param array $categories
     * @return bool
     * @throws \RedisException
     */
    public function setPricelistQuery(string $email, array $categories): bool
    {
        $data = [
            Params::EMAIL => $email,
            Params::CATEGORIES => $categories,
        ];

        $key = sprintf("%s:%s", self::PREFIX_KEY_PRICELIST, $email);

        return self::$_cache->set($key, json_encode($data), ['ex' => self::PRICELIST_LIFETIME]);
    }

    /**
     * Список ключей задач для отправки прайслистов
     * @return array
     * @throws \RedisException
     */
    public function getPricelistQuiries(): array
    {
        $pattern = sprintf("%s*", self::PREFIX_KEY_PRICELIST);
        return self::$_cache->keys($pattern);
    }

    /**
     * Список ключей задач для отправки прайслистов
     * @param string $key
     * @return array
     * @throws \RedisException
     */
    public function getPricelistQuiryByKey(string $key): array
    {
         $data = self::$_cache->get($key);
        self::$_cache->del($key);

        return json_decode($data, true);
    }

    /**
     * Возвращает из кеша список параметров с остатками товаров
     * @return string
     * @throws \RedisException
     */
    public function getParamsWithProductQty(): string
    {
        return self::$_cache->get(self::KEY_PARAMS_WITH_PRODUCTS);
    }

    /**
     * Кеширование списка параметров с остатками товаров
     * @param string $params
     * @return bool|Redis
     * @throws \RedisException
     */
    public function saveParamsWithProductQty(string $params)
    {
        return self::$_cache->set(self::KEY_PARAMS_WITH_PRODUCTS, $params, ['ex' => self::PARAMS_LIFETIME]);
    }
}
