<?php

namespace App\Service;

use App\Controller\ApiController;
use App\Entity\Category;
use App\Entity\CustomRequest;
use App\Entity\Order;
use App\Entity\OrderItem;
use App\Entity\Payer;
use App\Entity\Security\AuthUser;
use App\Entity\ShippingAddress;
use App\Helpers\MoneyHelper;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use RetailCrm\Api\Factory\SimpleClientFactory;
use RetailCrm\Api\Model\Entity\Customers\Customer;
use RetailCrm\Api\Model\Entity\Customers\CustomerPhone;
use RetailCrm\Api\Model\Entity\Orders\Delivery\DeliveryData;
use RetailCrm\Api\Model\Entity\Orders\Delivery\OrderDeliveryAddress;
use RetailCrm\Api\Model\Entity\Orders\Delivery\SerializedDeliveryService;
use RetailCrm\Api\Model\Entity\Orders\Delivery\SerializedOrderDelivery;
use RetailCrm\Api\Model\Entity\Orders\Items\OrderProduct;
use RetailCrm\Api\Model\Entity\Orders\OrderContragent;
use RetailCrm\Api\Model\Entity\Tasks\Task;
use RetailCrm\Api\Model\Filter\Customers\CustomerFilter;
use RetailCrm\Api\Model\Request\BySiteRequest;
use RetailCrm\Api\Model\Request\Customers\CustomersCreateRequest;
use RetailCrm\Api\Model\Request\Customers\CustomersEditRequest;
use RetailCrm\Api\Model\Request\Customers\CustomersRequest;
use RetailCrm\Api\Model\Request\Orders\OrdersCreateRequest;
use RetailCrm\Api\Model\Request\Orders\OrdersEditRequest;
use RetailCrm\Api\Model\Request\Orders\OrdersRequest;
use RetailCrm\Api\Model\Request\Tasks\TasksCreateRequest;
use Symfony\Component\DependencyInjection\ContainerInterface;
use \RetailCrm\Api\Exception\Api\AccountDoesNotExistException;
use \RetailCrm\Api\Exception\Api\ApiErrorException;
use \RetailCrm\Api\Exception\Api\MissingCredentialsException;
use \RetailCrm\Api\Exception\Api\MissingParameterException;
use \RetailCrm\Api\Exception\Api\ValidationException;
use \RetailCrm\Api\Exception\Client\HandlerException;
use \RetailCrm\Api\Exception\Client\HttpClientException;
use \RetailCrm\Api\Interfaces\ApiExceptionInterface;
use \RetailCrm\Api\Interfaces\ClientExceptionInterface;
use \RetailCrm\Api\Model\Entity\Orders\Order as CrmOrder;
use Symfony\Component\HttpKernel\KernelInterface;

/**
 * Связь с RetailCRM
 */
class RetailCRM
{
    const BY_ID = 'id';
    const BY_EXTERNAL_ID = 'externalId';

    const ORDER_METHOD_FAST = 'fast';
    const ORDER_METHOD_CART = 'cart';

    const LOG_PREFIX = 'RetailCRM';
    const SITE = 'groster'; // символьный код магазина
    const COMMENT_SAMPLE = 'образец';

    private $client;

    private $em;

    private $isTestEnv;
    private $logger;

    public function __construct(ContainerInterface $ci, EntityManagerInterface $em, KernelInterface $kernel)
    {
        $url = $ci->getParameter('retailcrm_url');
        $apiKey = $ci->getParameter('retailcrm_apikey');
        $this->client = SimpleClientFactory::createClient($url, $apiKey);
        $this->em = $em;

        $this->isTestEnv = 'test' == $kernel->getEnvironment();
        $this->logger = ApiController::getLogger();
        $this->managerId = $ci->getParameter('retailcrm_managerid');
    }

    /**
     * Подписка на рассылку
     * @param $email
     * @return bool
     * @throws AccountDoesNotExistException
     * @throws ApiErrorException
     * @throws MissingCredentialsException
     * @throws MissingParameterException
     * @throws ValidationException
     * @throws HandlerException
     * @throws HttpClientException
     * @throws ApiExceptionInterface
     * @throws ClientExceptionInterface
     */
    public function subscribe($email): bool
    {
        if($this->isTestEnv) {
            return true;
        }

        $customer = $this->getCustomerByEmail($email);

        if(is_null($customer)) {
            $customer = new Customer();
            $customer->email = $email;
            /** @var AuthUser $user */
            if($user = $this->em->getRepository(AuthUser::class)->findOneByEmail($email)) {
                $customer->externalId = $user->getId();
            }
        }

        $customer->subscribed = true;
        $customer->emailMarketingUnsubscribedAt = null;
        $this->logger->info(sprintf("%s: новая подписка на email %s", self::LOG_PREFIX, $email));

        if($customer->id) {
            $request = new CustomersEditRequest();
            $request->customer = $customer;
            $request->by = self::BY_ID;
            $response = $this->client->customers->edit($customer->id, $request);
        }else{
            $request = new CustomersCreateRequest();
            $request->customer = $customer;
            $response = $this->client->customers->create($request);
        }

        return $response->success;

    }

    /**
     * Отписка от рассылки
     * @param $email
     * @return bool
     * @throws AccountDoesNotExistException
     * @throws ApiErrorException
     * @throws MissingCredentialsException
     * @throws MissingParameterException
     * @throws ValidationException
     * @throws HandlerException
     * @throws HttpClientException
     * @throws ApiExceptionInterface
     * @throws ClientExceptionInterface
     */
    public function unsubscribe($email): bool
    {
        if($this->isTestEnv) {
            return true;
        }

        $customer = $this->getCustomerByEmail($email);

        if(is_null($customer)) {
            return true;
        }

        $this->logger->info(sprintf("%s: отписка для email %s", self::LOG_PREFIX, $email));

        $customer->subscribed = false;
        $request = new CustomersEditRequest();
        $request->customer = $customer;
        $request->by = self::BY_ID;
        $response = $this->client->customers->edit($customer->id, $request);

        return $response->success;
    }

    /**
     * Возвращает клиента по email или null
     *
     * @param string $email
     * @return Customer|null
     *
     * @throws AccountDoesNotExistException
     * @throws ApiErrorException
     * @throws ApiExceptionInterface
     * @throws ClientExceptionInterface
     * @throws HandlerException
     * @throws HttpClientException
     * @throws MissingCredentialsException
     * @throws MissingParameterException
     * @throws ValidationException
     */
    private function getCustomerByEmail(string $email): ?Customer
    {
        $filter = new CustomerFilter();
        $filter->email = $email;


        $customers = $this->findCustomers($filter);
        if(0 == count($customers)) {
            return null;
        }

        if(count($customers) > 1) {
            throw new HandlerException(sprintf("RetailCRM: найдено %d клиентов с email %s", count($customers), $email));
        }

        return $customers[0];
    }

    /**
     * Возвращает клиента по телефону или null
     *
     * @param string $phone
     * @return Customer|null
     *
     * @throws AccountDoesNotExistException
     * @throws ApiErrorException
     * @throws ApiExceptionInterface
     * @throws ClientExceptionInterface
     * @throws HandlerException
     * @throws HttpClientException
     * @throws MissingCredentialsException
     * @throws MissingParameterException
     * @throws ValidationException
     */
    private function getCustomerByPhone(string $phone): ?Customer
    {
        $filter = new CustomerFilter();
        $filter->name = $phone;


        $customers = $this->findCustomers($filter);
        if(0 == count($customers)) {
            return null;
        }

        if(count($customers) > 1) {
            $this->logger->warning(sprintf("%s: найдено %d клиентов с телефоном %s", self::LOG_PREFIX, count($customers), $phone));
        }

        return $customers[0];
    }

    /**
     * Поиск пользователя по id
     * @param int $id
     * @return Customer|null
     * @throws AccountDoesNotExistException
     * @throws ApiErrorException
     * @throws ApiExceptionInterface
     * @throws ClientExceptionInterface
     * @throws HandlerException
     * @throws HttpClientException
     * @throws MissingCredentialsException
     * @throws MissingParameterException
     * @throws ValidationException
     */
    private function getCustomerById(int $id): ?Customer
    {
        $filter = new CustomerFilter();
        $filter->externalIds = [$id];


        $customers = $this->findCustomers($filter);
        if(0 == count($customers)) {
            $this->logger->info(sprintf("%s: не найдено клиентов с id %s", self::LOG_PREFIX, $id));
            return null;
        }

        if(count($customers) > 1) {
            $this->logger->warning(sprintf("%s: найдено %d клиентов с id %s", self::LOG_PREFIX, count($customers), $id));
        }

        return $customers[0];
    }


    /**
     * Поиск клиента в CRM
     * @param CustomerFilter $filter
     * @return array
     *
     * @throws AccountDoesNotExistException
     * @throws ApiErrorException
     * @throws ApiExceptionInterface
     * @throws ClientExceptionInterface
     * @throws HandlerException
     * @throws HttpClientException
     * @throws MissingCredentialsException
     * @throws MissingParameterException
     * @throws ValidationException
     */
    private function findCustomers(CustomerFilter $filter): array
    {
        $request = new CustomersRequest();
        $request->filter = $filter;

        $response = $this->client->customers->list($request);
        if(!$response->success) {
            throw new HandlerException("Пользователь с искомыми данными не найден");
        }

        return $response->customers;
    }

    /**
     * @param AuthUser $user
     * @return int
     * @throws ApiExceptionInterface
     * @throws ClientExceptionInterface
     */
    public function createCustomer(AuthUser $user): int
    {
        if($this->isTestEnv) {
            return 0;
        }

        $this->logger->info(sprintf("%s: создание нового клиента с externalId %d", self::LOG_PREFIX, $user->getId()));
        try {
            if($id = $user->getId()) {
                $customer = $this->getCustomerById($id);
            }

            if(empty($customer) && $user->getPhone()) {
                $customer = $this->getCustomerByPhone($user->getPhone());
                if(empty($customer)) {
                    if($user->getEmail()) {
                        $customer = $this->getCustomerByEmail($user->getEmail());
                    }

                    if(empty($customer)) {
                        $customer = new Customer();
                        if($phone = $user->getPhone()) {
                            $cusPhone = new CustomerPhone();
                            $cusPhone->number = $phone;
                            $customer->phones = [$cusPhone];
                        }
                    }
                }
            }

            if(empty($customer)) {
                $customer = $this->getCustomerByEmail($user->getEmail());
            }

            if(empty($customer)) {
                $customer = new Customer();
                $customer->email = $user->getEmail();
                if($phone = $user->getPhone()) {
                    $cusPhone = new CustomerPhone();
                    $cusPhone->number = $phone;
                    $customer->phones = [$cusPhone];
                }
            }

            $customer->externalId = $user->getId();
            $customer->subscribed = true;
            $customer->emailMarketingUnsubscribedAt = null;

            $customer->createdAt = $user->getCreateAt();

            $request = new CustomersCreateRequest();
            $request->customer = $customer;
            $response = $this->client->customers->create($request);
            if(!$response->success) {
                throw new Exception("Ошибка записи в CRM");
            }

            return $response->id;
        }catch (Exception $e) {
            $this->logger->error(sprintf("Ошибка создания клиента %d в CRM: %s", $user->getId(), $e->getMessage()));
        }

        return 0;
    }

    /**
     * Создание задачи для заказа обратного звонка. Возвращает ID задачи или 0
     * @param CustomRequest $recall
     * @return int
     * @throws ApiExceptionInterface
     * @throws ClientExceptionInterface
     */
    public function recall(CustomRequest $recall): int
    {
        if($this->isTestEnv) {
            return 0;
        }

        $this->logger->info(sprintf("%s: создание нового запроса обратного звонка на имя %s и телефон %s",
            self::LOG_PREFIX, $recall->getName(), $recall->getPhone()));
        try {
            $customer = $this->getCustomerByPhone($recall->getPhone());

            // Создание нового клиента
            if(is_null($customer)) {
                $customer = new Customer();
                $customer->phones = [new CustomerPhone($recall->getPhone())];
                $customer->firstName = $recall->getName();
                /** @var AuthUser $user */
                if($user = $this->em->getRepository(AuthUser::class)->findOneByPhone($recall->getPhone())) {
                    $customer->externalId = $user->getId();
                }

                $request = new CustomersCreateRequest();
                $request->customer = $customer;
                $response = $this->client->customers->create($request);
                if(!$response->success) {
                    throw new HandlerException("Не удалось создать пользователя для задачи обратного звонка");
                }

                $customer->id = $response->id;
            }

            $typeTitle = CustomRequest::typeTitle($recall->getType());
            $task = new Task();
            $task->customer = $customer;
            $task->phone = $recall->getPhone();
            $task->text = $typeTitle;
            $task->performerId = $this->managerId;
            $task->commentary = sprintf('%s:имя: %sтелефон: %s', $typeTitle, $recall->getName(), $recall->getPhone());

            $request = new TasksCreateRequest();
            $request->task = $task;
            $request->site = self::SITE;

            $response = $this->client->tasks->create($request);
            if(!$response->success) {
                throw new HandlerException("Не удалось создать задачу для обратного звонка");
            }

            return $response->id;
        }catch (Exception $e) {
            $this->logger->error(sprintf("%s: ошибка создания заявки на обратный звонок: %s",
                self::LOG_PREFIX, $e->getMessage()));
        }

        return 0;
    }

    /**
     * Сохранение заказа. Возвращает ID заказа в CRM или 0
     * @param Order $cart
     * @param bool $notCall
     * @return int
     * @throws ApiExceptionInterface
     * @throws ClientExceptionInterface
     */
    public function saveOrder(Order $cart, bool $notCall): int
    {
        if($this->isTestEnv) {
            return 0;
        }

        $this->logger->info(sprintf("%s: сохранение нового заказа #%d", self::LOG_PREFIX, $cart->getId()));

        try {
            $order = $this->prepareCrmOrder($cart, $notCall);

            $request = new OrdersCreateRequest();
            $request->order = $order;

            $response = $this->client->orders->create($request);
            if(!$response->success) {
                throw new HandlerException(sprintf("Не удалось создать заказа для внутреннего заказа %d", $cart->getId()));
            }

            $this->logger->info(sprintf("%s: заказ #%d успешно сохранен", self::LOG_PREFIX, $cart->getId()));
            return $response->id;
        } catch (Exception $e) {
            $this->logger->error(sprintf("%s: ошибка сохранения заказа: %s в %s(%d)", self::LOG_PREFIX, $e->getMessage(), $e->getFile(), $e->getLine()));
        }

        return 0;
    }

    public function cancelOrder(Order $cart)
    {
        if($this->isTestEnv) {
            return 0;
        }

        $this->logger->info(sprintf("%s: отмена заказа #%d", self::LOG_PREFIX, $cart->getId()));

        try {
            $order = $this->findOrder($cart);
            if (empty($order)) {
                throw new HandlerException(sprintf("Запрос в CRM не вернул заказ с uid %s", $cart->getUid()));
            }

            for ($i = 0; $i < count($order->items); $i++) {
                $order->items[$i]->externalIds = null;
            }

            $order->status = 'cancel-other';

            $request = new OrdersEditRequest();
            $request->order = $order;
            $request->by = self::BY_ID;

            $response = $this->client->orders->edit($cart->getCrmId(), $request);

            if(!$response->success) {
                throw new HandlerException(sprintf("Не удалось отменить заказ #%d", $cart->getId()));
            }

            $this->logger->info(sprintf("%s: заказ #%d успешно отменен", self::LOG_PREFIX, $cart->getId()));

            return $response->id;
        } catch (Exception $e) {
            $this->logger->error(sprintf("%s: ошибка отмены заказа: %s в %s(%d)", self::LOG_PREFIX, $e->getMessage(), $e->getFile(), $e->getLine()));
        }

        return 0;
    }

    /**
     * Get order by uid(externalId)
     * @param Order $cart
     * @return CrmOrder
     * @throws AccountDoesNotExistException
     * @throws ApiErrorException
     * @throws ApiExceptionInterface
     * @throws ClientExceptionInterface
     * @throws HandlerException
     * @throws HttpClientException
     * @throws MissingCredentialsException
     * @throws MissingParameterException
     * @throws ValidationException
     */
    private function findOrder(Order $cart): CrmOrder
    {
        $request = new BySiteRequest();
        $request->by = self::BY_ID;

        $response = $this->client->orders->get($cart->getCrmId(), $request);
        if(!$response->success) {
            throw new HandlerException(sprintf("Заказ с uuid %s не найден", $cart->getUid()));
        }

        return $response->order;
    }

    /**
     * Если это не быстрый заказ, то привязываем клиента
     * @param CrmOrder $order
     * @param Order $cart
     * @return void
     * @throws AccountDoesNotExistException
     * @throws ApiErrorException
     * @throws ApiExceptionInterface
     * @throws ClientExceptionInterface
     * @throws HandlerException
     * @throws HttpClientException
     * @throws MissingCredentialsException
     * @throws MissingParameterException
     * @throws ValidationException
     */
    private function setOrderCustomer(CrmOrder &$order, Order $cart)
    {
        $user = ApiController::getCurrentUser();
        if($cart->getIsFast()) {
            $userPhone = $cart->getPhone();
            $userEmail = $cart->getEmail();
            if (empty($user)) {
                $user = new AuthUser();
                $user->setPhone($userPhone);
                $user->setEmail($userEmail);
            }
        } else {
            $userPhone = $user->getPhone();
            $userEmail = $user->getEmail();
        }

        $customer = $userPhone ? $this->getCustomerByPhone($userPhone) : $this->getCustomerByEmail($userEmail);

        // Пытаемся создать клиента
        if(is_null($customer)) {
            if($this->createCustomer($user) > 0) {
                $customer = $userPhone ? $this->getCustomerByPhone($userPhone) : $this->getCustomerByEmail($userEmail);
            }
        }
        if(is_null($customer)) {
            throw new HandlerException(sprintf("не найден клиент для заказа %d", $cart->getId()));
        }

        $order->customer = $customer;
    }

    /**
     * Указание плательщика для заказа
     * @param Payer $payer
     * @return OrderContragent
     */
    private function getOrderContragent(Payer $payer): OrderContragent
    {
        $contragent = new OrderContragent();
        $contragent->legalName = implode(" ", [(string)$payer->getName(), (string)$payer->getContact()]);
        $contragent->INN = $payer->getInn();
        $contragent->BIK = $payer->getBik();
        $contragent->OGRNIP = $payer->getOgrnip();
        $contragent->bankAccount = $payer->getAccount();
        $contragent->KPP = $payer->getKpp();

        return $contragent;
    }

    /**
     * Добавление товара к заказу
     * @param CrmOrder $order
     * @param Collection|OrderItem[] $products
     * @return void
     */
    private function addItems(CrmOrder &$order, $products)
    {
        $items = [];
        /** @var OrderItem $cartItem */
        foreach ($products as $cartItem) {
            if($cartItem->getProduct()->isKit()) {
                continue;
            }
            $product = $cartItem->getProduct();
            $id = $product->getId();
            if(!isset($items[$id])) {
                $item = new OrderProduct();
                $item->order = $order;
                $item->externalId = $product->getUuid();
                $item->productName = $product->getName();
                $item->initialPrice = MoneyHelper::convertToRubles($product->getPrice());
                $items[$id] = $item;
            }

            if($sample = $cartItem->getSample()) {
                $items[$id]->comment = self::COMMENT_SAMPLE;
                $qty = $sample;
            } else {
                $qty = $cartItem->getQuantity();
            }

            $items[$id]->quantity += $qty;
        }

        $order->items = $items;
    }

    /**
     * Оформление доставки
     * @param CrmOrder $order
     * @param Order $cart
     */
    private function setDelivery(CrmOrder &$order, Order $cart)
    {
        $delivery = new SerializedOrderDelivery();
        $delivery->cost = MoneyHelper::convertToRubles($cart->getShippingCost());

        if($shippingDate = $cart->getShippingDate()) {
            $delivery->date = $shippingDate;
        }

        // Адрес доставки
        $deliveryAddress = new OrderDeliveryAddress();

        if($address = $cart->getShippingAddress()) {
            $deliveryAddress->text = $address;
        } else {
            $deliveryAddress->region = $cart->getRegion();
        }

        $delivery->address = $deliveryAddress;

        // Способ доставки
        $shippingMethod = $cart->getShippingMethod();
        $service = new SerializedDeliveryService();
        $service->active = true;
        $service->name = $shippingMethod->getName();
        $service->code = $shippingMethod->getAlias();
        $delivery->service = $service;

        $order->delivery = $delivery;
    }

    /**
     * Создание задачи нового обращения
     *
     * @param CustomRequest $customRequest
     * @return int
     * @throws ApiExceptionInterface
     * @throws ClientExceptionInterface
     */
    public function createTask(CustomRequest $customRequest): int
    {
        if($this->isTestEnv) {
            return 0;
        }

        $this->logger->info(sprintf("%s: создание нового запроса на телефон %s и сообщение %s",
            self::LOG_PREFIX, $customRequest->getPhone(), $customRequest->getMessage()));

        try {
            $customer = $this->getCustomerByPhone($customRequest->getPhone());

            // Создание нового клиента
            if(is_null($customer)) {
                $customer = new Customer();
                $customer->phones = [new CustomerPhone($customRequest->getPhone())];
                /** @var AuthUser $user */
                if($user = $this->em->getRepository(AuthUser::class)->findOneByPhone($customRequest->getPhone())) {
                    $customer->externalId = $user->getId();
                }

                $request = new CustomersCreateRequest();
                $request->customer = $customer;
                $response = $this->client->customers->create($request);
                if(!$response->success) {
                    throw new HandlerException("Не удалось создать пользователя для задачи обращения");
                }

                $customer->id = $response->id;
            }

            $typeTitle = CustomRequest::typeTitle($customRequest->getType());
            $task = new Task();
            $task->customer = $customer;
            $task->phone = $customRequest->getPhone();
            $task->text = $typeTitle;
            $task->performerId = $this->managerId;
            $task->commentary = sprintf("Телефон %s\nCообщение %s", $customRequest->getPhone(), $customRequest->getMessage());

            $request = new TasksCreateRequest();
            $request->task = $task;
            $request->site = self::SITE;

            $response = $this->client->tasks->create($request);
            if(!$response->success) {
                throw new HandlerException("Не удалось создать задачу для обращения");
            }

            return $response->id;
        }catch (Exception $e) {
            $this->logger->error(sprintf("%s: ошибка создания обращения: %s",
                self::LOG_PREFIX, $e->getMessage()));
        }

        return 0;
    }

    /**
     * Создание запроса нового коммерческого предложения
     *
     * @param CustomRequest $customRequest
     * @return int
     * @throws ApiExceptionInterface
     * @throws ClientExceptionInterface
     */
    public function commercialFeed(string $category, string $brand, string $representation, string $status,
                                   string $fio, string $phone, string $email, string $info, string $socialBrand,
                                   string $site): int
    {
        if($this->isTestEnv) {
            return 0;
        }

        $this->logger->info(sprintf("%s: новое коммерческое предложение от email %s и телефон %s",
            self::LOG_PREFIX, $email, $phone));

        try {
            $customer = $this->getCustomerByPhone($phone);

            // Создание нового клиента
            if(is_null($customer)) {
                $customer = new Customer();
                $customer->phones = [new CustomerPhone($phone)];
                /** @var AuthUser $user */
                if($user = $this->em->getRepository(AuthUser::class)->findOneByPhone($phone)) {
                    $customer->externalId = $user->getId();
                }

                $request = new CustomersCreateRequest();
                $request->customer = $customer;
                $response = $this->client->customers->create($request);
                if(!$response->success) {
                    throw new HandlerException("Не удалось создать пользователя для коммерческого предложения");
                }

                $customer->id = $response->id;
            }

            $typeTitle = 'Коммерческое предложение';
            $task = new Task();
            $task->customer = $customer;
            $task->phone = $phone;
            $task->text = $typeTitle;
            $task->performerId = $this->managerId;
            $task->commentary = sprintf("Категория: %s\nБренд: %s\nПредставленность в крупных сетях:  %s\nCтатус представителя: %s\nФИО: %s\nТелефон: %s\n>Email: %s\nБренд в соц. сетях: %s\nСайт компании: %s\nИнфо: %s", $category, $brand, $representation, $status, $fio, $phone, $email,
                $socialBrand, $site, $info);

            $request = new TasksCreateRequest();
            $request->task = $task;
            $request->site = self::SITE;

            $response = $this->client->tasks->create($request);
            if(!$response->success) {
                throw new HandlerException("Не удалось создать задачу для коммерческого предложения");
            }

            return $response->id;
        }catch (Exception $e) {
            $this->logger->error(sprintf("%s: ошибка создания коммерческого предложения: %s",
                self::LOG_PREFIX, $e->getMessage()));
        }

        return 0;
    }

    private function prepareCrmOrder(Order $cart, bool $notCall): CrmOrder
    {
        $order = new CrmOrder();
        $order->externalId = $cart->getUid();
        $order->phone = $cart->getPhone();
        $order->email = $cart->getEmail();
        $order->createdAt = $cart->getCheckoutCompleteAt();
        $order->firstName = $cart->getFio();

        $this->setOrderCustomer($order, $cart);


        if($cart->getIsFast()) {
            $order->orderMethod = self::ORDER_METHOD_FAST;

//            if($phone = $cart->getPhone()) {
//                $customer = $this->getCustomerByPhone($phone);
//            }elseif($email = $cart->getEmail()) {
//                $customer = $this->getCustomerByEmail($email);
//            }else{
//                $user = new AuthUser();
//                $user->setPhone($cart->getPhone());
//                $user->setEmail($cart->getEmail());
//                $customer = $this->createCustomer($user);
//            }
//
//            if(!empty($customer)) {
//                $order->customer = $customer;
//            }
        } else {
            $order->orderMethod = self::ORDER_METHOD_CART;

            if($payer = $cart->getPayer()) {
                $order->contragent = $this->getOrderContragent($payer);
                if($payer->getContactPhone()) {
                    $order->additionalPhone = $payer->getContactPhone();
                }
            }

            $this->setDelivery($order, $cart);

            if($cart->getReplacement()) {
                $order->customerComment = sprintf("%s\nСпособ замены товара: %s - %s.", $cart->getComment(),
                    $cart->getReplacement()->getTitle(), $cart->getReplacement()->getDescription()
                );
            }

        }

        if($discount = $cart->getDiscountValue()) {
            $order->discountManualAmount = MoneyHelper::convertToRubles($discount);
        }

        $this->addItems($order, $cart->getItems());
        $order->call = !$notCall;

        return $order;
    }
}