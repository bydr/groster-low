<?php

namespace App\Service\Sms;

use App\Controller\ApiController;
use Exception;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\KernelInterface;

/**
 * Класс для подготовки данных для отправки SMS через SMS.RU
 */
class Sms
{
    const DOMAIN = 'https://sms.ru';

    // Тестовое ли окружение запущено
    private $isTestEnv;

    /**
     * @var string
     */
    private $apiKey;

    /**
     * Номер телефона получателя (либо несколько номеров, через запятую — до 100 штук за один запрос).
     * Если вы указываете несколько номеров и один из них указан неверно,
     * то на остальные номера сообщения также не отправляются, и возвращается код ошибки.
     * @var string
     */
    private $phone;

    /**
     * Текст сообщения в кодировке UTF-8
     * @var string
     */
    private $message;

    /**
     * Имя отправителя (должно быть согласовано с администрацией).
     * Если не заполнено, в качестве отправителя будет указан ваш номер.
     * @var string
     */
    private $from;

    /**
     * Переводит все русские символы в латинские.
     * 0 - нет (по умолчанию)
     * 1 - да
     * @var int
     */
    private $isTranslit;

    /**
     * Имитирует отправку сообщения для тестирования ваших программ на правильность обработки ответов сервера.
     * При этом само сообщение не отправляется и баланс не расходуется
     * 0 - нет (по умолчанию)
     * 1 - да
     * @var int
     */
    private $isTest;

    public function __construct(ContainerInterface $ci, KernelInterface $kernel)
    {
        $this->apiKey = $ci->getParameter("sms.api");
        $this->isTest = (int)$ci->getParameter("sms.isTest");
        $this->from = $ci->getParameter("sms.from");
        $this->isTranslit = (int)$ci->getParameter("sms.isTranslit");
        $this->isTestEnv = 'test' == $kernel->getEnvironment();
    }

    /**
     * Отправка СМС
     * @param string $phone
     * @param string $msg
     * @return bool
     * @throws Exception
     */
    public function send(string $phone, string $msg): bool
    {
        $this->phone = $phone;
        $this->message = $msg;

        $infoMsg = sprintf("%s - отправка СМС %s", $this->phone, $this->message) . ($this->isTestEnv ? '(тест)' : '');
        ApiController::getLogger()->info($infoMsg);

        if($this->isTestEnv) {
            return true;
        }

        $url = self::DOMAIN . '/sms/send';
        $request = $this->Request($url);
        return $this->CheckReplyError($request);
    }

    /**
     * Отправка SMS
     * @param $url
     * @return bool|string
     * @throws Exception
     */
    private function Request($url)
    {
        $ch = curl_init($url . "?json=1");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
        curl_setopt($ch, CURLOPT_VERBOSE, 1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($this->createPost()));

        $body = curl_exec($ch);
        if ($body === FALSE) {
            $error = curl_error($ch);
            curl_close($ch);
            throw new Exception(sprintf("Ошибка отправки смс: %s - %s", $this->phone, $error));
        }
        curl_close($ch);

        return $body;
    }

    /**
     * Обработка ответа sms.ru
     * @param $res
     * @return bool
     * @throws Exception
     */
    private function CheckReplyError($res): bool
    {
        if (!$res) {
            return false;
        }

        $result = json_decode($res);

        if (!$result || !$result->status) {
            throw new Exception("Ошибка отправки смс: неожиданная ошибка");
        }

        if($result->status != "OK") {
            throw new Exception("Смс не отправлено: %s", $res->status_text);
        }

        return true;
    }

    /**
     * @param $str
     * @param $post_charset
     * @param $send_charset
     * @return string
     */
    private function sms_mime_header_encode($str, $post_charset, $send_charset): string
    {
        if ($post_charset != $send_charset) {
            $str = iconv($post_charset, $send_charset, $str);
        }
        return "=?" . $send_charset . "?B?" . base64_encode($str) . "?=";
    }

    /**
     * Преобразование объекта в массив для отправки
     * @return array
     */
    private function createPost(): array
    {
        $post['api_id'] = $this->apiKey;
        $post['to'] = $this->phone;
        $post['msg'] = $this->message;
        $post['from'] = $this->from;
        $post['translit'] = $this->isTranslit;
        $post['test'] = $this->isTest;

        return $post;

    }

}