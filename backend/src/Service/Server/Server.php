<?php

namespace App\Service\Server;

use App\Controller\ApiController;
use App\Entity\Order;
use App\Entity\Security\AuthUser;
use App\Helpers\Params;
use App\Helpers\ProductHelper;
use App\Helpers\SerializerHelper;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use App\Service\Server\Order as RemoteOrder;
use App\Service\Server\User as RemoteUser;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Интеграция с 1С
 */
class Server
{
    const METHOD_ORDER_ADD = '/1cob/hs/grosternewpublic/webhooks/add-order';
    const METHOD_ORDER_CANCEL = '/1cob/hs/grosternewpublic/webhooks/cancel-order';
    const METHOD_PRICELIST = '/1cob/hs/grosternewpublic/webhooks/generate-price-list';
    const METHOD_INVOICE_PAYMENT = '/1cob/hs/grosternewpublic/webhooks/generate-document-invoice-payment';
    const METHOD_ATTACHED_DOCS = '/1cob/hs/grosternewpublic/webhooks/attached-documents-contractpayer';
    const METHOD_ORDER_DOCUMENT = '/1cob/hs/grosternewpublic/webhooks/generate-document-contractpayer';
    const METHOD_PACKING_DOCS = '/1cob/hs/grosternewpublic/webhooks/generate-document-packing-list';
    const METHOD_USER_UPDATE = '/1cob/hs/grosternewpublic/webhooks/products-buyers';

    const REQUEST_METHOD_POST = 'POST';
    const REQUEST_METHOD_PUT = 'PUT';
    const LOG_PREFIX = '1C';

    const DIR_DOCS = 'docs';

    private string $domain;
    private string $serverKey;
    private string $docsDir;
    private $ci;
    private $em;
    private $isTestEnv;

    public function __construct(ContainerInterface $ci, EntityManagerInterface $em)
    {
        $this->ci = $ci;
        $this->domain = $ci->getParameter('server_domain_1c');
        $this->serverKey = $ci->getParameter('server_key_1c');
        $this->em = $em;
        $this->isTestEnv = 'test' == $_SERVER['APP_ENV'];
        $this->docsDir = $ci->getParameter('kernel.project_dir') . '/public/' . self::DIR_DOCS;
    }

    /**
     * Отправка нового заказа
     * @param Order $order
     * @return bool
     */
    public function addOrder(Order $order): bool
    {
        try {
            $remoteOrder = new RemoteOrder($this->em, $order);

            $payload = SerializerHelper::serialize([Params::ORDER => [$remoteOrder]], [SerializerHelper::GROUP_REMOTE_ORDER]);

            $this->request(self::METHOD_ORDER_ADD, self::REQUEST_METHOD_POST, $payload);
        } catch (Exception $exception) {
            ApiController::getLogger()->error($exception->getMessage());
            return false;
        }

        return true;
    }

    public function cancelOrder(Order $order): bool
    {
        try {
            $method = sprintf("%s/%s", self::METHOD_ORDER_CANCEL, $order->getUid());
            $this->request($method, self::REQUEST_METHOD_PUT);
        } catch (Exception $exception) {
            ApiController::getLogger()->error($exception->getMessage());
            return false;
        }

        return true;
    }

    /**
     * Обновление данных пользователя на сервере и получение списка рекомендуемых товаров
     * @param AuthUser $user
     * @return bool
     */
    public function updateUserProduct(AuthUser $user)
    {
        try {
            $remoteUser = new RemoteUser($user);

            $payload = SerializerHelper::serialize([Params::BUYERS => [$remoteUser]], [SerializerHelper::GROUP_REMOTE_USER]);
            $res = $this->request(self::METHOD_USER_UPDATE, self::REQUEST_METHOD_POST, $payload);

            if(!empty($res)) {
                $data = json_decode($res, true);

                if(!isset($data[Params::BUYERS]) || empty($data[Params::BUYERS])) {
                    return true;
                }

                ProductHelper::updateUserProducts($this->em, $data[Params::BUYERS][0], $user);
            }

        } catch (Exception $exception) {
            ApiController::getLogger()->error($exception->getMessage());
            return false;
        }



        return true;
    }

    /**
     * Отправка запроса в 1С
     * @param string $method
     * @param string $requestMethod - метод запроса. Если пусто, то GET
     * @param string $payload - данные для отправки
     * @return string|null
     * @throws Exception
     */
    private function request(string $method, string $requestMethod = '', string $payload = ''): ?string
    {
        if ($this->isTestEnv) {
            return null;
        }

        ApiController::getLogger()->info(sprintf("INFO %s: new request: %s -> %s", self::LOG_PREFIX, $method, $payload));

        $curl = $this->getCurl($method);
        if (!empty($requestMethod)) {
            curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $requestMethod);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_HEADER, false);

            if (!empty($payload)) {
                curl_setopt($curl, CURLOPT_POSTFIELDS, $payload);
            }
        }

        $res = curl_exec($curl);

        if (FALSE === $res) {
            $err = curl_error($curl);
            curl_close($curl);

            $msg = sprintf("%s: Fail to send request: %s -> %s", self::LOG_PREFIX, $method, $err);
            throw new Exception($msg);
        }

        $info = curl_getinfo($curl);
        curl_close($curl);

        switch ($info['http_code']) {
            case 200:
                ApiController::getLogger()->info(sprintf("INFO %s: response: %s -> %s", self::LOG_PREFIX, $method, $res));
                break;
            case 400:
                $msg = sprintf("%s: Fail to send request: %s -> %s", self::LOG_PREFIX, $method, $res);
                throw new Exception($msg);
            case 404:
                $msg = sprintf("%s: Not found request with url %s", self::LOG_PREFIX, $method);
                throw new Exception($msg);
            default:
                $msg = sprintf("%s: Fail to send request with status %d: %s", self::LOG_PREFIX, $info['http_code'], $res);
                throw new Exception($msg);
        }

        return $res;
    }

    public function getPricelist(array $categories): ?string
    {
        if($this->isTestEnv) {
            return null;
        }

        ini_set('memory_limit', '-1');

        $params = [];
        foreach ($categories as $key => $category) {
            $params[] = sprintf("productsections[%d]=%s", $key, $category);
        }

        $method = sprintf("%s?%s", Server::METHOD_PRICELIST, implode("&", $params));

        $filename = "pricelist-" . md5(microtime());
        $filepath = $this->downloadDocument($method, $filename);

        return $filepath ? sprintf("%s/%s", $this->ci->getParameter('static_domain'), $filepath) : '';

    }

    /**
     * Выгрузка файлов. Возвращает название файла или null
     * @param string $method
     * @param string $filename
     * @return string|null
     */
    public function downloadDocument(string $method, string $filename): ?string
    {
        if($this->isTestEnv) {
            return null;
        }

        ApiController::getLogger()->info(sprintf("INFO %s: new request: %s -> %s", self::LOG_PREFIX, $method, $filename));

        try {
            $ch = $this->getCurl($method);
            $response = curl_exec($ch);
            // Separation of header and body
            $contentType = '';
            $body = '';
            if (curl_getinfo($ch, CURLINFO_HTTP_CODE) == '200') {
                $headerSize = curl_getinfo ($ch, CURLINFO_HEADER_SIZE); // header information size
                $contentType = curl_getinfo($ch, CURLINFO_CONTENT_TYPE);
                $body = substr($response, $headerSize);
            }
            curl_close($ch);
            // File name
            if (!empty($body) || !empty($contentType)) {
                $contentType = str_replace('application/', '', $contentType);
                $file = sprintf('%s.%s', $filename, $contentType);
                // Create directories and set permissions
                if (!file_exists($this->docsDir)) {
                    @mkdir($this->docsDir, 0777, true);
                    @chmod($this->docsDir, 0777);
                }
                $filepath = sprintf("%s/%s", $this->docsDir, $file);
                if (file_put_contents($filepath, $body)) {
                    return self::DIR_DOCS . DIRECTORY_SEPARATOR . $file;
                }
            }
        } catch (Exception $exception) {
            ApiController::getLogger()->error($exception->getMessage());
        }

        return null;
    }

    /**
     * Returns curl resource
     * @param string $method
     * @return false|resource
     */
    private function getCurl(string $method)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->domain . $method);
        curl_setopt($ch, CURLOPT_HTTPHEADER, [
            sprintf("Authorization: Basic %s", $this->serverKey),
            'Content-type: application/json',
        ]);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HEADER, TRUE); //Need response header
        curl_setopt ($ch, CURLOPT_NOBODY, FALSE); //Need response body

        // Don't check ssl
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

        return $ch;
    }
}