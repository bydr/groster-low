<?php

namespace App\Service\Server;

use App\Entity\Order as Cart;
use App\Entity\Security\AuthUser;
use App\Helpers\Params;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\SerializedName;

/**
 * Пользователь для отправки в 1С
 */
class User
{
    /**
     * id заказа на сайте
     * @SerializedName("id")
     * @Groups("remote-user")
     */
    private string $id;

    private string $phone;

    private string $email;

    /**
     * @return string[]
     * @SerializedName("contacts")
     * @Groups("remote-user")
     */
    public function getContacts(): array
    {
        return [
            Params::PHONE => [$this->phone],
            Params::EMAIL => [$this->email],
        ];
    }

    public function __construct(AuthUser $user)
    {
        $this->id = $user->getUid();
        $this->phone = (string)$user->getPhone();
        $this->email = (string)$user->getEmail();
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @param string $id
     */
    public function setId(string $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getPhone(): string
    {
        return $this->phone;
    }

    /**
     * @param string $phone
     */
    public function setPhone(string $phone): void
    {
        $this->phone = $phone;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail(string $email): void
    {
        $this->email = $email;
    }
}