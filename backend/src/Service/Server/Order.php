<?php

namespace App\Service\Server;

use App\Controller\ApiController;
use App\Entity\OrderItem;
use App\Entity\PaymentMethod;
use App\Entity\Price;
use App\Entity\Product;
use \App\Entity\Order as Cart;
use App\Entity\ShippingMethod;
use App\Entity\Store;
use DateTimeInterface;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\SerializedName;

/**
 * Заказ для отправки в 1С
 */
class Order
{
    /** @var EntityManagerInterface */
    private $em;

    /**
     * id заказа на сайте
     * @SerializedName("NumberOrder")
     * @Groups("remote-order")
     */
    private string $id;

    /**
     * Номер заказа на сайте, тот который отображается клиенту и будет показан сотрудникам
     * @SerializedName("IDOrder")
     * @Groups("remote-order")
     */
    private string $uid;

    /**
     * Признак быстрого заказа
     * @SerializedName("QuickOrder")
     * @Groups("remote-order")
     */
    private bool $fastOrder;

    /**
     * Дата заказа (ISO 8601 YYYY-MM-DDThh:mm:ss)
     */
    private DateTimeInterface $createdAt;

    /**
     * дата отгрузки заказа (ISO 8601 YYYY-MM-DDThh:mm:ss)
     */
    private DateTimeInterface $shippingDate;

    /**
     * Имя контакта заказа
     * @SerializedName("NameContactOrder")
     * @Groups("remote-order")
     */
    private ?string $orderFio;

    /**
     * Телефон заказа
     * @SerializedName("ViewTelContactOrder")
     * @Groups("remote-order")
     */
    private ?string $orderPhone;

    /**
     * email заказа
     * @SerializedName("ViewEmailContactOrder")
     * @Groups("remote-order")
     */
    private ?string $orderEmail;

    /**
     * id склада заказа, полученный из остатков FTP
     * @SerializedName("StoreID")
     * @Groups("remote-order")
     */
    private string $storeId;

    /**
     * комментарий покупателя к заказу
     * @SerializedName("OrderComment")
     * @Groups("remote-order")
     */
    private string $comment;

    /**
     * вместе с доставкой необходимо привезти теминал эквайринга
     * @SerializedName("BringAcquiringTerminal")
     * @Groups("remote-order")
     */
    private bool $needTerminal;

    /**
     * Клиенту необходимы товарные накладные
     * @SerializedName("NeedWaybills")
     * @Groups("remote-order")
     */
    private bool $needWaybills;

    /**
     * желаемое время доставки начало (ISO 8601 hh:mm:ss)
     * @SerializedName("DesiredDeliveryTimeStart")
     * @Groups("remote-order")
     */
    private DateTimeInterface $deliveryTimeStart;

    /**
     * желаемое время доставки конец (ISO 8601 hh:mm:ss)
     * @SerializedName("DesiredDeliveryTimeEnd")
     * @Groups("remote-order")
     */
    private DateTimeInterface $deliveryTimeEnd;

    /**
     * время перерыва доставки начало (ISO 8601 hh:mm:ss)
     * @SerializedName("TimeBreakDeliveryStart")
     * @Groups("remote-order")
     */
    private DateTimeInterface $breakDeliveryStart;

    /**
     * время перерыва доставки конец (ISO 8601 hh:mm:ss)
     * @SerializedName("TimeBreakDeliveryEnd")
     * @Groups("remote-order")
     */
    private DateTimeInterface $breakDeliveryEnd;

    /**
     * время в минутах за которое нужно предварительно позвонить перед доставкой
     * @SerializedName("TimeCallBeforeDelivery")
     * @Groups("remote-order")
     */
    private int $timeCallBeforeDelivery;

    /**
     * вариант доставки заказа ("Курьер","Самовывоз")
     * @SerializedName("TypeDelivery")
     * @Groups("remote-order")
     */
    private string $shippingMethod;

    /**
     * бесплатная доставка
     * @SerializedName("FreeShipping")
     * @Groups("remote-order")
     */
    private bool $isFreeShipping;

    /**
     * себестоимость доставки
     * @SerializedName("ShippingCost")
     * @Groups("remote-order")
     */
    private int $shippingCost;

    /**
     * стоимость доставки
     * @SerializedName("ShippingPrice")
     * @Groups("remote-order")
     */
    private int $shippingPrice;

    /**
     * ширина габаритов заказа
     * @SerializedName("DeliveryDimensionsWeight")
     * @Groups("remote-order")
     */
    private int $deliveryDimensionsWeight;

    /**
     * длина габаритов заказа
     * @SerializedName("DeliveryDimensionsLength")
     * @Groups("remote-order")
     */
    private int $deliveryDimensionsLength;

    /**
     * высота габаритов заказа
     * @SerializedName("DeliveryDimensionsHeight")
     * @Groups("remote-order")
     */
    private int $deliveryDimensionsHeight;

    /**
     * объем габаритов заказа в кубическим сантиметрах
     * @SerializedName("DeliveryDimensionsSize")
     * @Groups("remote-order")
     */
    private int $deliveryDimensionsSize;

    /**
     * вес габаритов заказа
     * @SerializedName("DeliveryDimensionsWidth")
     * @Groups("remote-order")
     */
    private int $deliveryDimensionsWidth;

    /**
     * id клиента
     * @SerializedName("IDBuyer")
     * @Groups("remote-order")
     */
    private string $customerId;

    /**
     * телефон клиента
     * @SerializedName("ViewTelContactBuyer")
     * @Groups("remote-order")
     */
    private string $customerPhone;

    /**
     * email клиента
     * @SerializedName("ViewEmailContactBuyer")
     * @Groups("remote-order")
     */
    private string $customerEmail;

    /**
     * id плательщика
     * @SerializedName("IDPayer")
     * @Groups("remote-order")
     */
    private string $payerUid;

    /**
     * телефон плательщика
     * @SerializedName("TelViewPayer")
     * @Groups("remote-order")
     */
    private string $payerPhone;

    /**
     * юридическое наименование плательщика
     * @SerializedName("JuristicNamePayer")
     * @Groups("remote-order")
     */
    private string $payerName;

    /**
     * тип плательщика ("Юридическое лицо","Индивидуальный предприниматель",
     * "Физическое лицо","Государственный орган")
     * @SerializedName("TypePayer")
     * @Groups("remote-order")
     */
    private string $payerType;

    /**
     * ИНП плательщика если тип = "Юридическое лицо","Индивидуальный предприниматель",
     * "Физическое лицо","Государственный орган"
     * @SerializedName("TaxpayerIdentificationNumberPayer")
     * @Groups("remote-order")
     */
    private string $payerInn;

    /**
     * КПП плательщика если тип = "Юридическое лицо","Государственный орган"
     * @SerializedName("RegistrationCodePayer")
     * @Groups("remote-order")
     */
    private string $payerKpp;

    /**
     * ОКПО плательщика если тип = "Юридическое лицо","Индивидуальный предприниматель"
     * @SerializedName("ClassifierOrganizationsPayer")
     * @Groups("remote-order")
     */
    private string $payerOkpo;

    /**
     * ОГРН плательщика если тип = "Юридическое лицо","Индивидуальный предприниматель"
     * @SerializedName("StateRegistrationNumberPayer")
     * @Groups("remote-order")
     */
    private string $payerOgrn;

    /**
     * вид государственного органа плательщика если тип = "Государственный орган"
     * (в вариантах "Налоговый орган","Орган ФСС","Орган ПФР","Прочий")
     * @SerializedName("TypeGovernmentPayer")
     * @Groups("remote-order")
     */
    private string $payerGovernmentType;

    /**
     * код государственного органа плательщика если тип = "Государственный орган"
     * @SerializedName("NumberGovernmentPayer")
     * @Groups("remote-order")
     */
    private string $payerGovernmentNumber;

    /**
     * свидетельство регистрации ИП серия номер плательщика если тип = "Индивидуальный предприниматель"
     * @SerializedName("CertificateSeriesNumberPayer")
     * @Groups("remote-order")
     */
    private string $payerCertificateNumber;

    /**
     * свидетельство регистрации ИП дата выдачи плательщика если тип = "Индивидуальный предприниматель"
     * (ISO 8601 YYYY-MM-DDThh:mm:ss)
     * @SerializedName("CertificateSeriesDatePayer")
     * @Groups("remote-order")
     */
    private DateTimeInterface $payerCertificateSeries;

    /**
     * серия номер паспорта удостоверяющий личность плательщика если тип =
     * "Физическое лицо","Индивидуальный предприниматель"
     * @SerializedName("IdentityDocumentPayer")
     * @Groups("remote-order")
     */
    private string $payerPassport;

    /**
     * ФИО плательщика если тип = "Индивидуальный предприниматель","Физическое лицо"
     * @SerializedName("FullNamePayer")
     * @Groups("remote-order")
     */
    private string $payerContact;

    /**
     * пол физического лица плательщика если тип = "Физическое лицо","Индивидуальный предприниматель"
     * @SerializedName("GenderPayer")
     * @Groups("remote-order")
     */
    private string $payerGender;

    /**
     * дата рождения физического лица плательщика если тип = "Физическое лицо","Индивидуальный предприниматель"
     * (ISO 8601 YYYY-MM-DDThh:mm:ss)
     * @SerializedName("DateBirthPayer")
     * @Groups("remote-order")
     */
    private DateTimeInterface $payerBirthday;

    /**
     * БИК банка банковского счета
     */
    private string $payerBik;

    /**
     * тип банковского счета (0 - "Расчетный", 1 - "Депозитный", 2 - "Ссудный", 3 - "Иной")
     */
    private int $payerAccountType;

    /**
     * номер банковского счета
     */
    private string $payerAccount;

    /**
     * Корр счет банковского счета
     */
    private string $payerCorrespondentAccount;

    /**
     * Массив сфер бизнеса заказа
     * @var string|null
     */
    private ?string $businessArea;

    /**
     * Id точки продажи клиента
     * @SerializedName("IDStoreBuyer")
     * @Groups("remote-order")
     */
    private string $customerStore;

    /**
     * Наименование точки продажи клиента
     * @SerializedName("NameStoreBuyer")
     * @Groups("remote-order")
     */
    private string $customerStoreName;

    /**
     * Телефон для связи точки продажи клиента
     * @SerializedName("TelViewStoreBuyer")
     * @Groups("remote-order")
     */
    private string $customerStorePhone;

    /**
     * Представление адреса точки продажи клиента
     * @SerializedName("AdressStoreBuyerView")
     * @Groups("remote-order")
     */
    private string $customerStoreAddress;

    /**
     * Способ замены товара (
     * 0 - "Позвонить мне. Если не удастся, выбирает сборщик",
     * 1 - "Позвонить мне. Если не удастся, убрать товар",
     * 2 - "Не звонить мне. Замену выбирает сборщик",
     * 3 - "Не звонить мне. Убрать отсутствующий товар")
     * @SerializedName("ProductReplacementMethod")
     * @Groups("remote-order")
     */
    private int $replacementMethod;

    /**
     * Товары в заказе
     */
    private Collection $items;

    /**
     * @var string|null
     */
    private ?string $promocode;

    /**
     * Описание бакнковских счетов плательщика
     * @SerializedName("BankAccounts")
     * @Groups("remote-order")
     */
    public function getBankAccounts(): array
    {
        $account = [];

        if (!empty($this->payerBik)) {
            $account['BankIdentificationCode'] = $this->payerBik;
        }

        if (!empty($this->payerAccountType)) {
            $account['TypeBankAccount'] = $this->payerAccountType;
        }

        if (!empty($this->payerAccount)) {
            $account['NumberBankAccount'] = $this->payerAccount;
        }

        if (!empty($this->payerCorrespondentAccount)) {
            $account['NumberCorrespondentAccount'] = $this->payerCorrespondentAccount;
        }

        if (empty($account)) {
            return [];
        }

        return [$account];
    }

    /**
     * @return array
     * @SerializedName("Products")
     * @Groups("remote-order")
     */
    public function getProducts(): array
    {
        $products = [];

        /** @var OrderItem $item */
        foreach ($this->items as $item) {
            $products[] = $this->fillProduct($item);

            if ($item->getSample()) {
                $products[] = $this->fillProduct($item, true);
            }
        }

        return $products;
    }

    private function fillProduct(OrderItem $item, bool $isSample = false): array
    {
        $product = [
            'IDKitProduct' => $item->getParentUuid(),
            'PriceProduct' => $item->getPrice(),
            'SumDiscount' => $item->getDiscount(),
            'SumProduct' => $item->getTotal(),
            'IDPriceType' => $this->getPriceType($item->getOrder()),
        ];

        /** @var Product $parent */
        if ($parent = $item->getProduct()->getCollectionParent()) {
            $product['IDCollectionProduct'] = $item->getProductUuid();
            $product['IDProduct'] = $parent->getUuid();
        } else {
            $product['IDProduct'] = $item->getProductUuid();
        }

        if ($isSample) {
            $product['CountProduct'] = $item->getSample();
            $product['ThisProductSample'] = true;
        } else {
            $product['CountProduct'] = $item->getQuantity();
        }

        return $product;
    }

    /**
     * @return string
     * @SerializedName("OrderDate")
     * @Groups("remote-order")
     */
    public function getOrderDate(): string
    {
        return $this->createdAt->format(DateTimeInterface::ATOM);
    }

    /**
     * @SerializedName("OrderShipmentDate")
     * @Groups("remote-order")
     */
    public function getOrderShipmentDate(): string
    {
        return !empty($this->shippingDate) ? $this->shippingDate->format(DateTimeInterface::ATOM) : '';
    }

    /**
     * Код скидки
     * @SerializedName("Promocodes")
     * @Groups("remote-order")
     */
    public function getPromocodes(): array
    {
        return [$this->promocode];
    }

    /**
     * @return array
     * @SerializedName("BusinessAreasOrder")
     * @Groups("remote-order")
     */
    public function getBusinessAreas(): array
    {
        return [["NameBusinessArea" => $this->getBusinessArea()]];
    }

    public function __construct(EntityManagerInterface $em, Cart $order)
    {
        $this->em = $em;
        $this->fillOrder($order);
    }

    private function fillOrder(Cart $order)
    {
        $this->setId($order->getId())->setUid($order->getUid())->setCreatedAt($order->getCheckoutCompleteAt())
            ->setItems($order->getItems())
            ->setOrderFio($order->getFio())->setOrderPhone($order->getPhone())->setOrderEmail($order->getEmail())
            ->setBusinessArea($order->getBussinessArea())
            ->setFastOrder($order->getIsFast());
        if ($shippingDate = $order->getShippingDate()) {
            $this->setShippingDate($shippingDate);
        }
        if ($order->getComment()) {
            $this->setComment($order->getComment());
        }
        if ($shippingMethod = $order->getShippingMethod()) {
            $this->setShippingMethod($shippingMethod->getAlias());
            // Если самовывоз, то указываем ID склада
            if (ShippingMethod::METHOD_PICKUP == $shippingMethod->getAlias()) {
                /** @var Store $store */
                $store = $this->em->getRepository(Store::class)->findOneBy(['address' => $order->getShippingAddress(), 'isActive' => true]);
                if ($store) {
                    $this->setStoreId($store->getUuid());
                }
            } else { // иначе адрес доставки
                $shippingAddress = $order->getShippingAddress() ?? '';
                $this->setCustomerStoreAddress($shippingAddress);
            }
        }

        $this->setIsFreeShipping(0 == $order->getShippingCost());
        if ($order->getShippingCost()) {
            $this->setShippingPrice($order->getShippingCost());
        }
        if ($order->getCustomer()) {
            $this->setCustomerId($order->getCustomer()->getUid())
                ->setCustomerEmail($order->getCustomer()->getEmail() ?? '')
                ->setCustomerPhone($order->getCustomer()->getPhone() ?? '');
        }

        if ($order->getReplacement()) {
            $replacementMethod = null;
            switch ($order->getReplacement()->getId()) {
                case 1:
                    $replacementMethod = 0;
                    break;
                case 2:
                    $replacementMethod = 1;
                    break;
                case 3:
                    $replacementMethod = 2;
                    break;
                case 4:
                    $replacementMethod = 3;
            }
            $this->setReplacementMethod($replacementMethod);
        }

        if ($order->getPayer()) {
            $payer = $order->getPayer();
            $this->setPayerUid($payer->getUid())->setPayerName($payer->getName())->setPayerAccount($payer->getAccount())
                ->setPayerBik($payer->getBik())->setPayerInn($payer->getInn())->setPayerOgrn($payer->getOgrnip())
                ->setPayerKpp($payer->getKpp())->setPayerContact($payer->getContact())->setPayerPhone($payer->getContactPhone());
        }

        $promocode = null;
        if ($discount = $order->getDiscount()) {
            $promocode = $discount->getCode();
        }

        $this->setPromocode($promocode);

        if($order->getPaymentMethod()) {
            switch ($order->getPaymentMethod()->getUid()) {
                case PaymentMethod::UID_BANK;
                    $this->setNeedWaybills(true);
                    break;
                case PaymentMethod::UID_CARD_COURIER:
                    $this->setNeedTerminal(true);
                    break;
            }
        }

    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @param string $id
     * @return Order
     */
    public function setId(string $id): self
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getUid(): string
    {
        return $this->uid;
    }

    /**
     * @param string $uid
     * @return Order
     */
    public function setUid(string $uid): self
    {
        $this->uid = $uid;
        return $this;
    }

    /**
     * @return DateTimeInterface
     */
    public function getCreatedAt(): DateTimeInterface
    {
        return $this->createdAt;
    }

    /**
     * @param DateTimeInterface $createdAt
     */
    public function setCreatedAt(DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;
        return $this;
    }

    /**
     * @return DateTimeInterface
     */
    public function getShippingDate(): ?DateTimeInterface
    {
        return $this->shippingDate;
    }

    /**
     * @param DateTimeInterface $shippingDate
     * @return Order
     */
    public function setShippingDate(DateTimeInterface $shippingDate): self
    {
        $this->shippingDate = $shippingDate;
        return $this;
    }

    /**
     * @return string
     */
    public function getStoreId(): ?string
    {
        return $this->storeId ?? null;
    }

    /**
     * @param string $storeId
     */
    public function setStoreId(string $storeId): self
    {
        $this->storeId = $storeId;
        return $this;
    }

    /**
     * @return string
     */
    public function getComment(): string
    {
        return $this->comment ?? '';
    }

    /**
     * @param string $comment
     */
    public function setComment(string $comment): self
    {
        $this->comment = $comment;
        return $this;
    }

    /**
     * @return bool
     */
    public function isNeedTerminal(): ?bool
    {
        return $this->needTerminal ?? null;
    }

    /**
     * @param bool $needTerminal
     */
    public function setNeedTerminal(bool $needTerminal): self
    {
        $this->needTerminal = $needTerminal;
        return $this;
    }

    /**
     * @return bool
     */
    public function isNeedWaybills(): ?bool
    {
        return $this->needWaybills ?? null;
    }

    /**
     * @param bool $needWaybills
     */
    public function setNeedWaybills(bool $needWaybills): self
    {
        $this->needWaybills = $needWaybills;
        return $this;
    }

    /**
     * @return DateTimeInterface
     */
    public function getDeliveryTimeStart(): ?DateTimeInterface
    {
        return $this->deliveryTimeStart ?? null;
    }

    /**
     * @param DateTimeInterface $deliveryTimeStart
     */
    public function setDeliveryTimeStart(DateTimeInterface $deliveryTimeStart): self
    {
        $this->deliveryTimeStart = $deliveryTimeStart;
        return $this;
    }

    /**
     * @return DateTimeInterface
     */
    public function getDeliveryTimeEnd(): ?DateTimeInterface
    {
        return $this->deliveryTimeEnd ?? null;
    }

    /**
     * @param DateTimeInterface $deliveryTimeEnd
     */
    public function setDeliveryTimeEnd(DateTimeInterface $deliveryTimeEnd): self
    {
        $this->deliveryTimeEnd = $deliveryTimeEnd;
        return $this;
    }

    /**
     * @return DateTimeInterface
     */
    public function getBreakDeliveryStart(): ?DateTimeInterface
    {
        return $this->breakDeliveryStart ?? null;
    }

    /**
     * @param DateTimeInterface $breakDeliveryStart
     */
    public function setBreakDeliveryStart(DateTimeInterface $breakDeliveryStart): self
    {
        $this->breakDeliveryStart = $breakDeliveryStart;
        return $this;
    }

    /**
     * @return DateTimeInterface
     */
    public function getBreakDeliveryEnd(): ?DateTimeInterface
    {
        return $this->breakDeliveryEnd ?? null;
    }

    /**
     * @param DateTimeInterface $breakDeliveryEnd
     */
    public function setBreakDeliveryEnd(DateTimeInterface $breakDeliveryEnd): self
    {
        $this->breakDeliveryEnd = $breakDeliveryEnd;
        return $this;
    }

    /**
     * @return int
     */
    public function getTimeCallBeforeDelivery(): ?int
    {
        return $this->timeCallBeforeDelivery ?? null;
    }

    /**
     * @param int $timeCallBeforeDelivery
     */
    public function setTimeCallBeforeDelivery(int $timeCallBeforeDelivery): self
    {
        $this->timeCallBeforeDelivery = $timeCallBeforeDelivery;
        return $this;
    }

    /**
     * @return string
     */
    public function getShippingMethod(): ?string
    {
        $methodTitle = '';
        if (empty($this->shippingMethod)) {
            return null;
        }
        switch ($this->shippingMethod) {
            case ShippingMethod::METHOD_EXPRESS:
                $methodTitle = 'Экспресс';
                break;
            case ShippingMethod::METHOD_PICKUP:
                $methodTitle = 'Самовывоз';
                break;
            case ShippingMethod::METHOD_COURIER:
                $methodTitle = 'Курьер';
                break;
            case ShippingMethod::METHOD_COMPANY:
                $methodTitle = 'Транспортная компания';
                break;
        }
        return !empty($methodTitle) ? $methodTitle : null;
    }

    /**
     * @param string|null $shippingMethod
     * @return Order
     */
    public function setShippingMethod(?string $shippingMethod): self
    {
        $this->shippingMethod = $shippingMethod;
        return $this;
    }

    /**
     * @return bool
     */
    public function isFreeShipping(): ?bool
    {
        return $this->isFreeShipping ?? null;
    }

    /**
     * @param bool $isFreeShipping
     */
    public function setIsFreeShipping(bool $isFreeShipping): self
    {
        $this->isFreeShipping = $isFreeShipping;
        return $this;
    }

    /**
     * @return int
     */
    public function getShippingCost(): ?int
    {
        return $this->shippingCost ?? null;
    }

    /**
     * @param int $shippingCost
     */
    public function setShippingCost(int $shippingCost): self
    {
        $this->shippingCost = $shippingCost;
        return $this;
    }

    /**
     * @return int
     */
    public function getShippingPrice(): ?int
    {
        return $this->shippingPrice ?? null;
    }

    /**
     * @param int $shippingPrice
     */
    public function setShippingPrice(int $shippingPrice): self
    {
        $this->shippingPrice = $shippingPrice;
        return $this;
    }

    /**
     * @return int
     */
    public function getDeliveryDimensionsWeight(): ?int
    {
        return $this->deliveryDimensionsWeight ?? null;
    }

    /**
     * @param int $deliveryDimensionsWeight
     */
    public function setDeliveryDimensionsWeight(int $deliveryDimensionsWeight): self
    {
        $this->deliveryDimensionsWeight = $deliveryDimensionsWeight;
        return $this;
    }

    /**
     * @return int
     */
    public function getDeliveryDimensionsLength(): ?int
    {
        return $this->deliveryDimensionsLength ?? null;
    }

    /**
     * @param int $deliveryDimensionsLength
     */
    public function setDeliveryDimensionsLength(int $deliveryDimensionsLength): self
    {
        $this->deliveryDimensionsLength = $deliveryDimensionsLength;
        return $this;
    }

    /**
     * @return int
     */
    public function getDeliveryDimensionsHeight(): ?int
    {
        return $this->deliveryDimensionsHeight ?? null;
    }

    /**
     * @param int $deliveryDimensionsHeight
     */
    public function setDeliveryDimensionsHeight(int $deliveryDimensionsHeight): self
    {
        $this->deliveryDimensionsHeight = $deliveryDimensionsHeight;
        return $this;
    }

    /**
     * @return int
     */
    public function getDeliveryDimensionsSize(): ?int
    {
        return $this->deliveryDimensionsSize ?? null;
    }

    /**
     * @param int $deliveryDimensionsSize
     */
    public function setDeliveryDimensionsSize(int $deliveryDimensionsSize): self
    {
        $this->deliveryDimensionsSize = $deliveryDimensionsSize;
        return $this;
    }

    /**
     * @return int
     */
    public function getDeliveryDimensionsWidth(): ?int
    {
        return $this->deliveryDimensionsWidth ?? null;
    }

    /**
     * @param int $deliveryDimensionsWidth
     */
    public function setDeliveryDimensionsWidth(int $deliveryDimensionsWidth): self
    {
        $this->deliveryDimensionsWidth = $deliveryDimensionsWidth;
        return $this;
    }

    /**
     * @return string
     */
    public function getCustomerId(): ?string
    {
        return $this->customerId ?? '';
    }

    /**
     * @param string $customerId
     */
    public function setCustomerId(string $customerId): self
    {
        $this->customerId = $customerId;
        return $this;
    }

    /**
     * @return string
     */
    public function getCustomerPhone(): string
    {
        return $this->customerPhone ?? '';
    }

    /**
     * @param string $customerPhone
     */
    public function setCustomerPhone(string $customerPhone): self
    {
        $this->customerPhone = $customerPhone;
        return $this;
    }

    /**
     * @return string
     */
    public function getCustomerEmail(): string
    {
        return $this->customerEmail ?? '';
    }

    /**
     * @param string $customerEmail
     */
    public function setCustomerEmail(string $customerEmail): self
    {
        $this->customerEmail = $customerEmail;
        return $this;
    }

    /**
     * @return string
     */
    public function getPayerUid(): ?string
    {
        return $this->payerUid ?? null;
    }

    /**
     * @param string $payerUid
     * @return Order
     */
    public function setPayerUid(string $payerUid): self
    {
        $this->payerUid = $payerUid;
        return $this;
    }

    /**
     * @return string
     */
    public function getPayerPhone(): string
    {
        return $this->payerPhone ?? '';
    }

    /**
     * @param string $payerPhone
     * @return Order
     */
    public function setPayerPhone(?string $payerPhone): self
    {
        $this->payerPhone = (string)$payerPhone;
        return $this;
    }

    /**
     * @return string
     */
    public function getPayerName(): string
    {
        return $this->payerName ?? '';
    }

    /**
     * @param string $payerName
     */
    public function setPayerName(?string $payerName): self
    {
        $this->payerName = (string)$payerName;
        return $this;
    }

    /**
     * @return string
     */
    public function getPayerType(): ?string
    {
        return $this->payerType ?? null;
    }

    /**
     * @param string $payerType
     */
    public function setPayerType(string $payerType): self
    {
        $this->payerType = $payerType;
        return $this;
    }

    /**
     * @return string
     */
    public function getPayerInn(): string
    {
        return $this->payerInn ?? '';
    }

    /**
     * @param string $payerInn
     */
    public function setPayerInn(?string $payerInn): self
    {
        $this->payerInn = (string)$payerInn;
        return $this;
    }

    /**
     * @return string
     */
    public function getPayerKpp(): string
    {
        return $this->payerKpp ?? '';
    }

    /**
     * @param string $payerKpp
     */
    public function setPayerKpp(?string $payerKpp): self
    {
        $this->payerKpp = (string)$payerKpp;
        return $this;
    }

    /**
     * @return string
     */
    public function getPayerOkpo(): string
    {
        return $this->payerOkpo ?? '';
    }

    /**
     * @param string $payerOkpo
     */
    public function setPayerOkpo(string $payerOkpo): self
    {
        $this->payerOkpo = $payerOkpo;
        return $this;
    }

    /**
     * @return string
     */
    public function getPayerOgrn(): string
    {
        return $this->payerOgrn ?? '';
    }

    /**
     * @param string $payerOgrn
     */
    public function setPayerOgrn(?string $payerOgrn): self
    {
        $this->payerOgrn = (string)$payerOgrn;
        return $this;
    }

    /**
     * @return string
     */
    public function getPayerGovernmentType(): string
    {
        return $this->payerGovernmentType ?? '';
    }

    /**
     * @param string $payerGovernmentType
     */
    public function setPayerGovernmentType(string $payerGovernmentType): self
    {
        $this->payerGovernmentType = $payerGovernmentType;
        return $this;
    }

    /**
     * @return string
     */
    public function getPayerGovernmentNumber(): string
    {
        return $this->payerGovernmentNumber ?? '';
    }

    /**
     * @param string $payerGovernmentNumber
     */
    public function setPayerGovernmentNumber(string $payerGovernmentNumber): self
    {
        $this->payerGovernmentNumber = $payerGovernmentNumber;
        return $this;
    }

    /**
     * @return string
     */
    public function getPayerCertificateNumber(): string
    {
        return $this->payerCertificateNumber ?? '';
    }

    /**
     * @param string $payerCertificateNumber
     */
    public function setPayerCertificateNumber(string $payerCertificateNumber): self
    {
        $this->payerCertificateNumber = $payerCertificateNumber;
        return $this;
    }

    /**
     * @return DateTimeInterface
     */
    public function getPayerCertificateSeries(): ?DateTimeInterface
    {
        return $this->payerCertificateSeries ?? null;
    }

    /**
     * @param DateTimeInterface $payerCertificateSeries
     */
    public function setPayerCertificateSeries(DateTimeInterface $payerCertificateSeries): self
    {
        $this->payerCertificateSeries = $payerCertificateSeries;
        return $this;
    }

    /**
     * @return string
     */
    public function getPayerPassport(): string
    {
        return $this->payerPassport ?? '';
    }

    /**
     * @param string $payerPassport
     */
    public function setPayerPassport(string $payerPassport): self
    {
        $this->payerPassport = $payerPassport;
        return $this;
    }

    /**
     * @return string
     */
    public function getPayerContact(): string
    {
        return $this->payerContact ?? '';
    }

    /**
     * @param string $payerContact
     */
    public function setPayerContact(?string $payerContact): self
    {
        $this->payerContact = (string)$payerContact;
        return $this;
    }

    /**
     * @return string
     */
    public function getPayerGender(): string
    {
        return $this->payerGender ?? '';
    }

    /**
     * @param string $payerGender
     */
    public function setPayerGender(string $payerGender): self
    {
        $this->payerGender = $payerGender;
        return $this;
    }

    /**
     * @return DateTimeInterface
     */
    public function getPayerBirthday(): ?DateTimeInterface
    {
        return $this->payerBirthday ?? null;
    }

    /**
     * @param DateTimeInterface $payerBirthday
     */
    public function setPayerBirthday(DateTimeInterface $payerBirthday): self
    {
        $this->payerBirthday = $payerBirthday;
        return $this;
    }

    /**
     * @return string
     */
    public function getPayerBik(): string
    {
        return $this->payerBik;
    }

    /**
     * @param string $payerBik
     * @return Order
     */
    public function setPayerBik(?string $payerBik): self
    {
        $this->payerBik = (string)$payerBik;
        return $this;
    }

    /**
     * @return int
     */
    public function getPayerAccountType(): int
    {
        return $this->payerAccountType;
    }

    /**
     * @param int $payerAccountType
     */
    public function setPayerAccountType(int $payerAccountType): self
    {
        $this->payerAccountType = $payerAccountType;
        return $this;
    }

    /**
     * @return string
     */
    public function getPayerAccount(): string
    {
        return $this->payerAccount ?? '';
    }

    /**
     * @param string $payerAccount
     */
    public function setPayerAccount(?string $payerAccount): self
    {
        $this->payerAccount = (string)$payerAccount;
        return $this;
    }

    /**
     * @return string
     */
    public function getPayerCorrespondentAccount(): string
    {
        return $this->payerCorrespondentAccount ?? '';
    }

    /**
     * @param string $payerCorrespondentAccount
     */
    public function setPayerCorrespondentAccount(string $payerCorrespondentAccount): self
    {
        $this->payerCorrespondentAccount = $payerCorrespondentAccount;
        return $this;
    }

    /**
     * @return string
     */
    public function getCustomerStore(): string
    {
        return $this->customerStore ?? '';
    }

    /**
     * @param string $customerStore
     */
    public function setCustomerStore(string $customerStore): self
    {
        $this->customerStore = $customerStore;
        return $this;
    }

    /**
     * @return string
     */
    public function getCustomerStoreName(): string
    {
        return $this->customerStoreName ?? '';
    }

    /**
     * @param string $customerStoreName
     */
    public function setCustomerStoreName(string $customerStoreName): self
    {
        $this->customerStoreName = $customerStoreName;
        return $this;
    }

    /**
     * @return string
     */
    public function getCustomerStorePhone(): string
    {
        return $this->customerStorePhone ?? '';
    }

    /**
     * @param string $customerStorePhone
     */
    public function setCustomerStorePhone(string $customerStorePhone): self
    {
        $this->customerStorePhone = $customerStorePhone;
        return $this;
    }

    /**
     * @return string
     */
    public function getCustomerStoreAddress(): string
    {
        return $this->customerStoreAddress ?? '';
    }

    /**
     * @param string $customerStoreAddress
     */
    public function setCustomerStoreAddress(string $customerStoreAddress): self
    {
        $this->customerStoreAddress = $customerStoreAddress;
        return $this;
    }

    /**
     * @return int
     */
    public function getReplacementMethod(): int
    {
        return $this->replacementMethod ?? 0;
    }

    /**
     * @param int $replacementMethod
     * @return Order
     */
    public function setReplacementMethod(int $replacementMethod): self
    {
        $this->replacementMethod = $replacementMethod;
        return $this;
    }

    /**
     * @return Collection
     */
    public function getItems(): Collection
    {
        return $this->items;
    }

    /**
     * @param Collection $items
     * @return Order
     */
    public function setItems(Collection $items): self
    {
        $this->items = $items;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getOrderFio(): ?string
    {
        return $this->orderFio;
    }

    /**
     * @param string|null $orderFio
     * @return Order
     */
    public function setOrderFio(?string $orderFio): self
    {
        $this->orderFio = $orderFio;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getOrderPhone(): ?string
    {
        return $this->orderPhone;
    }

    /**
     * @param string|null $orderPhone
     * @return Order
     */
    public function setOrderPhone(?string $orderPhone): self
    {
        $this->orderPhone = $orderPhone;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getOrderEmail(): ?string
    {
        return $this->orderEmail;
    }

    /**
     * @param string|null $orderEmail
     * @return Order
     */
    public function setOrderEmail(?string $orderEmail): self
    {
        $this->orderEmail = $orderEmail;
        return $this;
    }

    public function getPromocode(): ?string
    {
        return $this->promocode;
    }

    public function setPromocode(?string $code): self
    {
        $this->promocode = $code;
        return $this;
    }

    /**
     * @return bool
     */
    public function getFastOrder(): bool
    {
        return $this->fastOrder;
    }

    /**
     * @param bool $fastOrder
     */
    public function setFastOrder(bool $fastOrder): void
    {
        $this->fastOrder = $fastOrder;
    }

    /**
     * @return string
     */
    public function getBusinessArea(): ?string
    {
        return $this->businessArea;
    }

    /**
     * @param string|null $businessArea
     */
    public function setBusinessArea(?string $businessArea): self
    {
        $this->businessArea = $businessArea;
        return $this;
    }

    /**
     * Возвращает тип цены товара
     * @param OrderItem $item
     * @return string|null
     */
    private function getPriceType(Cart $order): ?string
    {
        if (!$this->em->getRepository(Price::class)) {
            return null;
        }

        /** @var Price $defaultPrice */
        $defaultPrice = $this->em->getRepository(Price::class)->findOneBy(['isDefault' => true]);

        if(!$defaultPrice) {
            return null;
        }

        if(!$order->getCustomer()) {
            return $defaultPrice->getUuid();
        }

        /** @var Price $customerPrice */
        $customerPrice = $order->getCustomer()->getPriceType();

        if (empty($customerPrice)) {
            return $defaultPrice->getUuid();
        }

        return $customerPrice->getUuid();
    }

}