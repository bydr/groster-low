<?php

namespace App\Entity;

use App\Repository\DiscountRepository;
use DateTime;
use DateTimeInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\SerializedName;

/**
 * @ORM\Entity(repositoryClass=DiscountRepository::class)
 * @ORM\Table(uniqueConstraints={
 *     @ORM\UniqueConstraint(name="unique_discount_code", columns={"code"}),
 * })
 */
class Discount
{

    public const TYPE_RATE = 1;     // скидка в процентах
    public const TYPE_MONEY = 2;    // скидка в рублях

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @SerializedName("id")
     * @Groups({"admin-promo-list", "admin-promo"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @SerializedName("name")
     * @Groups({"admin-promo-list", "admin-promo", "promo-1c"})
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=20)
     * @SerializedName("code")
     * @Groups({"admin-promo-list", "admin-promo", "promo-1c"})
     */
    private $code;

    /**
     * @ORM\Column(type="smallint")
     * @SerializedName("type")
     * @Groups({"admin-promo-list", "admin-promo", "promo-1c"})
     */
    private $type;

    /**
     * @ORM\Column(type="float")
     * @SerializedName("value")
     * @Groups({"admin-promo-list", "admin-promo", "promo-1c"})
     */
    private $value;

    /**
     * @ORM\Column(type="boolean", options={"defult":true})
     * @SerializedName("reusable")
     * @Groups({"admin-promo-list", "admin-promo", "promo-1c"})
     */
    private $reusable;

    /**
     * @ORM\Column(type="datetime")
     */
    private $start;

    /**
     * @ORM\Column(type="datetime")
     */
    private $finish;

    /**
     * @ORM\ManyToMany(targetEntity=Category::class)
     */
    private $categories;

    /**
     * @SerializedName("start")
     * @Groups({"admin-promo-list", "admin-promo", "promo-1c"})
     */
    public function getStartTime(): string
    {
        return $this->getStart()->format(DateTimeInterface::ISO8601);
    }

    /**
     * @SerializedName("finish")
     * @Groups({"admin-promo-list", "admin-promo", "promo-1c"})
     */
    public function getFinishTime(): string
    {
        return $this->getFinish()->format(DateTimeInterface::ISO8601);
    }

    /**
     * Список Uuid категорий
     * @SerializedName("categories")
     * @Groups({"admin-promo", "promo-1c"})
     * @return array
     */
    public function getCategoriesUuid(): array
    {
        $categories = [];
        /** @var Category $category */
        foreach ($this->categories as $category) {
            $categories[] = $category->getUuid();
        }

        return $categories;
    }

    public function __construct()
    {
        $this->reusable = true;
        $this->categories = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getCode(): ?string
    {
        return $this->code;
    }

    public function setCode(string $code): self
    {
        $this->code = $code;

        return $this;
    }

    public function getType(): ?int
    {
        return $this->type;
    }

    public function setType(int $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getValue(): ?float
    {
        return $this->value;
    }

    public function setValue(float $value): self
    {
        $this->value = $value;

        return $this;
    }

    public function getReusable(): ?bool
    {
        return $this->reusable;
    }

    public function setReusable(bool $reusable): self
    {
        $this->reusable = $reusable;

        return $this;
    }

    public function getStart(): ?\DateTimeInterface
    {
        return $this->start;
    }

    public function setStart(\DateTimeInterface $start): self
    {
        $this->start = $start;

        return $this;
    }

    public function getFinish(): ?\DateTimeInterface
    {
        return $this->finish;
    }

    public function setFinish(\DateTimeInterface $finish): self
    {
        $this->finish = $finish;

        return $this;
    }

    /**
     * @return Collection|Category[]
     */
    public function getCategories(): Collection
    {
        return $this->categories;
    }

    public function hasCategory(Category $category): bool
    {
        return $this->categories->contains($category);
    }

    public function addCategory(Category $category): self
    {
        if (!$this->hasCategory($category)) {
            $this->categories[] = $category;
        }

        return $this;
    }

    public function removeCategory(Category $category): self
    {
        $this->categories->removeElement($category);

        return $this;
    }

    /**
     * Список id категорий
     * @return array
     */
    public function getCategoryIds(): array
    {
        $ids = [];
        foreach ($this->categories as $category) {
            $ids[] = $category->getId();
        }

        return $ids;
    }

    /**
     * Проверка, что промокод можно использовать
     * @return bool
     */
    public function isActive(): bool
    {
        $now = new DateTime();
        return $this->start <= $now && $this->finish > $now;
    }

}
