<?php

namespace App\Entity;

use App\Repository\BannerRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\SerializedName;

/**
 * @ORM\Entity(repositoryClass=BannerRepository::class)
 */
class Banner
{
    const TYPE_SLIDER = 1;
    const TYPE_HOME = 2;
    const TYPE_PRODUCT_LIST = 3;
    const TYPE_CATALOG = 4;

    const FOLDER = 'images/banners';

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @SerializedName("id")
     * @Groups({"admin-banners"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @SerializedName("desktop")
     * @Groups({"banners", "admin-banners"})
     */
    private $desktop;

    /**
     * @ORM\Column(type="string", length=255)
     * @SerializedName("tablet")
     * @Groups({"banners", "admin-banners"})
     */
    private $tablet;

    /**
     * @ORM\Column(type="string", length=255)
     * @SerializedName("mobile")
     * @Groups({"banners", "admin-banners"})
     */
    private $mobile;

    /**
     * @ORM\Column(type="smallint")
     * @SerializedName("type")
     * @Groups({"banners", "admin-banners"})
     */
    private $type;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @SerializedName("url")
     * @Groups({"banners", "admin-banners"})
     */
    private $url;

    /**
     * @ORM\Column(type="integer", options={"default":1})
     * @SerializedName("weight")
     * @Groups({"banners", "admin-banners"})
     */
    private $weight;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $updatedAt;

    public function __construct()
    {
        $this->weight = 1;
        $this->createdAt = new \DateTime();
        $this->desktop = '';
        $this->tablet = '';
        $this->mobile = '';
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDesktop(): ?string
    {
        return $this->desktop;
    }

    public function setDesktop(string $desktop): self
    {
        $this->desktop = $desktop;

        return $this;
    }

    public function getTablet(): ?string
    {
        return $this->tablet;
    }

    public function setTablet(string $tablet): self
    {
        $this->tablet = $tablet;

        return $this;
    }

    public function getMobile(): ?string
    {
        return $this->mobile;
    }

    public function setMobile(string $mobile): self
    {
        $this->mobile = $mobile;

        return $this;
    }

    public function getType(): ?int
    {
        return $this->type;
    }

    public function setType(int $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getUrl(): ?string
    {
        return $this->url;
    }

    public function setUrl(?string $url): self
    {
        $this->url = $url;

        return $this;
    }

    public function getWeight(): ?int
    {
        return $this->weight;
    }

    public function setWeight(int $weight): self
    {
        $this->weight = $weight;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(?\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }
}
