<?php

namespace App\Entity;

use App\Repository\ProductValueRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ProductValueRepository::class)
 */
class ProductValue
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Product::class, inversedBy="productValues")
     * @ORM\JoinColumn(nullable=false)
     */
    private $product;

    /**
     * @ORM\ManyToOne(targetEntity=Value::class)
     * @ORM\JoinColumn(nullable=false)
     */
    private $value;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isFilter;

    /**
     * @ORM\Column(type="smallint", options={"default": 0})
     */
    private $weight;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getProduct(): ?Product
    {
        return $this->product;
    }

    public function setProduct(?Product $product): self
    {
        $this->product = $product;

        return $this;
    }

    public function getValue(): ?Value
    {
        return $this->value;
    }

    public function setValue(?Value $value): self
    {
        $this->value = $value;

        return $this;
    }

    public function getIsFilter(): ?bool
    {
        return $this->isFilter;
    }

    public function setIsFilter(bool $isFilter): self
    {
        $this->isFilter = $isFilter;

        return $this;
    }

    public function getWeight(): ?int
    {
        return $this->weight;
    }

    public function setWeight(int $weight): self
    {
        $this->weight = $weight;

        return $this;
    }
}
