<?php

namespace App\Entity;

use App\Repository\FaqTypeRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\SerializedName;

/**
 * @ORM\Entity(repositoryClass=FaqTypeRepository::class)
 */
class FaqType
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     *
     * @SerializedName("id")
     * @Groups({"admin-faq-block"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     *
     * @SerializedName("name")
     * @Groups({"faq", "admin-faq-block"})
     */
    private $name;

    /**
     * @ORM\Column(type="smallint")
     *
     * @SerializedName("weight")
     * @Groups({"admin-faq-block"})
     */
    private $weight;

    /**
     * @ORM\OneToMany(targetEntity=Faq::class, mappedBy="type", orphanRemoval=true)
     * @ORM\OrderBy({"weight": "ASC", "id": "DESC"})
     *
     * @SerializedName("questions")
     * @Groups({"faq"})
     */
    private $faqs;

    public function __construct()
    {
        $this->faqs = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getWeight(): ?int
    {
        return $this->weight;
    }

    public function setWeight(int $weight): self
    {
        $this->weight = $weight;

        return $this;
    }

    /**
     * @return Collection|Faq[]
     */
    public function getFaqs(): Collection
    {
        return $this->faqs;
    }

    public function addFaq(Faq $faq): self
    {
        if (!$this->faqs->contains($faq)) {
            $this->faqs[] = $faq;
            $faq->setType($this);
        }

        return $this;
    }

    public function removeFaq(Faq $faq): self
    {
        if ($this->faqs->removeElement($faq)) {
            // set the owning side to null (unless already changed)
            if ($faq->getType() === $this) {
                $faq->setType(null);
            }
        }

        return $this;
    }
}
