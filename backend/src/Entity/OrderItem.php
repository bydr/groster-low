<?php

namespace App\Entity;

use App\Repository\OrderItemRepository;
use DateTime;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\SerializedName;

/**
 * @ORM\Entity(repositoryClass=OrderItemRepository::class)
 */
class OrderItem
{
    const MAX_SAMPLE_QTY = 2;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Order::class, inversedBy="items",cascade={"persist"})
     * @ORM\JoinColumn(name="order_id", referencedColumnName="id", nullable=false)
     */
    private $order;

    /**
     * @ORM\ManyToOne(targetEntity=Product::class,cascade={"persist"})
     * @ORM\JoinColumn(name="product_id", referencedColumnName="id", nullable=false)
     */
    private $product;

    /**
     * @ORM\Column(type="integer")
     * @SerializedName("quantity")
     * @Groups({"cart", "account-order-detail"})
     */
    private $quantity;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createAt;

    /**
     * Цена товара
     * @ORM\Column(type="integer", options={"default": 0})
     */
    private $price;

    /**
     * Сумма скидки для товара
     * @ORM\Column(type="integer", options={"default": 0})
     */
    private $discount;

    /**
     * Стоимость товаров в заказе
     * @ORM\Column(type="integer")
     * @SerializedName("total_cost")
     * @Groups({"account-order-detail"})
     */
    private $total;

    /**
     * @ORM\Column(type="smallint", options={"default":0})
     * @SerializedName("sample")
     * @Groups({"cart", "account-order-detail"})
     */
    private $sample;

    /**
     * @ORM\ManyToOne(targetEntity=Product::class)
     */
    private $parent;

    /**
     * @SerializedName("uuid")
     * @Groups({"cart", "account-order-detail"})
     */
    public function getProductUuid(): string
    {
        return $this->getProduct()->getUuid();
    }

    /**
     * Возвращает uuid комплекта
     * @return string|null
     *
     * @SerializedName("parent")
     * @Groups({"cart", "account-order-detail"})
     */
    public function getParentUuid(): ?string
    {
        return $this->getParent() ? $this->getParent()->getUuid() : null;
    }

    public function __construct()
    {
        $this->createAt = new DateTime();
        $this->sample = 0;
        $this->total = 0;
        $this->discount = 0;
        $this->quantity = 0;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getOrder(): ?Order
    {
        return $this->order;
    }

    public function setOrder(?Order $order): self
    {
        $this->order = $order;

        return $this;
    }

    public function getProduct(): ?Product
    {
        return $this->product;
    }

    public function setProduct(?Product $product): self
    {
        $this->product = $product;

        return $this;
    }

    public function getQuantity(): int
    {
        return $this->quantity;
    }

    public function setQuantity(int $quantity): self
    {
        $this->quantity = $quantity;
        $this->updateTotal();

        return $this;
    }

    public function getCreateAt(): \DateTime
    {
        return $this->createAt;
    }

    public function setCreateAt(\DateTime $createAt): self
    {
        $this->createAt = $createAt;

        return $this;
    }

    public function getTotal(): int
    {
        return $this->total;
    }

    public function setTotal(int $total): self
    {
        $this->total = $total;

        return $this;
    }

    /**
     * Обновление общей стоимости товара с учетом заказа образца
     * @return $this
     */
    public function updateTotal(): self
    {
        // Изменяем, только если это не комплект
        if($this->getProduct() && !$this->getProduct()->isKit()) {
            $qty = $this->quantity;
            $qty += $this->sample;

            $this->total = $this->price * $qty - $this->discount;
        }


        return $this;
    }

    public function getSample(): int
    {
        return $this->sample;
    }

    public function setSample(int $sample): self
    {
        $this->sample = $sample;
        $this->updateTotal();

        return $this;
    }

    public function getParent(): ?Product
    {
        return $this->parent;
    }

    public function setParent(?Product $parent): self
    {
        $this->parent = $parent;

        return $this;
    }

    public function getDiscount(): ?int
    {
        return $this->discount;
    }

    public function setDiscount(int $discount): self
    {
        $this->discount = $discount;
        $this->updateTotal();

        return $this;
    }

    public function getPrice(): ?int
    {
        return $this->price;
    }

    public function setPrice(int $price): self
    {
        $this->price = $price;
        $this->updateTotal();

        return $this;
    }
}
