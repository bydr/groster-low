<?php

namespace App\Entity;

use App\Entity\Security\AuthUser;
use App\Helpers\OrderHelper;
use App\Helpers\Params;
use App\Repository\OrderRepository;
use DateTime;
use DateTimeInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\SerializedName;

/**
 * @ORM\Entity(repositoryClass=OrderRepository::class)
 * @ORM\Table(name="`order`")
 */
class Order
{
    /** Изменил здесь - измени в контрактах */
    const STATE_CART = 1;   // корзина
    const STATE_NEW = 2;    // заказ оформлен
    const STATE_FINISHED = 3;     // заказ доставлен
    const STATE_CANCELED = 4;     // заказ отменен
    const STATE_PROCESSING = 5;     // заказ в обработке в 1С
    const STATE_COURIER = 6;     // заказ выдан курьеру

    /**
     * Частота уведомления о необходимости повторить заказ
     */
    const SUBSCRIBE_DAILY = 1;      // ежедневно
    const SUBSCRIBE_WEEKLY = 2;     // каждые 2 недели
    const SUBSCRIBE_MONTHLY = 3;    // 1-е число каждого месяца

    const UID_PREFIX = 'gcart';

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @SerializedName("number")
     * @Groups({"account-orders", "account-order-detail"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=28)
     * @SerializedName("cart")
     * @Groups({"change-qty", "delete-product"})
     */
    private $uid;

    /**
     * @ORM\Column(type="smallint")
     * @SerializedName("state")
     * @Groups({"account-orders", "account-order-detail"})
     */
    private $state;

    /**
     * @ORM\Column(name="create_at", type="datetime")
     */
    private $createAt;

    /**
     * @ORM\Column(name="update_at", type="datetime", nullable=true)
     */
    private $updateAt;

    /**
     * @ORM\Column(name="checkout_complete_at", type="datetime", nullable=true)
     */
    private $checkoutCompleteAt;

    /**
     * @ORM\Column(type="integer")
     */
    private $total;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $comment;

    /**
     * @ORM\ManyToOne(targetEntity=AuthUser::class,cascade={"persist"})
     */
    private $customer;

    /**
     * @ORM\OneToMany(targetEntity=OrderItem::class, mappedBy="order", orphanRemoval=true)
     */
    private $items;

    /**
     * @var Discount
     * @ORM\ManyToOne(targetEntity=Discount::class)
     */
    private $discount;

    /**
     * @ORM\Column(type="integer", nullable=true)
     * @SerializedName("discount")
     * @Groups({"cart", "change-qty", "checkout", "promo-add", "delete-product", "account-order-detail"})
     */
    private $discountValue;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @SerializedName("fio")
     * @Groups({"last-order", "account-order-detail"})
     */
    private $fio;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @SerializedName("email")
     * @Groups({"last-order", "account-order-detail"})
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=15, nullable=true)
     * @SerializedName("phone")
     * @Groups({"last-order", "account-order-detail"})
     */
    private $phone;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @SerializedName("bussiness_area")
     * @Groups({"last-order", "account-order-detail"})
     */
    private $bussinessArea;

    /**
     * @ORM\ManyToOne(targetEntity=ShippingMethod::class)
     */
    private $shippingMethod;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $shippingDate;

    /**
     * @ORM\Column(type="integer", options={"default":0})
     * @SerializedName("shipping_cost")
     * @Groups({"account-order-detail"})
     */
    private $shippingCost;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @SerializedName("region")
     * @Groups({"last-order"})
     */
    private $region;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @SerializedName("shipping_address")
     * @Groups({"last-order", "account-order-detail"})
     */
    private $shippingAddress;

    /**
     * @ORM\ManyToOne(targetEntity=ReplacementMethod::class)
     * @SerializedName("replacement")
     * @Groups({"last-order"})
     */
    private $replacement;

    /**
     * @ORM\ManyToOne(targetEntity=Payer::class)
     */
    private $payer;

    /**
     * @ORM\Column(type="string", length="10", nullable=true)
     */
    private $crmId;

    /**
     * @ORM\Column(type="boolean", options={"default":false})
     */
    private $isFast;

    /**
     * @ORM\Column(type="integer", nullable=true)
     * @SerializedName("subscribe_interval")
     * @Groups({"account-orders", "account-order-detail"})
     */
    private $subscribeInterval;

    /**
     * @ORM\ManyToOne(targetEntity=PaymentMethod::class)
     */
    private $paymentMethod;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $sent_at_1c;

    /**
     * @SerializedName("products")
     * @Groups({"cart", "account-order-detail"})
     */
    public function getUnits()
    {
        return $this->getItems();
    }

    public function getReplacementMethod()
    {
        return $this->getReplacement()->getTitle();
    }

    /**
     * Примененный промокод
     * @return string|null
     *
     * @SerializedName("promocode")
     * @Groups({"cart", "change-qty", "checkout"})
     */
    public function getPromocode(): ?string
    {
        return $this->discount ? $this->discount->getCode() : null;
    }

    /**
     * Дата создания заказа
     * @return string
     * @SerializedName("create_at")
     * @Groups({"account-orders", "account-order-detail"})
     */
    public function getCheckoutAt(): string
    {
        return $this->getCheckoutCompleteAt()->format(DateTimeInterface::ISO8601);
    }

    /**
     * Дата создания заказа
     * @return string|null
     * @SerializedName("shipping_date")
     * @Groups({"account-order-detail"})
     */
    public function getShippingDateString(): ?string
    {
        if($this->shippingDate) {
            return $this->getShippingDate()->format(DateTimeInterface::ISO8601);
        }

        return null;
    }

    /**
     * Название способа доставки
     * @return string|null
     * @SerializedName("shipping_method")
     * @Groups({"account-order-detail"})
     */
    public function getShippingMethodName(): ?string
    {
        if($this->shippingMethod) {
            return $this->getShippingMethod()->getName();
        }

        return null;
    }

    /**
     * Название способа оплаты
     * @return string|null
     * @SerializedName("payment_method")
     * @Groups({"account-order-detail"})
     */
    public function getPaymentMethodName(): ?string
    {
        if($this->paymentMethod) {
            return $this->getPaymentMethod()->getName();
        }

        return null;
    }

    /**
     * Дата создания заказа
     * @return string|null
     * @SerializedName("payer")
     * @Groups({"account-order-detail"})
     */
    public function getPayerName(): ?string
    {
        if($this->payer) {
            return $this->getPayer()->getName();
        }

        return null;
    }

    /**
     * @SerializedName("shipping_method")
     * @Groups({"last-order"})
     * @return string|null
     */
    public function getShippingMethodUuid(): ?string
    {
        return $this->shippingMethod ? $this->getShippingMethod()->getAlias() : null;
    }

    /**
     * @SerializedName("payment_method")
     * @Groups({"last-order"})
     * @return string|null
     */
    public function getPaymentMethodUuid(): ?string
    {
        return $this->getPaymentMethod() ? $this->getPaymentMethod()->getUid() : null;
    }

    /**
     * @SerializedName("payer")
     * @Groups({"last-order"})
     * @return string|null
     */
    public function getPayerUid(): ?string
    {
        return $this->getPayer() ? $this->getPayer()->getUid() : null;
    }

    public function __construct(?AuthUser $customer = null)
    {
        $this->uid = uniqid(self::UID_PREFIX, true);
        $this->createAt = new DateTime();
        $this->state = self::STATE_CART;
        $this->items = new ArrayCollection();
        $this->customer = $customer;
        $this->total = 0;
        $this->shippingCost = 0;
        $this->isFast = false;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): self
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string|null
     * @SerializedName("uid")
     * @Groups({"account-orders"})
     */
    public function getUid(): ?string
    {
        return $this->uid;
    }

    public function getState(): ?int
    {
        return $this->state;
    }

    public function setState(int $state): self
    {
        $this->state = $state;

        return $this;
    }

    public function getCreateAt(): ?\DateTimeInterface
    {
        return $this->createAt;
    }

    public function setCreateAt(\DateTimeInterface $createAt): self
    {
        $this->createAt = $createAt;

        return $this;
    }

    public function getUpdateAt(): ?\DateTimeInterface
    {
        return $this->updateAt;
    }

    public function setUpdateAt(?\DateTimeInterface $updateAt): self
    {
        $this->updateAt = $updateAt;

        return $this;
    }

    public function getCheckoutCompleteAt(): ?\DateTimeInterface
    {
        return $this->checkoutCompleteAt;
    }

    public function setCheckoutCompleteAt(?\DateTimeInterface $checkoutCompleteAt): self
    {
        $this->checkoutCompleteAt = $checkoutCompleteAt;

        return $this;
    }

    public function getTotal(): ?int
    {
        return $this->total;
    }

    public function setTotal(int $total): self
    {
        $this->total = $total;

        return $this;
    }

    public function getComment(): ?string
    {
        return $this->comment;
    }

    public function setComment(?string $comment): self
    {
        $this->comment = $comment;

        return $this;
    }

    public function getCustomer(): ?AuthUser
    {
        return $this->customer;
    }

    public function setCustomer(?AuthUser $customer): self
    {
        $this->customer = $customer;

        return $this;
    }

    /**
     * @return Collection|OrderItem[]
     */
    public function getItems(): Collection
    {
        return $this->items;
    }

    public function addItem(OrderItem $item): self
    {
        if (!$this->items->contains($item)) {
            $this->items[] = $item;
            $item->setOrder($this);
            $this->total += $item->getTotal();
        }

        return $this;
    }

    public function removeItem(OrderItem $item): self
    {
        if ($this->items->removeElement($item)) {
            $this->total -= $item->getTotal();
            // set the owning side to null (unless already changed)
            if ($item->getOrder() === $this) {
                $item->setOrder(null);
            }
        }

        return $this;
    }

    public function hasItem(OrderItem $item): bool
    {
        return $this->items->contains($item);
    }

    /**
     * Проверка возможности редактировать заказ
     * @return bool
     */
    public function isEditable(): bool
    {
        if($this->state != self::STATE_CART) {
            return false;
        }

        return true;
    }

    public function getDiscount(): ?Discount
    {
        return $this->discount;
    }

    public function setDiscount(?Discount $discount): self
    {
        $this->discount = $discount;

        return $this;
    }

    public function getDiscountValue(): ?int
    {
        return $this->discountValue;
    }

    public function setDiscountValue(?int $discountValue): self
    {
        $this->discountValue = $discountValue;

        return $this;
    }

    public function getFio(): ?string
    {
        return $this->fio;
    }

    public function setFio(string $fio): self
    {
        $this->fio = $fio;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getPhone(): ?string
    {
        return $this->phone;
    }

    public function setPhone(string $phone): self
    {
        $this->phone = $phone;

        return $this;
    }

    public function getBussinessArea(): ?string
    {
        return $this->bussinessArea;
    }

    public function setBussinessArea(string $bussinessArea): self
    {
        $this->bussinessArea = $bussinessArea;

        return $this;
    }

    public function getShippingMethod(): ?ShippingMethod
    {
        return $this->shippingMethod;
    }

    public function setShippingMethod(?ShippingMethod $shippingMethod): self
    {
        $this->shippingMethod = $shippingMethod;

        return $this;
    }

    public function getShippingDate(): ?\DateTimeInterface
    {
        return $this->shippingDate;
    }

    public function setShippingDate(?\DateTimeInterface $shippingDate): self
    {
        $this->shippingDate = $shippingDate;

        return $this;
    }

    public function getShippingCost(): ?int
    {
        return $this->shippingCost;
    }

    public function setShippingCost(int $shippingCost): self
    {
        $this->shippingCost = $shippingCost;

        return $this;
    }

    public function getRegion(): ?string
    {
        return $this->region;
    }

    public function setRegion(string $region): self
    {
        $this->region = $region;

        return $this;
    }

    public function getShippingAddress(): ?string
    {
        return $this->shippingAddress;
    }

    public function setShippingAddress(string $shippingAddress): self
    {
        $this->shippingAddress = $shippingAddress;

        return $this;
    }

    public function getReplacement(): ?ReplacementMethod
    {
        return $this->replacement;
    }

    public function setReplacement(?ReplacementMethod $replacement): self
    {
        $this->replacement = $replacement;

        return $this;
    }

    public function getPayer(): ?Payer
    {
        return $this->payer;
    }

    public function setPayer(?Payer $payer): self
    {
        $this->payer = $payer;

        return $this;
    }

    public function getCrmId(): ?string
    {
        return $this->crmId;
    }

    public function setCrmId(?string $crmId): self
    {
        $this->crmId = $crmId;

        return $this;
    }

    public function getIsFast(): bool
    {
        return $this->isFast;
    }

    public function setIsFast(bool $isFast): self
    {
        $this->isFast = $isFast;

        return $this;
    }

    public function getSubscribeInterval(): ?int
    {
        return $this->subscribeInterval;
    }

    public function setSubscribeInterval(?int $subscribeInterval): self
    {
        $this->subscribeInterval = $subscribeInterval;

        return $this;
    }

    public function getPaymentMethod(): ?PaymentMethod
    {
        return $this->paymentMethod;
    }

    public function setPaymentMethod(?PaymentMethod $paymentMethod): self
    {
        $this->paymentMethod = $paymentMethod;

        return $this;
    }

    public function getSentAt1c(): ?\DateTimeInterface
    {
        return $this->sent_at_1c;
    }

    public function setSentAt1c(?\DateTimeInterface $sent_at_1c): self
    {
        $this->sent_at_1c = $sent_at_1c;

        return $this;
    }

    /**
     * Общая сумма заказа по всем элементам корзины.
     * Этот метод нужен, т.к. обновление суммы заказа в БД выполняется через триггер
     * @return int
     * @SerializedName("total_cost")
     * @Groups({"cart", "change-qty", "checkout", "promo-add", "promo-remove", "delete-product", "account-orders", "account-order-detail"})
     */
    public function getItemsTotal(): int
    {
        $total = 0;

        foreach ($this->getItems() as $item) {
            $total += $item->getTotal();
        }

        return $total;
    }

    /**
     * Общая скидка по всем элементам корзины.
     * Этот метод нужен, т.к. обновление скидки заказа в БД выполняется через триггер
     * @return int
     */
    public function getItemsDiscount(): int
    {
        $total = 0;

        foreach ($this->getItems() as $item) {
            $total += $item->getDiscount();
        }

        return $total;
    }

    public function getStateTitle(): string
    {
        $states = [
            self::STATE_CART => 'Корзина',
            self::STATE_NEW => 'Оформлен',
            self::STATE_FINISHED => 'Доставлен',
            self::STATE_CANCELED => 'Отменен',
            self::STATE_PROCESSING => 'В обработке',
            self::STATE_COURIER => 'Передан курьеру',
        ];

        return $states[$this->state] ?: "";
    }

    public function getShippingMethodTitle(): string
    {
        return OrderHelper::getShippingMethodTitle($this->shippingMethod->getAlias());
    }
}
