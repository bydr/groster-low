<?php

namespace App\Entity;

use App\Controller\ApiController;
use App\Helpers\Params;
use App\Repository\ProductRepository;
use DateTime;
use DateTimeInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\SerializedName;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=ProductRepository::class)
 * @ORM\Table(uniqueConstraints={
 *     @ORM\UniqueConstraint(name="unique_product_uuid", columns={"uuid"}),
 *     @ORM\UniqueConstraint(name="unique_product_alias", columns={"alias"}),
 * })
 */
class Product
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=50)
     * @SerializedName("uuid")
     * @Assert\Type("string")
     * @Groups({"catalog", "product-list", "product"})
     */
    private $uuid;

    /**
     * @ORM\Column(type="datetime", nullable=false)
     */
    private $createAt;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $updateAt;

    /**
     * @ORM\Column(type="string", length=255)
     * @return string
     * @SerializedName("name")
     * @Groups({"catalog", "product-list", "product"})
     */
    private $name;

    /**
     * Хранится базовая цена
     * @ORM\Column(type="integer", nullable=true)
     * @SerializedName("price")
     * @Assert\Type("int")
     * @Groups({"catalog", "product-list", "product"})
     */
    private $price;

    /**
     * @ORM\OneToMany(targetEntity=ProductImage::class, mappedBy="product", orphanRemoval=true)
     */
    private $gallery;

    /**
     * @ORM\ManyToMany(targetEntity=Category::class, inversedBy="products")
     * @Assert\All({
     *   @Assert\Type("App\Entity\Category")
     * })
     */
    private $categoriesUuid;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isActive;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @SerializedName("description")
     * @Groups({"product"})
     */
    private $description;

    /**
     * @ORM\Column(type="string", length=10)
     * @SerializedName("unit")
     * @Groups({"catalog", "product-list", "product"})
     */
    private $unit;

    /**
     * @ORM\Column(type="string", length=15)
     * @SerializedName("code")
     * @Groups({"catalog", "product", "product-list"})
     */
    private $code;

    /**
     * Продается только тарами
     * @ORM\Column(type="boolean")
     */
    private $isSellUnit;

    /**
     * Состав комплекта
     * @ORM\JoinTable(name="kit")
     * @ORM\ManyToMany(targetEntity=Product::class, inversedBy="children")
     */
    private $children;

    /**
     * @ORM\JoinTable(name="kit",
     *     joinColumns={@ORM\JoinColumn(name="product_target", referencedColumnName="id")},
     *     inverseJoinColumns={@ORM\JoinColumn(name="product_source", referencedColumnName="id", unique=true)}
     * )
     * @ORM\ManyToMany(targetEntity=Product::class, inversedBy="parentKit", mappedBy="children")
     */
    private $parentKit;

    /**
     * Тара
     * @ORM\OneToMany(targetEntity=Container::class, mappedBy="product", orphanRemoval=true)
     */
    private $productContainers;

    /**
     * Остатки
     * @ORM\OneToMany(targetEntity=Rest::class, mappedBy="product", orphanRemoval=true)
     */
    private $rests;

    /**
     * Родитель для вариаций
     * @ORM\ManyToOne(targetEntity=Product::class, inversedBy="productVariations")
     */
    private $collectionParent;

    /**
     * Вариации для товара
     * @ORM\OneToMany(targetEntity=Product::class, mappedBy="collectionParent")
     */
    private $productVariations;

    /**
     * @ORM\Column(type="integer", options={"default":0})
     * @SerializedName("total_qty")
     * @Groups({"catalog", "product-list", "product"})
     */
    private $totalQty;

    /**
     * Остатки на основном складе (быстрая доставка)
     * @ORM\Column(type="integer", options={"default":0})
     * @SerializedName("fast_qty")
     * @Groups({"catalog", "product", "product-list"})
     */
    private $fastQty;

    /**
     * @ORM\Column(type="string", length=10, nullable=true)
     */
    private $weight;

    /**
     * @ORM\Column(type="string", length=10, nullable=true)
     */
    private $length;

    /**
     * @ORM\Column(type="string", length=10, nullable=true)
     */
    private $width;

    /**
     * @ORM\Column(type="string", length=10, nullable=true)
     */
    private $height;

    /**
     * @ORM\Column(type="string", length=10, nullable=true)
     */
    private $size;

    /**
     * @ORM\Column(type="boolean", options={"default":false})
     * @SerializedName("is_bestseller")
     * @Groups({"catalog", "product", "product-list"})
     */
    private $isBestseller;

    /**
     * @ORM\Column(type="string", length=255)
     * @SerializedName("alias")
     * @Assert\Type("string")
     * @Groups({"catalog", "product-list", "product"})
     */
    private $alias;

    /**
     * @ORM\OneToMany(targetEntity=ProductPrice::class, mappedBy="product", orphanRemoval=true)
     */
    private $prices;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @SerializedName("meta_title")
     * @Assert\Type("string")
     * @Groups({"product"})
     */
    private $metaTitle;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @SerializedName("meta_description")
     * @Assert\Type("string")
     * @Groups({"product"})
     */
    private $metaDescription;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @SerializedName("keywords")
     * @Groups({"catalog", "product-list", "product"})
     */
    private $keywords;

    /**
     * @ORM\OneToMany(targetEntity=ProductAnalogs::class, mappedBy="productSource", orphanRemoval=true)
     * @ORM\OrderBy({"weight"="ASC"})
     */
    private $analogs;

    /**
     * @ORM\OneToMany(targetEntity=ProductCompanion::class, mappedBy="parent")
     * @ORM\OrderBy({"weight"="ASC"})
     */
    private $companions;

    /**
     * @ORM\OneToMany(targetEntity=ProductValue::class, mappedBy="product", orphanRemoval=true)
     * @ORM\OrderBy({"weight" = "ASC", "id" = "ASC"})
     */
    private $productValues;

    /**
     * @ORM\Column(type="boolean", options={"default":false})
     * @SerializedName("is_new")
     * @Groups({"catalog", "product", "product-list"})
     */
    private $isNew;

    /**
     * @ORM\Column(type="boolean", options={"default":false})
     * @SerializedName("allow_sample")
     * @Groups({"catalog", "product", "product-list"})
     */
    private $allowSample;

    /**
     * Список Uuid категорий
     * @SerializedName("categories")
     * @Groups({"catalog", "product-list", "product"})
     * @return array
     */
    public function getCategories(): array
    {
        $categories = [];
        /** @var Category $category */
        foreach ($this->categoriesUuid as $category) {
            if(Category::TYPE_BUSSINESS_AREA == $category->getType()) {
                continue;
            }
            $categories[] = $category->getUuid();
        }

        return $categories;
    }

    /**
     * Список Uuid сфер бизнеса
     * @SerializedName("business_areas")
     * @Groups({"catalog"})
     * @return array
     */
    public function getBusinessAreas(): array
    {
        $categories = [];
        /** @var Category $category */
        foreach ($this->categoriesUuid as $category) {
            if(Category::TYPE_PRODUCT_SECTION == $category->getType()) {
                continue;
            }
            $categories[] = $category->getUuid();
        }

        return $categories;
    }

    /**
     * Картинки товара
     * @SerializedName("images")
     * @Assert\Type("string")
     * @Groups({"product","catalog", "product-list", "product"})
     */
    public function getImages(): array
    {
        $images = [];
        /** @var ProductImage $image */
        foreach ($this->gallery as $image) {
            if (!$image->getIsActive()) {
                continue;
            }
            if($image->getIsMain()) {
                array_unshift($images, $image->getPath());
            } else {
                array_push($images, $image->getPath());
            }
        }

        return $images;
    }

    /**
     * @SerializedName("params")
     * @Groups({"catalog", "product"})
     */
    public function getProductParams(): array
    {
        $params = [];
        /** @var ProductValue $productValue */
        foreach ($this->productValues as $productValue) {
            $params[] = $productValue->getValue()->getUuid();
        }

        return $params;
    }

    /**
     * @SerializedName("containers")
     * @Groups({"catalog", "product-list", "product"})
     */
    public function getContainers(): array
    {
        // Если это вариация, то возвращаем родительские
        if ($this->getCollectionParent()) {
            return $this->getCollectionParent()->getContainers();
        }

        $containers['only_containers'] = $this->isSellUnit;

        /** @var Container $container */
        foreach ($this->productContainers as $key => $container) {
            if(!$container->getIsActive()) {
                continue;
            }

            $containers['units'][$key]['name'] = $container->getName();
            $containers['units'][$key]['value'] = $container->getRate();
        }

        return $containers;
    }

    /**
     * @SerializedName("analogs")
     * @Groups({"catalog", "product-list", "product"})
     */
    public function getProductAnalogs(): array
    {
        $analogs = [
            Params::FAST => [],
            Params::OTHER => [],
        ];
        /** @var ProductAnalogs $analog */
        foreach ($this->analogs as $analog) {
            $product = $analog->getProductTarget();
            if(!$product->getIsActive()) {
                continue;
            }

            $key = $product->getFastQty() > 0 ? Params::FAST : Params::OTHER;

            $analogs[$key][] = [
                Params::UUID => $product->getUuid(),
                Params::WEIGHT => $analog->getWeight(),
            ];
        }

        return $analogs;
    }

    /**
     * @SerializedName("companions")
     * @Groups({"catalog", "product-list", "product"})
     */
    public function getProductCompanions(): array
    {
        $companions = [];
        /** @var ProductCompanion $companion */
        foreach ($this->companions as $companion) {
            $product = $companion->getCompanion();
            if(!$product->getIsActive()) {
                continue;
            }
            $companions[] = [
                Params::UUID => $product->getUuid(),
                Params::WEIGHT => $companion->getWeight(),
            ];
        }

        return $companions;
    }

    /**
     * @SerializedName("kit")
     * @Groups({"catalog", "product", "product-list"})
     */
    public function getKit(): array
    {
        $children = [];
        /** @var Product $child */
        foreach ($this->children as $child) {
            if(!$child->getIsActive()) {
                continue;
            }
            $children[] = $child->getUuid();
        }

        return $children;
    }

    /**
     * Родитель для вариации
     * @SerializedName("parent")
     * @Groups({"product", "product-list"})
     */
    public function getParent(): ?string
    {
        return $this->collectionParent ? $this->collectionParent->getUuid() : null;
    }

    /**
     * @SerializedName("variations")
     * @Groups({"product"})
     */
    public function getVariations(): array
    {
        $children = [];
        /** @var Product $child */
        foreach ($this->productVariations as $child) {
            if(!$child->getIsActive()) {
                continue;
            }
            $children[] = $child->getUuid();
        }

        return $children;
    }

    /**
     * Список свойств товара
     *
     * @return array
     * @SerializedName("properties")
     * @Groups({"catalog", "product", "product-list"})
     */
    public function getProperties(): array
    {
        $propsName = ['Weight', 'Length', 'Width', 'Height', 'Size'];
        $props = [];

        foreach ($propsName as $item) {
            $method = 'get' . $item;
            $value = $this->$method();
            if(!empty($value)) {
                $props[] = [
                    'name' => mb_convert_case($item, MB_CASE_LOWER),
                    'value' => $value
                ];
            }
        }

//        return $props;
        //TODO заказчик сказал, что не выводить, а использовать в будущем для расчета стоимости доставки
        return [];
    }

    /**
     * Список складов
     * @return array
     * @SerializedName("stores")
     * @Groups({"catalog", "product", "product-list"})
     */
    public function getStores(): array
    {
        $stores = [];
        /** @var Rest $rest */
        foreach ($this->rests as $rest) {
            $store = $rest->getStore();
            if(!$store->getIsActive() || empty($store->getLatitude()) || empty($store->getLongitude())) {
                continue;
            }

            $stores[$store->getName()] = [
                Params::UUID => $store->getUuid(),
                Params::NAME => $store->getAddress(),
                Params::QUANTITY => $rest->getValue(),
                Params::IS_MAIN => $store->getIsMain(),
            ];
        }

        ksort($stores, SORT_STRING);

        return array_values($stores);
    }

    /**
     * @SerializedName("kit_parents")
     * @Groups({"catalog", "product", "product-list"})
     */
    public function getParentKit(): array
    {
        $parents = [];
        /** @var Product $item */
        foreach ($this->parentKit as $item) {
            if(!$item->getIsActive()) {
                continue;
            }
            $parents[] = $item->getAlias();
        }

        return $parents;
    }

    public function __construct()
    {
        $this->createAt = new DateTime();
        $this->gallery = new ArrayCollection();
        $this->categoriesUuid = new ArrayCollection();
        $this->children = new ArrayCollection();
        $this->productContainers = new ArrayCollection();
        $this->totalQty = 0;
        $this->fastQty = 0;
        $this->rests = new ArrayCollection();
        $this->productVariations = new ArrayCollection();
        $this->isActive = true;
        $this->isBestseller = false;
        $this->isNew = false;
        $this->prices = new ArrayCollection();
        $this->analogs = new ArrayCollection();
        $this->companions = new ArrayCollection();
        $this->productValues = new ArrayCollection();
        $this->allowSample = false;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUuid(): ?string
    {
        return $this->uuid;
    }

    public function setUuid(string $uuid): self
    {
        $this->uuid = $uuid;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getPrice(): ?int
    {
        $user = ApiController::getCurrentUser();
        if(!$user || !$user->getPriceType()) {
            return $this->price;
        }

        $productPrice = $this->getProductPrice($user->getPriceType());

        return $productPrice ? $productPrice->getValue() : $this->price;
    }

    public function setPrice(int $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getTotalQty(): ?int
    {
        return $this->totalQty;
    }

    public function setTotalQty(int $totalQty): self
    {
        $this->totalQty = $totalQty;

        return $this;
    }

    public function getFastQty(): ?int
    {
        return $this->fastQty;
    }

    public function setFastQty(int $fastQty): self
    {
        $this->fastQty = $fastQty;

        return $this;
    }

    /**
     * @return Collection|ProductImage[]
     */
    public function getGallery(): Collection
    {
        return $this->gallery;
    }

    public function addGallery(ProductImage $gallery): self
    {
        if (!$this->gallery->contains($gallery)) {
            $this->gallery[] = $gallery;
            $gallery->setProduct($this);
        }

        return $this;
    }

    public function removeGallery(ProductImage $gallery): self
    {
        if ($this->gallery->removeElement($gallery)) {
            // set the owning side to null (unless already changed)
            if ($gallery->getProduct() === $this) {
                $gallery->setProduct(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Category[]
     */
    public function getCategoriesUuid(): Collection
    {
        $categories = new ArrayCollection();
        /** @var Category $category */
        foreach ($this->categoriesUuid as $category) {
            if(Category::TYPE_PRODUCT_SECTION == $category->getType()) {
                $categories->add($category);
            }
        }

        return $categories;
    }

    /**
     * @return Collection|Category[]
     */
    public function getAreas(): Collection
    {
        $areas = new ArrayCollection();
        /** @var Category $category */
        foreach ($this->categoriesUuid as $category) {
            if(Category::TYPE_BUSSINESS_AREA == $category->getType()) {
                $areas->add($category);
            }
        }

        return $areas;
    }

    public function hasCategory(Category $category): bool
    {
        return $this->categoriesUuid->contains($category);
    }

    public function addCategory(Category $category): self
    {
        if (!$this->hasCategory($category)) {
            $this->categoriesUuid[] = $category;
        }

        return $this;
    }

    public function removeCategory(Category $category): self
    {
        $this->categoriesUuid->removeElement($category);

        return $this;
    }

//    /**
//     * @return Collection|Value[]
//     */
//    public function getParamsUuid(): Collection
//    {
//        return $this->paramsUuid;
//    }
//
//    public function hasParam(Value $param): bool
//    {
//        return $this->paramsUuid->contains($param);
//    }
//
//    public function addParam(Value $param): self
//    {
//        if (!$this->hasParam($param)) {
//            $this->paramsUuid[] = $param;
//            $param->addProduct($this);
//        }
//
//        return $this;
//    }
//
//    public function removeParam(Value $param): self
//    {
//        if ($this->paramsUuid->removeElement($param)) {
//            $param->removeProduct($this);
//        }
//
//        return $this;
//    }


    public function getIsActive(): ?bool
    {
        return $this->isActive;
    }

    public function setIsActive(bool $isActive): self
    {
        $this->isActive = $isActive;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getUnit(): string
    {
        return $this->unit;
    }

    public function setUnit(?string $unit): self
    {
        $this->unit = $unit;

        return $this;
    }

    public function getCode(): string
    {
        return $this->code;
    }

    public function setCode(string $code): self
    {
        $this->code = $code;

        return $this;
    }

    public function isKit(): bool
    {
        return count($this->children) > 0;
    }

    public function getIsSellUnit(): bool
    {
        return $this->isSellUnit;
    }

    public function setIsSellUnit(bool $isSellUnit): self
    {
        $this->isSellUnit = $isSellUnit;

        return $this;
    }

    /**
     * @return Collection|ProductAnalogs[]
     */
    public function getAnalogs(): Collection
    {
        return $this->analogs;
    }

    public function hasAnalog(ProductAnalogs $analog): bool
    {
        return $this->analogs->contains($analog);
    }

    public function addAnalog(ProductAnalogs $analog): self
    {
        if (!$this->analogs->contains($analog)) {
            $this->analogs[] = $analog;
        }

        return $this;
    }

    public function removeAnalog(ProductAnalogs $analog): self
    {
        $this->analogs->removeElement($analog);

        return $this;
    }

    /**
     * @return Collection|self[]
     */
    public function getChildren(): Collection
    {
        return $this->children;
    }

    public function hasChild(self $child): bool
    {
        return $this->children->contains($child);
    }

    public function addChild(self $child): self
    {
        if (!$this->hasChild($child)) {
            $this->children[] = $child;
        }

        return $this;
    }

    public function removeChild(self $child): self
    {
        $this->children->removeElement($child);

        return $this;
    }

    /**
     * @return Collection|Container[]
     */
    public function getProductContainers(): Collection
    {
        return $this->productContainers;
    }

    public function hasContainer(Container $container): bool
    {
        return $this->productContainers->contains($container);
    }

    public function addContainer(Container $container): self
    {
        if (!$this->hasContainer($container)) {
            $this->productContainers[] = $container;
            $container->setProduct($this);
        }

        return $this;
    }

    public function removeContainer(Container $container): self
    {
        if ($this->productContainers->removeElement($container)) {
            // set the owning side to null (unless already changed)
            if ($container->getProduct() === $this) {
                $container->setProduct(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Rest[]
     */
    public function getRests(): Collection
    {
        return $this->rests;
    }

    public function hasRest(Rest $rest): bool
    {
        return $this->rests->contains($rest);
    }

    public function addRest(Rest $rest): self
    {
        if (!$this->hasRest($rest)) {
            $this->rests[] = $rest;
            $rest->setProduct($this);
        }

        return $this;
    }

    public function removeRest(Rest $rest): self
    {
        if ($this->rests->removeElement($rest)) {
            // set the owning side to null (unless already changed)
            if ($rest->getProduct() === $this) {
                $rest->setProduct(null);
            }
        }

        return $this;
    }

    public function getCreateAt(): DateTimeInterface
    {
        return $this->createAt;
    }

    public function setCreateAt(DateTimeInterface $createAt): self
    {
        $this->createAt = $createAt;

        return $this;
    }

    public function getUpdateAt(): ?DateTimeInterface
    {
        return $this->updateAt;
    }

    public function setUpdateAt(?DateTimeInterface $updateAt): self
    {
        $this->updateAt = $updateAt;

        return $this;
    }

    public function getCollectionParent(): ?self
    {
        return $this->collectionParent;
    }

    public function setCollectionParent(?self $collectionParent): self
    {
        $this->collectionParent = $collectionParent;

        return $this;
    }

    /**
     * @return Collection|self[]
     */
    public function getProductVariations(): Collection
    {
        return $this->productVariations;
    }

    public function hasVariation(self $variation): bool
    {
        return $this->productVariations->contains($variation);
    }

    public function addVariation(self $variation): self
    {
        if (!$this->hasVariation($variation)) {
            $this->productVariations[] = $variation;
            $variation->setCollectionParent($this);
        }

        return $this;
    }

    public function removeVariation(self $variation): self
    {
        if ($this->productVariations->removeElement($variation)) {
            // set the owning side to null (unless already changed)
            if ($variation->getCollectionParent() === $this) {
                $variation->setCollectionParent(null);
            }
        }

        return $this;
    }

    public function getWeight(): ?string
    {
        return $this->weight;
    }

    public function setWeight(?string $weight): self
    {
        $this->weight = $weight;

        return $this;
    }

    public function getLength(): ?string
    {
        return $this->length;
    }

    public function setLength(?string $length): self
    {
        $this->length = $length;

        return $this;
    }

    public function getWidth(): ?string
    {
        return $this->width;
    }

    public function setWidth(?string $width): self
    {
        $this->width = $width;

        return $this;
    }

    public function getHeight(): ?string
    {
        return $this->height;
    }

    public function setHeight(?string $height): self
    {
        $this->height = $height;

        return $this;
    }

    public function getSize(): ?string
    {
        return $this->size;
    }

    public function setSize(?string $size): self
    {
        $this->size = $size;

        return $this;
    }

    public function getIsBestseller(): ?bool
    {
        return $this->isBestseller;
    }

    public function setIsBestseller(bool $isBestseller): self
    {
        $this->isBestseller = $isBestseller;

        return $this;
    }

    public function getAlias(): ?string
    {
        return $this->alias;
    }

    public function setAlias(string $alias): self
    {
        $this->alias = $alias;

        return $this;
    }

    /**
     * @return Collection|ProductPrice[]
     */
    public function getPrices(): Collection
    {
        return $this->prices;
    }

    public function addPrice(ProductPrice $price): self
    {
        if (!$this->prices->contains($price)) {
            $this->prices[] = $price;
            $price->setProduct($this);
        }

        return $this;
    }

    public function removePrice(ProductPrice $price): self
    {
        if ($this->prices->removeElement($price)) {
            // set the owning side to null (unless already changed)
            if ($price->getProduct() === $this) {
                $price->setProduct(null);
            }
        }

        return $this;
    }

    /**
     * Возвращает связку товар-цена по типу цены
     * @param Price $priceType
     * @return ProductPrice|null
     */
    public function getProductPrice(Price $priceType): ?ProductPrice
    {
        /** @var ProductPrice $price */
        foreach ($this->prices as $price) {
            if($price->getPrice()->getUuid() == $priceType->getUuid()) {
                return $price;
            }
        }

        return null;
    }

    public function getMetaTitle(): ?string
    {
        return $this->metaTitle;
    }

    public function setMetaTitle(?string $metaTitle): self
    {
        $this->metaTitle = $metaTitle;

        return $this;
    }

    public function getMetaDescription(): ?string
    {
        return $this->metaDescription;
    }

    public function setMetaDescription(?string $metaDescription): self
    {
        $this->metaDescription = $metaDescription;

        return $this;
    }

    public function getKeywords(): ?string
    {
        return $this->keywords;
    }

    public function setKeywords(?string $keywords): self
    {
        $this->keywords = $keywords;

        return $this;
    }

    /**
     * @return Collection|ProductCompanion[]
     */
    public function getCompanions(): Collection
    {
        return $this->companions;
    }

    public function addCompanion(ProductCompanion $companion): self
    {
        if (!$this->companions->contains($companion)) {
            $this->companions[] = $companion;
            $companion->setParent($this);
        }

        return $this;
    }

    public function removeCompanion(ProductCompanion $companion): self
    {
        if ($this->companions->removeElement($companion)) {
            // set the owning side to null (unless already changed)
            if ($companion->getParent() === $this) {
                $companion->setParent(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|ProductValue[]
     */
    public function getProductValues(): Collection
    {
        return $this->productValues;
    }

    public function hasProductValue(ProductValue $productValue): bool
    {
        return $this->productValues->contains($productValue);
    }

    public function addProductValue(ProductValue $productValue): self
    {
        if (!$this->hasProductValue($productValue)) {
            $this->productValues[] = $productValue;
            $productValue->setProduct($this);
        }

        return $this;
    }

    public function removeProductValue(ProductValue $productValue): self
    {
        if ($this->productValues->removeElement($productValue)) {
            // set the owning side to null (unless already changed)
            if ($productValue->getProduct() === $this) {
                $productValue->setProduct(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Value[]
     */
    public function getValues(): Collection
    {
        $values = new ArrayCollection();

        /** @var ProductValue $productValue */
        foreach ($this->productValues as $productValue) {
            $values->add($productValue->getValue());
        }

        return $values;
    }

    public function getIsNew(): ?bool
    {
        return $this->isNew;
    }

    public function setIsNew(bool $isNew): self
    {
        $this->isNew = $isNew;

        return $this;
    }

    public function getAllowSample(): ?bool
    {
        return $this->allowSample;
    }

    public function setAllowSample(bool $allowSample): self
    {
        $this->allowSample = $allowSample;

        return $this;
    }
}
