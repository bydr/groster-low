<?php

namespace App\Entity;

use App\Repository\SettingsRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\SerializedName;

/**
 * @ORM\Entity(repositoryClass=SettingsRepository::class)
 */
class Settings
{
    const DEFAULT_ID = 1;
    const SEPARATOR = "|";

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="smallint", nullable=false)
     * @SerializedName("delivery_shift")
     * @Groups({"settings"})
     */
    private $deliveryShift;

    /**
     * @ORM\Column(type="string", length=10, nullable=true)
     * @SerializedName("delivery_fast_time")
     * @Groups({"settings"})
     */
    private $deliveryFastTime;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @SerializedName("holidays")
     * @Groups({"settings"})
     */
    private $holidays;

    /**
     * Минимальное значение для показателя "Много"
     * @ORM\Column(type="integer", options={"default": 100})
     * @SerializedName("min_many_quantity")
     * @Groups({"settings"})
     */
    private $minManyQuantity;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     * @SerializedName("viber")
     * @Groups({"settings"})
     */
    private $viber;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     * @SerializedName("whatsapp")
     * @Groups({"settings"})
     */
    private $whatsapp;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     * @SerializedName("telegram")
     * @Groups({"settings"})
     */
    private $telegram;

    public function __construct()
    {
        $this->deliveryShift = 0;
        $this->minManyQuantity = 100;
    }

    /**
     * @param int $id
     * @return $this
     */
    public function setId(int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDeliveryShift(): int
    {
        return $this->deliveryShift;
    }

    public function setDeliveryShift(int $deliveryShift): self
    {
        $this->deliveryShift = $deliveryShift;

        return $this;
    }

    public function getDeliveryFastTime(): ?string
    {
        return $this->deliveryFastTime;
    }

    public function setDeliveryFastTime(?string $deliveryFastTime): self
    {
        $this->deliveryFastTime = $deliveryFastTime;

        return $this;
    }

    public function getHolidays(): array
    {
        if(empty($this->holidays)) {
            return [];
        }

        return explode(self::SEPARATOR, $this->holidays);
    }

    public function setHolidays(array $holidays): self
    {
        $this->holidays = implode(self::SEPARATOR, $holidays);

        return $this;
    }

    public function getMinManyQuantity(): ?int
    {
        return $this->minManyQuantity;
    }

    public function setMinManyQuantity(int $minManyQuantity): self
    {
        $this->minManyQuantity = $minManyQuantity;

        return $this;
    }

    public function getViber(): ?string
    {
        return $this->viber;
    }

    public function setViber(?string $viber): self
    {
        $this->viber = $viber;

        return $this;
    }

    public function getWhatsapp(): ?string
    {
        return $this->whatsapp;
    }

    public function setWhatsapp(?string $whatsapp): self
    {
        $this->whatsapp = $whatsapp;

        return $this;
    }

    public function getTelegram(): ?string
    {
        return $this->telegram;
    }

    public function setTelegram(?string $telegram): self
    {
        $this->telegram = $telegram;

        return $this;
    }
}
