<?php

namespace App\Entity;

use App\Repository\ProductAnalogsRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ProductAnalogsRepository::class)
 */
class ProductAnalogs
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Product::class, inversedBy="analogs")
     * @ORM\JoinColumn(nullable=false)
     */
    private $productSource;

    /**
     * @ORM\ManyToOne(targetEntity=Product::class)
     * @ORM\JoinColumn(nullable=false)
     */
    private $productTarget;

    /**
     * @ORM\Column(type="integer")
     */
    private $weight;

    public function __construct()
    {
        $this->weight = 0;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getProductSource(): ?Product
    {
        return $this->productSource;
    }

    public function setProductSource(?Product $productSource): self
    {
        $this->productSource = $productSource;

        return $this;
    }

    public function getProductTarget(): ?Product
    {
        return $this->productTarget;
    }

    public function setProductTarget(?Product $productTarget): self
    {
        $this->productTarget = $productTarget;

        return $this;
    }

    public function getWeight(): ?int
    {
        return $this->weight;
    }

    public function setWeight(int $weight): self
    {
        $this->weight = $weight;

        return $this;
    }
}
