<?php

namespace App\Entity;

use App\Entity\Security\AuthUser;
use App\Repository\FavoriteRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=FavoriteRepository::class)
 */
class Favorite
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Product::class)
     * @ORM\JoinColumn(nullable=false)
     */
    private $product;

    /**
     * @ORM\ManyToOne(targetEntity=AuthUser::class, inversedBy="favorites")
     * @ORM\JoinColumn(nullable=false)
     */
    private $shopUser;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getProduct(): ?Product
    {
        return $this->product;
    }

    public function setProduct(?Product $product): self
    {
        $this->product = $product;

        return $this;
    }

    public function getShopUser(): ?AuthUser
    {
        return $this->shopUser;
    }

    public function setShopUser(?AuthUser $shopUser): self
    {
        $this->shopUser = $shopUser;

        return $this;
    }
}
