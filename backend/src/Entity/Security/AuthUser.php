<?php

namespace App\Entity\Security;

use App\Entity\Favorite;
use App\Entity\Payer;
use App\Entity\Price;
use App\Entity\Product;
use App\Entity\ShippingAddress;
use App\Entity\WaitingProduct;
use App\Repository\AuthUserRepository;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\SerializedName;

/**
 * @ORM\Entity(repositoryClass=AuthUserRepository::class)
 */
class AuthUser implements UserInterface, PasswordAuthenticatedUserInterface
{

    const ROLE_USER = 'ROLE_USER';
    const ROLE_ADMIN = 'ROLE_ADMIN';
    const UID_PREFIX = 'guser';

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=180, unique=false, nullable=true)
     * @SerializedName("email")
     * @Groups({"auth-info"})
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=20, unique=false, nullable=true)
     * @SerializedName("phone")
     * @Groups({"auth-info"})
     */
    private $phone;

    /**
     * @ORM\Column(type="json")
     */
    private $roles = [];

    /**
     * @var string The hashed password
     * @ORM\Column(type="string")
     */
    private $password;

    /**
     * @var string The hashed password
     * @ORM\Column(type="boolean", options={"default": 0})
     */
    private $is_login;

    /**
     * @var
     * @ORM\Column(name="create_at", type="datetime")
     */
    private $createAt;

    /**
     * @ORM\OneToMany(targetEntity=Favorite::class, mappedBy="shopUser", orphanRemoval=true)
     */
    private $favorites;

    /**
     * @ORM\OneToMany(targetEntity=Payer::class, mappedBy="customer", orphanRemoval=true)
     */
    private $payers;

    /**
     * @ORM\OneToMany(targetEntity=ShippingAddress::class, mappedBy="customer", orphanRemoval=true)
     */
    private $shippingAddresses;

    /**
     * @ORM\Column(type="string", length="10", nullable=true)
     */
    private $crmId;

    /**
     * @ORM\Column(type="string", length=28)
     */
    private $uid;

    /**
     * @ORM\Column(type="string", length=250, nullable=true)
     * @SerializedName("fio")
     * @Groups({"auth-info"})
     */
    private $fio;

    /**
     * @ORM\ManyToOne(targetEntity=Price::class)
     */
    private $priceType;

    /**
     * Список товаров, на которые подписан покупатель
     *
     * @ORM\OneToMany(targetEntity=WaitingProduct::class, mappedBy="customer")
     */
    private $waitingProducts;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $sendTo1c;

    /**
     * @ORM\ManyToMany(targetEntity=Product::class)
     */
    private $recommendation;

    /**
     * @return array
     */
    public function getWaiting(): array
    {
        $products = [];
        /** @var WaitingProduct $waitingProduct */
        foreach ($this->waitingProducts as $waitingProduct) {
            $products[] = $waitingProduct->getProduct()->getUuid();
        }

        return $products;
    }

    /**
     * @return array
     */
    public function getRecommendationProducts(): array
    {
        $products = [];
        /** @var Product $product */
        foreach ($this->recommendation as $product) {
            $products[] = $product->getUuid();
        }

        return $products;
    }

    public function __construct()
    {
        $this->createAt = new DateTime();
        $this->favorites = new ArrayCollection();
        $this->payers = new ArrayCollection();
        $this->shippingAddresses = new ArrayCollection();
        $this->uid = uniqid(self::UID_PREFIX);
        $this->waitingProducts = new ArrayCollection();
        $this->is_login = false;
        $this->recommendations = new ArrayCollection();
        $this->recommendation = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUid(): string
    {
        return $this->uid;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUserIdentifier(): string
    {
        return $this->uid;
    }

    /**
     * @deprecated since Symfony 5.3, use getUserIdentifier instead
     */
    public function getUsername(): string
    {
        return (string)$this->email;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = self::ROLE_USER;

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see PasswordAuthenticatedUserInterface
     */
    public function getPassword(): string
    {
        return (string)$this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Returning a salt is only needed, if you are not using a modern
     * hashing algorithm (e.g. bcrypt or sodium) in your security.yaml.
     *
     * @see UserInterface
     */
    public function getSalt(): ?string
    {
        return null;
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
    }

    /**
     * @return bool
     */
    public function IsLogin(): bool
    {
        return $this->is_login;
    }

    /**
     * @param boolean $is_login
     */
    public function setIsLogin(bool $is_login): self
    {
        $this->is_login = $is_login;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getPhone(): ?string
    {
        return $this->phone;
    }

    /**
     * @param mixed $phone
     */
    public function setPhone($phone): self
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCreateAt()
    {
        return $this->createAt;
    }

    /**
     * @param mixed $createAt
     */
    public function setCreateAt($createAt): self
    {
        $this->createAt = $createAt;

        return $this;
    }

    /**
     * @return Collection|Favorite[]
     */
    public function getFavorites(): Collection
    {
        return $this->favorites;
    }

    public function addFavorite(Favorite $favorite): self
    {
        if (!$this->favorites->contains($favorite)) {
            $this->favorites[] = $favorite;
            $favorite->setShopUser($this);
        }

        return $this;
    }

    public function removeFavorite(Favorite $favorite): self
    {
        if ($this->favorites->removeElement($favorite)) {
            // set the owning side to null (unless already changed)
            if ($favorite->getShopUser() === $this) {
                $favorite->setShopUser(null);
            }
        }

        return $this;
    }

    public function isAdmin(): bool
    {
        return in_array(self::ROLE_ADMIN, $this->getRoles());
    }

    /**
     * @return Collection|Payer[]
     */
    public function getPayers(): Collection
    {
        return $this->payers;
    }

    public function addPayer(Payer $payer): self
    {
        if (!$this->payers->contains($payer)) {
            $this->payers[] = $payer;
            $payer->setCustomer($this);
        }

        return $this;
    }

    public function removePayer(Payer $payer): self
    {
        if ($this->payers->removeElement($payer)) {
            // set the owning side to null (unless already changed)
            if ($payer->getCustomer() === $this) {
                $payer->setCustomer(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|ShippingAddress[]
     */
    public function getShippingAddresses(): Collection
    {
        return $this->shippingAddresses;
    }

    public function addShippingAddress(ShippingAddress $shippingAddress): self
    {
        if (!$this->shippingAddresses->contains($shippingAddress)) {
            $this->shippingAddresses[] = $shippingAddress;
            $shippingAddress->setCustomer($this);
        }

        return $this;
    }

    public function removeShippingAddress(ShippingAddress $shippingAddress): self
    {
        if ($this->shippingAddresses->removeElement($shippingAddress)) {
            // set the owning side to null (unless already changed)
            if ($shippingAddress->getCustomer() === $this) {
                $shippingAddress->setCustomer(null);
            }
        }

        return $this;
    }

    public function getCrmId(): ?string
    {
        return $this->crmId;
    }

    public function setCrmId(?string $crmId): self
    {
        $this->crmId = $crmId;

        return $this;
    }

    public function getFio(): ?string
    {
        return $this->fio;
    }

    public function setFio(string $fio): self
    {
        $this->fio = $fio;
        return $this;
    }

    public function getPriceType(): ?Price
    {
        return $this->priceType;
    }

    public function setPriceType(?Price $priceType): self
    {
        $this->priceType = $priceType;

        return $this;
    }

    /**
     * @return Collection|WaitingProduct[]
     */
    public function getWaitingProducts(): Collection
    {
        return $this->waitingProducts;
    }

    public function addWaitingProduct(WaitingProduct $waitingProduct): self
    {
        if (!$this->waitingProducts->contains($waitingProduct)) {
            $this->waitingProducts[] = $waitingProduct;
            $waitingProduct->setCustomer($this);
        }

        return $this;
    }

    public function removeWaitingProduct(WaitingProduct $waitingProduct): self
    {
        if ($this->waitingProducts->removeElement($waitingProduct)) {
            // set the owning side to null (unless already changed)
            if ($waitingProduct->getCustomer() === $this) {
                $waitingProduct->setCustomer(null);
            }
        }

        return $this;
    }

    public function getSendTo1c(): ?\DateTimeInterface
    {
        return $this->sendTo1c;
    }

    public function setSendTo1c(?\DateTimeInterface $sendTo1c): self
    {
        $this->sendTo1c = $sendTo1c;

        return $this;
    }

    /**
     * @return Collection|Product[]
     */
    public function getRecommendation(): Collection
    {
        return $this->recommendation;
    }

    public function hasRecommendation(Product $product): bool
    {
        return $this->recommendation->contains($product);
    }

    public function addRecommendation(Product $product): self
    {
        if (!$this->hasRecommendation($product)) {
            $this->recommendation[] = $product;
        }

        return $this;
    }

    public function removeRecommendation(Product $product): self
    {
        if (!$this->hasRecommendation($product)) {
            $this->recommendation->removeElement($product);
        }

        return $this;
    }
}
