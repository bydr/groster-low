<?php

namespace App\Entity;

use App\Repository\ShippingLimitRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\SerializedName;

/**
 * @ORM\Entity(repositoryClass=ShippingLimitRepository::class)
 */
class ShippingLimit
{
    const TYPE_COST = 1;    // ограничения по сумме заказа
    const TYPE_REGION = 2;  // ограничения по региону

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @SerializedName("id")
     * @Groups({"admin-shipping-method-limit-list"})
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=ShippingMethod::class, inversedBy="limits")
     * @ORM\JoinColumn(nullable=false)
     */
    private $method;

    /**
     * Временно тип всегда 1, т.к. неясно с общими требованиями к ограничениям
     * @ORM\Column(type="smallint")
     */
    private $type;

    /**
     * @ORM\Column(type="integer", nullable=true)
     * @SerializedName("order_min_cost")
     * @Groups({"admin-shipping-method-limit-list", "admin-shipping-method-limit"})
     */
    private $orderMinCost;

    /**
     * @ORM\Column(type="integer", nullable=true)
     * @SerializedName("shipping_cost")
     * @Groups({"admin-shipping-method-limit-list", "admin-shipping-method-limit"})
     */
    private $shippingCost;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @SerializedName("region")
     * @Groups({"admin-shipping-method-limit-list", "admin-shipping-method-limit"})
     */
    private $region;

    public function __construct()
    {
        $this->type = self::TYPE_COST;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getMethod(): ?ShippingMethod
    {
        return $this->method;
    }

    public function setMethod(?ShippingMethod $method): self
    {
        $this->method = $method;

        return $this;
    }

    public function getType(): ?int
    {
        return $this->type;
    }

    public function setType(int $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getOrderMinCost(): ?int
    {
        return $this->orderMinCost;
    }

    public function setOrderMinCost(?int $orderMinCost): self
    {
        $this->orderMinCost = $orderMinCost;

        return $this;
    }

    public function getShippingCost(): ?int
    {
        return $this->shippingCost;
    }

    public function setShippingCost(?int $shippingCost): self
    {
        $this->shippingCost = $shippingCost;

        return $this;
    }

    public function getRegion(): ?string
    {
        return $this->region;
    }

    public function setRegion(?string $region): self
    {
        $this->region = $region;

        return $this;
    }
}
