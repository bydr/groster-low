<?php

namespace App\Entity;

use App\Repository\StoreRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\SerializedName;

/**
 * @ORM\Entity(repositoryClass=StoreRepository::class)
 */
class Store
{
    const IMAGE_FOLDER = 'images/shop';
    const MAIN_STORE_UUID = '5f268ab5-c232-11e7-82fe-448a5b2839e6';

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=50)
     * @SerializedName("uuid")
     * @Groups({"shop-list", "shop", "admin-shop-list", "admin-shop"})
     */
    private $uuid;

    /**
     * @ORM\Column(type="string", length=255)
     * @SerializedName("name")
     * @Groups({"admin-shop-list", "admin-shop"})
     */
    private $name;

    /**
     * @ORM\Column(type="boolean")
     * @SerializedName("is_active")
     * @Groups({"admin-shop-list", "admin-shop"})
     */
    private $isActive;

    /**
     * @ORM\OneToMany(targetEntity=Rest::class, mappedBy="store", orphanRemoval=true)
     */
    private $rests;

    /**
     * @ORM\Column(type="boolean", options={"default": 0})
     */
    private $isMain;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @SerializedName("address")
     * @Groups({"shop-list", "shop", "admin-shop-list", "admin-shop"})
     */
    private $address;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @SerializedName("schedule")
     * @Groups({"shop-list", "shop", "admin-shop-list", "admin-shop"})
     */
    private $schedule;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @SerializedName("description")
     * @Groups({"shop", "admin-shop-list", "admin-shop"})
     */
    private $description;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     * @SerializedName("phone")
     * @Groups({"shop-list", "shop", "admin-shop-list", "admin-shop"})
     */
    private $phone;

    /**
     * @ORM\Column(type="string", length=25, nullable=true)
     */
    private $latitude;

    /**
     * @ORM\Column(type="string", length=25, nullable=true)
     */
    private $longitude;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @SerializedName("image")
     * @Groups({"shop", "admin-shop-list", "admin-shop"})
     */
    private $image;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @SerializedName("email")
     * @Groups({"shop", "admin-shop-list", "admin-shop"})
     */
    private $email;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $updateAt;

    /**
     * @return array
     * @SerializedName("coordinates")
     * @Groups({"shop-list", "shop", "admin-shop-list", "admin-shop"})
     */
    public function getCoordinates(): array
    {
        return [
            'latitude' => $this->latitude,
            'longitude' => $this->longitude
        ];
    }

    public function __construct()
    {
        $this->rests = new ArrayCollection();
        $this->isMain = false;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUuid(): ?string
    {
        return $this->uuid;
    }

    public function setUuid(string $uuid): self
    {
        $this->uuid = $uuid;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getIsActive(): ?bool
    {
        return $this->isActive;
    }

    public function setIsActive(bool $isActive): self
    {
        $this->isActive = $isActive;

        return $this;
    }

    /**
     * @return Collection|Rest[]
     */
    public function getRests(): Collection
    {
        return $this->rests;
    }

    public function hasRest(Rest $rest): bool
    {
        return $this->rests->contains($rest);
    }


    public function addRest(Rest $rest): self
    {
        if (!$this->hasRest($rest)) {
            $this->rests[] = $rest;
            $rest->setStore($this);
        }

        return $this;
    }

    public function removeRest(Rest $rest): self
    {
        if ($this->rests->removeElement($rest)) {
            // set the owning side to null (unless already changed)
            if ($rest->getStore() === $this) {
                $rest->setStore(null);
            }
        }

        return $this;
    }

    public function getIsMain(): ?bool
    {
        return $this->isMain;
    }

    public function setIsMain(bool $isMain): self
    {
        $this->isMain = $isMain;

        return $this;
    }

    public function getAddress(): ?string
    {
        return $this->address;
    }

    public function setAddress(?string $address): self
    {
        $this->address = $address;

        return $this;
    }

    public function getSchedule(): ?string
    {
        return $this->schedule;
    }

    public function setSchedule(string $schedule): self
    {
        $this->schedule = $schedule;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getPhone(): ?string
    {
        return $this->phone;
    }

    public function setPhone(?string $phone): self
    {
        $this->phone = $phone;

        return $this;
    }

    public function getLatitude(): ?string
    {
        return $this->latitude;
    }

    public function setLatitude(?string $latitude): self
    {
        $this->latitude = $latitude;

        return $this;
    }

    public function getLongitude(): ?string
    {
        return $this->longitude;
    }

    public function setLongitude(?string $longitude): self
    {
        $this->longitude = $longitude;

        return $this;
    }

    public function getImage(): ?string
    {
        return $this->image;
    }

    public function setImage(?string $image): self
    {
        $this->image = $image;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getUpdateAt(): ?\DateTimeInterface
    {
        return $this->updateAt;
    }

    public function setUpdateAt(?\DateTimeInterface $updated): self
    {
        $this->updateAt = $updated;

        return $this;
    }
}
