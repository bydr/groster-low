<?php

namespace App\Entity;

use App\Entity\Security\AuthUser;
use App\Repository\PayerRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\SerializedName;

/**
 * @ORM\Entity(repositoryClass=PayerRepository::class)
 * @ORM\Table(uniqueConstraints={
 *     @ORM\UniqueConstraint(name="payer_unique_uid", columns={"uid"}),
 * })
 */
class Payer
{
    const UID_PREFIX = 'payer';
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=AuthUser::class, inversedBy="payers")
     * @ORM\JoinColumn(nullable=false)
     */
    private $customer;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @SerializedName("name")
     * @Groups({"payer"})
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=15, nullable=true)
     * @SerializedName("ogrnip")
     * @Groups({"payer"})
     */
    private $ogrnip;

    /**
     * @ORM\Column(type="string", length=12, nullable=true)
     * @SerializedName("inn")
     * @Groups({"payer"})
     */
    private $inn;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     * @SerializedName("account")
     * @Groups({"payer"})
     */
    private $account;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     * @SerializedName("bik")
     * @Groups({"payer"})
     */
    private $bik;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     * @SerializedName("kpp")
     * @Groups({"payer"})
     */
    private $kpp;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @SerializedName("contact")
     * @Groups({"payer"})
     */
    private $contact;

    /**
     * @ORM\Column(type="string", length=20, nullable=true)
     * @SerializedName("contact_phone")
     * @Groups({"payer"})
     */
    private $contactPhone;

    /**
     * @ORM\Column(type="string", length=28)
     * @SerializedName("uid")
     * @Groups({"payer"})
     */
    private $uid;

    /**
     * @return string|null
     * @SerializedName("title")
     * @Groups({"payer"})
     */
    public function getTitle(): string
    {
        $title = $this->getContact();
        if(!empty($this->name)) {
            $title = $this->name;
            if(!empty($this->ogrnip)) {
                $title = sprintf("%s, ОГРН %d", $title, $this->ogrnip);
            }
        }

        return $title;
    }

    public function __construct()
    {
        $this->uid = uniqid(self::UID_PREFIX, true);
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getCustomer(): ?AuthUser
    {
        return $this->customer;
    }

    public function setCustomer(?AuthUser $customer): self
    {
        $this->customer = $customer;

        return $this;
    }

    public function getOgrnip(): ?string
    {
        return $this->ogrnip;
    }

    public function setOgrnip(?string $ogrnip): self
    {
        $this->ogrnip = $ogrnip;

        return $this;
    }

    public function getInn(): ?string
    {
        return $this->inn;
    }

    public function setInn(?string $inn): self
    {
        $this->inn = $inn;

        return $this;
    }

    public function getAccount(): ?string
    {
        return $this->account;
    }

    public function setAccount(?string $account): self
    {
        $this->account = $account;

        return $this;
    }

    public function getBik(): ?string
    {
        return $this->bik;
    }

    public function setBik(?string $bik): self
    {
        $this->bik = $bik;

        return $this;
    }

    public function getKpp(): ?string
    {
        return $this->kpp;
    }

    public function setKpp(?string $kpp): self
    {
        $this->kpp = $kpp;

        return $this;
    }

    public function getContact(): ?string
    {
        return $this->contact;
    }

    public function setContact(?string $contact): self
    {
        $this->contact = $contact;

        return $this;
    }

    public function getContactPhone(): ?string
    {
        return $this->contactPhone;
    }

    public function setContactPhone(?string $contactPhone): self
    {
        $this->contactPhone = $contactPhone;

        return $this;
    }

    public function getUid(): ?string
    {
        return $this->uid;
    }

    public function setUid(string $uid): self
    {
        $this->uid = $uid;

        return $this;
    }
}
