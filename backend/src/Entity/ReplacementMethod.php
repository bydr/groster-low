<?php

namespace App\Entity;

use App\Repository\ReplacementMethodRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\SerializedName;

/**
 * @ORM\Entity(repositoryClass=ReplacementMethodRepository::class)
 */
class ReplacementMethod
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @SerializedName("id")
     * @Groups({"replacement-list"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @SerializedName("title")
     * @Groups({"replacement-list"})
     */
    private $title;

    /**
     * @ORM\Column(type="text")
     * @SerializedName("description")
     * @Groups({"replacement-list"})
     */
    private $description;

    /**
     * @ORM\Column(type="smallint")
     * @SerializedName("weight")
     * @Groups({"replacement-list"})
     */
    private $weight;

    public function __construct()
    {
        $this->weight = 0;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getWeight(): int
    {
        return $this->weight;
    }

    /**
     * @param mixed $weight
     * @return self
     */
    public function setWeight($weight): self
    {
        $this->weight = $weight;

        return $this;
    }
}
