<?php

namespace App\Entity;

use App\Repository\ShippingMethodRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\SerializedName;

/**
 * @ORM\Entity(repositoryClass=ShippingMethodRepository::class)
 * @ORM\Table(uniqueConstraints={
 *     @ORM\UniqueConstraint(name="unique_alias", columns={"alias"}),
 * })
 */
class ShippingMethod
{
    const METHOD_EXPRESS = 'express';
    const METHOD_PICKUP = 'pickup';
    const METHOD_COURIER = 'courier';
    const METHOD_COMPANY = 'company';

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @SerializedName("name")
     * @Groups({"shipping-method-list", "admin-shipping-method", "admin-shipping-method-list"})
     */
    private $name;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @SerializedName("description")
     * @Groups({"shipping-method-list", "admin-shipping-method"})
     */
    private $description;

    /**
     * @ORM\Column(type="integer")
     * @SerializedName("weight")
     * @Groups({"shipping-method-list", "admin-shipping-method", "admin-shipping-method-list"})
     */
    private $weight;

    /**
     * @ORM\Column(type="string")
     * @SerializedName("cost")
     * @Groups({"shipping-method-list", "admin-shipping-method", "admin-shipping-method-list"})
     */
    private $cost;

    /**
     * @ORM\Column(type="boolean")
     * @SerializedName("use_address")
     * @Groups({"shipping-method-list", "admin-shipping-method"})
     */
    private $useAddress;

    /**
     * @ORM\OneToMany(targetEntity=ShippingLimit::class, mappedBy="method")
     */
    private $limits;

    /**
     * @ORM\Column(type="boolean")
     * @SerializedName("is_fast")
     * @Groups({"shipping-method-list", "admin-shipping-method"})
     */
    private $isFast;

    /**
     * @ORM\Column(type="string", length=255)
     * @SerializedName("alias")
     * @Groups({"shipping-method-list", "admin-shipping-method", "admin-shipping-method-list"})
     */
    private $alias;

    /**
     * @ORM\Column(type="boolean", options={"default": true})
     * @SerializedName("is_active")
     * @Groups({"admin-shipping-method", "admin-shipping-method-list"})
     */
    private $isActive;

    /**
     * Регион с наименьшей стоимостью доставки
     * @SerializedName("region")
     * @Groups({"shipping-method-list"})
     */
    public function getRegion(): ?string
    {
        $minLimit = null;
        foreach ($this->getLimits() as $limit) {
            if(is_null($minLimit) || $limit->getShippingCost() < $minLimit->getShippingCost()) {
                $minLimit = $limit;
            }
        }

        if(is_null($minLimit)) {
            return "";
        }

        return $minLimit->getRegion();
    }

    public function __construct()
    {
        $this->limits = new ArrayCollection();
        $this->useAddress = false;
        $this->weight = 0;
        $this->isFast = false;
        $this->isActive = true;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getWeight(): ?int
    {
        return $this->weight;
    }

    public function setWeight(int $weight): self
    {
        $this->weight = $weight;

        return $this;
    }

    public function getCost(): ?string
    {
        return $this->cost;
    }

    public function setCost(string $cost): self
    {
        $this->cost = $cost;

        return $this;
    }

    public function getUseAddress(): ?bool
    {
        return $this->useAddress;
    }

    public function setUseAddress(bool $useAddress): self
    {
        $this->useAddress = $useAddress;

        return $this;
    }

    /**
     * @return Collection|ShippingLimit[]
     */
    public function getLimits(): Collection
    {
        return $this->limits;
    }

    public function addLimit(ShippingLimit $limit): self
    {
        if (!$this->limits->contains($limit)) {
            $this->limits[] = $limit;
            $limit->setMethod($this);
        }

        return $this;
    }

    public function removeLimit(ShippingLimit $limit): self
    {
        if ($this->limits->removeElement($limit)) {
            // set the owning side to null (unless already changed)
            if ($limit->getMethod() === $this) {
                $limit->setMethod(null);
            }
        }

        return $this;
    }

    public function getIsFast(): ?bool
    {
        return $this->isFast;
    }

    public function setIsFast(bool $isFast): self
    {
        $this->isFast = $isFast;

        return $this;
    }

    public function getAlias(): ?string
    {
        return $this->alias;
    }

    public function setAlias(string $alias): self
    {
        $this->alias = $alias;

        return $this;
    }

    public function getIsActive(): ?bool
    {
        return $this->isActive;
    }

    public function setIsActive(bool $isActive): self
    {
        $this->isActive = $isActive;

        return $this;
    }
}
