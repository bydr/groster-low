<?php

namespace App\Entity;

use App\Repository\CategoryImageRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\Ignore;
use Symfony\Component\Serializer\Annotation\SerializedName;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=CategoryImageRepository::class)
 */
class CategoryImage
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Ignore()
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @SerializedName("path")
     * @Assert\Type("string")
     * @Groups({"categories"})
     */
    private $path;

    /**
     * @ORM\OneToOne(targetEntity=Category::class, inversedBy="image")
     * @ORM\JoinColumn(nullable=false)
     * @Ignore()
     */
    private $category;

    public function getPath(): ?string
    {
        return $this->path;
    }

    public function setPath(string $path): self
    {
        $this->path = $path;

        return $this;
    }

    public function getCategory(): ?Category
    {
        return $this->category;
    }

    public function setCategory(?Category $category): self
    {
        $this->category = $category;

        return $this;
    }
}
