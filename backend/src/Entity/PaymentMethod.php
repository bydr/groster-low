<?php

namespace App\Entity;

use App\Repository\PaymentMethodRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\SerializedName;

/**
 * @ORM\Entity(repositoryClass=PaymentMethodRepository::class)
 */
class PaymentMethod
{
    const UID_PREFIX = 'gpay';

    const UID_CASH = 'cash_courier';
    const UID_CARD_COURIER = 'card_courier';
    const UID_CARD_ONLINE = 'card_online';
    const UID_BANK = 'bank_transfer';

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=33)
     * @SerializedName("uid")
     * @Groups({"payment-method-list", "admin-payment-method-list"})
     */
    private $uid;

    /**
     * @ORM\Column(type="string", length=255)
     * @SerializedName("name")
     * @Groups({"payment-method-list", "admin-payment-method-list"})
     */
    private $name;

    /**
     * @ORM\Column(type="integer")
     * @SerializedName("weight")
     * @Groups({"payment-method-list", "admin-payment-method-list"})
     */
    private $weight;

    /**
     * @ORM\Column(type="boolean")
     * @SerializedName("is_active")
     * @Groups({"admin-payment-method-list"})
     */
    private $isActive;

    public function __construct()
    {
        $this->uid = uniqid(self::UID_PREFIX, true);
        $this->weight = 0;
        $this->isActive = true;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUid(): ?string
    {
        return $this->uid;
    }

    public function setUid(string $uid): self
    {
        $this->uid = $uid;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getWeight(): ?int
    {
        return $this->weight;
    }

    public function setWeight(int $weight): self
    {
        $this->weight = $weight;

        return $this;
    }

    public function getIsActive(): ?bool
    {
        return $this->isActive;
    }

    public function setIsActive(bool $isActive): self
    {
        $this->isActive = $isActive;

        return $this;
    }
}
