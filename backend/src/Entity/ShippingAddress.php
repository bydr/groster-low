<?php

namespace App\Entity;

use App\Entity\Security\AuthUser;
use App\Repository\ShippingAddressRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\SerializedName;

/**
 * @ORM\Entity(repositoryClass=ShippingAddressRepository::class)
 * @ORM\Table(uniqueConstraints={
 *     @ORM\UniqueConstraint(name="shipping_address_unique_uid", columns={"uid"}),
 * })
 */
class ShippingAddress
{
    const UID_PREFIX = "address";
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @SerializedName("name")
     * @Groups({"shipping-address"})
     */
    private $name;

    /**
     * @ORM\Column(type="text")
     * @SerializedName("address")
     * @Groups({"shipping-address"})
     */
    private $address;

    /**
     * @ORM\Column(type="string", length=30)
     * @SerializedName("uid")
     * @Groups({"shipping-address"})
     */
    private $uid;

    /**
     * @ORM\ManyToOne(targetEntity=AuthUser::class, inversedBy="shippingAddresses")
     * @ORM\JoinColumn(nullable=false)
     */
    private $customer;

    /**
     * @ORM\Column(type="boolean", options={"default":false})
     * @SerializedName("is_default")
     * @Groups({"shipping-address"})
     */
    private $isDefault;

    public function __construct()
    {
        $this->uid = uniqid(self::UID_PREFIX, true);
        $this->isDefault = false;
    }


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getAddress(): ?string
    {
        return $this->address;
    }

    public function setAddress(string $address): self
    {
        $this->address = $address;

        return $this;
    }

    public function getUid(): ?string
    {
        return $this->uid;
    }

    public function setUid(string $uid): self
    {
        $this->uid = $uid;

        return $this;
    }

    public function getCustomer(): ?AuthUser
    {
        return $this->customer;
    }

    public function setCustomer(?AuthUser $customer): self
    {
        $this->customer = $customer;

        return $this;
    }

    public function getIsDefault(): ?bool
    {
        return $this->isDefault;
    }

    public function setIsDefault(bool $isDefault): self
    {
        $this->isDefault = $isDefault;

        return $this;
    }
}
