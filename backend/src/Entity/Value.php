<?php

namespace App\Entity;

use App\Repository\ValueRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\SerializedName;
use Symfony\Component\Validator\Constraints as Assert;
/**
 * @ORM\Entity(repositoryClass=ValueRepository::class)
 */
class Value
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @SerializedName("id")
     * @Assert\Type("int")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Param::class, inversedBy="values")
     * @ORM\JoinColumn(nullable=false)
     * @SerializedName("param")
     * @Assert\Type("App\Entity\Param")
     */
    private $param;

    /**
     * @ORM\Column(type="string", length=50)
     * @SerializedName("uuid")
     * @Assert\Type("string")
     * @Groups({"params", "catalog"})
     */
    private $uuid;

    /**
     * @ORM\Column(type="string", length=255)
     * @SerializedName("name")
     * @Assert\Type("string")
     * @Groups({"params", "catalog"})
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @SerializedName("image")
     * @Assert\Type("string")
     * @Groups({"params", "catalog"})
     */
    private $image;

    /**
     * @var int
     * @SerializedName("product_qty")
     * @Assert\Type("integer")
     * @Groups({"params"})
     */
    private $productQty;

    public function __construct()
    {
        $this->uuid = md5(bin2hex(random_bytes(32)));
        $this->products = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getParam(): ?Param
    {
        return $this->param;
    }

    public function setParam(?Param $param): self
    {
        $this->param = $param;

        return $this;
    }

    public function getUuid(): ?string
    {
        return $this->uuid;
    }

    public function setUuid(string $uuid): self
    {
        $this->uuid = $uuid;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getImage(): ?string
    {
        return $this->image;
    }

    public function setImage(?string $image): self
    {
        $this->image = $image;

        return $this;
    }

    /**
     * @return Collection|Product[]
     */
    public function getProducts(): Collection
    {
        return $this->products;
    }

    public function addProduct(Product $product): self
    {
        if (!$this->products->contains($product)) {
            $this->products[] = $product;
        }

        return $this;
    }

    public function removeProduct(Product $product): self
    {
        $this->products->removeElement($product);

        return $this;
    }

    /**
     * @return int
     */
    public function getProductQty(): int
    {
        return $this->productQty;
    }

    /**
     * @param int $productQty
     */
    public function setProductQty(int $productQty): self
    {
        $this->productQty = $productQty;
        return $this;
    }
}
