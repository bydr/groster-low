<?php

namespace App\Entity;

use App\Repository\CustomRequestRepository;
use DateTime;
use Doctrine\ORM\Mapping as ORM;

/**
 * Запросы пользователей
 * @ORM\Entity(repositoryClass=CustomRequestRepository::class)
 */
class CustomRequest
{
    const TYPE_RECALL = 1;      // заказ звонка
    const TYPE_CHEAPER = 2;     // нашли дешевле
    const TYPE_NOT_FOUND = 3;   // не нашли нужного

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="smallint")
     */
    private $type;

    /**
     * @ORM\Column(type="string", length=40, nullable=true)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=18, nullable=true)
     */
    private $phone;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @ORM\Column(type="string", length="10", nullable=true)
     */
    private $crmId;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $message;

    public function __construct()
    {
        $this->createdAt = new DateTime();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getType(): ?int
    {
        return $this->type;
    }

    public function setType(int $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getPhone(): ?string
    {
        return $this->phone;
    }

    public function setPhone(?string $phone): self
    {
        $this->phone = $phone;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getCrmId(): ?string
    {
        return $this->crmId;
    }

    public function setCrmId(?string $crmId): self
    {
        $this->crmId = $crmId;

        return $this;
    }

    public function getMessage(): ?string
    {
        return $this->message;
    }

    public function setMessage(?string $message): self
    {
        $this->message = $message;

        return $this;
    }

    public static function typeTitle(int $type): string
    {
        $titles = [
            self::TYPE_RECALL => 'Заказ обратного звонка',
            self::TYPE_CHEAPER => 'Нашли дешевле',
            self::TYPE_NOT_FOUND => 'Не нашли нужного товара',
        ];

        return $titles[$type] ?: '';
    }
}
