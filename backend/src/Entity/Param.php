<?php

namespace App\Entity;

use App\Repository\ParamRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\SerializedName;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=ParamRepository::class)
 */
class Param
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @SerializedName("id")
     * @Assert\Type("int")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=50)
     * @SerializedName("uuid")
     * @Assert\Type("string")
     * @Groups({"params"})
     */
    private $uuid;

    /**
     * @ORM\Column(type="string", length=255)
     * @SerializedName("name")
     * @Assert\Type("string")
     * @Groups({"params"})
     */
    private $name;

    /**
     * @ORM\Column(type="smallint", options={"default": 0})
     * @SerializedName("order")
     * @Assert\Type("integer")
     * @Groups({"params"})
     */
    private $weight;

    /**
     * @ORM\Column(type="boolean", options={"default": 1})
     * @SerializedName("is_show")
     * @Assert\Type("bool")
     * @Groups({"admin_params"})
     */
    private $isShow;

    /**
     * @ORM\OneToMany(targetEntity=Value::class, mappedBy="param", orphanRemoval=true)
     * @SerializedName("values")
     * @Assert\All({
     *   @Assert\Type("App\Entity\Value")
     * })
     * @Groups({"params", "catalog"})
     */
    private $values;

    public function __construct()
    {
        $this->uuid = md5(bin2hex(random_bytes(32)));
        $this->values = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUuid(): ?string
    {
        return $this->uuid;
    }

    public function setUuid(string $uuid): self
    {
        $this->uuid = $uuid;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getWeight(): ?int
    {
        return $this->weight;
    }

    public function setWeight(int $weight): self
    {
        $this->weight = $weight;

        return $this;
    }

    public function getIsShow(): ?bool
    {
        return $this->isShow;
    }

    public function setIsShow(bool $isShow): self
    {
        $this->isShow = $isShow;

        return $this;
    }

    /**
     * @return Collection|Value[]
     */
    public function getValues(): Collection
    {
        return $this->values;
    }

    public function addValue(Value $value): self
    {
        if (!$this->values->contains($value)) {
            $this->values[] = $value;
            $value->setParam($this);
        }

        return $this;
    }

    public function removeValue(Value $value): self
    {
        if ($this->values->removeElement($value)) {
            // set the owning side to null (unless already changed)
            if ($value->getParam() === $this) {
                $value->setParam(null);
            }
        }

        return $this;
    }
}
