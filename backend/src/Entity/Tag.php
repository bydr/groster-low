<?php

namespace App\Entity;

use App\Repository\TagRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\SerializedName;
use Symfony\Component\Validator\Constraints as Assert;
/**
 * @ORM\Entity(repositoryClass=TagRepository::class)
 */
class Tag
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @SerializedName("id")
     * @Assert\Type("int")
     * @Groups({"tags"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @SerializedName("name")
     * @Assert\Type("string")
     * @Groups({"tags","tag"})
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     * @SerializedName("url")
     * @Assert\Type("string")
     * @Groups({"tags","tag"})
     */
    private $url;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @SerializedName("meta_title")
     * @Assert\Type("string")
     * @Groups({"tag"})
     */
    private $metaTitle;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @SerializedName("meta_description")
     * @Assert\Type("string")
     * @Groups({"tag"})
     */
    private $metaDescription;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @SerializedName("h1")
     * @Assert\Type("string")
     * @Groups({"tag"})
     */
    private $h1;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @SerializedName("text")
     * @Assert\Type("string")
     * @Groups({"tag"})
     */
    private $bottomText;

    public function __construct()
    {
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getUrl(): ?string
    {
        return $this->url;
    }

    public function setUrl(string $url): self
    {
        $this->url = $url;

        return $this;
    }

    public function getMetaTitle(): ?string
    {
        return $this->metaTitle;
    }

    public function setMetaTitle(?string $metaTitle): self
    {
        $this->metaTitle = $metaTitle;

        return $this;
    }

    public function getMetaDescription(): ?string
    {
        return $this->metaDescription;
    }

    public function setMetaDescription(?string $metaDescription): self
    {
        $this->metaDescription = $metaDescription;

        return $this;
    }

    public function getH1(): ?string
    {
        return $this->h1;
    }

    public function setH1(?string $h1): self
    {
        $this->h1 = $h1;

        return $this;
    }

    public function getBottomText(): ?string
    {
        return $this->bottomText;
    }

    public function setBottomText(?string $bottomText): self
    {
        $this->bottomText = $bottomText;

        return $this;
    }
}
