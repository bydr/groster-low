<?php

namespace App\Entity;

use App\Repository\CategoryRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\SerializedName;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=CategoryRepository::class)
 * @ORM\Table(uniqueConstraints={
 *     @ORM\UniqueConstraint(name="unique_uuid", columns={"uuid"}),
 *     @ORM\UniqueConstraint(name="unique_type_alias", columns={"parent_id", "alias"}),
 * })
 */
class Category
{
    const TYPE_BUSSINESS_AREA = 1;
    const TYPE_PRODUCT_SECTION = 2;

    const SELECTION_TRAINING_DIR = 'files/selection_training';

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @SerializedName("id")
     * @Assert\Type("int")
     * @Groups({"categories", "admin-category"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=50)
     * @SerializedName("uuid")
     * @Assert\Type("string")
     * @Groups({"categories", "categories-by-area", "admin-category"})
     */
    private $uuid;

    /**
     * @ORM\Column(type="string", length=255)
     * @SerializedName("name")
     * @Assert\Type("string")
     * @Groups({"categories", "admin-category"})
     */
    private $name;

    /**
     * @ORM\Column(name="name_1c", type="string", length=255)
     * @SerializedName("name_1c")
     * @Groups({"admin-category"})
     */
    private $name1C;

    /**
     * @ORM\ManyToOne(targetEntity=Category::class, inversedBy="children")
     */
    private $parent;

    /**
     * @ORM\OneToMany(targetEntity=Category::class, mappedBy="parent")
     */
    private $children;

    /**
     * @ORM\ManyToMany(targetEntity=Product::class, mappedBy="categoriesUuid")
     * @SerializedName("products")
     * @Assert\All({
     *   @Assert\Type("App\Entity\Product")
     * })
     * @Groups({"catalog"})
     */
    private $products;

    /**
     * @ORM\ManyToMany(targetEntity=Tag::class)
     */
    private $tags;

    /**
     * @ORM\OneToOne(targetEntity=CategoryImage::class, mappedBy="category")
     */
    private $image;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @SerializedName("help_page")
     * @Assert\Type("string")
     * @Groups({"helpPage", "admin-category"})
     */
    private $helpPage;

    /**
     * @ORM\Column(name="is_active", type="boolean")
     */
    private $isActive;

    /**
     * @ORM\Column(type="integer")
     */
    private $type;

    /**
     * @ORM\Column(type="string", length=255)
     * @SerializedName("alias")
     * @Assert\Type("string")
     * @Groups({"categories", "admin-category"})
     */
    private $alias;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @SerializedName("meta_title")
     * @Assert\Type("string")
     * @Groups({"categories", "categories-by-area", "admin-category"})
     */
    private $metaTitle;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @SerializedName("meta_description")
     * @Assert\Type("string")
     * @Groups({"categories", "categories-by-area", "admin-category"})
     */
    private $metaDescription;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $selectionTraining;

    /**
     * @ORM\Column(type="integer", options={"default": 0})
     * @SerializedName("weight")
     * @Assert\Type("int")
     * @Groups({"categories", "categories-by-area", "admin-category"})
     */
    private $weight;

    /**
     * @SerializedName("image")
     * @Assert\Type("string")
     * @Groups({"categories", "admin-category"})
     */
    public function getImagePath(): string
    {
        return $this->image ? $this->getImage()->getPath() : "";
    }

    /**
     * @SerializedName("selection_file")
     * @Assert\Type("string")
     * @Groups({"categories", "admin-category"})
     */
    public function getSelectionFilePath(): string
    {
        return $this->getSelectionTraining() ?? "";
    }

    public function __construct()
    {
        $this->products = new ArrayCollection();
        $this->tags = new ArrayCollection();
        $this->children = new ArrayCollection();
        $this->isActive = true;
        $this->weight = 0;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUuid(): ?string
    {
        return $this->uuid;
    }

    public function setUuid(string $uuid): self
    {
        $this->uuid = $uuid;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getName1C(): ?string
    {
        return $this->name1C;
    }

    public function setName1C(string $name1C): self
    {
        $this->name1C = $name1C;

        return $this;
    }

    public function getParent(): ?self
    {
        return $this->parent;
    }

    public function setParent(?self $parent): self
    {
        $this->parent = $parent;

        return $this;
    }

    /**
     * @return Collection|Rest[]
     */
    public function getChildren(): Collection
    {
        return $this->children;
    }

    public function hasChild(Category $child): bool
    {
        return $this->children->contains($child);
    }

    public function addChild(Category $child): self
    {
        if (!$this->hasChild($child)) {
            $this->children[] = $child;
            $child->setParent($this);
        }

        return $this;
    }
    /**
     * @return Collection|Product[]
     */
    public function getProducts(): Collection
    {
        return $this->products;
    }

    public function addProduct(Product $product): self
    {
        if (!$this->products->contains($product)) {
            $this->products[] = $product;
            $product->addCategory($this);
        }

        return $this;
    }

    public function removeProduct(Product $product): self
    {
        if ($this->products->removeElement($product)) {
            $product->removeCategory($this);
        }

        return $this;
    }

    /**
     * @return Collection|Tag[]
     */
    public function getTags(): Collection
    {
        return $this->tags;
    }

    public function addTag(Tag $tag): self
    {
        if (!$this->tags->contains($tag)) {
            $this->tags[] = $tag;
        }

        return $this;
    }

    public function removeTag(Tag $tag): self
    {
        $this->tags->removeElement($tag);

        return $this;
    }

    public function removeAllTags(): self
    {
        foreach ($this->getTags() as $tag) {
            $this->removeTag($tag);
        }

        return $this;
    }

    public function getImage(): ?CategoryImage
    {
        return $this->image;
    }

    public function setImage(?CategoryImage $image): self
    {
        $this->image = $image;
        return $this;
    }

    /**
     * @return int
     *
     * @SerializedName("parent")
     * @Groups({"categories", "admin-category"})
     */
    public function getParentId(): int
    {
        if($this->getParent()) {
            return $this->getParent()->getId();
        }else{
            return 0;
        }
    }

    /**
     * @SerializedName("tags")
     * @Groups({"categories", "admin-category"})
     *
     * @return array
     */
    public function getTagIds(): array
    {
        $tags = [];
        foreach ($this->getTags() as $tag) {
            $tags[] = $tag->getId();
        }

        return $tags;
    }

    public function getHelpPage(): ?string
    {
        return $this->helpPage;
    }

    public function setHelpPage(?string $helpPage): self
    {
        $this->helpPage = $helpPage;

        return $this;
    }

    public function getIsActive(): ?bool
    {
        return $this->isActive;
    }

    public function setIsActive(bool $isActive): self
    {
        $this->isActive = $isActive;

        return $this;
    }

    public function getType(): ?int
    {
        return $this->type;
    }

    public function setType(int $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getAlias(): ?string
    {
        return $this->alias;
    }

    public function setAlias(string $alias): self
    {
        $this->alias = $alias;

        return $this;
    }

    /**
     * @SerializedName("product_qty")
     * @Groups({"categories", "categories-by-area"})
     */
    public function getProductQty(): int
    {
        $count = 0;

        /** @var Product $product */
        foreach ($this->products as $product) {
            if($product->getTotalQty() > 0) {
                $count++;
            }
        }

        /** @var Category $child */
        foreach ($this->children as $child) {
            $count += $child->getProductQty();
        }

        return $count;
    }

    public function getMetaTitle(): ?string
    {
        return $this->metaTitle;
    }

    public function setMetaTitle(?string $metaTitle): self
    {
        $this->metaTitle = $metaTitle;

        return $this;
    }

    public function getMetaDescription(): ?string
    {
        return $this->metaDescription;
    }

    public function setMetaDescription(?string $metaDescription): self
    {
        $this->metaDescription = $metaDescription;

        return $this;
    }

    public function getSelectionTraining(): ?string
    {
        return $this->selectionTraining;
    }

    public function setSelectionTraining(?string $selectionTraining): self
    {
        $this->selectionTraining = $selectionTraining;

        return $this;
    }

    public function getWeight(): ?int
    {
        return $this->weight;
    }

    public function setWeight(int $weight): self
    {
        $this->weight = $weight;

        return $this;
    }
}
