<?php

namespace App\Entity;

use App\Entity\Security\AuthUser;
use App\Repository\WaitingProductRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\SerializedName;

/**
 * Уведомление о поступлении товара
 * @ORM\Entity(repositoryClass=WaitingProductRepository::class)
 */
class WaitingProduct
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Product::class)
     * @ORM\JoinColumn(nullable=false)
     */
    private $product;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $contact;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $notificatedAt;

    /**
     * @ORM\ManyToOne(targetEntity=AuthUser::class, inversedBy="waitingProducts")
     */
    private $customer;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getProduct(): ?Product
    {
        return $this->product;
    }

    public function setProduct(?Product $product): self
    {
        $this->product = $product;

        return $this;
    }

    public function getContact(): ?string
    {
        return $this->contact;
    }

    public function setContact(string $contact): self
    {
        $this->contact = $contact;

        return $this;
    }

    public function getNotificatedAt(): ?\DateTimeInterface
    {
        return $this->notificatedAt;
    }

    public function setNotificatedAt(?\DateTimeInterface $notificatedAt): self
    {
        $this->notificatedAt = $notificatedAt;

        return $this;
    }

    public function getCustomer(): ?AuthUser
    {
        return $this->customer;
    }

    public function setCustomer(?AuthUser $customer): self
    {
        $this->customer = $customer;

        return $this;
    }
}
