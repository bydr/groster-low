<?php

namespace App\Entity;

use App\Repository\FaqRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\SerializedName;

/**
 * @ORM\Entity(repositoryClass=FaqRepository::class)
 */
class Faq
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     *
     * @SerializedName("id")
     * @Groups({"admin-faq"})
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=FaqType::class, inversedBy="faqs")
     * @ORM\JoinColumn(nullable=false)
     */
    private $type;

    /**
     * @ORM\Column(type="text")
     *
     * @SerializedName("title")
     * @Groups({"faq", "admin-faq"})
     */
    private $question;

    /**
     * @ORM\Column(type="text")
     *
     * @SerializedName("answer")
     * @Groups({"faq", "admin-faq"})
     */
    private $answer;

    /**
     * @ORM\Column(type="smallint")
     *
     * @SerializedName("weight")
     * @Groups({"admin-faq"})
     */
    private $weight;

    /**
     * @return int|null
     *
     * @SerializedName("block")
     * @Groups({"admin-faq"})
     */
    public function getBlock()
    {
        return $this->getType()->getId();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getType(): ?FaqType
    {
        return $this->type;
    }

    public function setType(?FaqType $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getQuestion(): ?string
    {
        return $this->question;
    }

    public function setQuestion(string $question): self
    {
        $this->question = $question;

        return $this;
    }

    public function getAnswer(): ?string
    {
        return $this->answer;
    }

    public function setAnswer(string $answer): self
    {
        $this->answer = $answer;

        return $this;
    }

    public function getWeight(): ?int
    {
        return $this->weight;
    }

    public function setWeight(int $weight): self
    {
        $this->weight = $weight;

        return $this;
    }
}
