<?php

namespace App\Helpers;

use App\Controller\ApiController;
use Exception;
use Liip\ImagineBundle\Imagine\Cache\CacheManager;
use Psr\Container\ContainerInterface;

/**
 * Class ImageHelper
 *
 * Общие методы для работы с картинками
 */
class ImageHelper
{
    const ACCEPTABLE_FILE_FORMATS = [
        'image/jpeg',
        'image/png',
    ];

    /**
     * Декодирует картинку из base64, проверяет ее, сохраняет в файл
     * @param string $base64Img изображение в base64
     * @param $imageFolder
     * @return string Название файла картинки
     */
    public function saveBase64Image(string $base64Img, $imageFolder): string
    {
        $imageData = base64_decode($base64Img);
        $extension = self::decodeBase64Img($imageData);

        if (empty($extension)) {
            ApiController::getLogger()->info('Изображения должны быть предоставлены в JPEG или PNG формате, строкой в кодировке base64');
            return "";
        }

        if (!is_dir($imageFolder)) {
            mkdir($imageFolder, 0777, true);
        }

        $filename = md5(microtime());
        $fullFilename = $filename . "." . $extension;

        if (!file_put_contents(sprintf("%s/%s", $imageFolder, $fullFilename), $imageData)) {
            ApiController::getLogger()->error("Ошибка при сохранении картинки");
            return "";
        }

        return $fullFilename;
    }

    public function getImageNameFromWebpath(string $webpath): ?string
    {
        if(!preg_match('~.*/(.*\.\w{3,4})~', $webpath, $matches)) {
            return null;
        }

        return $matches[1];
    }

    /**
     * Проверяет что закодировано в поле => Проверяет принимаем ли мы этот тип данных => Получаем расширение
     * @param string $img изображение
     * @return string Строка с расширением файла если ок (пример: "jpeg"). Пустая - если не подходит
     */
    private function decodeBase64Img(string $img): string
    {
        $f = finfo_open();
        $mime_type = finfo_buffer($f, $img, FILEINFO_MIME_TYPE);     // example: image/jpeg
        $extension = explode('/', $mime_type)[1];                      // example: jpeg
        if (in_array($mime_type, self::ACCEPTABLE_FILE_FORMATS)) {
            return $extension;
        }

        return "";
    }
}