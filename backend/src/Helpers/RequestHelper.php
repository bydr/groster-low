<?php

namespace App\Helpers;


use Symfony\Component\HttpFoundation\Request;

/**
 * Class RequestHelper
 */
class RequestHelper
{
    /**
     * Возвращает весь запрос в виде строки
     * @param Request $request
     * @return string
     */
    public static function getRequestString(Request $request): string
    {
        $url = $request->getUri();

        if(Request::METHOD_GET == $request->getMethod()) {
            return $url;
        }

        $content = json_decode($request->getContent(), true);
        if(is_null($content)) {
            return $url;
        }

        $params = self::getParams($content);

        return $url . "?" . implode("&", $params);
    }

    /**
     * Возвращает массив переданных параметров в запросе
     * @param $content
     * @return array
     */
    private static function getParams($content): array
    {
        $params = [];
        foreach ($content as $key => $param) {
            $params[$key] = $key;
            if(is_array($param)) {
                $params[$key] .= '=' . implode("&", self::getParams($param));
            }else{
                $params[$key] .= '=' . $param;
            }
        }

        return $params;
    }
}