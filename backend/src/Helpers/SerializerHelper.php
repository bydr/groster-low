<?php

namespace App\Helpers;

use Doctrine\Common\Annotations\AnnotationReader;
use Symfony\Component\Serializer\Encoder\JsonDecode;
use Symfony\Component\Serializer\Encoder\JsonEncode;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Mapping\Factory\ClassMetadataFactory;
use Symfony\Component\Serializer\Mapping\Loader\AnnotationLoader;
use Symfony\Component\Serializer\NameConverter\MetadataAwareNameConverter;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

/**
 * Сериализатор
 */
class SerializerHelper
{
    const GROUP_ACCOUNT_ORDERS = "account-orders";
    const GROUP_ACCOUNT_ORDER_DETAIL = "account-order-detail";
    const GROUP_AUTH_INFO = 'auth-info';
    const GROUP_BANNERS = 'banners';
    const GROUP_CART = "cart";
    const GROUP_CART_DELETE_PRODUCT = "delete-product";
    const GROUP_CATALOG = "catalog";
    const GROUP_CATEGORIES = "categories";
    const GROUP_CATEGORIES_BY_AREA = "categories-by-area";
    const GROUP_CHANGE_QTY = "change-qty";
    const GROUP_FAQ = "faq";
    const GROUP_HELP_PAGE = "helpPage";
    const GROUP_LAST_ORDER = "last-order";
    const GROUP_PARAMS = "params";
    const GROUP_PRODUCT = "product";
    const GROUP_PRODUCT_LIST = "product-list";
    const GROUP_PAYER = "payer";
    const GROUP_PAYER_LIST = "payer-list";
    const GROUP_PAYMENT_METHOD_LIST = 'payment-method-list';
    const GROUP_PROMO_ADD = "promo-add";
    const GROUP_PROMO_REMOVE = "promo-remove";
    const GROUP_REPLACEMENT_METHODS = "replacement-list";
    const GROUP_SETTINGS = 'settings';
    const GROUP_SHIPPING_ADDRESS = 'shipping-address';
    const GROUP_SHIPPING_METHOD_LIST = 'shipping-method-list';
    const GROUP_SHOP = "shop";
    const GROUP_SHOP_LIST = 'shop-list';
    const GROUP_TAG = "tag";
    const GROUP_TAGS = "tags";
    const GROUP_USER = "user";
    const GROUP_WAITING_PRODUCTS = "waiting-products";

    // ADMIN
    const GROUP_ADMIN_BANNERS = "admin-banners";
    const GROUP_ADMIN_CATEGORY = "admin-category";
    const GROUP_ADMIN_FAQ = "admin-faq";
    const GROUP_ADMIN_FAQ_BLOCK = "admin-faq-block";
    const GROUP_ADMIN_PAYMENT_METHOD_LIST = 'admin-payment-method-list';
    const GROUP_ADMIN_PROMO = "admin-promo";
    const GROUP_ADMIN_PROMO_LIST = "admin-promo-list";
    const GROUP_ADMIN_SHIPPING_METHOD = 'admin-shipping-method';
    const GROUP_ADMIN_SHIPPING_METHOD_LIST = 'admin-shipping-method-list';
    const GROUP_ADMIN_SHIPPING_METHOD_LIMIT = 'admin-shipping-method-limit';
    const GROUP_ADMIN_SHIPPING_METHOD_LIMIT_LIST = 'admin-shipping-method-limit-list';
    const GROUP_ADMIN_SHOP = "admin-shop";
    const GROUP_ADMIN_SHOP_LIST = "admin-shop-list";

    // 1C
    const GROUP_REMOTE_ORDER = "remote-order";
    const GROUP_REMOTE_USER = 'remote-user';
    const GROUP_PROMO_1C = 'promo-1c';

    /** @var Serializer */
    private static $serializer;

    private static function getSerializer(): Serializer
    {
        if(!self::$serializer) {
            $classMetadataFactory = new ClassMetadataFactory(new AnnotationLoader(new AnnotationReader()));

            $metadataAwareNameConverter = new MetadataAwareNameConverter($classMetadataFactory);
            $encoders = [new JsonEncoder()];
            $normalizers = [new ObjectNormalizer($classMetadataFactory, $metadataAwareNameConverter)];

            self::$serializer = new Serializer($normalizers, $encoders);
        }

        return self::$serializer;
    }

    /**
     * Serializes data to a given type format.
     *
     * @param mixed  $data   The data to serialize.
     * @param array $groups  Serialize groups name.
     * @param string $format The target serialization format.
     *
     * @return string A serialized data string.
     * @throws
     */
    public static function serialize($data, array $groups, array $attributes = [], string $format =  'json'): string
    {
        $serializer = self::getSerializer();
        $context[AbstractNormalizer::GROUPS] = $groups;
        if($attributes) {
            $context[AbstractNormalizer::ATTRIBUTES] = $attributes;
        }
        $normalized = $serializer->normalize($data, $format, $context);
        return $serializer->serialize($normalized, $format);
    }

    /**
     * Deserializes data from a given type format.
     *
     * @param string $data   The data to deserialize.
     * @param string $class  The target data class.
     * @param string $format The source serialization format.
     *
     * @return mixed A deserialized data.
     */
    public static function deserialize(string $data, string $class, string $format = 'json')
    {
        $serializer = self::getSerializer();
        return $serializer->deserialize($data, $class, $format);
    }
}