<?php

namespace App\Helpers;

use App\Controller\ApiController;
use App\Entity\Category;
use App\Entity\Discount;
use App\Entity\Order;
use App\Entity\PaymentMethod;
use App\Entity\Security\AuthUser;
use App\Entity\ShippingMethod;
use App\Repository\CategoryRepository;
use App\Repository\OrderRepository;
use App\Service\Cache;
use App\Service\Server\Server;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * Вспомогательные методы по заказу
 */
class OrderHelper
{
    /**
     * Рассчет стоимости доставки с учетом ограничений метода
     * @param int $orderCost
     * @param ShippingMethod $method
     * @param string $region
     * @return int
     */
    public static function shippingCost(int $orderCost, ShippingMethod $method, string $region): int
    {
        $min = 0;
        $cost = 0;
        foreach ($method->getLimits() as $limit) {
            // Если не совпадает регион
            if($limit->getRegion() != $region) {
                continue;
            }

            // Если мин стоимость больше заказа
            if($limit->getOrderMinCost() > $orderCost) {
                continue;
            }

            // Если это не максимально возможное условие
            if($limit->getOrderMinCost() < $min) {
                continue;
            }

            $cost = $limit->getShippingCost();
            $min = $limit->getOrderMinCost();
        }

        return $cost;
    }

    /**
     * Возвращает корзину для пользователя
     *
     * @param ManagerRegistry $doctrine
     * @param UserInterface $user
     * @return mixed|object|null
     */
    public static function getCartByUser(ManagerRegistry $doctrine, UserInterface $user)
    {
        return $doctrine->getRepository(Order::class)->findOneBy(['customer' => $user, 'state' => Order::STATE_CART]);
    }

    /**
     * Список сфер бизнеса клиента из кеша
     * @param Cache $cache
     * @param CategoryRepository $categoryRepo
     * @param OrderRepository $orderRepo
     * @param AuthUser $user
     * @return array
     * @throws \Doctrine\DBAL\Driver\Exception
     * @throws \Doctrine\DBAL\Exception
     */
    public static function getUserAreaList(Cache $cache,
                                           CategoryRepository $categoryRepo,
                                           OrderRepository $orderRepo,
                                           AuthUser $user): array
    {
        $areas = $cache->getCustomerAreaList($user->getId());

        if(!empty($areas)) {
            return json_decode($areas);
        }

        return self::updateAreaList($cache, $categoryRepo, $orderRepo, $user);

    }

    /**
     * Обновление сфер бизнеса клиента в кеше
     * @param Cache $cache
     * @param CategoryRepository $categoryRepo
     * @param OrderRepository $orderRepo
     * @param AuthUser $user
     * @return array
     * @throws \Doctrine\DBAL\Driver\Exception
     * @throws \Doctrine\DBAL\Exception
     */
    public static function updateAreaList(Cache $cache,
                                          CategoryRepository $categoryRepo,
                                          OrderRepository $orderRepo,
                                          AuthUser $user): array
    {
        $areas = self::createCustomerAreaList($categoryRepo, $orderRepo, $user);

        if(!$cache->saveCustomerAreaList($user->getId(), json_encode($areas))) {
            ApiController::getLogger()->error(sprintf("Failed to save total customer area list in cache for %d", $user->getId()));
        }

        return $areas;
    }

    /**
     * Создание общего списка сфер бизнеса клиента
     *
     * @param CategoryRepository $categoryRepo
     * @param OrderRepository $orderRepo
     * @param AuthUser $user
     * @return array
     * @throws \Doctrine\DBAL\Driver\Exception
     * @throws \Doctrine\DBAL\Exception
     */
    private static function createCustomerAreaList(CategoryRepository $categoryRepo,
                                       OrderRepository $orderRepo,
                                       AuthUser $user): array
    {
        $userAreas = [];
        $defaultAreas = $categoryRepo->findByType(Category::TYPE_BUSSINESS_AREA);

        /** @var Category $defaultArea */
        foreach ($defaultAreas as $defaultArea) {
            $userAreas[] = $defaultArea->getName();
        }

        $previousAreas = $orderRepo->getUserAreas($user);
        $userAreas = array_unique(array_merge($userAreas, $previousAreas));
        sort($userAreas);

        return $userAreas;
    }

    /**
     * Список почт клиента из кеша
     * @param Cache $cache
     * @param OrderRepository $orderRepo
     * @param AuthUser $user
     * @return array
     * @throws \Doctrine\DBAL\Driver\Exception
     * @throws \Doctrine\DBAL\Exception
     */
    public static function getUserEmailList(Cache $cache, OrderRepository $orderRepo, AuthUser $user): array
    {
        $emails = $cache->getCustomerEmailList($user->getId());

        if(!empty($emails)) {
            return json_decode($emails);
        }

        return self::updateEmailList($cache, $orderRepo, $user);
    }

    /**
     * Обновление email клиента в кеше
     * @param Cache $cache
     * @param OrderRepository $orderRepo
     * @param AuthUser $user
     * @return array
     * @throws \Doctrine\DBAL\Driver\Exception
     * @throws \Doctrine\DBAL\Exception
     */
    public static function updateEmailList(Cache $cache, OrderRepository $orderRepo, AuthUser $user): array
    {
        $emails = self::createCustomerEmailList($orderRepo, $user);

        if(!$cache->saveCustomerEmailList($user->getId(), json_encode($emails))) {
            ApiController::getLogger()->error(sprintf("Failed to save total customer email list in cache for %d", $user->getId()));
        }

        return $emails;
    }

    /**
     * Создание общего списка email клиента
     *
     * @param OrderRepository $orderRepo
     * @param AuthUser $user
     * @return array
     * @throws \Doctrine\DBAL\Driver\Exception
     * @throws \Doctrine\DBAL\Exception
     */
    private static function createCustomerEmailList(OrderRepository $orderRepo, AuthUser $user): array
    {
        $emails = [];
        if($user->getEmail()) {
            $emails[] = $user->getEmail();
        }
        $oldEmails = $orderRepo->getUserEmails($user);
        $emails = array_unique(array_merge($emails, $oldEmails));
        sort($emails);

        return $emails;
    }


    /**
     * Список телефонов клиента из кеша
     * @param Cache $cache
     * @param OrderRepository $orderRepo
     * @param AuthUser $user
     * @return array
     * @throws \Doctrine\DBAL\Driver\Exception
     * @throws \Doctrine\DBAL\Exception
     */
    public static function getUserPhoneList(Cache $cache, OrderRepository $orderRepo, AuthUser $user): array
    {
        $phones = $cache->getCustomerPhoneList($user->getId());

        if(!empty($phones)) {
            return json_decode($phones);
        }

        return self::updatePhoneList($cache, $orderRepo, $user);
    }

    /**
     * Обновление телефонов клиента в кеше
     * @param Cache $cache
     * @param OrderRepository $orderRepo
     * @param AuthUser $user
     * @return array
     * @throws \Doctrine\DBAL\Driver\Exception
     * @throws \Doctrine\DBAL\Exception
     */
    public static function updatePhoneList(Cache $cache, OrderRepository $orderRepo, AuthUser $user): array
    {
        $phones = self::createCustomerPhoneList($orderRepo, $user);

        if(!$cache->saveCustomerPhoneList($user->getId(), json_encode($phones))) {
            ApiController::getLogger()->error(sprintf("Failed to save total customer phone list in cache for %d", $user->getId()));
        }

        return $phones;
    }

    /**
     * Создание общего списка телефонов клиента
     *
     * @param OrderRepository $orderRepo
     * @param AuthUser $user
     * @return array
     * @throws \Doctrine\DBAL\Driver\Exception
     * @throws \Doctrine\DBAL\Exception
     */
    private static function createCustomerPhoneList(OrderRepository $orderRepo, AuthUser $user): array
    {
        $phones = [];
        if($user->getPhone()) {
            $phones[] = $user->getPhone();
        }
        $oldPhones = $orderRepo->getUserPhones($user);
        $phones = array_unique(array_merge($phones, $oldPhones));
        sort($phones);

        return $phones;
    }

    /**
     * Проверка, что выбранный статус является статусом заказа
     * @param int $state
     * @return bool
     */
    public static function isCompletedState(int $state): bool
    {
        return in_array($state, [Order::STATE_NEW, Order::STATE_FINISHED, Order::STATE_CANCELED]);
    }

    public static function updateDiscount(Order &$order)
    {
        $promoCategories = [];  // Список id категорий, к которым привязан промокод
        $promoDiscount = 0;     // Скидка по промокоду

        /** @var Discount|null $promo */
        $promo = $order->getDiscount();
        if($promo) {
            $promoCategories = $promo->getCategoryIds();
            $promoDiscount = $promo->getValue();
        }

        foreach ($order->getItems() as $item) {
            if(is_null($promo)) {
                $item->setDiscount(0);
                continue;
            }

            foreach ($item->getProduct()->getCategoriesUuid() as $category) {
                if(in_array($category->getId(), $promoCategories)) {
                    $itemTotal = $item->getPrice() * ($item->getQuantity() + $item->getSample());
                    switch ($promo->getType()) {
                        case Discount::TYPE_RATE:
                            $discountValue = $itemTotal * $promoDiscount / 100;
                            break;
                        case Discount::TYPE_MONEY:
                            $discountValue = $promoDiscount;
                            break;
                        default:
                            $discountValue = 0;
                    }

                    $discountValue = intval($discountValue);
                    $item->setDiscount($discountValue);
                    $item->setTotal($itemTotal - $discountValue);
                    break;
                }
            }
        }
    }

    /**
     * Отправка нового заказа в 1С
     * @param ContainerInterface $ci
     * @param EntityManagerInterface $em
     * @param Order $order
     * @return bool
     */
    public static function sendTo1C(ContainerInterface $ci, EntityManagerInterface $em, Order $order): bool
    {
        $server = new Server($ci, $em);
        if ($server->addOrder($order)) {
            $order->setSentAt1c(new \DateTime());
            $em->flush();

            return true;
        }

        return false;
    }

    /**
     * Отмена заказа в 1С
     * @param ContainerInterface $ci
     * @param EntityManagerInterface $em
     * @param Order $order
     * @return bool
     */
    public static function cancelIn1C(ContainerInterface $ci, EntityManagerInterface $em, Order $order): bool
    {
        $server = new Server($ci, $em);
        return $server->cancelOrder($order);
    }

    /**
     * Получение файлов счета на оплату
     * @param ContainerInterface $ci
     * @param EntityManagerInterface $em
     * @param Order $order
     * @return string
     */
    public static function get1CDocs(ContainerInterface $ci, EntityManagerInterface $em, string $uid, string $type, string $filename)
    {
        $server = new Server($ci, $em);

        switch ($type){
            case Server::METHOD_INVOICE_PAYMENT:
                $method = sprintf("%s?idorder=%s", Server::METHOD_INVOICE_PAYMENT, $uid);
                break;
            case Server::METHOD_ATTACHED_DOCS:
                $method = sprintf("%s?idorder=%s", Server::METHOD_ATTACHED_DOCS, $uid);
                break;
            case Server::METHOD_ORDER_DOCUMENT:
                $method = sprintf("%s?idorder=%s", Server::METHOD_ORDER_DOCUMENT, $uid);
                break;
            case Server::METHOD_PACKING_DOCS:
                $method = sprintf("%s?idorder=%s", Server::METHOD_PACKING_DOCS, $uid);
                break;
            default:
                $method = null;
        }

        if (empty($method)) {
            return '';
        }

        $filepath = $server->downloadDocument($method, $filename);

        return $filepath ? sprintf("%s/%s", $ci->getParameter('static_domain'), $filepath) : '';
    }

    /**
     * Возвращает список доступных методов оплаты в зависимости от выбранного способа доставки
     * @param ShippingMethod $shippingMethod
     * @param array $availablePaymentMethods
     * @return array
     */
    public static function getPaymentMethods(ShippingMethod $shippingMethod, array $availablePaymentMethods): array
    {
        $paymentMethods = [];
        if(in_array($shippingMethod->getAlias(), [ShippingMethod::METHOD_COMPANY])) {
            /** @var PaymentMethod $paymentMethod */
            foreach ($availablePaymentMethods as $paymentMethod) {
                // Костыль для метода оплаты Наличными, т.к. методы оплаты устанавливаются в админке
                if(stripos($paymentMethod->getName(), 'личным') !== false) {
                    continue;
                }

                $paymentMethods[] = $paymentMethod;
            }
        }else {
            $paymentMethods = $availablePaymentMethods;
        }

        return $paymentMethods;
    }
}