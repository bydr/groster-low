<?php

namespace App\Helpers;

use App\Controller\ApiController;
use App\Entity\Product;
use App\Entity\Security\AuthUser;
use App\Service\Cache;
use Doctrine\ORM\EntityManagerInterface;

/**
 * Вспомогательные методы по работе с товарами
 */
class ProductHelper
{
    /**
     * Возвращает json список хитов
     *
     * @param Cache $cache
     * @param EntityManagerInterface $em
     * @return string
     */
    public static function getHitList(Cache $cache, EntityManagerInterface $em): string
    {
        $hits = $cache->getHits();

        if(!empty($hits)) {
            return $hits;
        }

        return self::updateHitList($cache, $em);
    }

    /**
     * Обновление хитов в кеше
     * @param Cache $cache
     * @param EntityManagerInterface $em
     * @return string
     */
    public static function updateHitList(Cache $cache, EntityManagerInterface $em): string
    {
        $hits = $em->getRepository(Product::class)->getHits();

        if(empty($hits)) {
            return '';
        }

        $json = SerializerHelper::serialize($hits, [SerializerHelper::GROUP_CATALOG]);

        if(!$cache->saveHits($json)) {
            ApiController::getLogger()->error("Failed to save hits in cache");
        }

        return $json;
    }

    /**
     * Управление рекомендуемыми товарами пользователя
     * @param EntityManagerInterface $em
     * @param array $data
     * @param AuthUser $user
     * @return void
     */
    public static function updateUserProducts(EntityManagerInterface $em, array $data, AuthUser $user)
    {
        if (!empty($data[Params::PRODUCTS])) {
            $productIds = [];
            foreach ($data[Params::PRODUCTS] as $datum) {
                $productId = $datum['IDProduct'];

                if(!empty($productId) && $productId != "00000000-0000-0000-0000-000000000000") {
                    $productIds[] = $productId;
                }

                $productCollectionId = $datum['IDCollectionProduct'];

                if(!empty($productCollectionId) && $productCollectionId != "00000000-0000-0000-0000-000000000000") {
                    $productIds[] = $productCollectionId;
                }

            }

            if (!empty($productIds)) {
                $products = $em->getRepository(Product::class)->findBy(['uuid' => $productIds]);

                foreach ($products as $product) {
                    if($user->hasRecommendation($product)) {
                        continue;
                    }
                    $user->addRecommendation($product);
                }

                foreach ($user->getRecommendation() as $recommendation) {
                    if(!in_array($recommendation->getUuid(), $productIds)) {
                        $user->removeRecommendation($recommendation);
                    }
                }

                $em->flush();
            }
        }
    }

    /**
     * Суммирует количество товаров по каждому фильтру
     * @param array $productsQty
     * @param array $list
     * @return array
     */
    public static function mergeParams(array $productsQty, array $list): array
    {
        foreach ($list as $key => $value) {
            if (!isset($productsQty[$key])) {
                $productsQty[$key] = 0;
            }

            $productsQty[$key] += $value;
        }

        return $productsQty;
    }
}