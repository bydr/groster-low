<?php

namespace App\Helpers\Parsers;


use App\Entity\Category;
use App\Entity\Container;
use App\Entity\Param;
use App\Entity\Product;
use App\Entity\ProductAnalogs;
use App\Entity\ProductCompanion;
use App\Entity\ProductImage;
use App\Entity\ProductValue;
use App\Entity\Value;
use App\Helpers\TextHelper;
use App\Repository\ProductRepository;
use App\Service\Cache;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Liip\ImagineBundle\Imagine\Cache\CacheManager;
use SimpleXMLElement;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class ProductParser
{
    /** @var string домен сайта */
    private $staticDomain;

    /** @var EntityManagerInterface */
    private $em;

    /** @var ProductRepository */
    private $productRepo;

    /**
     * Созданные алиасы, но не добавленные в БД
     * @var array
     */
    private array $createdAliases = [];

    private string $publicDir;

    /** @var string Путь сохранения картинок для фронта */
    private string $imagesDir;

    private Cache $cache;

    private bool $hasNewParams = false;

    public function __construct(ContainerInterface $ci, EntityManagerInterface $em, Cache $cache)
    {
        $this->em = $em;
        $this->cache = $cache;
        $this->staticDomain = $ci->getParameter('static_domain');
        $this->productRepo = $this->em->getRepository(Product::class);
        $this->publicDir = $ci->getParameter('kernel.project_dir') . '/public/';
        $this->imagesDir = "images/shop";

        if(!is_dir($this->publicDir . $this->imagesDir)) {
            mkdir($this->publicDir . $this->imagesDir, 0777, true);
        }
    }

    public function parse(OutputInterface $output, SimpleXMLElement $sxe)
    {
        $count = 0;

        foreach ($sxe->children() as $child) {
            $this->saveProduct($output, $child);
            $count++;

            if($count % 100 == 0) {
                $output->write($count . " | ");
            }

            if($count % 500 == 0) {
                $this->em->flush();
            }
        }

        $this->em->flush();

        if($this->hasNewParams) {
            $this->cache->removeParams();
        }

        $output->writeln($count);

        /**
         * Сохраняем комплекты, аналоги и сопутствующие товары
         */
        $output->writeln("Аналоги, комплекты и сопутствующие товары");
        $count = 0;
        foreach ($sxe->children() as $child) {
            $count++;
            if (!empty($child->StructureKitProduct)) {
                $this->saveKit($output, $child);
            }

            if (!empty($child->AnalogsProduct)) {
                $this->saveAnalogs($output, $child);

            }

            if (!empty($child->CompanionsProduct)) {
                $this->saveCompanions($output, $child);
            }

            if($count % 500 == 0) {
                $this->em->flush();
                $output->write($count . " | ");
            }
        }

        $this->em->flush();
        $output->writeln(sprintf("\nВсего: %d", $count));

        $output->writeln("...");

        $cmd = sprintf("rm -r %smedia",$this->publicDir);
        exec($cmd);
    }

    private function saveProduct(OutputInterface $output, ?SimpleXMLElement $sxeProduct)
    {
        $attributes = $sxeProduct->attributes();
        $productUuid            = $attributes['IDProduct'];
        $productIsActive        = $attributes['ActiveProduct'] == 'true';
        $productName            = $attributes['NameProduct'];
        $productDescription     = $attributes['VerbalDescription'];
        $productUnit            = $attributes['UnitName'];
        $productCode            = $attributes['CodeProduct'];
        $productWeight          = $attributes['Weight'];
        $productLength          = $attributes['LengthSize'];
        $productWidth           = $attributes['WidthSize'];
        $productHeight          = $attributes['HeightSize'];
        $productSize            = $attributes['Size'];
        $collectionProductUsed  = $attributes['CollectionProductUsed'];
        $productSellOnlyUnit    = $attributes['SellOnlyUnit'] == 'true';
        $isBestseller           = $attributes['Bestseller'] == 'true';
        $isNew                  = $attributes['Novelty'] == 'true';
        $keywords               = !empty($attributes['Keywords']) ? $attributes['Keywords'] : null;
        $mainPicture            = $attributes['MainPicture'];
        $allowSample            = !empty($attributes['AllowSample']) && $attributes['AllowSample'] == 'true';

        if(empty($productUuid)) {
            return;
        }

        $product = $this->productRepo->findOneByUuid($productUuid);

        if(empty($product)) {
            $output->writeln($productUuid);
            $product = new Product();
            $product->setUuid($productUuid);
            $product->setAlias($this->getAlias($productUuid, $productName));
            $this->em->persist($product);
        }

        $product->setUpdateAt(new DateTime());

        $product->setIsActive($productIsActive);
        $product->setName($productName);
        $product->setCode($productCode);
        $product->setIsSellUnit($productSellOnlyUnit);
        $product->setKeywords($keywords);
        $product->setAllowSample($allowSample);

        if(!empty($productDescription)) {
            $product->setDescription($productDescription);
        }

        $product->setUnit($productUnit);
        $product->setIsBestseller($isBestseller);
        $product->setIsNew($isNew);

        if($productWeight != "0") {
            $product->setWeight($productWeight);
        }

        if($productLength != "0") {
            $product->setLength($productLength);
        }

        if($productWidth != "0") {
            $product->setWidth($productWidth);
        }

        if($productHeight != "0") {
            $product->setHeight($productHeight);
        }

        if($productSize != "0") {
            $product->setSize($productSize);
        }

        if(!empty($sxeProduct->ProductPictures)){
            $this->saveProductImages($product, $sxeProduct->ProductPictures, $mainPicture);
        }

        if(!empty($sxeProduct->ProductProperties)){
            $this->saveParams($output, $product, $sxeProduct->ProductProperties);
        }

        if(!empty($sxeProduct->BusinessAreasProduct)){
            $this->saveBussinessAreas($output, $product, $sxeProduct->BusinessAreasProduct);
        }

        if(!empty($sxeProduct->ProductEntrySections)){
            $this->saveSections($output, $product, $sxeProduct->ProductEntrySections);
        }

        if(!empty($sxeProduct->ProductUnits)){
            $this->saveContainers($output, $product, $sxeProduct->ProductUnits);
        }

        if(!empty($sxeProduct->CollectionsProducts)){
            $this->saveVariations($output, $product, $sxeProduct->CollectionsProducts);
        }
    }

    /**
     * Сохранение картинок товаров
     * @param Product $product
     * @param SimpleXMLElement|null $productPictures
     * @param string $mainPictureId
     * @return void
     */
    private function saveProductImages(Product $product, ?SimpleXMLElement $productPictures, string $mainPictureId)
    {
        $activeImages = [];
        foreach ($productPictures->ProductPicture as $picture) {
            $sourcePath = sprintf("upload/import/webdata/%s/%s", $picture->attributes()['NameFileDirectoryProductPicture'], $picture->attributes()['FilePathProductPicture']);
            $destName = $picture->attributes()['FilePathProductPicture']->__toString();
            $fileUrl = $this->staticDomain . DIRECTORY_SEPARATOR . $this->imagesDir . DIRECTORY_SEPARATOR . $destName; // будет путь 'https://static.groster.me/images/shop/file.jpg'
            $activeImages[$fileUrl] = [
                'destName' => $destName,
                'filePath' => $this->publicDir . $this->imagesDir . DIRECTORY_SEPARATOR . $destName, // будет путь '/var/www/public/images/shop/file.jpg'
                'sourcePath' => $sourcePath,
                'isMain' => $mainPictureId == $picture->attributes()['IDProductPicture']->__toString(),
                'isActive' => "true" == $picture->attributes()['ActiveProductPicture'],
            ];
        }

        $this->saveImages($product, $activeImages);
    }

    private function saveCollectionImages(Product $product, ?SimpleXMLElement $productPictures, string $mainImageId)
    {
        $activeImages = [];
        foreach ($productPictures->CollectionProductPicture as $picture) {
            $sourcePath = sprintf("upload/import/webdata/%s/%s", $picture->attributes()['NameFileDirectoryPictureCollectionProduct'], $picture->attributes()['FilePathPictureCollectionProduct']);
            $destName = $picture->attributes()['FilePathPictureCollectionProduct']->__toString();
            $fileUrl = $this->staticDomain . DIRECTORY_SEPARATOR . $this->imagesDir . DIRECTORY_SEPARATOR . $destName;
            $activeImages[$fileUrl] = [
                'destName' => $destName,
                'filePath' => $this->publicDir . $this->imagesDir . DIRECTORY_SEPARATOR . $destName, // будет путь '/var/www/public/images/shop/file.jpg'
                'sourcePath' => $sourcePath,
                'isMain' => $mainImageId == $picture->attributes()['IDPictureCollectionProduct']->__toString(),
                'isActive' => "true" == $picture->attributes()['ActivePictureCollectionProduct'],
            ];
        }

        $this->saveImages($product, $activeImages);
    }

    /**
     * Сохранение картинок товара
     * @param Product $product
     * @param array $activeImages
     */
    private function saveImages(Product $product, array $activeImages)
    {
        $images = $product->getGallery();

        $oldImages = [];
        $filePathes = array_keys($activeImages);
        foreach ($images as $image) {
            /** @var array{destName: string, filePath: string, sourcePath: string, isMain: bool, isActive: bool} $imageData */
            $imageData = $activeImages[$image->getPath()] ?? null;

            if(empty($imageData) || !is_file($imageData['filePath'])) {
                continue;
            }

            $oldImages[] = $image->getPath();
            $image->setIsMain($activeImages[$image->getPath()]['isMain']);
            $image->setIsActive($activeImages[$image->getPath()]['isActive']);
        }

        /** @var array{destName: string, filePath: string, sourcePath: string, isMain: bool, isActive: bool} $item */
        foreach ($activeImages as $imageUrl => $item) {
            // Если картинка уже есть в БД
            if(in_array($imageUrl, $oldImages)) {
                continue;
            }

            if(!$this->createThumb($item['sourcePath'], $item['destName'])) {
                continue;
            }

            $image = new ProductImage();
            $image->setProduct($product);
            $image->setPath($imageUrl);
            $image->setIsMain($item['isMain']);
            $image->setIsActive($item['isActive']);
            $this->em->persist($image);
        }
    }

    /**
     * Добавление свойств к товарам
     * @param OutputInterface $output
     * @param Product $product
     * @param SimpleXMLElement $productProperties
     */
    private function saveParams(OutputInterface $output, Product $product, SimpleXMLElement $productProperties)
    {
        $paramsRepo = $this->em->getRepository(Param::class);

        // Список полученных параметров из файла. Далее будем удалять те,
        // которые привязаны к товару, но нет в списке
        $productValues = [];
        foreach ($productProperties->children() as $item) {
            // Если нет значения
            $valueProperty = $item->attributes()['ValueProductProperty'] ? $item->attributes()['ValueProductProperty']->__toString() : $item->attributes()['ValueCollectionProductProperty']->__toString();
            if(empty($valueProperty)) {
                continue;
            }

//            // Если нет ID параметра
//            $paramUuid = $item->attributes()['IDProductProperty'] ?: $item->attributes()['IDCollectionProductProperty'];
//            if(empty($paramUuid)) {
//                $output->writeln('Не указан ID для параметра продукта ' . $product->getUuid());
//                continue;
//            }

            $name = $item->attributes()['NameProductProperty'] ?: $item->attributes()['NameCollectionProductProperty'];
            // Если нет названия параметра
            if (empty($name)) {
                continue;
            }

            $name = trim($name->__toString());
            // Если это Сферы бизнеса, то пропускаем этот параметр
            if('Сфера Бизнеса (строка)' == $name) {
                continue;
            }

            // Получаем параметр
            /** @var Param $param */
            $param = $paramsRepo->findOneByName($name);

            if(!$param) {
                $param = new Param();
                $param->setIsShow(true);
                $param->setName($name);
                $param->setWeight(0);

                $this->em->persist($param);
                $this->hasNewParams = true;
            }

            // Получаем значение параметра
            $value = $this->em->getRepository(Value::class)->findOneBy(['param' => $param, 'name' => $valueProperty]);
            if(!$value) {
                $value = new Value();
                $value->setName(trim($valueProperty));
                $value->setParam($param);
                $value->addProduct($product);

                $this->em->persist($value);
                $this->em->flush();

                $this->hasNewParams = true;
            }

            $productValues[] = $value->getUuid();

            $productValue = $this->em->getRepository(ProductValue::class)->findOneBy(['product' => $product, 'value' => $value]);
            $valuePropertyIsFiltr = $item->attributes()['FiltrateProductProperty'] ? $item->attributes()['FiltrateProductProperty'] == 'true' : $item->attributes()['FiltrateCollectionProductProperty'] == 'true';

            $productValues[] = $value->getUuid();
            if(!$productValue) {
                $productValue = new ProductValue();
                $productValue->setProduct($product);
                $productValue->setValue($value);
                $this->em->persist($productValue);

                $this->hasNewParams = true;
            }

            $productValue->setIsFilter($valuePropertyIsFiltr);

            $valuePropertyWeight = $item->attributes()['PriorityProductProperty'] ? $item->attributes()['PriorityProductProperty']->__toString() : $item->attributes()['PriorityCollectionProductProperty']->__toString();
            $productValue->setWeight((int)$valuePropertyWeight);
        }

        // Удаляем параметры товара, которые не пришли в новой выгрузке
        /** @var ProductValue $productValue */
        foreach ($product->getProductValues() as $productValue) {
            if(!in_array($productValue->getValue()->getUuid(), $productValues)) {
                $product->removeProductValue($productValue);
            }
        }
    }

    /**
     * Добавление бизнес сфер к товарам
     * @param OutputInterface $output
     * @param Product $product
     * @param SimpleXMLElement $productProperties
     */
    private function saveBussinessAreas(OutputInterface $output, Product $product, SimpleXMLElement $productBussinessAreas)
    {
        $catRepo = $this->em->getRepository(Category::class);

        // Список полученных сфер из файла. Далее будем удалять те,
        // которые привязаны к товару, но нет в списке
        $productAreas = [];
        foreach ($productBussinessAreas->children() as $item) {
            // Если нет ID параметра
            $areaUuid = $item->attributes()['IDBusinessAreaProduct'];
            if(empty($areaUuid)) {
                $output->writeln('Не указан ID для сферы бизнеса продукта ' . $product->getUuid());
                continue;
            }

            // Получаем сферу
            /** @var Category $area */
            $area = $catRepo->findOneByUuid($areaUuid);
            if(!$area) {
                $output->writeln("Не найдена сфера бизнеса " . $areaUuid);
                continue;
            }

            $productAreas[] = $area->getUuid();

            if(!$product->hasCategory($area)) {
                $product->addCategory($area);
            }
        }

        // Удаляем параметры товара, которые не пришли в новой выгрузке
        foreach ($product->getAreas() as $productArea) {
            if(!in_array($productArea->getUuid(), $productAreas)) {
                $product->removeCategory($productArea);
            }
        }
    }

    /**
     * Добавление категорий к товарам
     * @param OutputInterface $output
     * @param Product $product
     * @param SimpleXMLElement $productProperties
     */
    private function saveSections(OutputInterface $output, Product $product, SimpleXMLElement $productSections)
    {
        $catRepo = $this->em->getRepository(Category::class);

        // Список полученных сфер из файла. Далее будем удалять те,
        // которые привязаны к товару, но нет в списке
        $productCategories = [];
        foreach ($productSections->children() as $item) {
            // Если нет ID параметра
            $sectionUuid = $item->attributes()['ProductSectionID']->__toString();
            if(empty($sectionUuid)) {
                $output->writeln(sprintf('Не указан ID категории для продукта %s', $product->getUuid()));
                continue;
            }

            // Получаем параметр
            $category = $catRepo->findOneByUuid($sectionUuid);
            if(!$category) {
                $output->writeln(sprintf("Не найдена категория %s для товара %s", $sectionUuid, $product->getUuid()));
                continue;
            }

            $productCategories[] = $category->getUuid();

            if(!$product->hasCategory($category)) {
                $product->addCategory($category);
            }
        }

        // Удаляем категории товара, которые не пришли в новой выгрузке
        foreach ($product->getCategoriesUuid() as $productCategory) {
            if(!in_array($productCategory->getUuid(), $productCategories)) {
                $product->removeCategory($productCategory);
            }
        }
    }

    /**
     * Добавление тар к товарам
     * @param OutputInterface $output
     * @param Product $product
     * @param SimpleXMLElement $productProperties
     */
    private function saveContainers(OutputInterface $output, Product $product, SimpleXMLElement $productUnits)
    {
        $containerRepo = $this->em->getRepository(Container::class);

        // Список полученных тар из файла. Далее будем удалять те,
        // которые привязаны к товару, но нет в списке
        $productContainers = [];
        foreach ($productUnits->children() as $item) {
            // Если нет ID параметра
            $containerUuid = $item->attributes()['IDProductUnit'];
            if(empty($containerUuid)) {
                $output->writeln('Не указан ID для тары продукта ' . $product->getUuid());
                continue;
            }

            // Получаем тару
            $container = $containerRepo->findOneByUuid($containerUuid);
            if(!$container) {
                $container = new Container();
                $container->setUuid($containerUuid);
//                $container->setIsActive($item->attributes()['ActiveProductUnit'] == 'true');
                $container->setName($item->attributes()['NameProductUnit']);
                $container->setRate((int)$item->attributes()['RateProductUnit']);
                $container->setStandardName($item->attributes()['StandardUnitNameProductUnit']);
                $container->setWidth($item->attributes()['WidthSizeProductUnit']);
                $container->setHeight($item->attributes()['HeightSizeProductUnit']);
                $container->setLength($item->attributes()['LengthSizeProductUnit']);
                $container->setSize($item->attributes()['SizeProductUnit']);
                $container->setWeight($item->attributes()['WeightProductUnit']);
                $container->setProduct($product);

                $this->em->persist($container);
                $this->em->flush();
            }

            $productContainers[] = $container->getUuid();

            if(!$product->hasContainer($container)) {
                $product->addContainer($container);
            }
        }

        // Удаляем тары товара, которые не пришли в новой выгрузке
        foreach ($product->getProductContainers() as $productContainer) {
            if(!in_array($productContainer->getUuid(), $productContainers)) {
                $product->removeContainer($productContainer);
            }
        }
    }

    private function saveVariations(OutputInterface $output, Product $parent, SimpleXMLElement $collectionsProducts)
    {
        $productRepo = $this->em->getRepository(Product::class);

        // Список полученных вариаций из файла. Далее будем удалять те,
        // которые привязаны к товару, но нет в списке
        $variations = [];
        $names = [];
        foreach ($collectionsProducts->children() as $collectionsProduct) {
            $attributes = $collectionsProduct->attributes();
            $origName = $attributes['Name'];
            $code = $attributes['CodeCollectionProduct'];
            $uuid = $attributes['IDCollectionProduct'];
            $isActive = $attributes['ActiveCollectionProduct'] == 'true';
            $productDescription = $attributes['VerbalDescriptionCollectionProduct'];

            $name = sprintf("%s, %s", $parent->getName(), $origName);

            $variation = $productRepo->findOneByUuid($uuid);
            if(!$variation) {
                $translitName = TextHelper::translitRu2En($origName);
                $iter = 1;
                $varAlias = $translitName;
                while (true) {
                    if(!in_array($varAlias, $names)) {
                        $productAlias = sprintf("%s--%s", $parent->getAlias(), $varAlias);
                        if(!$this->em->getRepository(Product::class)->findOneByAlias($productAlias)) {
                            break;
                        }
                    }

                    $varAlias = sprintf("%s_%d", $varAlias, $iter++);
                }
                $names[] = $varAlias;
                $alias = sprintf("%s--%s", $parent->getAlias(), $varAlias);
                $variation = new Product();
                $variation->setUuid($uuid);
                $variation->setAlias($alias);

                $this->em->persist($variation);
            }

            $variations[] = $variation->getUuid();

            $variation->setIsActive($isActive);
            $variation->setName($name);
            $variation->setCode($code);
            $variation->setUnit($parent->getUnit());
            $variation->setIsSellUnit($parent->getIsSellUnit());
            $variation->setIsBestseller($parent->getIsBestseller());
            $variation->setIsNew($parent->getIsNew());

            if(!empty($productDescription)) {
                $variation->setDescription($productDescription);
            }

            if(!empty($collectionsProduct->CollectionProductPictures)) {
                $mainImageId = $attributes['MainPictureCollectionProduct']->__toString();
                $this->saveCollectionImages($variation, $collectionsProduct->CollectionProductPictures, $mainImageId);
            }
            if(!empty($collectionsProduct->CollectionProductPropertes)) {
                $this->saveParams($output, $variation, $collectionsProduct->CollectionProductPropertes);
            }

            if(!$parent->hasVariation($variation)) {
                $parent->addVariation($variation);
            }

            $varOldCategories = $variation->getCategoriesUuid();
            /** @var Category $category */
            foreach ($parent->getCategoriesUuid() as $category) {
                if(!$variation->hasCategory($category)) {
                    $variation->addCategory($category);
                } else {
                    $varOldCategories->removeElement($category);
                }
            }

            foreach ($varOldCategories as $oldCategory) {
                $variation->removeCategory($oldCategory);
            }
        }

        // Удаляем вариации товара, которые не пришли в новой выгрузке
        foreach ($parent->getProductVariations() as $productVariation) {
            if(!in_array($productVariation->getUuid(), $variations)) {
                $parent->removeVariation($productVariation);
            }
        }
    }

    /**
     * Привязка комплектов и их составов
     * @param OutputInterface $output
     * @param SimpleXMLElement|null $sxeProduct
     */
    private function saveKit(OutputInterface $output, ?SimpleXMLElement $sxeProduct)
    {
        $attributes = $sxeProduct->attributes();
        $output->writeln($attributes['Kit']);
        $productUuid = $attributes['IDProduct'];
        if($attributes['Kit'] != 'true') {
            return;
        }

        /** @var Product $parent */
        $parent = $this->productRepo->findOneByUuid($productUuid);
        
        if(!$parent) {
            $output->writeln('Не найден набор ' . $productUuid);
            return;
        }

        // Список полученных составляющих из файла. Далее будем удалять те,
        // которые привязаны к товару, но нет в списке
        $productChildren = [];
        foreach ($sxeProduct->StructureKitProduct->children() as $item) {
            $childUuid = $item->attributes()['IDKitProduct'];
            if(empty($childUuid)) {
                $output->writeln('Не установлено значение IDKitProduct для комплекта ' . $productUuid);
                continue;
            }

            /** @var Product $child */
            $child = $this->productRepo->findOneByUuid($childUuid);
            if (!$child) {
                $output->writeln(sprintf("Не найден товар %s для комплекта %s", $childUuid, $productUuid));
                continue;
            }

            $productChildren[] = $child->getUuid();
            if(!$parent->hasChild($child)) {
                $parent->addChild($child);
            }
        }

        // Удаляем комплектующие товара, которые не пришли в новой выгрузке
        foreach ($parent->getChildren() as $productChild) {
            if(!in_array($productChild->getUuid(), $productChildren)) {
                $parent->removeChild($productChild);
            }
        }

    }

    /**
     * Привязка аналогов
     * @param OutputInterface $output
     * @param SimpleXMLElement|null $sxeProduct
     */
    private function saveAnalogs(OutputInterface $output, ?SimpleXMLElement $sxeProduct)
    {
        $attributes = $sxeProduct->attributes();
        $productUuid = $attributes['IDProduct'];

        /** @var Product $parent */
        $parent = $this->productRepo->findOneByUuid($productUuid);

        if(!$parent) {
            $output->writeln('Не найден товар для аналогов ' . $productUuid);
            return;
        }

        // Список полученных аналогов из файла. Далее будем удалять те,
        // которые привязаны к товару, но нет в списке
        $productAnalogs = [];
        foreach ($sxeProduct->AnalogsProduct->children() as $item) {
            $analogUuid = $item->attributes()['IDAnalogProduct'];
            $analogWeight = (int)$item->attributes()['PriorityAnalogProduct'];

            if(empty($analogUuid)) {
                $output->writeln('Не установлено значение IDAnalogProduct для аналога ' . $productUuid);
                continue;
            }

            /** @var Product $analog */
            $analog = $this->productRepo->findOneByUuid($analogUuid);
            if (!$analog) {
                $output->writeln(sprintf("Не найден аналог %s для товара %s", $analogUuid, $productUuid));
                continue;
            }

            $productAnalog = $this->em->getRepository(ProductAnalogs::class)->findOneBy(['productSource' => $parent, 'productTarget' => $analog]);

            $productAnalogs[] = $analog->getUuid();
            if(!$productAnalog) {
                $productAnalog = new ProductAnalogs();
                $productAnalog->setProductSource($parent);
                $productAnalog->setProductTarget($analog);
                $this->em->persist($productAnalog);
            }
            $productAnalog->setWeight($analogWeight);
        }

        // Удаляем аналоги товара, которые не пришли в новой выгрузке
        /** @var ProductAnalogs $productAnalog */
        foreach ($parent->getAnalogs() as $productAnalog) {
            if(!in_array($productAnalog->getProductTarget()->getUuid(), $productAnalogs)) {
                $parent->removeAnalog($productAnalog);
            }
        }

    }

    /**
     * Привязка сопутствующих товаров
     * @param OutputInterface $output
     * @param SimpleXMLElement|null $sxeProduct
     */
    private function saveCompanions(OutputInterface $output, ?SimpleXMLElement $sxeProduct)
    {
        $attributes = $sxeProduct->attributes();
        $productUuid = $attributes['IDProduct'];

        /** @var Product $parent */
        $parent = $this->productRepo->findOneByUuid($productUuid);

        if(!$parent) {
            $output->writeln('Не найден товар для сопутствующих товаров ' . $productUuid);
            return;
        }

        // Список полученных компаньонов из файла. Далее будем удалять те,
        // которые привязаны к товару, но нет в списке
        $productCompanions = [];
        foreach ($sxeProduct->CompanionsProduct->children() as $item) {
            $companionUuid = $item->attributes()['IDCompanionProduct'];
            $companionWeight = (int)$item->attributes()['PriorityCompanionProduct'];

            if(empty($companionUuid)) {
                $output->writeln('Не установлено значение IDCompanionProduct для сопутствующего товара ' . $productUuid);
                continue;
            }

            /** @var Product $companion */
            $companion = $this->productRepo->findOneByUuid($companionUuid);
            if (!$companion) {
                $output->writeln(sprintf("Не найден товар %s для сопутствующего товара %s", $companionUuid, $productUuid));
                continue;
            }

            $productCompanion = $this->em->getRepository(ProductCompanion::class)->findOneBy(['parent' => $parent, 'companion' => $companion]);

            $productCompanions[] = $companion->getUuid();
            if(!$productCompanion) {
                $productCompanion = new ProductCompanion();
                $productCompanion->setParent($parent);
                $productCompanion->setCompanion($companion);
                $this->em->persist($productCompanion);
            }

            $productCompanion->setWeight($companionWeight);
        }

        // Удаляем параметры товара, которые не пришли в новой выгрузке
        /** @var ProductCompanion $productCompanion */
        foreach ($parent->getCompanions() as $productCompanion) {
            if(!in_array($productCompanion->getCompanion()->getUuid(), $productCompanions)) {
                $parent->removeCompanion($productCompanion);
            }
        }

    }

    /**
     * Создать уникальный алиас для категории
     * @param string $uuid
     * @param string $name
     * @return string
     */
    private function getAlias(string $uuid, string $name): string
    {
        $translit = TextHelper::translitRu2En($name);
        $alias = TextHelper::createAlias($uuid, $translit, $this->productRepo, $this->createdAliases);

        $this->createdAliases[$uuid] = $alias;

        return $alias;
    }

    /**
     * Создание миниатюры
     */
    private function createThumb($sourcePath, $destName): bool
    {
        $filter = "squared";
        /** Если нет исходного файла */
        if(!file_exists($this->publicDir . $sourcePath)) {
            echo sprintf("file not exists: %s\n", $sourcePath);
            return false;
        }

        $cachePath = $this->publicDir . 'media/cache/' . $filter . DIRECTORY_SEPARATOR . $sourcePath;
        $destPath = $this->publicDir . $this->imagesDir . DIRECTORY_SEPARATOR . $destName;

        $cmd = sprintf("bin/console liip:imagine:cache:resolve %s --filter=%s && mv %s %s",$sourcePath, $filter, $cachePath, $destPath);
        exec($cmd);

        return true;
    }
}