<?php

namespace App\Helpers\Parsers;


use App\Entity\Category;
use App\Entity\CategoryImage;
use App\Helpers\CategoryHelper;
use App\Helpers\TextHelper;
use App\Repository\CategoryRepository;
use Doctrine\ORM\EntityManagerInterface;
use SimpleXMLElement;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Парсинг категорий из 1С
 */
class CategoryParser
{
    /** @var EntityManagerInterface */
    private EntityManagerInterface $em;

    /** @var CategoryRepository  */
    private $repo;

    private $staticDomain;
    private $publicDir;
    private $imagesDir;
    private $filesDir;

    /**
     * Созданные алиасы, но не добавленные в БД
     * @var array
     */
    private array $createdAliases = [];

    public function __construct(ContainerInterface $ci, EntityManagerInterface $em)
    {
        $this->ci = $ci;
        $this->em = $em;
        $this->repo = $em->getRepository(Category::class);
        $this->staticDomain = $ci->getParameter('static_domain');
        $this->publicDir = $ci->getParameter('kernel.project_dir') . '/public/';
        $this->imagesDir = "images/shop/categories";

        if(!is_dir($this->publicDir . $this->imagesDir)) {
            mkdir($this->publicDir . $this->imagesDir, 0777, true);
        }

        if(!is_dir($this->publicDir . Category::SELECTION_TRAINING_DIR)) {
            mkdir($this->publicDir . Category::SELECTION_TRAINING_DIR, 0777, true);
        }
    }

    public function parse(OutputInterface $output, SimpleXMLElement $sxe, int $type)
    {
        $count = 0;
        foreach ($sxe->children() as $child) {
            switch ($type){
                case Category::TYPE_BUSSINESS_AREA:
                    $this->saveArea($child);
                    $count++;
                    break;
                case Category::TYPE_PRODUCT_SECTION:
                    $this->saveSection($output, $child);
                    $count++;
                    break;
            }

        }

        $output->writeln(sprintf(": %d", $count));
        $this->em->flush();

        // Для категорий проходимся второй раз и устанавливаем родителя
        if(Category::TYPE_PRODUCT_SECTION == $type) {
            foreach ($sxe->children() as $child) {
                $this->setCategoryParent($child);
            }
        }

        $this->em->flush();
    }

    private function saveArea(?SimpleXMLElement $child)
    {
        $uuid = $child->attributes()['IDBusinessArea']->__toString();
        $name = $child->attributes()['NameBusinessArea']->__toString();
        $isActive = $child->attributes()['ActiveBusinessArea']->__toString() == 'true';

        $category = $this->repo->findOneBy(['uuid' => $uuid, 'type' => Category::TYPE_BUSSINESS_AREA]);
        if(!$category) {
            $category = new Category();

            $category->setUuid($uuid);
            $category->setName($name);
            $alias = $this->getAlias($uuid, $name);
            $category->setAlias($alias);
            $category->setType(Category::TYPE_BUSSINESS_AREA);

            $this->em->persist($category);
        }

        $category->setName1C($name);
        $category->setIsActive($isActive);
    }

    private function saveSection(OutputInterface $output, ?SimpleXMLElement $child)
    {
        $uuid = $child->attributes()['IDProductSection']->__toString();

        /** @var Category $category */
        $category = $this->repo->findOneBy(['uuid' => $uuid, 'type' => Category::TYPE_PRODUCT_SECTION]);
        $name = $child->attributes()['NameProductSection']->__toString();
        $isActive = $child->attributes()['ActiveProductSection']->__toString() == 'true';
        $weight = (int)$child->attributes()['PriorityProductSection'];

        if(!$category) {
            $category = new Category();

            $category->setUuid($uuid);
            $category->setName($name);
            $category->setAlias($this->getAlias($uuid, $name));
            $category->setType(Category::TYPE_PRODUCT_SECTION);
            $this->em->persist($category);
        }

        if(!empty($child->ProductSectionImages)){
            $this->saveImages($category, $child->ProductSectionImages);
        }

        if(!empty($child->ProductSectionFilesSelectionTraining)){
            $this->saveSelectionTraining($category, $child->ProductSectionFilesSelectionTraining);
        }

        $category->setName1C($name);
        $category->setIsActive($isActive);
        $category->setWeight($weight);
    }

    private function setCategoryParent(?SimpleXMLElement $child)
    {
        $parentUuid = $child->attributes()['ParentIDProductSection']->__toString();

        // Если нет родителя
        if('00000000-0000-0000-0000-000000000000' == $parentUuid) {
            return;
        }

        $uuid = $child->attributes()['IDProductSection']->__toString();
        $category = $this->em->getRepository(Category::class)->findOneBy(['uuid' => $uuid, 'type' => Category::TYPE_PRODUCT_SECTION]);

        if(!$category) {
            return;
        }

        $parentCategory = $this->em->getRepository(Category::class)->findOneByUuid($parentUuid);
        if(!$parentCategory) {
            return;
        }

        $category->setParent($parentCategory);
    }

    /**
     * Создать уникальный алиас для категории
     * @param string $name
     * @return string
     */
    private function getAlias(string $uuid, string $name): string
    {
        $translit = TextHelper::translitRu2En($name);
        $alias = TextHelper::createAlias($uuid, $translit, $this->repo, $this->createdAliases);

        $this->createdAliases[] = $alias;

        return $alias;
    }

    /**
     * Сохранение картинки категории
     * @param Category $category
     * @param SimpleXMLElement|null $productPictures
     */
    private function saveImages(Category $category, ?SimpleXMLElement $categoryPicture)
    {
        $image = $category->getImage();
        $picture = $categoryPicture->ProductSectionImage[0];
        $filepath = sprintf("upload/import/webdata/%s/%s", $picture->attributes()['NameFileDirectoryProductSectionPicture'], $picture->attributes()['FilePathProductSectionPicture']);
        // TODO название картинки для webp. Использовать или это, или оригинальное название
//        $imageName = sprintf("%s.webp", $picture->attributes()['IDProductSectionPicture']);

        //оригинальное название
        $imageName = $picture->attributes()['FilePathProductSectionPicture']->__toString();

        if($image) {
            // Если такая картинка уже есть у товара, то удаляем из списка новых
            if(stripos($image->getPath(), $imageName) > 0 && is_file($this->publicDir . $this->imagesDir . DIRECTORY_SEPARATOR . $imageName)) {
                return;
            }

            // иначе картинки нет в новом списке и удаляем ее из базы
            $this->em->remove($image);
            $category->setImage(null);
            $this->em->flush();
        }

        $imagePath = $this->staticDomain . DIRECTORY_SEPARATOR . $this->imagesDir . DIRECTORY_SEPARATOR . $imageName;
        if(!$this->createThumb($filepath, $imageName)) {
            return;
        }

        $image = new CategoryImage();
        $image->setCategory($category);
        $image->setPath($imagePath);
        $this->em->persist($image);
    }

    /**
     * Сохранение файла помощи в выборе клиента
     * @param Category $category
     * @param SimpleXMLElement|null $selection
     */
    private function saveSelectionTraining(Category $category, ?SimpleXMLElement $selectionEl)
    {
        $selection = $category->getSelectionTraining();
        $selectionFile = $selectionEl->ProductSectionFileSelectionTraining[0];
        $filename = $selectionFile->attributes()['FilePathProductSectionFileSelectionTraining']->__toString();

        $origFilepath = sprintf("%supload/import/webdata/%s/%s", $this->publicDir, $selectionFile->attributes()['NameFileDirectoryProductSectionFileSelectionTraining'], $filename);
        /** Если нет исходного файла */
        if(!file_exists($origFilepath)) {
            echo sprintf("file not exists: %s\n", $origFilepath);
            return;
        }

        $fileDir = $this->publicDir . Category::SELECTION_TRAINING_DIR;
        if($selection) {
            // Если такая картинка уже есть у категории, то игнорируем
            if(stripos($selection, $filename) !== false && is_file($fileDir . DIRECTORY_SEPARATOR . $filename)) {
                return;
            }

            // иначе картинки нет в новом списке и удаляем ее из базы
            unlink($fileDir . DIRECTORY_SEPARATOR . $filename);
        }

        $newFilePath = sprintf("%s/%s", $fileDir, $filename);
        if(!rename($origFilepath, $newFilePath)) {
            echo sprintf("failed to move file: %s\n", $filename);
            return;
        }
        $url = $this->staticDomain . DIRECTORY_SEPARATOR . Category::SELECTION_TRAINING_DIR . DIRECTORY_SEPARATOR . $filename;
        $category->setSelectionTraining($url);
    }

    /**
     * Создание миниатюры
     */
    private function createThumb($filepath, $imageName): bool
    {
        $filter = "squared";
        /** Если нет исходного файла */
        if(!file_exists($this->publicDir . $filepath)) {
            echo sprintf("file not exists: %s\n", $filepath);
            return false;
        }

        $cachePath = $this->publicDir . 'media/cache/' . $filter . DIRECTORY_SEPARATOR . $filepath;
        $imagePath = $this->publicDir . $this->imagesDir . DIRECTORY_SEPARATOR . $imageName;
        $cmd = sprintf("bin/console liip:imagine:cache:resolve %s --filter=%s && mv %s %s",$filepath, $filter, $cachePath, $imagePath);
        exec($cmd);

        return true;
    }
}