<?php

namespace App\Helpers\Parsers;

use App\Entity\Category;
use Symfony\Component\Console\Output\OutputInterface;
use XMLReader;

class Parser
{
    public function __construct(CategoryParser $categoryParser,
                                ProductParser  $productParser,
                                StoreParser    $storeParser,
                                PriceParser    $priceParser,
                                BuyerPraser $buyerPraser)
    {
        $this->categoryParser = $categoryParser;
        $this->productParser = $productParser;
        $this->storeParser = $storeParser;
        $this->priceParser = $priceParser;
        $this->buyersParser = $buyerPraser;
    }

    public function products(string $filename, OutputInterface $output)
    {
        $output->writeln("Read Product.xml");

        $reader = new XMLReader();
        $reader->open($filename);

        while ($reader->read()){
            switch ($reader->nodeType) {
                case XMLReader::ELEMENT:
                    $node = $reader->expand();
                    $dom = new \DOMDocument();
                    $n = $dom->importNode($node, true);
                    $dom->appendChild($n);
                    $sxe = simplexml_import_dom($n);
                    switch ($reader->localName){
                        case "BusinessAreas":
                            $output->write("Парсинг сфер бизнеса");
                            $this->categoryParser->parse($output, $sxe, Category::TYPE_BUSSINESS_AREA);
                            break;
                        case "ProductSections":
                            $output->write("Парсинг категорий");
                            $this->categoryParser->parse($output, $sxe, Category::TYPE_PRODUCT_SECTION);
                            break;
                        case "Products":
                            $output->write("Парсинг товаров: \n");
                            $this->productParser->parse($output, $sxe);
                            return;
                    }
                    break;
            }
        }
    }

    public function stores(string $filename, OutputInterface $output)
    {
        $output->writeln("Read ProductsStock.xml");

        $reader = new XMLReader();
        $reader->open($filename);

        while ($reader->read()){
            switch ($reader->nodeType) {
                case XMLReader::ELEMENT:
                    $node = $reader->expand();
                    $dom = new \DOMDocument();
                    $n = $dom->importNode($node, true);
                    $dom->appendChild($n);
                    $sxe = simplexml_import_dom($n);
                    switch ($reader->localName){
                        case "Stores":
                            $output->writeln("Парсинг складов");
                            $this->storeParser->stores($sxe);
                            break;
                        case "Products":
                            $output->writeln("Парсинг остатков");
                            $this->storeParser->rests($output, $sxe);
                            break;
                    }
                    break;
            }
        }
    }

    public function productPrices(string $filename, OutputInterface $output)
    {
        $output->writeln("Read ProductsPrices.xml");

        $reader = new XMLReader();
        $reader->open($filename);

        while ($reader->read()){
            switch ($reader->nodeType) {
                case XMLReader::ELEMENT:
                    $node = $reader->expand();
                    $dom = new \DOMDocument();
                    $n = $dom->importNode($node, true);
                    $dom->appendChild($n);
                    $sxe = simplexml_import_dom($n);
                    switch ($reader->localName){
                        case "ProductsPrices":
                            $output->writeln("Парсинг цен");
                            $this->priceParser->parse($output, $sxe);
                            break;
                    }
                    break;
            }
        }
    }

    public function parseBuyers(string $filename, OutputInterface $output)
    {
        $output->writeln("Read Buyers.xml");

        $reader = new XMLReader();
        $reader->open($filename);

        while ($reader->read()){
            switch ($reader->nodeType) {
                case XMLReader::ELEMENT:
                    $node = $reader->expand();
                    $dom = new \DOMDocument();
                    $n = $dom->importNode($node, true);
                    $dom->appendChild($n);
                    $sxe = simplexml_import_dom($n);
                    switch ($reader->localName){
                        case "BuyersInformation":
                            $output->writeln("Парсинг пользователей");
                            $this->buyersParser->parse($output, $sxe);
                            break;
                    }
                    break;
            }
        }
    }
}