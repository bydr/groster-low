<?php

namespace App\Helpers\Parsers;


use App\Entity\Category;
use App\Entity\Product;
use App\Entity\Rest;
use App\Entity\Store;
use App\Entity\WaitingProduct;
use App\EventListener\EmailListener;
use App\Helpers\CategoryHelper;
use App\Helpers\ValidateHelper;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use SimpleXMLElement;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class StoreParser
{
    /** @var EntityManagerInterface */
    private $em;

    public function __construct(EntityManagerInterface $em, EmailListener $mailer)
    {
        $this->em = $em;
        $this->mailer = $mailer;
    }

    public function stores(SimpleXMLElement $sxe): void
    {
        foreach ($sxe->children() as $child) {
            $this->saveStore($child);
        }

        $this->em->flush();
    }

    public function rests(OutputInterface $output, ?SimpleXMLElement $sxe)
    {
        $count = 0;
        $notifications = [];
        $waiting = $this->em->getRepository(WaitingProduct::class)->getAllAsArray();
        foreach ($sxe->children() as $child) {
            $this->saveRest($output, $child, $waiting, $notifications);
            $count++;
            if($count % 100 == 0) {
                $output->write($count . " | ");
            }
        }

        $output->writeln($count);

        $this->em->flush();

        $this->updateKitRest($notifications);

        $this->em->flush();

        $output->writeln('Отправка уведомлений о наличии товаров');
        foreach ($notifications as $contact => $waitings) {
            $this->mailer->sendEmailNotification($contact, $waitings);

            /** @var WaitingProduct $waiting */
            foreach ($waitings as $waiting) {
                $waiting->setNotificatedAt(new DateTime());
            }
        }

        $this->em->flush();
    }

    /**
     * Обновление складов
     * @param SimpleXMLElement|null $sxe
     */
    private function saveStore(?SimpleXMLElement $sxe): void
    {
        $attributes = $sxe->attributes();
        $uuid = $attributes['ID'];
        $name = $attributes['Name'];
        $isActive = $attributes['Active'] == 'true';
        $address = $attributes['ActualAddressRepresentation'];
        $geoLat = $attributes['ActualAddressGeoLat'];
        $geoLan = $attributes['ActualAddressGeoLan'];
        $email = $attributes['Email'];
        $phoneStr = $attributes['Tel'];
        $description = $attributes['VerbalDescription'];
        $schedule = $attributes['Schedule'];

        /** @var Store $store */
        $store = $this->em->getRepository(Store::class)->findOneByUuid($uuid);

        if(!$store){
            $store = new Store();
            $store->setUuid($uuid);
            if(Store::MAIN_STORE_UUID == $uuid) {
                $store->setIsMain(true);
            }

            $this->em->persist($store);
        } else {
            if ($store->getIsActive() && !$isActive) {
                foreach ($store->getRests() as $rest) {
                    $rest->setValue(0);
                }
            }
        }

        $store->setName($name);
        $store->setIsActive($isActive);
        if(!empty($address)) {
            $store->setAddress($address);
        }
        if(!empty($geoLat)) {
            $store->setLatitude($geoLat);
        }
        if(!empty($geoLan)) {
            $store->setLongitude($geoLan);
        }
        if(!empty($email)) {
            $store->setEmail($email);
        }
        if(!empty($phoneStr)) {
            $store->setPhone($phoneStr);
        }

        if(!empty($description)) {
            $store->setDescription($description);
        }

        if(!empty($schedule)) {
            $store->setSchedule($schedule);
        }

    }

    /**
     * Обновление остатков на складах
     * @param OutputInterface $output
     * @param SimpleXMLElement|null $sxe
     * @param array $waitings
     */
    private function saveRest(OutputInterface $output, ?SimpleXMLElement $sxe, array $waitings, array &$notifications)
    {
        $productCollectionUuid = $sxe->attributes()['IDCollectionProduct'];
        // Если это вариация
        if("00000000-0000-0000-0000-000000000000" != $productCollectionUuid) {
            $productUuid = $productCollectionUuid;
        }else{
            $productUuid = $sxe->attributes()['IDProduct'];
        }

        /** @var Product $product */
        $product = $this->em->getRepository(Product::class)->findOneByUuid($productUuid);

        if(!$product){
            $output->writeln("Обновление остатков: не найден товар " . $productUuid);
            return;
        }

        $oldTotalRest = $product->getTotalQty();

        $totalRest = 0;
        foreach ($sxe->children() as $child) {
            $attributes = $child->attributes();
            $storeUuid = $attributes['StoreID'];
            $value = intval($attributes['Value']->__toString());

            /** @var Store $store */
            $store = $this->em->getRepository(Store::class)->findOneByUuid($storeUuid);
            if(!$store){
                $output->writeln('Обновление остатков: не найден склад ' . $storeUuid);
                continue;
            }

            /** Если склад не активен, то обнуляем остатки на нем */
            if(!$store->getIsActive()) {
                $value = 0;
            }

            $rest = $this->em->getRepository(Rest::class)->findOneBy(['product' => $product, 'store' => $store]);
            if(!$rest) {
                $rest = new Rest();
                $rest->setProduct($product);
                $rest->setStore($store);
                $product->addRest($rest);

                $this->em->persist($rest);
            }

            $rest->setValue($value);
            $rest->setUpdateAt(new DateTime());

            $totalRest += $value;

            /** Если это основной склад, то указываем остатки в товаре
             */
            if($store->getIsMain()){
                $product->setFastQty($value);
            }
        }

        $product->setTotalQty($totalRest);

        // Отправка уведомления, если товар появился после отсутствия
        if(0 == $oldTotalRest && $totalRest > 0) {
            if(empty($waitings[$product->getUuid()])) {
                return;
            }
            $contacts = $waitings[$product->getUuid()];
            /** @var WaitingProduct $waiting */
            foreach ($contacts as $waiting) {
                $notifications[$waiting->getContact()][] = $waiting;
            }
        }
    }

    /**
     * Установка остатков для наборов
     */
    private function updateKitRest(array &$notifications): void
    {
        $kits = $this->em->getRepository(Product::class)->findKits();

        // Наборы
        /** @var Product $kit */
        foreach ($kits as $kit) {
            $oldTotalRest = $kit->getTotalQty();
            // Минимальные остатки по каждому складу
            $rests = [];

            // Элементы состава набора
            /** @var Product $child */
            foreach ($kit->getChildren() as $child) {
                foreach ($child->getRests() as $rest) {
                    $storeId = $rest->getStore()->getId();
                    if(!isset($rests[$storeId])) {
                        $rests[$storeId] = PHP_INT_MAX;
                    }

                    $rests[$storeId] = min($rests[$storeId], $rest->getValue());
                }
            }

            $totalRest = 0;
            foreach ($kit->getRests() as $rest) {
                $value = $rests[$rest->getStore()->getId()];
                $rest->setValue($value);
                $totalRest += $value;

                if(Store::MAIN_STORE_UUID == $rest->getStore()->getUuid()) {
                    $kit->setFastQty($value);
                }
            }

            $kit->setTotalQty($totalRest);

            // Если товар появился после отсутствия
            if(0 == $oldTotalRest && $totalRest > 0) {
                if(empty($waitings[$kit->getUuid()])) {
                    return;
                }
                $contacts = $waitings[$kit->getUuid()];
                /** @var WaitingProduct $waiting */
                foreach ($contacts as $waiting) {
                    $notifications[$waiting->getContact()][] = $waiting;
                }
            }
        }
    }
}