<?php

namespace App\Helpers\Parsers;

use App\Controller\ApiController;
use App\Entity\Price;
use App\Entity\Security\AuthUser;
use Doctrine\ORM\EntityManagerInterface;
use SimpleXMLElement;
use Symfony\Component\Console\Output\OutputInterface;

class BuyerPraser
{
    /** @var EntityManagerInterface */
    private EntityManagerInterface $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function parse(OutputInterface $output, SimpleXMLElement $sxe): void
    {
        $count = 0;
        foreach ($sxe->Buyers->children() as $child) {
            $this->saveBuyer($output, $child);
            if($count++ % 100 == 0) {
                $output->write($count . " | ");
            }
        }

        $this->em->flush();
        $output->writeln("");
    }

    /**
     * Обновление цен
     * @param OutputInterface $output
     * @param SimpleXMLElement|null $sxe
     */
    private function saveBuyer(OutputInterface $output, ?SimpleXMLElement $sxe): void
    {
        $attributes = $sxe->attributes();
        $userUid = $attributes['IDSiteBuyer'];
        /** @var AuthUser $user */
        $user = $this->em->getRepository(AuthUser::class)->findOneBy(['uid' => $userUid]);
        if (!$user) {
            $msg = sprintf("Парсинг покупателей: не найден покупатель с uid %s", $userUid);
            $output->writeln($msg);
            ApiController::getLogger()->debug($msg);
            return;
        }

        $priceUuid = $attributes['IDPriceType'];
        /** @var Price $price */
        $price = $this->em->getRepository(Price::class)->findOneBy(['uuid' => $priceUuid]);
        if(!$price) {
            $msg = sprintf("Парсинг покупателей: не найден тип цен с uid %s", $priceUuid);
            $output->writeln($msg);
            ApiController::getLogger()->debug($msg);
            return;
        }

        $user->setPriceType($price);
    }
}