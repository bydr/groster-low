<?php

namespace App\Helpers\Parsers;


use App\Entity\Price;
use App\Entity\Product;
use App\Entity\ProductPrice;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use SimpleXMLElement;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class PriceParser
{
    /** @var EntityManagerInterface */
    private EntityManagerInterface $em;

    private array $priceTypes = [];

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function parse(OutputInterface $output, SimpleXMLElement $sxe): void
    {
        $this->savePriceTypes($sxe->PriceTypes);

        $count = 0;
        foreach ($sxe->ProductsPrice->children() as $child) {
            $this->savePrice($output, $child);
            if($count++ % 100 == 0) {
                $output->write($count . " | ");
            }
        }

        $this->em->flush();
        $output->writeln("");

        $this->updateKitPrice();
    }

    /**
     * Обновление цен
     * @param OutputInterface $output
     * @param SimpleXMLElement|null $sxe
     */
    private function savePrice(OutputInterface $output, ?SimpleXMLElement $sxe): void
    {
        $attributes = $sxe->attributes();
        $uuid = $attributes['IDProduct'];

        /** @var Product $product */
        $product = $this->em->getRepository(Product::class)->findOneByUuid($uuid);

        if(!$product){
            $output->writeln("Парсинг цен: не найден товар " . $uuid);
            return;
        }

        $mainPrice = 0;         // базовая цена для товара
        $activePrice = false;   // есть ли хоть одна активная цена
        foreach ($sxe->Prices->children() as $priceEl) {
            if($priceEl->attributes()['PriceSet'] != "true") {
                continue;
            }

            $activePrice = true;

            /** @var Price $priceType */
            $priceType = $this->priceTypes[$priceEl->attributes()['IDProductsPriceType']->__toString()];
            $hasCollection = $priceEl->attributes()['PriceHasCollection'] == 'true';
            $priceValue = intval(floatval($priceEl->attributes()['Value']->__toString())  * 100);

            if(!$hasCollection) {
                // Если еще не установили цену или это основная цена
                // Это необходимо, т.к. в выгрузке для вариативных товаров указана розничная цена на базе
                if(0 == $mainPrice || $priceType->getIsDefault()) {
                    $mainPrice = $priceValue;
                }

                $this->setProductPrice($product, $priceType, $priceValue);
                continue;
            }

            $variationUuid = $priceEl->attributes()['IDCollectionProduct'];
            /** @var Product $variation */
            $variation = $this->em->getRepository(Product::class)->findOneByUuid($variationUuid);

            if(!$variation) {
                $output->writeln("Парсинг цен: не найдена вариация " . $variationUuid);
                continue;
            }

            if($priceType->getIsDefault()) {
                $variation->setPrice($priceValue);
            }

            $this->setProductPrice($product, $priceType, $priceValue);
        }

        if($activePrice && is_null($mainPrice)) {
            $output->writeln("Парсинг цен: не установлена цена для товара " . $product->getUuid());
        }
        $product->setPrice($mainPrice);
    }

    /**
     * Установка цен для наборов
     */
    private function updateKitPrice(): void
    {
        $kits = $this->em->getRepository(Product::class)->findKits();

        /** @var Product $kit */
        foreach ($kits as $kit) {
            $price = 0;
            foreach ($kit->getChildren() as $item) {
                $price += $item->getPrice();
            }
            $kit->setPrice($price);
        }

        $this->em->flush();
    }

    /**
     * Сохранение типов цен
     * @param SimpleXMLElement $sxe
     * @return void
     */
    private function savePriceTypes(SimpleXMLElement $sxe)
    {
        $defaultPriceType = $sxe->attributes()['IDMainPriceType'];

        $priceRepo = $this->em->getRepository(Price::class);
        foreach ($sxe->children() as $priceType) {
            $uuid = $priceType->attributes()['IDPriceType']->__toString();
            $name = $priceType->attributes()['NamePriceType']->__toString();

            $price = $priceRepo->findOneByUuid($uuid);

            if(!$price) {
                $price = new Price();
                $price->setUuid($uuid);
                $this->em->persist($price);
            }

            $price->setName($name);
            $price->setIsDefault($defaultPriceType == $uuid);

            $this->priceTypes[$uuid] = $price;
        }

        $this->em->flush();
    }

    /**
     * Связка товара и типа цен
     * @param Product $product
     * @param Price $priceType
     * @param int $priceValue
     * @return void
     */
    private function setProductPrice(Product $product, Price $priceType, int $priceValue)
    {
        $productPrice = $product->getProductPrice($priceType);

        if(!$productPrice) {
            $productPrice = new ProductPrice();
            $productPrice->setPrice($priceType);
            $productPrice->setProduct($product);
            $product->addPrice($productPrice);
            $this->em->persist($productPrice);
        }

        $productPrice->setValue($priceValue);
        $productPrice->setUpdateAt(new DateTime());
    }
}