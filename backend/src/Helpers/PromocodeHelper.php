<?php

namespace App\Helpers;

use App\Entity\Discount;
use App\Entity\Order;
use App\Entity\Security\AuthUser;
use Doctrine\ORM\EntityManagerInterface;

/**
 * Вспомагательный класс для промокода
 */
class PromocodeHelper
{
    /** @var EntityManagerInterface */
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * Проверка возможности использовать промокод
     * @param Discount $promo
     * @param AuthUser $user
     * @return bool
     */
    public function canUse(Discount $promo, AuthUser $user): bool
    {
        // Если истек срок действия
        if (!$promo->isActive()) {
            return false;
        }

        // Если многоразовый
        if ($promo->getReusable()) {
            return true;
        }

        return !$this->em->getRepository(Order::class)->existsOrdersWithPromoByUser($promo, $user);
    }
}