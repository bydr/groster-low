<?php

namespace App\Helpers;

use App\Controller\ApiController;
use App\Repository\CategoryRepository;
use App\Repository\ProductRepository;

class TextHelper
{
    /**
     * Translit text from ru to en
     * @param $text
     * @param string $translit
     * @return string
     */
    public static function translitRu2En ($text, $replacement = "-") {
        $tr = array(
            "А"=>"a","Б"=>"b","В"=>"v","Г"=>"g","Д"=>"d",
            "Е"=>"e","Ё"=>"yo","Ж"=>"j","З"=>"z","И"=>"i",
            "Й"=>"y","К"=>"k","Л"=>"l","М"=>"m","Н"=>"n",
            "О"=>"o","П"=>"p","Р"=>"r","С"=>"s","Т"=>"t",
            "У"=>"u","Ф"=>"f","Х"=>"h","Ц"=>"c","Ч"=>"ch",
            "Ш"=>"sh","Щ"=>"sch","Ъ"=>"","Ы"=>"yi","Ь"=>"",
            "Э"=>"e","Ю"=>"yu","Я"=>"ya","а"=>"a","б"=>"b",
            "в"=>"v","г"=>"g","д"=>"d","е"=>"e","ё"=>"yo","ж"=>"j",
            "з"=>"z","и"=>"i","й"=>"y","к"=>"k","л"=>"l",
            "м"=>"m","н"=>"n","о"=>"o","п"=>"p","р"=>"r",
            "с"=>"s","т"=>"t","у"=>"u","ф"=>"f","х"=>"h",
            "ц"=>"c","ч"=>"ch","ш"=>"sh","щ"=>"sch","ъ"=>"y",
            "ы"=>"y","ь"=>"","э"=>"e","ю"=>"yu","я"=>"ya",
            " "=> "_", "."=> "", "/"=> "_"
        );

        $str = (string) $text; // преобразуем в строковое значение
        $str = strip_tags($str); // убираем HTML-теги
        $str = str_replace(array("\n", "\r"), " ", $str); // убираем перевод каретки
        $str = function_exists('mb_strtolower') ? mb_strtolower($str) : strtolower($str); // переводим строку в нижний регистр (иногда надо задать локаль)
        $str = strtr($str, $tr);    // транслитерируем строку
        $str = preg_replace("/[^0-9a-z]/i", " ", $str); // устанавливаем разделитель
        $str = preg_replace("/\s+/", ' ', $str); // удаляем повторяющие пробелы
        $str = trim($str);
        $str = preg_replace("/\s/", $replacement, $str); // меняем пробел на заменитель

        return $str;
    }

    /**
     * @param $name
     * @param CategoryRepository|ProductRepository $repo
     * @return string
     */
    public static function createAlias($uuid, $translit, $repo, $createdAliases = []): string
    {
        $aliases = $repo->getSameAliases($translit);

        $createdAliases = array_merge($createdAliases, $aliases);
        $alias = $translit;

        // Если $i >= 100, значит какая-то ошибка и прерываем цикл
        for($i = 1; $i < 100; $i++) {
            if(!in_array($alias, $createdAliases)) {
                return $alias;
            }

            // Если это алиас для текущего элемента
            if(isset($createdAliases[$uuid]) && $createdAliases[$uuid] == $alias) {
                return $alias;
            }

            $alias = sprintf("%s_%d", $translit, $i);
        }

        ApiController::getLogger()->warning("Failed to create alias for {$name}");

        return $name;
    }


}