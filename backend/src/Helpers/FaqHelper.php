<?php

namespace App\Helpers;

use App\Controller\ApiController;
use App\Entity\FaqType;
use App\Service\Cache;
use Doctrine\ORM\EntityManagerInterface;

/**
 * Вспомагательные методы по работе с блоком вопрос-ответ
 */
class FaqHelper
{
    /**
     * Возвращает список вопрос-ответ в json
     * @param Cache $cache
     * @param EntityManagerInterface $em
     * @return string
     */
    public static function getList(Cache $cache, EntityManagerInterface $em): string
    {
        $faqs = $cache->getFaqList();

        if (!empty($faqs)) {
            return $faqs;
        }

        return self::updateFaq($cache, $em);
    }

    /**
     * Возвращает блок вопро-ответ в json формате
     * @param Cache $cache
     * @param EntityManagerInterface $em
     * @return string
     */
    public static function updateFaq(Cache $cache, EntityManagerInterface $em): string
    {
        $faqs = self::createFaq($em);

        if(empty($faqs)) {
            return "";
        }

        $json = SerializerHelper::serialize($faqs, [SerializerHelper::GROUP_FAQ]);

        if(!$cache->saveFaqList($json)) {
            ApiController::getLogger()->error("Failed to save faqs in cache");
        }

        return $json;
    }

    /**
     * Список блоков вопрос-ответ
     * @param EntityManagerInterface $em
     * @return array
     */
    private static function createFaq(EntityManagerInterface $em): array
    {
        $all = $em->getRepository(FaqType::class)->findBy([], ['weight' => 'asc', 'id' => 'desc']);

        $faqs = [];
        /** @var FaqType $block */
        foreach ($all as $block) {
            if(!count($block->getFaqs())) {
                continue;
            }

            $faqs[] = $block;
        }

        return $faqs;
    }
}