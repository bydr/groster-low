<?php

namespace App\Helpers;

use App\Controller\ApiController;
use App\Entity\Param;
use App\Entity\Value;
use App\Service\Cache;
use Doctrine\ORM\EntityManagerInterface;

/**
 * Вспомагательные методы для работы с параметрами
 */
class ParamsHelper
{
    /**
     * Возвращает json список категорий
     *
     * @param Cache $cache
     * @param EntityManagerInterface $em
     * @return string
     */
    public static function getList(Cache $cache, EntityManagerInterface $em): string
    {
        $params = $cache->getParamsWithProductQty();

        if(!empty($params)) {
            return $params;
        }

        return self::updateParamsWithProductQty($cache, $em);
    }

    /**
     * Группировка выбранных на сайте значений параметров по их родителям.
     * Используется для правильного составления запроса
     * @param array $selectedParams
     * @param Cache $cache
     * @param EntityManagerInterface $em
     * @return array
     */
    public static function groupParams(array $selectedParams, Cache $cache, EntityManagerInterface $em): array
    {
        $allParams = self::getValuesList($cache, $em);

        $params = [];
        foreach ($selectedParams as $selectedParam) {
            if(!isset($allParams[$selectedParam])) {
                continue;
            }
            $params[$allParams[$selectedParam]][] = $selectedParam;
        }

        return $params;
    }

    /**
     * Список значений параметров
     * @param Cache $cache
     * @param EntityManagerInterface $em
     * @return array
     */
    private static function getValuesList(Cache $cache, EntityManagerInterface $em): array
    {
        $cachedParams = $cache->getParams();
        if(!empty($cachedParams)) {
            return json_decode($cachedParams, true);
        }

        $params = [];

        /** @var Value $item */
        foreach ($em->getRepository(Value::class)->findAll() as $item) {
            $params[$item->getUuid()] = $item->getParam()->getId();
        }

        if(!empty($params)) {
            $cache->setParams(json_encode($params));
        }

        return $params;
    }

    /**
     * Декартово множество сгруппированных параметров
     * @param $arr
     * @return array
     */
    public static function cartesian($arr): array
    {
        $variant = array();
        $result = array();
        $sizearr = sizeof($arr);

        return self::recursive($arr, $variant, -1, $result, $sizearr);
    }

    private static function recursive($arr, $variant, $level, $result, $sizearr){
        $keys = array_keys($arr);
        $level++;
        if($level<$sizearr){
            foreach ($arr[$keys[$level]] as $val){
                $variant[$keys[$level]] = $val;
                $result = self::recursive($arr, $variant, $level, $result, $sizearr);
            }
        }else{
            $result[] = $variant;
        }
        return $result;
    }

    private static function updateParamsWithProductQty(Cache $cache, EntityManagerInterface $em)
    {
        $params = $em->getRepository(Param::class)->findAllAvailableWithProductQty();

        $jsonTree = SerializerHelper::serialize($params, [SerializerHelper::GROUP_PARAMS]);
        if(!$cache->saveParamsWithProductQty($jsonTree)) {
            ApiController::getLogger()->error("Failed to save total categories in cache");
        }

        return $jsonTree;
    }
}