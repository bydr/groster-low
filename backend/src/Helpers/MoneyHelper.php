<?php

namespace App\Helpers;

class MoneyHelper
{

    public static function convertToRubles(int $discount)
    {
        return $discount / 100;
    }
}