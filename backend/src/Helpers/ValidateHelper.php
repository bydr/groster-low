<?php


namespace App\Helpers;


use libphonenumber\NumberParseException;
use libphonenumber\PhoneNumberFormat;
use libphonenumber\PhoneNumberUtil;

/**
 * Валидации
 */
class ValidateHelper
{
    const PHONE_COUNTRY = "RU";

    /**
     * Валидация email
     * @param $email
     * @return bool
     */
    public static function validateEmail($email): bool
    {
       return (bool) preg_match("~^\w+([-+.%']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$~", $email);
    }

    /**
     * Валидация телефона РФ
     * @param $phone
     * @return string|null
     */
    public static function getValidPhone($phone): ?string
    {
        $phoneUtil = PhoneNumberUtil::getInstance();
        try {
            $phoneNubmerProto = $phoneUtil->parse($phone, self::PHONE_COUNTRY);
            if(!$phoneUtil->isValidNumberForRegion($phoneNubmerProto, self::PHONE_COUNTRY)) {
                return null;
            }

            return $phoneUtil->format($phoneNubmerProto, PhoneNumberFormat::E164);
        } catch (NumberParseException $e) {
            return null;
        }
    }
}
