<?php

namespace App\Helpers;

use App\Controller\ApiController;
use App\Entity\Category;
use App\Service\Cache;
use Doctrine\ORM\EntityManagerInterface;

/**
 * Вспомогательные методы по работе с категориями
 */
class CategoryHelper
{
    const KEY_TOTAL = "total";

    /**
     * Возвращает json список категорий
     *
     * @param Cache $cache
     * @param EntityManagerInterface $em
     * @return string
     */
    public static function getList(Cache $cache, EntityManagerInterface $em): string
    {
        $categoriesTree = $cache->getCategoriesList(self::KEY_TOTAL);

        if(!empty($categoriesTree)) {
            return $categoriesTree;
        }

        return self::updateList($cache, $em);
    }

    /**
     * Обновление категорий в кеше
     * @param Cache $cache
     * @param EntityManagerInterface $em
     * @return string
     */
    public static function updateList(Cache $cache, EntityManagerInterface $em): string
    {
        $categoriesTree = self::createList($em);

        $jsonTree = SerializerHelper::serialize($categoriesTree, [SerializerHelper::GROUP_CATEGORIES]);

        if(!$cache->saveCategoriesList($jsonTree, self::KEY_TOTAL)) {
            ApiController::getLogger()->error("Failed to save total categories in cache");
        }

        return $jsonTree;
    }

    /**
     * Создание общего списка категорий
     *
     * @param EntityManagerInterface $em
     * @return array
     */
    private static function createList(EntityManagerInterface $em): array
    {
        $bussinessAreas = $em->getRepository(Category::class)->findBy(['type' => Category::TYPE_BUSSINESS_AREA, 'isActive' => true], ["id" => "ASC"]);
        $categories = $em->getRepository(Category::class)->findBy(['type' => Category::TYPE_PRODUCT_SECTION, 'isActive' => true], ["id" => "ASC"]);

        return [
            "bussinessAreas" => $bussinessAreas,
            "categories" => $categories
        ];
    }

    private static function createListByArea(EntityManagerInterface $em, string $areaUuid): array
    {
        $categories = $em->getRepository(Category::class)->getByBussinessArea($areaUuid);

        $list = [];


        $cats = []; // список всех категорий от текущей до родителя. Нужно, чтобы потом вернуть только их
        $mains = []; // список главных родительских категорий. Нужно, чтобы знать откуда начинать спускаться
        /** @var Category $category */
        foreach ($categories as $category) {
            self::getCategoryList($category, $cats, $mains);
            $cats[$category->getId()] = $category;
        }

        $childrenList = array_keys($cats);
        /** @var Category $main */
        foreach ($mains as $main) {
            $list[] = [
                'uuid' => $main->getUuid(),
                'product_qty' => $main->getProductQty(),
                'children' => self::getChildren($main, $childrenList)
            ];
        }

        return $list;
    }

    private static function getChildren($category, $childrenList)
    {
        $children = [];
        /** @var Category $child */
        foreach ($category->getChildren() as $child) {
            if(!in_array($child->getId(), $childrenList)) {
                continue;
            }
            $children[] = [
                'uuid' => $child->getUuid(),
                'product_qty' => $child->getProductQty(),
                'children' => self::getChildren($child, $childrenList)
            ];
        }

        return $children;
    }

    private static function getCategoryList(Category $category, &$list, &$parents)
    {
        if($category->getParent()) {
            $list[$category->getId()] = null;
            self::getCategoryList($category->getParent(), $list, $parents);
        }

        $parents[] = $category;
    }

    /**
     * Возвращает json список категорий
     *
     * @param Cache $cache
     * @param EntityManagerInterface $em
     * @return string
     */
    public static function getListForArea(Cache $cache, EntityManagerInterface $em, string $areaUuid): string
    {
        $categoriesTree = $cache->getCategoriesList($areaUuid);

        if(!empty($categoriesTree)) {
            return $categoriesTree;
        }

        $list = json_encode(self::createListByArea($em, $areaUuid));

        if(!$cache->saveCategoriesList($list, $areaUuid)) {
            ApiController::getLogger()->error(sprintf("Failed to save category %s in cache", $areaUuid));
        }

        return $list;
    }

    /**
     * Получение всех дочерних IDS для фильтрации товаров
     * @param $categories
     * @param array $ids
     * @return array
     */
    public static function getChildrenIds($categories, array $ids = []): array
    {
        /** @var Category $category */
        foreach ($categories as $category) {
            $ids[$category->getId()] = $category->getId();

            $ids = self::getChildrenIds($category->getChildren(), $ids);
        }

        return $ids;
    }
}