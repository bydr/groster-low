<?php

namespace App\Helpers;

class Params
{
    const ACCESS_TOKEN = 'access_token';
    const ACCOUNT = "account";
    const ADDED_QTY = 'addedQty';
    const ADDRESS = "address";
    const ALIAS = 'alias';
    const ALLOW_SAMPLE = 'allow_sample';
    const ANSWER = 'answer';
    const API_KEY = 'api_key';
    const AREAS = "areas";
    const BESTSELLER = 'bestseller';
    const BIK = "bik";
    const BLOCK = 'block';
    const BRAND = 'brand';
    const BUSSINESS_AREA = 'bussiness_area';
    const BUSINESS_AREAS = 'business_areas';
    const BUYERS = 'buyers';
    const ANALOGS = 'analogs';
    const CART = 'cart';
    const CATEGORIES = 'categories';
    const CATEGORY = 'category';
    const CODE = 'code';
    const COMMENT = 'comment';
    const COMPANIONS = 'companions';
    const CONTACT = "contact";
    const CONTACT_PHONE = "contact_phone";
    const CONTAINERS = 'containers';
    const COORDINATES = "coordinates";
    const COST = 'cost';
    const CREATE_AT = 'create_at';
    const DELIVERY_FAST_TIME = 'delivery_fast_time';
    const DELIVERY_SHIFT = 'delivery_shift';
    const DESCRIPTION = "description";
    const DESKTOP = 'desktop';
    const DISCOUNT = 'discount';
    const EMAIL = 'email';
    const EMAILS = 'emails';
    const FAST = 'fast';
    const FAST_QTY = 'fast_qty';
    const FIELDS = 'fields';
    const FINISH = "finish";
    const FIO = 'fio';
    const H1 = "h1";
    const HELP_PAGE = 'help_page';
    const HOLIDAYS = 'holidays';
    const ID = 'id';
    const IMAGE = 'image';
    const INN = 'inn';
    const IMAGES = 'images';
    const INFO = 'info';
    const INTERVAL = 'interval';
    const IP = 'ip';
    const IS_ACTIVE = 'is_active';
    const IS_ADMIN = 'is_admin';
    const IS_BESTSELLER = 'is_bestseller';
    const IS_DEFAULT = 'is_default';
    const IS_DELETE = 'is_delete';
    const IS_FREE = 'is_free';
    const IS_NEW = 'is_new';
    const IS_ENABLED = 'is_enabled';
    const IS_FAST = 'is_fast';
    const IS_MAIN = 'is_main';
    const IS_SEARCH = 'is_search';      // запрос используется в поиске
    const KEYWORDS = 'keywords';
    const KIT = 'kit';
    const KIT_PARENTS = 'kit_parents';
    const KPP = 'kpp';
    const LATITUDE = 'latitude';
    const LIFETIME = 'lifetime';
    const LINK = 'link';
    const LONGITUDE = 'longitude';
    const MAX = 'max';
    const MESSAGE = 'message';
    const META_DESCRIPTION = "meta_description";
    const META_TITLE = "meta_title";
    const MIN = 'min';
    const MIN_MANY_QUANTITY = 'min_many_quantity';
    const MOBILE = 'mobile';
    const NAME = "name";
    const NAME_1C = 'name_1c';
    const NEW = 'new';
    const NEW_PASSWORD = 'new_password';
    const NOT_CALL = 'not_call';
    const NUMBER = 'number';
    const OGRNIP = "ogrnip";
    const OK = 'ok';
    const OLD_PASSWORD = 'old_password';
    const ORDER_MAX_COST = 'order_max_cost';
    const ORDER_MIN_COST = 'order_min_cost';
    const ONLY_CONTAINERS = 'only_containers';
    const ORDER = 'order';
    const ORDERS = 'orders';
    const OTHER = 'other';
    const PAGE = 'page';
    const PARAMS = 'params';
    const PARENT = "parent";
    const PASSWORD = 'password';
    const PAYER = 'payer';
    const PAYMENT_METHOD = 'payment_method';
    const PAYMENT_METHODS = 'payment_methods';
    const PER_PAGE = 'per_page';
    const PHONE = 'phone';
    const PHONES = 'phones';
    const PRICE = 'price';
    const PRICE_RANGE = 'priceRange';
    const PRODUCT = "product";
    const PRODUCT_QTY = 'product_qty';
    const PRODUCTS = 'products';
    const PROPERTIES = 'properties';
    const QUANTITY = "quantity";
    const REFRESH_TOKEN = 'refresh_token';
    const REGION = 'region';
    const REGIONS = 'regions';
    const REPLACEMENT = 'replacement';
    const REPRESENTATION = 'representation';
    const RESTS = 'rests';
    const REUSABLE = "reusable";
    const SAMPLE = 'sample';
    const SCHEDULE = 'schedule';
    const SELECTION_FILE = 'selection_file';
    const SHIPPING_ADDRESS = 'shipping_address';
    const SHIPPING_COST = 'shipping_cost';
    const SHIPPING_DATE = 'shipping_date';
    const SHIPPING_METHOD = 'shipping_method';
    const SITE = 'site';
    const SOCIAL_BRAND = 'social_brand';
    const SORT_BY = 'sortby';
    const START = "start";
    const STATE = 'state';
    const STATUS = 'status';
    const STORE = 'store';
    const STORES = 'stores';
    const SUBSCRIBE_INTERVAL = 'subscribe_interval';
    const TABLET = 'tablet';
    const TAGS = 'tags';
    const TELEGRAM = 'telegram';
    const TEXT = "text";
    const TITLE = 'title';
    const TOTAL = 'total';
    const TOTAL_COST = 'total_cost';
    const TOTAL_QTY = 'total_qty';
    const TYPE = "type";
    const UID = 'uid';
    const UNIT = 'unit';
    const UNITS = 'units';
    const URL = "url";
    const USE_ADDRESS = 'use_address';
    const USER = 'user';
    const UUID = 'uuid';
    const VALUE = "value";
    const VARIATIONS = 'variations';
    const VIBER = 'viber';
    const WEIGHT = "weight";
    const WHATSAPP = 'whatsapp';
    const YEAR = 'year';

    const SORT_BY_PRICE_ASC = 'price_asc';
    const SORT_BY_PRICE_DESC = 'price_desc';
    const SORT_BY_NAME = 'name';
    const SORT_BY_POPULAR = 'popular';

    const ORDER_PER_PAGE = 20;
    const DOCUMENT_ATTACHED_CONTRACT = 'Подписанный договор плательщика';
    const DOCUMENT_CONTRACT = 'Договор плательщика';
    const DOCUMENT_INVOICE = 'Счет на оплату';
    const DOCUMENT_TORG12 = 'ТОРГ-12';

    public static function getSortTypes()
    {
        return [
            self::SORT_BY_NAME => 'по алфавиту',
            self::SORT_BY_POPULAR => 'по популярности',
            self::SORT_BY_PRICE_ASC => 'по цене',
            self::SORT_BY_PRICE_DESC => 'по цене',
        ];
    }

    /**
     * Возвращает тип контакта с пройденной валидацией
     *
     * @param string $key
     * @return string|null
     */
    public static function contactType(string $key): ?string
    {
        if(!empty(ValidateHelper::getValidPhone($key))){
            return Params::PHONE;
        }

        if(ValidateHelper::validateEmail($key)){
            return Params::EMAIL;
        }

        return null;
    }
}