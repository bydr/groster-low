<?php

namespace App\Helpers;

use App\Controller\ApiController;
use App\Entity\Banner;
use App\Service\Cache;
use Doctrine\ORM\EntityManagerInterface;

/**
 * Работа с баннерами
 */
class BannerHelper
{

    /**
     * @param Cache $cache
     * @param EntityManagerInterface $em
     * @return string
     */
    public static function getAll(Cache $cache, EntityManagerInterface $em): string
    {
        $banners = $cache->getBanners();

        if(!empty($banners)) {
            return $banners;
        }

        return self::update($cache, $em);
    }

    /**
     * Обновление баннеров в кеше
     * @param Cache $cache
     * @param EntityManagerInterface $em
     * @return string
     */
    public static function update(Cache $cache, EntityManagerInterface $em): string
    {
        $banners = self::createList($em);

        if(empty($banners)) {
            return "";
        }

        $jsonTree = SerializerHelper::serialize($banners, [SerializerHelper::GROUP_BANNERS]);

        if(!$cache->saveBanners($jsonTree)) {
            ApiController::getLogger()->error("Failed to save banners in cache");
        }

        return $jsonTree;
    }

    /**
     * Создание общего списка баннеров
     *
     * @param EntityManagerInterface $em
     * @return array
     */
    private static function createList(EntityManagerInterface $em): array
    {
        return $em->getRepository(Banner::class)->findAll();
    }
}