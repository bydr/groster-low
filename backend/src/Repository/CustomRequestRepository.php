<?php

namespace App\Repository;

use App\Entity\CustomRequest;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method CustomRequest|null find($id, $lockMode = null, $lockVersion = null)
 * @method CustomRequest|null findOneBy(array $criteria, array $orderBy = null)
 * @method CustomRequest[]    findAll()
 * @method CustomRequest[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CustomRequestRepository extends ServiceEntityRepository
{
    const TIME_UNIQ_RECALL = 60; // минимальное время в минутах между повторной отправкой заявки на звонок

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CustomRequest::class);
    }

    /**
     * Проверка на уникальность заявки обратного звонка в течение последнего часа
     * @param $phone
     * @return bool
     */
    public function isDoubleRecall($phone): bool
    {
        $doubleRecall = $this->createQueryBuilder('c')
            ->where('c.phone = :phone AND c.createdAt > :time')
            ->setParameter('phone', $phone)
            ->setParameter('time', (new \DateTime())->modify(sprintf('- %d minutes', self::TIME_UNIQ_RECALL)))
            ->getQuery()
            ->getArrayResult();

        return !empty($doubleRecall);
    }

    // /**
    //  * @return CustomRequest[] Returns an array of CustomRequest objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?CustomRequest
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
