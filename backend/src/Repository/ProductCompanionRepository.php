<?php

namespace App\Repository;

use App\Entity\ProductCompanion;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ProductCompanion|null find($id, $lockMode = null, $lockVersion = null)
 * @method ProductCompanion|null findOneBy(array $criteria, array $orderBy = null)
 * @method ProductCompanion[]    findAll()
 * @method ProductCompanion[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProductCompanionRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ProductCompanion::class);
    }

    // /**
    //  * @return ProductCompanion[] Returns an array of ProductCompanion objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ProductCompanion
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
