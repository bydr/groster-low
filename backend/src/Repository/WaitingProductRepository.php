<?php

namespace App\Repository;

use App\Entity\WaitingProduct;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method WaitingProduct|null find($id, $lockMode = null, $lockVersion = null)
 * @method WaitingProduct|null findOneBy(array $criteria, array $orderBy = null)
 * @method WaitingProduct[]    findAll()
 * @method WaitingProduct[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class WaitingProductRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, WaitingProduct::class);
    }

    public function setNotificationAt(WaitingProduct $waitingProduct)
    {
        $waitingProduct->setNotificatedAt(new \DateTime());
        $this->_em->flush();
    }

    /**
     * Возвращает список uuid товаров, о которых нужно сообщить о поступлении
     * @return array
     * @throws \Doctrine\DBAL\Driver\Exception
     * @throws \Doctrine\DBAL\Exception
     */
    public function getAllAsArray(): array
    {
        $waitings = $this->findBy(['notificatedAt' => null]);

        $uuids = [];
        foreach ($waitings as $waiting) {
            $uuids[$waiting->getProduct()->getUuid()][] = $waiting;
        }

        return $uuids;
    }

    // /**
    //  * @return WaitingProduct[] Returns an array of WaitingProduct objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('w')
            ->andWhere('w.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('w.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?WaitingProduct
    {
        return $this->createQueryBuilder('w')
            ->andWhere('w.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
