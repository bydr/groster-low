<?php

namespace App\Repository;

use App\Entity\FaqType;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method FaqType|null find($id, $lockMode = null, $lockVersion = null)
 * @method FaqType|null findOneBy(array $criteria, array $orderBy = null)
 * @method FaqType[]    findAll()
 * @method FaqType[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class FaqTypeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, FaqType::class);
    }

    // /**
    //  * @return FaqType[] Returns an array of FaqType objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('f.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?FaqType
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
