<?php

namespace App\Repository;

use App\Entity\ShippingLimit;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ShippingLimit|null find($id, $lockMode = null, $lockVersion = null)
 * @method ShippingLimit|null findOneBy(array $criteria, array $orderBy = null)
 * @method ShippingLimit[]    findAll()
 * @method ShippingLimit[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ShippingLimitRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ShippingLimit::class);
    }

    // /**
    //  * @return ShippingLimit[] Returns an array of ShippingLimit objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ShippingLimit
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
