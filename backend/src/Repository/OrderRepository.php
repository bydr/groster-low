<?php

namespace App\Repository;

use App\Controller\ApiController;
use App\Entity\Discount;
use App\Entity\Order;
use App\Entity\Param;
use App\Entity\Security\AuthUser;
use App\Helpers\Params;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\DBAL\ParameterType;
use Doctrine\ORM\Mapping\OrderBy;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\Persistence\ManagerRegistry;
use PDO;

/**
 * @method Order|null find($id, $lockMode = null, $lockVersion = null)
 * @method Order|null findOneBy(array $criteria, array $orderBy = null)
 * @method Order[]    findAll()
 * @method Order[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class OrderRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Order::class);
    }

    /**
     * @param AuthUser|null $user
     * @param string $cartUid
     * @param int $state
     * @return int|mixed|string|null
     * @throws NonUniqueResultException
     */
    public function findUserCart(?AuthUser $user, string $cartUid, int $state): ?Order
    {
        $query = $this->createQueryBuilder('c')
            ->where('c.uid = :uid AND c.state = :state')
            ->setParameter('uid', $cartUid, PDO::PARAM_STR)
            ->setParameter('state', $state);

        if($user){
            $query->andWhere('c.customer = :user')
                ->setParameter('user', $user);
        }else{
            $query->andWhere('c.customer IS NULL');
        }

        return $query->getQuery()->getOneOrNullResult();
    }

    /**
     * Проверяет есть ли у клиента еще заказы с таким промокодом
     * @param Discount $promo
     * @param AuthUser $user
     * @return bool
     */
    public function existsOrdersWithPromoByUser(Discount $promo, AuthUser $user): bool
    {
        $orders = $this->createQueryBuilder('o')
            ->where("o.customer = :customer AND o.discount = :discount")
            ->setParameter("customer", $user)
            ->setParameter("discount", $promo)
            ->getQuery()
            ->getResult();

        return count($orders) > 0;
    }

    /**
     * Возвращает последний оформленный заказ пользователя
     * @param AuthUser $customer
     * @return Order|null
     * @throws NonUniqueResultException
     */
    public function findLastUserOrder(AuthUser $customer): ?Order
    {
        return $this->createQueryBuilder('o')
            ->where('o.customer = :customer AND o.state > :state')
            ->setParameter("customer", $customer)
            ->setParameter("state", Order::STATE_CART)
            ->orderBy('o.checkoutCompleteAt', 'DESC')
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult();
    }

    /**
     * Список сфер бизнеса клиента
     * @param AuthUser $user
     * @return array
     * @throws \Doctrine\DBAL\Driver\Exception
     * @throws \Doctrine\DBAL\Exception
     */
    public function getUserAreas(AuthUser $user): array
    {
        $conn = $this->getEntityManager()->getConnection();

        $sql = 'SELECT DISTINCT bussiness_area FROM "order" WHERE customer_id = :customer AND state > :state';
        $stmt = $conn->prepare($sql);
        $stmt->bindValue("customer", $user->getId(), ParameterType::INTEGER);
        $stmt->bindValue("state", Order::STATE_CART, ParameterType::INTEGER);
        $result = $stmt->executeQuery();
        $areas = [];
        foreach ($result->fetchAllAssociative() as $item) {
            if(empty($item['bussiness_area'])) {
                continue;
            }

            $areas[] = $item['bussiness_area'];
        };

        return $areas;
    }

    /**
     * Список почт клиента
     * @param AuthUser $user
     * @return array
     * @throws \Doctrine\DBAL\Driver\Exception
     * @throws \Doctrine\DBAL\Exception
     */
    public function getUserEmails(AuthUser $user): array
    {
        $conn = $this->getEntityManager()->getConnection();

        $sql = 'SELECT DISTINCT email FROM "order" WHERE customer_id = :customer AND state > :state';
        $stmt = $conn->prepare($sql);
        $stmt->bindValue("customer", $user->getId(), ParameterType::INTEGER);
        $stmt->bindValue("state", Order::STATE_CART, ParameterType::INTEGER);
        $result = $stmt->executeQuery();
        $emails = [];
        foreach ($result->fetchAllAssociative() as $item) {
            if(empty($item['email'])) {
                continue;
            }

            $emails[] = $item['email'];
        };

        return $emails;
    }

    /**
     * Список телефонов клиента
     * @param AuthUser $user
     * @return array
     * @throws \Doctrine\DBAL\Driver\Exception
     * @throws \Doctrine\DBAL\Exception
     */
    public function getUserPhones(AuthUser $user): array
    {
        $conn = $this->getEntityManager()->getConnection();

        $sql = 'SELECT DISTINCT phone FROM "order" WHERE customer_id = :customer AND state > :state';
        $stmt = $conn->prepare($sql);
        $stmt->bindValue("customer", $user->getId(), ParameterType::INTEGER);
        $stmt->bindValue("state", Order::STATE_CART, ParameterType::INTEGER);
        $result = $stmt->executeQuery();
        $phones = [];
        foreach ($result->fetchAllAssociative() as $item) {
            if(empty($item['phone'])) {
                continue;
            }

            $phones[] = $item['phone'];
        };

        return $phones;
    }

    /**
     * Список истории заказов пользователя
     * @param AuthUser $customer
     * @param array $params
     * @return array
     */
    public function userOrders(AuthUser $customer, array $params): array
    {
        $query = $this->createQueryBuilder('o')
            ->where('o.customer = :customer')
            ->setParameter('customer', $customer)
            ->andWhere('o.state > 1');

        if(!empty($params[Params::STATE])) {
            $query->andWhere('o.state = :state')
                ->setParameter('state', $params[Params::STATE]);
        }

        if(!empty($params[Params::PAYER])) {
            $query->join('o.payer', 'payer')
                ->andWhere('payer.uid IN (:payers)')
                ->setParameter('payers', $params[Params::PAYER])
            ;
        }

        if(!empty($params[Params::YEAR])) {
            $query->andWhere("o.checkoutCompleteAt >= :start AND o.checkoutCompleteAt <= :end")
                ->setParameter('start', sprintf("%s-01-01 00:00:00", $params[Params::YEAR]))
                ->setParameter('end', sprintf("%s-12-31 23:59:59", $params[Params::YEAR]))
            ;
        }

        $total = clone $query;

        $result = [
            Params::TOTAL => count($total->getQuery()->getResult()),
            Params::ORDERS => [],
        ];


        if(0 == $result[Params::TOTAL]) {
            return $result;
        }

        $query->setMaxResults($params[Params::PER_PAGE]);
        $query->setFirstResult(($params[Params::PAGE] - 1) * $params[Params::PER_PAGE]);
        $query->orderBy('o.checkoutCompleteAt', 'DESC');

        $result[Params::ORDERS] = $query->getQuery()->getResult();

        return $result;
    }

    /**
     * Возвращает заказ для изменения статуса через 1С
     * @param string $uid
     * @return Order|null
     * @throws NonUniqueResultException
     */
    public function findOnePrepared(string $uid): ?Order
    {
        return $this->createQueryBuilder('o')
            ->where('o.uid = :uid AND o.state > :state')
            ->setParameter('uid', $uid)
            ->setParameter('state', Order::STATE_CART)
            ->getQuery()
            ->getOneOrNullResult();
    }

    // /**
    //  * @return Order[] Returns an array of Order objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('o.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Order
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
