<?php

namespace App\Repository;

use App\Controller\ApiController;
use App\Entity\Category;
use App\Entity\Product;
use App\Entity\ProductValue;
use App\Entity\Value;
use App\Helpers\Params;
use App\Helpers\ParamsHelper;
use App\Helpers\ProductHelper;
use App\Service\Cache;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\DBAL\Types\Type;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;
use PDO;

/**
 * @method Product|null find($id, $lockMode = null, $lockVersion = null)
 * @method Product|null findOneBy(array $criteria, array $orderBy = null)
 * @method Product[]    findAll()
 * @method Product[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 * @method Product[]    findByUuid(array $uuids)
 * @method Product|null findOneByUuid(string $productUuid)
 */
class ProductRepository extends ServiceEntityRepository
{
    const HITS_LIMIT = 10;

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Product::class);
    }

    /**
     * @param array $params
     * @return array
     */
    public function filter(array $params): array
    {
        $expr = $this->getEntityManager()->getExpressionBuilder();
        $query = $this->createQueryBuilder('p');
        $query->join('p.categoriesUuid', 'c')
            ->where('p.price > 0 and p.isActive = true')
        ;

        // Exclude variation parents
        $query->andWhere(
            $expr->notIn(
                'p.id',
                $this->getEntityManager()->createQueryBuilder()
                    ->select('p2.id')
                    ->distinct(true)
                    ->from(Product::class, 'p1')
                    ->join('p1.collectionParent', 'p2')
                    ->getDQL()
            )
        );

        if(!empty($params[Params::CATEGORIES])) {
            $query->andWhere($expr->in('c.id', $params[Params::CATEGORIES]));
        }

        if(!empty($params[Params::PRICE])) {
            $query->andWhere('p.price BETWEEN :min_price AND :max_price')
                ->setParameter('min_price', $params[Params::PRICE][0], PDO::PARAM_INT)
                ->setParameter('max_price', $params[Params::PRICE][1], PDO::PARAM_INT);
        }

        if($params[Params::IS_ENABLED]){
            $query->andWhere('p.totalQty > 0');
        }

        if($params[Params::IS_FAST]){
            $query->andWhere('p.fastQty > 0');
        }

        if($params[Params::BESTSELLER]) {
            $query->andWhere('p.isBestseller = true');
        }

        if($params[Params::NEW]) {
            $query->andWhere('p.isNew = true');
        }

        if(!empty($params[Params::STORE])) {
            $query->innerJoin('p.rests', 'r')
                ->innerJoin('r.store', 's')
                ->andWhere('r.value > 0 AND s.uuid IN (:stores)')
                ->setParameter('stores', $params[Params::STORE]);
        }

        $filteredParams = $this->filteredParams($params, $query);

        if(!empty($params[Params::PARAMS])) {
            $qb = $this->getEntityManager()->createQueryBuilder();
            foreach ($params[Params::PARAMS] as $key => $paramGroup) {
                $alias = 'pm' . $key;
                $query->innerJoin('p.productValues', $alias)
                    ->innerJoin($alias . '.value', $alias .'v')
                    ->andWhere($qb->expr()
                        ->in($alias . 'v.uuid', $paramGroup));
            }
            $query->groupBy('p.id');
        }

        $productQuery = clone $query;

        switch ($params[Params::SORT_BY]){
            case Params::SORT_BY_PRICE_DESC:
                $productQuery->orderBy('p.price', 'DESC');
                break;
            case Params::SORT_BY_PRICE_ASC:
                $productQuery->orderBy('p.price', 'ASC');
                break;
            case Params::SORT_BY_POPULAR:
                $productQuery->orderBy('p.isBestseller', 'DESC');
                break;
        }

        // Второй параметр сортировки всегда по алфавиту
        $productQuery->addOrderBy('p.name', 'ASC');

        $mainProducts = $productQuery->getQuery()->getResult();

        $result = [
            'total' => 0,
            'products' => [],
            'params' => [],
            'priceRange' => [],
        ];


        if(0 == count($mainProducts)) {
            return $result;
        }

        $products = [];
        /** @var Product $item */
        foreach ($mainProducts as $item) {
            if($item->getProductVariations()->count() > 0) {
                $products = array_merge($products, $item->getProductVariations()->toArray());
            }else{
                $products[] = $item;
            }
        }

        list($productsQty, $minPrice, $maxPrice) = $this->getProductsQtyByValue($products, true);
        $filteredParams = array_merge($filteredParams, $productsQty);

        $offset = $params[Params::PER_PAGE] * ($params[Params::PAGE] - 1);
        $result[Params::TOTAL] = count($products);
        $result[Params::PRODUCTS] = array_slice($mainProducts, $offset, $params[Params::PER_PAGE]);
        $result[Params::PARAMS] = $filteredParams;
        $result[Params::PRICE_RANGE] = [
            Params::MIN => $minPrice,
            Params::MAX => $maxPrice
        ];

        return $result;

    }

    public function getPriceRange($query): array
    {
        $priceRange = [];
        $query->select('MIN(p.price) as min, MAX(p.price) as max');
        $prices = $query->getQuery()->getArrayResult();
        if(!empty($prices)) {
            $priceRange = [
                'min' => $prices[0]['min'],
                'max' => $prices[0]['max'],
            ];
        }

        return $priceRange;
    }

    /**
     * @return Product[]|null
     */
    public function findKits(): ?array
    {
        return $this->createQueryBuilder('p')
            ->where('p.children is not empty')
            ->getQuery()
            ->getResult();
    }

    /**
     * Возвращает товар по uuid или алиасу
     * @param $uuid
     * @return Product|null
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findOneByUuidOrAlias($uuid): ?Product
    {
        return $this->createQueryBuilder('p')
            ->where('p.isActive = true AND (p.uuid = :uuid OR p.alias = :uuid)')
            ->setParameter('uuid', $uuid)
            ->getQuery()
            ->getOneOrNullResult();
    }

    /**
     * Возвращает список товаров по uuid или алиасу
     * @param $uuids
     * @return array
     */
    public function findByUuidOrAlias($uuids): array
    {
        return $this->createQueryBuilder('p')
            ->where('p.isActive = true AND (p.uuid IN (:uuids) OR p.alias IN (:uuids))')
            ->setParameter('uuids', $uuids)
            ->getQuery()
            ->getResult();
    }

    /**
     * Возвращает все алиасы, которые подобны переданному в аргументе
     * @param string $alias
     * @return array
     */
    public function getSameAliases(string $alias): array
    {
        $products = $this->createQueryBuilder('p')
            ->where("p.alias LIKE :alias")
            ->setParameter('alias', $alias . '%')
            ->getQuery()
            ->getResult();

        $aliases = [];
        foreach ($products as $product) {
            $aliases[$product->getUuid()] = $product->getAlias();
        }

        return $aliases;
    }

    /**
     * Последние измененные хиты
     * @return array
     */
    public function getHits(): array
    {
        return $this->createQueryBuilder('p')
            ->where('p.isBestseller = true')
            ->andWhere('p.price > 0 and p.isActive = true')
            ->orderBy('p.updateAt', 'DESC')
            ->setMaxResults(self::HITS_LIMIT)
            ->getQuery()
            ->getResult();
    }

    /**
     * Возвращает список параметров для вывода в каталоге
     * @param array $groupedParams
     * @param QueryBuilder $query
     * @return array
     */
    private function filteredParams(array $params, QueryBuilder $mainQquery): array
    {
        $groupedParams = ParamsHelper::cartesian($params[Params::PARAMS]);

        $where = [];
        foreach ($groupedParams as $key => $group) {
            $ands = [];
            foreach ($group as $item) {
                $ands[] = "pmv.uuid = '$item'";
            }
            $where[] = implode(' AND ', $ands);
        }

        $query = clone $mainQquery;
        $qb = $this->getEntityManager()->createQueryBuilder();
        $query->select('DISTINCT p.id')
            ->innerJoin('p.productValues', 'pm')
            ->innerJoin('pm.value', 'pmv');

        if(count($params[Params::PARAMS]) > 1) {
            $query->andWhere(
                $qb->expr()->orX(...$where)
            );
        }

        $query->groupBy('p.id')
        ;

        $arrProdIds = $query->getQuery()->getResult();
        $productIds = [];
        array_walk($arrProdIds, function ($item) use (&$productIds){$productIds = array_merge($productIds, array_values($item));});
        $products = $this->createQueryBuilder('p')->where('p.id IN (:ids)')->setParameter('ids', $productIds)->getQuery()->getResult();

        $list = $this->getProductsQtyByValue($products, false, array_keys($params[Params::PARAMS]));
        return $list[0];
    }

    /**
     * Возвращает количество товаров по каждому параметру фильтра
     * @param Product[] $products
     * @param bool $withPrice
     * @param int $paramId - ID параметра, по которому собирать значения
     * @return array
     */
    private function getProductsQtyByValue(array $products, bool $withPrice = false, array $paramIds = []): array
    {
        $minPrice = PHP_INT_MAX;
        $maxPrice = 0;
        $productsQty = [];

        foreach ($products as $product) {
            if ($withPrice) {
                $price = $product->getPrice();
                $minPrice = min($minPrice, $price);
                $maxPrice = max($maxPrice, $price);
            }

            /** @var ProductValue $productValue */
            foreach ($product->getProductValues() as $productValue) {
                if(!$productValue->getIsFilter()) {
                    continue;
                }

                if($paramIds) {
                    if (!in_array($productValue->getValue()->getParam()->getId(), $paramIds)) {
                        continue;
                    }
                }

                $value = $productValue->getValue();
                if(!isset($productsQty[$value->getUuid()])) {
                    $productsQty[$value->getUuid()] = 0;
                }

                $productsQty[$value->getUuid()]++;
            }
        }

        $result[] = $productsQty;
        if ($withPrice) {
            $result[] = $minPrice;
            $result[] = $maxPrice;
        }

        return $result;
    }

    // /**
    //  * @return Product[] Returns an array of Product objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Product
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
