<?php

namespace App\Repository;

use App\Entity\OrderItem;
use App\Entity\Security\AuthUser;
use App\Helpers\Params;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method OrderItem|null find($id, $lockMode = null, $lockVersion = null)
 * @method OrderItem|null findOneBy(array $criteria, array $orderBy = null)
 * @method OrderItem[]    findAll()
 * @method OrderItem[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class OrderItemRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, OrderItem::class);
    }

    /**
     * Список товаров по выполненным заказам
     * @param AuthUser $customer
     * @param array $params
     * @return array
     */
    public function userProducts(AuthUser $customer, array $params): array
    {
        $query = $this->createQueryBuilder('oi')
            ->select('p.uuid as product, c.uuid as category, o.checkoutCompleteAt')
            ->innerJoin('oi.product', 'p')
            ->innerJoin('p.categoriesUuid', 'c')
            ->innerJoin('oi.order', 'o')
            ->where('o.state > 1 AND o.customer = :customer')
            ->setParameter('customer', $customer)
            ->distinct()
            ->orderBy('o.checkoutCompleteAt', 'DESC')
        ;

        if(!empty($params[Params::STATE])) {
            $query->andWhere('o.state = :state')
                ->setParameter('state', $params[Params::STATE]);
        }

        if(!empty($params[Params::PAYER])) {
            $query->join('o.payer', 'payer')
                ->andWhere('payer.uid = :payer')
                ->setParameter('payer', $params[Params::PAYER])
            ;
        }

        if(!empty($params[Params::YEAR])) {
            $query->andWhere("o.checkoutCompleteAt >= :start AND o.checkoutCompleteAt <= :end")
                ->setParameter('start', sprintf("%s-01-01 00:00:00", $params[Params::YEAR]))
                ->setParameter('end', sprintf("%s-12-31 23:59:59", $params[Params::YEAR]))
            ;
        }

        $productsArr = [
            'main' => [],
            'slave' => [],
        ];

        $category = $params[Params::CATEGORY];
        foreach ($query->getQuery()->getResult() as $item) {
            if($category && $item['category'] == $category) {
                $productsArr['main'][$item['product']] = $item['product'];
            }else{
                $productsArr['slave'][$item['product']] = $item['product'];
            }
        }

        $products = array_unique(array_merge($productsArr['main'], $productsArr['slave']));

        $result = [
            Params::TOTAL => count($products),
            Params::PRODUCTS => []
        ];

        if(empty($products)) {
            return $result;
        }

        $result[Params::PRODUCTS] = array_chunk($products, $params[Params::PER_PAGE])[$params[Params::PAGE] - 1];

        return $result;
    }


    // /**
    //  * @return OrderItem[] Returns an array of OrderItem objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('o.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?OrderItem
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
