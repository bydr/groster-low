<?php

namespace App\Repository;

use App\Entity\PaymentMethod;
use App\Entity\ShippingMethod;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method PaymentMethod|null find($id, $lockMode = null, $lockVersion = null)
 * @method PaymentMethod|null findOneBy(array $criteria, array $orderBy = null)
 * @method PaymentMethod[]    findAll()
 * @method PaymentMethod[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PaymentMethodRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PaymentMethod::class);
    }

    public function findAvailableMethods(?string $shippingMethod = null)
    {
        $query = $this->createQueryBuilder('p')
            ->where('p.isActive = true')
            ;


        if (ShippingMethod::METHOD_COMPANY == $shippingMethod) {
            $query->andWhere('p.uid IN (:methods)')
                ->setParameter("methods", [PaymentMethod::UID_BANK, PaymentMethod::UID_CARD_ONLINE]);
        }

        return $query->getQuery()->getResult();
    }

    // /**
    //  * @return PaymentMethod[] Returns an array of PaymentMethod objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?PaymentMethod
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
