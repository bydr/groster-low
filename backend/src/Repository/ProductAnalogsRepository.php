<?php

namespace App\Repository;

use App\Entity\ProductAnalogs;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ProductAnalogs|null find($id, $lockMode = null, $lockVersion = null)
 * @method ProductAnalogs|null findOneBy(array $criteria, array $orderBy = null)
 * @method ProductAnalogs[]    findAll()
 * @method ProductAnalogs[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProductAnalogsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ProductAnalogs::class);
    }

    // /**
    //  * @return ProductAnalogs[] Returns an array of ProductAnalogs objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ProductAnalogs
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
