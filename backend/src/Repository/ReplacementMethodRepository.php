<?php

namespace App\Repository;

use App\Entity\ReplacementMethod;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ReplacementMethod|null find($id, $lockMode = null, $lockVersion = null)
 * @method ReplacementMethod|null findOneBy(array $criteria, array $orderBy = null)
 * @method ReplacementMethod[]    findAll()
 * @method ReplacementMethod[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ReplacementMethodRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ReplacementMethod::class);
    }

    // /**
    //  * @return ReplacementMethod[] Returns an array of ReplacementMethod objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('r.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ReplacementMethod
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
