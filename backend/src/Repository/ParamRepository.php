<?php

namespace App\Repository;

use App\Controller\ApiController;
use App\Entity\Param;
use App\Entity\Product;
use App\Entity\Value;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query\ResultSetMapping;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Param|null find($id, $lockMode = null, $lockVersion = null)
 * @method Param|null findOneBy(array $criteria, array $orderBy = null)
 * @method Param[]    findAll()
 * @method Param[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ParamRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Param::class);
    }

    public function findAllAvailableWithProductQty()
    {
        $params = self::findBy(['isShow' => true]);
        $dql = "SELECT IDENTITY(p.value) as id, COUNT(p.id) as c FROM App\Entity\ProductValue p WHERE p.isFilter = true GROUP BY p.value ORDER BY p.value";
        $valuesRes = $this->getEntityManager()->createQuery($dql)->getArrayResult();

        $values = [];
        foreach ($valuesRes as $item) {
            $values[$item['id']] = $item['c'];
        }

        $i = 0;
        foreach ($params as $param) {
            foreach ($param->getValues() as $value) {
                if(empty($values[$value->getId()])) {
                    $productQty = 0;
                }else{
                    $productQty = $values[$value->getId()];
                }
                $value->setProductQty($productQty);
                $i++;
            }

        }

        return $params;
    }

    // /**
    //  * @return Param[] Returns an array of Param objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Param
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
