<?php

namespace App\Repository;

use App\Entity\Category;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Category|null find($id, $lockMode = null, $lockVersion = null)
 * @method Category|null findOneBy(array $criteria, array $orderBy = null)
 * @method Category[]    findAll()
 * @method Category[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CategoryRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Category::class);
    }

    /**
     * Возвращает всех алиасов, которые подобны переданному в аргументе
     * @param string $alias
     * @return array
     */
    public function getSameAliases(string $alias): array
    {
        $categories = $this->createQueryBuilder('c')
            ->where("c.alias LIKE :alias")
            ->setParameter('alias', $alias . '%')
            ->getQuery()
            ->getResult();

        $aliases = [];
        foreach ($categories as $category) {
            $aliases[$category->getUuid()] = $category->getAlias();
        }

        return $aliases;
    }

    public function getActiveWithProductQty()
    {
        $dql = "SELECT c.*, COUNT(p.id) as ct 
                FROM App\Entity\Category c
                INNER JOIN c.products p 
                WHERE c.type = :type AND c.isActive = true 
                GROUP BY c.id ORDER BY c.id";
        $categories = $this->getEntityManager()->createQuery($dql)
            ->setParameter('type', Category::TYPE_PRODUCT_SECTION)->getResult();

        return $categories;
    }

    public function getByBussinessArea($uuid)
    {
        $dql = "SELECT c FROM App\Entity\Category c 
                INNER JOIN c.products p
                WHERE p.id IN (SELECT p2.id FROM App\Entity\Category c2
                                INNER JOIN c2.products p2 WHERE c2.uuid = :uuid)
                AND c.type = :type AND c.isActive = true";

        return $this->getEntityManager()->createQuery($dql)
            ->setParameter('uuid', $uuid)
            ->setParameter('type', Category::TYPE_PRODUCT_SECTION)
            ->getResult();
    }

    public function getCategories(array $data): array
    {
        return $this->createQueryBuilder('c')
            ->where(
                $this->getEntityManager()->getExpressionBuilder()
                ->orX(
                    $this->getEntityManager()->getExpressionBuilder()->in('c.uuid', $data),
                    $this->getEntityManager()->getExpressionBuilder()->in('c.alias', $data)
                )
            )
            ->getQuery()->getResult();
    }

    // /**
    //  * @return Category[] Returns an array of Category objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Category
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
