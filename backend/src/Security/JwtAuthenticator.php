<?php

namespace App\Security;

use App\Entity\Security\AuthUser;
use App\Repository\AuthUserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Firebase\JWT\JWT;
use Symfony\Component\DependencyInjection\ParameterBag\ContainerBagInterface;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\Exception\CustomUserMessageAuthenticationException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Guard\AbstractGuardAuthenticator;
use Symfony\Component\Security\Guard\Token\PostAuthenticationGuardToken;
use Symfony\Component\Security\Http\Authenticator\AbstractAuthenticator;
use Symfony\Component\Security\Http\Authenticator\Passport\Badge\UserBadge;
use Symfony\Component\Security\Http\Authenticator\Passport\Credentials\CustomCredentials;
use Symfony\Component\Security\Http\Authenticator\Passport\Passport;
use Symfony\Component\Security\Http\Authenticator\Passport\PassportInterface;
use Symfony\Component\Security\Http\Authenticator\Passport\SelfValidatingPassport;

class JwtAuthenticator extends AbstractAuthenticator
{
    private $em;
    private $params;

    public function __construct(EntityManagerInterface $em, ContainerBagInterface $params)
    {
        $this->em = $em;
        $this->params = $params;
    }


    public function supports(Request $request): ?bool
    {
        return $request->headers->has('Authorization');
    }

    public function getCredentials(Request $request): ?string
    {
        return $request->headers->get('Authorization');
    }

    public function getUser($credentials)
    {
        try {
            $credentials = str_replace('Bearer ', '', $credentials);
            $jwt = (array)JWT::decode(
                $credentials,
                $this->params->get('jwt_secret'),
                ['HS256']
            );
            if ($jwt['type'] != 'access')
                return null;
            return $this->em->getRepository(AuthUser::class)
                ->findOneBy([
                    'uid' => $jwt['user'],
                ]);
        } catch (\Exception $exception) {
            throw new AuthenticationException($exception->getMessage());
        }
    }

    public function authenticate(Request $request): PassportInterface
    {
        $creds = $this->getCredentials($request);
        if ($creds == null) {
            throw new CustomUserMessageAuthenticationException('No API token provided');
        }
        $user = $this->getUser($creds);

        return new Passport(new UserBadge($user->getUid()), new CustomCredentials(
            function ($credentials, UserInterface $user) {
                if ($user->IsLogin())
                    return true;
                return false;
            },
            $creds
        ));

    }

    public function onAuthenticationSuccess(Request $request, TokenInterface $token, string $firewallName): ?Response
    {
        return null;
    }

    public function onAuthenticationFailure(Request $request, AuthenticationException $exception): ?Response
    {
        return new JsonResponse([
            'code' => Response::HTTP_UNAUTHORIZED,
            'message' => $exception->getMessage()
        ], Response::HTTP_UNAUTHORIZED);
    }
}