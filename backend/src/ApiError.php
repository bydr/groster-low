<?php

namespace App;

use function Symfony\Component\DependencyInjection\Loader\Configurator\service_locator;

class ApiError
{
    const ERROR_UNHANDLED = "GR-000000";
    const ERROR_BAD_REQUEST = "GR-000001";
    const ERROR_DUPLICATE_EMAIL = "GR-000002";
    const ERROR_INVALID_CODE = "GR-000003";
    const ERROR_ACCESS_DENIED = "GR-000004";
    const ERROR_INVALID_JSON = "GR-000005";
    const ERROR_INVALID_EMAIL = "GR-000006";
    const ERROR_NOT_FOUND = "GR-000007";
    const ERROR_REMOVE_AUTH_HEADER = "GR-000008";
    const ERROR_INVALID_EMAIL_OR_PASS = "GR-000009";
    const ERROR_INVALID_PHONE = "GR-000010";
    const ERROR_DUPLICATE_PHONE = "GR-000011";
    const ERROR_EMPTY_PRODUCT = 'GR_000012';
    const ERROR_INVALID_DATA = 'GR_000013';
    const ERROR_CART = 'GR_000014';
    const ERROR_INVALID_PROMOCODE = 'GR_000015';
    const ERROR_INVALID_CART = 'GR_000016';
    const ERROR_UNAUTHORIZATION = 'GR_000017';
    const ERROR_DUPLICATE_ALIAS = 'GR_000018';
    const ERROR_INVALID_PAYER = 'GR_000019';
    const ERROR_INVALID_SHIPPING_ADDRESS = 'GR_000020';
    const ERROR_REGION = 'GR_000021';
    const ERROR_INVALID_SHIPPING_METHOD = 'GR_000022';
    const ERROR_INVALID_REPLACEMENT = 'GR_000023';
    const ERROR_INVALID_BLOCK = 'GR_000024';
    const ERROR_UNKNOWN_EMAIL = 'GR_000025';
    const ERROR_INVALID_PRODUCT = 'GR_000026';
    const ERROR_DUPLICATE_PROMOCODE = 'GR_000027';
    const INVALID_PASSWORD = 'GR_000028';
    const ERROR_ORDER_NOT_CANCELED = 'GR_000029';
    const ERROR_INVALID_ORDER_STATE = 'GR_000030';
    const ERROR_FILE_DO_NOT_READY = 'GR_000031';
    const ERROR_DOES_NOT_EXIST_REQUIRED_FIELDS = 'GR_000032';
    const ERROR_DOES_NOT_EXIST_TYPE = 'GR_000033';
    const ERROR_INVALID_PROMO_VALUE = 'GR_000034';
    const ERROR_INVALID_DATE = 'GR_000035';
    const ERROR_UPDATE = 'GR_000036';
    const ERROR_SAVE_PRICELIST_QUERY = 'GR_000037';
    const ERROR_INVALID_CATEGORY = 'GR_000038';
    const ERROR_NOT_FOUND_ORDER = 'GR_000039';

    /**
     * Возвращает текст ошибки для кода
     *
     * @param $code string
     * @return string
     */
    public static function getErrorMessage(string $code):string
    {
        $codes = [
            self::ERROR_UNHANDLED => 'Что-то пошло не так',
            self::ERROR_BAD_REQUEST => 'Непредвиденная ошибка',
            self::ERROR_DUPLICATE_EMAIL => 'Такой email уже зарегистрирован',
            self::ERROR_INVALID_CODE => 'Не верный код подтверждения',
            self::ERROR_ACCESS_DENIED => 'Доступ запрещен',
            self::ERROR_INVALID_JSON => 'Неверная структура JSON данных',
            self::ERROR_INVALID_EMAIL => 'Не корректный адрес почты',
            self::ERROR_NOT_FOUND => 'Данные не найдены',
            self::ERROR_REMOVE_AUTH_HEADER => 'Убрать заголовок Authorization',
            self::ERROR_INVALID_EMAIL_OR_PASS => 'Не верный email или пароль',
            self::ERROR_INVALID_PHONE => 'Не корректный номер телефона',
            self::ERROR_DUPLICATE_PHONE => 'Такой телефон уже зарегистрирован',
            self::ERROR_EMPTY_PRODUCT => 'Товар закончился на складе',
            self::ERROR_INVALID_DATA => 'Ошибка данных',
            self::ERROR_CART => 'Ошибка данных корзины',
            self::ERROR_INVALID_PROMOCODE => 'Проверьте значение промокода',
            self::ERROR_INVALID_CART => 'Проверьте uid корзины',
            self::ERROR_UNAUTHORIZATION => 'Проверьте token авторизации',
            self::ERROR_DUPLICATE_ALIAS => 'Алиас не уникален',
            self::ERROR_INVALID_PAYER => 'Неверный плательщик',
            self::ERROR_INVALID_SHIPPING_ADDRESS => 'Неверный адрес доставки',
            self::ERROR_REGION => 'Не указан населенный пункт',
            self::ERROR_INVALID_SHIPPING_METHOD => 'Не выбран способ доставки',
            self::ERROR_INVALID_REPLACEMENT => 'Неверный метод замены отсутствующего товара',
            self::ERROR_INVALID_BLOCK => 'Неверный идентификатор блока',
            self::ERROR_UNKNOWN_EMAIL => 'Указанный email не зарегистрирован',
            self::ERROR_INVALID_PRODUCT => 'Неверные данные товара',
            self::ERROR_DUPLICATE_PROMOCODE => 'Повторное использование промокода для заказа',
            self::INVALID_PASSWORD => 'Неверный пароль',
            self::ERROR_ORDER_NOT_CANCELED => 'Заказ не может быть отменен. Попробуйте еще раз или обратитесь к менеджеру',
            self::ERROR_INVALID_ORDER_STATE => 'Неверный статус заказа',
            self::ERROR_FILE_DO_NOT_READY => 'Файл на стадии формирования. Попробуйте повторить операцию позже',
            self::ERROR_DOES_NOT_EXIST_REQUIRED_FIELDS => 'Не заполнены все обязательные поля',
            self::ERROR_DOES_NOT_EXIST_TYPE => 'Не указан тип',
            self::ERROR_INVALID_PROMO_VALUE => 'Не корректное значение скидки',
            self::ERROR_INVALID_DATE => 'Неверный формат даты',
            self::ERROR_UPDATE => 'Ошибка обновления данных',
            self::ERROR_SAVE_PRICELIST_QUERY => 'Не удалось сохранить запрос на отправку прайслиста. Попробуйте еще раз или обратитесь к менеджеру',
            self::ERROR_INVALID_CATEGORY => 'Неверная категория',
            self::ERROR_NOT_FOUND_ORDER => 'Заказ не найден',
        ];


        return $codes[$code] ?: "";
    }
}