openapi: 3.0.0
info:
  title: Эндпоинты для вебхуков из 1С
  description: Эндпоинты для вебхуков из 1С
  version: '1.0'

servers:
  - url:  https://api.grosternew.vertical-web.ru
    description: test

paths:
  /api/v1/order/change-status/{uid}:
    put:
      description: изменение статуса заказа из 1С
      security:
        - ApiKeyAuth: []
      parameters:
        - in: path
          name: uid
          description: идентификатор заказа
          required: true
          schema:
            type: string
            example: 'gcart4b340550242239.64159797'
        - in: query
          name: state
          description: статус заказа  <br>
            * `3` - выполнен<br>
            * `4` - отменен<br>
            * `5` - в обработке
            * `6` - передан курьеру
          required: true
          schema:
            type: integer
            enum:
              - 3
              - 4
              - 5
              - 6
            example: 3
      responses:
        200:
          description: OK
        400:
          description: Ошибка изменения статуса
          content:
            application/json:
              schema:
                $ref: './error.yml#/components/schemas/ApiError'
        401:
          description:  невалидный ключ доступа
        404:
          description: not found
  /api/v1/order/change-products/{uid}:
    put:
      description: изменение состава заказа, который в обработке, из 1С
      security:
        - ApiKeyAuth: [ ]
      parameters:
        - in: path
          name: uid
          description: идентификатор заказа
          required: true
          schema:
            type: string
            example: 'gcart4b340550242239.64159797'
      requestBody:
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/ChangeOrderProductsRequest'
      responses:
        200:
          description: OK
        400:
          description: Ошибка изменения статуса
          content:
            application/json:
              schema:
                $ref: './error.yml#/components/schemas/ApiError'
        401:
          description: невалидный ключ доступа
        404:
          description: not found
  /api/v1/product/rest-update:
    put:
      description: Обновление остатков товара из 1С
      security:
        - ApiKeyAuth: [ ]
      requestBody:
        content:
          application/json:
            schema:
              type: object
              properties:
                products:
                  $ref: '#/components/schemas/RequestProductRestUpdate'
      responses:
        200:
          description: OK
        400:
          description: Ошибка изменения остатков
          content:
            application/json:
              schema:
                $ref: './error.yml#/components/schemas/ApiError'
        401:
          description: невалидный ключ доступа
  /api/v1/server/promocode/add:
    post:
      description: Создание промокода
      security:
        - ApiKeyAuth: [ ]
      requestBody:
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/CodeRequest'
      responses:
        200:
          description: OK
        400:
          description: Ошибка изменения остатков
          content:
            application/json:
              schema:
                $ref: './error.yml#/components/schemas/ApiError'
        401:
          description: невалидный ключ доступа
  /api/v1/server/promocode/{code}:
    get:
      description: получение данных промокода
      security:
        - ApiKeyAuth: [ ]
      parameters:
        - in: path
          name: code
          required: true
          schema:
            type: string
            example: newyear2022
      responses:
        200:
          description: OK
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/CodeResponse'
        400:
          description: Bad Request
          content:
            application/json:
              schema:
                $ref: './error.yml#/components/schemas/ApiError'
        401:
          description: нет доступа
          content:
            application/json:
              schema:
                $ref: './error.yml#/components/schemas/ApiError'
    put:
      description: обновление данных промокода
      security:
        - ApiKeyAuth: [ ]
      parameters:
        - in: path
          name: code
          required: true
          schema:
            type: string
            example: newyear2022
      requestBody:
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/CodeRequest'
      responses:
        200:
          description: OK
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/CodeResponse'
        400:
          description: Bad Request
          content:
            application/json:
              schema:
                $ref: './error.yml#/components/schemas/ApiError'
        401:
          description: нет доступа
          content:
            application/json:
              schema:
                $ref: './error.yml#/components/schemas/ApiError'
components:
  schemas:
    ChangeOrderProductsRequest:
      type: object
      properties:
        ShippingPrice:
          description: стоимость доставки в копейках
          type: integer
          example: 15000
        Products:
          type: array
          items:
            type: object
            properties:
              IDProduct:
                description: идентификатор товара
                type: string
                example: '00000000-0000-0000-0000-000000000000'
              IDKitProduct:
                description: идентификатор комплекта
                type: string
                example: '00000000-0000-0000-0000-000000000000'
              CountProduct:
                description: количество единиц товара в заказе
                type: integer
                example: 5
              PriceProduct:
                description: цена продажи товара в копейках
                type: integer
                example: 250000
              SumDiscount:
                description: сумма скидки в копейках
                type: integer
                example: 10000
              ThisProductSample:
                description: является ли товар образцом
                type: boolean
                example: false
    RequestProductRestUpdate:
      type: array
      items:
        type: object
        properties:
          id:
            description: идентификатор товара
            type: string
            example: '7433fd02-7a00-11e5-b4af-742f68689961'
          is_delete:
            description: пометка на удаление товара
            type: boolean
            example: false
          rests:
            description: остатки по складам
            type: array
            items:
              type: object
              properties:
                store:
                  description: идентификатор склада
                  type: string
                  example: 'c6daff65-c715-11e7-91d5-448a5b2839e6'
                value:
                  description: значение остатка на складе
                  type: integer
                  example: 356
    CodeRequest:
      allOf:
        - $ref: '#/components/schemas/Code'
        - type: object
          required:
            - categories
          properties:
            categories:
              description: список категорий, к которым привязан код, разделенных запятой
              type: string
              example: "00000000-0000-0000-0000-000000000000,00000000-0000-0000-0000-000000000001"
    CodeResponse:
      allOf:
        - $ref: '#/components/schemas/Code'
        - type: object
          required:
            - categories
          properties:
            categories:
              type: array
              items:
                type: string
                example: "00000000-0000-0000-0000-000000000000"
    Code:
      type: object
      required:
        - name
        - code
        - type
        - value
        - reusable
        - start
        - finish
      properties:
        name:
          description: название
          type: string
          example: "new year 2022"
        code:
          description: уникальный slug
          type: string
          maxLength: 20
          example: "newYear2022"
        type:
          type: integer
          enum:
            - 1
            - 2
          example: 1
          description: >
            тип скидки <br>
            * `1` - в процентах<br>
            * `2` - в рублях
        value:
          description: размер скидки (для типа 1 - в копейках, для типа 2 - в процентах)
          type: number
          example: 10
        reusable:
          description: многоразовый
          type: boolean
          example: true
        start:
          description: время начала действия промокода в формате ISO-8601
          type: string
          example: '2022-01-01T11:20:33+0300'
        finish:
          description: время окончания действия промокода в формате ISO-8601
          type: string
          example: '2022-01-26T11:20:33+0300'
  securitySchemes:
    ApiKeyAuth:
      type: apiKey
      in: query
      name: api_key
